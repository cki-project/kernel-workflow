---
include:
  - {project: cki-project/cki-lib, ref: production, file: .gitlab/ci_templates/cki-common.yml}
  - {project: cki-project/cki-lib, ref: production, file: .gitlab/ci_templates/cki-sast.yml}

linting:
  extends: .tox
  variables:
    TOX_ARGS: "-e lint"


tests:
  extends: .tox
  variables:
    TOX_ARGS: "-e test"


yamllint:
  extends: .yamllint

.images:
  variables:
    CHANGES: "{.gitlab-ci.yml,webhook/{common,defs,graphql,libbz,libjira,owners,pipelines,rh_metadata,session,session_events}.py,utils/{labels,rh_metadata,rh_webhooks}.yaml}"
    SUPPORTS_STAGING: 'true'
    SMOKE_TEST_COMMANDS_ACK_NACK: |
      python3 -m webhook.ack_nack --help
      python3 -m webhook.utils.owners_validator --help
      python3 -m webhook.utils.update_git_mirror --help
      python3 -m webhook.utils.update_owners --help
    SMOKE_TEST_COMMANDS_BACKPORTER: |
      python3 -m webhook.utils.backporter --help
      python3 -m webhook.utils.check_for_backports --help
      python3 -m webhook.utils.update_git_mirror --help
    SMOKE_TEST_COMMANDS_BUGLINKER: |
      python3 -m webhook.buglinker --help
    SMOKE_TEST_COMMANDS_CKIHOOK: |
      python3 -m webhook.ckihook --help
    SMOKE_TEST_COMMANDS_COMMIT_COMPARE: |
      python3 -m webhook.commit_compare --help
      python3 -m webhook.utils.update_git_mirror --help
    SMOKE_TEST_COMMANDS_CONFIGSHOOK: |
      python3 -m webhook.configshook --help
      python3 -m webhook.utils.update_git_mirror --help
    SMOKE_TEST_COMMANDS_ELNHOOK: |
      python3 -m webhook.elnhook --help
    SMOKE_TEST_COMMANDS_FIXES: |
      python3 -m webhook.fixes --help
      python3 -m webhook.utils.check_for_fixes --help
      python3 -m webhook.utils.update_git_mirror --help
    SMOKE_TEST_COMMANDS_JIRAHOOK: |
      python3 -m webhook.jirahook --help
      python3 -m webhook.utils.update_git_mirror --help
      python3 -m webhook.utils.update_owners --help
    SMOKE_TEST_COMMANDS_LIMITED_CI: |
      python3 -m webhook.limited_ci --help
    SMOKE_TEST_COMMANDS_MERGEHOOK: |
      python3 -m webhook.mergehook --help
      python3 -m webhook.utils.merge_conflicts_check --help
      python3 -m webhook.utils.report_generator --help
      python3 -m webhook.utils.update_git_mirror --help
      python3 -m webhook.utils.update_owners --help
    SMOKE_TEST_COMMANDS_METRICS: |
      python3 -m webhook.metrics --help
    SMOKE_TEST_COMMANDS_SAST: |
      python3 -m webhook.sast --help
    SMOKE_TEST_COMMANDS_SIGNOFF: |
      python3 -m webhook.signoff --help
    SMOKE_TEST_COMMANDS_SPRINTER: |
      python3 -m webhook.sprinter --help
    SMOKE_TEST_COMMANDS_SUBSYSTEMS: |
      python3 -m webhook.subsystems --help
      python3 -m webhook.utils.update_git_mirror --help
      python3 -m webhook.utils.update_owners --help
    SMOKE_TEST_COMMANDS_TERMINATOR: |
      python3 -m webhook.terminator --help
    SMOKE_TEST_COMMANDS_UMB_BRIDGE: |
      python3 -m webhook.umb_bridge --help

  parallel:
    matrix:
      - IMAGE_NAME: ack-nack
        CHANGES_EXTRA: "{webhook/{ack_nack,base_mr,cdlib,libjira,utils,kconfigs},utils/*}"
        SMOKE_TEST_COMMANDS_VARIABLE: SMOKE_TEST_COMMANDS_ACK_NACK
      - IMAGE_NAME: backporter
        CHANGES_EXTRA: "{webhook/{rhissue,libbackport,kgit,utils},utils/*.yml}"
        SMOKE_TEST_COMMANDS_VARIABLE: SMOKE_TEST_COMMANDS_BACKPORTER
      - IMAGE_NAME: buglinker
        CHANGES_EXTRA: "webhook/{buglinker,description,libjira,rhissue}"
        SMOKE_TEST_COMMANDS_VARIABLE: SMOKE_TEST_COMMANDS_BUGLINKER
      - IMAGE_NAME: ckihook
        CHANGES_EXTRA: "{webhook/{ckihook,base_mr,fragments,description,users}}"
        SMOKE_TEST_COMMANDS_VARIABLE: SMOKE_TEST_COMMANDS_CKIHOOK
      - IMAGE_NAME: commit-compare
        CHANGES_EXTRA: "{webhook/{commit_compare,base_mr,cdlib,utils},utils/upstream_kernel_git_repos.yml}"
        SMOKE_TEST_COMMANDS_VARIABLE: SMOKE_TEST_COMMANDS_COMMIT_COMPARE
      - IMAGE_NAME: component-webview
        CHANGES_EXTRA: "webhook/component_webview"
      - IMAGE_NAME: configshook
        CHANGES_EXTRA: "{webhook/{configshook,base_mr,cdlib,utils,kconfigs},utils/upstream_kernel_git_repos.yml}"
        SMOKE_TEST_COMMANDS_VARIABLE: SMOKE_TEST_COMMANDS_CONFIGSHOOK
      - IMAGE_NAME: elnhook
        CHANGES_EXTRA: "{webhook/{elnhook}"
        SMOKE_TEST_COMMANDS_VARIABLE: SMOKE_TEST_COMMANDS_ELNHOOK
      - IMAGE_NAME: fixes
        CHANGES_EXTRA: "{webhook/{fixes,base_mr,cdlib,utils},utils/upstream_kernel_git_repos.yml}"
        SMOKE_TEST_COMMANDS_VARIABLE: SMOKE_TEST_COMMANDS_FIXES
      - IMAGE_NAME: jirahook
        CHANGES_EXTRA: "{webhook/{jirahook,base_mr,cpc,rhissue,rhissue_tests,description,fragments,table,utils,kconfigs},utils/*}"
        SMOKE_TEST_COMMANDS_VARIABLE: SMOKE_TEST_COMMANDS_JIRAHOOK
      - IMAGE_NAME: kwf-utils
        CHANGES_EXTRA: "webhook/utils"
      - IMAGE_NAME: limited-ci
        CHANGES_EXTRA: "webhook/limited_ci.py"
        SMOKE_TEST_COMMANDS_VARIABLE: SMOKE_TEST_COMMANDS_LIMITED_CI
      - IMAGE_NAME: mergehook
        CHANGES_EXTRA: "{webhook/{mergehook,base_mr,utils},utils/*}"
        SMOKE_TEST_COMMANDS_VARIABLE: SMOKE_TEST_COMMANDS_MERGEHOOK
      - IMAGE_NAME: metrics
        CHANGES_EXTRA: "webhook/metrics"
        SMOKE_TEST_COMMANDS_VARIABLE: SMOKE_TEST_COMMANDS_METRICS
      - IMAGE_NAME: sast
        CHANGES_EXTRA: "webhook/{sast,base_mr,base_mr_mixins}"
        SMOKE_TEST_COMMANDS_VARIABLE: SMOKE_TEST_COMMANDS_SAST
      - IMAGE_NAME: signoff
        CHANGES_EXTRA: "webhook/{signoff,base_mr,fragments,description,users}"
        SMOKE_TEST_COMMANDS_VARIABLE: SMOKE_TEST_COMMANDS_SIGNOFF
      - IMAGE_NAME: sprinter
        CHANGES_EXTRA: "webhook/sprinter.py"
        SMOKE_TEST_COMMANDS_VARIABLE: SMOKE_TEST_COMMANDS_SPRINTER
      - IMAGE_NAME: subsystems
        CHANGES_EXTRA: "{webhook/{subsystems,base_mr,cdlib,utils,kconfigs},utils/upstream_subsystems_repo.yml}"
        SMOKE_TEST_COMMANDS_VARIABLE: SMOKE_TEST_COMMANDS_SUBSYSTEMS
      - IMAGE_NAME: terminator
        CHANGES_EXTRA: "webhook/terminator.py"
        SMOKE_TEST_COMMANDS_VARIABLE: SMOKE_TEST_COMMANDS_TERMINATOR
      - IMAGE_NAME: umb-bridge
        CHANGES_EXTRA: "webhook/{umb_bridge,description,users,jira,tracker}"
        SMOKE_TEST_COMMANDS_VARIABLE: SMOKE_TEST_COMMANDS_UMB_BRIDGE

build:
  extends: [.build, .images]

tag:
  extends: [.tag, .images]

prod:
  extends: [.deploy_production, .images]

stage:
  extends: [.deploy_staging, .images]

prod-git-tag:
  extends: .deploy_production_tag
