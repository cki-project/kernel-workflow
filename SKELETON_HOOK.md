#Skeleton Webhook

Skeleton.py is a barebones webhook implementation. It serves as a foundation for building more advanced webhooks. To use, install dependencies from kernel workflow and run python3 -m webhook.skeleton --merge-request {MR URL}
