# Buglinker Webhook

## Purpose

All Merge Requests must have at least one "Bugzilla:" entry that contains a URL
for a bugzilla.  This webhook adds a link to the Merge Request in the "Links"
section of each Bugzilla.  This webhook only acts on Merge Requests in the
ready state, ie) it does not act on draft Merge Requests.

## Manual runs

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.buglinker \
        --disable-inactive-branch-check \
        --merge-request https://gitlab.com/group/repo/-/merge_requests/1 \

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.

In addition, this webhook requires a bugzilla API key. Generate one from
[here](https://bugzilla.redhat.com/userprefs.cgi?tab=apikey) and then set
the corresponding environment variable `export BUGZILLA_API_KEY='1234567890abcedf'`.
