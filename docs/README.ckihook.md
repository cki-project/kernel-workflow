# ckihook Webhook

## Purpose

This webhook handles CKI for internal contributions.

## Manual Runs

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.ckihook \
        --disable-inactive-branch-check \
        --merge-request https://gitlab.com/group/repo/-/merge_requests/1 \
        --action pipeline

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.

## Environment variables

| Name                                   | Secret | Required | Description                                                                        |
|----------------------------------------|--------|----------|------------------------------------------------------------------------------------|
| `DATAWAREHOUSE_URL`                    | no     | yes      | URL to DataWarehouse.                                                              |
| `DATAWAREHOUSE_TOKEN_REPORTER`         | yes    | yes      | Token for DataWarehouse.                                                           |
| `MAX_RETRIES_MISMATCHING_STATUS`       | no     | no       | How many job retries the webhook is still willing to try to fix mismatching status |

For manual runs, you can keep `DATAWAREHOUSE_TOKEN_REPORTER` empty to access public data,
or set it to `"oidc"` if you need to fetch data restricted to Red Hat associates,
just make sure you have a valid Kerberos TGT, with `kinit yourusername@REALM`.
Read more at <https://cki-project.org/l/datawarehouse/api/auth>.

## Reporting

- Label prefix: `CKI::`
- Label prefix: `CKI_*::`
- Comment header: **CKI Pipelines Status**

The webhook reports the overall result of the check by applying a scoped label
to the MR with the prefix `CKI::`.

Additionally, the webhook will apply a separate label indicating the status of each
expected pipeline for the MR.

An MR may have one or more of these labels depending on the expected pipeline
types for the target branch of the MR as defined in the utils/rh_metadata.yml file.

The hook will leave a comment on the MR with the details of the check. The
header of the comment is **CKI Pipelines Status**. This comment will be edited
with updated information every time the hook runs. Refer to the timestamp at the
end of the comment to see when the hook last evaluated the MR.

## Triggering

To trigger reevaluation of an MR by the ckihook webhook either remove any of
the existing `CKI` scoped labels mentioned above or a leave a comment with one
of the following:

- `request-cki-evaluation`
- `request-ckihook-evaluation`

Alternatively, to retrigger all the webhooks at the same time leave a note in
the MR with `request-evaluation`.

## Workaround for mismatching status between Trigger Job status Downstream pipeline

There's a flaky bug in multi-project pipelines tracked by [gitlab#340064].
Sometimes, when downstream pipeline finishes running all its jobs,
its overall status does not match the upstream trigger job status,
which can be confusing when trying to evaluate other failures.
Most times, this can be fixed rerunning any job in the downstream pipeline,
and `ckihook` currently tries to rerun "check-kernel-results" when it finds the bug.

[gitlab#340064]: https://gitlab.com/gitlab-org/gitlab/-/issues/340064

## Listening to DataWarehouse messages to retry 'check-kernel-results' when issues are triaged

Whenever failures are triaged, DataWarehouse sends a message tagged as
"checkout_issueoccurrences_changed" with the checkout data, referencing its pipeline.
Additionally, it sends a "ready_to_report" message when all builds in a checkout finish processing.
`ckihook` listens for these messages and retries the "check-kernel-results" job, if it failed.
