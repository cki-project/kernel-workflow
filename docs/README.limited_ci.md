# Limited CI Webhook

## Purpose

This webhook manages CI for external contributors.

## Manual runs

You can run the webhook manually on a merge request URL with the command:

  ```bash
  python3 -m webhook.limited_ci \
      --disable-inactive-branch-check \
      --merge-request https://gitlab.com/group/repo/-/merge_requests/1
  ```

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.

## Environment variables

On top of common expected variables, the following ones are required:

* `LIMITED_CI_ROUTING_KEYS`: Whitespace-separated list of RabbitMQ routing keys.
* `LIMITED_CI_QUEUE`: RabbitMQ queue name.
* `CONFIG_PATH`: Path to the project configuration to use.

## Configuration format and example

Multiple projects per single webhook instance are supported.

Example of two project configurations sharing the pipeline projects:

```yaml
.pipeline_configs:
  .cki_external_pipeline_project: <path_with_namespace>
  .cki_external_pipeline_branch: <branch_name>

path_to_project:
  .extends: .pipeline_configs
  .members_of: <group or project_path_with_namespace>

path_to_project2:
  .extends: .pipeline_configs
  .members_of: <group or project_path_with_namespace>
```

Example of two project configurations *not* sharing the pipeline projects:

```yaml
path_to_project:
  .cki_external_pipeline_project: <path_with_namespace>
  .cki_external_pipeline_branch: <branch_name>
  .members_of: <group or project_path_with_namespace>

path_to_project2:
  .cki_external_pipeline_project: <path_with_namespace>
  .cki_external_pipeline_branch: <branch_name>
  .members_of: <group or project_path_with_namespace>
```

Add any variables required by the pipelines into either the project-specific or
common settings depending on your needs.
