# Sprinter

## Purpose

This webhook enforces the CKI workflow and manages the `CWF::*` labels on
issues and merge requests.

### Iterations

The webhook listens to closed issues and assigns them to the current sprint if
they are not assigned to an iteration yet.

### Planning labels

The webhook listens to closed and reopened issues and tags
them with a standardized label for planning purposes.

The label is composed of the year and the week number when the issue is
closed: `CWF::Sprint::{year}-week-{week_number}`.

### Incident labels

The webhook listens to issue updates and updates the `CWF::Incident::*` and
`CWF::Type::Incident` labels so they stay consistent when using the

[incident board](https://gitlab.com/groups/cki-project/-/boards/4642958):

- if an open issue is tagged with `CWF::Incident::*`, tag with
  `CWF::Type::Incident`
- if an open issue is tagged with `CWF::Type::Incident`, but no
  `CWF::Incident::*`, tag with `CWF::Incident::Active`
- if the `CWF::Type::Incident` label is removed from an open issue, remove any
  `CWF::Incident::*` labels
- if an issue is closed, remove `CWF::Incident::*`

### Team labels

The webhook listens to issue updates and adds correct `CWF::Team::*` labels:

- if an issue is not already tagged with `CWF::Team::*`, tag with
  `CWF::Team::*` depending on the project/group as specified in the configuration

### Stage labels

The webhook listens to issue updates and ensures correct `CWF::Stage::*` labels:

- if an issue is closed, remove `CWF::Stage::*`

### Issue links

The webhook listens to merge request changes and enforces the presence of an
issue link in the merge request description.

The resulting labels are `CWF::Issue::OK` (link found) or `CWF::Issue::Missing`
(link missing). Labels are updated on changes to the merge request. A label
update can be requested by manually removing the current label from the merge
request.

### Automatic weights

The webhook listens to issue updates and adds a default weight if missing.

## Running

Running the service requires the following parameters:

  ```bash
  python3 -m webhook.sprinter \
      --rabbitmq-host RABBITMQ_HOST \
      --rabbitmq-port RABBITMQ_PORT \
      --rabbitmq-user RABBITMQ_USER \
      --rabbitmq-password RABBITMQ_PASSWORD \
      --rabbitmq-exchange WEBHOOK_RECEIVER_EXCHANGE \
      --rabbitmq-routing-key SPRINTER_ROUTING_KEYS
  ```

## Configuration

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.

| Environment variable   | Description                                     |
|------------------------|-------------------------------------------------|
| `LABELS_NAMESPACE`     | Gitlab group where labels are created           |
| `SPRINTER_CONFIG`      | Configuration for the sprinter                  |
| `SPRINTER_CONFIG_PATH` | Path of the configuration file for the sprinter |

The configuration in `SPRINTER_CONFIG` can be provided in YAML or JSON format:

```yaml
---
# select projects for MR issue checks
issue_checks:
  # project-level rule
  - project: https://instance/group/project
    enabled: false  # defaults to enabled
  # all projects beneath the group
  - group: https://instance/group
    enabled: false
  # add labels at project level instead of group
  - project: https://instance/group/foo
    level: project
  # only for issues that belong to a certain team
  - team_label: 'CKI'
  # disable for everything else
  - enabled: false
# select projects for issue sprint labels
sprint_labels: []
# select projects for issue incident labels
incident_labels: []
# select projects for issue type labels
type_labels:
  # add a certain issue type label
  - project: https://gitlab.com/cki-project/kernel-ark
    type: 'ARK'
# select projects for automatic weights
automatic_weights:
  - project: https://gitlab.com/cki-project/cki-tools
    weight: 5
```
