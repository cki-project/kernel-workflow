# Subsystems Webhook

## Purpose

This webhook identifies the subsystems that a Merge Request's changeset
modifies and adds a "Subsystem:" specific label. It also adds an ExternalCI::
label that is set to ExternalCI::OK when all external testing requirements
indicated by owners.yaml are met.

## Manual Runs

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.subsystems \
        --disable-inactive-branch-check \
        --owners-yaml owners.yml \
        --repo-path /path/to/linus-tree

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.

## Reporting

- Label prefix: `ExternalCI::`
- Label prefix: `Subsystem:`

The webhook reports the overall result of the check by applying a scoped label
to the MR with the prefix `ExternalCI::`. There may be additional double-scoped
`ExternalCI::<sst>::<status>` labels created if the MR files touch areas defined
in owners.yaml that have readyForMergeDeps set. The corresponding SST teams will
manage these labels and set them to `OK` when their testing is complete or `Waived`
if testing will be skipped.
