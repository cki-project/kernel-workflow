"""Fake MR objects."""
from unittest import mock

from webhook.defs import JIRA_SERVER
from webhook.defs import JPFX
from webhook.jirahook import JiraMR
from webhook.rh_metadata import Projects


def make_mr(mr_url, query_results_list=None, projects=None, is_dependency=False):
    """Help make an MR."""
    test_session = mock.Mock()
    graphql = mock.Mock()
    graphql.get_mr_descriptions.return_value = {}
    results = [{'project': result} for result in query_results_list] if query_results_list else []
    graphql.check_query_results.side_effect = results if results else [None]
    if not projects:
        projects = Projects(extra_projects_paths=['tests/assets/rh_projects_private.yaml'])
    test_session.graphql = graphql
    test_session.rh_projects = projects
    return JiraMR.new(test_session, mr_url, is_dependency=is_dependency)


MR101_JIRA = f'JIRA: {JIRA_SERVER}browse/{JPFX}101\n'
MR404_JIRA = f'JIRA: {JIRA_SERVER}browse/{JPFX}2323232\n'
MR410_JIRA = f'JIRA: {JIRA_SERVER}browse/{JPFX}1234567\n'

MR101_DICT = {'id': 'gid://gitlab/Project/24152864',
              'mr': {'title': 'Does not actually matter',
                     'description': MR101_JIRA,
                     'id': 'gid://gitlab/MergeRequest/21122221',
                     'state': 'opened',
                     'draft': False,
                     'sourceBranch': 'feature',
                     'targetBranch': 'main',
                     'headPipeline': {'finishedAt': None},
                     'files': [{'path': 'fs/ext/fs.c'}, {'path': 'lib/include/what.h'}],
                     'labels': {'nodes': []},
                     'commits': {'pageInfo': {'hasNextPage': False, 'endCursor': 'Mw'},
                                 'nodes': [{'sha': '0aa467549b4e997d023c29f4d481aee01b9e9471',
                                            'description': MR410_JIRA +
                                            'CVE: CVE-1235-13516\n'},
                                           {'sha': 'e53eab9f887f784044ad32ef5c082695831d90d9',
                                            'description': MR410_JIRA +
                                            'CVE: CVE-1235-13516\n'},
                                           {'sha': '88cdd4035228dac16878eb907381afea6ceffeaa',
                                            'description': MR410_JIRA +
                                            'CVE: CVE-1235-13516\n'},
                                           {'sha': 'ce1fdd9354bdc315e49a40dc9da3ab03bf6af7b3',
                                            'description': MR404_JIRA},
                                           {'sha': 'f77278fcd9cef99358adc7f5e077be795a54ffca',
                                            'description': MR404_JIRA}]
                                 }}}

MR410_DICT = {'id': 'gid://gitlab/Project/24152864',
              'mr': {'title': 'Does not actually matter',
                     'description': MR410_JIRA +
                     'Depends: !404\n'
                     'CVE: CVE-1235-13516\n',
                     'id': 'gid://gitlab/MergeRequest/21122221',
                     'state': 'opened',
                     'draft': False,
                     'sourceBranch': 'feature',
                     'targetBranch': 'main',
                     'headPipeline': {'finishedAt': None},
                     'files': [{'path': 'fs/ext/fs.c'}, {'path': 'lib/include/what.h'}],
                     'labels': {'nodes': []},
                     'commits': {'pageInfo': {'hasNextPage': False, 'endCursor': 'Mw'},
                                 'nodes': [{'sha': '0aa467549b4e997d023c29f4d481aee01b9e9471',
                                            'description': MR410_JIRA +
                                            'CVE: CVE-1235-13516\n'},
                                           {'sha': 'e53eab9f887f784044ad32ef5c082695831d90d9',
                                            'description': MR410_JIRA +
                                            'CVE: CVE-1235-13516\n'},
                                           {'sha': '88cdd4035228dac16878eb907381afea6ceffeaa',
                                            'description': MR410_JIRA +
                                            'CVE: CVE-1235-13516\n'},
                                           {'sha': 'ce1fdd9354bdc315e49a40dc9da3ab03bf6af7b3',
                                            'description': MR404_JIRA},
                                           {'sha': 'f77278fcd9cef99358adc7f5e077be795a54ffca',
                                            'description': MR404_JIRA}]
                                 }}}

MR404_DICT = {'id': 'gid://gitlab/Project/24152864',
              'mr': {'title': 'Does not actually matter',
                     'description': MR404_JIRA,
                     'id': 'gid://gitlab/MergeRequest/262327',
                     'state': 'opened',
                     'draft': False,
                     'sourceBranch': 'feature',
                     'targetBranch': 'main',
                     'headPipeline': {'finishedAt': '2022-06-14T15:09:22Z'},
                     'files': [{'path': 'lib/include/what.h'}],
                     'labels': {'nodes': []},
                     'commits': {'pageInfo': {'hasNextPage': False, 'endCursor': 'Mw'},
                                 'nodes': [{'sha': 'ce1fdd9354bdc315e49a40dc9da3ab03bf6af7b3',
                                            'description': MR404_JIRA},
                                           {'sha': 'f77278fcd9cef99358adc7f5e077be795a54ffca',
                                            'description': MR404_JIRA}]
                                 }}}
