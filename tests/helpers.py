"""Helper bits for kernel-workflow unittests."""
import argparse
from importlib import resources
import os
import typing
from unittest import TestCase
from unittest import mock

import freezegun
import jira
import responses

from tests import fake_payloads
from webhook import session
from webhook import temp_utils
from webhook.common import get_arg_parser


class KwfTestCase(TestCase):
    """TestCase with socket.socket disabled."""

    def setUp(self):
        """Set up each test with socket.socket patched."""
        super().setUp()
        # enable yaml load caching
        temp_utils.set_yaml_load_caching(True)
        # disable validation during tests
        temp_utils.set_yaml_validation(False)
        # Set up some defs.
        self.GITLAB_API = 'https://gitlab.com/api/v4'
        self.GITLAB_GRAPHQL = fake_payloads.GITLAB_GRAPHQL
        self.JIRA_URL = 'https://issues.redhat.com'
        self.JIRA_USERNAME = 'example_user'
        self.JIRA_EMAIL = 'example_user@example.com'
        # Patch socket.socket.
        patched_socket = mock.patch('socket.socket', mock.Mock())
        patched_socket.start()
        self.addCleanup(patched_socket.stop)
        # Set up responses.
        self.responses = responses.RequestsMock()
        self.responses.start()
        self.addCleanup(self.responses.stop)
        self.addCleanup(self.responses.reset)
        # Clear the webhook.session caches.
        self.clear_caches()

    @staticmethod
    def clear_caches() -> None:
        """Clear the webhook.session caches."""
        session.get_gl_group.cache_clear()
        session.get_gl_instance.cache_clear()
        session.get_gl_project.cache_clear()
        session.get_graphql.cache_clear()

    @staticmethod
    def load_yaml_asset(
        path: str,
        module: str = 'tests.assets',
        sub_module: typing.Optional[str] = None
    ) -> dict | list:
        """Return the yaml (or json) test asset contents from the given module.sub_module."""
        if sub_module:
            module += f'.{sub_module}'
        return temp_utils.load_one_yaml(resources.files(module).joinpath(path))

    @classmethod
    def make_jira_issue(cls, key: str) -> jira.resources.Issue:
        """Return a jira.Issue loaded with the test asset json matching the given key."""
        raw_issue = cls.load_yaml_asset(f'{key}.json', sub_module='jira_rest_api')
        return jira.resources.Issue(options={}, session={}, raw=raw_issue)

    @staticmethod
    def base_session() -> session.BaseSession:
        """Return a BaseSession object."""
        return session.BaseSession.new()

    @staticmethod
    def session_runner(
        webhook_name: str,
        args: argparse.ArgumentParser | None = None,
        handlers: dict | None = None
    ) -> session.SessionRunner:
        """Return a SessionRunner object."""
        args = args or get_arg_parser(webhook_name.upper())
        handlers = handlers or {}
        return session.SessionRunner.new(webhook_name=webhook_name, args=args, handlers=handlers)

    def response_gql_user_data(self, **kwargs) -> responses.Response:
        """Return a responses.Response for graphql.GitlabGraph.user."""
        if 'rsps' not in kwargs:
            kwargs['rsps'] = self.responses
        return fake_payloads.mock_gql_user_data(**kwargs)

    def response_gl_auth(
        self,
        username: str = fake_payloads.USER_NAME,
        id: int = fake_payloads.USER_ID,
        body: typing.Optional[typing.Any] = None
    ) -> responses.Response:
        """Return a responses.Response for gitlab.auth()."""
        if body:
            return self.responses.get(f'{self.GITLAB_API}/user', body=body)
        return self.responses.get(f'{self.GITLAB_API}/user', json={'id': id, 'username': username})

    def response_jira_serverInfo(self, base_url: str = '') -> responses.Response:
        """Return a responses.Response for jira API serverInfo."""
        if not base_url:
            base_url = self.JIRA_URL

        server_info = self.load_yaml_asset('serverInfo.json', sub_module='jira_rest_api')
        server_info['baseURL'] = base_url

        return self.responses.get(f'{base_url}/rest/api/2/serverInfo', json=server_info)

    def response_jira_myself(
        self,
        base_url: str = '',
        username: str = '',
        email: str = ''
    ) -> responses.Response:
        """Return a responses.Response for jira API myself."""
        base_url = base_url or self.JIRA_URL
        username = username or self.JIRA_USERNAME
        email = email or self.JIRA_EMAIL

        myself_json = {
            'self': f'{base_url}/rest/api/2/user?username={username}',
            'key': username,
            'name': username,
            'emailAddress': email,
            'displayName': username.replace('_', ' ').capitalize(),
            'active': True,
            'deleted': False
        }

        return self.responses.get(f'{base_url}/rest/api/2/myself', json=myself_json)

    def response_jira_field(self, base_url: str = '') -> responses.Response:
        """Return a responses.Response for jira API field."""
        field_json = self.load_yaml_asset('field.json', sub_module='jira_rest_api')

        return self.responses.get(f'{base_url}/rest/api/2/field', json=field_json)

    def response_jira_session(
        self,
        base_url: str = '',
        username: str = '',
    ) -> responses.Response:
        """Return a responses.Response for jira API v1 session."""
        base_url = base_url or self.JIRA_URL
        username = username or self.JIRA_USERNAME

        session_json = {
            'self': f'{base_url}/rest/api/latest/user?username={username}',
            'name': username,
            'loginInfo': {'loginCount': 999, 'previousLoginTime': '2024-10-23T09:09:09.000+0000'}
        }

        return self.responses.get(f'{base_url}/rest/auth/1/session', json=session_json)

    def response_bugzilla_setup(self) -> None:
        """Set up version & user responses used when a new bugzilla.Bugzilla session is inited."""
        bz_version_json = self.load_yaml_asset('version.json', sub_module='bugzilla_rest_api')
        bz_user_json = self.load_yaml_asset('user.json', sub_module='bugzilla_rest_api')

        self.responses.get('https://bugzilla.redhat.com/rest/version', json=bz_version_json)
        self.responses.get('https://bugzilla.redhat.com/rest/user?ids=1', json=bz_user_json)

    def response_jira_setup(self, base_url: str = '', username: str = '', email: str = '') -> None:
        """Set up serverInfo, myself, and field responses and return them as a list."""
        base_url = base_url or self.JIRA_URL
        username = username or self.JIRA_USERNAME
        email = email or self.JIRA_EMAIL

        self.response_jira_serverInfo(base_url)
        self.response_jira_session(base_url, username)
        self.response_jira_myself(base_url, username, email)
        self.response_jira_field(base_url)

    def response_et_setup(self) -> None:
        """Set up errata tool advisory query."""
        test_issues = ['RHEL-101', 'RHEL-30580', 'RHEL-50200', 'RHEL-50264']
        os.environ['CKI_ERRATA_KWF_BOT_KEYTAB'] = '/path/to/your/keytab'
        for issue in test_issues:
            url = f"https://errata.devel.redhat.com/jira_issues/{issue}/advisories.json"
            advisories = self.load_yaml_asset(f"{issue}_advisories.json",
                                              sub_module='errata_tool_rest_api')
            self.responses.get(url, json=advisories)

    def freeze_time(
        self,
        time_str: str,
        start: bool = True,
        cleanup: bool = True
    ) -> freezegun.api._freeze_time:
        """Create a freeze time object and start it."""
        freezer = freezegun.freeze_time(time_str)
        if cleanup:
            self.addCleanup(freezer.stop)
        if start:
            freezer.start()
        return freezer

    def add_query(
        self,
        query: str,
        variables: typing.Union[dict, None],
        query_result: dict,
        url: str = fake_payloads.GITLAB_GRAPHQL,
        strict_match: bool = False
    ) -> responses.Response:
        """Add a graphql query response."""
        post_data = {'query': query.strip('\n')}
        if variables is not None:
            post_data['variables'] = variables

        return self.responses.post(
            url=url,
            match=[responses.matchers.json_params_matcher(post_data, strict_match=strict_match)],
            json=query_result
        )
