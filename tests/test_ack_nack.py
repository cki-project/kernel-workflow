"""Webhook interaction tests."""
from dataclasses import dataclass
from dataclasses import field
from unittest import mock

from tests import fakes
from tests.helpers import KwfTestCase
from webhook import ack_nack
from webhook import defs
from webhook.defs import GitlabURL
from webhook.defs import MrScope
from webhook.graphql import FIND_MEMBER_QUERY
from webhook.graphql import MR_MERGE_PERMISSION_QUERY
from webhook.session import SessionRunner
from webhook.users import User


def create_approve_mr(mr_url, blocking_event=None, commits_changed=False) -> ack_nack.ApproveMR:
    """Return a fresh MR object."""
    test_session = SessionRunner.new('ack_nack', args='')
    gl_instance = fakes.FakeGitLab()
    gl_instance.user = User(username='cki-kwf-bot')
    test_session.get_gl_instance = mock.Mock(return_value=gl_instance)

    return ack_nack.ApproveMR.new(
        test_session,
        mr_url,
        source_path='/src',
        blocking_event=blocking_event,
        commits_changed=commits_changed
    )


@dataclass(repr=False)
class JsonApproveMR:
    """Simple collection of file names of json data and users to build ApproveMR from."""

    mr_url: GitlabURL
    details: str
    commits: str
    rules: str = ''
    author: str = ''
    members: list = field(default_factory=list)
    add_rest: bool = False
    discussions: bool = False


@mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml',
                                'OWNERS_YAML': 'tests/assets/owners-snapshot.yaml'})
@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestAckNack(KwfTestCase):

    def populate_responses(self, json_approve_mr) -> str:
        # Make sure we're starting with an empty set of responses
        self.responses.reset()

        # Standard user authentication
        self.response_gl_auth()
        # Standard GraphQL user data response
        self.response_gql_user_data()

        mr_endpoint = ''
        # Set up the ApproveMR query responses.
        variables = {'namespace': json_approve_mr.mr_url.namespace,
                     'mr_id': str(json_approve_mr.mr_url.id)}

        details = self.load_yaml_asset(
            path=json_approve_mr.details,
            sub_module='gitlab_graphql_api'
        )
        self.add_query(ack_nack.ApproveMR.MR_QUERY, variables=variables, query_result=details)

        commits = self.load_yaml_asset(
            path=json_approve_mr.commits,
            sub_module='gitlab_graphql_api'
        )
        self.add_query(ack_nack.ApproveMR.COMMIT_QUERY, variables=variables, query_result=commits)

        if json_approve_mr.rules:
            rules = self.load_yaml_asset(
                path=json_approve_mr.rules,
                sub_module='gitlab_graphql_api'
            )
            self.add_query(ack_nack.ApproveMR.APPROVALS_QUERY,
                           variables=variables, query_result=rules)

        if json_approve_mr.members:
            find_member = self.load_yaml_asset(
                path='find_member.json',
                sub_module='gitlab_graphql_api'
            )
            for member in json_approve_mr.members:
                member_vars = {'namespace': json_approve_mr.mr_url.namespace,
                               'search_key': member}
                self.add_query(FIND_MEMBER_QUERY, variables=member_vars, query_result=find_member)

        if json_approve_mr.author:
            author_perms = self.load_yaml_asset(
                path='author-perms.json',
                sub_module='gitlab_graphql_api'
            )
            author_vars = {'namespace': json_approve_mr.mr_url.namespace,
                           'search_username': json_approve_mr.author}
            self.add_query(MR_MERGE_PERMISSION_QUERY,
                           variables=author_vars, query_result=author_perms)

        if json_approve_mr.add_rest:
            proj_name = json_approve_mr.mr_url.namespace.split('/')[-1]
            project = self.load_yaml_asset(
                path=f'project-{proj_name}.json',
                sub_module='gitlab_rest_api'
            )

            proj_id = details['data']['project']['mr']['project']['id'].split('/')[-1]
            proj_endpoint = "https://gitlab.com/api/v4/projects"
            proj_path = json_approve_mr.mr_url.namespace.replace("/", "%2F")

            self.responses.get(f'{proj_endpoint}/{proj_id}', json=project)
            self.responses.get(f'{proj_endpoint}/{proj_path}', json=project)

            merge_request = self.load_yaml_asset(
                path=f'mr-{json_approve_mr.mr_url.id}-{proj_name}.json',
                sub_module='gitlab_rest_api'
            )
            mr_endpoint = f'{proj_endpoint}/{proj_id}/merge_requests/{json_approve_mr.mr_url.id}'
            self.responses.get(mr_endpoint, json=merge_request)

            mr_versions = self.load_yaml_asset(
                path=f'mr-{json_approve_mr.mr_url.id}-{proj_name}-versions.json',
                sub_module='gitlab_rest_api'
            )
            vers_endpoint = f'{mr_endpoint}/versions?per_page=100'
            self.responses.get(vers_endpoint, json=mr_versions)

            if json_approve_mr.discussions:
                discussions_endpoint = f'{mr_endpoint}/discussions?per_page=100'
                discussions = self.load_yaml_asset(
                    path=f'mr-{json_approve_mr.mr_url.id}-{proj_name}-discussions.json',
                    sub_module='gitlab_rest_api'
                )
                self.responses.get(discussions_endpoint, json=discussions)

        return mr_endpoint

    def test_approvemr_load(self):
        mr_url = GitlabURL('https://gitlab.com/redhat/rhel/src/kernel/'
                           'rhel-8-sandbox/-/merge_requests/602')
        proj_name = mr_url.namespace.split('/')[-1]

        # Set up the ApproveMR query responses.
        details = f'approvemr_{proj_name}_{mr_url.id}-details.json'
        commits = f'approvemr_{proj_name}_{mr_url.id}-commits.json'
        rules = f'approvemr_{proj_name}_{mr_url.id}-approvalrules.json'
        json_approve_mr = JsonApproveMR(mr_url=mr_url, details=details,
                                        commits=commits, rules=rules)
        self.populate_responses(json_approve_mr)

        amr = create_approve_mr(mr_url)

        self.assertIn('All Members', amr.project_approval_rules)
        self.assertIn('ethernet', amr.approval_rules)
        self.assertIn('jwilsonrh', [user.username for user in amr.approved_by])
        self.assertEqual('All Members', amr.expected_all_members_rule.name)
        self.assertEqual(3, amr.expected_all_members_rule.approvals_required)
        self.assertEqual('Bot: readyForMerge', amr.expected_bot_rfm_rule.name)
        self.assertEqual(1, amr.expected_bot_rfm_rule.approvals_required)

    @mock.patch('webhook.ack_nack.ApproveMR.project_approval_resets_enabled',
                new_callable=mock.PropertyMock)
    @mock.patch('webhook.ack_nack.ApproveMR.do_approvals_reset')
    @mock.patch('webhook.ack_nack.ApproveMR.do_comments', mock.Mock())
    @mock.patch('webhook.ack_nack.ApproveMR.do_label_updates', mock.Mock())
    @mock.patch('webhook.ack_nack.ApproveMR.do_update_mr_rules', mock.Mock())
    @mock.patch('webhook.ack_nack.ApproveMR.do_reset_preliminary_testing', mock.Mock())
    @mock.patch('webhook.ack_nack.ApproveMR.code_changes', mock.Mock(return_value=True))
    def test_reset_approvals(self, mock_do_reset, mock_pare):
        mock_pare.return_value = True
        mr_url = GitlabURL('https://gitlab.com/redhat/rhel/src/kernel/'
                           'rhel-8-sandbox/-/merge_requests/602')
        proj_name = mr_url.namespace.split('/')[-1]

        # Set up the ApproveMR query responses.
        details = f'approvemr_{proj_name}_{mr_url.id}-details.json'
        commits = f'approvemr_{proj_name}_{mr_url.id}-commits.json'

        json_approve_mr = JsonApproveMR(mr_url=mr_url, details=details, commits=commits)
        self.populate_responses(json_approve_mr)

        amr = create_approve_mr(mr_url)
        self.assertFalse(amr.reset_approvals)
        amr.do_all_updates()
        mock_do_reset.assert_not_called()

        amr = create_approve_mr(mr_url, commits_changed=True)
        self.assertTrue(amr.reset_approvals)
        amr.do_all_updates()
        mock_do_reset.assert_not_called()

        mock_pare.return_value = False
        amr = create_approve_mr(mr_url, commits_changed=True)
        self.assertTrue(amr.reset_approvals)
        amr.do_all_updates()
        mock_do_reset.assert_called_once()

    @mock.patch('webhook.ack_nack.ApproveMR.draft', new_callable=mock.PropertyMock)
    @mock.patch('webhook.approval_rules.ProjectApprovalRule.update')
    @mock.patch('webhook.approval_rules.ProjectApprovalRule.create', mock.Mock())
    @mock.patch('cki_lib.misc.is_staging', mock.Mock(return_value=True))
    def test_expected_project_rules(self, mock_update_rule, mock_draft_status):
        mr_url = GitlabURL('https://gitlab.com/redhat/rhel/src/kernel/'
                           'rhel-8-sandbox/-/merge_requests/602')
        proj_name = mr_url.namespace.split('/')[-1]

        # Set up the ApproveMR query responses.
        details = f'approvemr_{proj_name}_{mr_url.id}-details.json'
        commits = f'approvemr_{proj_name}_{mr_url.id}-commits.json'
        rules = f'approvemr_{proj_name}_{mr_url.id}-approvalrules2.json'

        json_approve_mr = JsonApproveMR(mr_url=mr_url, details=details, commits=commits,
                                        rules=rules, author='ptalbert')
        self.populate_responses(json_approve_mr)

        amr = create_approve_mr(mr_url)
        amr.labels = ["ZStreamBuild"]
        mock_draft_status.return_value = True
        self.assertIn('All Members', amr.expected_project_rules.keys())
        self.assertEqual('All Members', amr.expected_all_members_rule.name)
        self.assertEqual(2, amr.expected_all_members_rule.approvals_required)
        self.assertIn('Bot: readyForMerge', amr.expected_project_rules.keys())
        self.assertEqual('Bot: readyForMerge', amr.expected_bot_rfm_rule.name)
        self.assertEqual(1, amr.expected_bot_rfm_rule.approvals_required)

        # Now with different approval rules to tickle another path in expected_project_rules
        rules = f'approvemr_{proj_name}_{mr_url.id}-approvalrules3.json'

        json_approve_mr = JsonApproveMR(mr_url=mr_url, details=details, commits=commits,
                                        rules=rules, author='ptalbert', add_rest=True)
        self.populate_responses(json_approve_mr)

        amr = create_approve_mr(mr_url)
        mock_draft_status.return_value = False
        self.assertIn('All Members', amr.expected_project_rules.keys())
        self.assertEqual('All Members', amr.expected_all_members_rule.name)
        self.assertEqual(3, amr.expected_all_members_rule.approvals_required)
        self.assertIn('Bot: readyForMerge', amr.expected_project_rules.keys())
        self.assertEqual('Bot: readyForMerge', amr.expected_bot_rfm_rule.name)
        self.assertEqual(1, amr.expected_bot_rfm_rule.approvals_required)
        self.assertIn('ethernet', [r.name for r in amr.rules_to_update])
        amr.do_update_mr_rules()
        mock_update_rule.assert_called_with(amr.session.graphql, amr.url,
                                            amr.expected_sst_rules['ethernet'].approvals_required,
                                            amr.expected_sst_rules['ethernet'].eligible_approvers)

    @mock.patch.dict('os.environ', {'OWNERS_YAML': 'tests/assets/owners-testing.yaml'})
    def test_approval_rules1(self):
        mr_url = GitlabURL('https://gitlab.com/redhat/rhel/src/kernel/'
                           'rhel-9-sandbox/-/merge_requests/19')
        proj_name = mr_url.namespace.split('/')[-1]

        query_result = f'approvemr_{proj_name}_{mr_url.id}.json'

        json_approve_mr = JsonApproveMR(mr_url=mr_url, details=query_result, commits=query_result,
                                        rules=query_result, members=['user1', 'user2'])
        self.populate_responses(json_approve_mr)

        amr = create_approve_mr(mr_url)

        self.assertEqual(1, len(amr.owners_subsystems))
        self.assertIn('All Members', amr.approval_rules.keys())
        self.assertIn('Some Subsystem', [ss.subsystem_name for ss in amr.owners_subsystems])
        self.assertIn('Some Subsystem', amr.current_sst_rules.keys())
        self.assertIn('Some Subsystem', amr.expected_sst_rules.keys())
        self.assertEqual(['Acks::Some Subsystem::NeedsReview'], amr.expected_sst_labels)

    @mock.patch('webhook.ack_nack._emails_to_gl_user_names')
    def test_approval_rules2(self, mock_cc_user):
        mr_url = GitlabURL('https://gitlab.com/redhat/rhel/src/kernel/'
                           'rhel-8-sandbox/-/merge_requests/602')
        proj_name = mr_url.namespace.split('/')[-1]

        # Set up the ApproveMR query responses.
        details = f'approvemr_{proj_name}_{mr_url.id}-details.json'
        commits = f'approvemr_{proj_name}_{mr_url.id}-commits.json'
        rules = f'approvemr_{proj_name}_{mr_url.id}-approvalrules.json'

        json_approve_mr = JsonApproveMR(mr_url=mr_url, details=details,
                                        commits=commits, rules=rules,
                                        add_rest=True, discussions=True)
        mr_endpoint = self.populate_responses(json_approve_mr)

        version = '211187326'
        ver_endpoint = f'{mr_endpoint}/versions/{version}'
        mr_version = self.load_yaml_asset(
            path=f'mr-{mr_url.id}-{proj_name}-version-{version}.json',
            sub_module='gitlab_rest_api'
        )
        self.responses.get(ver_endpoint, json=mr_version)

        version = '200890914'
        ver_endpoint = f'{mr_endpoint}/versions/{version}'
        mr_version = self.load_yaml_asset(
            path=f'mr-{mr_url.id}-{proj_name}-version-{version}.json',
            sub_module='gitlab_rest_api'
        )
        self.responses.get(ver_endpoint, json=mr_version)

        amr = create_approve_mr(mr_url)

        amr._labels = ["Acks::mm::OK", "Acks::ethernet::OK"]

        self.assertIn('ethernet', amr.expected_sst_rules.keys())
        self.assertIn('ethernet', amr.current_sst_rules.keys())
        self.assertIn('i40e', amr.expected_sst_rules.keys())
        self.assertIn('i40e', amr.current_sst_rules.keys())
        self.assertEqual(amr.expected_bot_rfm_rule.approvals_required, 1)
        self.assertEqual(amr.expected_bot_rfm_rule.name, defs.BOT_APPROVAL_RULE)
        self.assertFalse(amr.expected_bot_rfm_rule.approved)
        self.assertEqual(['Acks::ethernet::OK'], amr.expected_sst_labels)
        self.assertCountEqual(['Acks::OK', 'Acks::ethernet::OK'], amr.expected_labels)
        self.assertEqual(0, amr.remaining_approvals)
        summary = amr.status_report()
        self.assertIn('Approval Rule "ethernet" already has 2 ACK(s) (1 required).', summary)
        self.assertIn('Approval Rule "i40e" already has 1 ACK(s) (0 required).', summary)
        self.assertIn('Approval Rule "nic_rdma" already has 1 ACK(s) (0 required).', summary)
        self.assertIn('Approval Rule "my special rule" requests optional ACK(s) from '
                      'set (shadowman).', summary)
        self.assertIn('jwilsonrh', f"{[a.username for a in amr.approved_by]}")
        self.assertIn('ivecera', f"{[a.username for a in amr.approved_by]}")
        self.assertEqual(len(amr.approved_by), 2)
        self.assertEqual(len(amr.all_expected_rules), 6)
        self.assertNotIn("Acks::mm::OK", amr.expected_sst_labels)
        mock_cc_user.return_value = {'shadowman'}, ['unknown@redhat.com']
        cc_usernames, missing = amr.cc_reviewers
        self.assertIn('shadowman', cc_usernames)
        self.assertIn('unknown@redhat.com', missing)
        self.assertFalse(amr.reset_approvals)
        self.assertIsNone(amr.code_changed_label)
        self.assertIsNone(amr.new_blocked_by_rule)
        expected_reviewers = ['shadowman']
        self.assertCountEqual(expected_reviewers, amr.expected_reviewers)

        with self.assertLogs('cki.webhook.session', level='INFO') as logs:
            amr.do_comments()
            self.assertIn('**ACK/NACK Summary**: ~"Acks::OK"\n\nApproved by:', logs.output[-1])

    @mock.patch('webhook.ack_nack.ApproveMR.rules_to_update', new_callable=mock.PropertyMock)
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_approval_rules3(self, mock_to_update):
        mock_to_update.return_value = []
        # A draft MR with some stale rules to remove
        mr_url = GitlabURL('https://gitlab.com/redhat/rhel/src/kernel/'
                           'rhel-8-sandbox/-/merge_requests/777')
        proj_name = mr_url.namespace.split('/')[-1]

        # Set up the ApproveMR query responses.
        details = f'approvemr_{proj_name}_{mr_url.id}-details.json'
        commits = f'approvemr_{proj_name}_{mr_url.id}-commits.json'
        rules = f'approvemr_{proj_name}_{mr_url.id}-approvalrules.json'
        json_approve_mr = JsonApproveMR(mr_url=mr_url, details=details, commits=commits,
                                        rules=rules, author='jwilsonrh',
                                        members=['btissoir', 'jwilsonrh'],
                                        add_rest=True, discussions=True)
        mr_endpoint = self.populate_responses(json_approve_mr)

        version = '701181718'
        ver_endpoint = f'{mr_endpoint}/versions/{version}'
        mr_version = self.load_yaml_asset(
            path=f'mr-{mr_url.id}-{proj_name}-version-{version}.json',
            sub_module='gitlab_rest_api'
        )
        self.responses.get(ver_endpoint, json=mr_version)

        rule_endpoint = f'{mr_endpoint}/approval_rules/167253169'
        rule = self.load_yaml_asset(
            path=f'mr-{mr_url.id}-{proj_name}-rule-mm.json',
            sub_module='gitlab_rest_api'
        )
        self.responses.delete(rule_endpoint, json=rule)

        amr = create_approve_mr(mr_url)

        self.assertTrue(amr.draft)
        self.assertEqual(len(amr.all_expected_rules), 3)
        self.assertIn('mm', amr.approval_rules.keys())
        self.assertNotIn('hid', amr.approval_rules.keys())
        self.assertNotIn('hid', amr.expected_sst_rules.keys())
        self.assertIn('hid', amr._expected_sst_rules.keys())
        self.assertIn('mm', [r.name for r in amr.rules_to_remove])
        amr.do_update_mr_rules()
        self.assertEqual([], amr.expected_labels)
        summary = amr.status_report()
        self.assertIn('Expected Approvals (rules that will be created once the MR is out of '
                      'draft):  \n - Approval Rule "hid" requests optional ACK(s) from set '
                      '(btissoir).\n\nRequires 2 more Approval(s).', summary)

    @mock.patch('webhook.ack_nack.ApproveMR.do_reset_preliminary_testing', mock.Mock())
    @mock.patch('webhook.cdlib.assemble_interdiff_markdown')
    @mock.patch('webhook.ack_nack.ApproveMR.diffs', new_callable=mock.PropertyMock)
    @mock.patch('webhook.ack_nack.ApproveMR.code_changes')
    @mock.patch('webhook.ack_nack.ApproveMR.history', new_callable=mock.PropertyMock)
    @mock.patch('webhook.ack_nack.ApproveMR.project_approval_resets_enabled',
                new_callable=mock.PropertyMock)
    def test_approval_reset(self, mock_pare, mock_history, mock_changes, mock_diffs, mock_idiff):
        mock_pare.return_value = False
        mock_history.return_value = ['one', 'two', 'three', 'four']
        mock_changes.return_value = True
        mock_diffs.return_value = ['one', 'two', 'three', 'four']
        mock_idiff.return_value = '++This is a fake interdiff'

        mr_url = GitlabURL('https://gitlab.com/redhat/rhel/src/kernel/'
                           'rhel-8-sandbox/-/merge_requests/777')
        proj_name = mr_url.namespace.split('/')[-1]

        # Set up the ApproveMR query responses.
        details = f'approvemr_{proj_name}_{mr_url.id}-details.json'
        commits = f'approvemr_{proj_name}_{mr_url.id}-commits.json'
        rules = f'approvemr_{proj_name}_{mr_url.id}-approvalrules.json'
        json_approve_mr = JsonApproveMR(mr_url=mr_url, details=details, commits=commits,
                                        rules=rules, author='jwilsonrh', members=['btissoir'],
                                        add_rest=True, discussions=True)
        mr_endpoint = self.populate_responses(json_approve_mr)

        version = '701181718'
        ver_endpoint = f'{mr_endpoint}/versions/{version}'
        mr_version = self.load_yaml_asset(
            path=f'mr-{mr_url.id}-{proj_name}-version-{version}.json',
            sub_module='gitlab_rest_api'
        )
        self.responses.get(ver_endpoint, json=mr_version)

        amr = create_approve_mr(mr_url, commits_changed=True)

        self.assertEqual('CodeChanged::v4', amr.code_changed_label)
        self.assertCountEqual(['CodeChanged::v4'], amr.expected_labels)
        report = amr.approval_reset_report()
        self.assertIn("Code changes in revision v4 of this MR:  \n++This is a fake interdiff",
                      report)
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            amr.do_approvals_reset()
            self.assertIn("Clearing Approvals on redhat/rhel/src/kernel/rhel-8-sandbox "
                          "MR 777 (Rev v4)", logs.output[-1])

        with self.assertLogs('cki.webhook.session', level='INFO') as logs:
            amr.do_comments()
            self.assertIn('**ACK/NACK Summary**: **draft report**', logs.output[-1])

        mock_changes.return_value = False
        amr = create_approve_mr(mr_url)

        self.assertIsNone(amr.code_changed_label)
        self.assertEqual('', amr.approval_reset_report())

    @mock.patch('webhook.ack_nack.ApproveMR.manage_jiras', new_callable=mock.PropertyMock)
    @mock.patch('webhook.session.connect_jira')
    @mock.patch('webhook.ack_nack.reset_preliminary_testing')
    @mock.patch('webhook.ack_nack.fetch_issues')
    def test_do_reset_preliminary_testing(self, mock_fetch, mock_reset_pt, mock_jcon, manage_jiras):
        issue_list = [self.make_jira_issue('RHEL-101')]
        mock_fetch.return_value = issue_list
        mock_jira = mock.Mock()
        mock_jcon.return_value = mock_jira
        manage_jiras.return_value = False

        mr_url = GitlabURL('https://gitlab.com/redhat/rhel/src/kernel/'
                           'rhel-8-sandbox/-/merge_requests/777')
        proj_name = mr_url.namespace.split('/')[-1]

        details = f'approvemr_{proj_name}_{mr_url.id}-details-no-sst.json'
        commits = f'approvemr_{proj_name}_{mr_url.id}-commits.json'
        rules = f'approvemr_{proj_name}_{mr_url.id}-approvalrules.json'
        json_approve_mr = JsonApproveMR(mr_url=mr_url, details=details,
                                        commits=commits, rules=rules)
        self.populate_responses(json_approve_mr)
        amr = create_approve_mr(mr_url)

        amr.do_reset_preliminary_testing()
        mock_fetch.assert_not_called()
        mock_reset_pt.assert_not_called()

        details = f'approvemr_{proj_name}_{mr_url.id}-details.json'
        commits = f'approvemr_{proj_name}_{mr_url.id}-commits.json'
        rules = f'approvemr_{proj_name}_{mr_url.id}-approvalrules.json'
        json_approve_mr = JsonApproveMR(mr_url=mr_url, details=details,
                                        commits=commits, rules=rules)
        self.populate_responses(json_approve_mr)
        amr = create_approve_mr(mr_url)

        amr.do_reset_preliminary_testing()
        mock_fetch.assert_not_called()
        mock_reset_pt.assert_not_called()

        manage_jiras.return_value = True
        amr.do_reset_preliminary_testing()
        mock_reset_pt.assert_called_with(mock_jira, issue_list)

    @mock.patch('webhook.ack_nack.ApproveMR.status_report', mock.Mock(return_value='foo'))
    @mock.patch('webhook.ack_nack.ApproveMR.approval_reset_report', mock.Mock(return_value=''))
    def test_blocked_by_rules(self):
        # A draft MR with some stale rules to remove
        mr_url = GitlabURL('https://gitlab.com/redhat/rhel/src/kernel/'
                           'rhel-8-sandbox/-/merge_requests/777')
        proj_name = mr_url.namespace.split('/')[-1]

        # Set up the ApproveMR query responses.
        details = f'approvemr_{proj_name}_{mr_url.id}-details.json'
        commits = f'approvemr_{proj_name}_{mr_url.id}-commits.json'
        rules = f'approvemr_{proj_name}_{mr_url.id}-approvalrules-blocked-by.json'
        json_approve_mr = JsonApproveMR(mr_url=mr_url, details=details, commits=commits,
                                        rules=rules, author='jwilsonrh', members=['shadowman'],
                                        add_rest=True, discussions=True)
        self.populate_responses(json_approve_mr)

        # No new blocking event
        amr = create_approve_mr(mr_url)
        amr.blocking_event = None
        self.assertIsNone(amr.new_blocked_by_rule)
        self.assertIn('Blocked-by: lzampier', amr.expected_blocked_by_rules.keys())

        # User who already blocked is trying to block again
        amr = create_approve_mr(mr_url)
        amr.blocking_event = ack_nack.BlockingEvent('lzampier', ack_nack.BLOCK_KEYWORD)
        self.assertIsNone(amr.new_blocked_by_rule)

        # Author of the MR is trying to block
        amr = create_approve_mr(mr_url)
        amr.blocking_event = ack_nack.BlockingEvent('jwilsonrh', ack_nack.BLOCK_KEYWORD)
        self.assertIsNone(amr.new_blocked_by_rule)

        # An actual new blocking event
        amr = create_approve_mr(mr_url)
        amr.blocking_event = ack_nack.BlockingEvent('shadowman', ack_nack.BLOCK_KEYWORD)
        self.assertIsNotNone(amr.new_blocked_by_rule)
        self.assertIn('Blocked-by: lzampier', amr.expected_blocked_by_rules.keys())
        self.assertIn('Blocked-by: shadowman', amr.expected_blocked_by_rules.keys())
        self.assertEqual(2, len(amr.expected_blocked_by_rules))
        self.assertEqual('Blocked-by: shadowman', amr.blocking_event.rule_name)
        self.assertEqual('@shadowman has blocked this Merge Request via a `/block` action.',
                         amr.blocking_event.comment_text)

        with self.assertLogs('cki.webhook.session', level='INFO') as logs:
            amr.do_comments()
            self.assertIn('**ACK/NACK Summary**: **draft report**', ''.join(logs.output))

        # An unblock event for a user we do have a rule for
        amr = create_approve_mr(mr_url)
        amr.blocking_event = ack_nack.BlockingEvent('lzampier', ack_nack.UNBLOCK_KEYWORD)
        self.assertIsNone(amr.new_blocked_by_rule)
        self.assertEqual({}, amr.expected_blocked_by_rules)
        self.assertEqual('', amr.blocking_event.comment_text)
        self.assertIn('Blocked-by: lzampier', amr.current_blocked_by_rules.keys())
        self.assertNotIn('Blocked-by: lzampier', amr.all_expected_rules.keys())

        # An unblock event for a user we do NOT have a rule for
        amr = create_approve_mr(mr_url)
        amr.blocking_event = ack_nack.BlockingEvent('shadowman', ack_nack.UNBLOCK_KEYWORD)
        self.assertIsNone(amr.new_blocked_by_rule)
        self.assertIn('Blocked-by: lzampier', amr.expected_blocked_by_rules.keys())
        self.assertEqual(1, len(amr.expected_blocked_by_rules))

    def test_overall_approvals_scope(self):
        mr_url = GitlabURL('https://gitlab.com/redhat/rhel/src/kernel/'
                           'rhel-8-sandbox/-/merge_requests/777')
        proj_name = mr_url.namespace.split('/')[-1]

        # Set up the ApproveMR query responses.
        details = f'approvemr_{proj_name}_{mr_url.id}-details.json'
        commits = f'approvemr_{proj_name}_{mr_url.id}-commits.json'
        rules = f'approvemr_{proj_name}_{mr_url.id}-approvalrules-blocked-by.json'

        json_approve_mr = JsonApproveMR(mr_url=mr_url, details=details, commits=commits,
                                        rules=rules, author='jwilsonrh', add_rest=True)
        mr_endpoint = self.populate_responses(json_approve_mr)

        version = '701181718'
        ver_endpoint = f'{mr_endpoint}/versions/{version}'
        mr_version = self.load_yaml_asset(
            path=f'mr-{mr_url.id}-{proj_name}-version-{version}.json',
            sub_module='gitlab_rest_api'
        )
        self.responses.get(ver_endpoint, json=mr_version)

        rule_endpoint = f'{mr_endpoint}/approval_rules/167253169'
        rule = self.load_yaml_asset(
            path=f'mr-{mr_url.id}-{proj_name}-rule-mm.json',
            sub_module='gitlab_rest_api'
        )
        self.responses.delete(rule_endpoint, json=rule)

        # There's a blocked-by rule that isn't satisfied
        amr = create_approve_mr(mr_url)
        self.assertEqual(MrScope.BLOCKED, amr.overall_approvals_scope)

        rules = f'approvemr_{proj_name}_{mr_url.id}-approvalrules.json'
        json_approve_mr = JsonApproveMR(mr_url=mr_url, details=details, commits=commits,
                                        rules=rules, add_rest=True, author='jwilsonrh',
                                        members=['btissoir', 'jwilsonrh'])
        mr_endpoint = self.populate_responses(json_approve_mr)

        version = '701181718'
        ver_endpoint = f'{mr_endpoint}/versions/{version}'
        mr_version = self.load_yaml_asset(
            path=f'mr-{mr_url.id}-{proj_name}-version-{version}.json',
            sub_module='gitlab_rest_api'
        )
        self.responses.get(ver_endpoint, json=mr_version)

        rule_endpoint = f'{mr_endpoint}/approval_rules/167253169'
        rule = self.load_yaml_asset(
            path=f'mr-{mr_url.id}-{proj_name}-rule-mm.json',
            sub_module='gitlab_rest_api'
        )
        self.responses.delete(rule_endpoint, json=rule)

        # The MR Needs Review
        amr = create_approve_mr(mr_url)
        self.assertEqual(MrScope.NEEDS_REVIEW, amr.overall_approvals_scope)

        details = f'approvemr_{proj_name}_{mr_url.id}-details-no-sst.json'
        json_approve_mr = JsonApproveMR(mr_url=mr_url, details=details, commits=commits,
                                        rules=rules, add_rest=True, author='jwilsonrh',
                                        members=['btissoir', 'jwilsonrh'])
        mr_endpoint = self.populate_responses(json_approve_mr)

        version = '701181718'
        ver_endpoint = f'{mr_endpoint}/versions/{version}'
        mr_version = self.load_yaml_asset(
            path=f'mr-{mr_url.id}-{proj_name}-version-{version}.json',
            sub_module='gitlab_rest_api'
        )
        self.responses.get(ver_endpoint, json=mr_version)

        rule_endpoint = f'{mr_endpoint}/approval_rules/167253169'
        rule = self.load_yaml_asset(
            path=f'mr-{mr_url.id}-{proj_name}-rule-mm.json',
            sub_module='gitlab_rest_api'
        )
        self.responses.delete(rule_endpoint, json=rule)

        # The MR has no mapped SST approval rules
        amr = create_approve_mr(mr_url)
        self.assertEqual(MrScope.MISSING, amr.overall_approvals_scope)

        mr_url = GitlabURL('https://gitlab.com/redhat/rhel/src/kernel/'
                           'rhel-8-sandbox/-/merge_requests/602')

        details = f'approvemr_{proj_name}_{mr_url.id}-details.json'
        commits = f'approvemr_{proj_name}_{mr_url.id}-commits.json'
        rules = f'approvemr_{proj_name}_{mr_url.id}-approvalrules2.json'
        json_approve_mr = JsonApproveMR(mr_url=mr_url, details=details, commits=commits,
                                        rules=rules, add_rest=True, author='ptalbert')
        mr_endpoint = self.populate_responses(json_approve_mr)

        version = '211187326'
        ver_endpoint = f'{mr_endpoint}/versions/{version}'
        mr_version = self.load_yaml_asset(
            path=f'mr-{mr_url.id}-{proj_name}-version-{version}.json',
            sub_module='gitlab_rest_api'
        )
        self.responses.get(ver_endpoint, json=mr_version)

        version = '200890914'
        ver_endpoint = f'{mr_endpoint}/versions/{version}'
        mr_version = self.load_yaml_asset(
            path=f'mr-{mr_url.id}-{proj_name}-version-{version}.json',
            sub_module='gitlab_rest_api'
        )
        self.responses.get(ver_endpoint, json=mr_version)

        # The MR is approved
        amr = create_approve_mr(mr_url)
        self.assertEqual(MrScope.OK, amr.overall_approvals_scope)

    @mock.patch('webhook.approval_rules.BaseApprovalRule.create')
    @mock.patch('webhook.ack_nack._emails_to_gl_user_names')
    @mock.patch('webhook.ack_nack.ApproveMR.draft', new_callable=mock.PropertyMock)
    def test_review_request_comment(self, is_draft, mock_cc, mock_create_rule):
        is_draft.return_value = False
        mock_cc.return_value = {}, ['shadowman@redhat.com']

        mr_url = GitlabURL('https://gitlab.com/redhat/rhel/src/kernel/'
                           'rhel-8-sandbox/-/merge_requests/777')
        proj_name = mr_url.namespace.split('/')[-1]

        # Set up the ApproveMR query responses.
        details = f'approvemr_{proj_name}_{mr_url.id}-details-cc.json'
        commits = f'approvemr_{proj_name}_{mr_url.id}-commits.json'
        rules = f'approvemr_{proj_name}_{mr_url.id}-approvalrules.json'
        json_approve_mr = JsonApproveMR(mr_url=mr_url, details=details, commits=commits,
                                        rules=rules, author='jwilsonrh', members=['btissoir'],
                                        add_rest=True, discussions=True)
        mr_endpoint = self.populate_responses(json_approve_mr)

        version = '701181718'
        ver_endpoint = f'{mr_endpoint}/versions/{version}'
        mr_version = self.load_yaml_asset(
            path=f'mr-{mr_url.id}-{proj_name}-version-{version}.json',
            sub_module='gitlab_rest_api'
        )
        self.responses.get(ver_endpoint, json=mr_version)

        rule_endpoint = f'{mr_endpoint}/approval_rules/167253169'
        rule = self.load_yaml_asset(
            path=f'mr-{mr_url.id}-{proj_name}-rule-mm.json',
            sub_module='gitlab_rest_api'
        )
        self.responses.delete(rule_endpoint, json=rule)

        amr = create_approve_mr(mr_url)
        self.assertIn('hid', [r.name for r in amr.rules_to_add])
        comment = amr.review_request_comment()
        self.assertIn("Affected subsystem(s): hid\n\nRequesting review of subsystem hid "
                      "with 0 required approval(s) from user(s) @btissoir "
                      "(benjamin.tissoires@redhat.com)\n\n", comment)
        self.assertIn("Notice: the following email address(es) found in the MR Description "
                      "Cc: tags could not be mapped to Gitlab usernames: ['shadowman@redhat.com']",
                      comment)
        amr.do_update_mr_rules()
        mock_create_rule.assert_called_with(amr.gl_mr)
        with self.assertLogs('cki.webhook.session', level='INFO') as logs:
            amr.do_comments()
            self.assertIn('**ACK/NACK Summary**: ~"Acks::NeedsReview"', ''.join(logs.output))

    @mock.patch('webhook.ack_nack.ApproveMR.remove_labels')
    @mock.patch('webhook.ack_nack.ApproveMR.add_labels')
    def test_do_label_updates(self, mock_add_labels, mock_remove_labels):
        mr_url = GitlabURL('https://gitlab.com/redhat/rhel/src/kernel/'
                           'rhel-8-sandbox/-/merge_requests/602')
        proj_name = mr_url.namespace.split('/')[-1]

        details = f'approvemr_{proj_name}_{mr_url.id}-details.json'
        commits = f'approvemr_{proj_name}_{mr_url.id}-commits.json'
        rules = f'approvemr_{proj_name}_{mr_url.id}-approvalrules.json'

        json_approve_mr = JsonApproveMR(mr_url=mr_url, details=details,
                                        commits=commits, rules=rules, add_rest=True)
        mr_endpoint = self.populate_responses(json_approve_mr)

        version = '211187326'
        ver_endpoint = f'{mr_endpoint}/versions/{version}'
        mr_version = self.load_yaml_asset(
            path=f'mr-{mr_url.id}-{proj_name}-version-{version}.json',
            sub_module='gitlab_rest_api'
        )
        self.responses.get(ver_endpoint, json=mr_version)

        version = '200890914'
        ver_endpoint = f'{mr_endpoint}/versions/{version}'
        mr_version = self.load_yaml_asset(
            path=f'mr-{mr_url.id}-{proj_name}-version-{version}.json',
            sub_module='gitlab_rest_api'
        )
        self.responses.get(ver_endpoint, json=mr_version)

        amr = create_approve_mr(mr_url)

        amr.labels = ["Acks::mm::OK", "Acks::ethernet::OK"]

        amr.do_label_updates()

        mock_add_labels.assert_called_with(['Acks::OK'])
        mock_remove_labels.assert_called_with(['Acks::mm::OK'])

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_assign_reviewers(self):
        mock_mr = mock.MagicMock()
        reviewer = 'nobody'
        assign = {'body': f'/assign_reviewer @{reviewer}'}
        ack_nack.assign_reviewers(mock_mr, [f'@{reviewer}'])
        mock_mr.notes.create.assert_called_with(assign)

    def test_emails_to_gl_user_names(self):
        mock_instance = mock.Mock()
        user1 = mock.Mock(id="1", name="Some One", username="someone")
        user3 = mock.Mock(id="3", name="Red Hatter", username="redhatter")
        reviewers = ['someone@redhat.com', 'nobody@redhat.com', 'redhatter@redhat.com', 'x@y.com']
        mock_instance.users.list.side_effect = [[user1], [], [user3]]
        output = ack_nack._emails_to_gl_user_names(mock_instance, reviewers)
        self.assertEqual(output, ({'someone', 'redhatter'}, ['nobody@redhat.com']))
