"""Tests for backporter."""
from subprocess import CalledProcessError
from unittest import mock

from git.exc import BadName

from tests import fakes
from tests import fakes_jira
from tests.helpers import KwfTestCase
from webhook import common
from webhook import libbackport
from webhook.rhissue import RHIssue
from webhook.session import SessionRunner
from webhook.utils import backporter


def backport_session_runner(
    jira=None,
    args_str='--rhkernel-src /src/kernel-ark -j https://issues.redhat.com/browse/RHEL-101'.split()
) -> SessionRunner:
    """Return a BaseSession with some fake bits."""
    parser = common.get_arg_parser('BACKPORTER')
    parser.add_argument('-r', '--rhkernel-src',
                        help='Directory containing RH kernel projects git tree')
    parser.add_argument('-T', '--testing', action='store_true', default=False,
                        help="Run in testing mode, omit certain checks")
    args = parser.parse_args(args_str)
    session = SessionRunner.new('backporter', args=args)
    session.jira = jira or mock.Mock()
    session.get_gl_instance = mock.Mock()
    return session


@mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_sandbox.yaml',
                                'CKI_DEPLOYMENT_ENVIRONMENT': 'staging'})
class TestLibBackport(KwfTestCase):
    """Tests for libbackport library functions."""

    @mock.patch('webhook.kgit.worktree_add', mock.Mock(return_value=True))
    @mock.patch('webhook.libbackport.Repo', mock.Mock(return_value=True))
    def test_repr(self):
        mock_rhissue = RHIssue.new_from_ji(ji=fakes_jira.JI101, mrs=[])
        mock_bp = libbackport.Backport(mock_rhissue, '/src/kernel-ark')

        mock_repo = mock.Mock(spec_set=['active_branch', 'working_tree_dir'])
        mock_repo.active_branch = mock.Mock(spec_set=['name'])
        mock_repo.active_branch.name = 'backport-RHEL-101-rhel-8-sandbox-main'
        mock_repo.working_tree_dir = '/src/backport-RHEL-101-rhel-8-sandbox-main'
        mock_bp.repo = mock_repo

        self.assertIn(('Backport attempt:\n'
                       ' * Issue: RHEL-101 '
                       '(Fix Version: rhel-8.10)\n'
                       ' * Target branch: rhel-8-sandbox/main\n'
                       ' * Project namespace: redhat/rhel/src/kernel/rhel-8-sandbox\n'
                       ' * Backport directory: /src/backport-RHEL-101-rhel-8-sandbox-main\n'
                       ' * Source branch: backport-RHEL-101-rhel-8-sandbox-main\n'
                       ' * Commit(s): [\'abcdef012345\']'),
                      f'{mock_bp}')

    def test_trim_title_for_mr(self):
        start = "TRIAGE CVE-1999-2000 kernel: bug: this is a jira bug title [rhel-8.10.z]"
        expected = "CVE-1999-2000: bug: this is a jira bug title"
        result = libbackport.trim_title_for_mr(start)
        self.assertEqual(result, expected)

        start = "This is just a basic jira bug title"
        result = libbackport.trim_title_for_mr(start)
        self.assertEqual(result, start)

    @mock.patch('webhook.kgit.branch_delete')
    @mock.patch('webhook.kgit.worktree_add')
    @mock.patch('webhook.libbackport.Repo')
    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_prep_branch_for_backporting(self, mock_gitrepo, mock_gwta, mock_gbd):
        mock_rhissue = RHIssue.new_from_ji(ji=fakes_jira.JI101, mrs=[])
        mock_session = mock.Mock()
        mock_session.args.rhkernel_src = '/src/kernel-ark'
        mock_repo = mock.Mock(spec_set=['active_branch', 'working_tree_dir'])
        mock_repo.active_branch = mock.Mock(spec_set=['name'])
        mock_repo.active_branch.name = 'backport-RHEL-101-rhel-8-sandbox-main'
        mock_repo.working_tree_dir = '/src/backport-RHEL-101-rhel-8-sandbox-main'
        mock_gitrepo.return_value = mock_repo
        mock_gwta.side_effect = CalledProcessError(2, ["bad"])
        mock_gbd.side_effect = CalledProcessError(2, ["bad"])
        mock_bp = libbackport.Backport(mock_rhissue, mock_session)
        self.assertEqual(mock_bp.repo.active_branch.name,
                         'backport-RHEL-101-rhel-8-sandbox-main')
        self.assertEqual(mock_bp.repo.working_tree_dir,
                         '/src/backport-RHEL-101-rhel-8-sandbox-main')

    @mock.patch('webhook.libbackport.Backport.repo')
    def test_backport_commits(self, mock_repo):
        def side_effect(ref):
            raise BadName(ref)

        mock_commit = mock.Mock()
        mock_commit.hexsha = 'feeddeadbeef00000000feeddeadbeef00000000'
        mock_repo.commit.return_value = mock_commit

        issue = self.make_jira_issue('RHEL-101')
        rhi = RHIssue.new_from_ji(ji=issue, mrs=None)
        session = backport_session_runner()
        backport = libbackport.Backport(rhi, session)
        backport.provided_hashes = ['feeddeadbeef']
        self.assertIn('feeddeadbeef', backport.backport_commits)

        backport = libbackport.Backport(rhi, session)
        backport.provided_hashes = ['feeddeadbeef']
        mock_repo.commit.side_effect = side_effect
        self.assertIsNone(backport.backport_commits)
        self.assertIn("Commit ref not found in backporter local repo cache:", backport.status)
        self.assertIn("Ref 'feeddeadbeef' did not resolve to an object", backport.status)

    @mock.patch('webhook.libbackport.Backport.backport_commits', new_callable=mock.PropertyMock)
    @mock.patch('webhook.libbackport.SessionRunner.get_gl_project')
    def test_commits_already_backported(self, mock_gglp, mock_bpc):
        mock_proj = mock.Mock()
        mock_gglp.return_value = mock_proj
        sha = '693fe5956659373a6919f680fe6d99a7bf13c8b4'
        mock_commit = mock.Mock(hexsha=sha)

        # No project will be found in tests/assets/rh_projects_sandbox.yaml
        issue = self.make_jira_issue('RHEL-101')
        rhi = RHIssue.new_from_ji(ji=issue, mrs=None)
        session = backport_session_runner()
        backport = libbackport.Backport(rhi, session)
        self.assertFalse(libbackport.Backport.commits_already_backported(backport))

        # No commits found already backported
        issue = self.make_jira_issue('RHEL-108')
        rhi = RHIssue.new_from_ji(ji=issue, mrs=None)
        session = backport_session_runner()
        mock_proj.search.return_value = []
        backport = libbackport.Backport(rhi, session)
        mock_bpc.return_value = {sha: mock_commit}
        self.assertFalse(libbackport.Backport.commits_already_backported(backport))

        # Commit found already backported
        fake_commit = {'id': 'abcdef012345',
                       'message': 'foo\ncommit 693fe5956659373a6919f680fe6d99a7bf13c8b4\nbar'}
        issue = self.make_jira_issue('RHEL-108')
        rhi = RHIssue.new_from_ji(ji=issue, mrs=None)
        session = backport_session_runner()
        mock_proj.search.return_value = [fake_commit]
        backport = libbackport.Backport(rhi, session)
        self.assertTrue(libbackport.Backport.commits_already_backported(backport))

    @mock.patch('webhook.kgit.remotes')
    @mock.patch('webhook.kgit.cherry_pick')
    @mock.patch('webhook.libbackport.Repo')
    @mock.patch('webhook.kgit.fetch_all', mock.Mock(return_value=True))
    @mock.patch('webhook.libbackport.Backport.commits_already_backported',
                mock.Mock(return_value=False))
    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_try_to_backport(self, mock_repo, mock_gcp, mock_remotes):
        mock_rhissue = RHIssue.new_from_ji(ji=fakes_jira.JI101, mrs=[])
        mock_session = mock.Mock()
        mock_session.args.rhkernel_src = '/src/kernel-ark'
        mock_session.comment.gitlab_footer.return_value = 'This is a footer'

        mock_rhissue._jira = mock.Mock(server_url='https://issues.redhat.com')

        mock_repo().head.commit.summary = 'commit title'
        mock_repo().head.commit.message = 'something is happening here'
        mock_repo().git.log.return_value = 'git log output'
        mock_repo().active_branch.name = 'foo'
        mock_repo().working_tree_dir = '/src/foo'

        mock_backport = libbackport.Backport(mock_rhissue, mock_session)

        ret = libbackport.Backport.try_to_backport(mock_backport)
        self.assertTrue(ret)
        expected_msg = ('commit title\n\n'
                        'JIRA: https://issues.redhat.com/browse/RHEL-101  \n'
                        'CVE: CVE-1984-2001\n\n'
                        'git log output')
        mock_repo().git.commit.assert_called_with('--reset-author', '-s', '--amend', '-m',
                                                  expected_msg)

        mock_gcp.side_effect = CalledProcessError(2, ["bad"])
        ret = libbackport.Backport.try_to_backport(mock_backport)
        self.assertFalse(ret)

        test_issue = self.make_jira_issue('RHEL-48706')
        test_issue.fields.customfield_12324041 = ''
        test_rhi = RHIssue.new_from_ji(ji=test_issue, mrs=None)
        test_rhi._jira = mock.Mock(server_url='https://issues.redhat.com')
        mock_gcp.reset_mock()
        mock_backport = libbackport.Backport(test_rhi, mock_session)
        mock_backport.provided_hashes = '664d424accbc2f16681ad0b968a39fbb1096a695'
        ret = libbackport.Backport.try_to_backport(mock_backport)
        self.assertFalse(ret)
        mock_gcp.assert_not_called()
        self.assertIn("bad commit list in a CVE bug?", mock_backport.status)

    def test_mr_for_branch_exists(self):
        mock_gl = fakes.FakeGitLab()
        namespace = 'redhat/rhel/src/kernel/rhel-8-sandbox'
        mock_project = mock_gl.add_project(12345, namespace)

        bp_branch = 'backport-RHEL-101-rhel-8-sandbox-main'
        target_branch = 'main'
        self.assertFalse(libbackport.mr_for_branch_exists(mock_project, bp_branch, target_branch))
        mr_attrs = {'source_branch': 'backport-RHEL-101-rhel-8-sandbox-main'}
        mock_project.add_mr(555, actual_attributes=mr_attrs)
        self.assertTrue(libbackport.mr_for_branch_exists(mock_project, bp_branch, target_branch))

    @mock.patch('webhook.kgit.branches_differ')
    @mock.patch('webhook.libbackport.mr_for_branch_exists')
    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    @mock.patch('webhook.libbackport.Repo', mock.Mock(return_value=True))
    def test_submit_merge_request(self, mock_mrfbe, mock_gbd):
        mock_session = backport_session_runner()
        mock_session.args.rhkernel_src = '/src/kernel-ark'
        mock_mrfbe.return_value = False
        mock_gbd.return_value = True

        mock_gl = fakes.FakeGitLab()
        namespace = 'redhat/rhel/src/kernel/rhel-8-sandbox'
        fork_namespace = 'redhat/red-hat-ci-tools/kernel/bot-branches/rhel-8-sandbox'
        mock_project = mock_gl.add_project(1234, namespace)
        mock_fork_proj = mock_gl.add_project(5678, fork_namespace)

        mock_ji_project = mock.Mock(spec_set=['name', 'namespace'])
        mock_ji_project.name = 'rhel-8-sandbox'
        mock_ji_project.namespace = 'redhat/rhel/kernel/src/rhel-8-sandbox'
        mock_session.gl_instance.projects.get.return_value = mock_fork_proj

        mock_rhissue = RHIssue.new_from_ji(ji=fakes_jira.JI666, mrs=[])
        mock_backport = libbackport.Backport(mock_rhissue, mock_session)

        mock_repo = mock.Mock(spec_set=['head', 'active_branch', 'working_tree_dir', 'remote'])
        mock_commit = mock.Mock(spec_set=['summary', 'message'])
        mock_commit.summary = 'commit title'
        mock_commit.message = 'commit title\n\ncommit body text'
        mock_repo.head.commit = mock_commit
        mock_repo.active_branch = mock.Mock(spec_set=['name'])
        mock_repo.active_branch.name = 'foo'
        mock_repo.working_tree_dir = '/src/foo'
        mock_remote = mock.Mock(spec_set=['name', 'refs', 'push'])
        mock_remote.name = f'bot-{mock_project.name}'
        mock_remote.refs = []
        mock_repo.remote.return_value = mock_remote
        mock_backport.repo = mock_repo
        mr_attrs = {'web_url': 'https://gitlab.com/namespace/project/-/merge_requests/1234'}
        mock_fork_proj.add_mr(1234, actual_attributes=mr_attrs)
        mock_session.get_gl_project = mock.Mock(return_value=mock_project)

        ret = libbackport.Backport.submit_merge_request(mock_backport)
        self.assertTrue(ret)
        self.assertEqual("New MR: https://gitlab.com/namespace/project/-/merge_requests/1234",
                         mock_backport.status)

        mock_backport.status = ''
        mock_mrfbe.return_value = True
        ret = libbackport.Backport.submit_merge_request(mock_backport)
        self.assertFalse(ret)
        self.assertEqual("Existing open MR found, not creating a new one", mock_backport.status)

        mock_backport.status = ''
        mock_remote.refs = [mock_repo.active_branch.name]
        ret = libbackport.Backport.submit_merge_request(mock_backport)
        self.assertFalse(ret)
        self.assertIn("Updated existing branch for RHEL-666.\n", mock_backport.status)

        mock_backport.status = ''
        mock_gbd.return_value = False
        ret = libbackport.Backport.submit_merge_request(mock_backport)
        self.assertFalse(ret)
        self.assertIn("Latest backport attempt does not differ from prior one\n",
                      mock_backport.status)

        mock_backport.status = ''
        mock_session.args.testing = True
        ret = libbackport.Backport.submit_merge_request(mock_backport)
        self.assertFalse(ret)
        self.assertIn("Not pushing in testing mode.", mock_backport.status)

    @mock.patch('webhook.kgit.branch_delete')
    @mock.patch('webhook.kgit.worktree_remove')
    @mock.patch('webhook.session.SessionRunner')
    def test_cleanup(self, mock_session, mock_gwtr, mock_gbd):
        mock_session.args.rhkernel_src = '/src/kernel-ark'
        mock_rhissue = RHIssue.new_from_ji(ji=fakes_jira.JI666, mrs=[])
        mock_backport = libbackport.Backport(mock_rhissue, mock_session)
        libbackport.Backport.cleanup(mock_backport)
        mock_gwtr.assert_called_once()
        mock_gbd.assert_called_once()

    @mock.patch('jira.resources.Issue.update', mock.Mock())
    @mock.patch('webhook.session.is_production', mock.Mock(return_value=True))
    def test_add_info_to_jira_issue(self):
        issue = self.make_jira_issue('RHEL-108')
        rhi = RHIssue.new_from_ji(ji=issue, mrs=None)
        session = backport_session_runner()
        backport = libbackport.Backport(rhi, session)

        with self.assertLogs('cki.webhook.libbackport', level='INFO') as logs:
            libbackport.Backport.add_info_to_jira_issue(backport, False)
            self.assertIn("Adding label kwf-backport-fail", logs.output[-1])
            self.assertIn("[~shadowman@redhat.com]", logs.output[-1])
            session.jira.add_comment.assert_called_once()

            session.jira.add_comment.reset_mock()
            libbackport.Backport.add_info_to_jira_issue(backport, True)
            self.assertIn("Adding label kwf-backport-success", logs.output[-1])
            self.assertIn("[~shadowman@redhat.com]", logs.output[-1])
            session.jira.add_comment.assert_called_once()

    @mock.patch('webhook.rhissue.RHIssue.mr_urls', new_callable=mock.PropertyMock)
    @mock.patch('webhook.session.SessionRunner')
    def test_should_try_backport(self, mock_session, mock_mr_urls):
        mock_session.args.testing = False
        mock_session.args.rhkernel_src = '/src/kernel-ark'
        mock_mr_urls.return_value = []

        mock_rhissue = RHIssue.new_from_ji(ji=fakes_jira.JI2673283, mrs=[])
        mock_backport = libbackport.Backport(mock_rhissue, mock_session)
        msg = libbackport.Backport.should_try_backport(mock_backport)
        expected = "No project found for environment, aborting RHEL-2673283 backport."
        self.assertEqual(msg, expected)

        mock_rh_branch = mock.Mock(allow_backporter=False)
        with mock.patch.object(RHIssue, 'ji_branch', mock_rh_branch):
            mock_rhissue = RHIssue.new_from_ji(ji=fakes_jira.JI667, mrs=[])
            mock_backport = libbackport.Backport(mock_rhissue, mock_session)
            msg = libbackport.Backport.should_try_backport(mock_backport)
            expected = "No backport automation"
            self.assertIn(expected, msg)

        mock_rh_branch = mock.Mock(ji_branch=mock.Mock(inactive=True))
        with mock.patch.object(RHIssue, 'ji_branch', mock_rh_branch):
            mock_rhissue = RHIssue.new_from_ji(ji=fakes_jira.JI667, mrs=[])
            mock_backport = libbackport.Backport(mock_rhissue, mock_session)
            msg = libbackport.Backport.should_try_backport(mock_backport)
            expected = "Skipping inactive branch"
            self.assertIn(expected, msg)

        mock_rhissue = RHIssue.new_from_ji(ji=fakes_jira.JI667, mrs=[])
        mock_backport = libbackport.Backport(mock_rhissue, mock_session)
        msg = libbackport.Backport.should_try_backport(mock_backport)
        expected = "Prior backport attempt for RHEL-667 already failed, aborting."
        self.assertEqual(msg, expected)

        mock_rhissue = RHIssue.new_from_ji(ji=fakes_jira.JI668, mrs=[])
        mock_backport = libbackport.Backport(mock_rhissue, mock_session)
        msg = libbackport.Backport.should_try_backport(mock_backport)
        expected = "Prior backport attempt for RHEL-668 was successful, no need to try again."
        self.assertEqual(msg, expected)

        mock_rhissue = RHIssue.new_from_ji(ji=fakes_jira.JI669, mrs=[])
        mock_backport = libbackport.Backport(mock_rhissue, mock_session)
        msg = libbackport.Backport.should_try_backport(mock_backport)
        expected = "Aborting due to RHEL-669 having no Commit Hashes specified."
        self.assertEqual(msg, expected)

        mock_rhissue = RHIssue.new_from_ji(ji=fakes_jira.JI666, mrs=[])
        mock_backport = libbackport.Backport(mock_rhissue, mock_session)
        mock_mr_urls.return_value = ['there', 'are', 'mrs']
        msg = libbackport.Backport.should_try_backport(mock_backport)
        expected = "Aborting due to existing MR in RHEL-666."
        self.assertEqual(msg, expected)

        mock_rhissue = RHIssue.new_from_ji(ji=fakes_jira.JI670, mrs=[])
        mock_backport = libbackport.Backport(mock_rhissue, mock_session)
        msg = libbackport.Backport.should_try_backport(mock_backport)
        expected = "Aborting due to no Fix Version set in RHEL-670."
        self.assertEqual(msg, expected)

        mock_issue = fakes_jira.JI670
        mock_issue.fields.status = "Closed"
        mock_rhissue = RHIssue.new_from_ji(ji=mock_issue, mrs=[])
        mock_backport = libbackport.Backport(mock_rhissue, mock_session)
        msg = libbackport.Backport.should_try_backport(mock_backport)
        expected = "Aborting backport, the JIRA Issue RHEL-670 not New or Planning."
        self.assertEqual(msg, expected)


@mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_sandbox.yaml',
                                'CKI_DEPLOYMENT_ENVIRONMENT': 'staging'})
class TestBackporter(KwfTestCase):
    """Tests for the various backporter functions."""

    @mock.patch.dict('os.environ', {'RHKERNEL_SRC': ''})
    @mock.patch('webhook.session.connect_jira', mock.Mock())
    @mock.patch('subprocess.run', mock.Mock())
    @mock.patch('webhook.utils.backporter.make_rhissues')
    @mock.patch('webhook.session.SessionRunner')
    @mock.patch('webhook.libbackport.Backport')
    def test_main(self, mock_backport, mock_session, mock_mrhi):
        mock_backport().submit_merge_request.return_value = True, "MR Submitted"
        mock_backport().should_try_backport.return_value = 'bad juju'
        mock_args = '-j RHEL-666'
        with self.assertLogs('cki.webhook.utils.backporter', level='WARNING') as logs:
            backporter.main(mock_args.split())
            self.assertIn("No path to RH Kernel source git found, aborting!", logs.output[-1])

        mock_rhissue = RHIssue.new_from_ji(ji=fakes_jira.JI666, mrs=[])
        mock_mrhi.return_value = [mock_rhissue]
        mock_args = '-r /src/kernel-ark -j RHEL-666'.split()
        mock_session.jira.issue.return_value = fakes_jira.JI666
        with self.assertLogs('cki.webhook.utils.backporter', level='INFO') as logs:
            backporter.main(mock_args)
            self.assertIn("bad juju", logs.output[-1])

        mock_backport().should_try_backport.return_value = ''
        mock_rhissue = RHIssue.new_from_ji(ji=fakes_jira.JI666, mrs=[])
        mock_mrhi.return_value = [mock_rhissue]
        mock_backport().try_to_backport.return_value = False
        backporter.main(mock_args)
        mock_backport().add_info_to_jira_issue.assert_called_with(success=False)
        mock_backport().try_to_backport.return_value = True
        backporter.main(mock_args)
        mock_backport().submit_merge_request.assert_called_once()
