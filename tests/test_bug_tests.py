"""Tests for the bug_tests module."""
from unittest import mock

from tests.helpers import KwfTestCase
from webhook import bug_tests


class DepOnlyTests(KwfTestCase):
    """Validate the tests that are specific to dependency Bugs."""

    @staticmethod
    def _fake_bug(input_dict):
        """Return a mock object with spec set."""
        attr_dict = input_dict.copy()
        if 'alias' not in attr_dict:
            attr_dict['alias'] = 'FakeBug'
        if 'failed_tests' not in attr_dict:
            attr_dict['failed_tests'] = []
        if 'test_failed' not in attr_dict:
            attr_dict['test_failed'] = mock.Mock(return_value=False, spec=[])
        return mock.Mock(spec=list(attr_dict.keys()), **attr_dict)

    def _run_test(self, bug, test, expected_result=True, expected_scope=None,
                  expected_keep_going=True):
        """Run the given test on the given Bug."""
        if expected_scope is None:
            expected_scope = bug_tests.MrScope.READY_FOR_MERGE if expected_result else \
                bug_tests.MrScope.NEEDS_REVIEW
        actual_result, actual_scope, actual_keep_going = test(bug)
        self.assertIs(actual_result, expected_result)
        self.assertIs(actual_scope, expected_scope)
        self.assertIs(actual_keep_going, expected_keep_going)
        if expected_result is True:
            self.assertTrue(test.__name__ not in bug.failed_tests)
        else:
            self.assertTrue(test.__name__ in bug.failed_tests)

    def test_InMrDescription(self):
        """Passes if the Bug is listed in the MR description."""
        this_test = bug_tests.InMrDescription
        # A Bug without commits skips ("passes") the test.
        bug_values = {'commits': [],
                      'in_mr_description': False}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # A Bug with commits which is in the MR description passes the test.
        bug_values = {'commits': [1, 2, 3],
                      'in_mr_description': True}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # A Bug with commits which is NOT in the MR description fails the test.
        bug_values = {'commits': [1, 2, 3],
                      'in_mr_description': False}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test, expected_result=False,
                       expected_scope=bug_tests.MrScope.MISSING)

    def test_HasCommits(self):
        """Passes if the Bug has commits."""
        this_test = bug_tests.HasCommits
        # A Bug with commits passes the test.
        bug_values = {'commits': [1]}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # A Bug without commits fails the test.
        bug_values = {'commits': []}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test, expected_result=False,
                       expected_scope=bug_tests.MrScope.MISSING)

    def test_BZisNotUnknown(self):
        """Passes if the BZStatus is not UNKNOWN."""
        this_test = bug_tests.BZisNotUnknown
        # A Bug with a not UNKNOWN status passes the test.
        bug_values = {'bz_status': bug_tests.BZStatus.ASSIGNED}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # A Bug with an UNKNOWN status fails the test.
        bug_values = {'bz_status': bug_tests.BZStatus.UNKNOWN}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test, expected_result=False,
                       expected_scope=bug_tests.MrScope.INVALID, expected_keep_going=False)
