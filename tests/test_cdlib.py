"""Webhook interaction tests."""
from unittest import mock

from tests.helpers import KwfTestCase
from webhook import cdlib
from webhook import defs


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestCDLib(KwfTestCase):

    def test_extract_files(self):
        """Check function returns expected file list."""

        diff_a = {'newPath': 'net/dev/core.c'}
        diff_b = {'newPath': 'redhat/Makefile'}

        diff = [diff_a, diff_b]
        self.assertEqual(cdlib.extract_files(diff),
                         ['net/dev/core.c', 'redhat/Makefile'])

    def test_extract_ucid(self):
        d1 = mock.Mock(text="1\ncommit 1234567890abcdef1234567890abcdef12345678")
        d2 = mock.Mock(text="2\ncommit a012345")
        d7 = mock.Mock(text="6\n (cherry picked from commit 1234567890abcdef)")
        d8 = mock.Mock(text="1\n(cherry picked from commit "
                            "1234567890abcdef1234567890abcdef12345678)"
                            "\n (cherry picked from commit "
                            "2234567890abcdef1234567890abcdef12345678)"
                            "\n(cherry picked from commit   "
                            "3234567890abcdef1234567890abcdef12345678)")
        d9 = mock.Mock(text="1\ncommit 1234567890abcdef1234567890abcdef12345678"
                            "\ncommit 2234567890abcdef1234567890abcdef12345678"
                            "\n commit 3234567890abcdef1234567890abcdef12345678"
                            "\ncommit 4234567890abcdef1234567890abcdef12345678"
                            "\ncommit  5234567890abcdef1234567890abcdef12345678")
        d10 = mock.Mock(text="XYZ\nUpstream Status: https://github.com/torvalds/linux.git")
        self.assertEqual(cdlib.extract_ucid(d1.text),
                         ["1234567890abcdef1234567890abcdef12345678"])
        self.assertEqual(cdlib.extract_ucid(d2.text), [])
        self.assertEqual(cdlib.extract_ucid(d7.text), [])
        self.assertEqual(cdlib.extract_ucid(d8.text),
                         ["1234567890abcdef1234567890abcdef12345678"])
        self.assertEqual(cdlib.extract_ucid(d9.text),
                         ["1234567890abcdef1234567890abcdef12345678",
                          "2234567890abcdef1234567890abcdef12345678",
                          "4234567890abcdef1234567890abcdef12345678"])
        # This commit should NOT be considered Posted, as Linus' tree always needs a hash
        self.assertEqual(cdlib.extract_ucid(d10.text), [])

    def test_mr_get_diff_ids(self):
        """Test the webhook.cdlib.mr_get_diff_ids function."""
        mrequest = mock.MagicMock()
        diff1 = mock.MagicMock(id=1234)
        diff2 = mock.MagicMock(id=5678)
        mrequest.diffs.list.return_value = [diff1, diff2]
        diff_ids = cdlib.mr_get_diff_ids(mrequest)
        self.assertEqual(diff_ids, [1234, 5678])

    def test_mr_get_last_two_diff_ranges(self):
        """Test the webhook.cdlib.mr_get_last_two_diff_ranges function."""
        mrequest = mock.MagicMock()
        diff = mock.MagicMock(id=1234, head_commit_sha='abcd', start_commit_sha='ef01',
                              created_at='2021-02-10T19:13:53.254Z')
        mrequest.diffs.get.return_value = diff
        (latest, old) = cdlib.mr_get_last_two_diff_ranges(mrequest, [1234])
        self.assertEqual(latest['head'], 'abcd')
        self.assertEqual(latest['start'], 'ef01')
        self.assertEqual(latest['created'], '2021-02-10T19:13:53.254Z')

    def test_mr_get_latest_start_sha(self):
        """Test the webhook.cdlib.mr_get_latest_start_sha function."""
        mrequest = mock.MagicMock()
        latest = {'head': '', 'start': 'deadbeef', 'created': ''}
        mrequest.labels = ['Dependencies::abcdef0123']
        start_sha = cdlib.mr_get_latest_start_sha(mrequest, latest['start'])
        self.assertEqual(start_sha, 'abcdef0123')
        mrequest.labels = ['Dependencies::OK']
        start_sha = cdlib.mr_get_latest_start_sha(mrequest, latest['start'])
        self.assertEqual(start_sha, 'deadbeef')

    def test_is_first_dep(self):
        """Test the webhook.cdlib.is_first_dep function."""
        commit = mock.Mock(id="abcdef0")
        dep_sha = 'abcdef0'
        retval = cdlib.is_first_dep(commit, dep_sha)
        self.assertTrue(retval)
        dep_sha = 'deadbeef'
        retval = cdlib.is_first_dep(commit, dep_sha)
        self.assertFalse(retval)

    def test_get_dependencies_data(self):
        """Test the webhook.cdlib.get_dependencies_data function."""
        mrequest = mock.MagicMock()
        mrequest.diff_refs['start_sha'] = 'abbadabba'
        mrequest.labels = ['Dependencies::deafabba']
        (has_deps, dep_sha) = cdlib.get_dependencies_data(mrequest)
        self.assertTrue(has_deps)
        self.assertEqual(dep_sha, 'deafabba')
        mrequest.labels = ['Dependencies::OK']
        (has_deps, dep_sha) = cdlib.get_dependencies_data(mrequest)
        self.assertFalse(has_deps)

    def test_mr_get_old_start_sha(self):
        """Test the webhook.cdlib.mr_get_old_start_sha function."""
        mrequest = mock.MagicMock()
        # We have a removed Dependencies::<hash> label to consider
        rlevent = mock.MagicMock(id=8675309,
                                 label={'id': 1234, 'name': 'Dependencies::abcd'},
                                 created_at='2021-02-10T19:15:09.451Z', action='remove')
        old = {'head': '', 'start': 'eeeeeeee', 'created': '2021-02-10T19:14:09.451Z'}
        latest = {'head': '', 'start': 'ffffffff', 'created': '2021-02-14T19:14:09.451Z'}
        cc_ts = '2021-02-10T19:14:09.451Z'
        mrequest.resourcelabelevents.list.return_value = [rlevent]
        start_sha = cdlib.mr_get_old_start_sha(mrequest, old, latest, cc_ts)
        self.assertEqual(start_sha, 'abcd')
        # We have a stale removed Dependencies::<hash> label to consider
        cc_ts = '2021-02-09T19:14:09.451Z'
        start_sha = cdlib.mr_get_old_start_sha(mrequest, old, latest, cc_ts)
        self.assertEqual(start_sha, 'eeeeeeee')
        # We have an event newer than latest
        latest = {'head': '', 'start': 'ffffffff', 'created': '2021-02-01T19:14:09.451Z'}
        start_sha = cdlib.mr_get_old_start_sha(mrequest, old, latest, cc_ts)
        self.assertEqual(start_sha, 'eeeeeeee')
        # We have no evtid.label
        rlevent = mock.MagicMock(id=8675309, label=False)
        mrequest.resourcelabelevents.list.return_value = [rlevent]
        start_sha = cdlib.mr_get_old_start_sha(mrequest, old, latest, cc_ts)
        self.assertEqual(start_sha, 'eeeeeeee')
        # We have no Dependencies:: labels to consider
        rlevent = mock.MagicMock(id=8675309,
                                 label={'id': 1234, 'name': 'ILikePie::OK'},
                                 created_at='2021-02-12T19:14:09.451Z', action='remove')
        old = {'head': '', 'start': 'ffffffff', 'created': '2021-02-10T19:14:09.451Z'}
        latest = {'head': '', 'start': 'ffffffff', 'created': '2021-02-14T19:14:09.451Z'}
        mrequest.resourcelabelevents.list.return_value = [rlevent]
        start_sha = cdlib.mr_get_old_start_sha(mrequest, old, latest, cc_ts)
        self.assertEqual(start_sha, 'ffffffff')
        # We have a removed Dependencies::OK label to consider
        rlevent = mock.MagicMock(id=8675309,
                                 label={'id': 1234, 'name': 'Dependencies::OK'},
                                 created_at='2021-02-12T19:14:09.451Z', action='remove')
        old = {'head': '', 'start': 'deadbeef', 'created': '2021-02-10T19:14:09.452Z'}
        mrequest.resourcelabelevents.list.return_value = [rlevent]
        start_sha = cdlib.mr_get_old_start_sha(mrequest, old, latest, cc_ts)
        self.assertEqual(start_sha, 'deadbeef')

    @mock.patch('git.Repo')
    def test_get_git_diff(self, mocked_git_repo):
        """Test the webhook.cdlib.get_git_diff function."""
        latest = {'head': 'beef', 'start': 'dead', 'created': '2021-02-12T17:14:29.362Z'}
        old = {'head': 'eeee', 'start': 'ffff', 'created': '2021-02-10T19:14:09.451Z'}
        with self.assertLogs('cki.webhook.cdlib', level='INFO') as logs:
            old_diff = cdlib.get_git_diff(mocked_git_repo, old)
            new_diff = cdlib.get_git_diff(mocked_git_repo, latest)
            self.assertNotEqual(old_diff, None)
            self.assertNotEqual(new_diff, None)
            self.assertIn('Generating diff: ffff..eeee, 2021-02-10T19:14:09.451Z', logs.output[-2])
            self.assertIn('Generating diff: dead..beef, 2021-02-12T17:14:29.362Z', logs.output[-1])

    @mock.patch('webhook.cdlib.get_submitted_diff')
    def test_get_diff_from_mr(self, mocked_sub_diff):
        """Test the webhook.cdlib.get_diff_from_mr function."""
        mrequest = mock.MagicMock()
        diff_ids = [1234, 5678]
        latest = {'head': 'beef', 'start': 'dead', 'created': '2021-02-12T17:14:29.362Z'}
        old = {'head': 'eeee', 'start': 'ffff', 'created': '2021-02-10T19:14:09.451Z'}
        mocked_sub_diff.return_value = ('diff stuff', 'filelist')
        old_diff = cdlib.get_diff_from_mr(mrequest, diff_ids, old)
        new_diff = cdlib.get_diff_from_mr(mrequest, diff_ids, latest)
        self.assertEqual(old_diff, None)
        self.assertEqual(new_diff, None)

    @mock.patch('webhook.cdlib.mr_get_latest_start_sha')
    @mock.patch('webhook.cdlib.mr_get_diff_ids')
    def test_get_filtered_changed_files(self, get_diff_ids, get_start_sha):
        gl_mergerequest = mock.Mock()
        get_diff_ids.return_value = ['1234', '5678']
        get_start_sha.return_value = 'abcd1234'
        c1 = mock.MagicMock(id="12345678")
        c2 = mock.MagicMock(id="deadbeef")
        c1.diff.return_value = [{'new_path': 'kernel/fork.c'}]
        c2.diff.return_value = [{'new_path': 'include/linux/netdevice.h'}]
        gl_mergerequest.commits.return_value = [c1, c2]
        filelist = cdlib.get_filtered_changed_files(gl_mergerequest)
        self.assertEqual(filelist, ['include/linux/netdevice.h', 'kernel/fork.c'])
        get_start_sha.return_value = 'deadbeef'
        filelist = cdlib.get_filtered_changed_files(gl_mergerequest)
        self.assertEqual(filelist, ['kernel/fork.c'])

    def test_find_first_dependency_commit(self):
        """Test find_first_dependency_commit's functionality."""
        project = mock.Mock()
        project.commits.get.return_value = mock.Mock(parent_ids=["1234"])
        merge_request = mock.Mock(target_branch='main')
        message = ('This BZ has dependencies.\n'
                   'Bugzilla: https://bugzilla.redhat.com/1234567\n'
                   'Depends: https://bugzilla.redhat.com/22334455\n'
                   'Depends: https://bugzilla.redhat.com/33445566\n')
        merge_request.description = message
        c1 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                       message="1\ncommit 1234567890abcdef1234567890abcdef12345678",
                       parent_ids=["abcd"])
        c2 = mock.Mock(id="4567", author_email="xyz@example.com",
                       message="2\ncommit 12345678", parent_ids=["1234"])
        c3 = mock.Mock(id="890a", author_email="jdoe@redhat.com",
                       message="3\nBugzilla: https://bugzilla.redhat.com/33445566",
                       parent_ids=["f00d"])
        merge_request.commits.return_value = [c1, c2, c3]
        output = cdlib.find_first_dependency_commit(project, merge_request)
        self.assertEqual(output, ('890a', True))
        project.commits.get.return_value = mock.Mock(id="beef", parent_ids=["1234", "5678"])
        output = cdlib.find_first_dependency_commit(project, merge_request)
        self.assertEqual(output, ('beef', False))

    def test_get_dependencies_label_scope(self):
        """Test that a Dependencies label gets set properly."""
        scope = cdlib.get_dependencies_label_scope("1234567890abcdef")
        self.assertEqual("1234567890ab", scope)
        scope = cdlib.get_dependencies_label_scope(None)
        self.assertEqual("OK", scope)

    def test_get_last_code_changed_timestamp(self):
        """Test that we get a TS back from get_last_code_changed_timestamp when we should."""
        mock_mr = mock.Mock()
        rev = 2
        timestamp = '2021-02-10T19:13:53.254Z'
        mock_mr.labels = [f'{defs.CODE_CHANGED_PREFIX}v{rev}']
        diff_ids = ['1234', '5678']

        diff = mock.MagicMock(id=5678, head_commit_sha='abcd', start_commit_sha='ef01',
                              created_at=timestamp)
        mock_mr.diffs.get.return_value = diff

        output = cdlib.get_last_code_changed_timestamp(mock_mr, diff_ids)
        self.assertEqual(output, timestamp)

        mock_mr.labels = []
        output = cdlib.get_last_code_changed_timestamp(mock_mr, diff_ids)
        self.assertEqual(output, None)

    @mock.patch('webhook.cdlib.find_first_dependency_commit')
    def test_make_dependencies_label(self, first_dep):
        """Test that Dependencies labels are made correctly."""
        # Should be a group label if scope == OK, otherwise, project label
        project = mock.Mock()
        merge_request = mock.Mock()
        merge_request.iid = 617
        dep_sha = "abcdef0123456789"
        # Has dependency commits
        first_dep.return_value = (dep_sha, True)
        output = cdlib.make_dependencies_label(project, merge_request)
        self.assertEqual(output, f'Dependencies::{dep_sha[:12]}')
        # Has no dependencies
        first_dep.return_value = (None, False)
        output = cdlib.make_dependencies_label(project, merge_request)
        self.assertEqual(output, f'Dependencies::{defs.READY_SUFFIX}')
        # Has merge commits, but no dependency commits
        first_dep.return_value = (dep_sha, False)
        output = cdlib.make_dependencies_label(project, merge_request)
        self.assertEqual(output, f'Dependencies::{defs.READY_SUFFIX}::{dep_sha[:12]}')

    CHANGES = {'changes': [{'old_path': 'drivers/net/ethernet/intel/ixgbe/ixgbe.h',
                            'new_path': 'drivers/net/ethernet/intel/ixgbe/ixgbe.h'},
                           {'old_path': 'include/linux/mm.h',
                            'new_path': 'include/linux/mm2.h'},
                           {'old_path': 'include/net/bonding.h',
                            'new_path': 'include/net/bond_xor.h'}]
               }

    @mock.patch('webhook.cdlib.extract_files')
    def test_is_rhdocs_commit(self, filelist):
        filelist.return_value = ['redhat/rhdocs/hugo_config.yaml']
        commit = mock.Mock()
        status = cdlib.is_rhdocs_commit(commit)
        self.assertTrue(status)
        filelist.return_value = ['redhat/rhdocs/info/owners.yaml']
        status = cdlib.is_rhdocs_commit(commit)
        self.assertTrue(status)
        filelist.return_value = ['include/linux/net_device.h']
        status = cdlib.is_rhdocs_commit(commit)
        self.assertFalse(status)
        filelist.return_value = ['scripts/do_something.bat']
        self.assertFalse(cdlib.is_rhdocs_commit(commit))
        filelist.return_value = ['redhat/rhdocs/scripts/do_something.bat']
        self.assertTrue(cdlib.is_rhdocs_commit(commit))


class TestAssembleInterdiffMarkdown(KwfTestCase):
    """Tests for the assemble_interdiff_markdown function."""

    def test_assemble_interdiff_markdown_empty_input(self):
        """Returns None on empty input."""
        test_inputs = [
            [],
            ['', '', '']
        ]

        for test_input in test_inputs:
            with self.subTest(input_param=test_input):
                self.assertIsNone(cdlib.assemble_interdiff_markdown(test_input))

    def test_assemble_interdiff_markdown_with_input(self):
        """Returns a string when the input contains something."""
        test_inputs = [
            ['a', 'b', 'c'],
            ['d'],
            ['e', 'f'],
            ['weee']*200
        ]

        for test_input in test_inputs:
            with self.subTest(input_param=test_input):
                result = cdlib.assemble_interdiff_markdown(test_input)
                self.assertIsInstance(result, str)
                if len(test_input) > 100:
                    self.assertIn('Interdiff contains', result)
                else:
                    self.assertIn('old version', result)
                    self.assertIn('new version', result)
