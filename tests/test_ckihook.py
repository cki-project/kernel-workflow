"""Webhook interaction tests."""
from copy import deepcopy
import typing
from unittest import mock

from cki_lib import misc
from freezegun import freeze_time
import responses
from responses.matchers import json_params_matcher

from tests import fake_payloads
from tests import fakes
from tests.fake_payloads import GITLAB_GRAPHQL
from tests.helpers import KwfTestCase
from tests.test_pipelines import make_test_pipeline
from webhook import ckihook
from webhook import defs
from webhook import session_events
from webhook.pipelines import CkiFailure
from webhook.pipelines import PipelineType
from webhook.session import SessionRunner
from webhook.session_events import create_event

CHANGES = {'labels': {'previous': [{'title': f'{PipelineType.REALTIME.prefix}::Running'},
                                   {'title': 'CKI::Running'},
                                   {'title': f'{PipelineType.AUTOMOTIVE.prefix}::Running'}
                                   ],
                      'current': [{'title': f'{PipelineType.REALTIME.prefix}::Canceled'}]
                      }
           }

HEADERS = {'message-type': 'gitlab'}
DATAWAREHOUSE_HEADERS = {'message-type': 'datawarehouse'}
BASE_JOB_GID = "gid://gitlab/Ci::Build/1020304"
RETRY_STAGE_NAMES = ["kernel-results", "check-results", "triager"]
RETRY_JOB_NAME_PREFIX = "check-result "
FALLBACK_JOB_NAME = "prepare python"
JOB_ASSETS = {
    "prepare_python": {
        "id": BASE_JOB_GID + "1",
        "name": FALLBACK_JOB_NAME,
        "status": "SUCCESS",
        "stage": {"name": "prepare"},
        "retryable": True,
    },
    "random_build": {
        "id": BASE_JOB_GID + "2",
        "name": "build x86_64",
        "status": "SUCCESS",
        "stage": {"name": "build"},
    },
    "check_results1": {
        "id": BASE_JOB_GID + "3",
        "name": RETRY_JOB_NAME_PREFIX + "aarch64",
        "status": "SUCCESS",
        "stage": {"name": RETRY_STAGE_NAMES[0]},
        "retryable": True,
    },
    "check_results2": {
        "id": BASE_JOB_GID + "4",
        "name": RETRY_JOB_NAME_PREFIX + "x86_64",
        "status": "FAILED",
        "stage": {"name": RETRY_STAGE_NAMES[1]},
        "retryable": True,
    },
    "check_results_skipped": {
        "id": BASE_JOB_GID + "5",
        "name": RETRY_JOB_NAME_PREFIX + "aarch64",
        "status": "SKIPPED",
        "stage": {"name": RETRY_STAGE_NAMES[2]},
        "retryable": True,
    },
    "check_results_not_retryable": {
        "id": BASE_JOB_GID + "3",
        "name": RETRY_JOB_NAME_PREFIX + "aarch64",
        "status": "SUCCESS",
        "stage": {"name": RETRY_STAGE_NAMES[0]},
        "retryable": False,
    },
}


class TestReport(KwfTestCase):
    """Tests for the generate_simple_report function."""

    def test_missing(self) -> None:
        """Returns a string warning about missing pipelines."""
        pipelines = {ckihook.PipelineType.RHEL: None, ckihook.PipelineType.REALTIME: None}

        report = ckihook.generate_simple_report(pipelines, [], {})

        self.assertIn('RHEL | Missing', report)
        self.assertIn('Realtime | Missing', report)
        self.assertIn(ckihook.REPORT_HEADER_BAD, report)
        self.assertNotIn(ckihook.REPORT_HEADER_GOOD, report)

    def test_merge_failure(self) -> None:
        """Returns a string including merge failures."""
        pipe1 = make_test_pipeline(1537088748)
        pipelines = {pipe.type: pipe for pipe in [pipe1]}

        report = ckihook.generate_simple_report(pipelines, [], {})

        self.assertIn(ckihook.REPORT_HEADER_BAD, report)
        self.assertIn(
            '[CentOS](http://localhost/kcidb/checkouts/redhat:1537088748) | Failed (merge)',
            report
        )
        self.assertNotIn(ckihook.REPORT_HEADER_GOOD, report)

    def test_build_failure(self) -> None:
        """Returns a string including build failures."""
        pipe1 = make_test_pipeline(1555224113)
        pipelines = {pipe.type: pipe for pipe in [pipe1]}

        report = ckihook.generate_simple_report(pipelines, [], {})

        self.assertIn(ckihook.REPORT_HEADER_BAD, report)
        self.assertIn(ckihook.BUILD_FAILURE_HEADER, report)
        self.assertNotIn(ckihook.REPORT_HEADER_GOOD, report)

    def test_test_failure(self) -> None:
        """Returns a string including test failures."""
        pipe1 = make_test_pipeline(1559018829)
        pipelines = {pipe.type: pipe for pipe in [pipe1]}

        new_issue_format = {'footer': 'FOOTER', 'mr_url': 'MR_URL'}

        report = ckihook.generate_simple_report(pipelines, [], new_issue_format)

        self.assertIn(ckihook.REPORT_HEADER_BAD, report)
        self.assertIn(ckihook.TEST_FAILURE_HEADER, report)
        self.assertNotIn(ckihook.REPORT_HEADER_GOOD, report)

        expected_test_ids = {"redhat:1559018829-x86_64-kernel-debug_upt_10"}

        for test_id in expected_test_ids:
            self.assertIn(str(test_id), report)

    def test_pipeline_success(self) -> None:
        """Returns a string reporting success."""
        pipe1 = make_test_pipeline(1560072291)
        pipelines = {pipe.type: pipe for pipe in [pipe1]}

        report = ckihook.generate_simple_report(pipelines, [], {})

        self.assertIn(ckihook.REPORT_HEADER_GOOD, report)
        self.assertNotIn(ckihook.REPORT_HEADER_BAD, report)


class TestHelpers(KwfTestCase):
    """Test helper functions."""

    def test_waiver_url(self) -> None:
        """Returns a URL that helps create a jira issue."""
        # Format values needed for ckihook.NEW_ISSUE_DESCRIPTION plus jira_server URL.
        issue_format = {
            'jira_server': 'https://issues.redhat.com',
            'footer': 'FORMAT_FOOTER',
            'mr_url': 'FORMAT_MR_URL'
        }

        failure1 = CkiFailure(PipelineType.RHEL, 'TEST_NAME1', 123, 'aarch64', False, 'TEST_URL1')
        failure2 = CkiFailure(PipelineType.REALTIME, 'TEST_NAME1', 456, 'x86_64', True, 'TEST_URL2')
        failure3 = CkiFailure(PipelineType._64K, 'TEST_NAME2', 789, 'aarch64', True, 'TEST_URL3')

        # Raises a RuntimeError because all the test names are not the same.
        with self.assertRaises(RuntimeError):
            ckihook.waiver_url([failure1, failure2, failure3], issue_format)

        result = ckihook.waiver_url([failure1, failure2], issue_format)
        self.assertIsInstance(result, str)
        self.assertTrue(result)

    def test_branch_changed(self):
        """Returns True if it seems the MR branch changed."""
        # No head pipeline ID, returns False
        payload = {'object_attributes': {'head_pipeline_id': None}}
        self.assertIs(ckihook.branch_changed(payload), False)
        # Merge status exists, returns True
        payload['object_attributes']['head_pipeline_id'] = 1
        payload['changes'] = {'merge_status': 'boop'}
        self.assertIs(ckihook.branch_changed(payload), True)
        # Merge status not found, returns False
        payload['changes'] = {}
        self.assertIs(ckihook.branch_changed(payload), False)

    def test_cki_label_changed(self):
        """Returns True if any CKI labels are in the changes list."""
        changes = deepcopy(CHANGES)
        self.assertTrue(ckihook.cki_label_changed(changes))
        self.assertFalse(ckihook.cki_label_changed({}))

    @mock.patch('webhook.ckihook.get_downstream_pipeline_branch')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=False))
    def test_process_possible_branch_change_non_prod(self, mock_get_ds_branch):
        """Makes no changes."""
        payload = deepcopy(fake_payloads.MR_PAYLOAD)
        mock_instance = fakes.FakeGitLab()
        mock_project = mock_instance.add_project(fake_payloads.PROJECT_ID,
                                                 fake_payloads.PROJECT_PATH_WITH_NAMESPACE)
        mock_pipe = mock.Mock()

        # No pipelines, returns False.
        graph_mr = mock.Mock(gl_instance=mock_instance, gl_project=mock_project, pipelines=[])
        self.assertIs(ckihook.process_possible_branch_change(mock.Mock(), graph_mr, payload), False)

        # Branch not determined for current downstream pipeline, returns False.
        graph_mr.pipelines = [mock_pipe]
        mock_get_ds_branch.return_value = None
        self.assertIs(ckihook.process_possible_branch_change(mock.Mock(), graph_mr, payload), False)

        # Downstream pipeline 'branch' matches mr target branch, returns False.
        mock_get_ds_branch.return_value = payload['object_attributes']['target_branch']
        self.assertIs(ckihook.process_possible_branch_change(mock.Mock(), graph_mr, payload), False)

        # Downstream pipeline branch has changed, but not production, returns True.
        mock_get_ds_branch.return_value = 'new_branch'
        self.assertIs(ckihook.process_possible_branch_change(mock.Mock(), graph_mr, payload), True)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch('webhook.ckihook.get_downstream_pipeline_branch')
    @mock.patch('webhook.common.cancel_pipeline')
    @mock.patch('webhook.common.create_mr_pipeline')
    def test_process_possible_branch_change_prod(self, mock_create, mock_cancel,
                                                 mock_get_ds_branch):
        """Returns True when a change is detected and pipeline is canceled/triggered."""
        payload = deepcopy(fake_payloads.MR_PAYLOAD)
        mock_instance = fakes.FakeGitLab()
        test_session = SessionRunner.new('ckihook', 'args')
        test_session.get_gl_instance = mock.Mock(return_value=mock_instance)
        mock_project = mock_instance.add_project(fake_payloads.PROJECT_ID,
                                                 fake_payloads.PROJECT_PATH_WITH_NAMESPACE)
        mock_project.web_url = fake_payloads.PROJECT_WEB_URL

        mock_pipe = mock.Mock()
        graph_mr = mock.Mock(gl_project=mock_project,
                             pipelines=[mock_pipe], iid=fake_payloads.MR_IID,
                             head_pipeline_id=fake_payloads.MR_HEAD_PIPELINE_ID)
        mock_mr = mock_project.add_mr(graph_mr.iid)
        graph_mr.gl_mr = mock_mr

        # pipeline_branch_matches() says the branches differ so cancel/retrigger and return True
        mock_get_ds_branch.return_value = '8.8'
        self.assertIs(ckihook.process_possible_branch_change(test_session, graph_mr, payload), True)
        mock_get_ds_branch.assert_called_once_with(mock_instance, mock_pipe)
        mock_cancel.assert_called_once_with(mock_project, fake_payloads.MR_HEAD_PIPELINE_ID)
        mock_create.assert_called_once_with(mock_mr)

    def test_get_downstream_pipeline_branch(self):
        """Returns the 'branch' pipeline var value, or None."""
        pipeline_dict = {'ds_project_id': 23456, 'ds_pipeline_id': 87654}
        mock_pipe = mock.Mock(spec=[pipeline_dict.keys()], **pipeline_dict)
        mock_gl_instance = fakes.FakeGitLab()
        mock_gl_instance.add_project(mock_pipe.ds_project_id, 'group/ds_project')
        ds_project = mock_gl_instance.projects.get(mock_pipe.ds_project_id)

        # No 'branch' in pipeline vars.
        ds_pipeline_vars = [{'key': 'what', 'value': 'huh'}]
        ds_project.add_pipeline(mock_pipe.ds_pipeline_id, variables=ds_pipeline_vars)
        self.assertIs(ckihook.get_downstream_pipeline_branch(mock_gl_instance, mock_pipe), None)

        # A 'branch' in pipeline vars.
        ds_pipeline_vars = [{'key': 'branch', 'value': '8.4'}]
        ds_project.add_pipeline(mock_pipe.ds_pipeline_id, variables=ds_pipeline_vars)
        self.assertEqual(ckihook.get_downstream_pipeline_branch(mock_gl_instance, mock_pipe), '8.4')

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_retry_pipeline(self):
        """Retries pipelines with a prepare_python job in their prepare stage."""
        mock_graphql = mock.Mock()
        mock_pipe = mock.Mock(job_gid='gid://Gitlab/Bridge/123', bridge_name='name')
        ckihook.retry_pipeline(mock_graphql, 1, mock_pipe)
        mock_graphql.client.query.assert_called_once()

    def test_failed_rt_mrs(self):
        """Returns a dict with MR IDs as key and downstream pipeline nodes as values."""
        mock_graphql = mock.Mock()
        namespace = 'group/project'
        branches = ['9.1', '9.1-rt']
        mr1 = {'iid': 1, 'headPipeline': {'jobs': {'nodes': [1, 2, 3]}}}
        mr2 = {'iid': 2, 'headPipeline': {'jobs': {'nodes': [1, 2, 3]}}}
        mock_graphql.client.query.return_value = {'project': {'mrs': {'nodes': [mr1, mr2]}}}
        result = ckihook.failed_rt_mrs(mock_graphql, namespace, branches)
        self.assertEqual(result, {mr1['iid']: mr1['headPipeline']['jobs']['nodes'],
                                  mr2['iid']: mr2['headPipeline']['jobs']['nodes']})

    @mock.patch('webhook.ckihook.failed_rt_mrs')
    @mock.patch('webhook.ckihook.retry_pipeline')
    def test_retrigger_failed_pipelines(self, mock_retry, mock_failed_rt_mrs):
        """Finds failed pipelines and retries them."""
        mock_graphql = mock.Mock()
        namespace = 'group/project'
        branch = mock.Mock(pipelines=[], project=mock.Mock(namespace=namespace))
        branch.name = '9.0-rt'

        # No realtime pipeline for this branch, nothing to do.
        branch.pipelines = [PipelineType.RHEL]
        ckihook.retrigger_failed_pipelines(mock_graphql, branch)
        mock_failed_rt_mrs.assert_not_called()
        mock_retry.assert_not_called()

        # No MRs with failed on merge RT pipelines, nothing to do.
        branch.pipelines = [PipelineType.REALTIME]
        mock_failed_rt_mrs.return_value = {}
        ckihook.retrigger_failed_pipelines(mock_graphql, branch)
        mock_failed_rt_mrs.assert_called_once()
        mock_retry.assert_not_called()

        # Found some MRs, calls retry_pipelines.
        mr1_stages = {'nodes': [{'name': 'build', 'jobs': {'nodes': [{'status': 'FAILED'}]}},
                                {'name': 'test', 'jobs': {'nodes': [{'status': 'PENDING'}]}}]}
        mr2_stages = {'nodes': [{'name': 'merge', 'jobs': {'nodes': [{'status': 'FAILED'}]}},
                                {'name': 'test', 'jobs': {'nodes': [{'status': 'PENDING'}]}}]}
        mrs_data = {1: [{'name': 'c9s_rt_merge_request',
                         'downstreamPipeline': {'status': 'FAILED', 'stages': mr1_stages}}],
                    2: [{'name': 'c9s_rt_merge_request',
                         'downstreamPipeline': {'status': 'FAILED', 'stages': mr2_stages}}]}
        mock_failed_rt_mrs.return_value = mrs_data

        ckihook.retrigger_failed_pipelines(mock_graphql, branch)
        mock_retry.assert_called_once_with(mock_graphql, 2, mock.ANY)

    def test_pipeline_is_waived(self):
        """Returns True if the pipeline is waived, otherwise False."""
        mock_pipe = mock.Mock(type=PipelineType.REALTIME, status=ckihook.PipelineStatus.FAILED)
        self.assertTrue(ckihook.pipeline_is_waived(mock_pipe, ['CKI_RT::Waived']))
        mock_pipe = mock.Mock(type=PipelineType.RHEL, status=ckihook.PipelineStatus.FAILED)
        self.assertFalse(ckihook.pipeline_is_waived(mock_pipe, ['CKI::Waived']))
        mock_pipe = mock.Mock(type=PipelineType.AUTOMOTIVE, status=ckihook.PipelineStatus.FAILED)
        self.assertTrue(ckihook.pipeline_is_waived(mock_pipe, ['CKI_Automotive::Waived']))
        mock_pipe = mock.Mock(type=PipelineType.AUTOMOTIVE, status=ckihook.PipelineStatus.FAILED)
        self.assertFalse(ckihook.pipeline_is_waived(mock_pipe, ['CKI_RT::Waived']))

    @mock.patch('webhook.ckihook.generate_simple_report', mock.Mock())
    @mock.patch('webhook.session.SessionRunner.update_webhook_comment', mock.Mock())
    def test_process_pipe_mr(self):
        """Checks the PipeMR's pipelines and updates the MR."""
        self.response_gl_auth()
        self.response_jira_setup()
        test_session = SessionRunner.new('ckihook', 'args')
        url = fake_payloads.MR_URL
        mock_pipe_mr = mock.Mock(pipelines=[], labels=['CKI_Automotive::Waived'], url=url,
                                 state=ckihook.MrState.OPENED, spec=ckihook.PipeMR)
        mock_pipe_mr.rh_branch = mock.Mock(pipelines=[PipelineType.CENTOS, PipelineType.REALTIME,
                                                      PipelineType.AUTOMOTIVE])
        mock_pipe_mr.gl_project.protectedbranches.list.return_value = []
        # No pipelines found, everything is marked missing.
        ckihook.process_pipe_mr(test_session, mock_pipe_mr)
        mock_pipe_mr.add_labels.assert_called_with(
            {'CKI::Missing', 'CKI_CentOS::Missing', 'CKI_RT::Missing', 'CKI_Automotive::Missing'}
        )
        mock_pipe_mr.remove_labels.assert_called_with({'CKI_Automotive::Waived'})
        # A pipeline to process. CentOS is OK & Automotive is Waived so don't label them,
        # but we still expect RT so it is labeled Missing and overall CKI label is then Missing.
        mock_pipe1 = mock.Mock(type=PipelineType.CENTOS, label=defs.Label('CKI_CentOS::OK'),
                               status=ckihook.PipelineStatus.OK)
        mock_pipe2 = mock.Mock(type=PipelineType.AUTOMOTIVE,
                               label=defs.Label('CKI_Automotive::Failed::merge'),
                               status=ckihook.PipelineStatus.FAILED)
        mock_pipe_mr.pipelines = [mock_pipe1, mock_pipe2]
        ckihook.process_pipe_mr(test_session, mock_pipe_mr)
        mock_pipe_mr.add_labels.assert_called_with({'CKI::Missing', 'CKI_RT::Missing'})

    def test_process_pipe_mr_not_opened(self):
        """Does nothing when the MR is not in 'opened' state."""
        mock_pipe_mr = mock.Mock(state=ckihook.MrState.CLOSED)
        with mock.patch('webhook.ckihook.update_mr') as mock_update_mr:
            ckihook.process_pipe_mr(mock.Mock(), mock_pipe_mr)
            mock_update_mr.assert_not_called()

    def test_process_pipe_mr_unknown_target_branch(self):
        """Does nothing when the MR targets an unknown branch."""
        tb_name = 'weird branch'
        mock_pipe_mr = mock.Mock(state=ckihook.MrState.OPENED, rh_branch=None,
                                 target_branch=tb_name)
        with mock.patch('webhook.ckihook.update_mr') as mock_update_mr:
            ckihook.process_pipe_mr(mock.Mock(), mock_pipe_mr)
            mock_update_mr.assert_not_called()

    def test_generate_pipeline_labels(self):
        """Derives the set of CKI labels expected on the MR from the given data."""
        mock_pipe1 = mock.Mock(type=PipelineType.CENTOS, label=defs.Label('CKI_CentOS::OK'),
                               status=ckihook.PipelineStatus.OK)
        mock_pipe2 = mock.Mock(type=PipelineType.AUTOMOTIVE,
                               label=defs.Label('CKI_Automotive::Failed::merge'),
                               status=ckihook.PipelineStatus.FAILED)
        mock_pipe3 = mock.Mock(type=PipelineType.RHEL_COMPAT, label=defs.Label('CKI_RHEL::OK'),
                               status=ckihook.PipelineStatus.SUCCESS)
        mock_pipe4 = mock.Mock(type=PipelineType.REALTIME, label=defs.Label('CKI_RT::Running'),
                               status=ckihook.PipelineStatus.RUNNING)
        pipe_dict = {PipelineType.RHEL: mock_pipe1,
                     PipelineType.AUTOMOTIVE: mock_pipe2,
                     PipelineType.RHEL_COMPAT: mock_pipe3,
                     PipelineType.REALTIME: mock_pipe4,
                     PipelineType._64K: None}
        mr_cki_labels = [defs.Label('CKI_Automotive::Waived')]
        labels = ckihook.generate_pipeline_labels(mr_cki_labels, pipe_dict)
        self.assertEqual(labels, {'CKI_CentOS::OK', 'CKI_64k::Missing', 'CKI_RT::Running',
                                  'CKI_RHEL::OK', 'CKI_Automotive::Waived'})

    @mock.patch('webhook.session.SessionRunner.update_webhook_comment')
    @mock.patch('webhook.ckihook.generate_simple_report')
    @mock.patch('webhook.ckihook.generate_pipeline_labels')
    @mock.patch('webhook.ckihook.generate_status_label')
    def test_update_mr(self, mock_gen_status_label, mock_gen_pipe_labels, mock_gen_comment,
                       mock_update_comment):
        """Updates labels and MR status comment as needed."""
        self.response_gl_auth()
        self.response_jira_setup()
        test_session = SessionRunner.new('ckihook', 'args')
        mock_gen_comment.return_value = 'Comment string'
        mock_pipelines = mock.Mock()

        # No labels to update
        mock_gen_status_label.return_value = defs.Label('CKI::OK')
        mock_gen_pipe_labels.return_value = {
            defs.Label('CKI_RT::Failed::merge'), defs.Label('CKI_64k::Missing')
        }
        labels = ['CKI::OK', 'Acks::OK', 'CKI_RT::Failed::merge', 'CKI_64k::Missing']
        mock_pipe_mr = mock.Mock(labels=[defs.Label(lbl) for lbl in labels])
        ckihook.update_mr(test_session, mock_pipe_mr, mock_pipelines)
        mock_pipe_mr.add_labels.assert_not_called()

        # Labels to update.
        mock_gen_pipe_labels.return_value = {
            defs.Label('CKI_RT::Failed::merge'), defs.Label('CKI_64k::Missing')
        }
        mock_pipe_mr = mock.Mock(labels=[defs.Label('CKI_64k::Running')])
        ckihook.update_mr(test_session, mock_pipe_mr, mock_pipelines)
        mock_pipe_mr.add_labels.assert_called_once()

    @responses.activate
    @freeze_time("2024-01-01 00:00:00")
    @mock.patch("webhook.session.GitlabGraph.user",
                {'gid': 'gid//gitlab/User/1234', 'name': 'Example User', 'username': 'user1'})
    @mock.patch.dict("os.environ", {"CKI_DEPLOYMENT_ENVIRONMENT": "production"})
    @mock.patch("webhook.ckihook.attempt_to_retry_job", wraps=ckihook.attempt_to_retry_job)
    def test_retry_check_results_or_prepare_python_single_job(self, mocked_attempt_to_retry_job):
        """Test ckihook.retry_check_results_or_prepare_python works with a single job."""
        pipeline_id = 1234567890
        pipeline_iid = 6420
        pipeline_url = f"https://gitlab.com/redhat/linux/-/pipelines/{pipeline_id}"
        mock_session = SessionRunner.new('ckihook', [], ckihook.HANDLERS)
        gitlab_rest = "https://gitlab.com/api/v4"
        group_name = "redhat/linux"

        responses.get(f"{gitlab_rest}/user", json={"id": 1337, "username": "cki-bot"})

        # REST API responds with the "running" pipeline
        responses.get(f"{gitlab_rest}/projects/redhat%2Flinux/pipelines/1234567890", json={
            "id": pipeline_id, "iid": pipeline_iid, "project_id": 18194050, "status": "running",
        })
        with self.subTest("Pipeline status is not failed or success, therefore it didn't finish"):
            with self.assertLogs(logger=ckihook.LOGGER, level="INFO") as log_ctx:
                ckihook.retry_check_results_or_prepare_python(mock_session, pipeline_url, reason="")
            expected_log = (
                f"INFO:{ckihook.LOGGER.name}:"
                f"Skipping retry on pipeline {pipeline_id}"
                f" because it didn't finish (status='running')"
            )
            self.assertIn(expected_log, log_ctx.output)
            mocked_attempt_to_retry_job.assert_not_called()

        mocked_attempt_to_retry_job.reset_mock()
        # REST API responds with the "failed" pipeline
        responses.upsert(
            responses.GET, f"{gitlab_rest}/projects/redhat%2Flinux/pipelines/1234567890", json={
                "id": pipeline_id, "iid": 6420, "project_id": 18194050, "status": "failed",
            })
        # GraphQL API responds without the job
        match_job_query = {
            'operationName': 'allJobs',
            'variables': {
                'namespace': group_name, 'pipeline_iid': pipeline_iid,
            }
        }
        response_data = {"data": {"project": {"pipeline": {
            "retriedJobs": {"nodes": []}, "jobs": {"nodes": []}
            }}}}
        responses.post(
            GITLAB_GRAPHQL,
            match=[json_params_matcher(match_job_query, strict_match=False)],
            json=response_data
        )
        with self.subTest("Check-results job not found"):
            with self.assertLogs(logger=ckihook.LOGGER, level="INFO") as log_ctx:
                ckihook.retry_check_results_or_prepare_python(mock_session, pipeline_url, reason="")
            expected_log = (
                f"INFO:{ckihook.LOGGER.name}:"
                f"Skipping retry on pipeline {pipeline_id}"
                f" because there are no jobs"
            )
            self.assertIn(expected_log, log_ctx.output)
            mocked_attempt_to_retry_job.assert_not_called()

        # NOTE: from now on, we expect the "attempt_to_retry_job" to be called!

        mocked_attempt_to_retry_job.reset_mock()
        # GraphQL API responds with the job, but it's SKIPPED
        job_name = "check-result aarch64"
        response_data["data"]["project"]["pipeline"]["jobs"]["nodes"] = [
            {
                "id": BASE_JOB_GID,
                "name": job_name,
                "status": "SKIPPED",
                "stage": {"name": RETRY_STAGE_NAMES[0]},
            }
        ]
        responses.upsert(
            responses.POST,
            GITLAB_GRAPHQL,
            match=[json_params_matcher(match_job_query, strict_match=False)],
            json=response_data
        )
        with self.subTest("Job status is not failed or success, therefore it didn't finish"):
            with self.assertLogs(logger=ckihook.LOGGER, level="INFO") as log_ctx:
                ckihook.retry_check_results_or_prepare_python(mock_session, pipeline_url, reason="")
            expected_log = (
                f"INFO:{ckihook.LOGGER.name}:"
                f"Skipping {job_name!r} retry on pipeline {pipeline_id}"
                f" because the job didn't finish (status='SKIPPED')"
            )
            self.assertIn(expected_log, log_ctx.output)
            mocked_attempt_to_retry_job.assert_has_calls([
                mock.call(
                    mock_session,
                    response_data["data"]["project"]["pipeline"]["jobs"]["nodes"][0],
                    pipeline_id=pipeline_id,
                    retry_count=0,
                    max_attempts=0,
                    callback=mock.ANY,
                )
            ])

        mocked_attempt_to_retry_job.reset_mock()
        # GraphQL API responds with the "failed" job, but it's not retryable
        response_data["data"]["project"]["pipeline"]["jobs"]["nodes"][0].update({
            "status": "FAILED",
            "retryable": False,
        })
        responses.upsert(
            responses.POST,
            GITLAB_GRAPHQL,
            match=[json_params_matcher(match_job_query, strict_match=False)],
            json=response_data
        )
        with self.subTest("Job is not retryable"):
            with self.assertLogs(logger=ckihook.LOGGER, level="INFO") as log_ctx:
                ckihook.retry_check_results_or_prepare_python(mock_session, pipeline_url, reason="")
            expected_log = (
                f"INFO:{ckihook.LOGGER.name}:"
                f"Skipping {job_name!r} retry on pipeline {pipeline_id}"
                f" because the job was not retryable"
            )
            self.assertIn(expected_log, log_ctx.output)
            mocked_attempt_to_retry_job.assert_has_calls([
                mock.call(
                    mock_session,
                    response_data["data"]["project"]["pipeline"]["jobs"]["nodes"][0],
                    pipeline_id=pipeline_id,
                    retry_count=0,
                    max_attempts=0,
                    callback=mock.ANY,
                )
            ])

        mocked_attempt_to_retry_job.reset_mock()
        # GraphQL API responds with the "failed" and retryable job, but also too many retried jobs
        response_data["data"]["project"]["pipeline"]["jobs"]["nodes"][0]["retryable"] = True
        attempts = 2
        response_data["data"]["project"]["pipeline"]["retriedJobs"] = {
            "nodes": [{"name": job_name} for _ in range(attempts)]
        }
        responses.upsert(
            responses.POST,
            GITLAB_GRAPHQL,
            match=[json_params_matcher(match_job_query, strict_match=False)],
            json=response_data
        )
        # But we're not in prod
        with self.subTest("Exhausted retries"):
            with self.assertLogs(logger=ckihook.LOGGER, level="ERROR") as log_ctx:
                ckihook.retry_check_results_or_prepare_python(
                    mock_session, pipeline_url, reason="", max_attempts=attempts
                )
            expected_log = (
                f"ERROR:{ckihook.LOGGER.name}:"
                f"Skipping {job_name!r} retry on pipeline {pipeline_id}"
                f" because we exhausted retry attempts ({attempts})"
            )
            self.assertIn(expected_log, log_ctx.output)
            mocked_attempt_to_retry_job.assert_has_calls([
                mock.call(
                    mock_session,
                    response_data["data"]["project"]["pipeline"]["jobs"]["nodes"][0],
                    pipeline_id=pipeline_id,
                    retry_count=attempts,
                    max_attempts=attempts,
                    callback=mock.ANY,
                )
            ])

        mocked_attempt_to_retry_job.reset_mock()
        # GraphQL API responds with the "failed" and retryable job, and one shy of too many retries
        response_data["data"]["project"]["pipeline"]["retriedJobs"]["nodes"].pop()
        responses.upsert(
            responses.POST,
            GITLAB_GRAPHQL,
            match=[json_params_matcher(match_job_query, strict_match=False)],
            json=response_data
        )
        # But we're not in prod
        with (
            self.subTest("Not in production: Dry-run"),
            mock.patch.dict("os.environ", {"CKI_DEPLOYMENT_ENVIRONMENT": "test"}),
        ):
            with self.assertLogs(logger=ckihook.LOGGER, level="INFO") as log_ctx:
                ckihook.retry_check_results_or_prepare_python(
                    mock_session, pipeline_url, reason="", max_attempts=attempts
                )
            expected_log = (
                f"INFO:{ckihook.LOGGER.name}:"
                f"Skipping {job_name!r} retry on pipeline {pipeline_id}"
                f" because we're not in production/staging"
            )
            self.assertIn(expected_log, log_ctx.output)
            mocked_attempt_to_retry_job.assert_has_calls([
                mock.call(
                    mock_session,
                    response_data["data"]["project"]["pipeline"]["jobs"]["nodes"][0],
                    pipeline_id=pipeline_id,
                    retry_count=attempts - 1,
                    max_attempts=attempts,
                    callback=mock.ANY,
                )
            ])

        mocked_attempt_to_retry_job.reset_mock()
        # GraphQL API to retry the job
        retry_job_mutation = {"operationName": "retryJob", "variables": {"job_gid": BASE_JOB_GID}}
        job_1_mocked_retry_request = responses.post(
            GITLAB_GRAPHQL,
            match=[json_params_matcher(retry_job_mutation, strict_match=False)],
            json={"data": {"jobRetry": {"job": {"id": BASE_JOB_GID}}}},
        )
        with self.subTest("Successful path"):
            with self.assertLogs(logger=ckihook.LOGGER, level="INFO") as log_ctx:
                ckihook.retry_check_results_or_prepare_python(
                    mock_session, pipeline_url, reason="this is my reason", max_attempts=attempts
                )
            expected_log = (
                f"INFO:{ckihook.LOGGER.name}:"
                f"Retrying {job_name!r} on pipeline {pipeline_id}"
            )
            self.assertIn(expected_log, log_ctx.output)
            mocked_attempt_to_retry_job.assert_has_calls([
                mock.call(
                    mock_session,
                    response_data["data"]["project"]["pipeline"]["jobs"]["nodes"][0],
                    pipeline_id=pipeline_id,
                    retry_count=attempts - 1,
                    max_attempts=attempts,
                    callback=mock.ANY,
                )
            ])

            self.assertEqual(job_1_mocked_retry_request.call_count, 1,
                             "Expected retry mutation to have been called")

            labels = {"project_name": group_name, "reason": "this is my reason"}
            expected_metrics = [
                (labels, 'kwf_ckihook_results_retried_total', 1.0),
                (labels, 'kwf_ckihook_results_retried_created', misc.now_tz_utc().timestamp()),
            ]
            self.assertEqual(expected_metrics, [
                (sample.labels, sample.name, sample.value)
                for metric in ckihook.METRIC_KWF_CKIHOOK_RESULTS_RETRIED.collect()
                for sample in metric.samples
            ])

    @responses.activate
    @freeze_time("2024-01-01 00:00:00")
    @mock.patch(
        "webhook.session.GitlabGraph.user",
        {"gid": "gid//gitlab/User/1234", "name": "Example User", "username": "user1"},
    )
    @mock.patch("webhook.ckihook.attempt_to_retry_job")
    def test_retry_check_results_or_prepare_python_multiple_jobs(self, mocked_attempt_to_retry_job):
        pipeline_id = 1234567890
        pipeline_iid = 6420
        pipeline_url = f"https://gitlab.com/redhat/linux/-/pipelines/{pipeline_id}"
        mock_session = SessionRunner.new("ckihook", [], ckihook.HANDLERS)
        gitlab_rest = "https://gitlab.com/api/v4"
        group_name = "redhat/linux"

        responses.get(f"{gitlab_rest}/user", json={"id": 1337, "username": "cki-bot"})

        # Set REST API respond to a "failed" pipeline
        responses.get(
            f"{gitlab_rest}/projects/redhat%2Flinux/pipelines/1234567890",
            json={"id": pipeline_id, "iid": 6420, "project_id": 18194050, "status": "failed"},
        )
        # GraphQL API responds with a few "check-result" jobs and a "prepare python" job
        match_job_query = {
            "operationName": "allJobs",
            "variables": {
                "namespace": group_name,
                "pipeline_iid": pipeline_iid,
            },
        }
        response_data = {
            "data": {
                "project": {
                    "pipeline": {
                        "retriedJobs": {"nodes": []},
                        "jobs": {"nodes": list(JOB_ASSETS.values())},
                    }
                }
            }
        }
        responses.post(
            GITLAB_GRAPHQL,
            match=[json_params_matcher(match_job_query, strict_match=False)],
            json=response_data,
        )
        with self.subTest("Filtering for kernel-results jobs"):
            with self.assertNoLogs(logger=ckihook.LOGGER, level="INFO"):
                ckihook.retry_check_results_or_prepare_python(mock_session, pipeline_url, reason="")

            expected_calls = [
                mock.call(
                    mock_session,
                    job,
                    pipeline_id=pipeline_id,
                    retry_count=0,
                    max_attempts=0,
                    callback=mock.ANY,
                )
                for job in JOB_ASSETS.values()
                if job["stage"]["name"] in RETRY_STAGE_NAMES
            ]
            mocked_attempt_to_retry_job.assert_has_calls(expected_calls)
            self.assertEqual(
                len(mocked_attempt_to_retry_job.call_args_list),
                4,
                "Expects 4 calls, one for each filtered check-result",
            )

        mocked_attempt_to_retry_job.reset_mock()

        # Check if the retry_count argument is correctly passed for each job based on retriedJobs
        response_data["data"]["project"]["pipeline"]["jobs"]["nodes"] = [
            JOB_ASSETS["check_results1"],
            JOB_ASSETS["check_results2"],
        ]
        response_data["data"]["project"]["pipeline"]["retriedJobs"]["nodes"] = [
            {"name": JOB_ASSETS["check_results1"]["name"]},
            {"name": JOB_ASSETS["check_results2"]["name"]},
            {"name": JOB_ASSETS["check_results2"]["name"]},
        ]
        responses.upsert(
            responses.POST,
            GITLAB_GRAPHQL,
            match=[json_params_matcher(match_job_query, strict_match=False)],
            json=response_data,
        )
        with self.subTest("Correct retry count passed"):
            with self.assertNoLogs(logger=ckihook.LOGGER, level="INFO"):
                ckihook.retry_check_results_or_prepare_python(mock_session, pipeline_url, reason="")

            expected_calls = [
                mock.call(
                    mock_session,
                    JOB_ASSETS["check_results1"],
                    pipeline_id=pipeline_id,
                    retry_count=1,  # Relevant part
                    max_attempts=0,
                    callback=mock.ANY,
                ),
                mock.call(
                    mock_session,
                    JOB_ASSETS["check_results2"],
                    pipeline_id=pipeline_id,
                    retry_count=2,  # Relevant part
                    max_attempts=0,
                    callback=mock.ANY,
                ),
            ]
            mocked_attempt_to_retry_job.assert_has_calls(expected_calls)
            self.assertEqual(
                len(mocked_attempt_to_retry_job.call_args_list),
                2,
                "Expects 2 calls, one for each filtered check-result",
            )
        mocked_attempt_to_retry_job.reset_mock()

        # When there are no "check-result", fallback to "prepare_python"
        response_data["data"]["project"]["pipeline"]["jobs"]["nodes"] = [
            JOB_ASSETS["prepare_python"],
            JOB_ASSETS["random_build"],
        ]
        responses.upsert(
            responses.POST,
            GITLAB_GRAPHQL,
            match=[json_params_matcher(match_job_query, strict_match=False)],
            json=response_data,
        )
        with self.subTest("Fallback to prepare python job"):
            with self.assertNoLogs(logger=ckihook.LOGGER, level="INFO"):
                ckihook.retry_check_results_or_prepare_python(mock_session, pipeline_url, reason="")

            mocked_attempt_to_retry_job.assert_called_once_with(
                mock_session,
                JOB_ASSETS["prepare_python"],
                pipeline_id=pipeline_id,
                retry_count=0,
                max_attempts=0,
                callback=mock.ANY,
            )

    @responses.activate
    @freeze_time("2024-01-01 00:00:00")
    @mock.patch("webhook.session.GitlabGraph.user",
                {'gid': 'gid//gitlab/User/1234', 'name': 'Example User', 'username': 'user1'})
    @mock.patch.dict("os.environ", {"CKI_DEPLOYMENT_ENVIRONMENT": "production"})
    def test_attempt_to_retry_job(self):
        """Test ckihook.attempt_to_retry_job works as expected."""
        pipeline_id = 1234567890
        callback_mock = mock.MagicMock()
        mock_session = SessionRunner.new("ckihook", [], ckihook.HANDLERS)

        with self.subTest("Job skipped because of status"):
            job_data = JOB_ASSETS["check_results_skipped"]
            with self.assertLogs(logger=ckihook.LOGGER, level="INFO") as log_ctx:
                ckihook.attempt_to_retry_job(
                    mock_session,
                    job_data,
                    pipeline_id=pipeline_id,
                    retry_count=0,
                    max_attempts=0,
                    callback=callback_mock
                )

            job_name = job_data["name"]
            job_status = job_data["status"]
            expected_log = (
                f"INFO:{ckihook.LOGGER.name}:"
                f"Skipping '{job_name}' retry on pipeline {pipeline_id} "
                f"because the job didn't finish (status='{job_status}')"
            )
            self.assertIn(expected_log, log_ctx.output)
            callback_mock.assert_not_called()
        with self.subTest("Job skipped because not retriable"):
            job_data = JOB_ASSETS["check_results_not_retryable"]
            with self.assertLogs(logger=ckihook.LOGGER, level="INFO") as log_ctx:
                ckihook.attempt_to_retry_job(
                    mock_session,
                    job_data,
                    pipeline_id=pipeline_id,
                    retry_count=0,
                    max_attempts=0,
                    callback=callback_mock
                )

            job_name = job_data["name"]
            expected_log = (
                f"INFO:{ckihook.LOGGER.name}:"
                f"Skipping '{job_name}' retry on pipeline {pipeline_id} "
                "because the job was not retryable"
            )
            self.assertIn(expected_log, log_ctx.output)
            callback_mock.assert_not_called()
        with self.subTest("Job skipped because it was retried too many times"):
            job_data = JOB_ASSETS["check_results1"]
            with self.assertLogs(logger=ckihook.LOGGER, level="ERROR") as log_ctx:
                ckihook.attempt_to_retry_job(
                    mock_session,
                    job_data,
                    pipeline_id=pipeline_id,
                    retry_count=3,
                    max_attempts=3,
                    callback=callback_mock
                )

            job_name = job_data["name"]
            expected_log = (
                f"ERROR:{ckihook.LOGGER.name}:"
                f"Skipping '{job_name}' retry on pipeline {pipeline_id} "
                "because we exhausted retry attempts (3)"
            )
            self.assertIn(expected_log, log_ctx.output)
            callback_mock.assert_not_called()
        with (
            self.subTest("Not in production: Dry-run"),
            mock.patch.dict("os.environ", {"CKI_DEPLOYMENT_ENVIRONMENT": "test"}),
        ):
            job_data = JOB_ASSETS["check_results1"]
            with self.assertLogs(logger=ckihook.LOGGER, level="INFO") as log_ctx:
                ckihook.attempt_to_retry_job(
                    mock_session,
                    job_data,
                    pipeline_id=pipeline_id,
                    retry_count=0,
                    max_attempts=0,
                    callback=callback_mock
                )

            job_name = job_data["name"]
            expected_log = (
                f"INFO:{ckihook.LOGGER.name}:"
                f"Skipping '{job_name}' retry on pipeline {pipeline_id} "
                "because we're not in production/staging"
            )
            self.assertIn(expected_log, log_ctx.output)
            callback_mock.assert_not_called()

        with self.subTest("Successful path"):
            job_data = JOB_ASSETS["check_results1"]
            retry_job_mutation = {"operationName": "retryJob",
                                  "variables": {"job_gid": job_data["id"]}}
            mocked_retry_request = responses.post(
                GITLAB_GRAPHQL,
                match=[json_params_matcher(retry_job_mutation, strict_match=False)],
                json={"data": {"jobRetry": {"job": {"id": job_data["id"]}}}},
            )

            with self.assertLogs(logger=ckihook.LOGGER, level="INFO") as log_ctx:
                ckihook.attempt_to_retry_job(
                    mock_session,
                    job_data,
                    pipeline_id=pipeline_id,
                    retry_count=0,
                    max_attempts=0,
                    callback=callback_mock
                )

            job_name = job_data["name"]
            expected_log = (
                f"INFO:{ckihook.LOGGER.name}:"
                f"Retrying '{job_name}' on pipeline {pipeline_id}"
            )
            self.assertIn(expected_log, log_ctx.output)
            callback_mock.assert_called()
            self.assertEqual(mocked_retry_request.call_count, 1,
                             "Expected retry mutation to have been called")

    @responses.activate
    @mock.patch.dict(
        'os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml'}
    )
    @mock.patch("webhook.session.GitlabGraph.user",
                {'gid': 'gid//gitlab/User/1234', 'name': 'Example User', 'username': 'user1'})
    @mock.patch("webhook.ckihook.retry_check_results_or_prepare_python")
    @mock.patch("webhook.graphql.GitlabGraph.get_all_mr_labels", mock.Mock(return_value=[]))
    def test_fix_possible_mismatching_status(self, mock_retry_job):
        """Test ckihook.fix_possible_mismatching_status works as expected."""
        fake_payloads.mock_gql_mr_details(params={
            'variables': {
                'namespace': fake_payloads.PROJECT_PATH_WITH_NAMESPACE,
                'mr_id': str(fake_payloads.MR_IID),
            }
        })
        responses.get("https://gitlab.com/api/v4/user", json={"id": 1234, "username": "user1"})
        pipeline_url = fake_payloads.PIPELINE_DOWNSTREAM_DICT["url"]

        mock_session = SessionRunner.new('ckihook', [], ckihook.HANDLERS)

        def create_pipe_mr(event):
            """Return an instance of PipeMR with the given event."""
            pipe_mr = ckihook.PipeMR.new(mock_session, event.mr_url)
            return pipe_mr

        with self.subTest("Event is about an outdated MR pipeline"):
            payload = deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD)
            head_pipeline_id = fake_payloads.MR_HEAD_PIPELINE_ID
            event_mr_pipeline_id = head_pipeline_id - 1
            misc.set_nested_key(payload, 'source_pipeline/pipeline_id', event_mr_pipeline_id)
            event = session_events.GitlabPipelineEvent(mock_session, HEADERS, payload)

            with self.assertLogs(ckihook.LOGGER, "INFO") as log_ctx:
                ckihook.fix_possible_mismatching_status(mock_session, event, create_pipe_mr(event))

            expected_log = (
                f"INFO:{ckihook.LOGGER.name}:"
                f"Skipping 'fix_possible_mismatching_status' on pipeline {event_mr_pipeline_id!r}"
                f" because it's outdated in detriment of {head_pipeline_id!r}"
            )

            self.assertIn(expected_log, log_ctx.output)

            mock_retry_job.assert_not_called()

        with self.subTest("Downstream pipeline failed"):
            payload = deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD)
            payload["object_attributes"]["status"] = "failed"
            event = session_events.GitlabPipelineEvent(mock_session, HEADERS, payload)

            with self.assertLogs(logger=ckihook.LOGGER, level="DEBUG") as log_ctx:
                ckihook.fix_possible_mismatching_status(mock_session, event, create_pipe_mr(event))

            expected_log = (
                f"DEBUG:{ckihook.LOGGER.name}:"
                f"Skipping 'fix_possible_mismatching_status' on pipeline {pipeline_url!r}"
                " because the downstream pipeline didn't succeed"
            )
            self.assertIn(expected_log, log_ctx.output)

            mock_retry_job.assert_not_called()

        # GraphQL API responds with two trigger jobs:
        # c9s_merge_request:FAILED; c9s_rt_merge_request:SUCCESS
        fake_payloads.mock_gql_mr_pipelines(params={
            'variables': {
                'namespace': fake_payloads.PROJECT_PATH_WITH_NAMESPACE,
                'mr_id': str(fake_payloads.MR_IID),
            }
        })

        with self.subTest("Trigger job not found"):
            trigger_job_name = 'unfindable_trigger_job_name'

            payload = deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD)
            payload['object_attributes']['variables'] = [
                fake_payloads.VAR_MR_URL,
                fake_payloads.VAR_PROJECT_ID,
                {'key': 'trigger_job_name', 'value': trigger_job_name}
            ]
            event = session_events.GitlabPipelineEvent(mock_session, HEADERS, payload)

            with self.assertLogs(logger=ckihook.LOGGER, level="WARNING") as log_ctx:
                ckihook.fix_possible_mismatching_status(mock_session, event, create_pipe_mr(event))

            expected_log = (
                f"WARNING:{ckihook.LOGGER.name}:"
                f"Skipping 'fix_possible_mismatching_status' on pipeline {pipeline_url!r}"
                " because failed to find the trigger job in ["
                "<Pipeline 'c9s_rt_merge_request' (REALTIME), ds ID: 1196303600, status: OK>, "
                "<Pipeline 'c9s_merge_request' (CENTOS), ds ID: 1196303538, status: OK>]"
            )
            self.assertIn(expected_log, log_ctx.output)
            mock_retry_job.assert_not_called()

        with self.subTest("Trigger job didn't fail"):
            trigger_job_name = "c9s_rt_merge_request"
            payload = deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD)
            payload['object_attributes']['variables'] = [
                fake_payloads.VAR_MR_URL,
                fake_payloads.VAR_PROJECT_ID,
                {'key': 'trigger_job_name', 'value': trigger_job_name}
            ]
            event = session_events.GitlabPipelineEvent(mock_session, HEADERS, payload)
            with self.assertLogs(logger=ckihook.LOGGER, level="DEBUG") as log_ctx:
                ckihook.fix_possible_mismatching_status(mock_session, event, create_pipe_mr(event))

            expected_log = (
                f"DEBUG:{ckihook.LOGGER.name}:"
                f"Skipping 'fix_possible_mismatching_status' on pipeline {pipeline_url!r}"
                " because the trigger job didn't fail (status='OK')"
            )
            self.assertIn(expected_log, log_ctx.output)

            mock_retry_job.assert_not_called()

        with self.subTest("Mismatching upstream-downstream status (bug gitlab#340064)"):
            payload = deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD)
            event = session_events.GitlabPipelineEvent(mock_session, HEADERS, payload)

            with self.assertLogs(logger=ckihook.LOGGER, level="ERROR") as log_ctx:
                ckihook.fix_possible_mismatching_status(mock_session, event, create_pipe_mr(event))

            expected_log = (
                f"ERROR:{ckihook.LOGGER.name}:"
                "Mismatching status ('FAILED' != 'OK')"
                f" between upstream trigger job and downstream pipeline: {pipeline_url},"
                " tentatively retrying an idempotent job."
            )
            self.assertIn(expected_log, log_ctx.output)

            mock_retry_job.assert_called_once_with(
                mock_session, pipeline_url=pipeline_url, reason="fix_possible_mismatching_status",
                max_attempts=ckihook.MAX_RETRIES_MISMATCHING_STATUS)


class TestStatusLabel(KwfTestCase):
    """Tests for the generate_status_label function."""

    class StatusLabelTest(typing.NamedTuple):

        expected_label: defs.Label
        pipelines: dict[PipelineType, ckihook.PipelineResult | None]
        current_labels: list[defs.Label]
        branch_pipeline_types: list[PipelineType]

    def run_label_test(self, test: StatusLabelTest) -> None:
        """Runs the StatusLabelTest."""
        self.assertEqual(
            test.expected_label,
            ckihook.generate_status_label(
                test.pipelines,
                test.current_labels,
                test.branch_pipeline_types
            )
        )

    @staticmethod
    def create_pipeline_result(
        pipe_type: PipelineType,
        pipe_status: ckihook.PipelineStatus = ckihook.PipelineStatus.SUCCESS,
        allow_failure: bool = False
    ) -> mock.Mock:
        """Create a mock PipelineResult."""
        pipeline_result_spec = ['allow_failure', 'status', 'type']
        return mock.Mock(
            spec_set=pipeline_result_spec,
            allow_failure=allow_failure,
            status=pipe_status,
            type=pipe_type
        )

    def test_no_input(self) -> None:
        """Returns a CKI::Missing label."""
        self.run_label_test(
            self.StatusLabelTest(
                expected_label=defs.Label('CKI::Missing'),
                pipelines={},
                current_labels=[],
                branch_pipeline_types=[]
            )
        )

    def test_successful_input(self) -> None:
        """Returns a CKI::OK label."""
        pipelines = [
            self.create_pipeline_result(PipelineType.RHEL),
            self.create_pipeline_result(PipelineType.REALTIME),
        ]

        self.run_label_test(
            self.StatusLabelTest(
                expected_label=defs.Label('CKI::OK'),
                pipelines={pipe.type: pipe for pipe in pipelines},
                current_labels=[],
                branch_pipeline_types=[]
            )
        )

    def test_lowest_input(self) -> None:
        """Returns a CKI::Running label and ignores allow_failure result."""
        pipelines = [
            self.create_pipeline_result(PipelineType.RHEL),
            self.create_pipeline_result(
                PipelineType.AUTOMOTIVE,
                pipe_status=ckihook.PipelineStatus.RUNNING
            ),
            self.create_pipeline_result(
                PipelineType.ELN,
                pipe_status=ckihook.PipelineStatus.FAILED,
                allow_failure=True
            ),
            self.create_pipeline_result(PipelineType.REALTIME),
        ]

        self.run_label_test(
            self.StatusLabelTest(
                expected_label=defs.Label('CKI::Running'),
                pipelines={pipe.type: pipe for pipe in pipelines},
                current_labels=[],
                branch_pipeline_types=[]
            )
        )

    def test_waived(self) -> None:
        """Returns a CKI::OK label with failed realtime because it is waived.."""
        pipelines = [
            self.create_pipeline_result(PipelineType.RHEL),
            self.create_pipeline_result(
                PipelineType.REALTIME,
                pipe_status=ckihook.PipelineStatus.FAILED
            ),
        ]

        # Reflects the failure in the non-waived case.
        current_labels = []

        self.run_label_test(
            self.StatusLabelTest(
                expected_label=defs.Label('CKI::Failed'),
                pipelines={pipe.type: pipe for pipe in pipelines},
                current_labels=current_labels,
                branch_pipeline_types=[]
            )
        )

        # Ignores the failure in the waived case.
        current_labels = [defs.Label('CKI_RT::Waived')]

        self.run_label_test(
            self.StatusLabelTest(
                expected_label=defs.Label('CKI::OK'),
                pipelines={pipe.type: pipe for pipe in pipelines},
                current_labels=current_labels,
                branch_pipeline_types=[]
            )
        )

    def test_missing_branch(self) -> None:
        """Returns a CKI::Missing label when not all expected PipelineTypes are present."""
        pipelines = [
            self.create_pipeline_result(PipelineType.RHEL),
        ]

        branch_pipeline_types = [PipelineType.RAWHIDE]

        self.run_label_test(
            self.StatusLabelTest(
                expected_label=defs.Label('CKI::Missing'),
                pipelines={pipe.type: pipe for pipe in pipelines},
                current_labels=[],
                branch_pipeline_types=branch_pipeline_types
            )
        )


class TestCommentCode(KwfTestCase):
    """Tests for the comment generating bits."""

    OK_PIPE = {'bridge_name': 'rhel10_merge_request',
               'ds_url': 'https://gitlab.com/g/p/-/pipelines/1',
               'label': 'CKI_RHEL::OK',
               'status': ckihook.PipelineStatus.OK,
               'type': PipelineType.RHEL}

    FAILED_PIPE = {'bridge_name': 'c9s_rt_merge_request',
                   'ds_url': 'https://gitlab.com/g/p/-/pipelines/1',
                   'label': 'CKI_RT::Failed::merge',
                   'status': ckihook.PipelineStatus.FAILED,
                   'type': PipelineType.REALTIME}

    CANCELED_PIPE = {'bridge_name': 'c9s_merge_request',
                     'ds_url': 'https://gitlab.com/g/p/-/pipelines/1',
                     'label': 'CKI_CentOS::Canceled',
                     'status': ckihook.PipelineStatus.CANCELED,
                     'type': PipelineType.CENTOS}

    RUNNING_PIPE = {'bridge_name': 'rawhide_merge_request',
                    'ds_url': 'https://gitlab.com/g/p/-/pipelines/1',
                    'label': 'CKI_Rawhide::Running',
                    'status': ckihook.PipelineStatus.RUNNING,
                    'type': PipelineType.RAWHIDE}

    def test_generate_comment_boot_failures(self):
        """Returns a markdown string reporting all pipeline results."""
        self.response_gql_user_data()
        fake_payloads.mock_gql_mr_details(rsps=self.responses)
        fake_payloads.mock_gql_mr_labels(rsps=self.responses)
        # override the default mock with a failed downstream pipeline
        fake_payloads.mock_gql_mr_pipelines({"project": {"mr": {"headPipeline": {"jobs": {"nodes": [
            {
                "allowFailure": False,
                "id": "gid://gitlab/Ci::Bridge/6292206554",
                "name": "c9s_rt_merge_request",
                "createdAt": "2024-02-29T20:58:21Z",
                "pipeline": fake_payloads.HEAD_PIPELINE_DICT,
                "status": "FAILED",
                "downstreamPipeline": {
                    "id": "gid://gitlab/Ci::Pipeline/1196303600",
                    "project": fake_payloads.PROJECT_DICT,
                    "status": "FAILED",
                    "stages": fake_payloads.PIPELINES_MIXIN_RESPONSE_STAGES_NODES
                }
            },
        ]}}}}}, rsps=self.responses)
        self.responses.get("https://gitlab.com/api/v4/user", json={"id": 1234, "username": "bot"})
        self.responses.get('http://localhost/api/1/kcidb/checkouts/redhat:1196303600',
                           json={"id": "redhat:1196303600", "misc": {"iid": 132228}})
        self.responses.get('http://localhost/api/1/kcidb/checkouts/redhat:1196303600/all', json={
            "checkouts": [{
                "origin": "redhat", "id": "redhat:1196303600", "valid": True,
                "misc": {"iid": 132228},
            }],
            "builds": [{
                "origin": "redhat", "id": "redhat:1196303600-x86_64-kernel-rt",
                "checkout_id": "redhat:1196303600",
                "architecture": "x86_64", "valid": True, "misc": {"iid": 475869}
            }, {
                "origin": "redhat", "id": "redhat:1196303600-aarch64-kernel-rt",
                "checkout_id": "redhat:1196303600",
                "architecture": "aarch64", "valid": True, "misc": {"iid": 475870}
            }],
            "tests": [{
                "id": "redhat:1196303600-x86_64-kernel-rt_upt_4",
                "build_id": "redhat:1196303600-x86_64-kernel-rt",
                "comment": "Boot test", "status": "ERROR", "waived": False,
            }, {
                "id": "redhat:1196303600-aarch64-kernel-rt_upt_4",
                "build_id": "redhat:1196303600-aarch64-kernel-rt",
                "comment": "Boot test", "status": "ERROR", "waived": False,
            }],
            "testresults": [],
            "issueoccurrences": []
        })
        test_session = SessionRunner.new('ckihook', 'args')
        pipe_mr = ckihook.PipeMR.new(test_session, fake_payloads.MR_URL)

        pipelines = {pipe.type: pipe for pipe in pipe_mr.pipelines}
        comment_str = ckihook.generate_simple_report(pipelines, pipe_mr.labels, {})

        self.assertIn(ckihook.BOOT_FAILURE_HEADER, comment_str)
        self.assertIn(ckihook.REPORT_HEADER_BAD, comment_str)
        self.assertNotIn(ckihook.REPORT_HEADER_GOOD, comment_str)

        self.assertIn(
            '[aarch64](http://localhost/kcidb/builds/redhat:1196303600-aarch64-kernel-rt)',
            comment_str
        )
        self.assertIn(
            '[x86_64](http://localhost/kcidb/builds/redhat:1196303600-x86_64-kernel-rt)',
            comment_str
        )


class TestMRHandler(KwfTestCase):
    """Tests for the MR event handler."""

    @mock.patch.object(ckihook.PipeMR, 'new')
    @mock.patch('webhook.ckihook.process_pipe_mr')
    @mock.patch('webhook.ckihook.process_possible_branch_change', mock.Mock(return_value=False))
    def test_process_mr_event_label_changed(self, mock_process_pipe_mr, mock_pipemr):
        """Calls process_pipe_mr with the result of get_pipe_mr."""
        mock_session = mock.Mock()
        payload = deepcopy(fake_payloads.MR_PAYLOAD)
        payload['changes'] = deepcopy(CHANGES)
        payload['changes']['merge_status'] = 'pending'
        event = create_event(mock_session, HEADERS, payload)
        ckihook.process_gl_event({}, mock_session, event)
        mock_process_pipe_mr.assert_called_once_with(mock_session, mock_pipemr.return_value)


class TestPipelineHandler(KwfTestCase):
    """Tests for the Pipeline event handler."""

    @responses.activate
    @mock.patch("webhook.session.GitlabGraph.user",
                {'gid': 'gid//gitlab/User/1234', 'name': 'Example User', 'username': 'user1'})
    @mock.patch.object(ckihook.PipeMR, 'new')
    @mock.patch('webhook.ckihook.process_pipe_mr')
    @mock.patch("webhook.ckihook.fix_possible_mismatching_status")
    def test_process_pipeline_event_run_process_pipe(self, mock_fixer, mock_process_pipe_mr,
                                                     mock_pipemr):
        """Test process_gl_event with event.kind == PIPELINE works as expected."""
        responses.get("https://gitlab.com/api/v4/user", json={"id": 1234, "username": "user1"})
        mock_session = SessionRunner.new('ckihook', [], ckihook.HANDLERS)

        with self.subTest("Not downstream pipeline"):
            payload = deepcopy(fake_payloads.PIPELINE_PAYLOAD)
            event = create_event(mock_session, HEADERS, payload)

            ckihook.process_gl_event({}, mock_session, event)

            mock_fixer.assert_not_called()
            mock_process_pipe_mr.assert_called_once_with(mock_session, mock_pipemr.return_value)

        mock_process_pipe_mr.reset_mock()

        with self.subTest("Downstream pipeline"):
            payload = deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD)
            event = create_event(mock_session, HEADERS, payload)

            ckihook.process_gl_event({}, mock_session, event)

            mock_fixer.assert_called_once_with(mock_session, event, mock_pipemr.return_value)
            mock_process_pipe_mr.assert_called_once_with(mock_session, mock_pipemr.return_value)


class TestNoteHandler(KwfTestCase):
    """Tests for the Note event handler."""

    @mock.patch.object(ckihook.PipeMR, 'new')
    @mock.patch('webhook.ckihook.process_pipe_mr')
    def test_process_note_event(self, mock_process_pipe_mr, mock_pipemr):
        """Runs compute_new_labels and optionally add_labels if there is a proper request."""
        # No request, does nothing.
        payload = deepcopy(fake_payloads.NOTE_PAYLOAD)
        mock_session = mock.Mock()
        event = create_event(mock_session, HEADERS, payload)
        ckihook.process_gl_event({}, mock_session, event)
        mock_process_pipe_mr.assert_called_once_with(mock_session, mock_pipemr.return_value)


class TestPushHandler(KwfTestCase):
    """Tests for the Push event handler."""

    @mock.patch.dict(
        'os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml'}
    )
    @mock.patch('webhook.ckihook.retrigger_failed_pipelines')
    def test_process_push_event(self, mock_retrigger):
        """Checks the target_branch for RT and possibly retriggers pipelines."""
        body = deepcopy(fake_payloads.PUSH_PAYLOAD)
        body['project']['path_with_namespace'] = 'redhat/rhel/src/kernel/rhel-8'
        mock_session = SessionRunner.new('ckihook', [], ckihook.HANDLERS)
        mock_session.get_graphql = mock.Mock()

        # Branch is not RT, nothing to do.
        event = create_event(mock_session, HEADERS, body)
        ckihook.process_push_event({}, mock_session, event)
        mock_retrigger.assert_not_called()

        # Branch is recognized and RT, away we go...
        body['ref'] = 'refs/head/8.5-rt'
        event = create_event(mock_session, HEADERS, body)
        ckihook.process_push_event({}, mock_session, event)
        mock_retrigger.assert_called_once()


class TestDataWarehouseHandler(KwfTestCase):
    """Test handler for MessageType.DATAWAREHOUSE."""

    @mock.patch("webhook.ckihook.retry_check_results_or_prepare_python")
    def test_process_datawarehouse_event(self, mocked_retry_check_results_or_prepare_python):
        """Test ckihook.process_datawarehouse_event works as expected."""
        body = deepcopy(fake_payloads.DATAWAREHOUSE_PAYLOAD)
        mock_session = SessionRunner.new('ckihook', [], ckihook.HANDLERS)

        # Expects to use url from the provenance with service_name == "gitlab"
        pipeline_url = body["object"]["misc"]["provenance"][1]["url"]
        with self.subTest("Event processed successfully"):
            event = create_event(mock_session, DATAWAREHOUSE_HEADERS, body)
            ckihook.process_datawarehouse_event({}, mock_session, event)
            mocked_retry_check_results_or_prepare_python.assert_called_once_with(
                mock_session, pipeline_url, reason=body["status"]
            )

        mocked_retry_check_results_or_prepare_python.reset_mock()

        with self.subTest("Message has no pipeline_url. Can't process"):
            # Remove provenance from message
            del body["object"]["misc"]["provenance"]
            event = create_event(mock_session, DATAWAREHOUSE_HEADERS, body)
            with self.assertLogs(logger=ckihook.LOGGER, level="INFO") as log_ctx:
                ckihook.process_datawarehouse_event({}, mock_session, event)
            mocked_retry_check_results_or_prepare_python.assert_not_called()

            expected_log = (
                f"INFO:{ckihook.LOGGER.name}:"
                f"Nothing to be done to event {event!r}. Checkout has no pipeline url"
            )
            self.assertIn(expected_log, log_ctx.output)
