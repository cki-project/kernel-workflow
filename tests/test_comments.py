"""Tests for the webhook.comments module."""
import datetime
import os
import typing
from typing import Optional
from unittest import mock

from tests.helpers import KwfTestCase
from webhook import comments
from webhook.users import User

if typing.TYPE_CHECKING:
    from webhook.session import BaseSession


C9S_4612_MR_URL = ('https://gitlab.com/redhat/centos-stream/'
                   'src/kernel/centos-stream-9/-/merge_requests/4612')

C9S_4612_BOT_USERNAME = 'cki-kwf-bot'

C9S_4612_GET_MR_COMMENTS_QUERY = KwfTestCase.load_yaml_asset(
    path='mr_comments-c9s-4612.json',
    sub_module='gitlab_graphql_api'
)

C9S_4612_MR_GID = C9S_4612_GET_MR_COMMENTS_QUERY['data']['project']['mr']['id']

C9S_4612_RAW_COMMENTS = C9S_4612_GET_MR_COMMENTS_QUERY['data']['project']['mr']['notes']['nodes']


class TestComment(KwfTestCase):
    """Tests for the webhook.comments.Comment class."""

    COMMENT_WITH_FOOTER = """**Mergeability Summary:** ~"Merge::OK"\n\nThis MR can be merged cleanly to its target branch.\n\n---\n\n<small>Created 2024-06-26 15:00 UTC by mergehook - [KWF FAQ](https://red.ht/kernel_workflow_doc) - [Slack #team-kernel-workflow](https://redhat-internal.slack.com/archives/C04LRUPMJQ5) - [Source](https://gitlab.com/cki-project/kernel-workflow/-/blob/main/webhook/mergehook.py) - [Documentation](https://gitlab.com/cki-project/kernel-workflow/-/blob/main/docs/README.mergehook.md) - [Report an issue](https://gitlab.com/cki-project/kernel-workflow/-/issues/new?issue%5Btitle%5D=mergehook%20webhook%20issue)</small>"""  # noqa: E501

    COMMENT_WITHOUT_FOOTER = """**Mergeability Summary:** ~"Merge::OK"\n\nThis MR can be merged cleanly to its target branch."""  # noqa: E501

    def test_without_footer(self) -> None:
        """Returns the string with footer removed if found, or the original string."""
        # Make sure the test footer strings have the expected bits.
        self.assertTrue(self.COMMENT_WITH_FOOTER.startswith(self.COMMENT_WITH_FOOTER))
        self.assertIn(comments.Comment.CommentBody.FOOTER_PREFIX, self.COMMENT_WITH_FOOTER)
        self.assertNotIn(comments.Comment.CommentBody.FOOTER_PREFIX, self.COMMENT_WITHOUT_FOOTER)

        # without_footer returns the CommentBody string with the footer removed.
        self.assertEqual(
            comments.Comment.CommentBody(self.COMMENT_WITH_FOOTER).without_footer,
            self.COMMENT_WITHOUT_FOOTER
        )
        # without_footer returns the full string when no footer is found.
        self.assertEqual(
            comments.Comment.CommentBody(self.COMMENT_WITHOUT_FOOTER).without_footer,
            self.COMMENT_WITHOUT_FOOTER
        )

    @classmethod
    def make_comment(
        cls,
        index: int,
        session: Optional['BaseSession'] = None,
        namespace: str = 'group/project'
    ) -> comments.Comment:
        """Construct a Comment from the C9S_4612_RAW_COMMENTS data with the given index."""
        session = session or cls.base_session()
        return comments.Comment.new(C9S_4612_RAW_COMMENTS[index], session.get_user_cache(namespace))

    def test_new(self) -> None:
        """Constructs a new Comment instance from the input data."""
        comment = self.make_comment(0)

        # Make sure everything is of the expected type.
        self.assertIsInstance(comment.author, User)
        self.assertIsInstance(comment.body, comments.Comment.CommentBody)
        self.assertIsInstance(comment.id, comments.GitlabGID)
        self.assertIsInstance(comment.created_at, datetime.datetime)
        self.assertIsInstance(comment.updated_at, datetime.datetime)

        # Make sure the timestamps have tzinfo.
        self.assertIsNotNone(comment.created_at.tzinfo)
        self.assertIsNotNone(comment.updated_at.tzinfo)

        # Make sure we have the expected values.
        self.assertEqual(comment.author.username, 'cki-kwf-bot')
        self.assertEqual(comment.id, 'gid://gitlab/Note/1969002433')
        self.assertIn('JIRA Hook Readiness Report', comment.body)

    def test_new_comments(self) -> None:
        """Constructs a list of Comment instances from the input data."""
        session = self.base_session()
        user_cache = session.get_user_cache('group/namespace')
        comments_list = comments.Comment.new_comments(C9S_4612_RAW_COMMENTS, user_cache)

        self.assertEqual(len(comments_list), 11)
        for comment in comments_list:
            self.assertIsInstance(comment, comments.Comment)

    def test_matches(self) -> None:
        """Returns True when the comment matches the input."""
        class MatchesTest(typing.NamedTuple):
            match_expected: bool
            comment_index: int
            usernames: list[str]
            substring: str

        tests = [
            MatchesTest(
                match_expected=True,
                comment_index=0,
                usernames=['example_user', 'cki-kwf-bot'],
                substring='JIRA Hook Readiness Report'
            ),
            MatchesTest(
                match_expected=True,
                comment_index=1,
                usernames=['cki-kwf-bot'],
                substring='Fixes Status'
            ),
            MatchesTest(
                match_expected=True,
                comment_index=2,
                usernames=['cki-kwf-bot'],
                substring='DCO Signoff Check Report'
            ),
            MatchesTest(
                match_expected=False,
                comment_index=3,
                usernames=['cki-kwf-bot'],
                substring='DCO Signoff Check Report'
            ),
            MatchesTest(
                match_expected=True,
                comment_index=4,
                usernames=['cki-kwf-bot'],
                substring='ACK/NACK Summary'
            ),
            MatchesTest(
                match_expected=True,
                comment_index=5,
                usernames=['cki-kwf-bot'],
                substring='Upstream Config Differences'
            ),
            MatchesTest(
                match_expected=True,
                comment_index=6,
                usernames=['cki-kwf-bot'],
                substring='Upstream Commit ID Readiness Report'
            ),
            MatchesTest(
                match_expected=True,
                comment_index=7,
                usernames=['cki-kwf-bot'],
                substring='Mergeability Summary'
            ),
            MatchesTest(
                match_expected=True,
                comment_index=8,
                usernames=['cki-kwf-bot'],
                substring='CKI Pipelines Status'
            ),
            MatchesTest(
                match_expected=False,
                comment_index=9,
                usernames=['cki-kwf-bot'],
                substring='CKI Pipelines Status'
            ),
        ]

        for test in tests:
            comment = self.make_comment(test.comment_index)
            with self.subTest(comment=comment, **test._asdict()):
                self.assertIs(
                    comment.matches(test.usernames, test.substring),
                    test.match_expected
                )

    def test_update(self) -> None:
        """Does the updateNote mutation and updates the body & updated_at attribute values."""
        update_text = 'this is an updated note.'
        updated_iso_timestamp = '2024-06-27T11:12:00.583815+00:00'

        mutation_return_value = {
            'body': update_text,
            'id': 'gid://gitlab/Note/123',
            'updatedAt': updated_iso_timestamp
        }

        mock_graphql = mock.Mock(spec_set=['update_note'])
        mock_graphql.update_note.return_value = mutation_return_value

        # Make a comment and set the 'body' to be some known value.
        comment = self.make_comment(3)
        comment.body = comment.CommentBody(self.COMMENT_WITHOUT_FOOTER)
        self.assertEqual(comment.body, self.COMMENT_WITHOUT_FOOTER)

        # Update the comment and confirm the body value has changed.
        comment.update(mock_graphql, update_text)

        self.assertIsInstance(comment.body, comments.Comment.CommentBody)
        self.assertEqual(comment.body, update_text)
        self.assertEqual(comment.updated_at.isoformat(), updated_iso_timestamp)


class TestCommentCache(KwfTestCase):
    """Tests for the webhook.comment.CommentCache class."""

    def test_init(self) -> None:
        """Constructs a new instance with the expected values set."""
        # If no bot_usernames are given then the list is made to contain our logged in username.
        mock_session = mock.Mock(spec_set=['graphql'])
        mock_session.graphql = mock.Mock(spec_set=['username'], username='example_user')

        comment_cache = comments.CommentCache(mock_session)

        self.assertIs(comment_cache.session, mock_session)
        self.assertDictEqual(comment_cache.data, {})
        self.assertDictEqual(comment_cache.mr_id_map, {})
        self.assertEqual(comment_cache.bot_usernames, ['example_user'])

    def test_current_webhook(self) -> None:
        """Returns the name of the session instance webhook or raises if it doesn't exist."""
        # Works with a SessionRunner instance as that has a webhooks attribute.
        session_runner = self.session_runner('signoff')
        comment_cache = comments.CommentCache(session_runner, bot_usernames=['bot_user'])

        self.assertEqual(comment_cache.current_webhook.name, 'signoff')

        # Does not work with a BaseSession instance as that does not have a webhooks attribute.
        base_session = self.base_session()
        comment_cache = comments.CommentCache(base_session, bot_usernames=['bot_user'])

        with self.assertRaises(RuntimeError):
            comment_cache.current_webhook

    def test_get_mr_comment_existing(self) -> None:
        """Returns the matching comment."""
        base_session = self.base_session()
        comment_cache = comments.CommentCache(base_session, bot_usernames=[C9S_4612_BOT_USERNAME])

        # Set up some entries in the cache.
        comment_cache._add_cache_entry(C9S_4612_MR_URL, C9S_4612_MR_GID, C9S_4612_RAW_COMMENTS)
        self.assertIn(C9S_4612_MR_URL, comment_cache.data)
        self.assertEqual(len(comment_cache.data[C9S_4612_MR_URL]), 9,
                         msg='The number of webhook entries expected for MR 4612.')

        expected_hook_comments = (
            'ack_nack',
            'ckihook',
            'commit_compare',
            'configshook',
            'fixes',
            'jirahook',
            'mergehook',
            'signoff',
        )

        # Use get_mr_comment to get comments we expect to be in the cache.
        comment_cache.cache_mr_comments = mock.Mock(spec_set=[])
        for hook_name in expected_hook_comments:
            comment_cache.cache_mr_comments.reset_mock()
            with self.subTest(hook_name=hook_name):
                comment = comment_cache.get_webhook_comment(C9S_4612_MR_URL, hook_name)
                comment_cache.cache_mr_comments.assert_not_called()
                self.assertIsNotNone(comment,
                                     msg='This hook is expected to have a comment in the cache.')

    def test_get_mr_comment_with_query(self) -> None:
        """Uses a query to get comment data for an unknown MR."""
        self.response_gql_user_data()
        base_session = self.base_session()
        comment_cache = comments.CommentCache(base_session, bot_usernames=[C9S_4612_BOT_USERNAME])
        self.assertDictEqual(comment_cache.data, {})

        # Patch GitlabGraph.get_mr_comment so it returns us some comments.
        mock_get_mr_comment = mock.Mock(return_value=(C9S_4612_MR_GID, C9S_4612_RAW_COMMENTS))
        comment_cache.session.graphql.get_mr_comments = mock_get_mr_comment

        # Ask for the 'signoff' comment and confirm get_mr_comments ran and updated the cache.
        result = comment_cache.get_webhook_comment(C9S_4612_MR_URL, 'signoff')
        self.assertIsInstance(result, comments.Comment)
        mock_get_mr_comment.assert_called_once()
        self.assertEqual(len(comment_cache.data[C9S_4612_MR_URL]), 9,
                         msg='The number of webhook entries expected for MR 4612.')

    def test_update_comment_creates_comment(self) -> None:
        """Creates a comment when no existing one is found."""
        self.response_gql_user_data()
        base_session = self.base_session()
        comment_cache = comments.CommentCache(base_session, bot_usernames=[C9S_4612_BOT_USERNAME])

        # Test with the signoff hook :shrug:.
        test_webhook = base_session.rh_projects.webhooks['signoff']
        test_url = 'https://gitlab.com/group/project/-/merge_requests/123'
        test_label = 'Signoff::OK'
        test_message = 'Good job!'
        test_gid = 'gid://gitlab/MergeRequest/425342535'

        # We have to inject the test_gid into the mr_id_map as we're patching out
        # get_webhook_comment which would normally take care of this.
        comment_cache.mr_id_map[test_url] = test_gid
        # Same for the test_url's entry in the data dict.
        comment_cache.data[test_url] = {}

        # Make sure get_webhook_comment doesn't return anything so we create a new comment.
        mock_get_webhook_comment = mock.Mock(return_value=None)
        comment_cache.get_webhook_comment = mock_get_webhook_comment

        # Make sure we do this in non-production mode so GitlabGraph.create_note doesn't do any
        # real API calls.
        env = {'CKI_DEPLOYMENT_ENVIRONMENT': 'development'}
        with mock.patch.dict(os.environ, env, clear=True):
            self.assertIs(
                comment_cache.update_comment(test_url, test_label, test_message, test_webhook),
                True,
                msg='update_comment is expected to return True.'
            )
