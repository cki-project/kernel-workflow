"""Webhook interaction tests."""
from copy import deepcopy
from datetime import datetime
from datetime import timedelta
import os
from random import uniform
import re
import typing
from unittest import mock

from gitlab.client import Gitlab
from gitlab.exceptions import GitlabAuthenticationError
from gitlab.exceptions import GitlabCreateError
from gitlab.exceptions import GitlabGetError

from tests import fake_payloads
from tests import fakes
from tests import fakes_jira
from tests.helpers import KwfTestCase
from webhook import common
from webhook import defs
from webhook.rh_metadata import Projects

NEEDS_REVIEW_LABEL_COLOR = '#FF0000'
NEEDS_TESTING_LABEL_COLOR = '#CAC542'
READY_LABEL_COLOR = '#428BCA'
MERGE_LABEL_COLOR = '#8BCA42'


class MyLister:
    def __init__(self, labels):
        self.labels = labels
        self.call_count = 0
        self.called_with_all = None

    def list(self, search=None, iterator=False):
        self.call_count += 1
        if iterator:
            self.called_with_all = True
            return self.labels
        self.called_with_all = False
        if not search:
            raise AttributeError
        return [label for label in self.labels if label.name == search]


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestCommon(KwfTestCase):
    PROJECT_LABELS = [{'id': 1,
                       'name': 'readyForMerge',
                       'color': MERGE_LABEL_COLOR,
                       'description': ('All automated checks pass, this merge request'
                                       ' should be suitable for inclusion in main now.'),
                       'text_color': '#FFFFFF',
                       'priority': 1
                       },
                      {'id': 2,
                       'name': 'readyForQA',
                       'color': NEEDS_TESTING_LABEL_COLOR,
                       'description': ('This merge request has been explicitly Nacked'
                                       ' by Red Hat engineering.'),
                       'text_color': '#FFFFFF',
                       'priority': 12
                       },
                      {'id': 3,
                       'name': 'Acks::NACKed',
                       'color': NEEDS_REVIEW_LABEL_COLOR,
                       'description': ('This merge request has been explicitly Nacked'
                                       ' by Red Hat engineering.'),
                       'text_color': '#FFFFFF',
                       'priority': 12
                       },
                      {'id': 4,
                       'name': 'Acks::NeedsReview',
                       'color': NEEDS_REVIEW_LABEL_COLOR,
                       'description': ('This merge request needs more reviews and acks'
                                       ' from Red Hat engineering.'),
                       'text_color': '#FFFFFF',
                       'priority': 3
                       },
                      {'id': 5,
                       'name': 'Acks::OK',
                       'color': READY_LABEL_COLOR,
                       'description': ('This merge request has been reviewed by Red Hat'
                                       ' engineering and approved for inclusion.'),
                       'text_color': '#FFFFFF',
                       'priority': 4
                       },
                      {'id': 6,
                       'name': 'Bugzilla::OK',
                       'color': READY_LABEL_COLOR,
                       'description': ("This merge request's bugzillas are approved for"
                                       " integration"),
                       'text_color': '#FFFFFF',
                       'priority': 4
                       },
                      {'id': 7,
                       'name': 'Bugzilla::NeedsTesting',
                       'color': NEEDS_TESTING_LABEL_COLOR,
                       'description': "This merge request's bugzillas are ready for QA testing.",
                       'text_color': '#FFFFFF',
                       'priority': 4
                       },
                      {'id': 8,
                       'name': 'ExternalCI::lnst::NeedsTesting',
                       'color': NEEDS_TESTING_LABEL_COLOR,
                       'description': ('The subsystem-required lnst testing has not yet'
                                       ' been completed.'),
                       'text_color': '#FFFFFF',
                       'priority': 12
                       },
                      {'id': 9,
                       'name': 'ExternalCI::lnst::OK',
                       'color': READY_LABEL_COLOR,
                       'text_color': '#FFFFFF',
                       'priority': 12
                       },
                      {'id': 10,
                       'name': 'Dependencies::abcdef012345',
                       'color': '#123456',
                       'description': 'This MR has dependencies.',
                       'text_color': '#FFFFFF',
                       'priority': 15
                       },
                      {'id': 11,
                       'name': 'Acks::net::NeedsReview',
                       'color': NEEDS_TESTING_LABEL_COLOR,
                       'descroption': 'Subsystem needs Acks'
                       },
                      {'id': 12,
                       'name': 'Merge::OK',
                       'color': READY_LABEL_COLOR,
                       'description': ("This merge request merges cleanly into it's "
                                       "target branch."),
                       'text_color': '#FFFFFF',
                       'priority': 4
                       },
                      {'id': 13,
                       'name': 'Merge::Conflicts',
                       'color': NEEDS_REVIEW_LABEL_COLOR,
                       'description': ("This Merge Request can not be merged cleanly into "
                                       "it's target branch, due to conflicts with other "
                                       "recently merged code."),
                       'text_color': '#FFFFFF',
                       'priority': 4
                       },
                      {'id': 14,
                       'name': 'JIRA::OK',
                       'color': READY_LABEL_COLOR,
                       'description': ("This merge request's jira issues are approved for"
                                       " integration"),
                       'text_color': '#FFFFFF',
                       'priority': 4
                       },
                      {'id': 15,
                       'name': 'JIRA::InProgress',
                       'color': NEEDS_TESTING_LABEL_COLOR,
                       'description': "This merge request's jira issues are ready for QA testing.",
                       'text_color': '#FFFFFF',
                       'priority': 4
                       },
                      {'id': 16,
                       'name': 'CKI_RHEL::Failed::test',
                       'color': NEEDS_TESTING_LABEL_COLOR,
                       'description': 'Failed CKI testing'
                       }]

    MAPPINGS = {'subsystems':
                [{'subsystem': 'MEMORY MANAGEMENT',
                  'labels': {'name': 'mm'},
                  'paths': {'includes': ['include/linux/mm.h', 'include/linux/vmalloc.h', 'mm/']}
                  },
                 {'subsystem': 'NETWORKING',
                  'labels': {'name': 'net',
                             'readyForMergeDeps': ['lnst']},
                  'paths': {'includes': ['include/linux/net.h', 'include/net/', 'net/']},
                  },
                 {'subsystem': 'XDP',
                  'labels': {'name': 'xdp'},
                  'paths': {'includes': ['kernel/bpf/devmap.c', 'include/net/page_pool.h'],
                            'includeRegexes': ['xdp']}
                  }]
                }

    FILE_CONTENTS = (
        '---\n'
        'subsystems:\n'
        ' - subsystem: MEMORY MANAGEMENT\n'
        '   labels:\n'
        '     name: mm\n'
        '   paths:\n'
        '       includes:\n'
        '          - include/linux/mm.h\n'
        '          - include/linux/vmalloc.h\n'
        '          - mm/\n'
        ' - subsystem: NETWORKING\n'
        '   labels:\n'
        '     name: net\n'
        '     readyForMergeDeps:\n'
        '       - lnst\n'
        '   paths:\n'
        '       includes:\n'
        '          - include/linux/net.h\n'
        '          - include/net/\n'
        '          - net/\n'
        ' - subsystem: XDP\n'
        '   labels:\n'
        '     name: xdp\n'
        '   paths:\n'
        '       includes:\n'
        '          - kernel/bpf/devmap.c\n'
        '          - include/net/page_pool.h\n'
        '       includeRegexes:\n'
        '          - xdp\n'
    )

    YAML_LABELS = [{'name': 'Acks::OK', 'color': '#428BCA', 'description': 'all good'},
                   {'name': 'Bugzilla::OnQA', 'color': '#CAC542', 'description': 'maybe'},
                   {'name': '^Subsystem:(\\w+)$',
                    'color': '#778899',
                    'description': 'hi %s',
                    'regex': True}
                   ]

    def test_mr_is_closed(self):
        mrequest = mock.Mock()
        mrequest.state = "closed"
        status = common.mr_is_closed(mrequest)
        self.assertTrue(status)
        mrequest.state = "opened"
        status = common.mr_is_closed(mrequest)
        self.assertFalse(status)

    def test_build_note_string(self):
        notes = []
        notes += ["1"]
        notes += ["2"]
        notes += ["3"]
        notestring = common.build_note_string(notes)
        self.assertEqual(notestring, "See 1, 2, 3|\n")

    def test_build_commits_for_row(self):
        commits = ["abcdef012345"]
        table = [["012345abcdef", commits, 1, "", ""]]
        for row in table:
            commits = common.build_commits_for_row(row)
            self.assertEqual(commits, ["abcdef01"])

    def test_mr_action_affects_commits(self):
        """Check handling of a merge request with unwanted actions."""
        # Missing 'action'
        payload = {'object_attributes': {}}
        self.assertEqual(common.mr_action_affects_commits(payload), None)

        # 'open' action should return True.
        payload = {'object_attributes': {'action': 'open'}}
        self.assertTrue(common.mr_action_affects_commits(payload))

        # 'update' action with oldrev set should return True.
        payload = {'object_attributes': {'action': 'update',
                                         'oldrev': 'hi'}
                   }
        self.assertTrue(common.mr_action_affects_commits(payload))

        # 'update' action without oldrev should return False and log it.
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            payload = {'object_attributes': {'action': 'update'}}
            self.assertFalse(common.mr_action_affects_commits(payload))
            self.assertIn("Ignoring MR \'update\' action without an oldrev.", logs.output[-1])

        # Action other than 'new' or 'update' should return False and log it.
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            payload = {'object_attributes': {'action': 'partyhard'}}
            self.assertFalse(common.mr_action_affects_commits(payload))
            self.assertIn("Ignoring MR action 'partyhard'", logs.output[-1])

        # Target branch changed should return True
        payload = {'object_attributes': {'action': 'update'},
                   'changes': {'merge_status': {'previous': 'can_be_merged',
                                                'current': 'unchecked'}}}
        self.assertTrue(common.mr_action_affects_commits(payload))

    def test_required_label_removed(self):
        payload = {'object_attributes': {'action': 'update'}}
        testing = defs.NEEDS_TESTING_SUFFIX
        ready = defs.READY_SUFFIX
        failed = defs.TESTING_FAILED_SUFFIX
        waived = defs.TESTING_WAIVED_SUFFIX

        changed_labels = [f'ExternalCI::lnst::{testing}', f'whatever::{ready}']
        result = common.required_label_removed(payload, testing, changed_labels)
        self.assertEqual(result, True)

        changed_labels = ['Drivers:bonding']
        result = common.required_label_removed(payload, testing, changed_labels)
        self.assertEqual(result, False)

        changed_labels = [f'ExternalCI::rdmalab::{ready}']
        result = common.required_label_removed(payload, ready, changed_labels)
        self.assertEqual(result, True)

        changed_labels = [f'Acks::{ready}']
        result = common.required_label_removed(payload, ready, changed_labels)
        self.assertEqual(result, False)

        changed_labels = [f'ExternalCI::lnst::{failed}']
        result = common.required_label_removed(payload, ready, changed_labels)
        self.assertEqual(result, False)

        changed_labels = [f'ExternalCI::lnst::{failed}']
        result = common.required_label_removed(payload, failed, changed_labels)
        self.assertEqual(result, True)

        changed_labels = [f'ExternalCI::lnst::{waived}']
        result = common.required_label_removed(payload, waived, changed_labels)
        self.assertEqual(result, True)

    def test_has_label_changed(self):
        """Returns True if the label name has changed."""
        # No labels key in changes
        changes = {}
        self.assertFalse(common.has_label_changed(changes, 'my_cool_label'))

        # Label has not changed.
        labels = {'previous': [{'title': 'my_cool_label'}],
                  'current': [{'title': 'my_cool_label'}]
                  }
        changes['labels'] = labels
        self.assertFalse(common.has_label_changed(changes, 'my_cool_label'))

        # Label has changed.
        labels = {'previous': [{'title': 'my_cool_label'}],
                  'current': [{'title': 'a different label'}]
                  }
        changes['labels'] = labels
        self.assertTrue(common.has_label_changed(changes, 'my_cool_label'))

    def test_has_label_prefix_changed(self):
        """Returns True if label matched on prefix and changed, or False."""
        # No labels key in changes
        changes = {}
        self.assertFalse(common.has_label_prefix_changed(changes, 'Acks::'))

        # Label has not changed.
        changes = {}
        labels = {'previous': [{'title': 'Acks::OK'}],
                  'current': [{'title': 'Acks::OK'}]
                  }
        changes['labels'] = labels
        self.assertFalse(common.has_label_prefix_changed(changes, 'Acks::'))

        # Label has changed.
        labels = {'previous': [{'title': 'Acks::NeedsReview'}],
                  'current': [{'title': 'Acks::OK'}]
                  }
        changes['labels'] = labels
        self.assertTrue(common.has_label_prefix_changed(changes, 'Acks::'))

    def test_get_payload_username(self):
        """Returns the username from the payload."""
        payload = {'object_kind': 'merge_request',
                   'user': {'username': 'steve'}}
        self.assertEqual(common.get_payload_username(payload), 'steve')
        payload = {'object_kind': 'push',
                   'user_username': 'bill'}
        self.assertEqual(common.get_payload_username(payload), 'bill')

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_update_bot_approval_status(self):
        mock_aby = mock.Mock(spec_set=['approved_by'])
        mock_aby.approved_by = []
        mock_mr = mock.Mock()
        mock_mr.iid = 678
        mock_mr.approvals.get.return_value = mock_aby
        mock_proj = mock.Mock()
        mock_proj.name = 'Testing'
        mock_proj.manager.gitlab.user.username = 'cki-kwf-bot'
        mock_proj.mergerequests.get.return_value = mock_mr
        common._update_bot_approval_status(mock_proj, 678, approved=True)
        mock_mr.approve.assert_called_once()
        mock_bot = {'user': {'username': 'cki-kwf-bot'}}
        mock_aby.approved_by = [mock_bot]
        common._update_bot_approval_status(mock_proj, 678, approved=False)
        mock_mr.unapprove.assert_called_once()

    @mock.patch('webhook.libjira.fetch_issues')
    @mock.patch('webhook.libjira.update_issue_field')
    def test_set_release_milestone_values(self, mock_update, mock_fetch):
        mock_gl_mr = mock.Mock()
        mock_gl_mr.draft = True
        mock_issue = self.make_jira_issue('RHEL-101')

        common.set_release_milestone_values(mock_gl_mr, [])
        mock_fetch.assert_not_called()
        mock_update.assert_not_called()

        mock_gl_mr.draft = False
        mock_gl_mr.description = "There is no jira"
        common.set_release_milestone_values(mock_gl_mr, [])
        mock_fetch.assert_not_called()
        mock_update.assert_not_called()

        mock_gl_mr.description = "JIRA: https://issues.redhat.com/browse/RHEL-101"
        mock_fetch.return_value = []
        common.set_release_milestone_values(mock_gl_mr, [])
        mock_fetch.assert_called_once()
        mock_update.assert_not_called()

        mock_fetch.call_count = 0
        mock_fetch.return_value = [mock_issue]
        boxes = [{'id': defs.RELEASE_MILESTONE_BOXES['mr_approved']}]
        common.set_release_milestone_values(mock_gl_mr, boxes)
        mock_fetch.assert_called_once()
        mock_update.assert_called_with(mock_issue, defs.JiraField.Release_Milestone, boxes)

    @mock.patch('webhook.common.set_release_milestone_values')
    def test_update_jira_release_milestone_field(self, mock_set):
        mock_gl_mr = mock.Mock()
        test_labels = []
        common._update_jira_release_milestone_field(mock_gl_mr, test_labels)
        mock_set.assert_called_with(mock_gl_mr, [])

        test_labels = defs.SUBMITTER_DEPENDENCIES
        common._update_jira_release_milestone_field(mock_gl_mr, test_labels)
        mock_set.assert_called_with(mock_gl_mr,
                                    [{'id': defs.RELEASE_MILESTONE_BOXES['submitter_checks_pass']}])

        test_labels = ['CKI::Failed']
        common._update_jira_release_milestone_field(mock_gl_mr, test_labels)
        mock_set.assert_called_with(mock_gl_mr,
                                    [{'id': defs.RELEASE_MILESTONE_BOXES['cki_tests_done']}])

        test_labels = ['CKI::Waived']
        common._update_jira_release_milestone_field(mock_gl_mr, test_labels)
        mock_set.assert_called_with(mock_gl_mr,
                                    [{'id': defs.RELEASE_MILESTONE_BOXES['cki_tests_done']},
                                     {'id': defs.RELEASE_MILESTONE_BOXES['cki_tests_pass']}])
        test_labels = ['CKI::OK']
        common._update_jira_release_milestone_field(mock_gl_mr, test_labels)
        mock_set.assert_called_with(mock_gl_mr,
                                    [{'id': defs.RELEASE_MILESTONE_BOXES['cki_tests_done']},
                                     {'id': defs.RELEASE_MILESTONE_BOXES['cki_tests_pass']}])

        test_labels = ['Acks::OK']
        common._update_jira_release_milestone_field(mock_gl_mr, test_labels)
        mock_set.assert_called_with(mock_gl_mr,
                                    [{'id': defs.RELEASE_MILESTONE_BOXES['mr_approved']}])

        test_labels = defs.SUBMITTER_DEPENDENCIES + ['CKI::OK', 'Acks::OK']
        common._update_jira_release_milestone_field(mock_gl_mr, test_labels)
        mock_set.assert_called_with(mock_gl_mr,
                                    [{'id': defs.RELEASE_MILESTONE_BOXES['submitter_checks_pass']},
                                     {'id': defs.RELEASE_MILESTONE_BOXES['cki_tests_done']},
                                     {'id': defs.RELEASE_MILESTONE_BOXES['cki_tests_pass']},
                                     {'id': defs.RELEASE_MILESTONE_BOXES['mr_approved']}])

    @mock.patch('webhook.common.validate_labels')
    @mock.patch('cki_lib.misc.is_production', return_value=True)
    def _test_plabel_commands(self, mr_labels, new_labels, expected_cmd, is_prod, mock_validate,
                              branch='main'):
        label_names = [label['name'] for label in new_labels]
        mock_validate.return_value = new_labels
        label_objects = []
        for label in self.PROJECT_LABELS:
            label_obj = mock.Mock(name=label['name'])
            label_obj.save = mock.Mock()
            for key in label:
                setattr(label_obj, key, label[key])
            label_objects.append(label_obj)

        gl_mergerequest = mock.Mock(target_branch=branch)
        gl_mergerequest.iid = 2
        gl_mergerequest.labels = [label['name'] for label in mr_labels]
        gl_mergerequest.notes.create = mock.Mock()

        gl_project = mock.Mock()
        gl_project.labels = MyLister(label_objects)
        gl_project.labels.create = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest

        mock_bridge = mock.Mock(allow_failure=False)
        mock_bridge.name = 'rhel9_merge_request'
        if branch == 'main-rt':
            mock_bridge.name = 'rhel9_realtime_check_merge_request'
        elif branch == 'main-automotive':
            mock_bridge.name = 'c9s_automotive_merge_request'
        mock_bridges = mock.Mock()
        mock_bridges.list.return_value = [mock_bridge]
        gl_project.pipelines.get.return_value = mock.Mock(bridges=mock_bridges)

        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            label_ret = common.add_label_to_merge_request(
                gl_project, 2, label_names, level='project')
            self.assertTrue(isinstance(label_ret, list))

            # If there are more than 5 labels we should have used one API call to labels.list().
            labels_called_with_all = None
            labels_call_count = 0
            if len(new_labels) >= 5 and expected_cmd:
                labels_called_with_all = True
                labels_call_count = 1
            elif len(new_labels) <= 5 and expected_cmd:
                labels_called_with_all = False
                labels_call_count = expected_cmd.count('/label')
            self.assertEqual(gl_project.labels.called_with_all, labels_called_with_all)
            self.assertEqual(gl_project.labels.call_count, labels_call_count)

            # If label existed on the project, ensure it was edited if needed.
            for label in new_labels:
                pl_obj = \
                    next((pl for pl in label_objects if pl.name == label['name']), None)
                pl_dict = \
                    next((pl for pl in self.PROJECT_LABELS if pl['name'] == label['name']), None)
                if pl_obj and not (label.items() <= pl_dict.items()):
                    self.assertRegex(' '.join(logs.output),
                                     f"Editing label {re.escape(label['name'])}.*mock.path")
                    pl_obj.save.assert_called_once()
            # If new label didn't exist on the project, confirm it was added.
            call_list = []
            for label in new_labels:
                if not [plabel for plabel in self.PROJECT_LABELS
                        if plabel['name'] == label['name']]:
                    label_string = ("{\'name\': \'%s\', \'color\': \'%s\',"
                                    " \'description\': \'%s\'}") \
                        % (label['name'], label['color'], label['description'])
                    self.assertRegex(' '.join(logs.output),
                                     f"Creating label {re.escape(label_string)}.*mock.path")
                    call_list.append(mock.call(label))
            if call_list:
                gl_project.labels.create.assert_has_calls(call_list)
            else:
                gl_project.labels.create.assert_not_called()

        gl_project.mergerequests.get.assert_called_with(2)

        if expected_cmd:
            gl_mergerequest.notes.create.assert_called_with({'body': expected_cmd})
        else:
            gl_mergerequest.notes.create.assert_not_called()

    @mock.patch('webhook.common._update_jira_state')
    @mock.patch('cki_lib.misc.is_production', return_value=True)
    @mock.patch('webhook.common._update_jira_release_milestone_field', mock.Mock())
    @mock.patch('webhook.common._update_bot_approval_status', mock.Mock())
    def _test_label_commands(self, mr_labels, new_labels, expected_cmd, is_prod,
                             mock_update_jiras, remove_scoped=False, draft=False, project_id=123):
        label_names = [label['name'] for label in new_labels]
        label_objects = []
        for label in self.PROJECT_LABELS:
            label_obj = mock.Mock(name=label['name'])
            label_obj.save = mock.Mock()
            for key in label:
                setattr(label_obj, key, label[key])
            label_objects.append(label_obj)

        gl_instance = mock.Mock()
        rh_group = mock.Mock(id=10810393)
        gl_instance.groups.list.return_value = [rh_group]
        gl_group = mock.Mock()
        gl_instance.groups.get.return_value = gl_group

        mock_aby = mock.Mock(spec_set=['approved_by'])
        mock_aby.approved_by = []
        gl_mergerequest = mock.Mock(iid=2, draft=draft)
        gl_mergerequest.labels = [label['name'] for label in mr_labels]
        gl_mergerequest.notes.create = mock.Mock()
        gl_mergerequest.approvals.get.return_value = mock_aby

        gl_project = mock.Mock()
        gl_project.id = project_id
        gl_project.manager.gitlab = gl_instance
        gl_project.labels = MyLister(label_objects)
        gl_project.labels.create = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest
        gl_project.namespace = mock.MagicMock(id=1, full_path="redhat/rhel/src/kernel")

        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            label_ret = common.add_label_to_merge_request(
                gl_project, 2, label_names, remove_scoped=remove_scoped)
            self.assertTrue(isinstance(label_ret, list))

            # If there are more than 5 labels we should have used one API call to labels.list().
            labels_called_with_all = None
            labels_call_count = 0
            if len(new_labels) >= 5 and expected_cmd:
                labels_called_with_all = True
                labels_call_count = 1
            elif len(new_labels) <= 5 and expected_cmd:
                labels_called_with_all = False
                labels_call_count = expected_cmd.count('/label')
            self.assertEqual(gl_project.labels.called_with_all, labels_called_with_all)
            self.assertEqual(gl_project.labels.call_count, labels_call_count)

            # If label existed on the project, ensure it was edited if needed.
            for label in new_labels:
                pl_obj = \
                    next((pl for pl in label_objects if pl.name == label['name']), None)
                pl_dict = \
                    next((pl for pl in self.PROJECT_LABELS if pl['name'] == label['name']), None)
                if pl_obj and not (label.items() <= pl_dict.items()):
                    self.assertRegex(' '.join(logs.output),
                                     f"Editing label {re.escape(label['name'])}.*groups.get")
                    pl_obj.save.assert_called_once()
            # If new label didn't exist on the project, confirm it was added.
            call_list = []
            for label in new_labels:
                if not [plabel for plabel in self.PROJECT_LABELS
                        if plabel['name'] == label['name']]:
                    label_string = ("{\'name\': \'%s\', \'color\': \'%s\',"
                                    " \'description\': \'%s\'}") \
                        % (label['name'], label['color'], label['description'])
                    self.assertRegex(' '.join(logs.output),
                                     f"Creating label {re.escape(label_string)}.*groups.get")
                    call_list.append(mock.call(label))
            if call_list:
                gl_group.labels.create.assert_has_calls(call_list)
            else:
                gl_group.labels.create.assert_not_called()

        gl_project.mergerequests.get.assert_called_with(2)

        if expected_cmd:
            gl_mergerequest.notes.create.assert_called_with({'body': expected_cmd})
        else:
            gl_mergerequest.notes.create.assert_not_called()

    @mock.patch('webhook.common._compute_mr_status_labels')
    @mock.patch('webhook.common._add_label_quick_actions', return_value=['/label "hello"'])
    @mock.patch('webhook.common._filter_mr_labels')
    def test_add_label_exception(self, mock_filter, mock_quickaction, mock_compute):
        mergerequest = mock.Mock()
        mergerequest.iid = 10
        project = mock.Mock()
        project.mergerequests.get.return_value = mergerequest
        mock_filter.return_value = ([], [])
        label_list = ['Acks::OK']

        # Catch "can't be blank" and move on.
        err_text = "400 Bad request - Note {:note=>[\"can't be blank\"]}"
        mergerequest.notes.create.side_effect = GitlabCreateError(error_message=err_text,
                                                                  response_code=400,
                                                                  response_body='')
        exception_raised = False
        with self.assertLogs('cki.webhook.common', level='ERROR') as logs:
            with mock.patch('cki_lib.misc.is_production', return_value=True):
                try:
                    result = common.add_label_to_merge_request(project, mergerequest.iid,
                                                               label_list)
                except GitlabCreateError:
                    exception_raised = True
                self.assertTrue(result)
                self.assertFalse(exception_raised)
                self.assertIn("can't be blank", logs.output[-1])

        # For any other error let it bubble up.
        mergerequest.notes.create.side_effect = GitlabCreateError(error_message='oops',
                                                                  response_code=403,
                                                                  response_body='')
        exception_raised = False
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            try:
                common.add_label_to_merge_request(project, mergerequest.iid, label_list)
            except GitlabCreateError:
                exception_raised = True
        self.assertTrue(exception_raised)

    def test_add_plabel_to_merge_request(self):
        # Add a project label which does not exist on the project.
        new_labels = [{'name': 'Dependencies::abcdef012345', 'color': '#123456',
                       'description': 'This MR has dependencies.'}]
        self._test_plabel_commands([], new_labels, '/label "Dependencies::abcdef012345"')

        # Add scoped Deps::OK::sha label
        new_labels = [{'name': 'Dependencies::OK::abcdef012345', 'color': '#123456',
                       'description': 'This MR has dependencies.'}]
        self._test_plabel_commands([], new_labels, '/label "Dependencies::OK::abcdef012345"')

        # Label already exists on MR, nothing to do.
        mr_labels = [{'name': 'Dependencies::abcdef012345', 'color': '#123456',
                      'description': 'This MR has dependencies.'}]
        new_labels = [{'name': 'Dependencies::abcdef012345', 'color': '#123456',
                       'description': 'This MR has dependencies.'}]
        self._test_plabel_commands(mr_labels, new_labels, None)

    @mock.patch('cki_lib.misc.is_production', return_value=True)
    def test_create_group_milestone(self, prod):
        gl_group = mock.Mock()
        product = "Red Hat Enterprise Linux 10"
        rhmeta = mock.Mock()
        rhmeta.internal_target_release = ""
        rhmeta.zstream_target_release = ""
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            new_ms = common._create_group_milestone(gl_group, product, rhmeta)
            self.assertEqual(new_ms, None)
            self.assertIn("Unable to find release for milestone", " ".join(logs.output))

        rhmeta.internal_target_release = "10.1.0"
        rhmeta.milestone = "RHEL-10.1.0"
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            common._create_group_milestone(gl_group, product, rhmeta)
            self.assertIn("Creating new milestone for", " ".join(logs.output))
            gl_group.milestones.create.assert_called_once()

        prod.return_value = False
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            new_ms = common._create_group_milestone(gl_group, product, rhmeta)
            self.assertEqual(new_ms, None)
            self.assertIn("Would have created milestone for", " ".join(logs.output))

    @mock.patch.dict(
        'os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml'}
    )
    @mock.patch('webhook.common._get_gitlab_group')
    @mock.patch('cki_lib.misc.is_production', return_value=True)
    def test_add_merge_request_to_milestone(self, is_prod, gglg):
        mock_instance = fakes.FakeGitLab()
        mock_project = mock_instance.add_project(12345, 'mock_project')
        mock_mr = mock_project.add_mr(123, attributes={'target_branch': 'main'})
        mock_project.manager.gitlab = mock_instance
        mock_instance.add_group('mock_group')
        mock_group = mock_instance.groups.get('mock_group')
        gglg.return_value = mock_group
        target_milestone = mock.Mock()
        target_milestone.title = 'RHEL-8.7.0'
        target_milestone.id = 6060606
        mock_group.milestones.list.return_value = [target_milestone]
        mock_mr.milestone_id = 6060606
        mock_mr.milestone = {'title': 'RHEL-8.7.0', 'id': 6060606}
        mock_mr.references = {'full': 'path/to/project!666'}
        projects = Projects()

        # Branch has no milestone, nothing to do.
        branch = projects.projects[12345].branches[6]
        common.add_merge_request_to_milestone(branch, mock_project, mock_mr)
        mock_group.milestones.list.assert_not_called()

        # MR is already assigned to the correct Milestone
        branch = projects.projects[12345].branches[0]
        common.add_merge_request_to_milestone(branch, mock_project, mock_mr)
        mock_mr.save.assert_not_called()

        # MR will be assigned to an existing Milestone
        branch = projects.projects[12345].branches[0]
        mock_mr.milestone_id = 0
        mock_mr.milestone = None
        common.add_merge_request_to_milestone(branch, mock_project, mock_mr)
        self.assertEqual(mock_mr.milestone_id, target_milestone.id)
        mock_mr.save.assert_called_once()

        # MR will be assigned to a newly created Milestone
        mock_mr.milestone_id = 0
        target_milestone.title = 'RHEL-8.7.0'
        mock_mr.save.call_count = 0
        branch = projects.projects[12345].branches[2]
        common.add_merge_request_to_milestone(branch, mock_project, mock_mr)
        mock_group.milestones.create.assert_called_once()
        mock_mr.save.assert_called_once()

    def test_add_label_to_merge_request(self):
        # Add a label which already exists on the project.
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands([], new_labels, '/label "Acks::OK"')

        # Existing project label with new color.
        new_labels = [{'name': 'Acks::OK', 'color': '#123456'}]
        self._test_label_commands([], new_labels, '/label "Acks::OK"')

        # Label already exists on MR, nothing to do.
        mr_labels = [{'name': 'Acks::OK'},
                     {'name': 'CommitRefs::OK'}
                     ]
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands(mr_labels, new_labels, None)

        # Add a label which does not exist.
        new_labels = [{'name': 'Subsystem:net', 'color': '#778899',
                       'description': 'An MR that affects code related to net.'}]
        self._test_label_commands(mr_labels, new_labels, '/label "Subsystem:net"')

        # Add a label which triggers readyForMerge being added to the MR.
        mr_labels = [{'name': 'CKI::OK'},
                     {'name': 'CommitRefs::OK'},
                     {'name': 'Dependencies::OK'},
                     {'name': 'ExternalCI::OK'},
                     {'name': 'JIRA::OK'},
                     {'name': 'Signoff::OK'},
                     {'name': 'Merge::OK'},
                     {'name': 'readyForQA'}
                     ]
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands(mr_labels, new_labels,
                                  ('/label "Acks::OK"\n/label "readyForMerge"\n'
                                   '/unlabel "readyForQA"'))

        # Add a label which triggers readyForMerge being added to the MR for kernel-ark.
        mr_labels = [{'name': 'CKI::OK'}, {'name': 'Merge::OK'}, {'name': 'Signoff::OK'}]
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands(mr_labels, new_labels,
                                  ('/label "Acks::OK"\n/label "readyForMerge"\n'
                                   '/unlabel "readyForQA"'), project_id=defs.ARK_PROJECT_ID)

        # Add a label which triggers readyForMerge being added to the MR - JIRA variant,
        # with merged dependencies
        mr_labels = [{'name': 'CKI::OK'},
                     {'name': 'CommitRefs::OK'},
                     {'name': 'Dependencies::OK::12345678'},
                     {'name': 'ExternalCI::OK'},
                     {'name': 'JIRA::OK'},
                     {'name': 'Signoff::OK'},
                     {'name': 'Merge::OK'},
                     {'name': 'readyForQA'}
                     ]
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands(mr_labels, new_labels,
                                  ('/label "Acks::OK"\n/label "readyForMerge"\n'
                                   '/unlabel "readyForQA"'))

        # Add a label which doesn't trigger readyForMerge being added to the MR due to 'Blocked'
        # label.
        mr_labels = [{'name': 'CKI::OK'},
                     {'name': 'CommitRefs::OK'},
                     {'name': 'Dependencies::OK::12345678'},
                     {'name': 'ExternalCI::OK'},
                     {'name': 'JIRA::OK'},
                     {'name': 'Signoff::OK'},
                     {'name': 'Merge::OK'},
                     {'name': 'readyForQA'},
                     {'name': defs.BLOCKED_LABEL}
                     ]
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands(mr_labels, new_labels,
                                  ('/label "Acks::OK"\n'
                                   '/unlabel "readyForMerge"\n/unlabel "readyForQA"'))

        # Add a label which triggers readyForMerge being removed from the MR.
        mr_labels += [{'name': 'Acks::OK'}, {'name': 'readyForMerge'}]
        new_labels = [{'name': 'Acks::NeedsReview'}]
        expected_note = ['/label "Acks::NeedsReview"',
                         '/unlabel "Acks::OK"',
                         '/unlabel "readyForMerge"',
                         '/unlabel "readyForQA"'
                         ]
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note))

        # Add labels which trigger readyForQA, jira variant.
        mr_labels = [{'name': 'Acks::NeedsReview'},
                     {'name': 'CKI::OK'},
                     {'name': 'CommitRefs::OK'},
                     {'name': 'Dependencies::OK'},
                     {'name': 'Signoff::OK'},
                     {'name': 'Merge::OK'},
                     {'name': 'readyForMerge'}
                     ]
        new_labels = [{'name': 'Acks::OK'}, {'name': 'JIRA::InProgress'}]
        expected_note = ['/label "Acks::OK"',
                         '/label "JIRA::InProgress"',
                         '/unlabel "Acks::NeedsReview"',
                         '/label "readyForQA"',
                         '/unlabel "readyForMerge"'
                         ]
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note))

        # Add a double scoped label.
        new_labels = [{'name': 'CKI_RHEL::Failed::test', 'color': '#FF0000',
                       'description': "This MR\'s latest CKI pipeline failed."}]
        expected_note = ['/label "CKI_RHEL::Failed::test"',
                         '/unlabel "readyForMerge"',
                         '/unlabel "readyForQA"']
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note))

        # Change a single scoped label and ensure related double-scoped are not removed too.
        mr_labels = [{'name': 'Acks::OK'}, {'name': 'CommitRefs::OK'},
                     {'name': 'ExternalCI::lnst::NeedsTesting'}, {'name': 'Acks::net::OK'}]
        new_labels = [{'name': 'Acks::NeedsReview'}]
        expected_note = ['/label "Acks::NeedsReview"',
                         '/unlabel "Acks::OK"'
                         ]
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note))

        # Change a double scoped label and ensure only related double scoped are removed.
        mr_labels = [{'name': 'Acks::OK'}, {'name': 'Acks::net::OK'}]
        new_labels = [{'name': 'Acks::net::NeedsReview'}]
        expected_note = ['/label "Acks::net::NeedsReview"',
                         '/unlabel "Acks::net::OK"'
                         ]
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note))

        # Add a double scoped label with remove_scoped set.
        mr_labels = [{'name': 'CKI_RHEL::OK'}, {'name': 'Acks::net::OK'}]
        new_labels = [{'name': 'CKI_RHEL::Failed::test'}]
        expected_note = ['/label "CKI_RHEL::Failed::test"',
                         '/unlabel "CKI_RHEL::OK"'
                         ]
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note),
                                  remove_scoped=True)

        # Add a lot of labels to trigger labels.list(iterator=True).
        new_labels = []
        count = 1
        while count <= 8:
            name = f"Acks::ss{count}::OK"
            desc = f"This MR has been approved by reviewers in the ss{count} group."
            label = {'name': name,
                     'color': '#428BCA',
                     'description': desc}
            new_labels.append(label)
            count += 1
        self._test_label_commands(mr_labels, new_labels,
                                  ('/label "Acks::ss1::OK"\n/label "Acks::ss2::OK"'
                                   '\n/label "Acks::ss3::OK"\n/label "Acks::ss4::OK"'
                                   '\n/label "Acks::ss5::OK"\n/label "Acks::ss6::OK"'
                                   '\n/label "Acks::ss7::OK"\n/label "Acks::ss8::OK"'))

    def test_find_dep_mr_in_line(self):
        namespace = 'dummy/test'
        line = f'Depends: {defs.GITFORGE}/dummy/test/-/merge_requests/135'
        self.assertEqual(common.find_dep_mr_in_line(namespace, line), 135)
        namespace = 'foo/bar'
        self.assertEqual(common.find_dep_mr_in_line(namespace, line), None)
        line = 'Depends: https://foo.bar/nope/-/merge_requests/1'
        self.assertEqual(common.find_dep_mr_in_line(namespace, line), None)
        line = 'Depends: !8675309'
        self.assertEqual(common.find_dep_mr_in_line(namespace, line), 8675309)

    def test_extract_dependencies(self):
        """Check for expected output."""
        # Test handling of None input message.
        dep_mr = mock.Mock()
        dep_mr.iid = 110
        dep_mr.description = "Bugzilla: https://bugzilla.redhat.com/24681357"
        project = mock.Mock()
        project.mergerequests.get.return_value = dep_mr
        project.path_with_namespace = 'fake/stuff'
        bzs = common.extract_dependencies(project, None)
        self.assertEqual(bzs, [])

        message = ('This BZ has dependencies.\n'
                   'Bugzilla: https://bugzilla.redhat.com/1234567\n'
                   'Depends: https://bugzilla.redhat.com/22334455\n'
                   'Depends: https://bugzilla.redhat.com/33445566\n'
                   'Depends: https://issues.redhat.com/browse/RHEL-3\n'
                   f'Depends: {defs.GITFORGE}/fake/stuff/-/merge_requests/110\n')
        bzs = common.extract_dependencies(project, message)
        self.assertEqual(bzs, ['22334455', '33445566', 'RHEL-3', '24681357'])

    @mock.patch.dict(os.environ, {}, clear=True)
    def test_try_bugzilla_conn_no_key(self):
        """Check for negative return when BUGZILLA_API_KEY is not set."""
        self.assertFalse(common.try_bugzilla_conn())

    @mock.patch.dict(os.environ, {'BUGZILLA_API_KEY': 'mocked'}, clear=True)
    def test_try_bugzilla_conn_with_key(self):
        """Check for positive return when BUGZILLA_API_KEY is set."""
        with mock.patch('webhook.common.connect_bugzilla', return_value=True):
            self.assertTrue(common.try_bugzilla_conn())

    @mock.patch.dict(os.environ, {}, clear=True)
    def test_connect_bugzilla(self):
        """Check for expected return value."""
        api_key = 'totally_fake_api_key'
        bzcon = mock.Mock()
        with mock.patch('bugzilla.Bugzilla', return_value=bzcon):
            self.assertEqual(common.connect_bugzilla(api_key), bzcon)

    @mock.patch('bugzilla.Bugzilla')
    def test_connect_bugzilla_exception(self, mocked_bugzilla):
        """Check ConnectionError exception generates logging."""
        api_key = 'totally_fake_api_key'
        mocked_bugzilla.side_effect = ConnectionError
        with self.assertLogs('cki.webhook.common', level='ERROR') as logs:
            self.assertFalse(common.connect_bugzilla(api_key))
            self.assertIn('Problem connecting to bugzilla server.', logs.output[-1])
        mocked_bugzilla.side_effect = PermissionError
        with self.assertLogs('cki.webhook.common', level='ERROR') as logs:
            self.assertFalse(common.connect_bugzilla(api_key))
            self.assertIn('Problem with file permissions', logs.output[-1])

    @mock.patch('webhook.common._match_label', mock.Mock(return_value=''))
    @mock.patch('webhook.common._get_gitlab_group')
    def test_add_label_quick_actions(self, get_group):
        """Check for the right label type (project or group) being evaluated."""
        mergerequest = mock.Mock()
        mergerequest.iid = 10
        project = mock.Mock()
        project.namespace = mock.MagicMock(id=1, full_path="redhat/rhel/src/kernel")
        project.mergerequests.get.return_value = mergerequest
        label_list = [{'name': 'Acks::mm::OK'}]
        with mock.patch('webhook.common._edit_label') as label:
            output = common._add_label_quick_actions(project, label_list)
        label.assert_called_with(get_group.return_value, '', {'name': 'Acks::mm::OK'})
        self.assertEqual(['/label "Acks::mm::OK"'], output)
        project.id = defs.ARK_PROJECT_ID
        with mock.patch('webhook.common._edit_label') as label:
            common._add_label_quick_actions(project, label_list)
        label.assert_called_with(project, '', {'name': 'Acks::mm::OK'})
        self.assertEqual(['/label "Acks::mm::OK"'], output)

    def test_extract_all_from_message(self):
        """Check for expected output."""
        # Test handling of None input message.
        bzs, non_mr_bzs, dep_bzs, cves, non_mr_cvez = common.extract_all_from_message(
            None, [], [], []
        )
        self.assertEqual(bzs, [])
        self.assertEqual(non_mr_bzs, [])
        self.assertEqual(dep_bzs, [])
        self.assertEqual(cves, [])
        self.assertEqual(non_mr_cvez, [])

        message1 = 'Here is my perfect patch.\nBugzilla: https://bugzilla.redhat.com/18123456\n' \
                   'CVE: CVE-2021-00001'
        bzs, non_mr_bzs, dep_bzs, cves, non_mr_cvez = common.extract_all_from_message(
            message1, [], [], []
        )
        self.assertEqual(bzs, ['18123456'])
        self.assertEqual(non_mr_bzs, [])
        self.assertEqual(dep_bzs, [])
        self.assertEqual(cves, ['CVE-2021-00001'])
        self.assertEqual(non_mr_cvez, [])
        bzs, non_mr_bzs, dep_bzs, cves, non_mr_cvez = common.extract_all_from_message(
            message1, [], [], ['18123456']
        )
        self.assertEqual(bzs, [])
        self.assertEqual(non_mr_bzs, [])
        self.assertEqual(dep_bzs, ['18123456'])
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            bzs, non_mr_bzs, dep_bzs, cves, non_mr_cvez = \
                common.extract_all_from_message(message1, ['12345678'], ['CVE-2021-00002'], [])
            self.assertIn('Bugzilla: 18123456 not listed in MR description.', logs.output[-2])
            self.assertIn('CVE: CVE-2021-00001 not listed in MR description.', logs.output[-1])
            self.assertEqual(bzs, [])
            self.assertEqual(non_mr_bzs, ['18123456'])
            self.assertEqual(dep_bzs, [])
            self.assertEqual(cves, [])
            self.assertEqual(non_mr_cvez, ['CVE-2021-00001'])

        jirastuff = 'Here is an MR using jira.\nJIRA: https://issues.redhat.com/browse/RHEL-2'
        bzs, non_mr_bzs, dep_bzs, cves, non_mr_cvez = common.extract_all_from_message(
            jirastuff, [], [], []
        )
        self.assertEqual(bzs, ['RHEL-2'])
        self.assertEqual(dep_bzs, [])
        bzs, non_mr_bzs, dep_bzs, cves, non_mr_cvez = common.extract_all_from_message(
            jirastuff, [], [], ['RHEL-2']
        )
        self.assertEqual(bzs, [])
        self.assertEqual(dep_bzs, ['RHEL-2'])

    def test_parse_mr_url(self):
        url1 = 'http://www.gitlab.com/mycoolproject/-/merge_requests/12345'
        url2 = 'http://www.gitlab.com/group/subgroup/project/-/merge_requests/35435747'

        self.assertEqual(('mycoolproject', 12345), common.parse_mr_url(url1))
        self.assertEqual(('group/subgroup/project', 35435747), common.parse_mr_url(url2))

    def test_description_tag_changes(self):
        old_description = ('Bugzilla: https://bugzilla.redhat.com/1234567\n'
                           'Bugzilla: https://bugzilla.redhat.com/2345678\n'
                           'Bugzilla: https://bugzilla.redhat.com/3456789\n'
                           'Bugzilla: https://bugzilla.redhat.com/8765432')
        description = ('Bugzilla: https://bugzilla.redhat.com/1234567\n'
                       'Bugzilla: https://bugzilla.redhat.com/2345678\n'
                       'Bugzilla: https://bugzilla.redhat.com/3456789')
        changes = {'description': {'previous': old_description,
                                   'current': description}
                   }

        # Link all in description, unlink 8765432.
        result = common.description_tag_changes(description, False, changes)
        self.assertEqual(set(result['link']), {1234567, 2345678, 3456789})
        self.assertEqual(set(result['unlink']), {8765432})

        # If the action is close then just unlink everything in the current description.
        result = common.description_tag_changes(description, True, {})
        self.assertEqual(result['unlink'], {1234567, 2345678, 3456789})
        self.assertEqual(result['link'], set())

    def test_get_mr(self):
        gl_project = mock.Mock(id=234567)
        gl_mr = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mr

        # No error, gl_mr object is returned.
        exception_raised = False
        try:
            result = common.get_mr(gl_project, 123)
        except:  # noqa: E722
            exception_raised = True
        gl_project.mergerequests.get.assert_called_with(123)
        self.assertEqual(result, gl_mr)
        self.assertFalse(exception_raised)

        # GitlabGetError code 404, None returned.
        exception_raised = False
        gl_project.mergerequests.get.reset_mock(return_value=True)
        gl_project.mergerequests.get.side_effect = GitlabGetError(response_code=404,
                                                                  error_message='oh no!')
        with self.assertLogs('cki.webhook.common', level='WARNING') as logs:
            try:
                result = common.get_mr(gl_project, 456)
            except GitlabGetError as err:
                if err.response_code == 404:
                    exception_raised = True
            gl_project.mergerequests.get.assert_called_with(456)
            self.assertIn('MR 456 does not exist in project 234567 (404).', logs.output[-1])
            self.assertEqual(result, None)
            self.assertFalse(exception_raised)

        # An unhandled exception so blow up.
        exception_raised = False
        gl_project.mergerequests.get.reset_mock()
        gl_project.mergerequests.get.side_effect = GitlabGetError(response_code=502,
                                                                  error_message='oh no!')
        try:
            common.get_mr(gl_project, 789)
        except GitlabGetError as err:
            if err.response_code == 502:
                exception_raised = True
        gl_project.mergerequests.get.assert_called_with(789)
        self.assertTrue(exception_raised)

    def test_get_pipeline(self):
        gl_project = mock.Mock(id=234567)
        gl_pipeline = mock.Mock()
        gl_project.pipelines.get.return_value = gl_pipeline

        # No error, gl_pipeline object is returned.
        exception_raised = False
        try:
            result = common.get_pipeline(gl_project, 123)
        except:  # noqa: E722
            exception_raised = True
        gl_project.pipelines.get.assert_called_with(123)
        self.assertEqual(result, gl_pipeline)
        self.assertFalse(exception_raised)

        # GitlabGetError code 404, None returned.
        exception_raised = False
        gl_project.pipelines.get.reset_mock(return_value=True)
        gl_project.pipelines.get.side_effect = GitlabGetError(response_code=404,
                                                              error_message='oh no!')
        with self.assertLogs('cki.webhook.common', level='WARNING') as logs:
            try:
                result = common.get_pipeline(gl_project, 456)
            except GitlabGetError as err:
                if err.response_code == 404:
                    exception_raised = True
            gl_project.pipelines.get.assert_called_with(456)
            self.assertIn('Pipeline 456 does not exist in project 234567 (404).', logs.output[-1])
            self.assertEqual(result, None)
            self.assertFalse(exception_raised)

        # An unhandled exception so blow up.
        exception_raised = False
        gl_project.pipelines.get.reset_mock()
        gl_project.pipelines.get.side_effect = GitlabGetError(response_code=502,
                                                              error_message='oh no!')
        try:
            common.get_pipeline(gl_project, 789)
        except GitlabGetError as err:
            if err.response_code == 502:
                exception_raised = True
        gl_project.pipelines.get.assert_called_with(789)
        self.assertTrue(exception_raised)

    def test_draft_status(self):
        class DraftTest(typing.NamedTuple):
            msg_draft_value: bool
            msg_changes_prev_curr: typing.Optional[tuple[bool, bool]]
            expected_is_draft: bool
            expected_changed: bool

        tests = [
            # Not a Draft, MR message changes do not include 'draft'.
            DraftTest(False, None, False, False),
            # It's not a Draft any more!
            DraftTest(False, (True, False), False, True),
            # I turned into a Draft.
            DraftTest(True, (False, True), True, True)
        ]

        for test in tests:
            with self.subTest(expected_is_draft=test.expected_is_draft,
                              expected_changed=test.expected_changed,
                              msg_draft_value=test.msg_draft_value,
                              msg_changes_prev_curr=test.msg_changes_prev_curr):
                payload = {'object_attributes': {'draft': test.msg_draft_value}, 'changes': {}}
                if test.msg_changes_prev_curr:
                    payload['changes']['draft'] = {'previous': test.msg_changes_prev_curr[0],
                                                   'current': test.msg_changes_prev_curr[1]}
                self.assertEqual(
                    common.draft_status(payload),
                    (test.expected_is_draft, test.expected_changed)
                )

    def test_load_yaml_data(self):
        with self.assertLogs('cki.cki_lib.misc', level='ERROR') as logs:
            result = common.load_yaml_data('bad_map_file.yml')
            self.assertEqual(result, None)
            self.assertIn("No such file or directory: 'bad_map_file.yml'", logs.output[-1])

        with mock.patch('pathlib.Path.read_text', return_value=self.FILE_CONTENTS):
            result = common.load_yaml_data('map_file.yml')
            self.assertEqual(result, self.MAPPINGS)

    def test_match_single_label(self):
        # No match
        results = common.match_single_label('readyForMerge', self.YAML_LABELS)
        self.assertEqual(results, None)

        # Basic match
        results = common.match_single_label('Acks::OK', self.YAML_LABELS)
        self.assertEqual(results, self.YAML_LABELS[0])

        # Regex match
        results = common.match_single_label('Subsystem:Cookies', self.YAML_LABELS)
        expected = {'name': 'Subsystem:Cookies', 'color': '#778899', 'description': 'hi Cookies'}
        self.assertEqual(results, expected)

    @mock.patch('webhook.common.load_yaml_data')
    def test_validate_labels(self, mock_load_yaml):
        # No data generates a runtime error
        mock_load_yaml.return_value = None
        raised = False
        try:
            results = common.validate_labels(['hi', 'there'], 'utils/labels.yaml')
        except RuntimeError:
            raised = True
        mock_load_yaml.assert_called_with('utils/labels.yaml')
        self.assertTrue(raised)

        # Find some labels
        mock_load_yaml.return_value = {'labels': self.YAML_LABELS}
        results = common.validate_labels(['Acks::OK', 'Subsystem:net'], 'yaml_path')
        expected = [{'name': 'Acks::OK', 'color': '#428BCA', 'description': 'all good'},
                    {'name': 'Subsystem:net', 'color': '#778899', 'description': 'hi net'}
                    ]
        mock_load_yaml.assert_called_with('yaml_path')
        self.assertEqual(results, expected)

        # Can't find it!
        raised = False
        try:
            results = common.validate_labels(['Acks::OK', 'Frogs::OK', 'Subsystem:net'], 'yaml')
        except RuntimeError as err:
            raised = True
            self.assertIn("unknown: ['Frogs::OK']", err.args[0])
        mock_load_yaml.assert_called_with('yaml')
        self.assertTrue(raised)

    @mock.patch('webhook.common.get_authlevel')
    def test_get_commits_count(self, authlevel):
        mreq = mock.Mock()
        proj = mock.Mock()
        mreq.iid = 19
        commits = [1, 2, 3, 4]
        mreq.commits.return_value = commits
        mreq.author = mock.MagicMock(id=1234)
        authlevel.return_value = 30  # Developer access
        count, _ = common.get_commits_count(proj, mreq)
        self.assertEqual(count, 4)
        i = 5
        while i < 2002:
            commits.append(i)
            i += 1
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            count, _ = common.get_commits_count(proj, mreq)
            self.assertEqual(count, 2001)
            self.assertIn("MR 19 has 2001 commits, too many to process -- wrong target branch?",
                          logs.output[-1])
        mreq.commits.return_value = []
        count, _ = common.get_commits_count(proj, mreq)
        self.assertEqual(count, 0)

    def test_get_mr_state_from_event(self):
        """Returns the state string value, if any, or None."""
        payload = {'object_attributes': {'state': 'opened'}}
        self.assertEqual(common.get_mr_state_from_event(payload), 'opened')
        payload = {'object_attributes': {}}
        self.assertEqual(common.get_mr_state_from_event(payload), None)
        payload = {'object_attributes': {}, 'merge_request': {'state': 'closed'}}
        self.assertEqual(common.get_mr_state_from_event(payload), 'closed')
        payload = {'object_attributes': None, 'merge_request': None}
        self.assertEqual(common.get_mr_state_from_event(payload), None)

    @mock.patch('cki_lib.misc.is_production')
    def test_create_mr_pipeline(self, mock_is_production):
        """Creates a new pipeline and returns its ID."""
        mock_mr = mock.Mock(iid=2345)
        mock_mr.pipelines.create.return_value = mock.Mock(id=536737)

        # Not production, returns MR IID.
        mock_is_production.return_value = False
        self.assertEqual(common.create_mr_pipeline(mock_mr), 2345)
        mock_mr.pipelines.create.assert_not_called()

        # Production, calls create and returns pipeline ID.
        mock_is_production.return_value = True
        self.assertEqual(common.create_mr_pipeline(mock_mr), 536737)
        mock_mr.pipelines.create.assert_called_once()

    @mock.patch('cki_lib.misc.is_production')
    def test_cancel_pipeline(self, mock_is_production):
        """Cancels an existing pipeline."""
        mock_project = mock.Mock()
        mock_project.pipelines.get.return_value.cancel.return_value = {'status': 'canceled'}

        # Not production, doesn't call cancel().
        mock_is_production.return_value = False
        self.assertIs(common.cancel_pipeline(mock_project, 12345), True)
        mock_project.pipelines.get.return_value.cancel.assert_not_called()

        # Production, success.
        mock_is_production.return_value = True
        self.assertIs(common.cancel_pipeline(mock_project, 12345), True)
        mock_project.pipelines.get.assert_called_once_with(12345)
        mock_project.pipelines.get.return_value.cancel.assert_called_once()

        # Production, failure.
        mock_project.pipelines.get.reset_mock()
        mock_project.pipelines.get.return_value.cancel.reset_mock()
        mock_project.pipelines.get.return_value.cancel.return_value = {'status': 'running'}
        self.assertIs(common.cancel_pipeline(mock_project, 12345), False)
        mock_project.pipelines.get.assert_called_once_with(12345)
        mock_project.pipelines.get.return_value.cancel.assert_called_once()

    @mock.patch('webhook.libjira.move_issue_states_forward')
    @mock.patch('webhook.libjira.issues_to_move_to_in_progress')
    @mock.patch('webhook.libjira.fetch_issues')
    def test_set_mr_issues_status(self, mock_fetch_issues, mock_issues_to_in_prog,
                                  mock_update_status):
        """Calls update_issue_status for an MR's issues."""
        ji_id = f'{defs.JPFX}1234567'
        cve_id = 'CVE-2022-12345'
        description = f'JIRA: https://issues.redhat.com/browse/{ji_id}\nCVE: {cve_id}'
        mr_attributes = {'description': description,
                         'draft': True,
                         'project_id': 56789,
                         'target_branch': 'main',
                         'head_pipeline': {'finished_at': '10pm'}
                         }

        # MR is draft, nothing to do.
        gl_mr = fakes.FakeMergeRequest(123, mr_attributes, actual_attributes=mr_attributes)
        common.set_mr_issues_status(gl_mr)
        mock_fetch_issues.assert_not_called()

        # No JIRA issue in the description
        mr_attributes['draft'] = False
        mr_attributes['description'] = f'Bugzilla: https://issues.redhat.com/browse/{ji_id}'
        gl_mr = fakes.FakeMergeRequest(123, mr_attributes, actual_attributes=mr_attributes)
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            common.set_mr_issues_status(gl_mr)
            self.assertIn('No JIRA Issues to update for', logs.output[-1])

        # No CVE so just the JIRA: items to update.
        mr_attributes['draft'] = False
        mr_attributes['description'] = f'JIRA: https://issues.redhat.com/browse/{ji_id}'
        gl_mr = fakes.FakeMergeRequest(123, mr_attributes, actual_attributes=mr_attributes)
        mock_fv = mock.MagicMock()
        mock_fv.name = 'rhel-9.1.0'
        fake_ji = fakes_jira.FakeJI(ji_id)
        fake_ji.fields.fixVersions = [mock_fv]
        fake_ji.fields.customfield_12321540.value = 'Pass'
        mock_fetch_issues.return_value = [fake_ji]
        mock_issues_to_in_prog.return_value = [fake_ji]

        common.set_mr_issues_status(gl_mr)
        mock_fetch_issues.assert_called_once_with({ji_id}, filter_kwf=True)
        mock_update_status.assert_called_with([fake_ji])

        # MR with a CVE. Move the issue and corresponding rt issue to Testable.
        mock_fetch_issues.reset_mock()
        mock_issues_to_in_prog.reset_mock()
        mock_update_status.reset_mock()
        mr_attributes['description'] = description
        gl_mr = fakes.FakeMergeRequest(123, mr_attributes, actual_attributes=mr_attributes)
        mock_fv = mock.MagicMock()
        mock_fv.name = 'rhel-9.1.0'
        fake_ji = fakes_jira.FakeJI(ji_id)
        fake_ji.fields.fixVersions = [mock_fv]
        fake_ji.fields.customfield_12321540.value = 'Pass'
        mock_fetch_issues.return_value = [fake_ji]
        common.set_mr_issues_status(gl_mr)
        mock_update_status.assert_called_with([fake_ji])

    def test_wait_for_new_mrs_no_sleep(self):
        """Does not sleep since the MR is old."""
        # Payload without MR details, no sleep.
        payload = deepcopy(fake_payloads.PUSH_PAYLOAD)
        with mock.patch('webhook.common.sleep') as mock_sleep:
            common.wait_for_new_mrs(payload)
            mock_sleep.assert_not_called()

        # Super old MR payload, no sleep.
        payload = deepcopy(fake_payloads.MR_PAYLOAD)
        self.assertEqual(payload['object_attributes']['created_at'], '2013-12-03T17:23:34Z')
        with mock.patch('webhook.common.sleep') as mock_sleep:
            common.wait_for_new_mrs(payload)
            mock_sleep.assert_not_called()

    def test_wait_for_new_mrs_sleep(self):
        """Sleeps for some time."""
        payload = deepcopy(fake_payloads.NOTE_PAYLOAD)
        with mock.patch('webhook.common.sleep') as mock_sleep:
            for _ in range(100):
                mock_sleep.reset_mock()
                # Create a payload timestamp that is up to DELAY_MAX seconds old.
                random_delay_secs = uniform(0, common.WAIT_FOR_NEW_MRS_DELAY_MAX)
                recent_time = datetime.now() - timedelta(seconds=random_delay_secs)
                payload['merge_request']['created_at'] = recent_time.isoformat()
                common.wait_for_new_mrs(payload)
                mock_sleep.assert_called_once()
                sleepy_time = mock_sleep.call_args[0][0]
                self.assertTrue(
                    0 <= sleepy_time <=
                    common.WAIT_FOR_NEW_MRS_DELAY_MAX + common.WAIT_FOR_NEW_MRS_DELAY_RANGE
                )


class TestEnsureGlInstanceAuth(KwfTestCase):
    """Tests for ensure_gl_instance_auth."""

    class InstanceAuthTest(typing.NamedTuple):
        gl_instance: Gitlab
        raises: bool = False
        expected_retval: typing.Optional[str] = None

    def run_instance_auth_test(
        self,
        auth_test: 'TestEnsureGlInstanceAuth.InstanceAuthTest'
    ) -> None:
        """Runs a test."""
        if auth_test.raises:
            with self.assertRaises(RuntimeError):
                common.ensure_gl_instance_auth(auth_test.gl_instance)
            return

        self.assertEqual(
            auth_test.expected_retval,
            common.ensure_gl_instance_auth(auth_test.gl_instance)
        )

    def test_ensure_gl_instance_auth_no_call(self):
        """Returns the username for an already authenticaed instance."""
        test_gl_instance = Gitlab('https://gitlab.com')
        test_gl_instance.user = mock.Mock(spec_set=['username'], username='test user')

        self.run_instance_auth_test(self.InstanceAuthTest(
            gl_instance=test_gl_instance,
            expected_retval='test user'
        ))

    def test_ensure_gl_instance_auth_after_call(self):
        """Returns the username after calling auth() on the given instance."""
        self.response_gl_auth(username='test me')
        test_gl_instance = Gitlab('https://gitlab.com')

        self.run_instance_auth_test(self.InstanceAuthTest(
            gl_instance=test_gl_instance,
            expected_retval='test me'
        ))

    def test_ensure_gl_instance_auth_raises(self):
        """Raises a Runtime error when there is no user."""
        self.response_gl_auth(body=GitlabAuthenticationError(error_message='oh no'))
        test_gl_instance = Gitlab('https://gitlab.com')

        self.run_instance_auth_test(self.InstanceAuthTest(
            gl_instance=test_gl_instance,
            raises=True
        ))
