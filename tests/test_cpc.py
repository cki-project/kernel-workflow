"""Webhook interaction tests."""
from copy import deepcopy
from unittest import mock

from tests.helpers import KwfTestCase
from webhook import cpc


class TestCpc(KwfTestCase):
    """ Test Webhook class."""

    def process_rule(self, rule, data):
        res = None
        err = ''
        failed = []
        steps = []
        errexp = ''

        tokens = cpc.tokenize(rule)
        try:
            tokens = cpc.rpn(tokens)
        except Exception as ex:
            return res, str(ex), failed, errexp

        for token in tokens:
            if token.type is not cpc.TokenType.VAR:
                continue
            if token.name in data:
                token.value = data[token.name]
            else:
                token.value = ''
        try:
            res = cpc.evaluate(tokens, failed=failed, steps=steps)
        except Exception as ex:
            return res, str(ex), failed, errexp

        errexp = cpc.errexp(tokens)

        return res, err, failed, errexp

    def test_rules(self):
        # Approved
        rule = 'release == +'
        data = {'release': '+'}
        res, err, failed, errexp = self.process_rule(rule, data)
        self.assertEqual(errexp, 'release "+" == "+"')
        self.assertEqual(failed, [])
        self.assertEqual(err, '')
        self.assertTrue(res)

        rule = '((release == + and zstream == + and zstream_target_release == 8.5.0) or ((jiraProject == RHELPLAN) and ((jiraField(release) == +) or (jiraField(BZ release) == +)) and ((jiraField(zstream) == +) or (jiraField(BZ zstream) == +)) and (jiraField(ZStream Target Release) == 8.5.0)))'  # noqa: E501
        data = {'release': '+', 'zstream': '+', 'zstream_target_release': '8.5.0'}
        res, err, failed, errexp = self.process_rule(rule, data)
        self.assertEqual(errexp, 'release "+" == "+" and zstream "+" == "+" and zstream_target_release "8.5.0" == "8.5.0"')  # noqa: E501
        self.assertEqual(failed, [])
        self.assertEqual(err, '')
        self.assertTrue(res)

        rule = '(((jiraProject == RHEL) and (jiraField(fixVersions) == rhel-8.9.0)) and ((jiraField(Release Blocker) == Approved Blocker) or (jiraField(Release Blocker) == Approved Exception)))'  # noqa: E501
        data = {'jiraProject': 'RHEL', 'jiraField(fixVersions)': 'rhel-8.9.0', 'jiraField(Release Blocker)': 'Approved Blocker'}  # noqa: E501
        res, err, failed, errexp = self.process_rule(rule, data)
        self.assertEqual(errexp, 'jiraProject "RHEL" == "RHEL" and jiraField(fixVersions) "rhel-8.9.0" == "rhel-8.9.0" and (jiraField(Release Blocker) "Approved Blocker" == "Approved Blocker" or jiraField(Release Blocker) "Approved Blocker" == "Approved Exception")')  # noqa: E501
        self.assertEqual(failed, [])
        self.assertEqual(err, '')
        self.assertTrue(res)

        rule = '(release == + and internal_target_release == 9.0.0)'
        data = {'release': '+', 'internal_target_release': '9.0.0'}
        res, err, failed, errexp = self.process_rule(rule, data)
        self.assertEqual(errexp, 'release "+" == "+" and internal_target_release "9.0.0" == "9.0.0"')  # noqa: E501
        self.assertEqual(failed, [])
        self.assertEqual(err, '')
        self.assertTrue(res)

        rule = 'release == + and internal_target_release == 9.0.0'
        data = {'release': '+', 'internal_target_release': '9.0.0'}
        res, err, failed, errexp = self.process_rule(rule, data)
        self.assertEqual(errexp, 'release "+" == "+" and internal_target_release "9.0.0" == "9.0.0"')  # noqa: E501
        self.assertEqual(failed, [])
        self.assertEqual(err, '')
        self.assertTrue(res)

        rule = '((release == + and internal_target_release == 9.0.0) and (blocker == + or exception == +))'  # noqa: E501
        data = {'release': '+', 'blocker': '+', 'internal_target_release': '9.0.0'}
        res, err, failed, errexp = self.process_rule(rule, data)
        self.assertEqual(errexp, 'release "+" == "+" and internal_target_release "9.0.0" == "9.0.0" and (blocker "+" == "+" or exception "" == "+")')  # noqa: E501
        self.assertEqual(failed, ['exception="" [need: "+"]'])
        self.assertEqual(err, '')
        self.assertTrue(res)

        rule = 'release == + and internal_target_release == 9.0.0 and (blocker == + or exception == +)'  # noqa: E501
        data = {'release': '+', 'exception': '+', 'internal_target_release': '9.0.0'}
        res, err, failed, errexp = self.process_rule(rule, data)
        self.assertEqual(errexp, 'release "+" == "+" and internal_target_release "9.0.0" == "9.0.0" and (blocker "" == "+" or exception "+" == "+")')  # noqa: E501
        self.assertEqual(failed, ['blocker="" [need: "+"]'])
        self.assertEqual(err, '')
        self.assertTrue(res)

        # Not Approved
        rule = '(release == + and internal_target_release == 9.0.0)'
        data = {'release': '?', 'internal_target_release': '9.0.0'}
        res, err, failed, errexp = self.process_rule(rule, data)
        self.assertEqual(errexp, 'release "?" == "+" and internal_target_release "9.0.0" == "9.0.0"')  # noqa: E501
        self.assertEqual(failed, ['release="?" [need: "+"]'])
        self.assertEqual(err, '')
        self.assertFalse(res)

        rule = '((release == + and zstream == + and zstream_target_release == 8.5.0) or ((jiraProject == RHELPLAN) and ((jiraField(release) == +) or (jiraField(BZ release) == +)) and ((jiraField(zstream) == +) or (jiraField(BZ zstream) == +)) and (jiraField(ZStream Target Release) == 8.5.0)))'  # noqa: E501
        data = {'release': '+', 'zstream': '?', 'zstream_target_release': '8.5.0'}
        res, err, failed, errexp = self.process_rule(rule, data)
        self.assertEqual(errexp, 'release "+" == "+" and zstream "?" == "+" and zstream_target_release "8.5.0" == "8.5.0"')  # noqa: E501
        self.assertEqual(failed, ['zstream="?" [need: "+"]'])
        self.assertEqual(err, '')
        self.assertFalse(res)

        rule = 'release == + and internal_target_release == 9.0.0 and (blocker == + or exception == +)'  # noqa: E501
        data = {'release': '+', 'exception': '?', 'internal_target_release': '9.0.0'}
        res, err, failed, errexp = self.process_rule(rule, data)
        self.assertEqual(errexp, 'release "+" == "+" and internal_target_release "9.0.0" == "9.0.0" and (blocker "" == "+" or exception "?" == "+")')  # noqa: E501
        self.assertEqual(failed, ['blocker="" [need: "+"]', 'exception="?" [need: "+"]'])
        self.assertEqual(err, '')
        self.assertFalse(res)

        rule = 'release == + and internal_target_release == 9.0.0 and (blocker == + or exception == +)'  # noqa: E501
        data = {'release': '?', 'blocker': '-', 'exception': '?', 'internal_target_release': '9.0.0'}  # noqa: E501
        res, err, failed, errexp = self.process_rule(rule, data)
        self.assertEqual(errexp, 'release "?" == "+" and internal_target_release "9.0.0" == "9.0.0" and (blocker "-" == "+" or exception "?" == "+")')  # noqa: E501
        self.assertEqual(failed, ['release="?" [need: "+"]', 'blocker="-" [need: "+"]', 'exception="?" [need: "+"]'])  # noqa: E501
        self.assertEqual(err, '')
        self.assertFalse(res)

        # Malformatted
        rule = 'release == + and internal_target_release == 9.0.0) and (blocker == + or exception == +)'  # noqa: E501
        res, err, failed, errexp = self.process_rule(rule, data)
        self.assertEqual(errexp, '')
        self.assertEqual(failed, [])
        self.assertIn('Missing left bracket for token', err)
        self.assertIsNone(res)

        rule = '(release == + and internal_target_release == 9.0.0 and (blocker == + or exception == +)'  # noqa: E501
        res, err, failed, errexp = self.process_rule(rule, data)
        self.assertEqual(errexp, '')
        self.assertEqual(failed, [])
        self.assertIn('Expecting EQ,OR,AND token', err)
        self.assertIsNone(res)

        rule = '(release ==  and internal_target_release == 9.0.0)'
        res, err, failed, errexp = self.process_rule(rule, data)
        self.assertEqual(errexp, '')
        self.assertEqual(failed, [])
        self.assertIn('Comparison requires two operands', err)
        self.assertIsNone(res)

        rule = 'release =='
        res, err, failed, errexp = self.process_rule(rule, data)
        self.assertEqual(errexp, '')
        self.assertEqual(failed, [])
        self.assertIn('Unexpected stack content', err)
        self.assertIsNone(res)

        rule = '(release == + + and internal_target_release == 9.0.0)'
        res, err, failed, errexp = self.process_rule(rule, data)
        self.assertEqual(errexp, '')
        self.assertEqual(failed, [])
        self.assertIn('Second "==" operand has to be LIT', err)
        self.assertIsNone(res)

        rule = 'release == + and internal_target_release 9.0.0'
        res, err, failed, errexp = self.process_rule(rule, data)
        self.assertEqual(errexp, '')
        self.assertEqual(failed, ['release="?" [need: "+"]'])
        self.assertIn('Expecting LIT,EXP token', err)
        self.assertIsNone(res)

        rule = 'release == + == x and internal_target_release == 9.0.0'
        res, err, failed, errexp = self.process_rule(rule, data)
        self.assertEqual(errexp, '')
        self.assertEqual(failed, [])
        self.assertIn('First "==" operand has to be VAR', err)
        self.assertIsNone(res)


class TestPolicies(KwfTestCase):
    """Tests the policy loading wrappers and helpers."""

    POLICY_HTML = ('<!DOCTYPE html>'
                   '<html>'
                   '<head>'
                   '    <meta charset="UTF-8">'
                   '    <title>Dist-Git Server</title>'
                   '</head>'
                   '<body>'
                   '    <h1>Dist-Git Server</h1>'
                   '    <div class="row">'
                   '        <div class="column">'
                   '            <h2>Commit Policy Overview</h2>'
                   '            <input type="text" id="filter" placeholder="Filter…">'
                   '            <dl>'
                   '                <div class="header">'
                   '                    <dt>Branch</dt>'
                   '                    </dd>Policy</dd>'
                   '                </div>'
                   '                <div class="rule" data-branch="*main">'
                   '                    <dt>*main</dt>'
                   '                    <dd>((release == + and internal_target_release == 9.0.0) '
                   'or ((jiraProject == RHELPLAN) and ((jiraField(release) == +) '
                   'or (jiraField(BZ release) == +)) and (jiraField(fixVersions) == 9.0.0)))</dd>'
                   '                </div>'
                   '                <div class="rule" data-branch="*rhel-8.7.0">'
                   '                    <dt>*rhel-8.7.0</dt>'
                   '                    <dd>((release == + and internal_target_release == 8.7.0) '
                   'or ((jiraProject == RHELPLAN) and ((jiraField(release) == +) '
                   'or (jiraField(BZ release) == +)) and (jiraField(fixVersions) == 8.7.0)))</dd>'
                   '                </div>'
                   '                <div class="rule" data-branch="*rhel-8.6.0">'
                   '                    <dt>*rhel-8.6.0</dt>'
                   '                    <dd>((release == + and internal_target_release == 8.6.0) '
                   'or ((jiraProject == RHELPLAN) and ((jiraField(release) == +) '
                   'or (jiraField(BZ release) == +)) and (jiraField(fixVersions) == 8.6.0)))</dd>'
                   '                </div>')

    POLICY_TXT = ('c9s\n'
                  '  ((release == + and internal_target_release == 9.1.0) or'
                  '((jiraProject == RHELPLAN) and ((jiraField(release) == +) or '
                  '(jiraField(BZ release) == +)) and (jiraField(fixVersions) == 9.1.0)))\n'
                  'c8s\n'
                  '  ((release == + and internal_target_release == 8.7.0) or'
                  '((jiraProject == RHELPLAN) and ((jiraField(release) == +) or '
                  '(jiraField(BZ release) == +)) and (jiraField(fixVersions) == 8.7.0)))\n'
                  '*rhel-8.5.0\n'
                  '  ((release == + and zstream == + and zstream_target_release == 8.5.0) or '
                  '((jiraProject == RHELPLAN) and ((jiraField(release) == +) or '
                  '(jiraField(BZ release) == +)) and ((jiraField(zstream) == +) or '
                  '(jiraField(BZ zstream) == +)) and (jiraField(ZStream Target Release) == '
                  '8.5.0)))\n'
                  '*rhel-8.4.0\n'
                  '  ((release == + and zstream == + and zstream_target_release == 8.4.0) or '
                  '((jiraProject == RHELPLAN) and ((jiraField(release) == +) or '
                  '(jiraField(BZ release) == +)) and ((jiraField(zstream) == +) or '
                  '(jiraField(BZ zstream) == +)) and (jiraField(ZStream Target Release) == '
                  '8.4.0)))\n'
                  '  rpms/redhat-release:\n'
                  '  (((jiraProject == RHELBLD) or (jiraProject == SPRHEL) or '
                  '(jiraProject == RHELPLAN)) or ((release == + and zstream == + and '
                  'zstream_target_release == 8.4.0) or ((jiraProject == RHELPLAN) and '
                  '((jiraField(release) == +) or (jiraField(BZ release) == +)) and '
                  '((jiraField(zstream) == +) or (jiraField(BZ zstream) == +)) and '
                  '(jiraField(ZStream Target Release) == 8.4.0))))\n'
                  '*rhel-9-main\n'
                  '  Anything Goes!\n')

    @mock.patch('webhook.cpc.get_session')
    @mock.patch('webhook.cpc.get_env_var_or_raise')
    def test_get_policy_data_html(self, mock_get_env_var, mock_get_session):
        """Returns a dict of policy tokens."""
        mock_response = mock.Mock(text=self.POLICY_HTML)
        mock_response.headers = {'Content-Type': 'text/html; charset=UTF-8'}
        mock_get_session().get.return_value = mock_response
        result = cpc.get_policy_data()
        self.assertEqual(len(result), 3)
        self.assertEqual(len(result['main']), 23)
        self.assertEqual(len(result['rhel-8.7.0']), 23)
        self.assertEqual(len(result['rhel-8.6.0']), 23)
        mock_get_session().get.assert_called_with(mock_get_env_var.return_value)

    @mock.patch('webhook.cpc.get_session')
    @mock.patch('webhook.cpc.get_env_var_or_raise')
    def test_get_policy_data_html_fails(self, mock_get_env_var, mock_get_session):
        """Raises a RuntimeError when no data found."""
        mock_response = mock.Mock(text='')
        mock_response.headers = {'Content-Type': 'text/html; charset=UTF-8'}
        mock_get_session().get.return_value = mock_response
        exception_raised = False
        try:
            cpc.get_policy_data()
        except RuntimeError:
            exception_raised = True
        self.assertTrue(exception_raised)
        mock_get_session().get.assert_called_with(mock_get_env_var.return_value)

    @mock.patch('webhook.cpc.get_session')
    @mock.patch('webhook.cpc.get_env_var_or_raise')
    def test_get_policy_data_txt(self, mock_get_env_var, mock_get_session):
        """Returns a dict of policy tokens."""
        mock_response = mock.Mock(text=self.POLICY_TXT)
        mock_response.headers = {'Content-Type': 'text/plain; charset=UTF-8'}
        mock_get_session().get.return_value = mock_response
        result = cpc.get_policy_data()
        self.assertEqual(len(result), 5)
        self.assertEqual(len(result['c9s']), 23)
        self.assertEqual(len(result['c8s']), 23)
        self.assertEqual(len(result['rhel-8.5.0']), 35)
        self.assertEqual(len(result['rhel-8.4.0']), 35)
        self.assertEqual(len(result['rhel-9-main']), 0)
        mock_get_session().get.assert_called_with(mock_get_env_var.return_value)

    @mock.patch('webhook.cpc.get_session')
    @mock.patch('webhook.cpc.get_env_var_or_raise')
    def test_get_one_policy_html(self, mock_get_env_var, mock_get_session):
        """Returns the list of policy tokens matching the given branch."""
        mock_response = mock.Mock(text=self.POLICY_HTML)
        mock_response.headers = {'Content-Type': 'text/html; charset=UTF-8'}
        mock_get_session().get.return_value = mock_response
        result = cpc.get_one_policy('rhel-8.7.0')
        self.assertEqual(len(result), 23)

    @mock.patch('webhook.cpc.get_session')
    @mock.patch('webhook.cpc.get_env_var_or_raise')
    def test_get_one_policy_txt(self, mock_get_env_var, mock_get_session):
        """Returns the list of policy tokens matching the given branch."""
        mock_response = mock.Mock(text=self.POLICY_TXT)
        mock_response.headers = {'Content-Type': 'text/plain; charset=UTF-8'}
        mock_get_session().get.return_value = mock_response
        result = cpc.get_one_policy('rhel-8.4.0')
        self.assertEqual(len(result), 35)

    @mock.patch('webhook.cpc.get_session')
    @mock.patch('webhook.cpc.get_env_var_or_raise')
    def test_get_policy_data_txt_filtered(self, mock_get_env_var, mock_get_session):
        """Returns a dict with a subset of policy tokens."""
        mock_response = mock.Mock(text=self.POLICY_TXT)
        mock_response.headers = {'Content-Type': 'text/plain; charset=UTF-8'}
        mock_get_session().get.return_value = mock_response
        result = cpc.get_policy_data(policy_regex=r'rhel-8')
        self.assertTrue({'rhel-8.5.0', 'rhel-8.4.0'} == result.keys())

    @mock.patch('webhook.cpc.get_session')
    @mock.patch('webhook.cpc.get_env_var_or_raise')
    def test_get_policy_data_html_filtered(self, mock_get_env_var, mock_get_session):
        """Returns a dict with a subset of policy tokens."""
        mock_response = mock.Mock(text=self.POLICY_HTML)
        mock_response.headers = {'Content-Type': 'text/html; charset=UTF-8'}
        mock_get_session().get.return_value = mock_response
        result = cpc.get_policy_data(policy_regex=r'rhel-8')
        self.assertTrue({'rhel-8.7.0', 'rhel-8.6.0'} == result.keys())

    @mock.patch('webhook.cpc.get_session')
    @mock.patch('webhook.cpc.get_env_var_or_raise')
    def test_get_policy_data_txt_regex(self, mock_get_env_var, mock_get_session):
        """Returns a dict with a subset of policy tokens."""
        mock_response = mock.Mock(text=self.POLICY_TXT)
        mock_response.headers = {'Content-Type': 'text/plain; charset=UTF-8'}
        mock_get_session().get.return_value = mock_response
        result = cpc.get_policy_data(policy_regex=r'^(rhel-.*|c\d+s)$')
        print(result.keys())
        self.assertTrue({'c9s', 'c8s', 'rhel-8.5.0', 'rhel-8.4.0', 'rhel-9-main'} == result.keys())


class TestBugReady(KwfTestCase):
    """Tests for is_bug_ready and helpers."""

    MOCK_Y_ATTRS = {'id': 12345,
                    'product': 'Red Hat Enterprise Linux 8',
                    'component': 'kernel',
                    'flags': [{'name': 'release', 'status': '+'}],
                    'cf_internal_target_release': '8.7.0'
                    }

    MOCK_Z_ATTRS = {'id': 56789,
                    'product': 'Red Hat Enterprise Linux 8',
                    'component': 'kernel-rt',
                    'flags': [{'name': 'release', 'status': '?'}],
                    'cf_zstream_target_release': '8.2.0'
                    }

    MOCK_J_ATTRS = {'id': 12345,
                    'fields': {'project': {'name': 'Red Hat Enterprise Linux 8'},
                               'components': ['kernel'],
                               'versions': [{'name': 'rhel-8.7.0'}],
                               'fixVersions': [{'name': 'rhel-8-7.0'}]
                               }
                    }

    @mock.patch('webhook.cpc._load_tokens')
    @mock.patch('webhook.cpc.errexp')
    @mock.patch('webhook.cpc.evaluate')
    def test_is_bug_ready_good_with_errexp(self, mock_evaluate, mock_errexp, mock_load_tokens):
        """Calls evaluate and returns True. errexp() not called."""
        mock_evaluate.return_value = True
        result, fail_reason = cpc.is_bug_ready('bug', 'tokens')
        self.assertEqual(result, True)
        self.assertEqual(fail_reason, '')
        mock_load_tokens.assert_called_once_with('bug', 'tokens')
        mock_evaluate.assert_called_once()
        mock_errexp.assert_not_called()

    @mock.patch('webhook.cpc._load_jira_tokens')
    @mock.patch('webhook.cpc.errexp')
    @mock.patch('webhook.cpc.evaluate')
    def test_is_rhissue_ready_good_with_errexp(self, mock_evaluate, mock_errexp, mock_load_tokens):
        """Calls evaluate and returns True. errexp() not called."""
        mock_evaluate.return_value = True
        mock_issue = mock.Mock()
        mock_issue.fields = mock.Mock()
        rhelver = mock.Mock()
        rhelver.name = 'rhel-8.8.0'
        mock_issue.fields.fixVersions = [rhelver]
        result, fail_reason = cpc.is_rhissue_ready(mock_issue, 'tokens')
        self.assertEqual(result, True)
        self.assertEqual(fail_reason, '')
        mock_load_tokens.assert_called_once_with(mock_issue, 'tokens')
        mock_evaluate.assert_called_once()
        mock_errexp.assert_not_called()

    @mock.patch('webhook.cpc._load_tokens')
    @mock.patch('webhook.cpc.errexp')
    @mock.patch('webhook.cpc.evaluate')
    def test_is_bug_ready_bad_with_errexp(self, mock_evaluate, mock_errexp, mock_load_tokens):
        """Calls evaluate and returns False. Second return value is errexp return value."""
        mock_evaluate.return_value = False
        result, fail_reason = cpc.is_bug_ready('bug', 'tokens')
        self.assertEqual(result, False)
        self.assertIs(fail_reason, mock_errexp.return_value)
        mock_load_tokens.assert_called_once_with('bug', 'tokens')
        mock_evaluate.assert_called_once()
        mock_errexp.assert_called_once_with('tokens')

    @mock.patch('webhook.cpc._load_jira_tokens')
    @mock.patch('webhook.cpc.errexp')
    @mock.patch('webhook.cpc.evaluate')
    def test_is_rhissue_ready_bad_with_errexp(self, mock_evaluate, mock_errexp, mock_load_tokens):
        """Calls evaluate and returns False. Second return value is errexp return value."""
        mock_evaluate.return_value = False
        mock_issue = mock.Mock()
        mock_issue.fields = mock.Mock()
        rhelver = mock.Mock()
        rhelver.name = 'rhel-8.8.0'
        mock_issue.fields.fixVersions = [rhelver]
        result, fail_reason = cpc.is_rhissue_ready(mock_issue, 'tokens')
        self.assertEqual(result, False)
        self.assertIs(fail_reason, mock_errexp.return_value)
        mock_load_tokens.assert_called_once_with(mock_issue, 'tokens')
        mock_evaluate.assert_called_once()
        mock_errexp.assert_called_once_with('tokens', is_jira=True)

    @mock.patch('webhook.cpc._load_tokens')
    @mock.patch('webhook.cpc.errexp')
    @mock.patch('webhook.cpc.evaluate')
    def test_is_bug_ready_good_no_errexp(self, mock_evaluate, mock_errexp, mock_load_tokens):
        """Calls evaluate and returns True. errexp() not called; second return value is a list."""
        mock_evaluate.return_value = True
        result, fail_reason = cpc.is_bug_ready('bug', 'tokens', get_errexp=False)
        self.assertEqual(result, True)
        self.assertEqual(fail_reason, [])
        mock_load_tokens.assert_called_once_with('bug', 'tokens')
        mock_evaluate.assert_called_once()
        mock_errexp.assert_not_called()

    @mock.patch('webhook.cpc._load_jira_tokens')
    @mock.patch('webhook.cpc.errexp')
    @mock.patch('webhook.cpc.evaluate')
    def test_is_rhissue_ready_good_no_errexp(self, mock_evaluate, mock_errexp, mock_load_tokens):
        """Calls evaluate and returns True. errexp() not called; second return value is a list."""
        mock_evaluate.return_value = True
        mock_issue = mock.Mock()
        mock_issue.fields = mock.Mock()
        rhelver = mock.Mock()
        rhelver.name = 'rhel-8.8.0'
        mock_issue.fields.fixVersions = [rhelver]
        result, fail_reason = cpc.is_rhissue_ready(mock_issue, 'tokens', get_errexp=False)
        self.assertEqual(result, True)
        self.assertEqual(fail_reason, [])
        mock_load_tokens.assert_called_once_with(mock_issue, 'tokens')
        mock_evaluate.assert_called_once()
        mock_errexp.assert_not_called()

    @mock.patch('webhook.cpc._load_tokens')
    @mock.patch('webhook.cpc.errexp')
    @mock.patch('webhook.cpc.evaluate')
    def test_is_bug_ready_bad_no_errexp(self, mock_evaluate, mock_errexp, mock_load_tokens):
        """Calls evaluate and returns False. errexp() not called; second return value is a list."""
        mock_evaluate.return_value = False
        result, fail_reason = cpc.is_bug_ready('bug', 'tokens', get_errexp=False)
        self.assertEqual(result, False)
        self.assertIs(fail_reason, mock_evaluate.call_args.kwargs['failed'])
        mock_load_tokens.assert_called_once_with('bug', 'tokens')
        mock_evaluate.assert_called_once()
        mock_errexp.assert_not_called()

    @mock.patch('webhook.cpc._load_jira_tokens')
    @mock.patch('webhook.cpc.errexp')
    @mock.patch('webhook.cpc.evaluate')
    def test_is_rhissue_ready_bad_no_errexp(self, mock_evaluate, mock_errexp, mock_load_tokens):
        """Calls evaluate and returns False. errexp() not called; second return value is a list."""
        mock_evaluate.return_value = False
        mock_issue = mock.Mock()
        mock_issue.fields = mock.Mock()
        rhelver = mock.Mock()
        rhelver.name = 'rhel-8.8.0'
        mock_issue.fields.fixVersions = [rhelver]
        result, fail_reason = cpc.is_rhissue_ready(mock_issue, 'tokens', get_errexp=False)
        self.assertEqual(result, False)
        self.assertIs(fail_reason, mock_evaluate.call_args.kwargs['failed'])
        mock_load_tokens.assert_called_once_with(mock_issue, 'tokens')
        mock_evaluate.assert_called_once()
        mock_errexp.assert_not_called()

    def test_is_bug_ready_with_errexp_bogus_policy(self):
        """Handles a non-rpn policy string; returns False and str output of dump_tokens."""
        mock_tokens = cpc._tokenize_string('Anything Goes!')
        mock_bug = mock.Mock(**self.MOCK_Y_ATTRS)
        result, fail_reason = cpc.is_bug_ready(mock_bug, mock_tokens)
        self.assertIs(result, False)
        self.assertEqual(fail_reason, cpc.dump_tokens(mock_tokens))

    def test_is_rhissue_ready_with_errexp_bogus_policy(self):
        """Handles a non-rpn policy string; returns False and str output of dump_tokens."""
        mock_tokens = cpc._tokenize_string('Anything Goes!')
        mock_issue = mock.Mock(**self.MOCK_J_ATTRS)
        result, fail_reason = cpc.is_rhissue_ready(mock_issue, mock_tokens)
        self.assertIs(result, False)
        self.assertEqual(fail_reason, cpc.dump_tokens(mock_tokens))

    def test_is_bug_ready_without_errexp_bogus_policy(self):
        """Handles a non-rpn policy string; returns False and str output of dump_tokens."""
        mock_tokens = cpc._tokenize_string('Anything Goes!')
        mock_bug = mock.Mock(**self.MOCK_Z_ATTRS)
        result, fail_reason = cpc.is_bug_ready(mock_bug, mock_tokens, get_errexp=False)
        self.assertIs(result, False)
        self.assertEqual(fail_reason, [cpc.dump_tokens(mock_tokens)])

    def test_is_rhissue_ready_without_errexp_bogus_policy(self):
        """Handles a non-rpn policy string; returns False and str output of dump_tokens."""
        mock_tokens = cpc._tokenize_string('Anything Goes!')
        mock_issue = mock.Mock(**self.MOCK_J_ATTRS)
        result, fail_reason = cpc.is_rhissue_ready(mock_issue, mock_tokens, get_errexp=False)
        self.assertIs(result, False)
        self.assertEqual(fail_reason, [cpc.dump_tokens(mock_tokens)])

    @mock.patch('webhook.cpc._load_tokens')
    @mock.patch('webhook.cpc.errexp')
    @mock.patch('webhook.cpc.evaluate')
    def test_is_bug_ready_with_product_component_matches(self, mock_evaluate, mock_errexp,
                                                         mock_load_tokens):
        """Also checks the bug product/component."""
        mock_bug = mock.Mock(**self.MOCK_Y_ATTRS)
        mock_evaluate.return_value = True

        product = 'Red Hat Enterprise Linux 8'
        component = 'kernel'
        result, fail_reason = cpc.is_bug_ready(mock_bug, 'tokens', product=product,
                                               component=component)
        self.assertEqual(result, True)
        self.assertEqual(fail_reason, '')
        mock_load_tokens.assert_called_once_with(mock_bug, 'tokens')
        mock_evaluate.assert_called_once()
        mock_errexp.assert_not_called()

    @mock.patch('webhook.cpc._load_jira_tokens')
    @mock.patch('webhook.cpc.errexp')
    @mock.patch('webhook.cpc.evaluate')
    def test_is_rhissue_ready(self, mock_evaluate, mock_errexp, mock_load_tokens):
        """Checks the JIRA issue readiness."""
        mock_issue = mock.Mock()
        mock_issue.fields = mock.Mock()
        rhelver = mock.Mock()
        rhelver.name = 'rhel-8.8.0'
        mock_issue.fields.fixVersions = [rhelver]
        mock_evaluate.return_value = True

        result, fail_reason = cpc.is_rhissue_ready(mock_issue, 'tokens')
        self.assertTrue(result)
        self.assertEqual(fail_reason, '')
        mock_load_tokens.assert_called_once_with(mock_issue, 'tokens')
        mock_evaluate.assert_called_once()
        mock_errexp.assert_not_called()

    @mock.patch('webhook.cpc._load_tokens')
    @mock.patch('webhook.cpc.errexp')
    @mock.patch('webhook.cpc.evaluate')
    def test_is_bug_ready_with_product_component_no_match(self, mock_evaluate, mock_errexp,
                                                          mock_load_tokens):
        """Also checks the bug product/component."""
        mock_bug = mock.Mock(**self.MOCK_Y_ATTRS)
        mock_evaluate.return_value = True
        mock_errexp.return_value = ''

        product = 'Red Hat Enterprise Linux 7'
        component = 'kernel-rt'
        result, fail_reason = cpc.is_bug_ready(mock_bug, 'tokens', product=product,
                                               component=component)
        self.assertEqual(result, False)
        self.assertEqual(fail_reason, ' and product "Red Hat Enterprise Linux 8" == "Red Hat '
                                      'Enterprise Linux 7" and component "kernel" == "kernel-rt"')
        mock_load_tokens.assert_called_once_with(mock_bug, 'tokens')
        mock_evaluate.assert_called_once()
        mock_errexp.assert_called_once_with('tokens')

    def test_get_jira_fix_version(self):
        """Test jira-based version extraction logic."""
        issue = mock.Mock()
        issue.fields.fixVersions = None

        result = cpc.get_jira_fix_version(issue)
        self.assertEqual(result, "")

        version = mock.Mock()
        version.name = 'rhel-8.7.0'
        issue.fields.fixVersions = [version]
        result = cpc.get_jira_fix_version(issue)
        self.assertEqual(result, "rhel-8.7.0")

        version2 = mock.Mock()
        version2.name = 'foobar'
        issue.fields.fixVersions = [version, version2]
        result = cpc.get_jira_fix_version(issue)
        self.assertEqual(result, "")

    @mock.patch('webhook.cpc.get_jira_fix_version')
    def test_load_jira_tokens(self, fixver):
        """Set token value attributes from input jira issue data."""
        token1 = mock.Mock(type=cpc.TokenType.VAR, value='')
        token1.name = 'jiraField(fixVersions)'
        token2 = mock.Mock(type=cpc.TokenType.VAR, value='')
        token2.name = 'jiraProject'
        token3 = mock.Mock(type=cpc.TokenType.VAR, value='')
        token3.name = 'jiraField(Release Blocker)'

        # sets the token values from the JIRA Issue
        fixver.return_value = 'rhel-8.7.0'
        mock_issue = mock.Mock()
        mock_issue.fields.project = 'RHEL'
        mock_issue.fields.customfield_12319743 = 'Approved Blocker'
        mock_tokens = [token1, token2, token3]
        cpc._load_jira_tokens(mock_issue, mock_tokens)
        self.assertEqual(token1.value, fixver.return_value)
        self.assertEqual(token2.value, 'RHEL')
        self.assertEqual(token3.value, 'Approved Blocker')

        # Z-Stream versions
        fixver.return_value = 'rhel-8.6.0.z'
        mock_tokens = [token1, token2]
        cpc._load_jira_tokens(mock_issue, mock_tokens)
        self.assertEqual(token1.value, fixver.return_value)
        self.assertEqual(token2.value, 'RHEL')

    def test_load_tokens(self):
        """Sets token value attributes from input bug data."""
        token1 = mock.Mock(type=cpc.TokenType.VAR, value='')
        token1.name = 'internal_target_release'
        token2 = mock.Mock(type=cpc.TokenType.VAR, value='')
        token2.name = 'zstream_target_release'
        token3 = mock.Mock(type=cpc.TokenType.VAR, value='')
        token3.name = 'release'

        # sets the token values from the BZ
        mock_bug = mock.Mock(**self.MOCK_Y_ATTRS)
        mock.seal(mock_bug)
        mock_tokens = [token1, token2, token3]
        cpc._load_tokens(mock_bug, mock_tokens)
        self.assertEqual(token1.value, self.MOCK_Y_ATTRS['cf_internal_target_release'])
        self.assertEqual(token2.value, '')
        self.assertEqual(token3.value, '+')

        # sets the token values from a different bz
        mock_bug = mock.Mock(**self.MOCK_Z_ATTRS)
        mock.seal(mock_bug)
        mock_tokens = [token1, token2, token3]
        cpc._load_tokens(mock_bug, mock_tokens)
        self.assertEqual(token1.value, '')
        self.assertEqual(token2.value, self.MOCK_Z_ATTRS['cf_zstream_target_release'])
        self.assertEqual(token3.value, '?')

    def test_is_bug_ready_optional_parameters(self):
        """On failure includes only the requested parameter checks in the failure string."""
        policy_str = '(release == + and zstream == + and zstream_target_release == 9.0.0)'

        base_bug_attrs = {
            'product': 'Red Hat Enterprise Linux 9',
            'component': 'kernel',
            'status': 'POST',
            'cf_zstream_target_release': '9.0.0',
            'flags': [{'name': 'release', 'status': '?'},  # This causes is_bug_ready to be False.
                      {'name': 'zstream', 'status': '+'}]
        }

        optional_params = {'product': 'Fedora 13',
                           'component': 'NetworkManager',
                           'status': cpc.BZStatus.CLOSED}

        for option_name, bad_value in optional_params.items():
            bug_attrs = deepcopy(base_bug_attrs)
            mock_bug = mock.Mock(spec=bug_attrs.keys(), **bug_attrs)
            with self.subTest(option_name=option_name, option_value=bad_value, bug=mock_bug):
                is_bug_ready_kwargs = dict.fromkeys(optional_params.keys())
                is_bug_ready_kwargs[option_name] = bad_value
                result, fail_reason = cpc.is_bug_ready(
                    mock_bug,
                    cpc._tokenize_string(policy_str),
                    **is_bug_ready_kwargs
                )
                self.assertFalse(result)
                for param in optional_params:
                    assertion_method = self.assertIn if param == option_name else self.assertNotIn
                    assertion_method(f' and {param} ', fail_reason)
