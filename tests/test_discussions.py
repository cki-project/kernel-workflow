"""Tests for the discussions module."""
from unittest import mock

from tests import fake_payloads
from tests.helpers import KwfTestCase
from webhook.discussions import Discussion
from webhook.users import UserCache


class TestDiscussion(KwfTestCase):
    """Tests for the Discussion dataclass."""

    def test_discussion_note_create(self):
        """Returns a Discussion instance with notes from a graphl query result."""
        mock_graphql = mock.Mock()
        user_cache = UserCache(mock_graphql, fake_payloads.PROJECT_PATH_WITH_NAMESPACE)

        notes = [{'id': 'gid://gitlab/Note/1234567',
                  'author': fake_payloads.USER_DICT,
                  'body': 'hello there',
                  'system': False,
                  'updatedAt': '2013-12-03T17:23:34Z'}]
        query_result = {'resolvable': True,
                        'resolved': True,
                        'notes': {'nodes': notes}}

        discussion = Discussion(**query_result, user_cache=user_cache)
        note = discussion.notes[0]

        self.assertEqual(discussion.resolvable, query_result['resolvable'])
        self.assertEqual(discussion.resolved, query_result['resolved'])
        self.assertEqual(len(discussion.notes), 1)

        self.assertEqual(note.id, notes[0]['id'])
        self.assertEqual(note.author.name, fake_payloads.USER_DICT['name'])
        self.assertEqual(note.body, notes[0]['body'])
        self.assertEqual(note.system, notes[0]['system'])
