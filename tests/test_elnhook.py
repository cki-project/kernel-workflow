"""Test for the elnhook."""
import unittest
from unittest import mock

from kwf_lib.common import JiraKey

from tests.helpers import KwfTestCase
from webhook import defs
from webhook import elnhook
from webhook.common import get_arg_parser
from webhook.kwf_tracker import KwfIssueTracker
from webhook.session_events import GitlabMREvent


class TestConfigMR(KwfTestCase):
    """Tests for the elnhook process_config_merge_request function."""

    @mock.patch('webhook.session.SessionRunner.owners', mock.PropertyMock())
    def setUp(self) -> None:
        """Mock a JIRA and GitLab instance and several other objects."""
        super().setUp()

        # Patch out the webhook.session.get_gl_instance function.
        mock_gl_instance = mock.patch('webhook.session.get_gl_instance',
                                      new_callable=mock.Mock())
        self.addCleanup(mock_gl_instance.stop)
        self.mock_jira = mock_gl_instance.start()

        # Patch out the webhook.session.BaseSession.jira property.
        mock_jira = mock.patch('webhook.session.BaseSession.jira',
                               new_callable=mock.PropertyMock(spec_set=['create_issue']))
        self.addCleanup(mock_jira.stop)
        self.mock_jira = mock_jira.start()

        # Patch out the webhook.session.BaseSession.owners property.
        mock_owners = mock.patch('webhook.session.BaseSession.owners',
                                 new_callable=mock.PropertyMock(
                                     spec_set=['get_matching_subsystems'])
                                 )
        self.addCleanup(mock_owners.stop)
        self.mock_owners = mock_owners.start()

        self.mock_gitlab_mr = \
            mock.Mock(spec_set=['save', 'description'], description='MR description')

        namespace = 'group/project'
        mr_id = 123
        self.mr_url = defs.GitlabURL(f'https://gitlab.com/{namespace}/-/merge_requests/{mr_id}')

        # Create a test SessionRunner instance.
        parser = get_arg_parser('ELNHOOK')
        test_args = parser.parse_args('--repo-path /src/foo'.split())
        test_session = self.session_runner('elnhook', test_args, elnhook.HANDLERS)
        self.mock_session = test_session

        # Create a test GitlabMR event with a mock rh_project property.
        test_event = GitlabMREvent(
            test_session,
            headers={'message-type': 'gitlab'},
            body={'object_kind': 'merge_request'}
        )
        test_event.rh_project = mock.Mock(spec_set=['name'])
        test_event.rh_project.name = 'kernel-mock'
        self.mock_event = test_event

        mock_issue = mock.Mock()
        mock_issue.key = 'FAUX-123'
        mock_issue.permalink.return_value = f'https://issues.example.org/{mock_issue.key}'
        self.mock_issue = mock_issue

        # Create a test elnhook.JiraMR instance with the gl_mr property & link_to_tracker methods
        # patched out.
        test_mr = elnhook.JiraMR(
            session=test_session,
            url=self.mr_url,
            source_path=test_args.repo_path,
            _description=self.mock_gitlab_mr.description,
            title='MR summary title.'
        )
        test_mr.gl_mr = self.mock_gitlab_mr
        test_mr.link_to_tracker = mock.Mock(return_value=None)
        self.mock_mr = test_mr

        mock_jira_tracker = KwfIssueTracker(id=JiraKey(mock_issue.key), session=test_session)
        mock_jira_tracker.set_remote_link = mock.Mock()
        mock_jira_tracker.post_comment = mock.Mock()
        mock_jira_tracker.remove_remote_link = mock.Mock()
        mock_jira_tracker.comments = mock.Mock()
        self.mock_jira_tracker = mock_jira_tracker

    @mock.patch('webhook.elnhook.process_config_merge_request', mock.Mock(return_value=None))
    @mock.patch('webhook.jirahook.JiraMR.new')
    def test_webhook_ignore_normal_mr(self, mock_MR) -> None:
        """Test that an MR that isn't a config MR is ignored."""
        mock_mr = self.mock_mr
        mock_mr.source_branch = 'fix_kernel_bug'
        mock_MR.return_value = mock_mr
        elnhook.process_mr_event(dict(), session=self.mock_session, event=self.mock_event)
        elnhook.process_config_merge_request.assert_not_called()

    @mock.patch('webhook.elnhook.process_config_merge_request', mock.Mock(return_value=None))
    @mock.patch('webhook.jirahook.JiraMR.new')
    def test_webhook_for_config_mr(self, mock_MR) -> None:
        """Test that a config MR will be processed."""
        mock_mr = self.mock_mr
        mock_mr.source_branch = 'configs/os-build/test'
        mock_MR.return_value = mock_mr
        elnhook.process_mr_event(dict(), session=self.mock_session, event=self.mock_event)
        elnhook.process_config_merge_request.assert_called_once()

    @mock.patch('webhook.jira.is_production_or_staging', mock.Mock(return_value=True))
    @mock.patch('webhook.elnhook._get_components', mock.Mock(return_value={'kernel / fake'}))
    @mock.patch('webhook.elnhook._fixup_description', mock.Mock(return_value=('Fake description')))
    @mock.patch('webhook.jira.get_issues_with_link')
    @mock.patch('webhook.jirahook.JiraMR.new')
    def test_webhook_jira_already_open(self, mock_MR, mock_get_issues_with_link) -> None:
        """Test that we don't open a jira issue if one is already open."""
        mock_mr = self.mock_mr
        mock_mr.source_branch = 'configs/os-build/test'
        mock_MR.return_value = mock_mr
        mock_get_issues_with_link.return_value = [self.mock_issue]
        elnhook.process_mr_event(dict(), session=self.mock_session, event=self.mock_event)
        self.mock_jira.create_issue.assert_not_called()

    @mock.patch('webhook.jira.is_production_or_staging', mock.Mock(return_value=True))
    @mock.patch('webhook.elnhook._get_components', mock.Mock(return_value={'faux'}))
    @mock.patch('webhook.jira.get_issues_with_link', mock.Mock(return_value=[]))
    @mock.patch('webhook.kwf_tracker.make_trackers')
    @mock.patch('webhook.jirahook.JiraMR.new')
    def test_webhook_open_jira(self, mock_MR, mock_make_trackers) -> None:
        """Test that we open a jira issue, link the MR and update the MR description."""
        mock_mr = self.mock_mr
        mock_mr.source_branch = 'configs/os-build/test'
        mock_MR.return_value = mock_mr
        self.mock_jira.create_issue.return_value = self.mock_issue
        mock_make_trackers.return_value = [self.mock_jira_tracker]
        elnhook.process_mr_event(dict(), session=self.mock_session, event=self.mock_event)
        # check that an issue was created, linked to the MR and that the MR description was updated
        self.mock_jira.create_issue.assert_called_once()
        mock_mr.link_to_tracker.assert_called()
        self.mock_gitlab_mr.save.assert_called_once()
        # check that the MR description was updated to include a JIRA tag
        jira_tag = f'JIRA: {self.mock_issue.permalink.return_value}'
        self.assertTrue(mock_mr.gl_mr.description.startswith(jira_tag))

    @mock.patch('webhook.jira.is_production_or_staging', mock.Mock(return_value=True))
    @mock.patch('webhook.elnhook._get_components', mock.Mock(return_value={'faux', 'kernel'}))
    @mock.patch('webhook.jira.get_issues_with_link', mock.Mock(return_value=[]))
    @mock.patch('webhook.kwf_tracker.make_trackers')
    @mock.patch('webhook.jirahook.JiraMR.new')
    def test_webhook_open_jira_many(self, mock_MR, mock_make_trackers) -> None:
        """Test that we open multiple jira issues. One for each component."""
        mock_mr = self.mock_mr
        mock_mr.source_branch = 'configs/os-build/test'
        mock_MR.return_value = mock_mr
        self.mock_jira.create_issue.return_value = self.mock_issue
        mock_make_trackers.return_value = [self.mock_jira_tracker, self.mock_jira_tracker]
        elnhook.process_mr_event(dict(), session=self.mock_session, event=self.mock_event)
        # check that an issue was created, linked to the MR and that the MR description was updated
        self.assertEqual(self.mock_jira.create_issue.call_count, 2)
        self.assertEqual(mock_mr.link_to_tracker.call_count, 2)
        self.mock_gitlab_mr.save.assert_called_once()
        # check that the MR description was updated to include a JIRA tag
        jira_tag = f'JIRA: {self.mock_issue.permalink.return_value}'
        self.assertTrue(mock_mr.gl_mr.description.startswith(jira_tag))

    def test_is_config_branch(self):
        """Test the config branch check function."""
        self.assertTrue(elnhook._is_config_branch('configs/os-build/test'))
        self.assertFalse(elnhook._is_config_branch('configs/fix_bug'))

    def test_is_new_branch(self):
        """Test the new branch check function."""
        self.assertTrue(elnhook._is_new_branch('0000000000000000000000000000000000000000'))
        self.assertFalse(elnhook._is_new_branch('5939a747fd244bd57084650aacc7bd5ea3bb2e97'))

    def test_fixup_description(self):
        """Test that the jira issue description is modified as expected."""
        msg = 'This message needs fixed up.\nSigned-off-by: Scott Weaver <scweaver@redhat.com>'
        new_msg = elnhook._fixup_description(msg, self.mr_url)
        self.assertTrue(new_msg.endswith(self.mr_url))
        self.assertFalse('Signed-off-by:' in new_msg)

        new_msg = elnhook._fixup_description(msg, None)
        self.assertFalse('Signed-off-by:' in new_msg)

        msg = 'This message needs fixed up.\n'
        new_msg = elnhook._fixup_description(msg, self.mr_url)
        self.assertTrue(new_msg.endswith(self.mr_url))

    @mock.patch('webhook.jira.is_production_or_staging', mock.Mock(return_value=True))
    def test_open_jira(self) -> None:
        """Test open jira isssue wrapper."""
        self.mock_jira.create_issue.return_value = self.mock_issue
        elnhook._open_jira_issue(session=self.mock_session, summary='summary',
                                 desc='description', component='kernel')
        self.mock_jira.create_issue.assert_called()

    @mock.patch('webhook.jira.is_production_or_staging', mock.Mock(return_value=False))
    def test_open_jira_none(self) -> None:
        """Test that we handle not opening an issue."""
        self.mock_jira.create_issue.return_value = self.mock_issue
        issue = elnhook._open_jira_issue(session=self.mock_session, summary='summary',
                                         desc='description', component='kernel')
        self.mock_jira.create_issue.assert_not_called()
        self.assertIsNone(issue)

    @mock.patch('webhook.kconfigs.KconfigManager.new')
    def test_get_components_fix_kernel(self, mock_manager):
        """Test that the get components helper function corrects the bad 'kernel' component."""
        mock_mgr = mock.Mock(return_value='Kconfig.fake')
        mock_manager.return_value = mock_mgr
        mock_subsystem = mock.Mock(spec_set=['jira_component'])
        mock_subsystem.jira_component = 'kernel'
        self.mock_session.owners.get_matching_subsystems.return_value = [mock_subsystem]
        components = elnhook._get_components(self.mock_session, ['CONFIG_FAKE'])
        self.assertEqual(len(components), 1)
        self.assertEqual(components.pop(), 'kernel / Other')

    @mock.patch('webhook.kconfigs.KconfigManager.new')
    def test_get_components(self, mock_manager):
        """Test the get components helper function."""
        mock_mgr = mock.Mock(return_value='Kconfig.fake')
        mock_manager.return_value = mock_mgr
        mock_subsystem = mock.Mock(spec_set=['jira_component'])
        mock_subsystem.jira_component = 'kernel / fake'
        self.mock_session.owners.get_matching_subsystems.return_value = [mock_subsystem]
        components = elnhook._get_components(self.mock_session, ['CONFIG_FAKE'])
        self.assertEqual(len(components), 1)
        self.assertEqual(components.pop(), mock_subsystem.jira_component)

    @mock.patch('webhook.kconfigs.KconfigManager.new')
    def test_get_components_many(self, mock_manager):
        """Test the get components helper function handles multiple SSTs."""
        mock_mgr = mock.Mock(return_value='Kconfig.fake')
        mock_manager.return_value = mock_mgr
        mock_subsystem1 = mock.Mock(spec_set=['jira_component'])
        mock_subsystem1.jira_component = 'kernel / fake'
        mock_subsystem2 = mock.Mock(spec_set=['jira_component'])
        mock_subsystem2.jira_component = 'kernel'
        subsystems = [mock_subsystem1, mock_subsystem2]
        self.mock_session.owners.get_matching_subsystems.return_value = subsystems
        components = elnhook._get_components(self.mock_session, ['CONFIG_FAKE'])
        self.assertEqual(len(components), 2)
        self.assertTrue(mock_subsystem1.jira_component in components)
        self.assertTrue('kernel / Other' in components)

    @mock.patch('webhook.kconfigs.KconfigManager.new')
    def test_get_components_none(self, mock_manager):
        """Test that the get components helper function handles no SSTs."""
        mock_mgr = mock.Mock(return_value='Kconfig.fake')
        mock_manager.return_value = mock_mgr
        self.mock_session.owners.get_matching_subsystems.return_value = []
        components = elnhook._get_components(self.mock_session, ['CONFIG_FAKE'])
        self.assertEqual(len(components), 1)
        self.assertEqual(components.pop(), 'kernel / Other')

    @unittest.skip("The push event handler is currently a WIP.")
    def test_process_config_branch(self):
        """Test the config branch push event handler."""
        elnhook.process_config_branch(None, None)

    @unittest.skip("The push event handler is currently a WIP.")
    def test_process_push_event(self):
        """Test the push event handler."""
        elnhook.process_push_event(None, None)
