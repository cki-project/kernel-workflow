"""Tests for the jirahook."""
from copy import deepcopy
from dataclasses import dataclass
from unittest import mock

from tests import fakes
from tests import fakes_bz
from tests import fakes_jira
from tests.helpers import KwfTestCase
from webhook import defs
from webhook import jirahook
from webhook.base_mr_mixins import DependsMixin
from webhook.bug import Bug
from webhook.graphql import GET_MR_DESCRIPTIONS_QUERY
from webhook.rhissue import RHIssue
from webhook.session import SessionRunner
from webhook.users import User


def create_jirahook_mr(mr_url) -> jirahook.JiraMR:
    """Return a fresh MR object."""
    test_session = SessionRunner.new('jirahook', args='')
    gl_instance = fakes.FakeGitLab()
    gl_instance.user = User(username='cki-kwf-bot')
    test_session.get_gl_instance = mock.Mock(return_value=gl_instance)

    return jirahook.JiraMR.new(test_session, mr_url, source_path='/src/foo', merge_subsystems=True)


@dataclass(repr=False)
class JsonJirahookMR:
    """Simple collection of file names of json data and users to build jirahook.JiraMR from."""

    mr_url: defs.GitlabURL
    details: str
    commits: str
    descriptions: str
    add_rest: bool = False


@mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml',
                                'OWNERS_YAML': 'tests/assets/owners-snapshot.yaml'})
class JiraMRBuilder(KwfTestCase):
    """JiraMR builder helper functions that leverage and extend KwfTestCase helpers."""

    def populate_responses(self, json_jirahook_mr, reset_responses=False) -> None:
        # Make sure we're starting with an empty set of responses, if requested
        if reset_responses:
            self.responses.reset()

        # Standard user authentication
        self.response_gl_auth()
        # Standard GraphQL user data response
        self.response_gql_user_data()

        mr_endpoint = ''
        # Set up the jirahook.JiraMR query responses.
        variables = {'namespace': json_jirahook_mr.mr_url.namespace,
                     'mr_id': str(json_jirahook_mr.mr_url.id)}

        details = self.load_yaml_asset(
            path=json_jirahook_mr.details,
            sub_module='gitlab_graphql_api'
        )
        self.add_query(jirahook.JiraMR.MR_QUERY, variables=variables, query_result=details)

        commits = self.load_yaml_asset(
            path=json_jirahook_mr.commits,
            sub_module='gitlab_graphql_api'
        )
        self.add_query(jirahook.JiraMR.COMMIT_QUERY, variables=variables, query_result=commits)

        descriptions = self.load_yaml_asset(
            path=json_jirahook_mr.descriptions,
            sub_module='gitlab_graphql_api'
        )
        variables = {'namespace': json_jirahook_mr.mr_url.namespace,
                     'mr_ids': [str(json_jirahook_mr.mr_url.id)]}
        self.add_query(GET_MR_DESCRIPTIONS_QUERY, variables=variables, query_result=descriptions)

        if json_jirahook_mr.add_rest:
            proj_name = json_jirahook_mr.mr_url.namespace.split('/')[-1]
            project = self.load_yaml_asset(
                path=f'project-{proj_name}.json',
                sub_module='gitlab_rest_api'
            )

            proj_id = details['data']['project']['mr']['project']['id'].split('/')[-1]
            proj_endpoint = "https://gitlab.com/api/v4/projects"
            proj_path = json_jirahook_mr.mr_url.namespace.replace("/", "%2F")

            self.responses.get(f'{proj_endpoint}/{proj_id}', json=project)
            self.responses.get(f'{proj_endpoint}/{proj_path}', json=project)

            merge_request = self.load_yaml_asset(
                path=f'mr-{json_jirahook_mr.mr_url.id}-{proj_name}.json',
                sub_module='gitlab_rest_api'
            )
            mr_endpoint = f'{proj_endpoint}/{proj_id}/merge_requests/{json_jirahook_mr.mr_url.id}'
            self.responses.get(mr_endpoint, json=merge_request)

        self.response_et_setup()

    def build_cs9_mr(self, mr_id):
        mr_url = defs.GitlabURL('https://gitlab.com/redhat/centos-stream/src/kernel/'
                                f'centos-stream-9/-/merge_requests/{mr_id}')
        proj_name = mr_url.namespace.split('/')[-1]

        details = f'jirahookmr_{proj_name}_{mr_id}-details.json'
        commits = f'jirahookmr_{proj_name}_{mr_id}-commits.json'
        descriptions = f'jirahookmr_{proj_name}_{mr_id}-descriptions.json'

        json_jirahook_mr = JsonJirahookMR(mr_url=mr_url, details=details, commits=commits,
                                          descriptions=descriptions)
        self.populate_responses(json_jirahook_mr)

        return create_jirahook_mr(mr_url)


@mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml',
                                'OWNERS_YAML': 'tests/assets/owners-snapshot.yaml'})
@mock.patch('webhook.rhissue.fetch_issues', mock.Mock(return_value=[]))
@mock.patch('webhook.libbz.fetch_bugs', mock.Mock(return_value=[]))
class TestJiraMR(JiraMRBuilder):
    """Tests for the JiraMR dataclass."""

    @mock.patch('webhook.jirahook.JiraMR.rhissues', new_callable=mock.PropertyMock)
    def test_zstream_clones(self, mock_rhissues):
        base_issue = self.make_jira_issue('RHEL-50203')
        clone_issue = self.make_jira_issue('RHEL-50200')
        base_issue2 = self.make_jira_issue('RHEL-50265')
        clone_issue2 = self.make_jira_issue('RHEL-50264')

        base_rhi = RHIssue.new_from_ji(ji=base_issue, mrs=None)
        clone_rhi = RHIssue.new_from_ji(ji=clone_issue, mrs=None)
        base_rhi2 = RHIssue.new_from_ji(ji=base_issue2, mrs=None)
        clone_rhi2 = RHIssue.new_from_ji(ji=clone_issue2, mrs=None)
        base_rhi.zstream_clones = [clone_rhi]
        base_rhi2.zstream_clones = [clone_rhi2]
        mock_rhissues.return_value = [base_rhi, base_rhi2]

        jmr = self.build_cs9_mr(4809)

        self.assertCountEqual([clone_rhi, clone_rhi2], jmr.zstream_clones)

    @mock.patch.object(DependsMixin, '_set_mr_blocks', mock.Mock())
    @mock.patch('webhook.jirahook.JiraMR.linked_rhissues', new_callable=mock.PropertyMock)
    @mock.patch('webhook.rhissue.fetch_issues')
    def test_rhi_linked_rhissues(self, mock_fetch, mock_linked):
        base_issue = self.make_jira_issue('RHEL-50203')
        linked_issue = self.make_jira_issue('RHEL-50200')
        base_issue2 = self.make_jira_issue('RHEL-50265')
        linked_issue2 = self.make_jira_issue('RHEL-50264')

        mock_fetch.return_value = [base_issue, base_issue2]

        jmr = self.build_cs9_mr(4809)

        linked_rhi = RHIssue.new_from_ji(ji=linked_issue, mrs=[jmr])
        linked_rhi2 = RHIssue.new_from_ji(ji=linked_issue2, mrs=[jmr])
        mock_linked.return_value = [linked_rhi, linked_rhi2]

        self.assertCountEqual([linked_rhi, linked_rhi2], jmr.linked_rhissues)
        self.assertCountEqual([linked_rhi, linked_rhi2], jmr.linked_rhissues_with_scopes)

        cve_bug1 = Bug.new_missing(bz_id='CVE-2024-41090', mrs=[jmr])
        cve_bug2 = Bug.new_missing(bz_id='CVE-2024-41091', mrs=[jmr])
        self.assertCountEqual([cve_bug1, cve_bug2], jmr.cves)
        self.assertCountEqual([cve_bug1, cve_bug2], jmr.cves_with_scopes)

    @mock.patch.object(DependsMixin, '_set_mr_blocks', mock.Mock())
    @mock.patch('webhook.jirahook.RHIssue.linked_rhissues', new_callable=mock.PropertyMock)
    @mock.patch('webhook.jirahook.JiraMR.rhissues', new_callable=mock.PropertyMock)
    def test_mr_linked_rhissues(self, mock_rhissues, mock_linked):
        base_issue = self.make_jira_issue('RHEL-50203')
        linked_issue = self.make_jira_issue('RHEL-50200')

        jmr = self.build_cs9_mr(4809)

        base_rhi = RHIssue.new_from_ji(ji=base_issue, mrs=[jmr])
        mock_rhissues.return_value = [base_rhi]

        linked_rhi = RHIssue.new_from_ji(ji=linked_issue, mrs=[jmr])
        mock_linked.return_value = [linked_rhi]

        self.assertCountEqual([linked_rhi], jmr.linked_rhissues)
        self.assertCountEqual([linked_rhi], jmr.linked_rhissues_with_scopes)

        closed_issue = self.make_jira_issue('RHEL-103')
        closed_rhi = RHIssue.new_from_ji(ji=closed_issue, mrs=[jmr])
        mock_rhissues.return_value = [base_rhi, closed_rhi]
        self.assertCountEqual([closed_rhi], jmr.closed_rhissues)


class TestIssueRow(JiraMRBuilder):
    """Tests for the IssueRow class."""

    def test_issuerow_populate_empty(self):
        """Updates the attribute values of a IssueRow."""
        test_issue = RHIssue.new_missing(ji_id=f'{defs.JPFX}1234567', mrs=[])
        test_issue._jira = mock.Mock(server_url='https://issues.redhat.com')
        test_issuerow = jirahook.IssueRow()
        test_issuerow.populate(test_issue, '')
        exp = f'[{defs.JPFX}1234567](https://issues.redhat.com/browse/{defs.JPFX}1234567) (UNKNOWN)'
        self.assertEqual(test_issuerow.JIRA_Issue, exp)
        self.assertEqual(test_issuerow.CVEs, 'None')
        self.assertEqual(test_issuerow.Commits, 'None')
        self.assertEqual(test_issuerow.Readiness, 'INVALID')
        self.assertEqual(test_issuerow.Policy_Check, 'Check not done: No JIRA Issue')
        self.assertEqual(test_issuerow.Notes, '-')

    @mock.patch('webhook.rhissue.is_rhissue_ready')
    def test_issuerow_populate(self, mock_is_ready):
        """Updates the attribute vales of a IssueRow."""
        mock_is_ready.return_value = True, ""

        jmr = self.build_cs9_mr(4809)

        tun_issue = self.make_jira_issue('RHEL-50203')
        tun_rhi = RHIssue.new_from_ji(ji=tun_issue, mrs=[jmr])
        tun_rhi._jira = mock.Mock(server_url='https://issues.redhat.com')

        tun_issuerow = jirahook.IssueRow()
        tun_issuerow.populate(tun_rhi, [1])

        exp = f'[{defs.JPFX}50203](https://issues.redhat.com/browse/{defs.JPFX}50203) (IN_PROGRESS)'
        self.assertEqual(tun_issuerow.JIRA_Issue, exp)
        self.assertEqual(tun_issuerow.CVEs,
                         '[CVE-2024-41091](https://bugzilla.redhat.com/CVE-2024-41091)<br>')
        self.assertIn('fcfad23917cee945fce060cc16d9a49ca9a13b45', tun_issuerow.Commits)
        self.assertEqual(tun_issuerow.Notes, 'See 1')


@mock.patch('webhook.rhissue.fetch_issues', mock.Mock(return_value=[]))
class TestCveRow(JiraMRBuilder):
    """Tests for the CveRow class."""

    def test_cverow_populate_empty(self):
        """Updates the attribute values of CveRow."""
        test_cve = Bug.new_missing('CVE-2101-21515', mrs=[])
        test_cverow = jirahook.CveRow()
        test_cverow.populate(test_cve, '')
        exp = '[CVE-2101-21515](https://bugzilla.redhat.com/CVE-2101-21515)<br>'
        self.assertEqual(test_cverow.CVEs, exp)
        self.assertEqual(test_cverow.Priority, 'Unknown')
        self.assertEqual(test_cverow.Commits, 'None')
        self.assertEqual(test_cverow.Clones, 'Unknown')
        self.assertEqual(test_cverow.Readiness, 'INVALID')
        self.assertEqual(test_cverow.Notes, '-')

    def test_cverow_populate(self):
        """Updates the attribute values of CveRow."""
        jmr = self.build_cs9_mr(4809)

        test_cve = Bug.new_from_bz(bz=fakes_bz.BZ1999999, mrs=[jmr])

        test_cverow = jirahook.CveRow()
        test_cverow.populate(test_cve, [2, 3])

        exp = '[CVE-2024-41091](https://bugzilla.redhat.com/CVE-2024-41091)<br>'
        self.assertEqual(test_cverow.CVEs, exp)
        self.assertEqual(test_cverow.Priority, 'High')
        self.assertIn('fcfad23917cee945fce060cc16d9a49ca9a13b45', test_cverow.Commits)
        self.assertEqual(test_cverow.Clones, 'None')
        self.assertEqual(test_cverow.Readiness, 'INVALID')
        self.assertEqual(test_cverow.Notes, 'See 2<br>See 3')

    def test_format_clones(self):
        """Returns a string describing the CVE clones, if any."""
        # No parent_mr, returns 'Unknown'
        mock_cve = mock.Mock(parent_mr=None)
        self.assertEqual(jirahook.CveRow._format_Clones(mock_cve), 'Unknown')

        # Not a High Prio, returns 'N/A'
        mock_cve = mock.Mock(parent_mr=True, bz_priority=jirahook.defs.BZPriority.LOW)
        self.assertEqual(jirahook.CveRow._format_Clones(mock_cve), 'N/A')

        # High Prio but RHEL-6, returns 'N/A'
        mock_mr = mock.Mock(rh_project=mock.Mock())
        mock_mr.rh_project.name = 'rhel-6'
        mock_cve = mock.Mock(parent_mr=mock_mr, bz_priority=jirahook.defs.BZPriority.HIGH)
        self.assertEqual(jirahook.CveRow._format_Clones(mock_cve), 'N/A')

        # No ji_depends_on, returns 'None'
        mock_mr = mock.Mock(rh_project=mock.Mock())
        mock_mr.rh_project.name = 'rhel-9'
        mock_cve = mock.Mock(parent_mr=mock_mr, bz_priority=jirahook.defs.BZPriority.URGENT,
                             jira_clones=[])
        self.assertEqual(jirahook.CveRow._format_Clones(mock_cve), 'None')

        # No parent_clone, nothing is **bold**.
        mock_mr.rh_branch = 2
        mock_issue = mock.Mock(ji_branch=mock.Mock(internal_target_release='9.1.0'),
                               ji_status=jirahook.defs.JIStatus.IN_PROGRESS,
                               ji_component='kernel', ji_fix_version='rhel-9.1.0',
                               jira_link='https://issues.redhat.com/browse/RHEL-1234567')
        mock_issue.id = f'{defs.JPFX}1234567'
        mock_cve = mock.Mock(parent_mr=mock_mr, bz_priority=jirahook.defs.BZPriority.URGENT,
                             jira_clones=[mock_issue])
        host = 'https://issues.redhat.com/browse/'
        self.assertEqual(
            jirahook.CveRow._format_Clones(mock_cve),
            f'rhel-9.1.0 (kernel): [{defs.JPFX}1234567]({host}{defs.JPFX}1234567) (IN_PROGRESS)<br>'
        )

        # The parent_clone is the same branch as the MR, clone is **bold**.
        mock_mr.rh_branch = mock_issue.ji_branch
        mock_cve = mock.Mock(parent_mr=mock_mr, bz_priority=jirahook.defs.BZPriority.URGENT,
                             jira_clones=[mock_issue])
        self.assertEqual(
            jirahook.CveRow._format_Clones(mock_cve),
            f'**rhel-9.1.0 (kernel): [{defs.JPFX}1234567]({host}{defs.JPFX}1234567)'
            ' (IN_PROGRESS)**<br>'
        )

    def test_format_jira_issues(self):
        """Returns a string describing the JIRA issues."""
        mock_issue1 = mock.Mock(id=f'{defs.JPFX}1234567',
                                ji_status=jirahook.defs.JIStatus.IN_PROGRESS)

        mock_issue2 = mock.Mock(id=f'{defs.JPFX}1122334',
                                ji_status=defs.JIStatus.CLOSED,
                                ji_resolution=defs.JIResolution.DONEERRATA)

        self.assertEqual(
            jirahook.TagRow._format_JIRA_Issues(mock_issue1),
            f'{defs.JPFX}1234567 (IN_PROGRESS)'
        )
        self.assertEqual(
            jirahook.TagRow._format_JIRA_Issues(mock_issue2),
            f'{defs.JPFX}1122334 (CLOSED: DONEERRATA)'
        )


class TestDepRow(JiraMRBuilder):
    """Tests for the DepRow class."""

    def test_deprow_populate_empty(self):
        """Updates the attribute values of a DepRow."""
        jmr = self.build_cs9_mr(4809)

        dep_issue = self.make_jira_issue('RHEL-50201')
        test_dep = RHIssue.new_from_ji(ji=dep_issue, mrs=[jmr])
        test_dep._jira = mock.Mock(server_url='https://issues.redhat.com')

        test_deprow = jirahook.DepRow()
        test_deprow.populate(test_dep, '')

        self.assertEqual(test_deprow.MR, '!4809 (main)')
        exp = f'[{defs.JPFX}50201](https://issues.redhat.com/browse/{defs.JPFX}50201) (IN_PROGRESS)'
        self.assertEqual(test_deprow.JIRA_Issue, exp)
        self.assertEqual(test_deprow.Readiness, 'INVALID')
        self.assertIn("Check not done", test_deprow.Policy_Check)
        self.assertEqual(test_deprow.Notes, '-')

    @mock.patch('webhook.rhissue.is_rhissue_ready')
    def test_deprow_populate(self, mock_is_ready):
        """Updates the attribute vales of a DepRow."""
        cpc_err = 'release "?" == "+" and internal_target_release "9.1.0" == "9.1.0"'
        mock_is_ready.return_value = False, cpc_err

        jmr = self.build_cs9_mr(4809)

        dep_issue = self.make_jira_issue('RHEL-50200')
        test_dep = RHIssue.new_from_ji(ji=dep_issue, mrs=[jmr])
        test_dep._jira = mock.Mock(server_url='https://issues.redhat.com')

        test_deprow = jirahook.DepRow()

        test_deprow.populate(test_dep, [1, 3, 5])
        self.assertEqual(test_deprow.MR, '!4809 (main)')
        exp = f'[{defs.JPFX}50200](https://issues.redhat.com/browse/{defs.JPFX}50200) (IN_PROGRESS)'
        self.assertEqual(test_deprow.JIRA_Issue, exp)


class TestLinkedIssueRow(KwfTestCase):
    """Tests for the LinkedIssueRow class."""

    @mock.patch('webhook.rhissue.is_rhissue_ready')
    def test_linkedissuerow_populate(self, mock_is_ready):
        """Updates the attribute vales of a LinkedIssueRow."""
        tun_issue = self.make_jira_issue('RHEL-50201')
        tun_rhi = RHIssue.new_from_ji(ji=tun_issue, mrs=[])
        tun_rhi._jira = mock.Mock(server_url='https://issues.redhat.com')

        test_linkedissuerow = jirahook.LinkedIssueRow()
        test_linkedissuerow.populate(tun_rhi, '')

        exp = (f'[{defs.JPFX}50201](https://issues.redhat.com/browse/{defs.JPFX}50201) '
               '(BUG: IN_PROGRESS)')
        exp_cves = '[CVE-2024-41091](https://bugzilla.redhat.com/CVE-2024-41091)<br>'
        self.assertEqual(test_linkedissuerow.JIRA_Issue, exp)
        self.assertEqual(test_linkedissuerow.CVEs, exp_cves)
        self.assertEqual(test_linkedissuerow.Component, 'kernel')
        self.assertEqual(test_linkedissuerow.Notes, '-')


@mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml',
                                'OWNERS_YAML': 'tests/assets/owners-snapshot.yaml'})
class TestMisc(JiraMRBuilder):
    """Tests for misc functions."""

    def test_find_needed_footnotes(self):
        """Returns a dict of testnames and footnote messages from the list of tag items."""
        issue1 = RHIssue.new_missing(ji_id=f'{defs.JPFX}1', mrs=[])
        issue2 = RHIssue.new_missing(ji_id=f'{defs.JPFX}2', mrs=[])
        issue3 = RHIssue.new_missing(ji_id=f'{defs.JPFX}3', mrs=[])
        issue4 = RHIssue.new_missing(ji_id=f'{defs.JPFX}4', mrs=[])
        issue1.failed_tests = ['BranchMatches']
        issue2.failed_tests = ['JIisNotClosed', 'PrelimTestingPass']
        issue3.failed_tests = []
        issue4.failed_tests = ['PrelimTestingPass', 'CommitPolicyApproved']

        result = jirahook.find_needed_footnotes([issue1, issue2, issue3, issue4])
        self.assertEqual(len(result), 4)
        self.assertTrue('BranchMatches' in result)
        self.assertTrue('JIisNotClosed' in result)
        self.assertTrue('PrelimTestingPass' in result)
        self.assertTrue('CommitPolicyApproved' in result)

    def test_create_ji_table(self):
        """Creates a Table from the list of items with the given type of Row class."""
        issue = self.make_jira_issue('RHEL-50203')
        clone_issue = self.make_jira_issue('RHEL-50200')
        clone_issue2 = self.make_jira_issue('RHEL-50201')
        rhi = RHIssue.new_from_ji(ji=issue, mrs=None)
        rhi._jira = mock.Mock(server_url='https://issues.redhat.com')
        clone_rhi = RHIssue.new_from_ji(ji=clone_issue, mrs=None)
        clone_rhi._jira = mock.Mock(server_url='https://issues.redhat.com')
        clone_rhi2 = RHIssue.new_from_ji(ji=clone_issue2, mrs=None)
        clone_rhi2._jira = mock.Mock(server_url='https://issues.redhat.com')

        test_items = [rhi, clone_rhi, clone_rhi2]
        results = jirahook.create_ji_table(jirahook.IssueRow, test_items)
        self.assertEqual(len(results), 3)

    @mock.patch('webhook.cpc.is_rhissue_ready')
    @mock.patch('webhook.rhissue.fetch_issues', mock.Mock(return_value=[]))
    def test_generate_comment(self, mock_is_ready):
        """Returns a string of markdown that will render the MR status and Tag tables."""
        cpc_err = 'release "?" == "+" and internal_target_release "9.1.0" == "9.1.0"'
        mock_is_ready.return_value = False, cpc_err

        jmr = self.build_cs9_mr(4809)

        base_issue = self.make_jira_issue('RHEL-50203')
        dep_issue = self.make_jira_issue('RHEL-50201')
        linked_issue = self.make_jira_issue('RHEL-50200')

        base_rhi = RHIssue.new_from_ji(ji=base_issue, mrs=[jmr])
        base_rhi._jira = mock.Mock(server_url='https://issues.redhat.com')
        dep_rhi = RHIssue.new_from_ji(ji=dep_issue, mrs=[jmr])
        dep_rhi._jira = mock.Mock(server_url='https://issues.redhat.com')
        linked_rhi = RHIssue.new_linked_ji(ji=linked_issue, mrs=[jmr])
        linked_rhi._jira = mock.Mock(server_url='https://issues.redhat.com')
        cve_items = [Bug.new_from_bz(bz=fakes_bz.BZ1999999, mrs=[jmr])]

        issue_table = jirahook.create_ji_table(jirahook.IssueRow, [base_rhi])
        dep_table = jirahook.create_ji_table(jirahook.DepRow, [dep_rhi])
        cve_table = jirahook.create_ji_table(jirahook.CveRow, cve_items)
        linked_issue_table = jirahook.create_ji_table(jirahook.LinkedIssueRow, [linked_rhi])

        mr_scope = defs.MrScope.READY_FOR_QA
        tables = (issue_table, dep_table, cve_table, linked_issue_table)
        mock_branch = mock.Mock(spec_set=['name'])
        mock_branch.name = '9.1'
        mock_mr = mock.Mock(spec_set=['is_build_mr', 'rh_branch'],
                            is_build_mr=False, rh_branch=mock_branch)
        comment = jirahook.generate_comment(mock_mr, mr_scope, tables)
        self.assertIn('Target Branch: 9.1', comment)
        self.assertIn('**passes**', comment)
        self.assertIn('JIRA Issue tags', comment)
        self.assertIn('Depends tags', comment)
        self.assertIn('CVE tags', comment)
        self.assertIn('Linked JIRA Issues', comment)

        # Simulate a build MR
        cve_table = jirahook.create_ji_table(jirahook.CveRow, [])
        linked_issue_table = jirahook.create_ji_table(jirahook.LinkedIssueRow, [])
        tables = (issue_table, dep_table, cve_table, linked_issue_table)
        mock_mr = mock.Mock(spec_set=['is_build_mr', 'rh_branch'],
                            is_build_mr=True, rh_branch=mock_branch)
        comment = jirahook.generate_comment(mock_mr, mr_scope, tables)
        self.assertIn('Target Branch: 9.1', comment)
        self.assertIn('**passes**', comment)
        self.assertIn('evaluation done on Build MRs is somewhat limited', comment)
        self.assertNotIn('CVE tags', comment)
        self.assertNotIn('Linked JIRA Issues', comment)

    @mock.patch('webhook.rhissue.RHIssue.spawn_testing_tasks')
    def test_spawn_testing_tasks(self, mock_spawn):
        """Iterate over issues, call issue-specific spawn_testing_task."""
        test_issue = self.make_jira_issue('RHEL-101')
        test_rhi = RHIssue.new_from_ji(ji=test_issue, mrs=[])
        mock_jmr = mock.Mock()
        mock_jmr.rhissues = [test_rhi]
        mock_jmr.test_variants = ['kernel-foobar']
        mock_jmr.draft = False
        jirahook.JiraMR.spawn_testing_tasks(mock_jmr)
        mock_spawn.assert_called_with(['kernel-foobar'])

    @mock.patch('webhook.rhissue.RHIssue.spawn_testing_tasks')
    def test_spawn_testing_tasks_as_draft(self, mock_spawn):
        """Iterate over issues, call issue-specific spawn_testing_task."""
        test_issue = self.make_jira_issue('RHEL-101')
        test_rhi = RHIssue.new_from_ji(ji=test_issue, mrs=[])
        mock_jmr = mock.Mock()
        mock_jmr.rhissues = [test_rhi]
        mock_jmr.test_variants = ['kernel-foobar']
        mock_jmr.draft = True
        jirahook.JiraMR.spawn_testing_tasks(mock_jmr)
        mock_spawn.assert_not_called()

    @mock.patch('webhook.common.add_merge_request_to_milestone', mock.Mock())
    @mock.patch('webhook.session.get_gl_instance')
    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.session.SessionRunner.update_webhook_comment')
    def test_update_statuses_comment_with_labels(self, mock_update_comment,
                                                 mock_add_label, mock_get_gl_instance):
        """A comment with labels."""
        jmr = self.build_cs9_mr(4809)
        labels = ['JIRA::OK']
        jmr._labels = labels
        jmr.labels_to_add = labels
        mock_add_label.return_value = labels + ['readyForMerge']

        comment = 'a good comment'
        mock_issues = mock.Mock()

        # Post Comment is True and we have labels.
        jmr.update_statuses(mock_issues, comment)
        mock_update_comment.assert_called_once_with(jmr.gl_mr, comment,
                                                    bot_name=jmr.session.gl_user.username,
                                                    identifier=jirahook.COMMENT_TITLE)
        mock_add_label.assert_called_once_with(jmr.gl_project, 4809, labels, remove_scoped=False)

    @mock.patch('webhook.common.add_merge_request_to_milestone', mock.Mock())
    @mock.patch('webhook.session.get_gl_instance')
    @mock.patch('webhook.common.remove_labels_from_merge_request')
    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.session.SessionRunner.update_webhook_comment')
    def test_update_statuses_comment_with_labels_to_remove(self,
                                                           mock_update_comment,
                                                           mock_add_label,
                                                           mock_remove_labels,
                                                           mock_get_gl_instance):
        """A comment with labels."""
        jmr = self.build_cs9_mr(4809)
        jmr._labels = ['JIRA::OK', 'readyForQA']
        jmr.labels_to_remove = ['readyForQA']
        jmr.labels_to_add = ['readyForMerge']
        mock_add_label.return_value = ['JIRA::OK', 'readyForMerge']
        mock_remove_labels.return_value = ['JIRA::OK']

        comment = 'a good comment'
        mock_issues = mock.Mock()

        # Post Comment is True and we have labels.
        jmr.update_statuses(mock_issues, comment)
        mock_update_comment.assert_called_once_with(jmr.gl_mr, comment,
                                                    bot_name=jmr.session.gl_user.username,
                                                    identifier=jirahook.COMMENT_TITLE)
        mock_add_label.assert_called_once_with(jmr.gl_project, 4809, ['readyForMerge'],
                                               remove_scoped=False)
        mock_remove_labels.assert_called_once_with(jmr.gl_project, 4809, ['readyForQA'])

    def test_sort_labels(self):
        """Make sure we get back expected add/remove label values."""
        rhissue_id = f'{defs.JPFX}1234567'
        mock_mr = mock.Mock(commits=True,
                            description=mock.Mock(bugzilla=False, jira_tags={rhissue_id}),
                            first_dep_sha='',
                            draft=True,
                            labels=['JIRA::InProgress', 'Severity::Moderate'],
                            state=jirahook.defs.MrState.OPENED)
        mock_issue = mock.Mock(ji_cves=[],
                               policy_check_ok=(False, 'Nope'),
                               commits=[],
                               failed_tests=[],
                               testing_tasks=[],
                               mr=mock_mr,
                               scope=jirahook.defs.MrScope.READY_FOR_MERGE,
                               ji_priority=jirahook.defs.JIPriority.MAJOR,
                               ji_pt_status=defs.JIPTStatus.PASS,
                               test_list=[])
        # CVE is a Bug object
        mock_cve = mock.Mock(test_list=[],
                             failed_tests=[],
                             cve_ids=['CVE-2022-1234'],
                             commits=['1a2b3c4d5e6f'],
                             scope=jirahook.defs.MrScope.READY_FOR_MERGE,
                             bz_priority=jirahook.defs.BZPriority.MEDIUM,
                             severity=jirahook.defs.IssueSeverity.IMPORTANT)

        mock_issue.id = rhissue_id
        mock_mr.rhissues = [mock_issue]
        mock_mr.labels_to_add = []
        mock_mr.labels_to_remove = []
        mock_mr.linked_rhissues = []
        mock_mr.rhissues_with_scopes = [mock_issue]
        mock_mr.cves_with_scopes = [mock_cve]
        mock_mr.linked_rhissues_with_scopes = []
        mock_mr.test_variants = []
        mock_mr.closed_rhissues = []

        jirahook.JiraMR.sort_labels(mock_mr, defs.MrScope.READY_FOR_MERGE)
        adds = ['Severity::Important', 'Time Critical', 'JIRA::OK']
        self.assertCountEqual(mock_mr.labels_to_add, adds)
        self.assertCountEqual(mock_mr.labels_to_remove, ['Severity::Moderate'])

        mock_mr.labels = ['JIRA::InProgress']
        mock_task = mock.Mock(test_list=[], failed_tests=['PrelimTestingPass'],
                              mr=mock_mr, ji_status=defs.JIStatus.IN_PROGRESS,
                              ji_component='kernel-rt')
        mock_issue.testing_tasks = [mock_task]
        jirahook.JiraMR.sort_labels(mock_mr, defs.MrScope.IN_PROGRESS)
        adds = ['Severity::Important', 'Time Critical',
                'JIRA::InProgress', 'kernel-rt testing::Requested']
        self.assertCountEqual(mock_mr.labels_to_add, adds)
        self.assertCountEqual(mock_mr.labels_to_remove, [])

        mock_mr.labels = ['JIRA::InProgress', 'kernel-rt testing::Requested']
        mock_task.ji_status = defs.JIStatus.CLOSED
        mock_task.failed_tests = []
        jirahook.JiraMR.sort_labels(mock_mr, defs.MrScope.READY_FOR_MERGE)
        adds = ['Severity::Important', 'Time Critical', 'JIRA::OK']
        self.assertCountEqual(mock_mr.labels_to_add, adds)
        self.assertCountEqual(mock_mr.labels_to_remove, ['kernel-rt testing::Requested'])

    @mock.patch('cki_lib.misc.is_staging')
    def test_valid_project_for_env(self, mock_is_staging):
        """See if we recognize valid/invalid project/env combinations."""

        mock_is_staging.return_value = False
        mock_rhissue = RHIssue.new_from_ji(ji=deepcopy(fakes_jira.JI8787878))
        mock_rhissue.allow_confidential = False
        proj = mock_rhissue.ji_project
        self.assertFalse(proj.confidential)
        self.assertFalse(proj.sandbox)

        mock_is_staging.return_value = True
        mock_rhissue = RHIssue.new_from_ji(ji=deepcopy(fakes_jira.JI8787878))
        mock_rhissue.allow_confidential = False
        proj = mock_rhissue.ji_project
        self.assertFalse(proj.confidential)
        self.assertTrue(proj.sandbox)

        mock_rhissue.allow_confidential = True
        proj = mock_rhissue.ji_project
        self.assertTrue(proj.confidential)
        self.assertTrue(proj.sandbox)


@mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml',
                                'OWNERS_YAML': 'tests/assets/owners-snapshot.yaml'})
class TestProcessMR(KwfTestCase):
    """Tests for process_mr function."""

    @mock.patch('webhook.jirahook.JiraMR.new')
    def test_process_mr_skip_funny(self, mock_MR):
        """Skips MRs with nothing to do."""
        namespace = 'group/project'
        mr_id = 123
        mr_url = defs.GitlabURL(f'https://gitlab.com/{namespace}/-/merge_requests/{mr_id}')
        mock_session = mock.Mock()
        mock_session.graphql = mock.Mock()
        mock_session.rh_projects = mock.Mock()
        mock_session.args.repo_path = '/src/foo'

        # Nothing to do with an MR with no Description.
        mock_mr = mock.Mock(state=jirahook.defs.MrState.OPENED, description=None, commits=True)
        mock_MR.return_value = mock_mr
        jirahook.process_mr(mock_session, mr_url)
        mock_MR.assert_called_with(mock_session, mr_url, source_path='/src/foo',
                                   merge_subsystems=True)

        # Nothing to do if there are no commits.
        mock_mr = mock.Mock(state=jirahook.defs.MrState.OPENED, description=True, commits=[])
        mock_MR.return_value = mock_mr
        jirahook.process_mr(mock_session, mr_url)
        mock_MR.assert_called_with(mock_session, mr_url, source_path='/src/foo',
                                   merge_subsystems=True)

    @mock.patch('webhook.libjira.add_gitlab_link_in_issues', mock.Mock(return_value=True))
    @mock.patch('webhook.libjira.move_issue_states_forward')
    @mock.patch('webhook.jirahook.JiraMR.update_statuses')
    @mock.patch('webhook.jirahook.JiraMR.new')
    def test_process_mr(self, mock_MR, mock_update, mock_issue_forward):
        """Does not update JIRA Issues or MR labels but leaves a comment."""
        mock_args = mock.Mock()
        mock_session = SessionRunner.new('jirahook', mock_args, jirahook.HANDLERS)
        username = 'user1'
        namespace = 'group/project'
        mr_id = 123
        mr_url = defs.GitlabURL(f'https://gitlab.com/{namespace}/-/merge_requests/{mr_id}')
        rhissue_id = f'{defs.JPFX}1234567'

        mock_session.get_graphql = mock.Mock()
        mock_session.get_graphql.return_value.username = username
        mock_session.is_production_or_staging = True
        mock_mr = mock.Mock(commits=True,
                            description=mock.Mock(bugzilla=False, jira_tags={rhissue_id}),
                            first_dep_sha='',
                            draft=False,
                            labels=[],
                            state=jirahook.defs.MrState.OPENED)
        mock_issue = mock.Mock(ji_cves=[],
                               policy_check_ok=(False, 'Nope'),
                               commits=[],
                               failed_tests=[],
                               mr=mock_mr,
                               scope=jirahook.defs.MrScope.READY_FOR_MERGE,
                               ji_component='kernel-rt',
                               ji_priority=jirahook.defs.JIPriority.NORMAL,
                               subsystems_with_test_variants=[],
                               test_variants=[],
                               labels=[],
                               test_list=[])
        mock_issue.id = rhissue_id
        mock_mr.rhissues = [mock_issue]
        mock_mr.linked_rhissues = []
        mock_mr.rhissues_with_scopes = mock_mr.rhissues
        mock_mr.cves_with_scopes = []
        mock_mr.linked_rhissues_with_scopes = [mock_issue]
        mock_mr.update_statuses = mock_update
        mock_mr.test_variants = {'kernel-foobar'}
        mock_mr.closed_rhissues = []
        mock_MR.return_value = mock_mr
        mock_gl = fakes.FakeGitLab()
        mock_project = mock_gl.add_project(45678, namespace)
        mock_project.add_mr(mr_id, actual_attributes={'description': 'hello',
                                                      'head_pipeline': {'id': 54321},
                                                      'path_with_namespace': namespace,
                                                      'target_branch': 'main'})
        mock_session.get_gl_instance = mock.Mock(return_value=mock_gl)
        mock_session.rh_projects = mock.Mock()

        jirahook.process_mr(mock_session, mr_url)
        mock_issue_forward.assert_called_once_with([mock_issue.ji])
        mock_update.assert_called_once_with(mock.ANY, mock.ANY)

    @mock.patch('webhook.libjira.move_issue_states_forward')
    @mock.patch('webhook.jirahook.JiraMR.update_statuses')
    @mock.patch('webhook.jirahook.JiraMR.new')
    def test_process_mr_with_draft(self, mock_MR, mock_update, mock_issue_forward):
        """Does not update JIRA Issues or MR labels but leaves a comment."""
        mock_args = mock.Mock()
        mock_session = SessionRunner.new('jirahook', mock_args, jirahook.HANDLERS)
        username = 'user1'
        namespace = 'group/project'
        mr_id = 123
        mr_url = defs.GitlabURL(f'https://gitlab.com/{namespace}/-/merge_requests/{mr_id}')
        rhissue_id = f'{defs.JPFX}1234567'

        mock_session.get_graphql = mock.Mock()
        mock_session.get_graphql.return_value.username = username
        mock_mr = mock.Mock(commits=True,
                            description=mock.Mock(bugzilla=False, jira_tags={rhissue_id}),
                            first_dep_sha='',
                            draft=True,
                            labels=['Time Critical'],
                            state=jirahook.defs.MrState.OPENED)
        mock_issue = mock.Mock(ji_cves=[],
                               policy_check_ok=(False, 'Nope'),
                               commits=[],
                               failed_tests=[],
                               mr=mock_mr,
                               scope=jirahook.defs.MrScope.READY_FOR_MERGE,
                               ji_priority=jirahook.defs.JIPriority.NORMAL,
                               test_list=[])
        mock_issue.id = rhissue_id
        mock_mr.rhissues = [mock_issue]
        mock_mr.linked_rhissues = []
        mock_mr.rhissues_with_scopes = mock_mr.rhissues
        mock_mr.cves_with_scopes = []
        mock_mr.linked_rhissues_with_scopes = []
        mock_mr.update_statuses = mock_update
        mock_mr.test_variants = []
        mock_mr.closed_rhissues = []
        mock_MR.return_value = mock_mr
        mock_session.rh_projects = mock.Mock()

        jirahook.process_mr(mock_session, mr_url)
        mock_issue_forward.assert_not_called()
        mock_update.assert_called_once_with(mock.ANY, mock.ANY)

    @mock.patch('webhook.libjira.move_issue_states_forward')
    @mock.patch('webhook.jirahook.JiraMR.update_statuses')
    @mock.patch('webhook.jirahook.JiraMR.new')
    def test_process_mr_with_cve(self, mock_MR, mock_update, mock_issue_forward):
        """Does not update JIRA Issues or MR labels but leaves a comment and check CVE."""
        mock_args = mock.Mock()
        mock_session = SessionRunner.new('jirahook', mock_args, jirahook.HANDLERS)
        username = 'user1'
        namespace = 'group/project'
        mr_id = 123
        mr_url = defs.GitlabURL(f'https://gitlab.com/{namespace}/-/merge_requests/{mr_id}')
        rhissue_id = f'{defs.JPFX}1234567'

        mock_session.get_graphql = mock.Mock()
        mock_session.get_graphql.return_value.username = username
        mock_mr = mock.Mock(commits=True,
                            description=mock.Mock(bugzilla=False, jira_tags={rhissue_id}),
                            first_dep_sha='',
                            draft=True,
                            labels=['Severity::Moderate'],
                            state=jirahook.defs.MrState.OPENED)
        mock_issue = mock.Mock(ji_cves=[],
                               policy_check_ok=(False, 'Nope'),
                               commits=[],
                               failed_tests=[],
                               mr=mock_mr,
                               scope=jirahook.defs.MrScope.READY_FOR_MERGE,
                               ji_priority=jirahook.defs.JIPriority.MAJOR,
                               test_list=[])
        # CVE is a Bug object
        mock_cve = mock.Mock(test_list=[],
                             failed_tests=[],
                             cve_ids=['CVE-2022-1234'],
                             commits=['1a2b3c4d5e6f'],
                             scope=jirahook.defs.MrScope.READY_FOR_MERGE,
                             bz_priority=jirahook.defs.BZPriority.MEDIUM,
                             severity=jirahook.defs.IssueSeverity.IMPORTANT)

        mock_issue.id = rhissue_id
        mock_mr.rhissues = [mock_issue]
        mock_mr.linked_rhissues = []
        mock_mr.rhissues_with_scopes = mock_mr.rhissues
        mock_mr.cves_with_scopes = [mock_cve]
        mock_mr.linked_rhissues_with_scopes = []
        mock_mr.update_statuses = mock_update
        mock_mr.test_variants = []
        mock_mr.closed_rhissues = []
        mock_MR.return_value = mock_mr
        mock_session.rh_projects = mock.Mock()

        jirahook.process_mr(mock_session, mr_url)
        mock_issue_forward.assert_not_called()
        mock_update.assert_called_once_with(mock.ANY, mock.ANY)
