"""Webhook interaction tests."""
from datetime import datetime
from subprocess import CalledProcessError
from subprocess import CompletedProcess
from unittest import mock

from cki_lib import messagequeue

from tests.helpers import KwfTestCase
from webhook import kgit


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestKGit(KwfTestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._mocked_runs = []
        self._mocked_calls = []

    def _mock_run(self, args, *, check=False, **_):
        for run in self._mocked_runs:
            if run[0] == args:
                (returncode, stdout) = run[1:]
                break
        else:
            self.fail(f'Command {args} not found in mocked subprocess.run')
        self._mocked_calls.append(args)
        if returncode:
            raise CalledProcessError(returncode, args, output=stdout)
        return CompletedProcess(args, returncode, stdout=stdout)

    def _add_run_result(self, args, returncode, stdout=None):
        self._mocked_runs.append((args, returncode, stdout))

    @mock.patch('webhook.kgit._git')
    def test_git_remotes(self, mock_git_remotes):
        mock_sub_ret = mock.Mock()
        mock_sub_ret.stdout = "kernel-ark\nlinus\norigin"
        mock_git_remotes.return_value = mock_sub_ret
        ret = kgit.remotes('/src/linux')
        self.assertEqual(ret, ['kernel-ark', 'linus', 'origin'])

    @mock.patch('webhook.kgit.fetch_remote')
    @mock.patch('webhook.kgit.remotes')
    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_git_fetch_all(self, mock_remotes, mock_fetch):
        location = '/src/kernel-ark/'
        mock_remotes.return_value = ['kernel-ark', 'linus']
        kgit.fetch_all(location)
        self.assertEqual(mock_fetch.call_count, 2)

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_git_pull_remote_branch(self):
        location = '/src/kernel-ark/'
        remote = 'kernel-ark'
        branch = 'os-build'
        with self.assertLogs('cki.webhook.kgit', level='DEBUG') as logs:
            kgit.pull_remote_branch(location, remote, branch)
            self.assertIn(f'git pull {remote} {branch}', logs.output[-1])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_git_branch_copy(self):
        location = '/src/kernel-ark/'
        branch = 'os-build'
        with self.assertLogs('cki.webhook.kgit', level='DEBUG') as logs:
            kgit.branch_copy(location, branch)
            self.assertIn(f'git branch --copy {branch}', logs.output[-1])

    def test_git_branches_differ_false(self):
        location = '/src/kernel-ark/'
        branch_a = 'os-build'
        branch_b = 'os-build-rt'
        m_args = ['git', 'diff', '--stat', branch_a, branch_b]
        self._add_run_result(m_args, 0, stdout=None)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = kgit.branches_differ(location, branch_a, branch_b)
            self.assertFalse(ret)

    def test_git_branches_differ(self):
        location = '/src/kernel-ark/'
        branch_a = 'os-build'
        branch_b = 'os-build-rt'
        m_args = ['git', 'diff', '--stat', branch_a, branch_b]
        self._add_run_result(m_args, 0, stdout=b'There was a diff')
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = kgit.branches_differ(location, branch_a, branch_b)
            self.assertTrue(ret)

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_git_checkout(self):
        location = '/src/kernel-ark/'
        branch = 'os-build'
        with self.assertLogs('cki.webhook.kgit', level='DEBUG') as logs:
            kgit.checkout(location, branch)
            self.assertIn(f'git checkout {branch}', logs.output[-1])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_git_reset(self):
        location = '/src/kernel-ark/'
        branch = 'os-build'
        with self.assertLogs('cki.webhook.kgit', level='DEBUG') as logs:
            kgit.hard_reset(location, branch)
            self.assertIn(f'git reset --hard {branch}', logs.output[-1])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_git_add(self):
        location = '/src/kernel-ark/'
        path = 'some/file.c'
        with self.assertLogs('cki.webhook.kgit', level='DEBUG') as logs:
            kgit.add(location, path)
            self.assertIn(f'git add {path}', logs.output[-1])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_git_commit(self):
        location = '/src/kernel-ark/'
        msg = 'commit this stuff'
        with self.assertLogs('cki.webhook.kgit', level='DEBUG') as logs:
            kgit.commit(location, msg)
            self.assertIn(f'git commit -s -m {msg}', logs.output[-1])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_git_raw_diff(self):
        location = '/src/kernel-ark/'
        base = None
        path = 'some/file.c'
        with self.assertLogs('cki.webhook.kgit', level='DEBUG') as logs:
            kgit.raw_diff(location, base, path)
            self.assertIn(f'git diff {path}', logs.output[-1])
        base = 'main'
        with self.assertLogs('cki.webhook.kgit', level='DEBUG') as logs:
            kgit.raw_diff(location, base, path)
            self.assertIn(f'git diff {base} -- {path}', logs.output[-1])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_git_merge(self):
        location = '/src/kernel-ark/'
        reference = 'origin/merge-requests/66'
        with self.assertLogs('cki.webhook.kgit', level='DEBUG') as logs:
            kgit.merge(location, reference)
            self.assertIn(f'git merge --quiet --no-edit {reference}', logs.output[-1])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_git_cherry_pick(self):
        location = '/src/kernel-ark/'
        reference = 'abcdef012345'
        with self.assertLogs('cki.webhook.kgit', level='DEBUG') as logs:
            kgit.cherry_pick(location, reference)
            self.assertIn(f'git cherry-pick -x --signoff {reference}', logs.output[-1])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_git_branch_delete(self):
        location = '/src/kernel-ark/'
        branch = 'os-build'
        with self.assertLogs('cki.webhook.kgit', level='DEBUG') as logs:
            kgit.branch_delete(location, branch)
            self.assertIn(f'git branch -D {branch}', logs.output[-1])

    @mock.patch('subprocess.run', mock.Mock())
    def test_setup_git_user(self):
        name = "backport bot"
        email = "backport-bot@example.com"
        with self.assertLogs('cki.webhook.kgit', level='DEBUG') as logs:
            kgit.setup_git_user('/src/', name, email)
            self.assertIn("git config user.name backport bot", logs.output[-2])
            self.assertIn("git config user.email backport-bot@example.com", logs.output[-1])

    @mock.patch('webhook.kgit.hard_reset', mock.Mock(return_value=True))
    def test_branch_mergeable_false(self):
        worktree_dir = '/src/kernel-ark-temp-merge-branch/'
        target_branch = 'kernel-ark/os-build'
        merge_branch = 'temp-merge-branch'
        m_args = ['git', 'merge', '--quiet', '--no-edit', merge_branch]
        self._add_run_result(m_args, 5, 'Catastrophic error!')
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = kgit.branch_mergeable(worktree_dir, target_branch, merge_branch)
            self.assertFalse(ret)

    @mock.patch('webhook.kgit.hard_reset', mock.Mock(return_value=True))
    def test_branch_mergeable(self):
        worktree_dir = '/src/kernel-ark-temp-merge-branch/'
        target_branch = 'kernel-ark/os-build'
        merge_branch = 'temp-merge-branch'
        m_args = ['git', 'merge', '--quiet', '--no-edit', merge_branch]
        self._add_run_result(m_args, 0)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = kgit.branch_mergeable(worktree_dir, target_branch, merge_branch)
            self.assertTrue(ret)

    @mock.patch('webhook.kgit.create_worktree_timestamp', mock.Mock(return_value=True))
    @mock.patch('os.path.exists', mock.Mock(return_value=False))
    @mock.patch('webhook.kgit.clean_up_temp_merge_branch')
    def test_handle_stale_worktree(self, clean_up):
        rhkernel_src = '/src/kernel'
        merge_branch = 'kernel-ark/os-build'
        worktree_dir = '/tmp/worktree'
        clean_up.return_value = True

        m_args = ['git', 'branch', '-D', f'{merge_branch}-save']
        self._add_run_result(m_args, 4, 'Uhhh yeah it exploded')
        with mock.patch('builtins.open', mock.mock_open(read_data='20200504112233')) as m:
            with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
                kgit.handle_stale_worktree(rhkernel_src, merge_branch, worktree_dir)
            m.assert_called_once_with(f'{worktree_dir}/timestamp', 'r', encoding='ascii')
            clean_up.assert_called_once()

        timestamp = datetime.now().strftime('%Y%m%d%H%M%S')
        clean_up.call_count = 0
        with mock.patch('builtins.open', mock.mock_open(read_data=timestamp)) as m2:
            with self.assertRaises(messagequeue.QuietNackException):
                kgit.handle_stale_worktree(rhkernel_src, merge_branch, worktree_dir)
            m2.assert_called_once_with(f'{worktree_dir}/timestamp', 'r', encoding='ascii')
            clean_up.assert_not_called()

    def test_create_worktree_timestamp(self):
        worktree_dir = '/tmp/worktree'
        with mock.patch('builtins.open', mock.mock_open()) as m:
            kgit.create_worktree_timestamp(worktree_dir)
            m.assert_called_once_with(f'{worktree_dir}/timestamp', 'w', encoding='ascii')
            handle = m()
            handle.write.assert_called_once()

    @mock.patch('os.path.isdir', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.handle_stale_worktree')
    @mock.patch('webhook.kgit.worktree_add')
    def test_prep_temp_merge_branch(self, mock_add, mock_stale):
        remote = 'foobar'
        mock_mr = mock.Mock(iid='1234', target_branch='main')
        kernel_src = '/src/linux'
        ret_branch, ret_dir = kgit.prep_temp_merge_branch(remote, mock_mr, kernel_src)
        mock_stale.assert_called_once()
        mock_add.assert_called_once()
        self.assertEqual(ret_branch, 'foobar-main-1234')
        self.assertEqual(ret_dir, '/src/foobar-main-1234-merge/')

    @mock.patch('webhook.kgit.branch_delete', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.worktree_remove', mock.Mock(return_value=True))
    def test_clean_up_temp_merge_branch(self):
        worktree_dir = '/src/kernel-ark-temp-merge-branch/'
        merge_branch = 'temp-merge-branch'
        with self.assertLogs('cki.webhook.kgit', level='DEBUG') as logs:
            kgit.clean_up_temp_merge_branch('mocked', merge_branch, worktree_dir)
            self.assertIn(f'Removed worktree {worktree_dir} and deleted branch {merge_branch}',
                          logs.output[-1])

    @mock.patch('subprocess.run')
    def test_get_remote_name(self, mock_subprocess_run) -> None:
        """Returns the remote name with the matching URL."""
        test_stdout = ('centos-stream-9\thttps://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-9.git (fetch)\n'  # noqa: E501
                       'centos-stream-9\thttps://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-9.git (push)\n'  # noqa: E501
                       'origin\tgit@gitlab.com:redhat/centos-stream/src/kernel/centos-stream-9.git (fetch)\n'  # noqa: E501
                       'origin\tgit@gitlab.com:redhat/centos-stream/src/kernel/centos-stream-9.git (push)\n'  # noqa: E501
                       'rhel9\tgit@gitlab.com:redhat/rhel/src/kernel/rhel-9.git (fetch)\n'
                       'rhel9\tgit@gitlab.com:redhat/rhel/src/kernel/rhel-9.git (push)\n')

        mock_subprocess_run.return_value = mock.Mock(stdout=test_stdout)

        tests = [
            (['https://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-9.git'],
             'centos-stream-9'),
            (['git@gitlab.com:redhat/centos-stream/src/kernel/centos-stream-9.git'], 'origin'),
            (['git@gitlab.com:redhat/rhel/src/kernel/rhel-9.git'], 'rhel9'),
            (['https://example.com/repo.git'], '')
        ]

        for test in tests:
            repo_urls = test[0]
            expected_name = test[1]

            with self.subTest(repo_urls=repo_urls, expected_name=expected_name):
                self.assertEqual(kgit.get_remote_name('location', repo_urls), expected_name)


class TestLoadRepoConfigs(KwfTestCase):
    """Tests for the load_repo_configs function."""

    EXPECTED_RH_KERNEL_REPOS = [
        'origin',
        'kernel-ark',
        'centos-stream-9',
        'centos-stream-10',
        'bot-centos-stream-9',
        'bot-centos-stream-10'
    ]

    def test_raises_on_empty_input_params(self) -> None:
        """Raises RuntimeError on empty input."""
        with self.assertRaises(RuntimeError):
            kgit.load_repo_configs('')

    def test_loads_expected_data(self) -> None:
        """Loads the expected data."""
        rh_kernel_repos = kgit.load_repo_configs('utils/rh_kernel_git_repos.yml')
        self.assertCountEqual(list(rh_kernel_repos.keys()), self.EXPECTED_RH_KERNEL_REPOS)

        # This is used by commit-compare hook.
        upstream_repos = kgit.load_repo_configs('utils/upstream_kernel_git_repos.yml')
        expected_upstream_repos = [
            "origin",
            "tip",
            "cryptodev",
            "crypto",
            "dma-mapping",
            "iommu",
            "iommufd",
            "pci",
            "net",
            "net-next",
            "bpf",
            "bpf-next",
            "scsi",
            "scsi-devel",
            "audit",
            "selinux",
            "sound",
            "dm",
            "powerpc",
            "block",
            "rdma",
            "wireless",
            "wireless-next",
            "perf",
            "drm",
            "drm-misc",
            "stable",
            "kernel-ark",
            "perf-tools",
            "perf-tools-next",
        ]
        self.assertCountEqual(list(upstream_repos.keys()), expected_upstream_repos)

        # Used by the subsystems hook.
        subsystems_repos = kgit.load_repo_configs('utils/upstream_subsystems_repo.yml')
        self.assertCountEqual(list(subsystems_repos.keys()), ['origin'])

    def test_loads_path_extra_data(self) -> None:
        """Loads additional repos from extra_file_path."""
        file_path = 'utils/rh_kernel_git_repos.yml'
        extra_file_path = 'tests/assets/rh_kernel_git_repos_private.yml'
        repos = kgit.load_repo_configs(file_path, extra_file_path)

        expected_repos = self.EXPECTED_RH_KERNEL_REPOS + ['rhel-8-sandbox', 'rhel-9-sandbox']
        self.assertCountEqual(list(repos.keys()), expected_repos)

    def test_raises_on_duplicate_repo_name(self) -> None:
        """Raises a ValueError because there are two repos with the same name."""
        with self.assertRaisesRegex(ValueError, expected_regex='Duplicate repository.*kernel-ark'):
            kgit.load_repo_configs('tests/assets/rh_kernel_git_repos.bad_with_duplicates.yml')

    def test_raises_on_multiple_checkouts(self) -> None:
        """Raises a ValueError because there are multiple repos with 'checkout' set."""
        err_regex = r'has checkout set:.*bot-centos-stream-9'
        with self.assertRaisesRegex(ValueError, expected_regex=err_regex):
            kgit.load_repo_configs('tests/assets/rh_kernel_git_repos.bad_multiple_checkouts.yml')
