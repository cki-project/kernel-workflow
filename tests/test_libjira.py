"""Tests of the libjira library."""
from copy import deepcopy
import typing
from unittest import mock

from jira.exceptions import JIRAError

from tests import fakes_jira
from tests.helpers import KwfTestCase
from webhook import libjira
from webhook.defs import GITFORGE
from webhook.defs import JIStatus
from webhook.defs import JIType
from webhook.defs import JPFX
from webhook.defs import RELEASE_MILESTONE_BOXES
from webhook.session import SessionRunner


class TestParseSearchList(KwfTestCase):
    """Test the parse_search_list function."""

    class Test(typing.NamedTuple):
        keys: list[str]
        cves: list[str]
        other: list[typing.Any]
        raises_type: Exception | None

    def run_parse_search_list_test(self, test):
        """Runs the test."""
        search_list = test.keys + test.cves + test.other

        if test.raises_type:
            with self.assertRaises(test.raises_type):
                libjira.parse_search_list(search_list)
            return

        keys, cves = libjira.parse_search_list(search_list)
        self.assertCountEqual(test.keys, keys)
        self.assertCountEqual(test.cves, cves)

    def test_parse_search_list(self):
        """Test the parse_search_list function."""
        tests = [
            # Unexpected input, raises ValueError
            self.Test([], [], [1], ValueError),
            # Does what it is supposed to.
            self.Test(['RHEL-123'], ['CVE-2020-26526', 'CVE-2156-26262'], [], None),
            # Does what it is supposed to.
            self.Test([], ['CVE-2020-123456'], [], None),
            # Does what it is supposed to.
            self.Test(['RHEL-15623', 'RHEL-234'], ['CVE-1235-12341', 'CVE-1235-21351'], [], None),
            # Nothing to do.
            self.Test([], [], [], None),
            # Non string value raises exception.
            self.Test(['VROOM-123', 'RHEL-456'], [], [123456], ValueError)
        ]

        for count, test in enumerate(tests):
            with self.subTest(tests_index=count, test=test):
                self.run_parse_search_list_test(test)


class TestGetIssues(KwfTestCase):
    """Test the _getissues function."""

    class Test(typing.NamedTuple):
        keys: list[str]
        cves: list[str]
        raises_type: Exception | None
        raises_msg: str | None
        jira_error: JIRAError | None

    def run_getissues_test(self, test):
        """Run the test."""
        mock_jira = mock.Mock()
        if test.jira_error:
            mock_jira.search_issues.side_effect = test.jira_error

        if test.raises_type:
            with self.assertRaisesRegex(test.raises_type, test.raises_msg):
                libjira._getissues(mock_jira, issues=test.keys, cves=test.cves, filter_kwf=True)
        else:
            result = libjira._getissues(mock_jira, issues=test.keys, cves=test.cves)

        if not test.keys or test.cves:
            mock_jira.assert_not_called()
            return

        jql_str = mock_jira.search_issues.call_args.args[0]
        for key in test.keys:
            self.assertIn(f'key={key}', jql_str)
        if test.cves:
            self.assertIn('labels in', jql_str)
        for cve in test.cves:
            self.assertIn(cve, jql_str)

        if not test.jira_error:
            self.assertEqual(result, mock_jira.search_issues.return_value)
        else:
            self.assertEqual(result, [])

    def test_getissues(self):
        """Test the _getissues function."""

        tests = [
            # No input, raises ValueError.
            self.Test([], [], ValueError, 'Search lists are empty.', False),
            # Queries an issue.
            self.Test(['RHEL-123'], [], None, None, False),
            # Queries a CVE.
            self.Test([], ['CVE-2020-123456'], None, None, False),
            # Queries a few issues and CVEs.
            self.Test(['RHEL-15623', 'RHEL-234'], ['CVE-1235-12341', 'CVE-1235-21351'],
                      None, None, None),
            # Queries an unknown issue, gets nothing back.
            self.Test(['RHEL-999999'], [], None, None, JIRAError)
        ]

        for count, test in enumerate(tests):
            with self.subTest(tests_index=count, test=test):
                self.run_getissues_test(test)

    @mock.patch('webhook.libjira._getissues')
    @mock.patch('webhook.libjira.connect_jira')
    def test_fetch_issues(self, mock_connect_jira, mock_get):
        mock_jira = mock.Mock()
        mock_connect_jira.return_value = mock_jira
        issue1 = self.make_jira_issue('RHEL-101')
        issue2 = self.make_jira_issue('RHEL-102')
        mock_get.return_value = [issue1, issue2]
        issues = libjira.fetch_issues(['RHEL-101', 'CVE-1999-2000'], filter_kwf=True)
        mock_get.assert_called_with(mock_jira,
                                    issues={'RHEL-101'},
                                    cves={'CVE-1999-2000'},
                                    filter_kwf=True)
        self.assertCountEqual(issues, [issue1, issue2])


class TestHelpers(KwfTestCase):
    """Tests for helper functions."""
    COMPONENT = mock.Mock(spec_set=['name'])
    COMPONENT.name = 'kernel'
    RTCOMPONENT = mock.Mock(spec_set=['name'])
    RTCOMPONENT.name = 'kernel-rt'
    FIXVERSION = mock.Mock(spec_set=['name'])
    FIXVERSION.name = 'rhel-10.0.0'
    ASSIGNEE = mock.Mock(spec_set=['name'])
    ASSIGNEE.name = 'shadowman@redhat.com'
    ITM = mock.Mock(spec_set=['value'])
    ITM.value = '25'
    DTM = mock.Mock(spec_set=['value'])
    DTM.value = '23'
    TESTABLE_BUILDS = 'This build has some artifacts'
    RELEASE_BLOCKER = mock.Mock(spec_set=['value'])
    RELEASE_BLOCKER.value = 'Approved Blocker'
    ISSUE_TYPE = mock.Mock(spec_set=['name'])
    ISSUE_TYPE.name = 'Bug'
    JIRA01 = fakes_jira.FakeJI(id=1, key='RHEL-1',
                               fields=mock.Mock(status='Unknown',
                                                issuetype=ISSUE_TYPE,
                                                components=[COMPONENT]))
    JIRA02 = fakes_jira.FakeJI(id=2, key='RHEL-2',
                               fields=mock.Mock(status='New',
                                                issuetype=ISSUE_TYPE,
                                                components=[COMPONENT]))
    JIRA03 = fakes_jira.FakeJI(id=3, key='RHEL-3',
                               fields=mock.Mock(status='Planning',
                                                issuetype=ISSUE_TYPE,
                                                components=[COMPONENT]))
    JIRA04 = fakes_jira.FakeJI(id=4, key='RHEL-4',
                               fields=mock.Mock(status='In Progress',
                                                issuetype=ISSUE_TYPE,
                                                components=[COMPONENT]))
    JIRA05 = fakes_jira.FakeJI(id=5, key='RHEL-5',
                               fields=mock.Mock(status='Tested',
                                                issuetype=ISSUE_TYPE,
                                                components=[COMPONENT]))
    JIRA06 = fakes_jira.FakeJI(id=6, key='RHEL-6',
                               fields=mock.Mock(status='Closed',
                                                issuetype=ISSUE_TYPE,
                                                components=[COMPONENT]))
    JIRA07 = fakes_jira.FakeJI(id=7, key='RHEL-7',
                               fields=mock.Mock(status='In Progress',
                                                issuetype=ISSUE_TYPE,
                                                components=[COMPONENT],
                                                fixVersions=[FIXVERSION],
                                                assignee=ASSIGNEE,
                                                customfield_12321040=ITM,
                                                customfield_12318141=DTM,
                                                customfield_12321740=TESTABLE_BUILDS,
                                                customfield_12319743=RELEASE_BLOCKER))
    JIRA08 = fakes_jira.FakeJI(id=8, key='RHEL-8',
                               fields=mock.Mock(status='Planning',
                                                issuetype=ISSUE_TYPE,
                                                components=[RTCOMPONENT],
                                                fixVersions=[FIXVERSION],
                                                assignee=mock.Mock(),
                                                customfield_12321040=None,
                                                customfield_12318141=None,
                                                customfield_12321740='',
                                                customfield_12319743=None))
    JIRA09 = fakes_jira.FakeJI(id=9, key='RHEL-9',
                               fields=mock.Mock(status='In Progress',
                                                issuetype=ISSUE_TYPE,
                                                components=[COMPONENT],
                                                fixVersions=None))

    def test_update_issue_field_bad_param(self):
        """Check for bad usage."""
        mock_field = mock.Mock(spec_set=['name'])
        mock_field.name = 'resolution'
        with self.assertLogs('cki.webhook.libjira', level='WARNING') as logs:
            libjira.update_issue_field(self.JIRA07, libjira.JiraField.resolution, 'mocked')
            self.assertIn('Attempt to use update_issue_field for an unknown field: resolution',
                          logs.output[-1])

    @mock.patch('webhook.libjira.is_production_or_staging', mock.Mock(return_value=True))
    def test_update_issue_field_release_milestone(self):
        """Make sure updating the Release Milestone custom field looks sane."""
        jfield = libjira.JiraField.Release_Milestone
        mock_update = mock.Mock()
        mock_issue = mock.Mock()
        mock_issue.update = mock_update
        boxes = [{'id': RELEASE_MILESTONE_BOXES['mr_approved']}]
        libjira.update_issue_field(mock_issue, jfield, boxes)
        mock_update.assert_called_with(update={jfield: [{'set': boxes}]})

    @mock.patch('webhook.libjira.is_production_or_staging', mock.Mock(return_value=True))
    def test_sync_cve_issue_fields(self):
        """Make sure linked jira issues have an acceptable ITM/DTM."""
        cve_list = ['CVE-2050-12345']
        mock_rhissue = mock.Mock(spec_set=['ji', 'id', 'ji_cves', 'ji_type', 'ji_status'],
                                 ji=self.JIRA07, id='RHEL-7', ji_cves=cve_list,
                                 ji_type=JIType.BUG, ji_status=JIStatus.NEW)
        mock_linked_rhissue = mock.Mock(spec_set=['ji', 'id', 'ji_cves', 'ji_type'],
                                        ji=self.JIRA08, id='RHEL-8', ji_cves=cve_list,
                                        ji_type=JIType.BUG)
        rhissues = [mock_rhissue]
        linked_rhissues = [mock_linked_rhissue]

        # Make sure ITM and DTM were copied into the linked issue
        with self.assertLogs('cki.webhook.libjira', level='DEBUG') as logs:
            libjira.sync_cve_issue_fields(rhissues, linked_rhissues)
            self.assertIn('Setting issue RHEL-8\'s field Internal_Target_Milestone to 25',
                          logs.output[-5])
            self.assertIn('Setting issue RHEL-8\'s field Dev_Target_Milestone to 23',
                          logs.output[-4])
            self.assertIn('Setting issue RHEL-8\'s field Testable_Builds '
                          'to This build has some artifacts...', logs.output[-3])
            self.assertIn('Setting issue RHEL-8\'s field assignee to shadowman@redhat.com',
                          logs.output[-2])
            self.assertIn('Setting issue RHEL-8\'s field Release_Blocker to Approved Blocker',
                          logs.output[-1])

            libjira.sync_cve_issue_fields(rhissues, rhissues)
            self.assertIn('ITM value 25 for RHEL-7 and RHEL-7 already match', logs.output[-5])
            self.assertIn('DTM value 23 for RHEL-7 and RHEL-7 already match', logs.output[-4])
            self.assertIn('Testable Builds already populated for RHEL-7', logs.output[-3])
            self.assertIn('Assignee field already matches', logs.output[-2])
            self.assertIn('Blocker/Exception field already matches', logs.output[-1])

            mock_linked_rhissue.ji_cves = ['CVE-1977-2000']
            libjira.sync_cve_issue_fields(rhissues, linked_rhissues)
            self.assertIn('Issue RHEL-7 and RHEL-8 CVE lists do not match, ignoring',
                          logs.output[-1])

            mock_linked_rhissue.ji_type = JIType.TASK
            libjira.sync_cve_issue_fields(rhissues, linked_rhissues)
            self.assertIn('Tasks (RHEL-8) are handled differently', logs.output[-1])

    def test_issues_with_lower_status(self):
        """Returns the JIRA Issues whose status is lower than status and at least min_status."""

        issue_list = [self.JIRA01, self.JIRA02, self.JIRA03, self.JIRA04,
                      self.JIRA05, self.JIRA06]
        self.assertCountEqual(libjira.issues_with_lower_status(issue_list, JIStatus.PLANNING),
                              [self.JIRA02])
        self.assertCountEqual(libjira.issues_with_lower_status(issue_list, JIStatus.TESTED),
                              [self.JIRA02, self.JIRA03, self.JIRA04])
        self.assertCountEqual(libjira.issues_with_lower_status(issue_list, JIStatus.CLOSED,
                              JIStatus.IN_PROGRESS), [self.JIRA04, self.JIRA05])

    @mock.patch('webhook.libjira.connect_jira', mock.Mock())
    def test_update_issue_status_no_prod(self):
        """Moves qualifying issues to the given status and returns them in a list."""
        # Given an empty issue_list there is nothing to do.
        self.assertEqual(libjira.update_issue_status([], JIStatus.IN_PROGRESS), [])

        # If none of the issues have a status less than the input, nothing to do.
        self.assertEqual(libjira.update_issue_status([self.JIRA05], JIStatus.IN_PROGRESS),
                         [])

        # Not production, just set the new status and return the list.
        issue1 = deepcopy(self.JIRA01)
        issue2 = deepcopy(self.JIRA02)
        issue3 = deepcopy(self.JIRA03)
        issue4 = deepcopy(self.JIRA04)
        issue_list = [issue1, issue2, issue3, issue4]
        result = libjira.update_issue_status(issue_list, JIStatus.IN_PROGRESS)
        self.assertCountEqual(result, [issue2, issue3])
        self.assertEqual(issue2.fields.status, 'New')
        self.assertEqual(issue3.fields.status, 'Planning')

    @mock.patch('webhook.libjira.connect_jira')
    @mock.patch('webhook.libjira.is_production_or_staging', mock.Mock(return_value=True))
    @mock.patch('jira.JIRA.add_comment', mock.Mock())
    def test_update_issue_status_prod(self, mock_connect_jira):
        """Moves qualifying issues to the given status and returns them in a list."""
        mock_transition_issue = mock_connect_jira.return_value.transition_issue
        # Production, set the new status and return the list.
        issue1 = deepcopy(self.JIRA01)
        issue2 = deepcopy(self.JIRA02)
        issue3 = deepcopy(self.JIRA03)
        issue4 = deepcopy(self.JIRA04)
        issue5 = deepcopy(self.JIRA05)
        issue_list = [issue1, issue2, issue3, issue4, issue5]

        t_comment = "GitLab kernel MR bot updated status to In Progress"
        result = libjira.update_issue_status(issue_list, JIStatus.IN_PROGRESS)
        self.assertEqual(mock_transition_issue.call_count, 2)
        mock_transition_issue.assert_called_with(mock.ANY, "In Progress", comment=t_comment)
        self.assertCountEqual(result, [issue2, issue3])
        self.assertEqual(issue1.fields.status, 'Unknown')
        self.assertEqual(issue2.fields.status, 'In Progress')
        self.assertEqual(issue3.fields.status, 'In Progress')
        self.assertEqual(issue4.fields.status, 'In Progress')
        self.assertEqual(issue5.fields.status, 'Tested')

        # test NOT moving issue to TESTED
        mock_transition_issue.reset_mock()
        with self.assertLogs('cki.webhook.libjira', level='INFO') as logs:
            issue4_status = issue4.fields.status
            result = libjira.update_issue_status([issue4], JIStatus.TESTED)
            self.assertEqual(result, [])
            self.assertIn('Unsupported transition status: Tested', logs.output[-1])
            self.assertEqual(issue4.fields.status, issue4_status)
            mock_transition_issue.assert_not_called()

    @mock.patch('webhook.libjira.filter_kwf_issues')
    @mock.patch('webhook.libjira.is_production_or_staging', mock.Mock(return_value=True))
    def test_update_testable_builds(self, mock_filter_kwf_issues):
        fields1 = mock.Mock(status=21, customfield_12321740='Contains pipeline_url in it')
        fields2 = mock.Mock(status=21, customfield_12321740='garbage')
        fields3 = mock.Mock(status=21, customfield_12321740='garbage')
        updater1 = mock.Mock()
        updater2 = mock.Mock()
        updater3 = mock.Mock()
        issue1 = mock.Mock(key=f'{JPFX}11223344', fields=fields1, update=updater1)
        issue2 = mock.Mock(key=f'{JPFX}22334455', fields=fields2, update=updater2)
        issue3 = mock.Mock(key=f'{JPFX}33445566', fields=fields3, update=updater3)

        # No issue objects, nothing to return.
        issue_list = []
        mock_filter_kwf_issues.return_value = issue_list
        libjira.update_testable_builds(issue_list, 'text', ['pipeline_url'])
        issue1.update.assert_not_called()
        issue2.update.assert_not_called()
        issue3.update.assert_not_called()

        # Comment already posted to one issue but not the other
        issue_list = [issue1, issue2]
        mock_filter_kwf_issues.return_value = issue_list
        with self.assertLogs('cki.webhook.libjira', level='DEBUG') as logs:
            libjira.update_testable_builds(issue_list, 'text', ['pipeline_url'])
            self.assertIn(f'All downstream pipelines found in {JPFX}11223344', logs.output[-2])
            issue1.update.assert_not_called()
            issue2.update.assert_called_with(fields={'customfield_12321740': 'text'})

        mock_filter_kwf_issues.return_value = [issue3]
        libjira.update_testable_builds([issue3], 'exec() getcwd() /usr/bin/hostname',
                                       ['pipeline_url'])
        issue3.update.assert_called_with(fields={
                                         'customfield_12321740': 'exec getcwd usr bin hostname'})

    @mock.patch('webhook.libjira.update_issue_status')
    @mock.patch('webhook.libjira.is_production_or_staging', mock.Mock(return_value=True))
    def test_reset_preliminary_testing(self, mock_update_status):
        mock_jira = mock.Mock()

        mock_issue = mock.Mock()
        mock_issue.fields.customfield_12321540 = None
        mock_issue.key = 'RHEL-101'
        libjira.reset_preliminary_testing(mock_jira, [mock_issue])
        mock_jira.add_comment.assert_not_called()

        mock_issue = mock.Mock()
        mock_issue.fields.customfield_12321540.value = 'Pass'
        mock_issue.key = 'RHEL-101'
        libjira.reset_preliminary_testing(mock_jira, [mock_issue])
        mock_issue.update.assert_called_with(fields={'customfield_12321540': None})
        mock_jira.add_comment.assert_called_once()

        mock_issue = mock.Mock()
        mock_issue.fields.issuetype.name = 'Task'
        mock_issue.key = 'RHEL-102'
        libjira.reset_preliminary_testing(mock_jira, [mock_issue])
        mock_issue.update.assert_not_called()
        mock_update_status.assert_called_with([mock_issue], JIStatus.NEW)

    @mock.patch('webhook.libjira.is_production_or_staging', mock.Mock(return_value=True))
    def test_request_preliminary_testing(self):
        mock_issue = mock.Mock()
        mock_issue.fields.customfield_12321540.value = 'Pass'
        mock_issue.fields.issuetype.name = 'Bug'
        mock_issue.key = 'RHEL-101'

        with self.assertLogs('cki.webhook.libjira', level='DEBUG') as logs:
            libjira.request_preliminary_testing([mock_issue])
            self.assertIn("Jira Issue RHEL-101 PT already set to Pass", logs.output[-2])

        # Don't set PT:Requested if in Fail and reset_failed is False
        mock_issue.fields.customfield_12321540.value = 'Fail'
        with self.assertLogs('cki.webhook.libjira', level='DEBUG') as logs:
            libjira.request_preliminary_testing([mock_issue])
            self.assertIn("Jira Issue RHEL-101 Failed QE Preliminary Testing", logs.output[-2])

    @mock.patch('webhook.libjira.filter_kwf_issues')
    @mock.patch('webhook.libjira.is_production_or_staging', mock.Mock(return_value=True))
    @mock.patch('webhook.libjira.connect_jira')
    def test_add_gitlab_link_in_issues(self, mock_jira, mock_filter_kwf_issues):
        """Test gitlab link additions in jira issues."""
        mock_session = SessionRunner.new('buglinker', 'args')
        mr_id = 666
        url = f'{GITFORGE}/foo/bar/-/merge_requests/{mr_id}'
        icon_url = (f'{GITFORGE}/assets/favicon-'
                    '72a2cad5025aa931d6ea56c3201d1f18e68a8cd39788c7c80d5b2b82aa5143ef.png')
        icon = {'url16x16': icon_url, 'title': 'GitLab Merge Request'}
        title = "A great MR"
        obj = mock.Mock(url=url, title=f'Merge Request: {title}', icon=icon)
        remote_link = mock.Mock(id=1112, object=obj)
        mock_jira().remote_link.return_value = remote_link
        mock_jira().remote_links.return_value = [remote_link]
        mock_MR = mock.Mock(iid=mr_id,
                            commits=True,
                            title=title,
                            description=mock.Mock(bugzilla=False),
                            namespace='foo/bar')
        mock_issues = [mock.Mock(key=f'{JPFX}1234')]
        mock_filter_kwf_issues.return_value = mock_issues
        with self.assertLogs('cki.webhook.libjira', level='INFO') as logs:
            libjira.add_gitlab_link_in_issues(mock_session, mock_issues, mock_MR)
            self.assertIn(f'MR {mr_id} already linked in {JPFX}1234',
                          logs.output[-1])
            mock_jira().remote_links.return_value = []
            libjira.add_gitlab_link_in_issues(mock_session, mock_issues, mock_MR)
            self.assertIn(f'Linking [Merge Request: A great MR]({url}) to issue {JPFX}1234',
                          logs.output[-1])
            exp_obj = {'url': url, 'title': f'Merge Request: {title}', 'icon': icon}
            mock_jira().add_simple_link.assert_called_with(issue=mock_issues[0], object=exp_obj)
            mock_jira().add_comment.assert_called_once()

    def test_remove_gitlab_link_comment_in_issue(self):
        """Test removal of comments we left in the issue pointing to our MR."""
        mock_issue = mock.Mock(id=55, key='RHEL-55')
        mock_author = mock.Mock()
        mock_author.name = 'gitlab-jira'
        mr_url = f'{GITFORGE}/foo/bar/-/merge_requests/66'
        mock_comment = mock.Mock(id=1234, body=f"Contains {mr_url} in it", author=mock_author)
        mock_jira = mock.Mock()
        mock_jira.comments.return_value = [mock_comment]
        mock_jira.comment.return_value = mock_comment
        with self.assertLogs('cki.webhook.libjira', level='INFO') as logs:
            libjira.remove_gitlab_link_comment_in_issue(mock_jira, mock_issue, mr_url)
            self.assertIn(f"Removing {mock_issue.key} comment pointing to {mr_url}",
                          logs.output[-1])
            mock_comment.delete.assert_called_once()

    @mock.patch('webhook.libjira.filter_kwf_issues')
    @mock.patch('webhook.libjira._getissues')
    @mock.patch('webhook.libjira.connect_jira')
    def test_remove_gitlab_link_in_issues(self, mock_jira, mock_getissues, mock_filter_kwf_issues):
        """Test gitlab link additions in jira issues."""
        mr_id = 666
        url = f'{GITFORGE}/foo/bar/-/merge_requests/{mr_id}'
        obj = mock.Mock(url=url, title='Merge request - blah blah',
                        icon={'url16x16': 'https://example.com/favicon.png',
                              'title': 'GitLab'})
        remote_link = mock.Mock(id=1112, object=obj)
        mock_jira().remote_link.return_value = remote_link
        mock_jira().remote_links.return_value = [remote_link]
        namespace = 'foo/bar'
        mock_getissues.return_value = [fakes_jira.JI7777777]
        mock_filter_kwf_issues.return_value = mock_getissues.return_value

        # Ensure we do nothing if there's no issue list
        libjira.remove_gitlab_link_in_issues(mr_id, namespace, set())
        mock_getissues.assert_not_called()

        # make sure we call unlink functions when we do get an issue to remove
        with self.assertLogs('cki.webhook.libjira', level='INFO') as logs:
            libjira.remove_gitlab_link_in_issues(mr_id, namespace, [f'{JPFX}7777777'])
            self.assertIn(f'MR {mr_id} linked in {JPFX}7777777, removing it',
                          logs.output[-1])

    @mock.patch('webhook.libjira.update_issue_status', mock.Mock(return_value=True))
    @mock.patch('webhook.libjira.issues_to_move_to_in_progress')
    def test_move_issue_states_forward(self, mock_progress):
        """Make sure we move issues forward to Planning and In Progress properly."""
        mock_planning_issue = mock.Mock(id=55, key='RHEL-55')
        mock_planning_issue.fields.customfield_12318141 = None
        mock_planning_issue.fields.customfield_12321040 = None
        mock_planning_fv = mock.Mock(name='rhel-11.3.0')
        mock_planning_issue.fields.fixVersions = [mock_planning_fv]

        mock_in_prog_issue = mock.Mock(id=56, key='RHEL-56')
        mock_in_prog_issue.fields.customfield_12318141.value = '2'
        mock_in_prog_issue.fields.customfield_12321040.value = '4'
        mock_in_prog_fv = mock.Mock(name='rhel-11.3.0')
        mock_in_prog_issue.fields.fixVersions = [mock_in_prog_fv]
        mock_progress.return_value = [mock_in_prog_issue]

        mock_issue_list = [mock_planning_issue, mock_in_prog_issue]
        with self.assertLogs('cki.webhook.libjira', level='DEBUG') as logs:
            libjira.move_issue_states_forward(mock_issue_list)
            self.assertIn(f'Issues to move to In Progress: [{mock_in_prog_issue}]', logs.output[-2])
            self.assertIn(f'Issues to move to Planning: [{mock_planning_issue}]', logs.output[-1])

    @mock.patch('webhook.libjira.issues_with_lower_status')
    def test_issues_to_move_to_in_progress(self, mock_update):
        """Test issue vetting of issues that can be moved to In Progress."""
        mock_issue = mock.Mock(id=55, key="RHEL-55")
        mock_fv = mock.Mock()

        # Standard y-stream case, 'Story' issue type, everything is fine..
        mock_issue.fields.issuetype.name = 'Vulnerability'
        mock_issue.fields.customfield_12318141.value = '2'         # DTM
        mock_issue.fields.customfield_12321040.value = '4'         # ITM
        mock_fv.name = 'rhel-11.3.0'
        mock_issue.fields.fixVersions = [mock_fv]
        mock_update.return_value = [mock_issue]
        ret = libjira.issues_to_move_to_in_progress([mock_issue])
        self.assertEqual(ret, {mock_issue})

        # Same as above but 'Bug' issue type & no severity set, do not update.
        mock_issue.fields.issuetype.name = 'Bug'
        ret = libjira.issues_to_move_to_in_progress([mock_issue])
        self.assertEqual(ret, set())

        # Standard y-stream case, 'Bug' issue type with severity set, everything is fine
        mock_issue.fields.customfield_12316142.value = 'Moderate'  # severity
        ret = libjira.issues_to_move_to_in_progress([mock_issue])
        self.assertEqual(ret, {mock_issue})

        # bot assignee
        mock_issue.fields.assignee.emailAddress = 'kernel-mgr@redhat.com'
        ret = libjira.issues_to_move_to_in_progress([mock_issue])
        self.assertEqual(ret, set())

        # No fixver set, do not update
        mock_issue.fields.assignee.emailAddress = 'someone@redhat.com'
        mock_issue.fields.fixVersions = []
        mock_update.return_value = [mock_issue]
        ret = libjira.issues_to_move_to_in_progress([mock_issue])
        self.assertEqual(ret, set())

        # No ITM, do not update
        mock_issue.fields.customfield_12321040 = None
        mock_fv.name = 'rhel-11.3.0'
        mock_issue.fields.fixVersions = [mock_fv]
        mock_update.return_value = [mock_issue]
        ret = libjira.issues_to_move_to_in_progress([mock_issue])
        self.assertEqual(ret, set())

        # Standard z-stream case, everything is fine
        mock_issue.fields.customfield_12318141 = None
        mock_issue.fields.customfield_12321040 = None
        mock_fv.name = 'rhel-3.20.0.z'
        mock_issue.fields.fixVersions = [mock_fv]
        mock_update.return_value = [mock_issue]
        ret = libjira.issues_to_move_to_in_progress([mock_issue])
        self.assertEqual(ret, {mock_issue})

    def test_filter_kwf_issues(self):
        """Returns the issues which are kwf compatible types 'Bug' or 'Story' and kernel* comp."""
        def mock_issue(key: str, issuetype: str, component: str) -> mock.Mock:
            amock = mock.Mock(spec_set=['key', 'fields'], key=key)
            amock.fields.issuetype.name = issuetype
            mock_comp = mock.Mock(spec_set=['name'])
            mock_comp.name = component
            amock.fields.components = [mock_comp]
            return amock

        issue1 = mock_issue('RHEL-123', 'Bug', 'kernel / perf')
        issue2 = mock_issue('RHEL-456', 'Bug', 'kernel-rt')
        issue3 = mock_issue('RHEL-345', 'Epic', 'kernel')
        issue4 = mock_issue('RHEL-444', 'Story', 'kernel')
        issue5 = mock_issue('RHEL-555', 'Subtask', 'kernel')
        issue6 = mock_issue('RHEL-555', 'Bug', 'systemd')

        self.assertCountEqual(
            libjira.filter_kwf_issues([issue1, issue2, issue3, issue4, issue5, issue6]),
            [issue1, issue2, issue4]
        )


class TestKwfJiraClient(KwfTestCase):
    """Test KwfJiraClient wrapper."""

    class FakeJIRA(libjira.KwfJiraClient):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.fail_count = 0
            self.code = 0

        def fail_next_with(self, code, count):
            self.code = code
            self.fail_count = count

        def remote_links(self, issue_id):
            if self.fail_count:
                self.fail_count = self.fail_count - 1
                if self.code == 401:
                    raise JIRAError('401 Unauthorized', status_code=401,
                                    url='https://issues.redhat.com')
                if self.code == 400:
                    raise JIRAError('400 Bad Request', status_code=400,
                                    url='https://issues.redhat.com')
            return [mock.Mock(id=123)]

    def setUp(self) -> None:
        """Set up the login (validate) responses."""
        super().setUp()

        self.response_jira_setup()
        self.jira = self.FakeJIRA(server=self.JIRA_URL, token_auth='123abc', validate=True)

    def test_good_call(self):
        jira = self.jira

        links = jira.remote_links('JIRA-111')
        self.assertTrue(links)
        self.assertEqual(links[0].id, 123)

    def test_single_401(self):
        jira = self.jira

        jira.fail_next_with(401, 1)
        links = jira.remote_links('JIRA-111')
        self.assertTrue(links)
        self.assertEqual(links[0].id, 123)

    def test_multiple_401(self):
        jira = self.jira

        jira.fail_next_with(401, 4)

        with self.assertRaisesRegex(JIRAError, '401 Unauthorized'):
            jira.remote_links('JIRA-111')

    def test_single_400(self):
        jira = self.jira

        jira.fail_next_with(400, 1)
        with self.assertRaisesRegex(JIRAError, '400 Bad Request'):
            jira.remote_links('JIRA-111')
