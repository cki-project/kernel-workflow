"""Tests for merge_conflicts_check."""
import os
from subprocess import CalledProcessError
from subprocess import CompletedProcess
from unittest import mock

from tests.helpers import KwfTestCase
from webhook import defs
from webhook.utils import merge_conflicts_check


class TestMergeConflictsChecker(KwfTestCase):
    """Tests for the various helper functions."""

    # For mocking subprocess.run
    _mocked_runs = []
    _mocked_calls = []

    MOCK_MR = {'webUrl': 'https://gitlab.com/foo/bar/blah/-/merge_requests/66'}
    MOCK_MR2 = {'webUrl': 'https://gitlab.com/foo/bar/blah/-/merge_requests/67'}
    MOCK_MR3 = {'webUrl': 'https://gitlab.com/foo/bar/blah/-/merge_requests/68'}
    MOCK_MR_LIST = {"foo/bar/blah_main": [MOCK_MR, MOCK_MR2]}

    GQL_MRS = {'project':
               {'id': 'gid://gitlab/Project/1234',
                'mergeRequests':
                {'pageInfo': {'hasNextPage': False, 'endCursor': 'eyJjc'},
                 'nodes': [MOCK_MR, MOCK_MR2]}}}

    def _mock_run(self, args, *, check=False, **_):
        for run in self._mocked_runs:
            if run[0] == args:
                (returncode, stdout) = run[1:]
                break
        else:
            self.fail(f'Command {args} not found in mocked subprocess.run')
        self._mocked_calls.append(args)
        if returncode:
            raise CalledProcessError(returncode, args, output=stdout)
        return CompletedProcess(args, returncode, stdout=stdout)

    def _add_run_result(self, args, returncode, stdout=None):
        self._mocked_runs.append((args, returncode, stdout))

    @mock.patch('webhook.mergehook.MergeMR.new_from_query')
    def test_get_open_mrs(self, mock_new_merge_mrs):
        mock_session = mock.Mock()
        namespace = 'foo/bar/blah'
        mock_mmr1 = mock.Mock()
        mock_mmr1.iid = 67
        mock_mmr1.target_branch = 'main'
        mock_mmr1.rh_branch = True
        mock_mmr1.is_build_mr = False
        mock_mmr2 = mock.Mock()
        mock_mmr2.iid = 68
        mock_mmr2.target_branch = 'main-automotive'
        mock_mmr2.rh_branch = True
        mock_mmr2.is_build_mr = False
        mock_mmr3 = mock.Mock()
        mock_mmr3.iid = 69
        mock_mmr3.is_build_mr = True
        mock_mmr3.target_branch = 'main'
        mock_mmr3.rh_branch = True
        mock_mmr3.title = "This is a build MR"
        mock_mmr4 = mock.Mock()
        mock_mmr4.iid = 70
        mock_mmr4.is_build_mr = False
        mock_mmr4.target_branch = 'bzzt'
        mock_mmr4.rh_branch = False
        mock_mmr4.title = "This is an MR for an unknown branch"
        mock_new_merge_mrs.return_value = [mock_mmr1, mock_mmr2, mock_mmr3, mock_mmr4]
        results = merge_conflicts_check.get_open_mrs(mock_session, namespace)
        mock_new_merge_mrs.assert_called_once()
        self.assertEqual({'foo/bar/blah_main': [mock_mmr1],
                          'foo/bar/blah_main-automotive': [mock_mmr2]}, results)

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_prep_branch_for_merges(self):
        rhkernel_src = '/src/linux'
        mock_proj = mock.Mock()
        mock_proj.name = 'blah'
        mr_params = {'gl_project': mock_proj, 'target_branch': 'main',
                     'merge_dir': '/src/foo-bar-blah_main',
                     'proj_tb': 'blah_main'}
        ret = merge_conflicts_check.prep_branch_for_merges(rhkernel_src, mr_params)
        self.assertEqual(ret, '/src/blah-main-megamerge/')

    @mock.patch('webhook.kgit.hard_reset', mock.Mock())
    @mock.patch('webhook.kgit.branch_copy', mock.Mock())
    @mock.patch('webhook.kgit.branch_delete', mock.Mock())
    def test_try_nested_merges_ok(self):
        mock_mmr1 = mock.Mock()
        mock_mmr1.iid = 67
        mrs = [mock_mmr1]
        mock_mmr2 = mock.Mock()
        mock_mmr2.iid = 68
        conflict_mrs = [mock_mmr2]
        mock_proj = mock.Mock()
        mock_proj.name = 'blah'
        mr_params = {'gl_project': mock_proj, 'target_branch': 'main',
                     'merge_dir': '/src/foo-bar-blah_main', 'proj_tb': 'blah_main'}
        self._add_run_result(['git', 'merge', '--quiet', '--no-edit', 'blah/merge-requests/68'], 0)
        self._add_run_result(['git', 'merge', '--quiet', '--no-edit', 'blah/merge-requests/67'], 0)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = merge_conflicts_check.try_nested_merges(mrs, conflict_mrs, mr_params)
            self.assertEqual(ret, [])

    @mock.patch('webhook.kgit.hard_reset', mock.Mock())
    @mock.patch('webhook.kgit.branch_copy', mock.Mock())
    @mock.patch('webhook.kgit.branch_delete', mock.Mock())
    def test_try_nested_merges_bad(self):
        mock_mmr1 = mock.Mock()
        mock_mmr1.iid = 67
        mrs = [mock_mmr1]
        mock_mmr2 = mock.Mock()
        mock_mmr2.iid = 68
        conflict_mrs = [mock_mmr2]
        mock_proj = mock.Mock()
        mock_proj.name = 'blah'
        mr_params = {'gl_project': mock_proj, 'target_branch': 'main',
                     'merge_dir': '/src/foo-bar-blah_main', 'proj_tb': 'blah_main'}
        run_args = ['git', 'merge', '--quiet', '--no-edit', 'blah/merge-requests/68']
        self._add_run_result(run_args, 5, "fail")
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = merge_conflicts_check.try_nested_merges(mrs, conflict_mrs, mr_params)
            self.assertEqual(ret, [])
        self._mocked_runs = []
        run_args = ['git', 'merge', '--quiet', '--no-edit', 'blah/merge-requests/68']
        self._add_run_result(run_args, 0)
        run_args = ['git', 'merge', '--quiet', '--no-edit', 'blah/merge-requests/67']
        self._add_run_result(run_args, 5, "fail")
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = merge_conflicts_check.try_nested_merges(mrs, conflict_mrs, mr_params)
            self.assertEqual(ret, [mock_mmr1])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_try_merging_all(self):
        mock_mmr1 = mock.Mock()
        mock_mmr1.iid = 67
        mock_mmr2 = mock.Mock()
        mock_mmr2.iid = 68
        mrs = [mock_mmr1, mock_mmr2]
        mock_proj = mock.Mock(name='blah')
        mr_params = {'gl_project': mock_proj, 'target_branch': 'main',
                     'merge_dir': '/src/foo-bar-blah_main',
                     'proj_tb': 'blah_main'}
        ret = merge_conflicts_check.try_merging_all(mrs, mr_params)
        self.assertEqual(ret, [])

    def test_merge_mr_to_entry(self):
        mock_mmr = mock.Mock()
        mock_mmr.iid = 100
        mock_mmr.author.username = 'shadowman'
        mock_mmr.title = 'title of record'
        expected = {'iid': 100, 'author': {'username': 'shadowman'}, 'title': 'title of record'}
        result = merge_conflicts_check.merge_mr_to_entry(mock_mmr)
        self.assertEqual(expected, result)

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    @mock.patch('webhook.mergehook.MergeMR.check_for_replicants')
    @mock.patch('webhook.kgit.branch_mergeable')
    def test_check_pending_conflicts(self, mock_mergeable, mock_replicants):
        mock_mmr = mock.Mock()
        mock_mmr.project_remote = 'blah'
        mock_mmr.target_branch = 'main'
        mock_mmr.worktree_dir = 'blah_main'
        mock_mmr.iid = 66
        mock_mmr.check_for_replicants = mock_replicants
        mock_replicants.return_value = False
        conflict_mr = mock.Mock()
        conflict_mr.iid = 67
        conflict_mrs = [conflict_mr]
        mock_mergeable.return_value = True
        ret = merge_conflicts_check.check_pending_conflicts(mock_mmr, conflict_mrs)
        self.assertFalse(ret)
        mock_mergeable.return_value = False
        with self.assertLogs('cki.webhook.utils.merge_conflicts_check', level='DEBUG') as logs:
            ret = merge_conflicts_check.check_pending_conflicts(mock_mmr, conflict_mrs)
            self.assertIn("MR 67 can't be merged by itself, skipping conflict check",
                          logs.output[-1])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    @mock.patch('webhook.mergehook.MergeMR.update_mr')
    @mock.patch('webhook.utils.merge_conflicts_check.check_pending_conflicts')
    @mock.patch('webhook.mergehook.MergeMR.check_for_merge_conflicts')
    @mock.patch('webhook.session.SessionRunner.update_webhook_comment', mock.Mock())
    def test_find_direct_conflicts(self, mock_check, mock_checkp, mock_update):
        mock_mmr = mock.Mock()
        mock_mmr.iid = 68
        mock_mmr.check_for_merge_conflicts = mock_check
        mock_mmr.update_mr = mock_update
        mock_mmr.merge_label = "Merge::OK"
        mock_mmr.session.args.testing = False
        conflict_mrs = [mock_mmr]
        mock_mr = mock.Mock()
        mock_proj = mock.Mock(name='blah')
        mock_proj.mergerequests.get.return_value = mock_mr
        mr_params = {'gl_project': mock_proj, 'target_branch': 'main',
                     'merge_dir': '/src/foo-bar-blah_main',
                     'proj_tb': 'blah_main', 'namespace': 'blah'}
        # MR has conflicts with target branch
        mock_check.return_value = True
        merge_conflicts_check.find_direct_conflicts(conflict_mrs, mr_params)
        mock_update.assert_called_once()
        mock_checkp.assert_not_called()
        self.assertEqual(mock_mmr.merge_label, defs.MERGE_CONFLICT_LABEL)
        # MR has conflicts with other MRs
        mock_check.return_value = False
        mock_checkp.return_value = True
        mock_update.reset_mock()
        merge_conflicts_check.find_direct_conflicts(conflict_mrs, mr_params)
        mock_update.assert_called_once()
        self.assertEqual(mock_mmr.merge_label, defs.MERGE_WARNING_LABEL)

    @mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS':
                                    'tests/assets/rh_projects_private.yaml',
                                    'CKI_DEPLOYMENT_ENVIRONMENT': 'staging'})
    @mock.patch('webhook.kgit.hard_reset', mock.Mock())
    @mock.patch('webhook.mergehook.MergeMR.new')
    def test_check_for_duplicated_backports(self, mock_merge_mr):
        mock_proj = mock.Mock(name='rhel-9-sandbox', id='56792')
        mr_params = {'gl_project': mock_proj,
                     'merge_dir': '/src/rhel-9-sandbox-main-merge',
                     'proj_tb': 'rhel-9-sandbox_main'}
        mock_mmr = mock.Mock()
        mock_mmr.worktree_dir = ''
        mock_merge_mr.return_value = mock_mmr
        mock_mmr.check_for_existing_backports.return_value = True
        retval = merge_conflicts_check.check_for_duplicated_backports([mock_mmr], mr_params)
        self.assertEqual(retval, [mock_mmr])

    @mock.patch('webhook.kgit.clean_up_temp_merge_branch')
    def test_clean_up_temp_merge_branches(self, mock_cleanup):
        args = mock.Mock(rhkernel_src='/src/kernel')
        merge_dirs = {'foo/bar/blah_main': '/src/foo-bar-blah_main'}
        merge_conflicts_check.clean_up_temp_merge_branches(args, merge_dirs)
        mock_cleanup.assert_called_with('/src/kernel', 'foo-bar-blah_main',
                                        '/src/foo-bar-blah_main')

    @mock.patch('webhook.utils.merge_conflicts_check.add_label_to_merge_request')
    def test_set_merge_ok_labels(self, mock_add_label):
        args = mock.Mock(testing=False)
        mock_merge_mr = mock.Mock()
        mock_merge_mr.iid = 68
        mock_merge_mr.target_branch = 'main'
        mock_merge_mr.author = 'shadowman'
        mock_mrs = [mock_merge_mr]
        mock_mr = mock.Mock()
        mock_proj = mock.Mock(name='blah')
        mock_proj.mergerequests.get.return_value = mock_mr
        mr_params = {'gl_project': mock_proj, 'target_branch': 'main', 'namespace': 'blah'}
        merge_conflicts_check.set_merge_ok_labels(args, mock_mrs, mr_params)
        mock_add_label.assert_called_with(mock_proj, mock_merge_mr.iid,
                                          [f'Merge::{defs.READY_SUFFIX}'])
        mock_add_label.reset_mock()
        merge_conflicts_check.set_merge_ok_labels(args, [], mr_params)
        mock_add_label.assert_not_called()

    @mock.patch.dict(os.environ, {'REQUESTS_CA_BUNDLE': '/etc/pki/certs/whatever.ca'})
    def test_parser_args(self):
        with mock.patch("sys.argv", ["_get_parser_args", "-r", "/src/linux"]):
            args = merge_conflicts_check._get_parser_args()
            self.assertFalse(args.testing)
            self.assertEqual(args.rhkernel_src, '/src/linux')
            self.assertEqual(args.sentry_ca_certs, '/etc/pki/certs/whatever.ca')

    def test_main_no_src(self):
        mock_session = mock.Mock()
        mock_session.args = mock.Mock(rhkernel_src=None)
        with self.assertLogs('cki.webhook.utils.merge_conflicts_check', level='DEBUG') as logs:
            merge_conflicts_check.main(mock_session)
            self.assertIn("No path to RH Kernel source git found, aborting!", logs.output[-1])

    @mock.patch('webhook.kgit.fetch_remote', mock.Mock())
    @mock.patch('webhook.utils.merge_conflicts_check.get_open_mrs')
    def test_main_no_open_mrs(self, mock_mrs):
        mock_session = mock.Mock()
        mock_session.args = mock.Mock(rhkernel_src='/src/linux',
                                      projects=['cki-project/kernel-ark'])
        mock_mrs.return_value = {}
        with self.assertLogs('cki.webhook.utils.merge_conflicts_check', level='INFO') as logs:
            merge_conflicts_check.main(mock_session)
            self.assertIn("Fetching from git remotes to ensure we have the latest data needed",
                          logs.output[-3])
            self.assertIn("Finding open MRs for projects: ['cki-project/kernel-ark']",
                          logs.output[-2])
            self.assertIn("No open MRs to process.", logs.output[-1])

    @mock.patch('webhook.kgit.fetch_remote', mock.Mock())
    @mock.patch('webhook.kgit.branch_copy', mock.Mock(return_value=True))
    @mock.patch('webhook.utils.merge_conflicts_check.prep_branch_for_merges',
                mock.Mock(return_value='foo/bar/blah'))
    @mock.patch('webhook.utils.merge_conflicts_check.try_merging_all',
                mock.Mock(return_value=True))
    @mock.patch('webhook.utils.merge_conflicts_check.find_direct_conflicts', mock.Mock())
    @mock.patch('webhook.utils.merge_conflicts_check.set_merge_ok_labels', mock.Mock())
    @mock.patch('webhook.utils.merge_conflicts_check.clean_up_temp_merge_branches', mock.Mock())
    @mock.patch('webhook.utils.merge_conflicts_check.filter_out_conflicting', mock.Mock())
    @mock.patch('webhook.utils.merge_conflicts_check.check_for_duplicated_backports', mock.Mock())
    @mock.patch('webhook.utils.merge_conflicts_check.get_instance')
    @mock.patch('webhook.utils.merge_conflicts_check.get_open_mrs')
    def test_main_open_mrs(self, mock_mrs, mock_get_instance):
        mock_session = mock.Mock()
        mock_session.args = mock.Mock(rhkernel_src='/src/linux',
                                      projects=['foo/bar/blah'])
        mock_mrs.return_value = self.MOCK_MR_LIST
        mock_instance = mock.Mock()
        mock_instance.projects.git.return_value = mock.Mock()
        mock_get_instance.return_value = mock_instance
        with self.assertLogs('cki.webhook.utils.merge_conflicts_check', level='INFO') as logs:
            merge_conflicts_check.main(mock_session)
            self.assertIn("Finding open MRs for projects: ['foo/bar/blah']",
                          logs.output[-1])
