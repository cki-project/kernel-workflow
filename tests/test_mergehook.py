"""Webhook interaction tests."""
from subprocess import CalledProcessError
from subprocess import CompletedProcess
import typing
from unittest import mock

from tests.helpers import KwfTestCase
from webhook import defs
from webhook import mergehook
from webhook.description import Description
from webhook.session import SessionRunner
from webhook.session_events import create_event


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestMergehook(KwfTestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._mocked_runs = []
        self._mocked_calls = []

    def _mock_run(self, args, *, check=False, **_):
        for run in self._mocked_runs:
            if run[0] == args:
                (returncode, stdout) = run[1:]
                break
        else:
            self.fail(f'Command {args} not found in mocked subprocess.run')
        self._mocked_calls.append(args)
        if returncode:
            raise CalledProcessError(returncode, args, output=stdout)
        return CompletedProcess(args, returncode, stdout=stdout)

    def _add_run_result(self, args, returncode, stdout=None):
        self._mocked_runs.append((args, returncode, stdout))

    def test_find_orig_mr_fileset(self):
        orig_mr_id = '777'
        mr_list = [{'iid': '777',
                    'author': {'username': 'shadowman'},
                    'title': 'This is the base MR',
                    'targetBranch': 'os-build',
                    'project': {'fullPath': 'cki-project/kernel-ark'},
                    'files': [{'path': 'some/file.c'}]
                    },
                   {'iid': '876',
                    'author': {'username': 'shadowman'},
                    'title': 'This is a test of the emergency mergecast system',
                    'targetBranch': 'os-build',
                    'project': {'fullPath': 'cki-project/kernel-ark'},
                    'files': [{'path': 'some/file.c'}]
                    }]
        result = mergehook.find_orig_mr_fileset(mr_list, orig_mr_id)
        self.assertEqual(result, {'some/file.c'})

    @mock.patch('webhook.mergehook.find_orig_mr_fileset')
    def test_fetch_mr_list(self, mock_orig_files):
        mock_orig_files.return_value = {'some/file.c'}
        mock_mmr = mock.Mock()
        mock_mmr.namespace = 'cki-project/kernel-ark'
        mock_mmr.iid = '777'
        mock_mmr.target_branch = 'os-build'

        mr_list = [{'iid': '876',
                    'author': {'username': 'shadowman'},
                    'title': 'This is a test of the emergency mergecast system',
                    'targetBranch': 'os-build',
                    'project': {'fullPath': 'cki-project/kernel-ark'},
                    'files': [{'path': 'some/file.c'}]
                    }]
        qres = {'project':
                {'id': 'gid://gitlab/Project/13604247',
                 'mergeRequests':
                 {'pageInfo': {'hasNextPage': False, 'endCursor': 'xyz'},
                  'nodes': mr_list
                  }
                 }
                }
        mock_mmr.session.graphql.check_query_results.return_value = qres
        result = mergehook.MergeMR.fetch_mr_list(mock_mmr)
        self.assertEqual(result, mr_list)
        mock_orig_files.return_value = {}
        with self.assertLogs('cki.webhook.mergehook', level='WARNING') as logs:
            result = mergehook.MergeMR.fetch_mr_list(mock_mmr)
            self.assertIn("Originating MR 777 appears to have no files", logs.output[-1])
            self.assertEqual(result, [])

    @mock.patch('webhook.mergehook.MergeMR.check_for_replicants')
    @mock.patch('webhook.mergehook.MergeMR.fetch_mr_list')
    @mock.patch('webhook.kgit.branch_copy', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.branch_mergeable', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.hard_reset', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.branch_delete', mock.Mock(return_value=True))
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_check_for_other_merge_conflicts_ok(self, mock_fmrl, mock_replicants):
        mock_mmr = mock.Mock()
        mock_mmr.project_remote = 'kernel-ark'
        mock_mmr.target_branch = 'os-build'
        mock_mmr.iid = 66
        mock_mmr.worktree_dir = '/src/kernel-ark/'
        mock_mmr.description = mock.Mock(depends_mrs={})
        mock_mmr.work_branch = 'temp-merge-branch'
        mock_mmr.worktree_dir = '/src/kernel-ark-temp-merge-branch/'
        mock_mmr.check_for_replicants = mock_replicants
        mock_replicants.return_value = False

        mr_list = [{'iid': '44',
                    'author': {'username': 'shadowman'},
                    'title': 'Update the kernel release name',
                    'targetBranch': 'os-build',
                    'project': {'fullPath': 'cki-project/kernel-ark'},
                    'labels': {'nodes': [{'title': 'blah'}]}
                    },
                   {'iid': '55',
                    'author': {'username': 'joedev'},
                    'title': 'A terrible misguided commit without Signof-by',
                    'targetBranch': 'os-build',
                    'project': {'fullPath': 'cki-project/kernel-ark'},
                    'labels': {'nodes': [{'title': 'blah'}]}
                    },
                   {'iid': '66',
                    'author': {'username': 'rhnewbie'},
                    'title': 'My first MR!',
                    'targetBranch': 'os-build',
                    'project': {'fullPath': 'cki-project/kernel-ark'},
                    'labels': {'nodes': [{'title': 'blah'}]}
                    }
                   ]
        mock_mmr.fetch_mr_list = mock_fmrl
        mock_fmrl.return_value = mr_list
        m44_args = ['git', 'merge', '--quiet', '--no-edit',
                    f"{mock_mmr.project_remote}/merge-requests/{mr_list[0]['iid']}"]
        m55_args = ['git', 'merge', '--quiet', '--no-edit',
                    f"{mock_mmr.project_remote}/merge-requests/{mr_list[1]['iid']}"]
        m66_args = ['git', 'merge', '--quiet', '--no-edit',
                    f"{mock_mmr.project_remote}/merge-requests/{mr_list[1]['iid']}"]
        self._add_run_result(m44_args, 0)
        self._add_run_result(m55_args, 0)
        self._add_run_result(m66_args, 0)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = mergehook.MergeMR.check_for_other_merge_conflicts(mock_mmr)
            self.assertFalse(ret)

    @mock.patch('webhook.mergehook.MergeMR.check_for_replicants')
    @mock.patch('webhook.kgit.branch_mergeable')
    @mock.patch('webhook.mergehook.MergeMR.fetch_mr_list')
    @mock.patch('webhook.kgit.branch_copy', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.hard_reset', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.branch_delete', mock.Mock(return_value=True))
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_check_for_other_merge_conflicts_bad(self, mock_fmrl, mock_mergeable, mock_replicants):
        mock_mmr = mock.Mock()
        mock_mmr.project_remote = 'kernel-ark'
        mock_mmr.target_branch = 'os-build'
        mock_mmr.iid = 66
        mock_mmr.worktree_dir = '/src/kernel-ark/'
        mock_mmr.description = mock.Mock(depends_mrs={})
        mock_mmr.work_branch = 'temp-merge-branch'
        mock_mmr.worktree_dir = '/src/kernel-ark-temp-merge-branch/'
        mock_mmr.conflicts = []

        mr_list = [{'iid': '44',
                    'author': {'username': 'shadowman'},
                    'title': 'Update the kernel release name',
                    'targetBranch': 'os-build',
                    'project': {'fullPath': 'cki-project/kernel-ark'},
                    'labels': {'nodes': [{'title': defs.MERGE_CONFLICT_LABEL}]}
                    }]
        mock_mmr.fetch_mr_list = mock_fmrl
        mock_fmrl.return_value = mr_list
        mock_mmr.check_for_replicants = mock_replicants
        mock_replicants.return_value = False
        m_args = ['git', 'merge', '--quiet', '--no-edit',
                  f"{mock_mmr.project_remote}/merge-requests/{mr_list[0]['iid']}"]

        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = mergehook.MergeMR.check_for_other_merge_conflicts(mock_mmr)
            self.assertNotIn(m_args, self._mocked_calls)
            self.assertFalse(ret)

        mr_list = [{'iid': '44',
                    'author': {'username': 'shadowman'},
                    'title': 'Update the kernel release name',
                    'targetBranch': 'os-build',
                    'project': {'fullPath': 'cki-project/kernel-ark'},
                    'labels': {'nodes': [{'title': 'blah'}]}
                    }]
        error_text = (f"MR !{mr_list[0]['iid']} from @{mr_list[0]['author']['username']} "
                      f"(`{mr_list[0]['title']}`) conflicts with this MR.  \n")
        mock_fmrl.return_value = mr_list
        m_args = ['git', 'merge', '--quiet', '--no-edit',
                  f"{mock_mmr.project_remote}/merge-requests/{mr_list[0]['iid']}"]

        mock_mergeable.return_value = False
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = mergehook.MergeMR.check_for_other_merge_conflicts(mock_mmr)
            self.assertNotIn(m_args, self._mocked_calls)
            self.assertFalse(ret)

        conflict_note = ("CONFLICT: your dependency MR has different hashes from the ones "
                         "included in your MR. Please rebase this MR on top of the "
                         "current version of !44.")
        mock_mergeable.return_value = True
        mock_replicants.return_value = True
        mock_mmr.description = mock.Mock(depends_mrs={44})
        self._add_run_result(m_args, 5, 'Catastrophic error!')
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = mergehook.MergeMR.check_for_other_merge_conflicts(mock_mmr)
            self.assertIn(m_args, self._mocked_calls)
            self.assertTrue(ret)
            self.assertEqual(mock_mmr.conflicts, [conflict_note, error_text, 'Catastrophic error!'])

    @mock.patch('webhook.kgit.create_worktree_timestamp', mock.Mock(return_value=True))
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_check_for_merge_conflicts_ok(self):
        mock_mmr = mock.Mock()
        mock_mmr.project_remote = 'kernel-ark'
        mock_mmr.target_branch = 'os-build'
        mock_mmr.iid = 2
        mock_mmr.worktree_dir = '/src/kernel-ark/'

        # Clean merges
        m_args = ['git', 'merge', '--quiet', '--no-edit',
                  f"{mock_mmr.project_remote}/merge-requests/{mock_mmr.iid}"]
        self._add_run_result(m_args, 0)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = mergehook.MergeMR.check_for_merge_conflicts(mock_mmr)
            self.assertIn(m_args, self._mocked_calls)
            self.assertFalse(ret)

    @mock.patch('webhook.kgit.create_worktree_timestamp', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.hard_reset', mock.Mock(return_value=True))
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_check_for_merge_conflicts_bad(self):
        mock_mmr = mock.Mock()
        mock_mmr.project_remote = 'kernel-ark'
        mock_mmr.target_branch = 'os-build'
        mock_mmr.iid = 2
        mock_mmr.worktree_dir = '/src/kernel-ark/'
        mock_mmr.conflicts = []
        error_text = (f"MR !{mock_mmr.iid} cannot be merged to "
                      f"{mock_mmr.project_remote}/{mock_mmr.target_branch}  \n")

        # Failed merge
        self._mocked_calls = []
        m_args = ['git', 'merge', '--quiet', '--no-edit',
                  f"{mock_mmr.project_remote}/merge-requests/{mock_mmr.iid}"]
        self._add_run_result(m_args, 5, 'Catastrophic error!')
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = mergehook.MergeMR.check_for_merge_conflicts(mock_mmr)
            self.assertIn(m_args, self._mocked_calls)
            self.assertTrue(ret)
            self.assertEqual(mock_mmr.conflicts, [error_text, 'Catastrophic error!'])

    @mock.patch('webhook.kgit.patch_id', mock.Mock(return_value='deadbeef1234'))
    @mock.patch('git.Repo')
    def test_check_for_replicants(self, mock_repo):
        mock_mmr = mock.Mock()
        mock_mmr.iid = 23
        mock_mmr.project_remote = 'kernel-ark'
        mock_mmr.target_branch = 'os-build'
        second_mr_id = 24

        mock_first_commit = mock.Mock()
        mock_first_commit.stats.files = {'path/to/file.c': 'diff'}
        mock_first_commit.hexsha = '123412341234'
        mock_first_commit.message = 'commit aaaaaaaaaabbbbbbbbbbccccccccccdddddddddd'
        commit_data = mergehook.CommitData(rhelcid=mock_first_commit.hexsha,
                                           files={'path/to/file.c'}, patch_id='abcd1234deadbeef')
        mock_mmr.commits_data = {}
        mock_mmr.commits_data['aaaaaaaaaabbbbbbbbbbccccccccccdddddddddd'] = commit_data
        mock_mmr.repo = mock_repo.return_value

        mock_base_commit = mock.Mock()
        mock_base_commit.hexsha = 'deadbeef1234'
        mock_mmr.repo.merge_base.return_value = [mock_base_commit]

        mock_second_commit = mock.Mock()
        mock_second_commit.stats.files = {'path/to/file.c': 'diff2'}
        mock_second_commit.hexsha = 'abcdabcdabcd'
        mock_second_commit.message = 'commit aaaaaaaaaabbbbbbbbbbccccccccccdddddddddd'
        mock_mmr.repo.iter_commits.return_value = {mock_second_commit}

        mock_mmr.build_commit_data.return_value = \
            mergehook.MergeMR.build_commit_data(mock_mmr, mock_second_commit)
        second_branch = f"refs/remotes/{mock_mmr.project_remote}/merge-requests/{second_mr_id}"
        target_branch = f"{mock_mmr.project_remote}/{mock_mmr.target_branch}"
        mock_mmr.get_commits_data.return_value = mergehook.MergeMR.get_commits_data(mock_mmr,
                                                                                    second_branch,
                                                                                    target_branch)

        with self.assertLogs('cki.webhook.mergehook', level='INFO') as logs:
            retval = mergehook.MergeMR.check_for_replicants(mock_mmr, second_mr_id)
            self.assertIn(("Flagging duplicate upstream backports for "
                           "{'aaaaaaaaaabbbbbbbbbbccccccccccdddddddddd'}"), logs.output[-1])
            self.assertTrue(retval)

        mock_mmr.repo.iter_commits.return_value = {mock_first_commit}
        mock_mmr.build_commit_data.return_value = \
            mergehook.MergeMR.build_commit_data(mock_mmr, mock_first_commit)
        mock_mmr.get_commits_data.return_value = mergehook.MergeMR.get_commits_data(mock_mmr,
                                                                                    second_branch,
                                                                                    target_branch)
        with self.assertLogs('cki.webhook.mergehook', level='DEBUG') as logs:
            retval = mergehook.MergeMR.check_for_replicants(mock_mmr, second_mr_id)
            self.assertIn(("Skipping 123412341234, exact same git object present in both "
                           "MR 23 and 24"), logs.output[-1])
            self.assertFalse(retval)

    def test_find_reverts(self):
        commits = {}
        commit1 = mock.Mock(sha='deadbeefffff',
                            title='This is a great commit',
                            description=Description(text='It does neat things'))
        commit2 = mock.Mock(sha='432100000000',
                            title='Revert: This is not a great commit',
                            description=Description(text='This reverts commit deadbeefffff.'))
        commit3 = mock.Mock(sha='abcdef012345',
                            title='Something is happening here',
                            description=Description(text='This is a dep commit.'))
        commits[commit1.sha] = commit1
        commits[commit2.sha] = commit2
        commits[commit3.sha] = commit3
        mock_mmr = mock.Mock()
        mock_mmr.commits = commits
        mock_mmr.first_dep_sha = 'abcdef012345'
        mock_mmr.reverts = set()
        mergehook.MergeMR.find_reverts(mock_mmr)
        self.assertEqual(mock_mmr.reverts, {'deadbeefffff'})

    def test_find_ignore_duplicates(self):
        mock_mmr = mock.Mock()
        mock_mmr.description = Description(text='Ignore-duplicate: abcd1234feedbeef')
        mock_mmr.overrides = set()
        mergehook.MergeMR.find_ignore_duplicates(mock_mmr)
        self.assertEqual(mock_mmr.overrides, {'abcd1234feedbeef'})

    def test_exclude_ok_possible_duplicate(self):
        mock_mmr = mock.Mock()
        mock_mmr.iid = 23
        mock_mmr.project_remote = 'kernel-ark'
        mock_base_commit = mock.Mock()
        sha_a = 'abcd1234abcd1234abcd1234abcd1234abcd1234'
        sha_b = '1234abcd1234abcd1234abcd1234abcd1234abcd'
        mock_base_commit.hexsha = sha_a
        mock_mmr.repo.merge_base.return_value = [mock_base_commit]
        mock_mmr.reverts = {sha_a}
        with self.assertLogs('cki.webhook.mergehook', level='WARNING') as logs:
            self.assertTrue(mergehook.MergeMR.exclude_ok_possible_duplicate(mock_mmr, sha_a))
            self.assertIn(f'Commit {sha_a} found as reverted', logs.output[-1])
        mock_mmr.reverts = set()
        mock_mmr.overrides = {sha_a}
        with self.assertLogs('cki.webhook.mergehook', level='WARNING') as logs:
            self.assertTrue(mergehook.MergeMR.exclude_ok_possible_duplicate(mock_mmr, sha_a))
            self.assertIn(f'Duplicate commit {sha_a} in MR overrides', logs.output[-1])
        mock_mmr.overrides = set()
        with self.assertLogs('cki.webhook.mergehook', level='WARNING') as logs:
            self.assertTrue(mergehook.MergeMR.exclude_ok_possible_duplicate(mock_mmr, sha_a))
            self.assertIn(f'Commit {sha_a} found as merge_base', logs.output[-1])
        self.assertFalse(mergehook.MergeMR.exclude_ok_possible_duplicate(mock_mmr, sha_b))

    @mock.patch('webhook.kgit.patch_id')
    @mock.patch('git.Repo')
    def test_check_for_existing_backports(self, mock_repo, mock_patch_id):
        mock_mmr = mock.Mock()
        mock_mmr.iid = 23
        mock_mmr.project_remote = 'kernel-ark'
        mock_mmr.target_branch = 'os-build'
        mock_mmr.repo = mock_repo.return_value
        mock_mmr.session.args.no_kerneloscope = False
        mock_mmr.reverts = set()
        mock_mmr.overrides = set()
        mock_commit = mock.Mock()
        mock_commit.stats.files = {'path/to/file.c': 'diff2'}
        mock_commit.hexsha = 'abcdabcdabcd'
        mock_commit.message = 'commit aaaaaaaaaabbbbbbbbbbccccccccccdddddddddd'
        mock_mmr.repo.iter_commits.return_value = {mock_commit}
        mock_mmr.repo.commit.return_value = mock_commit
        mock_mmr.exclude_ok_possible_duplicate.return_value = False
        commit_data = mergehook.CommitData(rhelcid=mock_commit.hexsha,
                                           files={'path/to/file.c'}, patch_id='abcd1234deadbeef')
        mock_mmr.build_commit_data.return_value = commit_data
        mr_branch = f"refs/remotes/{mock_mmr.project_remote}/merge-requests/{mock_mmr.iid}"
        target_branch = f"{mock_mmr.project_remote}/{mock_mmr.target_branch}"
        mock_mmr.get_commits_data.return_value = mergehook.MergeMR.get_commits_data(mock_mmr,
                                                                                    mr_branch,
                                                                                    target_branch)
        mock_mmr.find_already_backported.return_value = [{'commit': '123412341234',
                                                          'subject': 'This is a patch title'}]
        mock_mmr.find_already_backported_orig.return_value = [{'commit': '123412341234',
                                                               'subject': 'This is a patch title'}]

        mock_patch_id.return_value = 'deadbeef1234abcd'
        with self.assertLogs('cki.webhook.mergehook', level='DEBUG') as logs:
            retval = mergehook.MergeMR.check_for_existing_backports(mock_mmr)
            self.assertIn("different patch-id than already committed", logs.output[-2])
            self.assertIn(("Duplicate backports already in tree: "
                           "{'aaaaaaaaaabbbbbbbbbbccccccccccdddddddddd'}"), logs.output[-1])
            self.assertTrue(retval)

        mock_patch_id.return_value = 'abcd1234deadbeef'
        with self.assertLogs('cki.webhook.mergehook', level='DEBUG') as logs:
            retval = mergehook.MergeMR.check_for_existing_backports(mock_mmr)
            self.assertIn("has the same patch-id as", logs.output[-2])
            self.assertIn(("Duplicate backports already in tree: "
                           "{'aaaaaaaaaabbbbbbbbbbccccccccccdddddddddd'}"), logs.output[-1])
            self.assertTrue(retval)

        mock_mmr.session.args.no_kerneloscope = True
        mock_patch_id.return_value = 'abcd1234deadbeef'
        with self.assertLogs('cki.webhook.mergehook', level='DEBUG') as logs:
            retval = mergehook.MergeMR.check_for_existing_backports(mock_mmr)
            self.assertIn("has the same patch-id as", logs.output[-2])
            self.assertIn(("Duplicate backports already in tree: "
                           "{'aaaaaaaaaabbbbbbbbbbccccccccccdddddddddd'}"), logs.output[-1])
            self.assertTrue(retval)

        mock_mmr.exclude_ok_possible_duplicate.return_value = True

        mock_patch_id.return_value = 'abcd1234deadbeef'
        mock_mmr.reverts = {'abcdabcdabcd'}
        with self.assertLogs('cki.webhook.mergehook', level='DEBUG') as logs:
            retval = mergehook.MergeMR.check_for_existing_backports(mock_mmr)
            self.assertIn(("Duplicate backports already in tree: set()"), logs.output[-1])
            self.assertFalse(retval)

        mock_mmr.session.args.no_kerneloscope = False
        mock_mmr.reverts = set()
        mock_mmr.overrides = {'abcdabcdabcd'}
        with self.assertLogs('cki.webhook.mergehook', level='DEBUG') as logs:
            retval = mergehook.MergeMR.check_for_existing_backports(mock_mmr)
            self.assertIn(("Duplicate backports already in tree: set()"), logs.output[-1])
            self.assertFalse(retval)

        mock_mmr.find_already_backported.return_value = [{'commit': '123412341234',
                                                          'subject': 'This is a patch title'},
                                                         {'commit': 'feeddeadbeef',
                                                          'subject': 'revert this bad patch'}]
        mock_mmr.reverts = set()
        mock_mmr.overrides = {'abcdabcdabcd'}
        with self.assertLogs('cki.webhook.mergehook', level='DEBUG') as logs:
            retval = mergehook.MergeMR.check_for_existing_backports(mock_mmr)
            self.assertIn(("Duplicate backports already in tree: set()"), logs.output[-1])
            self.assertFalse(retval)

    def test_format_conflict_info(self):
        mock_mmr = mock.Mock()
        mock_mmr.merge_label = defs.MERGE_CONFLICT_LABEL
        mock_mmr.conflicts = ["MR has some issues", "CONFLICT in here\nNothing over there\n"]
        output = mergehook.MergeMR.format_conflict_info(mock_mmr)
        self.assertIn("This merge request cannot be merged to its target branch", output)
        self.assertIn("MR has some issues", output)
        self.assertIn("* CONFLICT in here  ", output)
        self.assertNotIn("Nothing over there", output)

        mock_mmr.merge_label = defs.MERGE_WARNING_LABEL
        output = mergehook.MergeMR.format_conflict_info(mock_mmr)
        self.assertIn("There are other pending MRs that conflict with this one", output)
        self.assertIn("MR has some issues", output)
        self.assertIn("* CONFLICT in here  ", output)
        self.assertNotIn("Nothing over there", output)

        mock_mmr.merge_label = "Merge::OK"
        mock_mmr.conflicts = []
        output = mergehook.MergeMR.format_conflict_info(mock_mmr)
        self.assertIn("This MR can be merged cleanly to its target branch", output)

    @mock.patch('webhook.kgit._git')
    @mock.patch('webhook.kgit.handle_stale_worktree', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.clean_up_temp_merge_branch', mock.Mock(return_value=True))
    @mock.patch('webhook.comments.CommentCache.update_comment', mock.Mock())
    @mock.patch('webhook.base_mr.BaseMR.add_labels')
    @mock.patch('webhook.mergehook.MergeMR.check_for_other_merge_conflicts')
    @mock.patch('webhook.mergehook.MergeMR.check_for_merge_conflicts')
    @mock.patch('webhook.mergehook.MergeMR.check_for_existing_backports')
    def test_process_merge_request(self, mock_existing, mock_conflicts, mock_other_conflicts,
                                   mock_add_label, mock_git):
        mock_inst = mock.Mock()
        mock_proj = mock.Mock()
        mock_proj.name = 'kernel-ark'
        mock_projects = mock.Mock()
        mock_projects.return_value.get_project_by_id.return_value = mock.Mock(confidential=False)
        mock_mr = mock.Mock(iid=66)
        mock_mr.target_branch = 'os-build'
        mock_gql = mock.Mock()
        mr_commit = {'author': {'username': 'shadowman'}, 'sha': 'feeddeadbeef1234'}
        mr_data = {'project': {'mr': {'commitCount': 0, 'commits': {'nodes': [mr_commit]}}}}
        mock_gql.client.query.return_value = mr_data
        mock_session = SessionRunner.new('mergehook', [], mergehook.HANDLERS)
        mock_session.get_gl_instance = mock.Mock(return_value=mock_inst)
        mock_session.get_graphql = mock.Mock(return_value=mock_gql)
        mock_session.get_gl_project = mock.Mock(return_value=mock_proj)
        mock_session.args = mock.Mock(rhkernel_src='/src/kernel-ark')
        mock_existing.return_value = False
        mock_conflicts.return_value = True
        mock_other_conflicts.return_value = ['CONFLICTS']
        mock_event = mock.Mock(
            mr_url=defs.GitlabURL('https://gitlab.com/cki-project/kernel-ark/-/merge_requests/66'),
            kind=defs.GitlabObjectKind.MERGE_REQUEST,
            namespace='cki-project/kernel-ark',
            gl_mr=mock_mr,
        )
        with mock.patch('webhook.mergehook.event_days_old', mock.Mock()) as patched_event_days_old:
            patched_event_days_old.return_value = 0
            mergehook.process_gl_event({}, mock_session, mock_event)
            mock_add_label.assert_called_with([defs.MERGE_CONFLICT_LABEL])

            mock_conflicts.return_value = False
            mergehook.process_gl_event({}, mock_session, mock_event)
            mock_add_label.assert_called_with([defs.MERGE_WARNING_LABEL])

            mock_other_conflicts.return_value = False
            mergehook.process_gl_event({}, mock_session, mock_event)
            mock_add_label.assert_called_with([f'Merge::{defs.READY_SUFFIX}'])

            mock_existing.return_value = True
            mergehook.process_gl_event({}, mock_session, mock_event)
            mock_add_label.assert_called_with([defs.MERGE_CONFLICT_LABEL])


class TestEventDaysOld(KwfTestCase):
    """Test the event_days_old function."""

    class EventAgeTest(typing.NamedTuple):

        expected_days_old: int
        test_run_timestamp: str
        event_headers_timestamp: str
        event_body: dict
        event_body_mr_updated_at: str

    def run_test(self, test: 'TestEventDaysOld.EventAgeTest') -> None:
        """Run an EventAgeTest."""
        headers = {'message-type': 'gitlab'}
        if test.event_headers_timestamp:
            headers['message-date'] = test.event_headers_timestamp

        body = test.event_body or \
            self.load_yaml_asset('gl_merge_request_1.json', sub_module='webhook_events')
        if test.event_body_mr_updated_at:
            key = None
            if 'merge_request' in body:
                key = 'merge_request'
            elif 'object_attributes' in body:
                key = 'object_attributes'
            if key:
                body[key]['updated_at'] = test.event_body_mr_updated_at

        test_session = SessionRunner.new('mergehook', '', mergehook.HANDLERS)
        test_event = create_event(test_session, headers, body)

        self.freeze_time(test.test_run_timestamp)
        self.assertEqual(mergehook.event_days_old(test_event), test.expected_days_old)

    def test_event_header_ts_0_days_old(self) -> None:
        """Uses BaseEvent.timestamp to find event is 0 days old."""
        self.run_test(self.EventAgeTest(
            expected_days_old=0,
            test_run_timestamp='Fri 01 May 2024 12:00:00 UTC',
            event_headers_timestamp='2024-05-01T11:59:00+00:00',
            event_body={},
            event_body_mr_updated_at=''
        ))

    def test_event_header_ts_5_days_old(self) -> None:
        """Uses BaseEvent.timestamp to find event is 5 days old."""
        self.run_test(self.EventAgeTest(
            expected_days_old=5,
            test_run_timestamp='Fri 06 May 2024 12:00:00 UTC',
            event_headers_timestamp='2024-05-01T11:59:00+00:00',
            event_body={},
            event_body_mr_updated_at=''
        ))

    def test_event_body_updated_at_0_days_old(self) -> None:
        """Uses Event body to find event is 0 days old."""
        self.run_test(self.EventAgeTest(
            expected_days_old=0,
            test_run_timestamp='Fri 01 May 2024 12:00:00 UTC',
            event_headers_timestamp='',
            event_body={},
            event_body_mr_updated_at='2024-05-01 05:00:00 UTC'
        ))

    def test_event_body_updated_at_3_days_old(self) -> None:
        """Uses Event body to find event is 3 days old."""
        self.run_test(self.EventAgeTest(
            expected_days_old=3,
            test_run_timestamp='Fri 04 May 2024 12:00:00 UTC',
            event_headers_timestamp='',
            event_body={},
            event_body_mr_updated_at='2024-05-01 05:00:00 UTC'
        ))

    def test_no_timestamps(self) -> None:
        """Returns 0 when there are no working available timestamps."""
        self.run_test(self.EventAgeTest(
            expected_days_old=0,
            test_run_timestamp='Fri 04 May 2024 12:00:00 UTC',
            event_headers_timestamp='',
            event_body={},
            event_body_mr_updated_at='a very bad timestamp'
        ))
