"""Webhook interaction tests."""

from copy import deepcopy
from datetime import datetime
import json
import typing
from unittest import mock

from cki_lib.gitlab import get_instance
from gitlab.exceptions import GitlabGetError
from gitlab.v4.objects.pipelines import ProjectPipeline
from gitlab.v4.objects.projects import Project
from reporter.data import CheckoutData
from requests import HTTPError
import responses

from tests.helpers import KwfTestCase
from webhook import defs
from webhook.pipelines import BridgeJob
from webhook.pipelines import PipelineResult
from webhook.pipelines import PipelineStatus
from webhook.pipelines import PipelineType
from webhook.pipelines import SetupJob

# A regular pipeline which has completed successfully.
PIPE1_STAGES = [{'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'prepare'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'merge'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'build'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'build-tools'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'publish'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'setup'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'test'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'wait-for-triage'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'kernel-results'}]
PIPE1_DICT = {'allowFailure': False,
              'createdAt': '2023-04-11T13:39:18Z',
              'downstreamPipeline': {'id': 'gid://gitlab/Ci::Pipeline/345',
                                     'project': {'id': 'gid://gitlab/Project/765',
                                                 'fullPath': 'group/project'},
                                     'stages': {'nodes': PIPE1_STAGES},
                                     'status': 'SUCCESS'},
              'id': 'gid://gitlab/Ci::Bridge/865',
              'name': 'c9s_merge_request',
              'pipeline': {'id': 'gid://gitlab/Ci::Pipeline/653'},
              'status': 'FAILED'}  # Example of the bug gitlab#340064

# A running RT pipeline.
PIPE2_STAGES = [{'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'prepare'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'merge'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'build'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'build-tools'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'publish'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'setup'},
                {'jobs': {'nodes': [{'status': 'RUNNING'}]}, 'name': 'test'},
                {'jobs': {'nodes': [{'status': 'PENDING'}]}, 'name': 'wait-for-triage'},
                {'jobs': {'nodes': [{'status': 'PENDING'}]}, 'name': 'kernel-results'}]
PIPE2_DICT = {'allowFailure': True,
              'createdAt': '2023-04-11T13:37:10Z',
              'downstreamPipeline': {'id': 'gid://gitlab/Ci::Pipeline/936',
                                     'project': {'id': 'gid://gitlab/Project/765',
                                                 'fullPath': 'group/project'},
                                     'stages': {'nodes': PIPE2_STAGES},
                                     'status': 'RUNNING'},
              'id': 'gid://gitlab/Ci::Bridge/235',
              'name': 'c9s_rt_merge_request',
              'pipeline': {'id': 'gid://gitlab/Ci::Pipeline/653'},
              'status': 'RUNNING'}

# An older failed RT pipeline.
PIPE3_STAGES = [{'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'prepare'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'merge'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'build'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'build-tools'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'publish'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'setup'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'test'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'wait-for-triage'},
                {'jobs': {'nodes': [{'status': 'FAILED'}]}, 'name': 'kernel-results'}]
PIPE3_DICT = {'allowFailure': True,
              'createdAt': '2023-04-10T23:13:52Z',
              'downstreamPipeline': {'id': 'gid://gitlab/Ci::Pipeline/726',
                                     'project': {'id': 'gid://gitlab/Project/765',
                                                 'fullPath': 'group/project'},
                                     'stages': {'nodes': PIPE3_STAGES},
                                     'status': 'FAILED'},
              'id': 'gid://gitlab/Ci::Bridge/161',
              'name': 'c9s_rt_merge_request',
              'pipeline': {'id': 'gid://gitlab/Ci::Pipeline/653'},
              'status': 'FAILED'}

# A kernel-ark pipeline.
PIPE4_STAGES = [{'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'prepare'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'merge'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'build'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'publish'}]
PIPE4_DICT = {'allowFailure': False,
              'createdAt': '2023-04-15T01:12:12Z',
              'downstreamPipeline': {'id': 'gid://gitlab/Ci::Pipeline/622',
                                     'project': {'id': 'gid://gitlab/Project/836',
                                                 'fullPath': 'group/project'},
                                     'stages': {'nodes': PIPE4_STAGES},
                                     'status': 'SUCCESS'},
              'id': 'gid://gitlab/Ci::Bridge/243',
              'name': 'rawhide_merge_request',
              'pipeline': {'id': 'gid://gitlab/Ci::Pipeline/874'},
              'status': 'SUCCESS'}


class TestPipelineType(KwfTestCase):
    """Tests for the PipelineType enum."""

    PIPE_MAP = {'rhel9_private_merge_request': PipelineType.RHEL,
                'c9s_merge_request': PipelineType.CENTOS,
                'c9s_realtime_merge_request': PipelineType.REALTIME,
                'c9s_rt_merge_request': PipelineType.REALTIME,
                'c9s_realtime_automotive_check_merge_request': PipelineType.AUTOMOTIVE,
                'c9s_rt_64k_merge_request': PipelineType.REALTIME_64K,
                'c9s_rt_64k_debug_merge_request': PipelineType.REALTIME_64K_DEBUG,
                'c9s_automotive_merge_request': PipelineType.AUTOMOTIVE,
                'c9s_automotive_debug_merge_request': PipelineType.AUTOMOTIVE_DEBUG,
                'c9s_rhel9_compat_merge_request': PipelineType.RHEL_COMPAT,
                'c9s_64k_merge_request': PipelineType._64K,
                'c9s_debug_merge_request': PipelineType.DEBUG,
                'c9s_zfcpdump_merge_request': PipelineType.ZFCPDUMP,
                'eln_merge_request': PipelineType.ELN,
                'eln_debug_merge_request': PipelineType.ELN_DEBUG,
                'eln_rt_merge_request': PipelineType.ELN_REALTIME,
                'eln_rt_debug_merge_request': PipelineType.ELN_REALTIME_DEBUG,
                'eln_realtime_merge_request': PipelineType.ELN_REALTIME,
                'eln_realtime_debug_merge_request': PipelineType.ELN_REALTIME_DEBUG,
                'eln_rt_64k_merge_request': PipelineType.ELN_REALTIME_64K,
                'eln_rt_64k_debug_merge_request': PipelineType.ELN_REALTIME_64K_DEBUG,
                'eln_64k_merge_request': PipelineType.ELN_64K,
                'eln_64k_debug_merge_request': PipelineType.ELN_64K_DEBUG,
                'eln_clang_merge_request': PipelineType.ELN_CLANG,
                'eln_clang_debug_merge_request': PipelineType.ELN_CLANG_DEBUG,
                'rawhide_merge_request': PipelineType.RAWHIDE,
                'rawhide_debug_merge_request': PipelineType.RAWHIDE_DEBUG,
                'rawhide_16k_merge_request': PipelineType.RAWHIDE_16K,
                'rawhide_16k_debug_merge_request': PipelineType.RAWHIDE_16K_DEBUG,
                'rawhide_clang_merge_request': PipelineType.RAWHIDE_CLANG,
                'rawhide_clang_debug_merge_request': PipelineType.RAWHIDE_CLANG_DEBUG,
                'rawhide_clanglto_merge_request': PipelineType.RAWHIDE_CLANGLTO,
                'rawhide_clanglto_debug_merge_request': PipelineType.RAWHIDE_CLANGLTO_DEBUG,
                }

    def _test_pipeline(self, func, pipe_name, result):
        result_string = result.name if isinstance(result, PipelineType) else result
        print(f"Testing {func.__name__}() with '{pipe_name}', expecting '{result_string}'...")
        self.assertIs(func(pipe_name), result)

    def test_pipelinetype_from_str(self):
        """Returns the expected PipelineType value for the given input."""
        func = PipelineType.get
        self._test_pipeline(func, '', PipelineType.INVALID)
        self._test_pipeline(func, '_what_', PipelineType.INVALID)
        self._test_pipeline(func, 'rhel', PipelineType.RHEL)
        self._test_pipeline(func, 'CentOS', PipelineType.CENTOS)
        self._test_pipeline(func, 'REALTIME', PipelineType.REALTIME)
        self._test_pipeline(func, 'realtime_debug', PipelineType.REALTIME_DEBUG)
        self._test_pipeline(func, 'realtime_64k', PipelineType.REALTIME_64K)
        self._test_pipeline(func, 'realtime_64k_debug', PipelineType.REALTIME_64K_DEBUG)
        self._test_pipeline(func, 'Rhel_Compat', PipelineType.RHEL_COMPAT)
        self._test_pipeline(func, 'Automotive', PipelineType.AUTOMOTIVE)
        self._test_pipeline(func, 'Automotive_debug', PipelineType.AUTOMOTIVE_DEBUG)
        self._test_pipeline(func, '64k', PipelineType._64K)
        self._test_pipeline(func, 'debug', PipelineType.DEBUG)
        self._test_pipeline(func, 'clang', PipelineType.CLANG)
        self._test_pipeline(func, 'clang_debug', PipelineType.CLANG_DEBUG)
        self._test_pipeline(func, 'rawhide', PipelineType.RAWHIDE)
        self._test_pipeline(func, 'rawhide_debug', PipelineType.RAWHIDE_DEBUG)
        self._test_pipeline(func, 'rawhide_16k', PipelineType.RAWHIDE_16K)
        self._test_pipeline(func, 'rawhide_16k_debug', PipelineType.RAWHIDE_16K_DEBUG)
        self._test_pipeline(func, 'rawhide_clang', PipelineType.RAWHIDE_CLANG)
        self._test_pipeline(func, 'rawhide_clang_debug', PipelineType.RAWHIDE_CLANG_DEBUG)
        self._test_pipeline(func, 'rawhide_clanglto', PipelineType.RAWHIDE_CLANGLTO)
        self._test_pipeline(func, 'rawhide_clanglto_debug', PipelineType.RAWHIDE_CLANGLTO_DEBUG)
        self._test_pipeline(func, 'eln', PipelineType.ELN)
        self._test_pipeline(func, 'eln_debug', PipelineType.ELN_DEBUG)
        self._test_pipeline(func, 'eln_64k', PipelineType.ELN_64K)
        self._test_pipeline(func, 'eln_64k_debug', PipelineType.ELN_64K_DEBUG)
        self._test_pipeline(func, 'eln_clang', PipelineType.ELN_CLANG)
        self._test_pipeline(func, 'eln_clang_debug', PipelineType.ELN_CLANG_DEBUG)
        self._test_pipeline(func, 'eln_realtime', PipelineType.ELN_REALTIME)
        self._test_pipeline(func, 'eln_realtime_debug', PipelineType.ELN_REALTIME_DEBUG)
        self._test_pipeline(func, 'eln_realtime_64k', PipelineType.ELN_REALTIME_64K)
        self._test_pipeline(func, 'eln_realtime_64k_debug', PipelineType.ELN_REALTIME_64K_DEBUG)
        for pipe_name, pipe_type in self.PIPE_MAP.items():
            self._test_pipeline(func, pipe_name, pipe_type)


class TestPipeStatus(KwfTestCase):
    """Tests for the PipelineStatus enum."""

    def test_title(self):
        """Title should capitalize() all Statuses except OK."""
        self.assertEqual(PipelineStatus.FAILED.title, 'Failed')
        self.assertEqual(PipelineStatus.CREATED.title, 'Running')
        self.assertEqual(PipelineStatus.OK.title, 'OK')


class TestPipelineResult(KwfTestCase):
    """Tests for the PipelineResult dataclass."""

    def test_init(self):
        """Creates a new PipelineResult with default values."""
        data = {'allow_failure': False,
                'bridge_gid': '',
                'bridge_status': '',
                'bridge_name': '',
                'created_at': '',
                'ds_pipeline_gid': '',
                'ds_project_gid': '',
                'ds_namespace': '',
                'mr_pipeline_gid': '',
                'stage_data': [],
                'status': ''
                }

        new_pipe = PipelineResult(**data)

        # Transformed attributes.
        self.assertIs(new_pipe.status, PipelineStatus.UNKNOWN)
        self.assertEqual(new_pipe.created_at, '')

        # Derived properties.
        self.assertEqual(new_pipe.bridge_id, 0)
        self.assertEqual(new_pipe.bridge_status, "")
        self.assertEqual(new_pipe.ds_pipeline_id, 0)
        self.assertEqual(new_pipe.ds_project_id, 0)
        self.assertEqual(new_pipe.ds_url, None)
        self.assertEqual(new_pipe.mr_pipeline_id, 0)
        self.assertEqual(new_pipe.label, defs.Label(''))
        self.assertEqual(new_pipe.failed_stage, None)
        self.assertEqual(new_pipe.kcidb_data, None)
        self.assertIs(new_pipe.type, PipelineType.INVALID)

        # represent
        self.assertIn('status: UNKNOWN', str(new_pipe))

    def test_init_from_dict_pipe1(self):
        """Object properties should be set."""
        input_dict = deepcopy(PIPE1_DICT)
        new_pipe = PipelineResult.from_dict(input_dict)

        # Attributes.
        self.assertIs(new_pipe.allow_failure, False)

        # Transformed attributes.
        self.assertIs(new_pipe.status, PipelineStatus.OK)
        self.assertEqual(new_pipe.created_at, datetime(2023, 4, 11, 13, 39, 18))

        # Derived properties.
        self.assertIs(new_pipe.status, PipelineStatus.OK)
        self.assertEqual(new_pipe.bridge_id, 865)
        self.assertEqual(new_pipe.bridge_status, PipelineStatus.FAILED, "gitlab#340064 example")
        self.assertEqual(new_pipe.ds_pipeline_id, 345)
        self.assertEqual(new_pipe.ds_project_id, 765)
        self.assertEqual(new_pipe.mr_pipeline_id, 653)
        self.assertEqual(new_pipe.ds_url, 'https://gitlab.com/group/project/-/pipelines/345')
        self.assertEqual(new_pipe.label, defs.Label('CKI_CentOS::OK'))
        self.assertEqual(new_pipe.failed_stage, None)
        self.assertIs(new_pipe.type, PipelineType.CENTOS)

    def test_init_from_dict_pipe2(self):
        """Object properties should be set."""
        input_dict = deepcopy(PIPE2_DICT)
        new_pipe = PipelineResult.from_dict(input_dict)

        # Attributes.
        self.assertIs(new_pipe.allow_failure, True)

        # Transformed attributes.
        self.assertIs(new_pipe.status, PipelineStatus.RUNNING)
        self.assertEqual(new_pipe.created_at, datetime(2023, 4, 11, 13, 37, 10))

        # Derived properties.
        self.assertIs(new_pipe.status, PipelineStatus.RUNNING)
        self.assertEqual(new_pipe.bridge_id, 235)
        self.assertEqual(new_pipe.bridge_status, PipelineStatus.RUNNING)
        self.assertEqual(new_pipe.ds_pipeline_id, 936)
        self.assertEqual(new_pipe.ds_project_id, 765)
        self.assertEqual(new_pipe.mr_pipeline_id, 653)
        self.assertEqual(new_pipe.ds_url, 'https://gitlab.com/group/project/-/pipelines/936')
        self.assertEqual(new_pipe.label, defs.Label('CKI_RT::Running'))
        self.assertEqual(new_pipe.failed_stage, None)
        self.assertIs(new_pipe.type, PipelineType.REALTIME)

    def test_init_from_dict_pipe3(self):
        """Object properties should be set."""
        input_dict = deepcopy(PIPE3_DICT)
        new_pipe = PipelineResult.from_dict(input_dict)

        # Attributes.
        self.assertIs(new_pipe.allow_failure, True)

        # Transformed attributes.
        self.assertIs(new_pipe.status, PipelineStatus.FAILED)
        self.assertEqual(new_pipe.created_at, datetime(2023, 4, 10, 23, 13, 52))

        # Derived properties.
        self.assertIs(new_pipe.status, PipelineStatus.FAILED)
        self.assertEqual(new_pipe.bridge_id, 161)
        self.assertEqual(new_pipe.bridge_status, PipelineStatus.FAILED)
        self.assertEqual(new_pipe.ds_pipeline_id, 726)
        self.assertEqual(new_pipe.ds_project_id, 765)
        self.assertEqual(new_pipe.mr_pipeline_id, 653)
        self.assertEqual(new_pipe.label, defs.Label('CKI_RT::Warning'))
        self.assertEqual(new_pipe.failed_stage.name, 'kernel-results')
        self.assertIs(new_pipe.type, PipelineType.REALTIME)

    def test_init_from_dict_pipe4(self):
        """Object properties should be set."""
        input_dict = deepcopy(PIPE4_DICT)
        new_pipe = PipelineResult.from_dict(input_dict)

        # Attributes.
        self.assertIs(new_pipe.allow_failure, False)

        # Transformed attributes.
        self.assertIs(new_pipe.status, PipelineStatus.SUCCESS)
        self.assertEqual(new_pipe.created_at, datetime(2023, 4, 15, 1, 12, 12))

        # Derived properties.
        self.assertIs(new_pipe.status, PipelineStatus.SUCCESS)
        self.assertEqual(new_pipe.bridge_id, 243)
        self.assertEqual(new_pipe.bridge_status, PipelineStatus.SUCCESS)
        self.assertEqual(new_pipe.ds_pipeline_id, 622)
        self.assertEqual(new_pipe.ds_project_id, 836)
        self.assertEqual(new_pipe.mr_pipeline_id, 874)
        self.assertEqual(new_pipe.label, defs.Label('CKI_Rawhide::OK'))
        self.assertEqual(new_pipe.failed_stage, None)
        self.assertIs(new_pipe.type, PipelineType.RAWHIDE)

    def test_label_unknown_type_pipe4(self):
        """A pipeline of unknown type should not have a label."""
        input_dict = deepcopy(PIPE4_DICT)
        input_dict['name'] = 'unknown bridge job'
        new_pipe = PipelineResult.from_dict(input_dict)
        self.assertIs(new_pipe.type, PipelineType.INVALID)
        self.assertEqual(new_pipe.label, defs.Label(''))

    def test_prepare_pipelines(self):
        """Generates a list of PipelineResults from the input dict and excludes older results."""
        pipes = PipelineResult.prepare_pipelines([PIPE3_DICT, PIPE2_DICT, PIPE1_DICT])

        self.assertEqual(len(pipes), 2)
        self.assertEqual(pipes[0].ds_pipeline_id, 936)
        self.assertEqual(pipes[1].ds_pipeline_id, 345)

    def test_get_stage(self):
        """Returns the stage_data value matching the given key."""
        new_pipe = PipelineResult.from_dict(PIPE4_DICT)
        self.assertEqual(new_pipe.get_stage('build'),
                         {'jobs': [{'status': 'SUCCESS'}], 'name': 'build'})

    def test_boot_failures(self) -> None:
        """Returns a list of build boot CkiFailures."""
        test_pipeline = make_test_pipeline(1560372949)
        expected_boot_build_ids = {
            'redhat:1560372949-ppc64le-kernel',
            'redhat:1560372949-x86_64-kernel-debug'
        }
        self.assertEqual({test.id for test in test_pipeline.boot_failures}, expected_boot_build_ids)

    def test_build_failures(self) -> None:
        """Returns a list of build CkiFailures."""
        test_pipeline = make_test_pipeline(1555224113)

        expected_build_ids = {
            'redhat:1555224113-s390x-kernel-debug',
            'redhat:1555224113-ppc64le-kernel',
            'redhat:1555224113-aarch64-kernel',
            'redhat:1555224113-aarch64-kernel-debug',
            'redhat:1555224113-s390x-kernel',
            'redhat:1555224113-x86_64-kernel',
            'redhat:1555224113-ppc64le-kernel-debug',
            'redhat:1555224113-x86_64-kernel-debug'
        }
        self.assertEqual({test.id for test in test_pipeline.build_failures}, expected_build_ids)

    def test_test_failures(self) -> None:
        """Returns a list of test CkiFailures."""
        test_pipeline = make_test_pipeline(1559018829)

        expected_test_ids = {"redhat:1559018829-x86_64-kernel-debug_upt_10"}
        self.assertEqual({test.id for test in test_pipeline.test_failures}, expected_test_ids)

    def test_kcidb_data_404(self) -> None:
        """Handles a 404 response from the datawarehouse API."""
        test_pipeline = make_test_pipeline(1560372949)
        del test_pipeline.__dict__['kcidb_data']

        self.responses.get('http://localhost/api/1/kcidb/checkouts/redhat:1560372949', status=404)

        self.assertEqual(test_pipeline.kcidb_data, None)

    def test_kcidb_data_400(self) -> None:
        """Raises with any HTTPError other than 404 from the datawarehouse API."""
        test_pipeline = make_test_pipeline(1560372949)
        del test_pipeline.__dict__['kcidb_data']

        self.responses.get('http://localhost/api/1/kcidb/checkouts/redhat:1560372949', status=400)

        with self.assertRaises(HTTPError):
            assert test_pipeline.kcidb_data


def make_test_pipeline(checkout_id: int) -> PipelineResult:
    """Return a PipelineResult with the given checkout asset data loaded."""
    pipeline_data = \
        KwfTestCase.load_yaml_asset(f'{checkout_id}_query.json', sub_module='datawarehouse')
    test_pipeline = PipelineResult.from_dict(pipeline_data)

    # Patch over the kcidb_data.
    kcidb = f'tests/assets/datawarehouse/redhat:{checkout_id}.json'
    test_pipeline.__dict__['kcidb_data'] = CheckoutData(f'redhat:{checkout_id}', kcidb_file=kcidb)

    return test_pipeline


build1_id = 'redhat:1234567893-ppc64le-kernel'
build2_id = 'redhat:1234567893-x86_64-kernel'

# kcidb_all.json file data for a 'setup' pipeline job.
setup_job1 = {
    'checkouts': [{
        'id': 'checkout-id-1',
        'misc': {'kernel_version': '4.18.0-387.el8.mr123', 'all_sources_targeted': True},
    }],
    'builds': [
        {'architecture': 'ppc64le',
         'id': build1_id,
         'misc': {'debug': False},
         'valid': True,
         'output_files': [
             {'name': 'kernel_package_url',
              'url': 'https://package_url/ppc64le/kernel-1.2.3.ppc64le'},
             {'name': 'kernel_browse_url',
              'url': 'https://browse_url.com'}]
         }]
}

# kcidb_all.json data.
setup_job2 = {
    'checkouts': [{
        'id': 'checkout-id-2',
        'misc': {'kernel_version': '4.18.0-387.el8.mr123', 'all_sources_targeted': False},
    }],
    'builds': [
        {'id': build1_id},
        {'architecture': 'x86_64',
         'id': build2_id,
         'misc': {'debug': True},
         'valid': True,
         'output_files': [
             {'name': 'kernel_package_url',
              'url': 'https://package_url/x86_64-debug/kernel-1.2.3.x86_64'},
             {'name': 'kernel_browse_url',
              'url': 'https://browse_url.com'}]
         }]
}


class MockArtifact:
    def __init__(self, artifact_map: dict[str, typing.Any]) -> None:
        self.artifact_map = artifact_map

    def __call__(self, name: str) -> str:
        if result := self.artifact_map.get(name):
            return json.dumps(result)
        raise GitlabGetError()


class TestSetupJob(KwfTestCase):
    """Tests for the SetupJob dataclass."""

    def test_setup_job(self):
        """Presents the ProjectJob data."""
        tests = [
            # (kcidb_all.json, build_data, checkout)
            # "Normal" kcidb data.
            (setup_job1, setup_job1['builds'][0], setup_job1['checkouts'][0]),
            # No artifact file.
            (None, {}, {}),
            # Not ready?
            (setup_job2, setup_job2['builds'][1], setup_job2['checkouts'][0]),
        ]

        for kcidb_data, build_data, checkout in tests:
            with self.subTest():
                mock_job = mock.Mock(spec=['artifact', 'id', 'pipeline'])
                mock_job.id = 456
                mock_job.pipeline = {'id': 123}
                mock_job.artifact.side_effect = MockArtifact({
                    'kcidb_all.json': kcidb_data,
                    'envvars.json': {'KCIDB_BUILD_ID': build_data.get('id')},
                })

                setupjob = SetupJob(mock_job)
                self.assertEqual(setupjob.build_data, build_data)
                self.assertEqual(setupjob.checkout, checkout)
                self.assertIn('pid: 123, jid: 456', repr(setupjob))

                # If there is no data then don't expect much...
                if kcidb_data is None:
                    self.assertIn('no artifact data', repr(setupjob))
                    self.assertEqual(setupjob.kcidb_all, {})
                    self.assertEqual(setupjob.arch, '')
                    self.assertEqual(setupjob.browse_url, '')
                    self.assertEqual(setupjob.repo_url, '')
                    self.assertEqual(setupjob.version, '')
                    self.assertEqual(setupjob.nvr, '')
                    self.assertEqual(setupjob.debug, False)
                    continue

                self.assertIn(setupjob.nvr, repr(setupjob))
                self.assertEqual(setupjob.arch, build_data['architecture'])
                self.assertEqual(setupjob.browse_url, build_data['output_files'][1]['url'])
                self.assertEqual(setupjob.debug, build_data['misc']['debug'])
                repo_url = build_data['output_files'][0]['url']
                self.assertEqual(setupjob.repo_url, repo_url)
                self.assertEqual(setupjob.version, checkout['misc']['kernel_version'])
                expected_nvr = f'{setupjob.version}.{setupjob.arch}'
                if setupjob.debug:
                    expected_nvr += '-debug'
                self.assertEqual(setupjob.nvr, expected_nvr)


API_URL = 'https://gitlab.com/api/v4'


class TestBridgeJob(KwfTestCase):
    """Tests for the BridgeJob dataclass."""

    @mock.patch.dict('os.environ', {'DATAWAREHOUSE_URL': 'https://datawarehouse'})
    def run_test(
            self,
            bridge_attrs: dict,
            ds_proj_attrs: dict,
            ds_jobs: list,
            job_artifacts: dict[int, dict],
            builds_valid: bool
    ) -> None:
        """Runs a test."""
        proj_id = bridge_attrs["pipeline"]["project_id"]
        pipe_id = bridge_attrs["pipeline"]["id"]
        ds_pipe_attrs = bridge_attrs['downstream_pipeline']

        with responses.RequestsMock(assert_all_requests_are_fired=False) as rsps:
            # The initial bridges.list()
            rsps.get(
                f'{API_URL}/projects/{proj_id}/pipelines/{pipe_id}/bridges', json=[bridge_attrs]
            )
            # If the bridge job's downstream_pipeline dict is empty then the
            # BridgeJob isn't going to do much.
            if ds_pipe_attrs:
                # The downstream project.
                ds_proj_url = f'{API_URL}/projects/{ds_proj_attrs["id"]}'
                rsps.get(ds_proj_url, json=ds_proj_attrs)
                # The downstream pipeline.
                ds_pipe_url = f'{ds_proj_url}/pipelines/{ds_pipe_attrs["id"]}'
                rsps.get(ds_pipe_url, json=ds_pipe_attrs)
                if ds_jobs:
                    # The downstream pipeline jobs.list().
                    rsps.get(f'{ds_pipe_url}/jobs', json=ds_jobs)
                    # The downstream project jobs.get().
                    for ds_job in ds_jobs:
                        job_url = f'{ds_proj_url}/jobs/{ds_job["id"]}'
                        rsps.get(job_url, json=ds_job)
                        # Add artifacts if we have them.
                        if ds_job['id'] in job_artifacts:
                            for filename, content in job_artifacts[ds_job['id']].items():
                                rsps.get(f'{job_url}/artifacts/{filename}', json=content)

            # Get the bridges.list().
            gl_instance = get_instance('https://gitlab.com')
            gl_project = gl_instance.projects.get(proj_id, lazy=True)
            gl_pipeline = gl_project.pipelines.get(pipe_id, lazy=True)
            gl_bridges_list = gl_pipeline.bridges.list()

            # Create our BridgeJob.
            bridge_job = BridgeJob(gl_bridges_list[0])

            self.assertEqual(len(bridge_job.setup_jobs), len(ds_jobs))
            self.assertIn(bridge_attrs['name'], repr(bridge_job))
            self.assertEqual(bridge_job.builds_valid, builds_valid)

            if not ds_pipe_attrs:
                # Not much here if the bridge job's downstream_pipeline is empty.
                self.assertIsNone(bridge_job.ds_project)
                self.assertIsNone(bridge_job.ds_pipeline)
                self.assertEqual(bridge_job.checkout, {})
                return

            # It fetches the objects.
            self.assertIsInstance(bridge_job.ds_project, Project)
            self.assertIsInstance(bridge_job.ds_pipeline, ProjectPipeline)
            self.assertEqual(bridge_job.checkout, bridge_job.setup_jobs[0].checkout)

            art_text = bridge_job.artifacts_text
            self.assertIn('DataWarehouse Checkout: '
                          'https://datawarehouse/kcidb/checkouts/checkout-id-1', art_text)
            self.assertEqual(art_text.count('Artifacts (RPMs):'), len(bridge_job.setup_jobs))
            if ds_proj_attrs['visibility'] == 'public':
                self.assertEqual(art_text.count('Repo URL:'), len(bridge_job.setup_jobs))
            else:
                self.assertEqual(art_text.count('Repo URL:'), 1)

    def test_bridge_job1(self):
        """Loads and presents the expected data."""
        bridge_attrs = {
            'id': 24627,
            'name': 'c9s_merge_request',
            'pipeline': {'id': 111, 'project_id': 256},
            'downstream_pipeline': {
                'id': 435, 'project_id': 678, 'status': 'success',
                'web_url': 'https://gitlab.com/group/ds_project/-/pipelines/435'},
        }
        ds_proj_attrs = {
            'id': 678,
            'visibility': 'public'
        }
        job1 = {
            'id': 34637,
            'name': 'setup ppc64le',
            'stage': 'setup',
            'status': 'success'
        }
        job2 = {
            'id': 65373,
            'name': 'setup x86_64',
            'stage': 'setup',
            'status': 'success'
        }
        job_artifacts = {
            job1['id']: {
                'kcidb_all.json': setup_job1,
                'envvars.json': {'KCIDB_BUILD_ID': build1_id},
            },
            job2['id']: {
                'kcidb_all.json': setup_job2,
                'envvars.json': {'KCIDB_BUILD_ID': build1_id},
            },
        }
        builds_valid = True
        self.run_test(bridge_attrs, ds_proj_attrs, [job1], job_artifacts, builds_valid)

    def test_bridge_job2(self):
        """Loads and presents minimal data as there is no downstream_pipeline data."""
        bridge_attrs = {
            'id': 24627,
            'name': 'c9s_merge_request',
            'pipeline': {'id': 111, 'project_id': 256},
            'downstream_pipeline': {}
        }
        ds_proj_attrs = {}
        job_artifacts = {}
        builds_valid = False
        self.run_test(bridge_attrs, ds_proj_attrs, [], job_artifacts, builds_valid)

    def test_bridge_job3(self):
        """Loads and presents the expected data for a non-public job."""
        bridge_attrs = {
            'id': 24627,
            'name': 'c9s_merge_request',
            'pipeline': {'id': 111, 'project_id': 256},
            'downstream_pipeline': {
                'id': 435, 'project_id': 678, 'status': 'success',
                'web_url': 'https://gitlab.com/group/ds_project/-/pipelines/435'},
        }
        ds_proj_attrs = {
            'id': 678,
            'visibility': 'private'
        }
        job1 = {
            'id': 34637,
            'name': 'setup ppc64le',
            'stage': 'setup',
            'status': 'success'
        }
        job2 = {
            'id': 65373,
            'name': 'setup x86_64',
            'stage': 'setup',
            'status': 'success'
        }
        job_artifacts = {
            job1['id']: {
                'kcidb_all.json': setup_job1,
                'envvars.json': {'KCIDB_BUILD_ID': build1_id},
            },
            job2['id']: {
                'kcidb_all.json': setup_job2,
                'envvars.json': {'KCIDB_BUILD_ID': build1_id},
            },
        }
        builds_valid = True
        self.run_test(bridge_attrs, ds_proj_attrs, [job1], job_artifacts, builds_valid)
