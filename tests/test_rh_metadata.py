"""Webhook interaction tests."""
import os
import typing
from unittest import mock

from cki_lib import yaml

from tests.helpers import KwfTestCase
from webhook import defs
from webhook import rh_metadata
from webhook.pipelines import PipelineType
from webhook.temp_utils import with_yaml_validation


@mock.patch.dict(
    'os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml'}, clear=True
)
class TestProjects(KwfTestCase):
    """Tests for the Projects module."""

    @mock.patch('webhook.rh_metadata.get_policy_data')
    def test_Projects_policies_loading(self, mock_get_policy_data):
        """Calls load_policies method."""
        mock_get_policy_data.return_value = {'c9s': [1, 2, 3],
                                             'rhel-8.7.0': None,
                                             'rhel-8.6.0': None,
                                             'rhel-8.5.0': [4, 5, 6]
                                             }
        results = rh_metadata.Projects(load_policies=True)
        self.assertEqual(results.projects[C9S_PROJECT_ID].branches[0].policy, [1, 2, 3])
        self.assertEqual(results.projects[12345].branches[4].policy, [4, 5, 6])
        self.assertEqual(results.projects[12345].branches[5].policy, [4, 5, 6])
        for i in range(1, 4):
            self.assertEqual(results.projects[12345].branches[i].policy, [])
        for i in range(6, 10):
            self.assertEqual(results.projects[12345].branches[i].policy, [])

    @mock.patch.dict(
        'os.environ', {'RH_METADATA_EXTRA_PATHS': ''}, clear=True
    )
    @mock.patch('sys.exit')
    def test_main(self, mock_exit) -> None:
        """Tests the argparser and cli-based validation of projects metadata."""
        args = ['validate', 'tests/assets/rh_projects_private.yaml']
        rh_metadata.main(args)
        mock_exit.assert_not_called()
        args = ['validate', 'tests/assets/rh_projects_broken.yaml',
                'tests/assets/rh_projects_sandbox.yaml']
        rh_metadata.main(args)
        mock_exit.assert_called_with(1)


ARK_PROJECT_ID = 13604247
C9S_PROJECT_ID = 24152864
C10S_PROJECT_ID = 56169485

# Projects defined in utils/rh_metadata.yaml.
PUBLIC_PROJECT_IDS = [ARK_PROJECT_ID, C9S_PROJECT_ID, C10S_PROJECT_ID]

# Projects defined in tests/assets/rh_projects_private.yaml.
PRIVATE_ASSET_PROJECT_PROD_IDS = [C9S_PROJECT_ID, 12345]
PRIVATE_ASSET_PROJECT_SANDBOX_IDS = [56791, 56789, 56792, 56793]
PRIVATE_ASSET_PROJECT_IDS = PRIVATE_ASSET_PROJECT_PROD_IDS + PRIVATE_ASSET_PROJECT_SANDBOX_IDS
PRIVATE_ASSET_PATH = 'tests/assets/rh_projects_private.yaml'

# Webhooks defined in utils/rh_webhooks.yaml.
ALL_WEBHOOK_NAMES = [
    'ack_nack',
    'backporter',
    'buglinker',
    'ckihook',
    'commit_compare',
    'configshook',
    'limited_ci',
    'elnhook',
    'fixes',
    'jirahook',
    'skeleton',
    'mergehook',
    'owners_validator',
    'report_generator',
    'sast',
    'signoff',
    'subsystems',
    'sprinter',
    'terminator',
    'umb_bridge'
]

# Should match the utils/rh_webhooks.yaml webhooks that have 'enabled_by_default: true' set
DEFAULT_KWF_WEBHOOKS = [
    'ack_nack',
    'buglinker',
    'ckihook',
    'commit_compare',
    'configshook',
    'fixes',
    'jirahook',
    'skeleton',
    'mergehook',
    'signoff',
    'subsystems',
    'umb_bridge'
]

# These should match what their project definitions in utils/rh_metadata.yaml.
CENTOS_WEBHOOKS = DEFAULT_KWF_WEBHOOKS + ['limited_ci']
ARK_WEBHOOKS = [
    'ack_nack',
    'ckihook',
    'configshook',
    'elnhook',
    'limited_ci',
    'mergehook',
    'signoff',
    'subsystems'
]

# Mapping of webhooks expected per project
PER_PROJECT_WEBHOOKS = {
    ARK_PROJECT_ID: ARK_WEBHOOKS,
    C9S_PROJECT_ID: CENTOS_WEBHOOKS,
    C10S_PROJECT_ID: CENTOS_WEBHOOKS,
}


class ProjectsTest(typing.NamedTuple):
    """Test settings for the TestProjectsLoading test case."""
    # Environment to set.
    env: typing.Dict
    # File paths we expect to see the yaml loader try.
    loads_yaml_paths: typing.Iterable[str]
    # Expected project IDs to be present in the Projects instance.
    expected_project_ids: typing.Iterable[int] = []
    # Expected webhooks to be present in the Projects instance.
    expected_webhook_names: typing.Iterable[str] = []
    # Expected webhooks per project ID. If None will check PER_PROJECT_WEBHOOKS.
    expected_per_project_webhooks: typing.Optional[typing.Dict[int, str]] = None
    # Whether calling Projects() is expected to raise an exception.
    raises: typing.Union[typing.Tuple[Exception, str], None] = None
    # Parameters to pass to Projects().
    load_policies: bool = False
    projects_yaml_path: typing.Optional[str] = None
    webhooks_yaml_path: typing.Optional[str] = None
    extra_projects_paths: typing.Optional[typing.Iterable[str]] = None


class TestProjectsLoading(KwfTestCase):
    """Tests for the Projects module with two yamls."""

    def setUp(self) -> None:
        """Wrap the yaml loader so we can see what it called and patch out the policy loader."""
        super().setUp()
        wrapped_yaml_loader = mock.patch('cki_lib.yaml.load', wraps=yaml.load)
        self.addCleanup(wrapped_yaml_loader.stop)
        self.wrapped_yaml_loader = wrapped_yaml_loader.start()

        mock_do_load_policies = mock.patch.object(rh_metadata.Projects, 'do_load_policies')
        self.addCleanup(mock_do_load_policies.stop)
        self.mock_do_load_policies = mock_do_load_policies.start()

        wrapped_yaml_validate = mock.patch('cki_lib.yaml.validate', wraps=yaml.validate)
        self.addClassCleanup(wrapped_yaml_validate.stop)
        self.wrapped_yaml_validate = wrapped_yaml_validate.start()

    def _run_ProjectsTest(self, test: ProjectsTest) -> None:
        """Runs one ProjectsTest."""
        # Possible parameters when constructing a new rh_metadata.Projects instance.
        projects_params = [
            'load_policies',
            'projects_yaml_path',
            'webhooks_yaml_path',
            'extra_projects_paths',
        ]

        with mock.patch.dict('os.environ', test.env, clear=True):
            # Confirm the environment has the expected values.
            self.assertCountEqual(os.environ, test.env)

            # Get the params (if any) to call Projects() with from ProjectsTest.
            params = {p: getattr(test, p) for p in projects_params if getattr(test, p) is not None}

            # Handle ProjectsTest.raises.
            if test.raises:
                with self.assertRaisesRegex(test.raises[0], test.raises[1]):
                    rh_metadata.Projects(**params)
                return

            # Make the Projects() object.
            rh_projects = rh_metadata.Projects(**params)

            # Confirm data was loaded from the expected files.
            file_paths = [call[1]['file_path'] for call in self.wrapped_yaml_loader.call_args_list]
            self.assertCountEqual(test.loads_yaml_paths, file_paths)

            # Confirm the loaded data matches the ProjectsTest expectations.
            self.assertCountEqual(test.expected_project_ids, rh_projects.projects)
            self.assertCountEqual(test.expected_webhook_names, rh_projects.webhooks)

            # Confirm each project has the expected webhooks.
            expected_project_hooks = test.expected_per_project_webhooks or PER_PROJECT_WEBHOOKS
            self.assertGreater(len(expected_project_hooks), 0)
            for project_id, webhooks_list in expected_project_hooks.items():
                if project_id in rh_projects.projects:
                    project_hooks = list(rh_projects.projects[project_id].webhooks)
                    with self.subTest(
                        project_id=project_id,
                        project_hooks=project_hooks,
                        expected_hooks=webhooks_list
                    ):
                        self.assertCountEqual(project_hooks, webhooks_list)

            # Confirm load_policies was recognized.
            self.assertEqual(test.load_policies, bool(self.mock_do_load_policies.call_count))

            # Confirm we validated something against each schema.
            self.assertEqual(self.wrapped_yaml_validate.call_count, 2)
            self.assertEqual(
                self.wrapped_yaml_validate.call_args_list[0][0][1], rh_metadata.PROJECTS_SCHEMA
            )
            self.assertEqual(
                self.wrapped_yaml_validate.call_args_list[1][0][1], rh_metadata.WEBHOOKS_SCHEMA
            )

    @with_yaml_validation
    def test_loading_with_no_params(self) -> None:
        """Loads data from default utils/*.yaml files."""
        this_test = ProjectsTest(
            env={},
            loads_yaml_paths=[defs.DEFAULT_RH_METADATA_PATH, defs.DEFAULT_RH_WEBHOOKS_PATH],
            expected_project_ids=PUBLIC_PROJECT_IDS,
            expected_webhook_names=ALL_WEBHOOK_NAMES,
        )
        self._run_ProjectsTest(this_test)

    @with_yaml_validation
    def test_loading_with_extra_projects_paths(self) -> None:
        """Loads data from default utils/*.yaml files and some given extras."""
        this_test = ProjectsTest(
            env={},
            loads_yaml_paths=[defs.DEFAULT_RH_METADATA_PATH,
                              defs.DEFAULT_RH_WEBHOOKS_PATH,
                              PRIVATE_ASSET_PATH],
            extra_projects_paths=[PRIVATE_ASSET_PATH],
            expected_project_ids=set(PUBLIC_PROJECT_IDS) | set(PRIVATE_ASSET_PROJECT_PROD_IDS),
            expected_webhook_names=ALL_WEBHOOK_NAMES,
        )
        self._run_ProjectsTest(this_test)

    def test_loading_with_extra_projects_paths_exception(self) -> None:
        """Raises an exception when the extra_project path doesn't have a 'projects' member."""
        # This inputs 'utils/labels.yaml' which has no 'projects' member.
        this_test = ProjectsTest(
            env={'RH_METADATA_EXTRA_PATHS': 'utils/labels.yaml'},
            loads_yaml_paths=[defs.DEFAULT_RH_METADATA_PATH,
                              defs.DEFAULT_RH_WEBHOOKS_PATH,
                              'utils/labels.yaml'],
            raises=(RuntimeError, "No 'projects' found in extra yaml.")
        )
        self._run_ProjectsTest(this_test)

    @with_yaml_validation
    def test_loading_with_load_policies(self) -> None:
        """Loads data from default utils/*.yaml files and calls do_load_policies."""
        this_test = ProjectsTest(
            env={},
            loads_yaml_paths=[defs.DEFAULT_RH_METADATA_PATH, defs.DEFAULT_RH_WEBHOOKS_PATH],
            expected_project_ids=PUBLIC_PROJECT_IDS,
            expected_webhook_names=ALL_WEBHOOK_NAMES,
            load_policies=True
        )
        self._run_ProjectsTest(this_test)

    def test_loading_raises_exception(self) -> None:
        """Raises an exception when given a file path it can't get data from."""
        test_env = {
            'RH_METADATA_YAML_PATH': 'bad/path',
        }
        this_test = ProjectsTest(
            env=test_env,
            loads_yaml_paths=['bad/path'],
            raises=(RuntimeError, "No 'projects' found in yaml.")
        )
        self._run_ProjectsTest(this_test)

    def test_loading_with_no_webhooks_raises_exception(self) -> None:
        """Raises an exception when there is no webhooks data."""
        test_env = {
            'RH_METADATA_YAML_PATH': defs.DEFAULT_RH_METADATA_PATH,
        }
        this_test = ProjectsTest(
            env=test_env,
            loads_yaml_paths=[defs.DEFAULT_RH_METADATA_PATH],
            expected_project_ids=PUBLIC_PROJECT_IDS,
            expected_webhook_names=ALL_WEBHOOK_NAMES,
            raises=(RuntimeError, "No 'webhooks' found in yaml.")
        )
        # Patch out DEFAULT_RH_WEBHOOKS_PATH or utils/rh_webhooks.yaml will be loaded automatically.
        with mock.patch('webhook.defs.DEFAULT_RH_WEBHOOKS_PATH', ''):
            self._run_ProjectsTest(this_test)


@mock.patch.dict(
    'os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml'}, clear=True
)
class TestProject(KwfTestCase):
    """Tests for the Project module."""

    @mock.patch('webhook.rh_metadata.check_data', wraps=rh_metadata.check_data)
    def test_Project(self, mock_check_data):
        """Test Project objects."""
        results = rh_metadata.Projects()
        rh_metadata.check_data.assert_called()

        project = results.projects[12345]

        self.assertEqual(project.get_branch_by_name('1'), None)
        self.assertEqual(project.get_branch_by_name('main').distgit_ref, 'rhel-8.7.0')
        self.assertIs(project.get_branch_by_name('main'), project.branches[0])

        self.assertEqual(project.get_branches_by_itr('1.0'), [])
        self.assertEqual(project.get_branches_by_itr('8.7.0')[0].components, {'kernel'})
        self.assertEqual(project.get_branches_by_itr('8.7.0')[0].sub_component, '')
        self.assertEqual(project.get_branches_by_itr('8.7.0')[1].components, {'kernel-rt'})
        self.assertEqual(project.get_branches_by_itr('8.7.0')[1].sub_component, '')
        self.assertEqual(project.get_branches_by_itr('8.7.0')[2].components, {'kernel'})
        self.assertEqual(project.get_branches_by_itr('8.7.0')[2].sub_component, 'automotive')

        self.assertEqual(project.get_branches_by_ztr('10.1.0'), [])
        self.assertEqual(project.get_branches_by_ztr('8.5.0')[0].components, {'kernel'})
        self.assertEqual(project.get_branches_by_ztr('8.5.0')[0].sub_component, '')
        self.assertEqual(len(project.webhooks), 12)

        # branch is inactive so an empty list
        self.assertEqual(project.get_branches_by_ztr('8.1.0'), [])

        # Confirm Branches have a pointer back to the Project
        self.assertIs(project.branches[0].project, project)
        self.assertIs(project.branches[1].project, project)
        self.assertIs(project.branches[2].project, project)
        self.assertIs(project.branches[3].project, project)

    @mock.patch.dict(
        'os.environ', {'RH_METADATA_EXTRA_PATHS':
                       'tests/assets/rh_projects_broken.yaml'}, clear=True
    )
    def test_Project_bad_minor(self):
        """Test that we fail to load projects due to a branch with a bad minor version."""
        with self.assertRaises(ValueError):
            _ = rh_metadata.Projects()

    @mock.patch('cki_lib.misc.is_staging', mock.Mock(return_value=True))
    def test_webhooks_status(self):
        """Correctly returns the MrScope indicated by the labels."""
        rh_projects = rh_metadata.Projects()
        project = rh_projects.get_project_by_id(56789)
        all_ok_labels = [f'{prefix}::OK' for hook in project.webhooks.values() for
                         prefix in hook.required_label_prefixes]

        test_list = []
        # No labels is always NEEDS_REVIEW
        test_list.append((defs.MrScope.NEEDS_REVIEW, []))
        # All OK labels, scope is OK
        test_list.append((defs.MrScope.OK, all_ok_labels))
        # Bugzilla is NeedsTesting, overall is NEEDS_TESTING aka READY_FOR_QA
        qa_labels = [label for label in all_ok_labels if not label.startswith('JIRA::')]
        qa_labels.append('JIRA::InProgress')
        test_list.append((defs.MrScope.NEEDS_TESTING, qa_labels))
        test_list.append((defs.MrScope.READY_FOR_QA, qa_labels))

        for scope, labels in test_list:
            with self.subTest(expected_scope=scope, labels=labels):
                self.assertIs(project.webhooks_status(labels), scope)


@mock.patch.dict(
    'os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml'}, clear=True
)
class TestBranch(KwfTestCase):
    """Tests for the Branch module."""

    @mock.patch('webhook.rh_metadata.check_data', wraps=rh_metadata.check_data)
    def test_Branch(self, mock_check_data):
        """Test Branch objects."""
        webhooks = {'jirahook': True}
        a_project = rh_metadata.Project(id=123, group_id=987, name='project', product='rhel',
                                        branches=[], pipelines=['rhel'], webhooks=webhooks)
        branch_input = {'name': 'test_branch',
                        'components': ['kernel-rt'],
                        'distgit_ref': 'rhel-2.5.5',
                        'internal_target_release': '2.5.5',
                        'pipelines': ['rhel', 'realtime'],
                        'project': a_project,
                        'fix_versions': ['rhel-2.5.0']
                        }
        branch = rh_metadata.Branch(**branch_input)
        rh_metadata.check_data.assert_called()

        self.assertEqual(branch.name, 'test_branch')
        self.assertEqual(branch.components, {'kernel-rt'})
        self.assertEqual(branch.distgit_ref, 'rhel-2.5.5')
        self.assertEqual(branch.internal_target_release, '2.5.5')
        self.assertEqual(branch.zstream_target_release, '')
        self.assertEqual(branch.pipelines, [PipelineType.RHEL, PipelineType.REALTIME])
        self.assertEqual(branch.major, 2)
        self.assertEqual(branch.minor, 5)
        self.assertEqual(branch.version, '2.5')
        self.assertIs(branch.inactive, False)

    def test_branch_comparisons(self):
        """Returns the expected value for Branch comparisons."""
        results = rh_metadata.Projects()
        project1 = results.projects[C9S_PROJECT_ID]
        project2 = results.projects[12345]

        # Same Branch, Same Project = Match.
        self.assertTrue(project1.branches[0] == project1.branches[0])
        self.assertFalse(project2.branches[0] != project2.branches[0])
        self.assertFalse(project2.branches[0] == project2.branches[1])

        # Branches from different Projects don't match.
        self.assertFalse(project1.branches[0] == project2.branches[1])

        # Higher version == greater than.
        self.assertTrue(project2.branches[0] > project2.branches[2] > project2.branches[4])
        self.assertTrue(project2.branches[1] > project2.branches[3] > project2.branches[5])

        # Lower version == less than.
        self.assertTrue(project2.branches[4] < project2.branches[2] < project2.branches[0])
        self.assertTrue(project2.branches[5] < project2.branches[3] < project2.branches[1])

        # Alpha < Beta < *
        self.assertTrue(project2.branches[-1] < project2.branches[-2] < project2.branches[-3])

        # Lead stream
        self.assertFalse(project2.branches[0].lead_stream)
        self.assertTrue(project2.branches[8].lead_stream)


@mock.patch.dict(
    'os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml'}, clear=True
)
class TestHelpers(KwfTestCase):
    """Tests for the helper methods."""

    def test_check_data(self):
        """Make sure the expected errors are raised."""
        webhooks = {'jirahook': True}
        # Wrong field type
        project = rh_metadata.Project(id=123, group_id=987, name='project', product='rhel',
                                      webhooks=webhooks)
        branch = rh_metadata.Branch(name='test_branch', components=['kernel-rt'],
                                    distgit_ref='rhel-2.5.5', project=project)
        branch.__dict__['distgit_ref'] = 5
        err_raised = False
        try:
            rh_metadata.check_data(branch)
        except TypeError:
            err_raised = True
        self.assertTrue(err_raised)

        # Empty string for value with no default
        branch = rh_metadata.Branch(name='test_branch', components=['kernel-rt'],
                                    distgit_ref='rhel-2.5.5', project=project)
        branch.__dict__['components'] = set()
        with self.assertRaises(ValueError):
            rh_metadata.check_data(branch)

        # happy branch, empty default values don't raise a ValueError
        branch_input = {'name': 'test_branch',
                        'components': ['kernel-rt'],
                        'distgit_ref': 'rhel-2.5.5',
                        'internal_target_release': '2.5.5',
                        'project': project
                        }
        branch = rh_metadata.Branch(**branch_input)
        self.assertEqual(branch.internal_target_release, '2.5.5')
        self.assertEqual(branch.zstream_target_release, '')


class TestGetMatchingBranch(KwfTestCase):
    """Tests for the webhook.rh_metadata.Projects.get_matching_branch() method."""

    def test_no_fix_version_or_component(self) -> None:
        """Returns None when not given both a fix_version and component."""
        extra_projects_paths = 'tests/assets/rh_projects_private.yaml'

        with mock.patch.dict(os.environ, {}, clear=True):
            rh_projects = rh_metadata.Projects(extra_projects_paths=[extra_projects_paths])

        self.assertIsNone(rh_projects.get_matching_branch(None, '', confidential=True))

        self.assertIsNone(rh_projects.get_matching_branch(None, 'kernel', confidential=True))

        self.assertIsNone(rh_projects.get_matching_branch(
            defs.FixVersion('rhel-8.10.0'), '', confidential=False
        ))

    def test_get_matching_branch(self) -> None:
        """Returns matching Branch object for the given fix version, component, & confidential."""
        extra_projects_paths = 'tests/assets/rh_projects_private.yaml'

        # Returns the non-confidential, non-sandbox Branch for 8.7.0 (main).
        env_name = 'production'
        with mock.patch.dict(os.environ, {'CKI_DEPLOYMENT_ENVIRONMENT': env_name}, clear=True):
            rh_projects = rh_metadata.Projects(extra_projects_paths=[extra_projects_paths])

        params = {
            'fix_version': defs.FixVersion('rhel-8.7.0'),
            'component': 'kernel',
            'confidential': False
        }
        expected_branch = rh_projects.projects[12345].branches[0]
        self.assertFalse(expected_branch.project.confidential)
        self.assertFalse(expected_branch.project.sandbox)
        self.assertIs(rh_projects.get_matching_branch(**params), expected_branch)

        # Returns the confidential & sandbox Branch for 8.7.0 (main).
        env_name = 'staging'
        with mock.patch.dict(os.environ, {'CKI_DEPLOYMENT_ENVIRONMENT': env_name}, clear=True):
            rh_projects = rh_metadata.Projects(extra_projects_paths=[extra_projects_paths])

        params = {
            'fix_version': defs.FixVersion('rhel-8.7.0'),
            'component': 'kernel',
            'confidential': True,
        }
        expected_branch = rh_projects.projects[56791].branches[0]
        self.assertTrue(expected_branch.project.confidential)
        self.assertTrue(expected_branch.project.sandbox)
        self.assertIs(rh_projects.get_matching_branch(**params), expected_branch)

        # Returns the non-confidential, sandbox Branch for 8.7.0 (main).
        params = {
            'fix_version': defs.FixVersion('rhel-8.7.0'),
            'component': 'kernel',
            'confidential': False,
        }
        expected_branch = rh_projects.projects[56789].branches[0]
        self.assertFalse(expected_branch.project.confidential)
        self.assertTrue(expected_branch.project.sandbox)
        self.assertIs(rh_projects.get_matching_branch(**params), expected_branch)

        # Returns nothing because there is no confidential, non-sandbox project in the asset yaml.
        env_name = 'production'
        with mock.patch.dict(os.environ, {'CKI_DEPLOYMENT_ENVIRONMENT': env_name}, clear=True):
            rh_projects = rh_metadata.Projects(extra_projects_paths=[extra_projects_paths])

        params = {
            'fix_version': defs.FixVersion('rhel-8.7.0'),
            'component': 'kernel',
            'confidential': True,
        }
        self.assertIsNone(rh_projects.get_matching_branch(**params))
