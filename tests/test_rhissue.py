"""Tests for the rhissue module."""
from unittest import mock

from tests import fakes_jira
from tests.helpers import KwfTestCase
from tests.test_jirahook import JiraMRBuilder
from webhook import defs
from webhook import rhissue
from webhook import rhissue_tests
from webhook.base_mr_mixins import DependsMixin
from webhook.graphql import GET_MR_DESCRIPTIONS_QUERY


@mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml',
                                'CKI_DEPLOYMENT_ENVIRONMENT': 'staging'})
class TestRHIssue(JiraMRBuilder, KwfTestCase):
    """Tests for the RHIssue class."""

    # expected Issue values when no JIRA or MRs are set.
    empty_mr_equal = {'_mrs': [],
                      'alias': 'RHIssue #0',
                      'failed_tests': [],
                      '_id': 0,
                      'id': 'BOGUS',       # This should be set by the tester,
                      'ji_cves': [],
                      'ji_depends_on': [],
                      'ji_fix_version': None,
                      'labels': [],
                      'policy_check_ok': (None, 'Check not done: No JIRA Issue'),
                      'commits': [],
                      'parent_mr_commits': []}

    empty_mr_is = {'_ji': None,
                   'scope': defs.MrScope.INVALID,
                   'internal': False,
                   'untagged': False,
                   'ji': None,
                   'mr': None,
                   'parent_mr': None,
                   'ji_branch': None,
                   'ji_pt_status': defs.JIPTStatus.UNKNOWN,
                   'ji_project': None,
                   'ji_resolution': None,
                   'ji_status': defs.JIStatus.UNKNOWN,
                   'cve_ids': None,
                   'is_cve_tracker': False,
                   'is_dependency': False,
                   'is_linked_issue': False,
                   'is_merged': False,
                   'in_mr_description': False,
                   'test_list': ['BOGUS']}  # This should be set by the tester.

    def validate_issue(self, rhissue, assert_equal, assert_is):
        """Helper to validate a JIRA Issue object."""
        print(f'Testing RHIssue {rhissue}...')
        for attribute, value in assert_equal.items():
            print(f'{attribute} should equal: {value}')
            self.assertCountEqual(getattr(rhissue, attribute), value) if \
                isinstance(value, list) else self.assertEqual(getattr(rhissue, attribute), value)
        for attribute, value in assert_is.items():
            print(f'{attribute} should be: {value}')
            self.assertIs(getattr(rhissue, attribute), value)

    def test_rhissue_init_empty(self):
        """Creates a RHIssue with no JIRA object set or MRs."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        # Expected values for an empty RHIssue.
        assert_equal['id'] = 0
        assert_equal['_id'] = 0
        assert_is.pop('_id', None)
        assert_is['test_list'] = rhissue_tests.ISSUE_TESTS

        test_rhissue = rhissue.RHIssue()
        self.validate_issue(test_rhissue, assert_equal, assert_is)

    def test_rhissue_new_internal_no_mrs(self):
        """Creates a RHIssue representing descriptions marked INTERNAL."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        # Unique values of an Internal JIRA Issue
        assert_equal['alias'] = 'RHIssue #INTERNAL'
        assert_equal['id'] = rhissue.INTERNAL_JISSUE
        assert_is['internal'] = True
        assert_is['test_list'] = rhissue_tests.INTERNAL_TESTS

        test_rhissue = rhissue.RHIssue.new_internal(mrs=[])
        self.validate_issue(test_rhissue, assert_equal, assert_is)

    def test_rhissue_new_missing_no_mrs(self):
        """Creates a RHIssue representing a MISSING JI."""
        issue_id = f'{defs.JPFX}1234567'
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        # Expected values for this Missing RHIssue
        assert_equal['alias'] = f'RHIssue #{defs.JPFX}1234567'
        assert_equal['id'] = issue_id
        assert_equal['_id'] = issue_id
        assert_is.pop('_id', None)
        assert_is['test_list'] = rhissue_tests.ISSUE_TESTS

        test_rhissue = rhissue.RHIssue.new_missing(issue_id, mrs=[])
        self.validate_issue(test_rhissue, assert_equal, assert_is)

    def test_rhissue_new_missing_cve_no_mrs(self):
        """Creates a RHIssue representing a MISSING CVE tracker JI."""
        issue_id = 'CVE-1998-12345'
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        # Expected values for this Missing CVE RHIssue
        assert_equal['alias'] = 'CVE-1998-12345 (JIRA Issue missing)'
        assert_equal['policy_check_ok'] = (None, 'Check not done: CVE tracker issue')
        assert_equal['cve_ids'] = [issue_id]
        assert_equal['id'] = issue_id
        assert_equal['_id'] = issue_id
        assert_is.pop('_id', None)
        assert_is.pop('cve_ids', None)
        assert_is['is_cve_tracker'] = True
        assert_is['test_list'] = rhissue_tests.CVE_TESTS

        test_rhissue = rhissue.RHIssue.new_missing(issue_id, mrs=[])
        self.validate_issue(test_rhissue, assert_equal, assert_is)

    def test_rhissue_new_untagged_no_mrs(self):
        """Creates a RHIssue representing descriptions with no tags (UNTAGGED)."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        # Unique values of an Untagged RHIssue
        assert_equal['alias'] = 'RHIssue #UNTAGGED'
        assert_equal['id'] = rhissue.UNTAGGED_JISSUE
        assert_is['untagged'] = True
        assert_is['test_list'] = rhissue_tests.UNTAGGED_TESTS

        test_rhissue = rhissue.RHIssue.new_untagged(mrs=[])
        self.validate_issue(test_rhissue, assert_equal, assert_is)

    def test_rhissue_new_from_ji_no_mrs_ji1234567(self):
        """Creates a RHIssue linked to the given JIRA Issue."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = f'RHIssue #{defs.JPFX}1234567'
        assert_equal['id'] = f'{defs.JPFX}1234567'
        assert_equal['ji_cves'] = ['CVE-1235-13516']
        assert_equal['labels'] = ['CVE-1235-13516', 'security']
        assert_equal['ji_fix_version'] = 'rhel-9.1.0'
        assert_equal['policy_check_ok'] = (None, 'Check not done: No MR or Branch data')
        assert_is['ji'] = fakes_jira.JI1234567
        assert_is['_ji'] = fakes_jira.JI1234567
        assert_is['ji_status'] = defs.JIStatus.IN_PROGRESS
        assert_is['test_list'] = rhissue_tests.ISSUE_TESTS

        test_rhissue = rhissue.RHIssue.new_from_ji(ji=fakes_jira.JI1234567, mrs=[])
        print(f'ji fields labels is {test_rhissue.ji.fields.labels}')
        self.validate_issue(test_rhissue, assert_equal, assert_is)

    def test_rhissue_new_from_ji_no_mrs_ji2323232(self):
        """Creates a RHIssue linked to the given JIRA Issue."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = f'RHIssue #{defs.JPFX}2323232'
        assert_equal['id'] = f'{defs.JPFX}2323232'
        assert_equal['ji_fix_version'] = 'rhel-9.1.0'
        assert_equal['policy_check_ok'] = (None, 'Check not done: No MR or Branch data')
        assert_is['ji'] = fakes_jira.JI2323232
        assert_is['_ji'] = fakes_jira.JI2323232
        assert_is['ji_status'] = defs.JIStatus.READY_FOR_QA
        assert_is['test_list'] = rhissue_tests.ISSUE_TESTS
        assert_is['ji_pt_status'] = defs.JIPTStatus.PASS

        test_rhissue = rhissue.RHIssue.new_from_ji(ji=fakes_jira.JI2323232, mrs=[])
        self.validate_issue(test_rhissue, assert_equal, assert_is)

    def test_rhissue_new_from_ji_no_mrs_ji7777777(self):
        """Creates a RHIssue linked to the given JIRA issue."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = f'RHIssue #{defs.JPFX}7777777'
        assert_equal['id'] = f'{defs.JPFX}7777777'
        assert_equal['ji_fix_version'] = 'rhel-9.1'
        assert_equal['policy_check_ok'] = (None, 'Check not done: No MR or Branch data')
        assert_is['ji'] = fakes_jira.JI7777777
        assert_is['_ji'] = fakes_jira.JI7777777
        assert_is['ji_status'] = defs.JIStatus.IN_PROGRESS
        assert_is['test_list'] = rhissue_tests.ISSUE_TESTS

        test_rhissue = rhissue.RHIssue.new_from_ji(ji=fakes_jira.JI7777777, mrs=[])
        self.validate_issue(test_rhissue, assert_equal, assert_is)

    def test_rhissue_new_from_ji_no_mrs_ji2345678(self):
        """Creates a RHIssue linked to the given JIRA Issue."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = f'RHIssue #{defs.JPFX}2345678'
        assert_equal['id'] = f'{defs.JPFX}2345678'
        assert_equal['ji_cves'] = ['CVE-2022-43210', 'CVE-1235-13516']
        assert_equal['labels'] = ['boop', 'CVE-2022-43210', 'CVE-1235-13516']
        assert_equal['ji_fix_version'] = 'rhel-9.1.0'
        assert_equal['ji_commit_hashes'] = ['abcd1234abcd']
        assert_equal['policy_check_ok'] = (None, 'Check not done: No MR or Branch data')
        assert_is['ji'] = fakes_jira.JI2345678
        assert_is['_ji'] = fakes_jira.JI2345678
        assert_is['ji_status'] = defs.JIStatus.READY_FOR_QA
        assert_is['test_list'] = rhissue_tests.ISSUE_TESTS
        assert_is['ji_pt_status'] = defs.JIPTStatus.PASS
        assert_is['severity'] = defs.IssueSeverity.CRITICAL

        test_rhissue = rhissue.RHIssue.new_from_ji(ji=fakes_jira.JI2345678, mrs=[])
        self.validate_issue(test_rhissue, assert_equal, assert_is)

    def test_rhissue_new_from_ji_no_mrs_ji3456789(self):
        """Creates a RHIssue linked to the given JIRA Issue."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = f'CVE-1235-13516 (RHIssue #{defs.JPFX}3456789)'
        assert_equal['id'] = f'{defs.JPFX}3456789'
        assert_equal['ji_cves'] = ['CVE-1235-13516']
        assert_equal['policy_check_ok'] = (None, 'Check not done: CVE tracker issue')
        assert_equal['cve_ids'] = ['CVE-1235-13516']
        assert_equal['labels'] = ['help', 'CVE-1235-13516']
        assert_is.pop('cve_ids', None)
        assert_is['ji'] = fakes_jira.JI3456789
        assert_is['_ji'] = fakes_jira.JI3456789
        assert_is['ji_status'] = defs.JIStatus.NEW
        assert_is['is_cve_tracker'] = True
        assert_is['test_list'] = rhissue_tests.CVE_TESTS
        assert_is['ji_pt_status'] = defs.JIPTStatus.UNSET
        assert_is['severity'] = defs.IssueSeverity.IMPORTANT

        test_rhissue = rhissue.RHIssue.new_from_ji(ji=fakes_jira.JI3456789, mrs=[])
        self.validate_issue(test_rhissue, assert_equal, assert_is)

    def test_rhissue_new_from_ji_no_mrs_ji4567890(self):
        """Creates a RHIssue linked to the given JIRA Issue."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = f'CVE-2022-7549, CVE-2022-7550 (RHIssue #{defs.JPFX}4567890)'
        assert_equal['id'] = f'{defs.JPFX}4567890'
        assert_equal['ji_cves'] = ['CVE-2022-7549', 'CVE-2022-7550']
        assert_equal['policy_check_ok'] = (None, 'Check not done: CVE tracker issue')
        assert_equal['cve_ids'] = ['CVE-2022-7549', 'CVE-2022-7550']
        assert_equal['labels'] = ['CVE-2022-7549', 'CVE-2022-7550', 'bing', 'bong']
        assert_is.pop('cve_ids', None)
        assert_is['ji'] = fakes_jira.JI4567890
        assert_is['_ji'] = fakes_jira.JI4567890
        assert_is['ji_resolution'] = defs.JIResolution.DONEERRATA
        assert_is['ji_status'] = defs.JIStatus.CLOSED
        assert_is['is_cve_tracker'] = True
        assert_is['test_list'] = rhissue_tests.CVE_TESTS
        assert_is['ji_pt_status'] = defs.JIPTStatus.UNSET

        test_rhissue = rhissue.RHIssue.new_from_ji(ji=fakes_jira.JI4567890, mrs=[])
        self.validate_issue(test_rhissue, assert_equal, assert_is)

    def test_equality(self):
        """Returns True if the IDs match, otherwise False."""
        rhissue1 = rhissue.RHIssue.new_from_ji(ji=fakes_jira.JI3456789, mrs=[])
        rhissue2 = rhissue.RHIssue.new_from_ji(ji=fakes_jira.JI4567890, mrs=[])
        rhissue3 = rhissue.RHIssue.new_missing(ji_id=f'{defs.JPFX}3456789', mrs=[])

        self.assertNotEqual(rhissue1, rhissue2)
        self.assertEqual(rhissue1, rhissue3)

        # Returns False if the other is not a RHIssue instance.
        self.assertFalse(rhissue1 == 'hello')

    def test_id_setter(self):
        """Sets the id property if self.ji is not set and not internal/untagged."""
        # Setting id property raises ValueError if the ji property is set.
        test_rhissue = rhissue.RHIssue.new_from_ji(ji=fakes_jira.JI2323232, mrs=[])
        with self.assertRaises(ValueError):
            test_rhissue.id = f'{defs.JPFX}1234567'

        # Setting the id property of a RHIssue marked internal or untagged raises a ValueError
        test_rhissue = rhissue.RHIssue.new_internal(mrs=[])
        with self.assertRaises(ValueError):
            test_rhissue.id = f'{defs.JPFX}7654321'
        test_rhissue = rhissue.RHIssue.new_untagged(mrs=[])
        with self.assertRaises(ValueError):
            test_rhissue.id = f'{defs.JPFX}7654321'

        # Setting the id property works for a RHIssue with no ji.
        test_rhissue = rhissue.RHIssue.new_missing(f'{defs.JPFX}1234567', mrs=[])
        self.assertEqual(test_rhissue.id, f'{defs.JPFX}1234567')
        test_rhissue.id = f'{defs.JPFX}7654321'
        self.assertEqual(test_rhissue.id, f'{defs.JPFX}7654321')

    @mock.patch.object(DependsMixin, '_set_mr_blocks', mock.Mock())
    @mock.patch.dict('os.environ', {'OWNERS_YAML': 'tests/assets/owners-snapshot.yaml',
                                    'CKI_DEPLOYMENT_ENVIRONMENT': 'development'})
    @mock.patch('webhook.session.connect_jira')
    @mock.patch('webhook.rhissue.fetch_issues')
    def test_rhissues_with_mrs(self, mock_fetch, mock_connect):
        """Returns the expected values when MRs are present."""
        mock_jira = mock.Mock()
        mock_connect.return_value = mock_jira
        base_issue = self.make_jira_issue('RHEL-39925')
        dep_issue = self.make_jira_issue('RHEL-30580')

        descriptions = self.load_yaml_asset(
            path='jirahookmr_centos-stream-9_4365-descriptions.json',
            sub_module='gitlab_graphql_api'
        )
        variables = {'namespace': 'redhat/centos-stream/src/kernel/centos-stream-9',
                     'mr_ids': ["4365"]}
        self.add_query(GET_MR_DESCRIPTIONS_QUERY, variables=variables, query_result=descriptions)

        mock_fetch.return_value = [base_issue, dep_issue]
        base_mr = self.build_cs9_mr(4401)

        mock_fetch.return_value = [dep_issue]
        dep_mr = self.build_cs9_mr(4365)

        self.assertIn('RHEL-39925', [rhi.id for rhi in base_mr.rhissues])
        self.assertIn('RHEL-30580', [rhi.id for rhi in base_mr.rhissues])
        for rhi in base_mr.rhissues_with_scopes:
            self.assertTrue(rhi.in_mr_description)
            if rhi.id == "RHEL-30580":
                self.assertTrue(rhi.is_dependency)
                self.assertEqual(defs.MrScope.IN_PROGRESS, rhi.scope)
            else:
                self.assertFalse(rhi.is_dependency)

        self.assertNotIn('RHEL-39925', [rhi.id for rhi in dep_mr.rhissues])
        self.assertIn('RHEL-30580', [rhi.id for rhi in dep_mr.rhissues])
        for rhi in dep_mr.rhissues:
            self.assertTrue(rhi.in_mr_description)

    def test_ji_pt_status_tasks(self):
        open_task = self.make_jira_issue('RHEL-36513')
        open_rhi = rhissue.RHIssue.new_from_ji(ji=open_task, mrs=None)
        self.assertEqual(defs.JIPTStatus.REQUESTED, open_rhi.ji_pt_status)

        closed_task = self.make_jira_issue('RHEL-49673')
        closed_rhi = rhissue.RHIssue.new_from_ji(ji=closed_task, mrs=None)
        self.assertEqual(defs.JIPTStatus.PASS, closed_rhi.ji_pt_status)

        new_task = self.make_jira_issue('RHEL-49674')
        new_rhi = rhissue.RHIssue.new_from_ji(ji=new_task, mrs=None)
        self.assertEqual(defs.JIPTStatus.UNSET, new_rhi.ji_pt_status)

    @mock.patch('webhook.rhissue.fetch_issues')
    def test_get_clones_from_issuelinks(self, mock_fi):
        mock_jira = mock.Mock()
        base_issue = self.make_jira_issue('RHEL-101')
        clone_issue = self.make_jira_issue('RHEL-104')
        base_rhi = rhissue.RHIssue.new_from_ji(ji=base_issue, jira=mock_jira, mrs=None)
        clone_rhi = rhissue.RHIssue.new_from_ji(ji=clone_issue, jira=mock_jira, mrs=None)
        mock_fi.return_value = [clone_issue]
        self.assertEqual(base_rhi.zstream_clones, [clone_rhi])
        self.assertEqual(clone_rhi.zstream_clones, [])

    @mock.patch('webhook.rhissue.fetch_issues')
    def test_find_cve_clones_from_jira_labels(self, mock_fi):
        mock_jira = mock.Mock()
        base_issue = self.make_jira_issue('RHEL-103')
        clone_issue = self.make_jira_issue('RHEL-104')
        base_rhi = rhissue.RHIssue.new_from_ji(ji=base_issue, jira=mock_jira, mrs=None)
        clone_rhi = rhissue.RHIssue.new_from_ji(ji=clone_issue, jira=mock_jira, mrs=None)
        mock_fi.return_value = [clone_issue]
        self.assertEqual(base_rhi.zstream_clones, [clone_rhi])

        no_clones_issue = self.make_jira_issue('RHEL-102')
        no_clones_rhi = rhissue.RHIssue.new_from_ji(ji=no_clones_issue, mrs=None)
        self.assertEqual(no_clones_rhi.zstream_clones, [])

    @mock.patch('webhook.rhissue.is_production_or_staging', mock.Mock(return_value=True))
    @mock.patch('webhook.rhissue.fetch_issues')
    def test_create_issue_links(self, mock_fetch):
        """Test issue link creation calls."""
        mock_jira = mock.Mock()
        base_issue = self.make_jira_issue('RHEL-103')
        base_issue.fields.status = 'New'
        clone_issue = self.make_jira_issue('RHEL-104')
        base_rhi = rhissue.RHIssue.new_from_ji(ji=base_issue, jira=mock_jira, mrs=None)
        mock_fetch.return_value = [clone_issue]
        with self.assertLogs('cki.webhook.rhissue', level='DEBUG') as logs:
            rhissue.RHIssue.create_issue_links(base_rhi)
            self.assertIn("Linking RHEL-104 as depending on RHEL-103", logs.output[-1])
            mock_jira.create_issue_link.assert_called_with('Depend', 'RHEL-104', 'RHEL-103')

    @mock.patch('webhook.rhissue.fetch_issues')
    def test_testing_tasks(self, mock_fetch):
        """Test out discovery of linked testing tasks."""
        mock_jira = mock.Mock()
        base_issue = self.make_jira_issue('RHEL-14726')
        closed_linked_issue = self.make_jira_issue('RHEL-49673')
        open_linked_issue = self.make_jira_issue('RHEL-36513')
        mock_fetch.return_value = [closed_linked_issue, open_linked_issue]
        base_rhi = rhissue.RHIssue.new_from_ji(ji=base_issue, jira=mock_jira, mrs=None)
        self.assertEqual(2, len(base_rhi.testing_tasks))
        self.assertEqual('Mike Stowell', base_rhi.qa_contact.displayName)
        closed_rhi = rhissue.RHIssue.new_from_ji(ji=closed_linked_issue, jira=mock_jira, mrs=None)
        self.assertEqual('Mike Stowell', closed_rhi.qa_contact.displayName)

    @mock.patch('webhook.rhissue.fetch_issues')
    def test_pool_team(self, mock_fetch):
        mock_fetch.return_value = []
        base_issue = self.make_jira_issue('RHEL-14726')
        base_rhi = rhissue.RHIssue.new_from_ji(ji=base_issue, mrs=None)
        self.assertEqual('sst_kernel_security', base_rhi.pool_team)
        base_issue = self.make_jira_issue('RHEL-108')
        base_rhi = rhissue.RHIssue.new_from_ji(ji=base_issue, mrs=None)
        self.assertIsNone(base_rhi.pool_team)

    @mock.patch('webhook.rhissue.fetch_issues')
    def test_subsystem_group(self, mock_fetch):
        mock_fetch.return_value = []
        base_issue = self.make_jira_issue('RHEL-1559')
        base_rhi = rhissue.RHIssue.new_from_ji(ji=base_issue, mrs=None)
        self.assertEqual('ssg_platform_enablement', base_rhi.subsystem_group)
        base_issue = self.make_jira_issue('RHEL-14726')
        base_rhi = rhissue.RHIssue.new_from_ji(ji=base_issue, mrs=None)
        self.assertIsNone(base_rhi.subsystem_group)

    def test_itm(self):
        issue = self.make_jira_issue('RHEL-101')
        mock_itm = mock.Mock()
        mock_itm.value = 7
        issue.fields.customfield_12321040 = mock_itm
        rhi = rhissue.RHIssue.new_from_ji(ji=issue, mrs=None)
        self.assertEqual(7, rhi.itm)

        issue.fields.customfield_12321040 = None
        self.assertEqual(0, rhi.itm)

        no_ji_rhi = rhissue.RHIssue.new_missing(ji_id="RHEL-101")
        self.assertEqual(0, no_ji_rhi.itm)

    def test_dtm(self):
        issue = self.make_jira_issue('RHEL-101')
        mock_dtm = mock.Mock()
        mock_dtm.value = 5
        issue.fields.customfield_12318141 = mock_dtm
        rhi = rhissue.RHIssue.new_from_ji(ji=issue, mrs=None)
        self.assertEqual(5, rhi.dtm)

        issue.fields.customfield_12318141 = None
        self.assertEqual(0, rhi.dtm)

        no_ji_rhi = rhissue.RHIssue.new_missing(ji_id="RHEL-101")
        self.assertEqual(0, no_ji_rhi.dtm)

    def test_release_milestone(self):
        issue = self.make_jira_issue('RHEL-59973')
        rhi = rhissue.RHIssue.new_from_ji(ji=issue, mrs=None)
        expected = {'submitter_checks_pass': False,
                    'cki_tests_done': True,
                    'cki_tests_pass': False,
                    'mr_approved': False}
        self.assertEqual(rhi.release_milestone, expected)

    @mock.patch('webhook.rhissue.fetch_issues')
    def test_vulnerability_fields(self, mock_fetch):
        mock_fetch.return_value = []
        issue = self.make_jira_issue('RHEL-44863')
        rhi = rhissue.RHIssue.new_from_ji(ji=issue, mrs=None)
        self.assertFalse(rhi.embargoed)
        self.assertEqual("CVE-2024-00000", rhi.cve_id)
        self.assertEqual(["CVE-2024-00000"], rhi.ji_cves)
        self.assertEqual(defs.IssueSeverity.CRITICAL, rhi.severity)

        issue.fields.customfield_12324750.value = "True"
        issue.fields.customfield_12316142.value = "Normal"
        issue.fields.customfield_12324749 = "CVE-1234-5678"
        rhi = rhissue.RHIssue.new_from_ji(ji=issue, mrs=None)
        self.assertTrue(rhi.embargoed)
        self.assertEqual("CVE-1234-5678", rhi.cve_id)
        self.assertEqual(["CVE-1234-5678"], rhi.ji_cves)
        self.assertEqual(defs.IssueSeverity.MODERATE, rhi.severity)

        issue = self.make_jira_issue('RHEL-108')
        rhi = rhissue.RHIssue.new_from_ji(ji=issue, mrs=None)
        self.assertIsNone(rhi.embargoed)

    @mock.patch('webhook.rhissue.RHIssue.mr', new_callable=mock.PropertyMock)
    def test_subsystems_with_test_variants(self, mock_rhimr):
        """Test parsing of subsystems with test variants."""
        test_issue = self.make_jira_issue('RHEL-49673')
        test_rhi = rhissue.RHIssue.new_from_ji(ji=test_issue, mrs=[])
        mock_mr = mock.Mock()
        mock_mr.subsystems_with_test_variants = {'rcu': ['kernel-rt']}
        mock_rhimr.return_value = mock_mr
        test_rhi._mrs = [mock_mr]
        sst_list = test_rhi.subsystems_with_test_variants
        self.assertEqual(sst_list, ['rcu'])

    @mock.patch('webhook.rhissue.is_production_or_staging', mock.Mock(return_value=True))
    @mock.patch('jira.resources.Issue.update')
    def test_spawn_testing_tasks(self, mock_update):
        """Test out functionality of spawn_testing_tasks method."""
        test_issue = self.make_jira_issue('RHEL-30580')
        test_rhi = rhissue.RHIssue.new_from_ji(ji=test_issue, mrs=[])
        new_variants = ['kernel-rt']
        test_rhi.spawn_testing_tasks(new_variants)
        expected_labels = test_rhi.labels
        expected_labels.append('KWF:kernel-rt')
        mock_update.assert_called_with(fields={"labels": expected_labels})

    def test_rhissue_mr_urls(self):
        """Test remote links parsing for mr_urls."""
        mock_link = mock.Mock()
        mock_link.object.url = f'{defs.GITFORGE}/redhat/something/-/merge_requests/1234'
        mock_jira = mock.Mock()
        mock_jira.remote_links.return_value = [mock_link]
        issue = self.make_jira_issue('RHEL-101')
        rhi = rhissue.RHIssue.new_from_ji(ji=issue, jira=mock_jira, mrs=None)
        mr_url = rhi.mr_urls[0]
        self.assertEqual(mr_url.base_url, defs.GITFORGE)
        self.assertEqual(mr_url.namespace, 'redhat/something')
        self.assertEqual(mr_url.id, 1234)

    @mock.patch('webhook.rhissue.fetch_issues')
    def test_rhissue_cloned_from(self, mock_fetch):
        """Test rhissue cloned_from property."""
        mock_jira = mock.Mock()
        issue = self.make_jira_issue('RHEL-101')
        rhi = rhissue.RHIssue.new_from_ji(ji=issue, jira=mock_jira, mrs=None)
        self.assertEqual([], rhi.cloned_from)

        # z-stream CVE issue, where we need to find the highest stream (same major) CVE issue
        z_issue = self.make_jira_issue('RHEL-47894')
        # z_issue.fields.customfield_12324749 = "CVE-2024-40927"
        zrhi = rhissue.RHIssue.new_from_ji(ji=z_issue, jira=mock_jira, mrs=None)
        self.assertEqual(zrhi.ji_fix_version, "rhel-9.4.z")
        self.assertIsNotNone(zrhi.projects)
        self.assertIsNotNone(zrhi.ji_cves)

        y_issue = self.make_jira_issue('RHEL-47892')
        yrhi = rhissue.RHIssue.new_from_ji(ji=y_issue, jira=mock_jira, mrs=None)
        self.assertEqual(yrhi.ji_fix_version, "rhel-9.5")
        mock_fetch.return_value = [y_issue]
        self.assertEqual([yrhi], zrhi.cloned_from)

    @mock.patch('webhook.rhissue.fetch_issues')
    @mock.patch('webhook.rhissue.update_issue_field', mock.Mock())
    def test_linked_rhissues(self, mock_fetch):
        """Test out discovery of linked issues."""
        mock_jira = mock.Mock()
        base_issue = self.make_jira_issue('RHEL-101')
        base_rhi = rhissue.RHIssue.new_linked_ji(ji=base_issue, jira=mock_jira, mrs=None)
        self.assertEqual([], base_rhi.linked_rhissues)

        non_kwf_issue = self.make_jira_issue('RHEL-105')
        non_kwf_rhi = rhissue.RHIssue.new_from_ji(ji=non_kwf_issue, jira=mock_jira, mrs=None)
        self.assertEqual([], non_kwf_rhi.linked_rhissues)

        non_cve_issue = self.make_jira_issue('RHEL-102')
        non_cve_rhi = rhissue.RHIssue.new_from_ji(ji=non_cve_issue, jira=mock_jira, mrs=None)
        self.assertEqual([], non_cve_rhi.linked_rhissues)

        closed_issue = self.make_jira_issue('RHEL-103')
        closed_rhi = rhissue.RHIssue.new_from_ji(ji=closed_issue, jira=mock_jira, mrs=None)
        self.assertEqual([], closed_rhi.linked_rhissues)

        base_issue = self.make_jira_issue('RHEL-103')
        base_issue.fields.status = 'New'
        base_rhi = rhissue.RHIssue.new_from_ji(ji=base_issue, jira=mock_jira, mrs=None)
        variant_issue = self.make_jira_issue('RHEL-104')
        variant_rhi = rhissue.RHIssue.new_linked_ji(ji=variant_issue, jira=mock_jira, mrs=None)
        mock_fetch.return_value = [variant_issue]
        self.assertEqual([variant_rhi], base_rhi.linked_rhissues)

        base_issue = self.make_jira_issue('RHEL-106')
        base_rhi = rhissue.RHIssue.new_from_ji(ji=base_issue, jira=mock_jira, mrs=None)
        variant_issue = self.make_jira_issue('RHEL-107')
        variant_rhi = rhissue.RHIssue.new_linked_ji(ji=variant_issue, jira=mock_jira, mrs=None)
        mock_fetch.return_value = [variant_issue]
        self.assertEqual([variant_rhi], base_rhi.linked_rhissues)

        base_issue = self.make_jira_issue('RHEL-14726')
        closed_linked_issue = self.make_jira_issue('RHEL-49673')
        open_linked_issue = self.make_jira_issue('RHEL-36513')
        mock_fetch.return_value = [closed_linked_issue, open_linked_issue]
        base_rhi = rhissue.RHIssue.new_from_ji(ji=base_issue, jira=mock_jira, mrs=None)
        closed_rhi = rhissue.RHIssue.new_linked_ji(ji=closed_linked_issue, jira=mock_jira, mrs=None)
        open_rhi = rhissue.RHIssue.new_linked_ji(ji=open_linked_issue, jira=mock_jira, mrs=None)
        self.assertCountEqual([closed_rhi, open_rhi], base_rhi.linked_rhissues)
