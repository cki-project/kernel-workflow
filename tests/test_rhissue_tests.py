"""Tests for the rhissue_tests module."""
import os
import typing
from unittest import mock

from tests.helpers import KwfTestCase
from webhook import defs
from webhook import rhissue_tests
from webhook.rh_metadata import Projects
from webhook.rhissue import RHIssue


class DepOnlyTests(KwfTestCase):
    """Validate the tests that are specific to dependency JIRA Issues."""

    @staticmethod
    def _fake_rhissue(input_dict):
        """Return a mock object with spec set."""
        attr_dict = input_dict.copy()
        if 'alias' not in attr_dict:
            attr_dict['alias'] = 'FakeRHIssue'
        if 'failed_tests' not in attr_dict:
            attr_dict['failed_tests'] = []
        if 'test_failed' not in attr_dict:
            attr_dict['test_failed'] = mock.Mock(return_value=False, spec=[])
        return mock.Mock(spec=list(attr_dict.keys()), **attr_dict)

    def _run_test(self, rhissue, test, expected_result=True, expected_scope=None,
                  expected_keep_going=True):
        """Run the given test on the given JIRA Issue."""
        if expected_scope is None:
            expected_scope = rhissue_tests.MrScope.READY_FOR_MERGE if expected_result else \
                rhissue_tests.MrScope.IN_PROGRESS
        actual_result, actual_scope, actual_keep_going = test(rhissue)
        self.assertIs(actual_result, expected_result)
        self.assertIs(actual_scope, expected_scope)
        self.assertIs(actual_keep_going, expected_keep_going)
        if expected_result is True:
            self.assertTrue(test.__name__ not in rhissue.failed_tests)
        else:
            self.assertTrue(test.__name__ in rhissue.failed_tests)

    def test_ParentCommitsMatch(self):
        """The Dependency MR commits should match the Dependant (parent) MR commits."""
        this_test = rhissue_tests.ParentCommitsMatch
        # The MR is not merged and commits == parent_mr_commits
        issue_values = {'commits': [1, 2, 3],
                        'mr': mock.Mock(spec=['state'], state=rhissue_tests.MrState.OPENED),
                        'parent_mr_commits': [1, 2, 3]}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # The MR is not merged and commits != parent_mr_commits
        issue_values = {'commits': [1, 2, 3],
                        'mr': mock.Mock(spec=['state'], state=rhissue_tests.MrState.OPENED),
                        'parent_mr_commits': [1, 2, 3, 4]}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False)

        # The MR is merged, but there are still visible commits on the parent that reference it.
        issue_values = {'mr': mock.Mock(spec=['state'], state=rhissue_tests.MrState.MERGED),
                        'parent_mr_commits': [1, 2, 3]}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False)

        # The MR is merged and the parent MR does not have any commits that reference it 👍.
        issue_values = {'mr': mock.Mock(spec=['state'], state=rhissue_tests.MrState.MERGED),
                        'parent_mr_commits': []}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

    def test_MRIsNotMerged(self):
        """Passes if the MR is not Merged, otherwise 'fails'."""
        this_test = rhissue_tests.MRIsNotMerged
        # It is not merged, passes.
        issue_values = {'mr': mock.Mock(spec=['state'], state=rhissue_tests.MrState.OPENED)}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # It IS merged, fails.
        issue_values = {'mr': mock.Mock(spec=['state'], state=rhissue_tests.MrState.MERGED)}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False,
                       expected_scope=rhissue_tests.MrScope.READY_FOR_MERGE,
                       expected_keep_going=False)

        # The test is skipped (passes) if the ParentCommitsMatch test failed.
        issue_values = {'mr': mock.Mock(spec=['state'], state=rhissue_tests.MrState.MERGED),
                        'test_failed': mock.Mock(return_value=True, spec=[])}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)
        issue_values['test_failed'].assert_called_once_with('ParentCommitsMatch')

    def test_InMrDescription(self):
        """Passes if the JIRA Issue is listed in the MR description."""
        this_test = rhissue_tests.InMrDescription

        # A build MR, true if is_build_mr and internal
        issue_values = {
            'commits': [],
            'in_mr_description': False,
            'internal': True,
            'mr': mock.Mock(spec='is_build_mr', is_build_mr=True)
        }
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # A JIRA Issue without commits skips ("passes") the test.
        issue_values = {
            'commits': [],
            'in_mr_description': False,
            'internal': False,
            'mr': mock.Mock(spec='is_build_mr', is_build_mr=False)
        }
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # A JIRA Issue with commits which are in the MR description passes the test.
        issue_values = {
            'commits': [1, 2, 3],
            'in_mr_description': True,
            'internal': False,
            'mr': mock.Mock(spec='is_build_mr', is_build_mr=False)
        }
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # A JIRA Issue with commits any of which are NOT in the MR description fails the test.
        issue_values = {
            'commits': [1, 2, 3],
            'in_mr_description': False,
            'internal': False,
            'mr': mock.Mock(spec='is_build_mr', is_build_mr=False)
        }
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False,
                       expected_scope=rhissue_tests.MrScope.MISSING)

    def test_HasCommits(self):
        """Passes if the JIRA Issue has commits."""
        this_test = rhissue_tests.HasCommits
        # A JIRA Issue with commits passes the test.
        issue_values = {'commits': [1]}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # A JIRA Issue without commits fails the test.
        issue_values = {'commits': []}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False,
                       expected_scope=rhissue_tests.MrScope.MISSING)

    def test_JIisNotWrongType(self):
        """Passes if the issuetype is in JIRA_SUPPORT_ISSUE_TYPES."""
        this_test = rhissue_tests.JIisNotWrongType

        class TypeTest(typing.NamedTuple):
            issue_values: dict
            expected_result: bool

        tests = [
            # A type the kwf doesn't support, fails the test.
            TypeTest(issue_values={'ji_type': defs.JIType.UNKNOWN}, expected_result=False),
            # A good type, passes the test
            TypeTest(issue_values={'ji_type': defs.JIType.BUG}, expected_result=True),
            TypeTest(issue_values={'ji_type': defs.JIType.STORY}, expected_result=True),
            TypeTest(issue_values={'ji_type': defs.JIType.TASK}, expected_result=True),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                fake_rhissue = self._fake_rhissue(test.issue_values)
                expected_scope = rhissue_tests.MrScope.OK if test.expected_result else \
                    rhissue_tests.MrScope.INVALID
                self._run_test(
                    rhissue=fake_rhissue,
                    test=this_test,
                    expected_result=test.expected_result,
                    expected_scope=expected_scope,
                    expected_keep_going=test.expected_result
                )

    def test_JIisNotUnknown(self):
        """Passes if the JIStatus is not UNKNOWN."""
        this_test = rhissue_tests.JIisNotUnknown
        # A JIRA Issue with a not UNKNOWN status passes the test.
        issue_values = {'ji_status': rhissue_tests.JIStatus.IN_PROGRESS}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # A JIRA Issue with an UNKNOWN status fails the test.
        issue_values = {'ji_status': rhissue_tests.JIStatus.UNKNOWN}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False,
                       expected_scope=rhissue_tests.MrScope.INVALID, expected_keep_going=False)

    def test_JIisNotClosed(self):
        """Passes if the JIStatus is not CLOSED."""
        this_test = rhissue_tests.JIisNotClosed
        # A JIRA Issue with a not Closed status passes the test.
        issue_values = {'ji_status': rhissue_tests.JIStatus.READY_FOR_QA,
                        'is_task': False}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # A JIRA Task Issue with a CLOSED status passes the test.
        issue_values = {'ji_status': rhissue_tests.JIStatus.CLOSED,
                        'is_task': True}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # A JIRA Bug Issue with a CLOSED status fails the test.
        issue_values = {'ji_status': rhissue_tests.JIStatus.CLOSED,
                        'is_task': False}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False,
                       expected_scope=rhissue_tests.MrScope.CLOSED, expected_keep_going=False)

    def test_NotUntagged(self):
        """Passes if the JIRA Issue is not the special UNTAGGED variant."""
        this_test = rhissue_tests.NotUntagged
        # A "normal" JIRA Issue passes to the test.
        issue_values = {'untagged': False}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # The UNTAGGED JIRA Issue fails the test.
        issue_values = {'untagged': True}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False,
                       expected_scope=rhissue_tests.MrScope.MISSING, expected_keep_going=False)

    def test_JIhasSeverity(self):
        """Passes if the JIRA Issues has a Severity set"""
        this_test = rhissue_tests.JIhasSeverity

        # Non-'Bug' issuetype Issue with no severity set passes the test.
        fake_rhissue = self._fake_rhissue({'ji_type': defs.JIType.STORY})
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=True)

        # 'Bug' issuetype without a severity fails the test.
        issue_attrs = {'ji_type': defs.JIType.BUG, 'severity': None}
        fake_rhissue = self._fake_rhissue(issue_attrs)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False,
                       expected_scope=rhissue_tests.MrScope.NEEDS_REVIEW)

        issue_attrs = {'ji_type': defs.JIType.BUG, 'severity': rhissue_tests.IssueSeverity.UNKNOWN}
        fake_rhissue = self._fake_rhissue(issue_attrs)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False,
                       expected_scope=rhissue_tests.MrScope.NEEDS_REVIEW)

        # 'Bug' issuetype with a severity that is not IssueSeverity.UNKNOWN pass the test.
        for severity_val in (1, 2, 3, 4):
            severity = rhissue_tests.IssueSeverity(severity_val)

            with self.subTest(severity=severity):
                fake_rhissue = self._fake_rhissue({
                    'ji_type': defs.JIType.BUG,
                    'severity': severity
                })

                self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=True)

    def test_CveInMrDescription(self):
        """Passes if the CVE IDs in the JIRA Issue summary are in the MR description."""
        this_test = rhissue_tests.CveInMrDescription

        # No ji_cves, test passes.
        fake_rhissue = self._fake_rhissue({'ji_cves': [],
                                           'mr': mock.Mock(spec='is_build_mr', is_build_mr=True)})
        self._run_test(rhissue=fake_rhissue, test=this_test)

        mr_cves_spec = ['cve_ids', 'in_mr_description']
        mr_cves = [mock.Mock(spec=mr_cves_spec, cve_ids=['CVE-5621-16611'], in_mr_description=True),
                   mock.Mock(spec=mr_cves_spec, cve_ids=['CVE-1234-26727'], in_mr_description=True)]

        # A build MR
        issue_values = {'ji_cves': ['CVE-1234-26727', 'CVE-5621-16611'],
                        'mr': mock.Mock(spec=['cves', 'mr'],
                                        cves=mr_cves,
                                        is_build_mr=True)}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # All the IDs in the summary are in the MR cves list, test passes.
        issue_values = {'ji_cves': ['CVE-1234-26727', 'CVE-5621-16611'],
                        'mr': mock.Mock(spec=['cves', 'mr'],
                                        cves=mr_cves,
                                        is_build_mr=False)}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # Not all the IDs in the summary are in the MR cves list, test fails.
        issue_values = {'ji_cves': ['CVE-1234-26727', 'CVE-2367-8538', 'CVE-5621-16611'],
                        'mr': mock.Mock(spec=['cves', 'mr'],
                                        cves=mr_cves,
                                        is_build_mr=False)}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False,
                       expected_scope=rhissue_tests.MrScope.NEEDS_REVIEW)

        # All the IDs in the summary are in the MR cves list, but CVEs are not in the description,
        # test fails.
        mr_cves[1].in_mr_description = False
        issue_values = {'ji_cves': ['CVE-1234-26727', 'CVE-5621-16611'],
                        'mr': mock.Mock(spec=['cves', 'mr'],
                                        cves=mr_cves,
                                        is_build_mr=False)}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False,
                       expected_scope=rhissue_tests.MrScope.NEEDS_REVIEW)

    def test_IsValidInternal(self):
        """Passes if the JIRA Issue and MR are properly marked internal."""
        this_test = rhissue_tests.IsValidInternal
        # A build MR
        issue_values = {'internal': False,
                        'mr': mock.Mock(spec=['only_internal_files', 'is_build_mr'],
                                        only_internal_files=False,
                                        is_build_mr=True)}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # A "normal" JIRA Issue passes to the test.
        issue_values = {'internal': False,
                        'mr': mock.Mock(spec=['only_internal_files', 'is_build_mr'],
                                        only_internal_files=False,
                                        is_build_mr=False)}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # An INTERNAL JIRA Issue passes the test when the MR has only_internal_files.
        issue_values = {'internal': True,
                        'mr': mock.Mock(spec=['only_internal_files', 'is_build_mr'],
                                        only_internal_files=True,
                                        is_build_mr=False)}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # An INTERNAL JIRA Issue fails the test when the MR does NOT have only_internal_files.
        issue_values = {'internal': True,
                        'mr': mock.Mock(spec=['only_internal_files', 'is_build_mr'],
                                        only_internal_files=False,
                                        is_build_mr=False)}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False,
                       expected_scope=rhissue_tests.MrScope.INVALID)

    def test_IsAssigned(self) -> None:
        """Passes if the issue is assigned to someone."""
        this_test = rhissue_tests.IsAssigned
        passing_scope = rhissue_tests.MrScope.READY_FOR_MERGE
        failure_scope = rhissue_tests.MrScope.NEEDS_REVIEW

        class IsAssignedTest(typing.NamedTuple):
            issue_values: typing.Dict
            expected_result: bool

        tests = [
            # An issue that is assigned to someone passes the test.
            IsAssignedTest(
                issue_values={'assignee': mock.Mock()},
                expected_result=True
            ),
            # An issue that is not assigned fails the test.
            IsAssignedTest(
                issue_values={'assignee': None},
                expected_result=False
            ),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                fake_rhissue = self._fake_rhissue(test.issue_values)
                expected_scope = passing_scope if test.expected_result else failure_scope
                self._run_test(rhissue=fake_rhissue, test=this_test,
                               expected_result=test.expected_result, expected_scope=expected_scope)

    def test_AssigneeIsNotBot(self) -> None:
        """Passes if the issue is not assigned to known bot/list."""
        this_test = rhissue_tests.AssigneeIsNotBot
        passing_scope = rhissue_tests.MrScope.READY_FOR_MERGE
        failure_scope = rhissue_tests.MrScope.NEEDS_REVIEW

        class AssigneeIsNotBotTest(typing.NamedTuple):
            issue_values: typing.Dict
            expected_result: bool

        real_user = mock.Mock()
        real_user.emailAddress = 'someone@redhat.com'

        bot_user = mock.Mock()
        bot_user.emailAddress = 'kernel-mgr@redhat.com'

        tests = [
            # An issue that is assigned to someone passes the test.
            AssigneeIsNotBotTest(
                issue_values={'assignee': real_user},
                expected_result=True
            ),
            # An issue that is not assigned fails the test.
            AssigneeIsNotBotTest(
                issue_values={'assignee': bot_user},
                expected_result=False
            ),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                fake_rhissue = self._fake_rhissue(test.issue_values)
                expected_scope = passing_scope if test.expected_result else failure_scope
                self._run_test(rhissue=fake_rhissue, test=this_test,
                               expected_result=test.expected_result, expected_scope=expected_scope)

    def test_HasQAContact(self) -> None:
        """Passes if the issue has a QA Contact."""
        this_test = rhissue_tests.HasQAContact
        passing_scope = rhissue_tests.MrScope.READY_FOR_MERGE
        failure_scope = rhissue_tests.MrScope.NEEDS_REVIEW

        class HasQAContactTest(typing.NamedTuple):
            issue_values: typing.Dict
            expected_result: bool

        tests = [
            # An issue that is assigned to someone passes the test.
            HasQAContactTest(
                issue_values={'qa_contact': mock.Mock()},
                expected_result=True
            ),
            # An issue that is not assigned fails the test.
            HasQAContactTest(
                issue_values={'qa_contact': None},
                expected_result=False
            ),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                fake_rhissue = self._fake_rhissue(test.issue_values)
                expected_scope = passing_scope if test.expected_result else failure_scope
                self._run_test(rhissue=fake_rhissue, test=this_test,
                               expected_result=test.expected_result, expected_scope=expected_scope)

    def test_IsNotTestOnly(self) -> None:
        """Passes if the issue does *not* have the TestOnly keyword."""
        this_test = rhissue_tests.IsNotTestOnly
        passing_scope = rhissue_tests.MrScope.READY_FOR_MERGE
        failure_scope = rhissue_tests.MrScope.NEEDS_REVIEW

        class IsNotTestOnlyTest(typing.NamedTuple):
            issue_key: str
            expected_result: bool

        tests = [
            # RHEL-1559 has the TestOnly keyword so should fail the test.
            IsNotTestOnlyTest(
                issue_key='RHEL-1559',
                expected_result=False
            ),
            # RHEL-26081 does *not* have the TestOnly keyword so should pass the test.
            IsNotTestOnlyTest(
                issue_key='RHEL-26081',
                expected_result=True
            ),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                test_ji = self.make_jira_issue(test.issue_key)
                test_rhissue = RHIssue.new_from_ji(ji=test_ji, mrs=[])
                expected_scope = passing_scope if test.expected_result else failure_scope
                self._run_test(rhissue=test_rhissue, test=this_test,
                               expected_result=test.expected_result, expected_scope=expected_scope)

    def test_QEApproved(self):
        """Passes if the ITM field has a value set."""
        this_test = rhissue_tests.QEApproved
        mock_ji = mock.Mock()

        # No ITM set, do not pass check
        issue_values = {'ji': mock_ji, 'ji_fix_version': defs.FixVersion('rhel-8.10.0'),
                        'is_task': False, 'itm': None}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False,
                       expected_scope=rhissue_tests.MrScope.PLANNING)

        # No ITM set, but it's z-stream
        issue_values = {'ji': mock_ji, 'ji_fix_version': defs.FixVersion('rhel-8.9.0.z'),
                        'is_task': False, 'itm': None}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # No ITM set, but it's RHEL-6-ELS
        issue_values = {'ji': mock_ji, 'ji_fix_version': defs.FixVersion('rhel-6-els'),
                        'is_task': False, 'itm': None}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # ITM is set, test should pass
        mock_itm = mock.Mock(value='1')
        issue_values = {'ji': mock_ji, 'ji_fix_version': defs.FixVersion('rhel-8.10.0'),
                        'is_task': False, 'itm': mock_itm}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

    def test_DevApproved(self):
        """Passes if the DTM field has a value set."""
        this_test = rhissue_tests.DevApproved
        mock_ji = mock.Mock()

        # No DTM set, do not pass check
        issue_values = {'ji': mock_ji, 'ji_fix_version': defs.FixVersion('rhel-8.10.0'),
                        'is_task': False, 'is_vulnerability': False, 'dtm': None}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False,
                       expected_scope=rhissue_tests.MrScope.PLANNING)

        # No DTM set, but it's z-stream
        issue_values = {'ji': mock_ji, 'ji_fix_version': defs.FixVersion('rhel-8.9.0.z'),
                        'is_task': False, 'is_vulnerability': False, 'dtm': None}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # DTM is set, test should pass
        mock_dtm = mock.Mock(value='1')
        issue_values = {'ji': mock_ji, 'ji_fix_version': defs.FixVersion('rhel-8.10.0'),
                        'is_task': False, 'is_vulnerability': False, 'dtm': mock_dtm}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # Vulnerability issue, there is no DTM
        issue_values = {'ji': mock_ji, 'ji_fix_version': defs.FixVersion('rhel-8.10.0'),
                        'is_task': False, 'is_vulnerability': True, 'dtm': None}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

    def test_JIisNotNewMRisDraft(self):
        """Passes if the Jira issue isn't in New and MR isn't in Draft."""
        this_test = rhissue_tests.JIisNotNewMRisDraft

        # This should fail
        mock_mr = mock.Mock(is_draft=True)
        issue_values = {'ji_status': rhissue_tests.JIStatus.NEW, 'mr': mock_mr}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test,
                       expected_scope=rhissue_tests.MrScope.NEW, expected_result=False)

        # This should succeed
        mock_mr = mock.Mock(is_draft=False)
        issue_values = {'ji_status': rhissue_tests.JIStatus.PLANNING, 'mr': mock_mr}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

    def test_CommitPolicyApproved(self):
        """Passes if the policy check passed."""
        this_test = rhissue_tests.CommitPolicyApproved
        # An Approved JIRA Issue passes the test.
        issue_values = {'policy_check_ok': (True, '')}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # A not-approved JIRA Issue fails the test.
        issue_values = {'policy_check_ok': (False, 'No JIRA Issue')}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False)

    def test_PrelimTestingPass(self):
        """Passes if the JIRA Issue is Tested."""
        this_test = rhissue_tests.PrelimTestingPass
        # A JIRA Bug Issue with Preliminary Testing: Pass passes the test.
        issue_values = {'ji_pt_status': defs.JIPTStatus.PASS,
                        'is_task': False}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # A JIRA Bug Issue without Preliminary Testing: Pass fails the test.
        issue_values = {'ji_pt_status': defs.JIPTStatus.FAIL,
                        'is_task': False}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False,
                       expected_scope=rhissue_tests.MrScope.READY_FOR_QA)

        # A JIRA Task Issue passes the test if Closed.
        issue_values = {'ji_pt_status': defs.JIPTStatus.PASS,
                        'ji_status': rhissue_tests.JIStatus.CLOSED,
                        'is_task': True}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # A JIRA Task Issue fails the test if not Closed.
        issue_values = {'ji_pt_status': defs.JIPTStatus.FAIL,
                        'ji_status': rhissue_tests.JIStatus.IN_PROGRESS,
                        'is_task': True}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False,
                       expected_scope=rhissue_tests.MrScope.READY_FOR_QA)

        # The test is skipped (passes) if the CommitPolicyApproved test failed.
        issue_values = {'ji_pt_status': defs.JIPTStatus.PASS,
                        'is_task': False,
                        'test_failed': mock.Mock(return_value=True, spec=[])}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)
        issue_values['test_failed'].assert_called_once_with('CommitPolicyApproved')

    def test_NotMissingTestingTasks(self):
        """Passes if the JIRA Issue has all expected linked JIRA testing Task issues."""
        this_test = rhissue_tests.NotMissingTestingTasks

        # A Task, which should always pass, this is only valid for Bug/Story Issues
        issue_values = {'ji_type': defs.JIType.TASK, 'ji_component': 'kernel'}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # An issue missing a Task
        issue_values = {'ji_type': defs.JIType.BUG,
                        'ji_component': 'kernel',
                        'test_variants': ['kernel-foobar'],
                        'mr': None,
                        'testing_tasks': []}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False,
                       expected_scope=rhissue_tests.MrScope.PLANNING)

        # Everything is good
        task_values = {'ji_type': defs.JIType.TASK,
                       'ji_component': 'kernel-foobar'}
        fake_task = self._fake_rhissue(task_values)
        issue_values = {'ji_type': defs.JIType.BUG,
                        'ji_component': 'kernel',
                        'test_variants': ['kernel-foobar'],
                        'mr': None,
                        'testing_tasks': [fake_task]}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

    def test_NotPrelimTestingFail(self):
        """Passes if the JIRA Issue did not Fail Preliminary Testing."""
        this_test = rhissue_tests.NotPrelimTestingFail
        # A JIRA Issue without Preliminary Testing: Fail passes the test.
        issue_values = {'ji_pt_status': defs.JIPTStatus.PASS}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # A JIRA Issue with Preliminary Testing: Fail fails the test.
        issue_values = {'ji_pt_status': defs.JIPTStatus.FAIL}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False,
                       expected_scope=rhissue_tests.MrScope.PLANNING)

        # The test is skipped (passes) if the CommitPolicyApproved test failed.
        issue_values = {'ji_pt_status': defs.JIPTStatus.PASS,
                        'test_failed': mock.Mock(return_value=True, spec=[])}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)
        issue_values['test_failed'].assert_called_once_with('CommitPolicyApproved')

    def test_TargetReleaseSet(self):
        """Passes if either ITR or ZTR are set."""
        this_test = rhissue_tests.TargetReleaseSet
        mock_ji = mock.Mock()
        mock_ji.fields = mock.Mock()
        # A JIRA Issue with no JIRA Issue skips (passes) the test.
        issue_values = {'ji': None}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # A JIRA Issue without a fix version fails the test.
        mock_ji.fields.fixVersions = []
        issue_values = {'ji': mock_ji, 'ji_fix_version': []}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False,
                       expected_scope=rhissue_tests.MrScope.PLANNING)

    def test_CentOSZStream(self):
        """Passes if a c9s JIRA Issue does not have zstream_target_release set."""
        this_test = rhissue_tests.CentOSZStream

        mock_project = mock.Mock(spec=['name'])
        mock_project.name = 'centos-stream-9'
        # A JIRA Issue that is a dependency skips (passes) the test.
        issue_values = {'is_dependency': True,
                        'mr': mock.Mock(spec=['rh_project'], rh_project=mock_project),
                        'ji': mock.Mock(spec=[])}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # A JIRA Issue in a non-c9s MR (passes) the test.
        mock_project.name = 'rhel-9'
        issue_values = {'is_dependency': False,
                        'mr': mock.Mock(spec=['rh_project'], rh_project=mock_project),
                        'ji': mock.Mock(spec=[])}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # A JIRA Issue without a rhissue (passes) the test.
        mock_project.name = 'centos-stream-9'
        issue_values = {'is_dependency': False,
                        'mr': mock.Mock(spec=['rh_project'], rh_project=mock_project), 'ji': None}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # A JIRA Issue without a ji_branch fails the test.
        mock_ji = mock.Mock(spec=[])
        issue_values = {'is_dependency': False,
                        'mr': mock.Mock(spec=['rh_project'], rh_project=mock_project),
                        'ji': mock_ji, 'ji_branch': None}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False,
                       expected_scope=rhissue_tests.MrScope.PLANNING)

        # A JIRA Issue with a zstream ji_branch fails the test.
        mock_branch = mock.Mock(spec=['zstream_target_release'], zstream_target_release='9.0')
        issue_values = {'is_dependency': False,
                        'mr': mock.Mock(spec=['rh_project'], rh_project=mock_project),
                        'ji': mock_ji, 'ji_branch': mock_branch}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False,
                       expected_scope=rhissue_tests.MrScope.PLANNING)

        # A JIRA Issue with a ystream ji_branch passes the test.
        mock_branch = mock.Mock(spec=['internal_target_release', 'zstream_target_release'],
                                internal_target_release='9.1', zstream_target_release='')
        issue_values = {'is_dependency': False,
                        'mr': mock.Mock(spec=['rh_project'], rh_project=mock_project),
                        'ji': mock_ji, 'ji_branch': mock_branch}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # A JIRA Issue that failed 'TargetReleaseSet' skips (passes) the test.
        mock_branch = mock.Mock(spec=['internal_target_release', 'zstream_target_release'],
                                internal_target_release='9.1', zstream_target_release='')
        issue_values = {'is_dependency': False,
                        'mr': mock.Mock(spec=['rh_project'], rh_project=mock_project),
                        'ji': mock_ji, 'ji_branch': mock_branch,
                        'test_failed': mock.Mock(return_value=True, spec=[])}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)
        issue_values['test_failed'].assert_called_once_with('TargetReleaseSet')

    def test_ComponentMatches(self):
        """Passes if the MR target branch component matches the JIRA Issue's component."""
        this_test = rhissue_tests.ComponentMatches
        mock_mr = mock.Mock(spec=['rh_branch'])
        mock_branch1 = mock.Mock(spec=['components', 'version'],
                                 components={'kernel'}, version='9.4')
        mock_branch2 = mock.Mock(spec=['components', 'version'],
                                 components={'kernel'}, version='8.9')

        # A rhissue with a component that is in the MR Branch passes the test.
        mock_mr.rh_branch = mock_branch1
        issue_values = {'ji': mock.Mock(), 'mr': mock_mr, 'ji_component': 'kernel',
                        'ji_cves': [], 'is_task': False}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # A JIRA Issue with a component not matching the MR branch component fails the test.
        issue_values = {'ji': mock.Mock(), 'mr': mock_mr, 'ji_component': 'systemd',
                        'ji_cves': [], 'is_task': False}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False)

        # A JIRA Issue without a ji skips (passes) the test.
        issue_values = {'ji': None}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # A JIRA Issue that failed the CentOSZStream test skips (passes) the test.
        issue_values = {'ji': None, 'mr': mock_mr,
                        'test_failed': mock.Mock(return_value=True, spec=[])}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)
        issue_values['test_failed'].assert_called_once_with('CentOSZStream')

        # A JIRA Issue with a different component and CVEs passes the test.
        mock_mr.rh_branch = mock_branch2
        issue_values = {'ji': mock.Mock(), 'mr': mock_mr, 'ji_component': 'kernel-rt',
                        'ji_cves': ['CVE-1991-00001'], 'is_task': False}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

    def test_BranchMatches(self):
        """Passes if the MR target Branch matches the JIRA Issue's Branch."""
        this_test = rhissue_tests.BranchMatches
        mock_branch1 = mock.Mock(spec=['version'], version='9.0')
        mock_branch2 = mock.Mock(spec=['version'], version='9.4')

        # A JIRA Issue with an MR branch version >= 9.3 always passes the test
        mock_mr = mock.Mock(spec=['rh_branch'], rh_branch=mock_branch2)
        issue_values = {'ji_branch': mock_branch2, 'mr': mock_mr}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # A JIRA Issue with a ji_branch that matches the MR branch passes the test.
        mock_mr = mock.Mock(spec=['rh_branch'], rh_branch=mock_branch1)
        issue_values = {'ji_branch': mock_branch1, 'mr': mock_mr}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)

        # A JIRA Issue with a ji_branch that does not match the MR branch fails the test.
        issue_values = {'ji_branch': mock_branch2, 'mr': mock_mr}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test, expected_result=False,
                       expected_scope=rhissue_tests.MrScope.PLANNING)

        # A JIRA Issue that failed CentOSZStream or ComponentMatches skips (passes) the test.
        issue_values = {'ji_branch': mock_branch1, 'mr': mock_mr,
                        'test_failed': mock.Mock(return_value=True, spec=[])}
        fake_rhissue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_rhissue, test=this_test)
        issue_values['test_failed'].assert_called_once_with('TargetReleaseSet')

    def test_ComponentIsKernel(self) -> None:
        """Passes if the MR target issue component is kernel."""
        this_test = rhissue_tests.ComponentIsKernel
        issue_values = {'ji': mock.Mock(), 'ji_component': 'kernel-rt'}
        fake_issue = self._fake_rhissue(issue_values)
        self._run_test(rhissue=fake_issue, test=this_test)

    def test_CvePriority(self):
        """Passes if the CVE's lead clone is on errata."""
        this_test = rhissue_tests.CvePriority

        # Skips (passes) the test if the CVE Priority is less than MAJOR.
        cve_values = {'alias': 'FakeCve', 'ji_priority': rhissue_tests.JIPriority.NORMAL}
        fake_cve = self._fake_rhissue(cve_values)
        self._run_test(rhissue=fake_cve, test=this_test)

        projects = Projects(extra_projects_paths=['tests/assets/rh_projects_private.yaml'])
        project1 = projects.projects[12345]
        branch1 = project1.branches[4]   # '8.5', ZTR 8.5.0
        branch2 = project1.branches[2]   # '8.6', ZTR 8.6.0
        branch3 = project1.branches[0]   # 'main', ITR 8.7.0

        ji1 = mock.Mock()
        ji1.fields = mock.Mock()
        ji1.fields.components = ['kernel']
        rhissue1 = self._fake_rhissue({'ji': ji1, 'ji_branch': branch1, 'ji_project': project1,
                                       'ji_resolution': None})
        ji2 = mock.Mock()
        ji2.fields = mock.Mock()
        ji2.fields.components = ['kernel']
        rhissue2 = self._fake_rhissue({'ji': ji2, 'ji_branch': branch2, 'ji_project': project1,
                                       'ji_resolution': None})
        ji3 = mock.Mock()
        ji3.fields = mock.Mock()
        ji3.fields.components = ['kernel']
        rhissue3 = self._fake_rhissue({'ji': ji3, 'ji_branch': branch3, 'ji_project': project1,
                                       'ji_resolution': None})

        # Not rhel6 or 7 and the lead_clone Branch is not higher than the parent_clone, passes.
        cve_values = {'alias': 'FakeCve', 'ji_priority': rhissue_tests.JIPriority.MAJOR,
                      'ji_depends_on': [rhissue1, rhissue2, rhissue3],
                      'parent_mr': mock.Mock(spec=['branch'], branch=branch2)}
        fake_cve = self._fake_rhissue(cve_values)
        self._run_test(rhissue=fake_cve, test=this_test)

        # Not rhel6 or 7 and the lead_clone is higher than the parent and not on ERRATA, fails.
        cve_values = {'alias': 'FakeCve', 'ji_priority': rhissue_tests.JIPriority.MAJOR,
                      'ji_depends_on': [rhissue1, rhissue2, rhissue3],
                      'parent_mr': mock.Mock(spec=['branch'], branch=branch1)}
        fake_cve = self._fake_rhissue(cve_values)
        self._run_test(rhissue=fake_cve, test=this_test, expected_result=False)

        # Not rhel6 or 7 and the lead_clone is higher than the parent is on ERRATA, passes.
        rhissue2 = self._fake_rhissue({'ji': ji2, 'ji_branch': branch2, 'ji_project': project1,
                                       'ji_resolution': rhissue_tests.JIResolution.DONEERRATA})
        cve_values = {'alias': 'FakeCve', 'ji_priority': rhissue_tests.JIPriority.MAJOR,
                      'ji_depends_on': [rhissue1, rhissue2, rhissue3],
                      'parent_mr': mock.Mock(spec=['branch'], branch=branch1)}
        fake_cve = self._fake_rhissue(cve_values)
        self._run_test(rhissue=fake_cve, test=this_test)

        # rhel7 so the lead_clone branch targets 'main'.
        project1.__dict__['name'] = 'rhel-7'
        rhissue2 = self._fake_rhissue({'ji': ji2, 'ji_branch': branch2, 'ji_project': project1,
                                       'ji_resolution': None})
        rhissue3 = self._fake_rhissue({'ji': ji3, 'ji_branch': branch3, 'ji_project': project1,
                                       'ji_resolution': rhissue_tests.JIResolution.DONEERRATA})
        cve_values = {'alias': 'FakeCve', 'ji_priority': rhissue_tests.JIPriority.MAJOR,
                      'ji_depends_on': [rhissue1, rhissue2, rhissue3],
                      'parent_mr': mock.Mock(spec=['branch'], branch=branch1)}
        fake_cve = self._fake_rhissue(cve_values)
        self._run_test(rhissue=fake_cve, test=this_test)
        project1.__dict__['name'] = 'rhel-8'

    @mock.patch.dict(os.environ, {'CKI_DEPLOYMENT_ENVIRONMENT': 'staging'}, clear=True)
    def test_CvePriorityYStream(self):
        """Passes if the CVE's lead clone is on errata."""
        this_test = rhissue_tests.CvePriorityYStream

        projects = Projects(extra_projects_paths=['tests/assets/rh_projects_sandbox.yaml'])
        project1 = projects.projects[56789]
        branch1 = project1.branches[0]  # 'main', Y-stream
        branch2 = project1.branches[3]  # '8.6' , Z-stream
        # Skips (passes) the test if the CVE Priority is less than MAJOR.
        cve_values = {'alias': 'FakeCve', 'ji_priority': rhissue_tests.JIPriority.NORMAL}
        fake_cve = self._fake_rhissue(cve_values)
        self._run_test(rhissue=fake_cve, test=this_test)

        # Skips (passes) the test if the CVE is not for y-stream.
        cve_values = {'alias': 'FakeCve', 'ji_priority': rhissue_tests.JIPriority.MAJOR,
                      'ji_branch': branch2}
        fake_cve = self._fake_rhissue(cve_values)
        self._run_test(rhissue=fake_cve, test=this_test)

        # Issues
        ji1 = mock.Mock()
        ji1.fields = mock.Mock()
        ji1.fields.components = ['kernel']
        rhissue1 = self._fake_rhissue({'ji': ji1, 'ji_branch': branch1, 'ji_resolution': None})

        ji2 = mock.Mock()
        ji2.fields = mock.Mock()
        ji2.fields.components = ['kernel']
        rhissue2 = self._fake_rhissue({'ji': ji2, 'ji_branch': branch2,
                                       'ji_resolution': rhissue_tests.JIResolution.DONEERRATA})
        fake_cve = self._fake_rhissue(cve_values)

        cve_values = {'alias': 'FakeCve', 'ji_priority': rhissue_tests.JIPriority.MAJOR,
                      'ji_parents': [rhissue1, rhissue2], 'ji_branch': branch1,
                      'parent_mr': mock.Mock(spec=['branch'], branch=branch2)}
        fake_cve = self._fake_rhissue(cve_values)

        # Z-stream is merged, so pass testing
        self._run_test(rhissue=fake_cve, test=this_test)

        # Z-stream is in progress, so fail testing
        rhissue2.ji_resolution = rhissue_tests.JIResolution.UNKNOWN
        self._run_test(rhissue=fake_cve, test=this_test, expected_result=False)

    @mock.patch("webhook.rhissue_tests.fetch_issues")
    def test_CveKernelRTVariant(self, fetch_issues: mock.Mock) -> None:
        this_test = rhissue_tests.CveKernelRTVariant
        cve = 'CVE_-2024-12345'

        issue_values = {
            'ji': mock.Mock(),
            'ji_fix_version': defs.FixVersion('rhel-9.2.0.z'),
            'ji_cves': [cve],
            'ji_component': 'kernel',
            'ji_branch': mock.Mock(components=['kernel', 'kernel-rt']),
        }

        rhissue = self._fake_rhissue(issue_values)
        issue_values['ji_component'] = 'kernel-rt'
        fetch_issues.side_effect = [[self._fake_rhissue(issue_values)], []]

        with self.subTest("Issue has a kernel-rt variant"):
            self._run_test(rhissue=rhissue, test=this_test)

        with self.subTest("Issue doesn't has a kernel-rt variant"):
            self._run_test(rhissue=rhissue, test=this_test, expected_result=False)

        with self.subTest("Branch doesn't have kernel-rt component"):
            issue_values['ji_branch'].components = ['kernel']
            rhissue = self._fake_rhissue(issue_values)
            self._run_test(rhissue=rhissue, test=this_test)

    def test_JInotOnErrata(self):
        self.response_et_setup()
        this_test = rhissue_tests.JInotOnErrata

        # A JIRA issue that has already been added to an errata should fail.
        test_ji = self.make_jira_issue('RHEL-30580')
        test_rhissue = RHIssue.new_from_ji(ji=test_ji, mrs=[])
        self._run_test(rhissue=test_rhissue, test=this_test, expected_result=False)

        # A JIRA issue not added to errata should pass.
        test_ji = self.make_jira_issue('RHEL-101')
        test_rhissue = RHIssue.new_from_ji(ji=test_ji, mrs=[])
        self._run_test(rhissue=test_rhissue, test=this_test, expected_result=True)
