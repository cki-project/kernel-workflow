"""Tests for skeleton."""
from dataclasses import dataclass
import os
from unittest import mock

from tests import fakes
from tests.helpers import KwfTestCase
from webhook import defs
from webhook import skeleton
from webhook.graphql import GET_MR_DESCRIPTIONS_QUERY
from webhook.session import SessionRunner
from webhook.users import User


def create_skeleton_mr(mr_url) -> skeleton.SkeletonMR:
    """Return a fresh MR object."""
    test_session = SessionRunner.new('skeleton', args='')
    gl_instance = fakes.FakeGitLab()
    gl_instance.user = User(username='cki-kwf-bot')
    test_session.get_gl_instance = mock.Mock(return_value=gl_instance)

    return skeleton.SkeletonMR.new(test_session, mr_url)


@dataclass(repr=False)
class JsonSkeletonMR:
    """Simple collection of file names of json data and users to build skeleton.SkeletonMR."""

    mr_url: defs.GitlabURL
    details: str
    commits: str
    descriptions: str
    add_rest: bool = False


@mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml',
                                'OWNERS_YAML': 'tests/assets/owners-20240711.yaml'})
class SkeletonMRBuilder(KwfTestCase):
    """SkeletonMR builder helper functions that leverage and extend KwfTestCase helpers."""

    def populate_responses(self, json_skeleton_mr, reset_responses=False) -> None:
        """Populate Responses for SkeletonMR."""
        # Make sure we're starting with an empty set of responses, if requested
        if reset_responses:
            self.responses.reset()

        # Standard user authentication
        self.response_gl_auth()
        # Standard GraphQL user data response
        self.response_gql_user_data()

        mr_endpoint = ''
        # Set up the skeleton.SkeletonMR query responses.
        variables = {'namespace': json_skeleton_mr.mr_url.namespace,
                     'mr_id': str(json_skeleton_mr.mr_url.id)}

        details = self.load_yaml_asset(
            path=json_skeleton_mr.details,
            sub_module='gitlab_graphql_api'
        )
        self.add_query(skeleton.SkeletonMR.MR_QUERY, variables=variables, query_result=details)

        commits = self.load_yaml_asset(
            path=json_skeleton_mr.commits,
            sub_module='gitlab_graphql_api'
        )
        self.add_query(skeleton.SkeletonMR.COMMIT_QUERY, variables=variables, query_result=commits)

        descriptions = self.load_yaml_asset(
            path=json_skeleton_mr.descriptions,
            sub_module='gitlab_graphql_api'
        )
        variables = {'namespace': json_skeleton_mr.mr_url.namespace,
                     'mr_ids': [str(json_skeleton_mr.mr_url.id)]}
        self.add_query(GET_MR_DESCRIPTIONS_QUERY, variables=variables, query_result=descriptions)

        if json_skeleton_mr.add_rest:
            proj_name = json_skeleton_mr.mr_url.namespace.split('/')[-1]
            project = self.load_yaml_asset(
                path=f'project-{proj_name}.json',
                sub_module='gitlab_rest_api'
            )

            proj_id = details['data']['project']['mr']['project']['id'].split('/')[-1]
            proj_endpoint = "https://gitlab.com/api/v4/projects"
            proj_path = json_skeleton_mr.mr_url.namespace.replace("/", "%2F")

            self.responses.get(f'{proj_endpoint}/{proj_id}', json=project)
            self.responses.get(f'{proj_endpoint}/{proj_path}', json=project)

            merge_request = self.load_yaml_asset(
                path=f'mr-{json_skeleton_mr.mr_url.id}-{proj_name}.json',
                sub_module='gitlab_rest_api'
            )
            mr_endpoint = f'{proj_endpoint}/{proj_id}/merge_requests/{json_skeleton_mr.mr_url.id}'
            self.responses.get(mr_endpoint, json=merge_request)

        self.response_et_setup()

    def build_cs9_mr(self, mr_id):
        """Build cs9 MR for SkeletonMR."""
        mr_url = defs.GitlabURL('https://gitlab.com/redhat/centos-stream/src/kernel/'
                                f'centos-stream-9/-/merge_requests/{mr_id}')
        proj_name = mr_url.namespace.split('/')[-1]

        details = f'jirahookmr_{proj_name}_{mr_id}-details.json'
        commits = f'jirahookmr_{proj_name}_{mr_id}-commits.json'
        descriptions = f'jirahookmr_{proj_name}_{mr_id}-descriptions.json'

        json_skeleton_mr = JsonSkeletonMR(mr_url=mr_url, details=details, commits=commits,
                                          descriptions=descriptions)
        self.populate_responses(json_skeleton_mr)

        return create_skeleton_mr(mr_url)


@mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml',
                                'OWNERS_YAML': 'tests/assets/owners-20240711.yaml'})
class TestProcessMR(KwfTestCase):
    """Tests for process_mr function."""

    @mock.patch('webhook.skeleton.SkeletonMR.update_statuses')
    @mock.patch('webhook.skeleton.SkeletonMR.new')
    def test_process_mr(self, mock_MR, mock_update):
        """Does not update MR labels but leaves a comment."""
        mock_args = mock.Mock()
        mock_session = SessionRunner.new('skeleton', mock_args, skeleton.HANDLERS)
        username = 'cki-kwf-bot'
        namespace = 'redhat/rhel/src/kernel/rhel-7'
        mr_id = 123
        mr_url = defs.GitlabURL(f'https://gitlab.com/{namespace}/-/merge_requests/{mr_id}')

        mock_session.get_graphql = mock.Mock()
        mock_session.get_graphql.return_value.username = username
        mock_session.is_production_or_staging = True
        mock_mr = mock.Mock(commits=True, labels=[])
        mock_mr.update_statuses = mock_update
        mock_mr.gl_mr.milestone = {'description': "RHEL-55.3"}
        mock_mr.gl_mr.title = "Title of record"
        mock_mr.rh_project = "Foo"
        mock_mr.rh_branch = "Bar"
        mock_mr.commit_count = 1
        mock_mr.files = ['foo/bar/blah.c']
        mock_MR.return_value = mock_mr
        mock_gl = fakes.FakeGitLab()
        mock_project = mock_gl.add_project(45678, namespace)
        mock_project.add_mr(mr_id)
        mock_session.get_gl_instance = mock.Mock(return_value=mock_gl)
        mock_session.rh_projects = mock.Mock()

        expected = "Product: RHEL-55.3\nRed Hat Project: Foo\nRed Hat Branch: Bar\n"
        expected += "MR Title: Title of record\nMR Commit Count: 1\n"
        expected += "MR Files touched: ['foo/bar/blah.c']\nMR Commit data: True\n"

        skeleton.process_mr(mock_session, mr_url)
        mock_update.assert_called_once_with([], [], expected)


@mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml',
                                'OWNERS_YAML': 'tests/assets/owners-20240711.yaml'})
class TestMisc(SkeletonMRBuilder):
    """Tests for misc functions."""

    @mock.patch('webhook.session.get_gl_instance')
    @mock.patch('webhook.skeleton.SkeletonMR.remove_labels')
    @mock.patch('webhook.skeleton.SkeletonMR.add_labels')
    @mock.patch('webhook.session.SessionRunner.update_webhook_comment')
    def test_update_statuses(self, mock_update_comment, mock_add_label,
                             mock_remove_labels, mock_get_gl_instance):
        """Test updating labels and hook comment."""
        skeleton_mr = self.build_cs9_mr(4809)
        skeleton_mr._labels = defs.READY_FOR_QA_DEPS + ["readyForQA"]

        skeleton_mr.update_statuses(["readyForMerge"], ["readyForQA"], "Say something")
        mock_add_label.assert_called_with(["readyForMerge"])
        mock_remove_labels.assert_called_with(["readyForQA"])
        mock_update_comment.assert_called_with(mock.ANY, "Say something", bot_name='cki-kwf-bot')


class TestMain(KwfTestCase):
    """Tests for the various helper functions."""

    @mock.patch.dict(os.environ, {'GL_PROJECTS': 'cki-project/kernel-ark',
                                  'REQUESTS_CA_BUNDLE': '/etc/pki/certs/whatever.ca'})
    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_get_parser_args(self):
        args = ['--rabbitmq-routing-key', 'RABBITMQ_ROUTING_KEY']
        skeleton.main(args)
