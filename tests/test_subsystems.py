"""Webhook interaction tests."""
from copy import deepcopy
from unittest import mock

from cki_lib import yaml
from cki_lib.owners import Parser
from responses.matchers import json_params_matcher

from tests import fake_payloads
from tests import fakes
from tests.helpers import KwfTestCase
from tests.test_base_mr_mixins import OWNERS_ENTRY_1
from tests.test_base_mr_mixins import OWNERS_ENTRY_2
from tests.test_base_mr_mixins import OWNERS_HEADER
from webhook import defs
from webhook import graphql
from webhook import subsystems
from webhook.common import get_arg_parser
from webhook.session import SessionRunner
from webhook.session_events import GitlabMREvent

# Expected values for a "bare" SubsysMR.
DEFAULT_SUBSYS_ATTRS = {'current_ss_names': set(),
                        'expected_ss_names': set(),
                        'stale_ss_names': set(),
                        'expected_ss_labels': [],
                        'current_ci_labels': set(),
                        'current_ci_names': set(),
                        'expected_ci_names': set(),
                        'missing_ci_labels': set(),
                        'current_ci_scope': None,
                        'expected_ci_scope': defs.MrScope.OK,
                        'expected_ci_labels': []}


@mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml'})
class TestSubsysMR(KwfTestCase):
    """Tests for the SubsysMR dataclass."""

    @classmethod
    def setUpClass(cls):
        """Confirms the DEFAULT_SUBSYS_ATTRS dict keys match the module properties."""
        subsys_attributes = {key for key in subsystems.SubsysMR.__dict__ if not key.startswith('_')}
        dict_keys = set(DEFAULT_SUBSYS_ATTRS.keys())
        if subsys_attributes != dict_keys:
            err = 'DEFAULT_SUBSYS_ATTRS keys do not match SubsysMR attributes'
            raise AttributeError(f'{err}: {subsys_attributes}')

    def setUp(self) -> None:
        """Patch out the owners parser so we can control what it returns."""
        super().setUp()
        mock_owners = mock.patch(
            'webhook.session.BaseSession.owners', new_callable=mock.PropertyMock
        )
        self.addCleanup(mock_owners.stop)
        self.mock_owners = mock_owners.start()
        mock_get_gl_instance = mock.patch('webhook.session.get_gl_instance')
        self.addCleanup(mock_get_gl_instance.stop)
        self.mock_get_gl_instance = mock_get_gl_instance.start()

    def _test_subsys_mr(self, response, parser_entries, expected_values):
        """Create and test a SubsysMR."""
        mr_id = fake_payloads.MR_IID
        namespace = fake_payloads.PROJECT_PATH_WITH_NAMESPACE
        # Code never changes here.
        mock_gl_instance = fakes.FakeGitLab()
        mock_gl_project = mock_gl_instance.add_project(fake_payloads.PROJECT_ID, namespace)
        mock_gl_project.add_mr(mr_id)
        self.mock_get_gl_instance.return_value = mock_gl_instance

        # Set up the owners Parser data.
        self.mock_owners.return_value = Parser(yaml.load(
            contents=OWNERS_HEADER + parser_entries))

        test_session = SessionRunner.new('subsystems', 'args')

        # Set up the login response.
        self.responses.post(
            self.GITLAB_GRAPHQL,
            json={'data': {'currentUser': fake_payloads.BASE_MR_RESPONSE['currentUser']}},
            match=[json_params_matcher(
                {'query': graphql.GET_USER_DETAILS_QUERY.strip('\n')}
            )]
        )

        # Set up the query response.
        matcher_params = {'variables': {'mr_id': str(mr_id), 'namespace': namespace}}
        self.responses.post(
            self.GITLAB_GRAPHQL,
            json={'data': response},
            match=[json_params_matcher(matcher_params, strict_match=False)]
        )

        # Set up the expected SubsysMR attribute values.
        subsys_attrs = deepcopy(DEFAULT_SUBSYS_ATTRS)
        if expected_values:
            # Don't accept anything that isn't already there.
            if key_diff := set(expected_values.keys()).difference(subsys_attrs):
                raise KeyError(f'expected_values has keys not in DEFAULT_SUBSYS_ATTRS: {key_diff}')
            subsys_attrs.update(expected_values)

        # Set up and create the SubsysMR.
        self.response_gql_user_data()
        subsys_mr = subsystems.SubsysMR.new(
            test_session, fake_payloads.MR_URL, source_path='test_path'
        )

        # Do the tests.
        for key, value in subsys_attrs.items():
            with self.subTest(key=key, value=value):
                self.clear_caches()
                test = self.assertCountEqual if isinstance(value, list) else self.assertEqual
                test(getattr(subsys_mr, key), value)

    def test_no_ci_or_ss(self):
        """Returns all default values due to no matching files."""
        response = deepcopy(fake_payloads.BASE_MR_RESPONSE)
        response['project']['mr']['files'] = []
        self._test_subsys_mr(response=response, parser_entries=OWNERS_ENTRY_1, expected_values=None)

    def test_base_mr_response(self):
        """Returns a SubsysMR with expected attributes for the BASE_MR_RESPONSE."""
        response = deepcopy(fake_payloads.BASE_MR_RESPONSE)
        entries = OWNERS_ENTRY_1 + OWNERS_ENTRY_2
        values = {'expected_ss_names': {'redhat'},
                  'expected_ci_names': {'testDep'},
                  'missing_ci_labels': {'ExternalCI::testDep::NeedsTesting'},
                  'expected_ss_labels': ['Subsystem:redhat'],
                  'expected_ci_labels': ['ExternalCI::testDep::NeedsTesting'],
                  'expected_ci_scope': defs.MrScope.READY_FOR_QA}
        self._test_subsys_mr(response=response, parser_entries=entries, expected_values=values)

    def test_draft_response(self):
        """Returns a SubsysMR without any ExpectedCI labels since this is a Draft."""
        mr_labels = ['ExternalCI::testDep::NeedsTesting',
                     'ExternalCI::old_test::Waived',
                     'ExternalCI::NeedsTesting']
        response = deepcopy(fake_payloads.BASE_MR_RESPONSE)
        response['project']['mr']['draft'] = True
        response['project']['mr']['labels']['nodes'] = [{'title': name} for name in mr_labels]
        entries = OWNERS_ENTRY_1 + OWNERS_ENTRY_2
        values = {'current_ci_labels': {'ExternalCI::testDep::NeedsTesting',
                                        'ExternalCI::old_test::Waived'},
                  'current_ci_names': {'testDep', 'old_test'},
                  'expected_ss_names': {'redhat'},
                  'expected_ss_labels': ['Subsystem:redhat'],
                  'current_ci_scope': defs.MrScope.NEEDS_TESTING,
                  'expected_ci_scope': defs.MrScope.OK}
        self._test_subsys_mr(response=response, parser_entries=entries, expected_values=values)

    def test_custom_response(self):
        """Returns a SubsysMR with expected attributes set."""
        mr_files = ['redhat/Makefile',
                    'includes/net/ipv8.c',
                    'drivers/scsi/scsi.h']
        mr_labels = ['ExternalCI::x86_testing::NeedsTesting',
                     'ExternalCI::scsi_testing::OK',
                     'ExternalCI::NeedsTesting',
                     'Subsystem:scsi',
                     'Subsystem:x86']
        response = deepcopy(fake_payloads.BASE_MR_RESPONSE)
        response['project']['mr']['files'] = [{'path': name} for name in mr_files]
        response['project']['mr']['labels']['nodes'] = [{'title': name} for name in mr_labels]

        entry1 = (' - subsystem: Net\n'
                  '   labels:\n'
                  '    name: net\n'
                  '    readyForMergeDeps:\n'
                  '     - net_testing\n'
                  '   paths:\n'
                  '    includes:\n'
                  '     - includes/net/\n')
        entry2 = (' - subsystem: SCSI\n'
                  '   labels:\n'
                  '    name: scsi\n'
                  '    readyForMergeDeps:\n'
                  '     - scsi_testing\n'
                  '     - hdd_tests\n'
                  '   paths:\n'
                  '    includes:\n'
                  '     - drivers/scsi/\n')
        entry3 = (' - subsystem: redhat\n'
                  '   labels:\n'
                  '    name: redhat\n'
                  '   paths:\n'
                  '    includes:\n'
                  '     - redhat/\n')
        entries = entry1 + entry2 + entry3
        values = {'current_ss_names': {'scsi', 'x86'},
                  'expected_ss_names': {'net', 'scsi', 'redhat'},
                  'stale_ss_names': {'x86'},
                  'expected_ss_labels': ['Subsystem:net', 'Subsystem:scsi', 'Subsystem:redhat'],
                  'expected_ci_labels': ['ExternalCI::net_testing::NeedsTesting',
                                         'ExternalCI::x86_testing::NeedsTesting',
                                         'ExternalCI::scsi_testing::OK',
                                         'ExternalCI::hdd_tests::NeedsTesting'],
                  'missing_ci_labels': {'ExternalCI::net_testing::NeedsTesting',
                                        'ExternalCI::hdd_tests::NeedsTesting'},
                  'expected_ci_names': {'net_testing', 'scsi_testing', 'hdd_tests'},
                  'current_ci_labels': {'ExternalCI::x86_testing::NeedsTesting',
                                        'ExternalCI::scsi_testing::OK'},
                  'current_ci_names': {'x86_testing', 'scsi_testing'},
                  'current_ci_scope': defs.MrScope.NEEDS_TESTING,
                  'expected_ci_scope': defs.MrScope.NEEDS_TESTING}
        self._test_subsys_mr(response=response, parser_entries=entries, expected_values=values)


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestHelpers(KwfTestCase):
    """Tests for the subsystems functions."""

    def test_update_mr_non_draft(self):
        """Updates the MR with the labels computed by the SubsysMR."""
        mr_attrs = {'url': 'https://gitlab.com/group/project/-/merge_request/123',
                    'all_files': {'Makefile'},
                    'draft': False,
                    'labels': [defs.Label('Subsystem:redhat'),
                               defs.Label('ExternalCI::abc::OK')],
                    'owners_subsystems': [],
                    'expected_ss_names': ['net', 'x86', 'cpu'],
                    'stale_ss_names': ['redhat'],
                    'current_ci_labels': [defs.Label('ExternalCI::abc::OK')],
                    'expected_ss_labels': [defs.Label('Subsystem:net'), defs.Label('Subsystem:x86'),
                                           defs.Label('Subsystem:cpu')],
                    'expected_ci_labels': [defs.Label('ExternalCI::team1::NeedsTesting')],
                    'current_ci_scope': defs.MrScope.OK,
                    'expected_ci_scope': defs.MrScope.NEEDS_TESTING,
                    'add_labels': mock.Mock(),
                    'remove_labels': mock.Mock()
                    }

        subsys_mr = mock.Mock(spec=list(mr_attrs.keys()), **mr_attrs)

        subsystems.update_mr(subsys_mr)
        remove_labels_calls = [mock.call(['Subsystem:redhat'])]
        self.assertEqual(subsys_mr.remove_labels.mock_calls, remove_labels_calls)
        subsys_mr.add_labels.assert_called_once_with({'ExternalCI::NeedsTesting',
                                                      'ExternalCI::team1::NeedsTesting',
                                                      'Subsystem:x86',
                                                      'Subsystem:net',
                                                      'Subsystem:cpu'},
                                                     remove_scoped=False)

    def test_update_mr_draft(self):
        """Updates the MR with the labels computed by the SubsysMR."""
        mr_attrs = {'url': 'https://gitlab.com/group/project/-/merge_request/123',
                    'all_files': {'Makefile'},
                    'draft': True,
                    'labels': [defs.Label('Subsystem:redhat'), defs.Label('ExternalCI::Waived')],
                    'owners_subsystems': [],
                    'expected_ss_names': ['net', 'x86', 'cpu'],
                    'stale_ss_names': ['redhat'],
                    'current_ci_labels': [],
                    'expected_ss_labels': [defs.Label('Subsystem:net'), defs.Label('Subsystem:x86'),
                                           defs.Label('Subsystem:cpu')],
                    'expected_ci_labels': [],
                    'current_ci_scope': defs.MrScope.WAIVED,
                    'expected_ci_scope': defs.MrScope.OK,
                    'add_labels': mock.Mock(),
                    'remove_labels': mock.Mock()
                    }
        subsys_mr = mock.Mock(spec=list(mr_attrs.keys()), **mr_attrs)

        subsystems.update_mr(subsys_mr)
        remove_labels_calls = [mock.call(['Subsystem:redhat'])]
        self.assertEqual(subsys_mr.remove_labels.mock_calls, remove_labels_calls)
        subsys_mr.add_labels.assert_called_once_with({'Subsystem:x86',
                                                      'Subsystem:net',
                                                      'Subsystem:cpu'},
                                                     remove_scoped=False)

    @mock.patch('webhook.subsystems.update_mr')
    @mock.patch('webhook.subsystems.SubsysMR', mock.Mock())
    def test_process_gl_event(self, mock_update_mr):
        """Updates the MR/notifies users as needed."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args(
            '--owners-yaml owners_path, --repo-path src'.split()
        )

        test_session = SessionRunner.new('subsystems', args, subsystems.HANDLERS)
        test_session.get_gl_instance = mock.Mock()
        test_session.get_graphql = mock.Mock()

        payload = deepcopy(fake_payloads.MR_PAYLOAD)
        changes = {'draft': {'previous': True, 'current': False}}
        payload['changes'] = changes
        test_event = GitlabMREvent(test_session, {}, payload)

        subsystems.process_gl_event(payload, test_session, test_event)
        mock_update_mr.assert_called_once()
