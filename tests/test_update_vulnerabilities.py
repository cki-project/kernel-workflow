"""Tests for update_vulnerabilities."""
from unittest import mock

from tests.helpers import KwfTestCase
from webhook.utils import update_vulnerabilities


@mock.patch.dict('os.environ',
                 {'VULNS_PATH': '/path/to/vulns',
                  'VULNS_URL': 'git://git.kernel.org/pub/scm/linux/security/vulns.git',
                  'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml',
                  'REQUESTS_CA_BUNDLE': '/path/to/certs'
                  }
                 )
class TestUpdateVulnerabilities(KwfTestCase):
    """Tests for the various update_vulnerabilities functions."""

    @mock.patch('os.makedirs', mock.Mock())
    @mock.patch('subprocess.run', mock.Mock())
    def test_VulnRepoManager(self):
        mock_args = mock.Mock()
        mock_args.vulns_path = '/path/to/vulns'
        mock_args.vulns_url = 'git://git.kernel.org/pub/scm/linux/security/vulns.git'

        test_mgr = update_vulnerabilities.VulnRepoManager(mock_args)
        self.assertEqual(test_mgr.repo_path, mock_args.vulns_path)
        self.assertEqual(test_mgr.remote_url, mock_args.vulns_url)

        with self.assertLogs('cki.webhook.utils.update_vulnerabilities', level='DEBUG') as logs:
            test_mgr.clone()
            self.assertIn(f'Running command: git clone {mock_args.vulns_url}', logs.output[-1])

            test_mgr.fetch()
            self.assertIn('Running command: git fetch origin', logs.output[-1])

            test_mgr.checkout()
            self.assertIn('Running command: git reset --hard origin/master', logs.output[-1])

            test_mgr.garbage_collection()
            self.assertIn('Running command: git gc --auto', logs.output[-2])
            self.assertIn('Running command: git repack --geometric 2 -d', logs.output[-1])

    def test_get_parser_args(self):
        args = ["--vulns-path", "/data/vulns", "--vulns-url", "git://foo.bar/vunls.git",
                "--mode", "update-repo"]
        pargs = update_vulnerabilities._get_parser_args(args)
        self.assertEqual(pargs.vulns_path, '/data/vulns')
        self.assertEqual(pargs.vulns_url, 'git://foo.bar/vunls.git')
        self.assertEqual(pargs.mode, 'update-repo')
        self.assertEqual(pargs.sentry_ca_certs, '/path/to/certs')

        args = ["--mode", "create-repo"]
        pargs = update_vulnerabilities._get_parser_args(args)
        self.assertEqual(pargs.vulns_path, '/path/to/vulns')
        self.assertEqual(pargs.vulns_url, 'git://git.kernel.org/pub/scm/linux/security/vulns.git')
        self.assertEqual(pargs.mode, 'create-repo')
        self.assertEqual(pargs.sentry_ca_certs, '/path/to/certs')

    @mock.patch('cki_lib.misc.sentry_init', mock.Mock())
    @mock.patch('webhook.utils.update_vulnerabilities.VulnRepoManager')
    def test_main(self, mock_vr_mgr):
        arglist = ['--mode', 'create-repo']
        mock_mgr = mock_vr_mgr.return_value
        update_vulnerabilities.main(arglist)
        mock_mgr.clone.assert_called_once()
        mock_mgr.fetch.assert_not_called()
        mock_mgr.checkout.assert_not_called()

        mock_mgr.reset_mock()
        arglist = ['--mode', 'update-repo']
        update_vulnerabilities.main(arglist)
        mock_mgr.clone.assert_not_called()
        mock_mgr.fetch.assert_called_once()
        mock_mgr.checkout.assert_called_once()
