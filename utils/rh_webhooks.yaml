---
# Webhook anchors
webhooks:
  ack_nack:
    name: "ack_nack"
    enabled_by_default: true
    required_label_prefixes: ["Acks"]
    extra_labels_regex: "CodeChanged::v\\d+"
    required_for_qa_scope: "OK"
    comment_header: "ACK/NACK Summary"
    source_code: https://gitlab.com/cki-project/kernel-workflow/-/blob/main/webhook/ack_nack.py
    faq_name: 'KWF FAQ'
    faq_url: 'https://red.ht/kernel_workflow_doc'
    documentation_url: 'https://gitlab.com/cki-project/kernel-workflow/-/blob/main/docs/README.ack_nack.md'
    slack_name: '#team-kernel-workflow'
    slack_url: 'https://redhat-internal.slack.com/archives/C04LRUPMJQ5'
    new_issue_url: 'https://gitlab.com/cki-project/kernel-workflow/-/issues/new?issue[title]=ack_nack webhook issue'
    request_evaluation_triggers: ["ack-nack", "acks"]
    run_on_drafts: true
    run_on_changed_approvals: true
    run_on_changed_commits: true
    run_on_changed_description: false
    run_on_changed_to_draft: true
    run_on_changed_to_ready: true
    run_on_closing: false
    note_text_patterns: ["^/(?P<action>(un)*block).*$"]
  backporter:
    name: "backporter"
    enabled_by_default: false
    required_label_prefixes: []
    required: false
    source_code: https://gitlab.com/cki-project/kernel-workflow/-/blob/main/webhook/utils/backporter.py
    faq_name: 'KWF FAQ'
    faq_url: 'https://red.ht/kernel_workflow_doc'
    documentation_url: 'https://gitlab.com/cki-project/kernel-workflow/-/blob/main/docs/README.backporter.md'
    slack_name: '#team-kernel-workflow'
    slack_url: 'https://redhat-internal.slack.com/archives/C04LRUPMJQ5'
    new_issue_url: 'https://gitlab.com/cki-project/kernel-workflow/-/issues/new?issue[title]=backporter webhook issue'
  buglinker:
    name: "buglinker"
    enabled_by_default: true
    required_label_prefixes: []
    extra_labels_regex: "TargetedTestingMissing"
    required: false
    source_code: https://gitlab.com/cki-project/kernel-workflow/-/blob/main/webhook/buglinker.py
    faq_name: 'KWF FAQ'
    faq_url: 'https://red.ht/kernel_workflow_doc'
    documentation_url: 'https://gitlab.com/cki-project/kernel-workflow/-/blob/main/docs/README.buglinker.md'
    slack_name: '#team-kernel-workflow'
    slack_url: 'https://redhat-internal.slack.com/archives/C04LRUPMJQ5'
    new_issue_url: 'https://gitlab.com/cki-project/kernel-workflow/-/issues/new?issue[title]=buglinker webhook issue'
    request_evaluation_triggers: ["buglinker"]
    run_on_build_downstream: true
    run_on_pipeline_downstream: true
    run_on_drafts: false
    run_on_changed_commits: false
    run_on_changed_description: true
    run_on_changed_to_draft: true
    run_on_changed_to_ready: true
    run_on_closing: true
    run_on_oversize_mrs: true
  ckihook:
    name: "ckihook"
    enabled_by_default: true
    required_label_prefixes: ["CKI"]
    extra_labels_regex: "CKI_.+::.+"
    comment_header: "CKI Pipelines Status"
    source_code: https://gitlab.com/cki-project/kernel-workflow/-/blob/main/webhook/ckihook.py
    faq_name: 'CKI FAQ'
    faq_url: 'https://cki-project.org/l/devel-faq'
    documentation_url: 'https://gitlab.com/cki-project/kernel-workflow/-/blob/main/docs/README.ckihook.md'
    slack_name: '#team-kernel-cki'
    slack_url: 'https://redhat-internal.slack.com/archives/C04KPCFGDTN'
    new_issue_url: 'https://gitlab.com/cki-project/infrastructure/-/issues/new?issue[title]=ckihook webhook issue'
    request_evaluation_triggers: ["cki", "ckihook", "pipeline"]
    run_on_build_downstream: true
    run_on_pipeline_downstream: true
    run_on_drafts: true
    run_on_changed_commits: true
    run_on_changed_description: false
    run_on_changed_to_draft: true
    run_on_changed_to_ready: true
    run_on_closing: false
    manage_special_labels: true
  commit_compare:
    name: "commit_compare"
    enabled_by_default: true
    required_label_prefixes: ["CommitRefs"]
    extra_labels_regex: "(KABI|Dependencies::.+)"
    required_for_qa_scope: "OK"
    comment_header: "Upstream Commit ID Readiness"
    source_code: https://gitlab.com/cki-project/kernel-workflow/-/blob/main/webhook/commit_compare.py
    faq_name: 'KWF FAQ'
    faq_url: 'https://red.ht/kernel_workflow_doc'
    documentation_url: 'https://gitlab.com/cki-project/kernel-workflow/-/blob/main/docs/README.commit_compare.md'
    slack_name: '#team-kernel-workflow'
    slack_url: 'https://redhat-internal.slack.com/archives/C04LRUPMJQ5'
    new_issue_url: 'https://gitlab.com/cki-project/kernel-workflow/-/issues/new?issue[title]=commit_compare webhook issue'
    request_evaluation_triggers: ["commit", "commit-id", "commit-compare"]
    run_on_drafts: true
    run_on_changed_commits: true
    run_on_changed_description: false
    run_on_changed_to_draft: true
    run_on_changed_to_ready: true
    run_on_closing: false
  configshook:
    name: "configshook"
    enabled_by_default: true
    required_label_prefixes: ["Configs"]
    extra_labels_regex: "Configuration"
    required_for_qa_scope: "OK"
    comment_header: "Kernel Configuration Evaluation"
    source_code: https://gitlab.com/cki-project/kernel-workflow/-/blob/main/webhook/configshook.py
    faq_name: 'KWF FAQ'
    faq_url: 'https://red.ht/kernel_workflow_doc'
    documentation_url: 'https://gitlab.com/cki-project/kernel-workflow/-/blob/main/docs/README.configshook.md'
    slack_name: '#team-kernel-workflow'
    slack_url: 'https://redhat-internal.slack.com/archives/C04LRUPMJQ5'
    new_issue_url: 'https://gitlab.com/cki-project/kernel-workflow/-/issues/new?issue[title]=configshook webhook issue'
    request_evaluation_triggers: ["configs", "configshook"]
    run_on_drafts: true
    run_on_changed_commits: true
    run_on_changed_description: false
    run_on_changed_to_draft: false
    run_on_changed_to_ready: false
    run_on_closing: false
  limited_ci:
    name: "limited_ci"
    enabled_by_default: false
    required_label_prefixes: ["Limited CI"]
    extra_labels_regex: "External Contribution"
    source_code: https://gitlab.com/cki-project/kernel-workflow/-/blob/main/webhook/limited_ci.py
    faq_name: 'CKI FAQ'
    faq_url: 'https://cki-project.org/l/devel-faq'
    documentation_url: 'https://gitlab.com/cki-project/kernel-workflow/-/blob/main/docs/README.limited_ci.md'
    slack_name: '#team-kernel-cki'
    slack_url: 'https://redhat-internal.slack.com/archives/C04KPCFGDTN'
    new_issue_url: 'https://gitlab.com/cki-project/infrastructure/-/issues/new?issue[title]=limited_ci webhook issue'
    request_evaluation_triggers: []
    run_on_pipeline_downstream: true
    run_on_pipeline_upstream: true
    run_on_drafts: true
    run_on_changed_commits: true
    run_on_changed_description: false
    run_on_changed_to_draft: false
    run_on_changed_to_ready: false
    run_on_closing: false
  elnhook:
    name: "elnhook"
    enabled_by_default: false
    source_code: https://gitlab.com/cki-project/kernel-workflow/-/blob/main/webhook/elnhook.py
    match_to_projects: false
    run_on_changed_commits: false
    run_on_changed_description: false
    run_on_changed_to_ready: false
  fixes:
    name: "fixes"
    enabled_by_default: true
    required_label_prefixes: ["Fixes"]
    required_for_qa_scope: "OK"
    comment_header: "Fixes Status"
    source_code: https://gitlab.com/cki-project/kernel-workflow/-/blob/main/webhook/fixes.py
    faq_name: 'KWF FAQ'
    faq_url: 'https://red.ht/kernel_workflow_doc'
    documentation_url: 'https://gitlab.com/cki-project/kernel-workflow/-/blob/main/docs/README.fixes.md'
    slack_name: '#team-kernel-workflow'
    slack_url: 'https://redhat-internal.slack.com/archives/C04LRUPMJQ5'
    new_issue_url: 'https://gitlab.com/cki-project/kernel-workflow/-/issues/new?issue[title]=fixes webhook issue'
    request_evaluation_triggers: ["fixes"]
    run_on_drafts: true
    run_on_changed_commits: true
    run_on_changed_description: true
    run_on_changed_to_draft: true
    run_on_changed_to_ready: true
    run_on_closing: false
  jirahook:
    name: "jirahook"
    enabled_by_default: true
    required_label_prefixes: ["JIRA"]
    extra_labels_regex: "kernel-.+ testing::.+"
    required_for_qa_scope: "InProgress"
    comment_header: "JIRA Hook Readiness Report"
    source_code: https://gitlab.com/cki-project/kernel-workflow/-/blob/main/webhook/jirahook.py
    faq_name: 'KWF FAQ'
    faq_url: 'https://red.ht/kernel_workflow_doc'
    documentation_url: 'https://gitlab.com/cki-project/kernel-workflow/-/blob/main/docs/README.jirahook.md'
    slack_name: '#team-kernel-workflow'
    slack_url: 'https://redhat-internal.slack.com/archives/C04LRUPMJQ5'
    new_issue_url: 'https://gitlab.com/cki-project/kernel-workflow/-/issues/new?issue[title]=jirahook webhook issue'
    request_evaluation_triggers: ["jira", "jirahook"]
    run_on_drafts: true
    run_on_changed_commits: true
    run_on_changed_description: true
    run_on_changed_to_draft: true
    run_on_changed_to_ready: true
    run_on_closing: true
  skeleton:
    name: "skeleton"
    enabled_by_default: true
    required_label_prefixes: []
    required: false
    comment_header: "Skeleton Hook Comments"
    source_code: https://gitlab.com/cki-project/kernel-workflow/-/blob/main/webhook/skeleton.py
    faq_name: 'KWF FAQ'
    faq_url: 'https://red.ht/kernel_workflow_doc'
    documentation_url: 'https://gitlab.com/cki-project/kernel-workflow/-/blob/main/docs/README.skeleton.md'
    slack_name: '#team-kernel-workflow'
    slack_url: 'https://redhat-internal.slack.com/archives/C04LRUPMJQ5'
    new_issue_url: 'https://gitlab.com/cki-project/kernel-workflow/-/issues/new?issue[title]=skeleton webhook issue'
  mergehook:
    name: "mergehook"
    enabled_by_default: true
    required_label_prefixes: ["Merge"]
    required_for_qa_scope: "OK"
    comment_header: "Mergeability Summary"
    source_code: https://gitlab.com/cki-project/kernel-workflow/-/blob/main/webhook/mergehook.py
    faq_name: 'KWF FAQ'
    faq_url: 'https://red.ht/kernel_workflow_doc'
    documentation_url: 'https://gitlab.com/cki-project/kernel-workflow/-/blob/main/docs/README.mergehook.md'
    slack_name: '#team-kernel-workflow'
    slack_url: 'https://redhat-internal.slack.com/archives/C04LRUPMJQ5'
    new_issue_url: 'https://gitlab.com/cki-project/kernel-workflow/-/issues/new?issue[title]=mergehook webhook issue'
    request_evaluation_triggers: ["merge", "mergehook"]
    run_on_drafts: true
    run_on_changed_commits: true
    run_on_changed_description: true
    run_on_changed_to_draft: true
    run_on_changed_to_ready: true
    run_on_closing: false
  owners_validator:
    name: "owners_validator"
    enabled_by_default: false
    required_label_prefixes: []
    required: false
    source_code: https://gitlab.com/cki-project/kernel-workflow/-/blob/main/webhook/utils/owners_validator.py
    match_to_projects: false
    faq_name: 'KWF FAQ'
    faq_url: 'https://red.ht/kernel_workflow_doc'
    slack_name: '#team-kernel-workflow'
    slack_url: 'https://redhat-internal.slack.com/archives/C04LRUPMJQ5'
    new_issue_url: 'https://gitlab.com/cki-project/kernel-workflow/-/issues/new?issue[title]=owners_validator issue'
  report_generator:
    name: "report_generator"
    enabled_by_default: false
    required_label_prefixes: []
    required: false
    source_code: https://gitlab.com/cki-project/kernel-workflow/-/blob/main/webhook/utils/report_generator.py
  sast:
    name: "sast"
    enabled_by_default: false
    required: false
    required_label_prefixes: ["SAST"]
    required_for_qa_scope: "OK"
    comment_header: "Kernel SAST Scan Report"
    source_code: https://gitlab.com/cki-project/kernel-workflow/-/blob/main/webhook/sast.py
    faq_name: 'KWF FAQ'
    faq_url: 'https://red.ht/kernel_workflow_doc'
    documentation_url: 'https://gitlab.com/cki-project/kernel-workflow/-/blob/main/docs/README.sast.md'
    slack_name: '#team-kernel-workflow'
    slack_url: 'https://redhat-internal.slack.com/archives/C04LRUPMJQ5'
    new_issue_url: 'https://gitlab.com/cki-project/kernel-workflow/-/issues/new?issue[title]=sast webhook issue'
    request_evaluation_triggers: ["sast"]
    run_on_drafts: false
    run_on_changed_commits: true
    run_on_changed_description: false
    run_on_changed_to_draft: false
    run_on_changed_to_ready: true
    run_on_closing: false
  signoff:
    name: "signoff"
    enabled_by_default: true
    required_label_prefixes: ["Signoff"]
    required_for_qa_scope: "OK"
    comment_header: "DCO Signoff Check Report"
    source_code: https://gitlab.com/cki-project/kernel-workflow/-/blob/main/webhook/signoff.py
    faq_name: 'KWF FAQ'
    faq_url: 'https://red.ht/kernel_workflow_doc'
    documentation_url: 'https://gitlab.com/cki-project/kernel-workflow/-/blob/main/docs/README.signoff.md'
    slack_name: '#team-kernel-workflow'
    slack_url: 'https://redhat-internal.slack.com/archives/C04LRUPMJQ5'
    new_issue_url: 'https://gitlab.com/cki-project/kernel-workflow/-/issues/new?issue[title]=signoff webhook issue'
    request_evaluation_triggers: ["signoff", "dco"]
    run_on_drafts: true
    run_on_changed_commits: true
    run_on_changed_description: true
    run_on_changed_to_draft: true
    run_on_changed_to_ready: true
    run_on_closing: false
  subsystems:
    name: "subsystems"
    enabled_by_default: true
    required_label_prefixes: ["ExternalCI"]
    extra_labels_regex: "Subsystem:.+"
    required: false
    source_code: https://gitlab.com/cki-project/kernel-workflow/-/blob/main/webhook/subsystems.py
    faq_name: 'KWF FAQ'
    faq_url: 'https://red.ht/kernel_workflow_doc'
    documentation_url: 'https://gitlab.com/cki-project/kernel-workflow/-/blob/main/docs/README.subsystems.md'
    slack_name: '#team-kernel-workflow'
    slack_url: 'https://redhat-internal.slack.com/archives/C04LRUPMJQ5'
    new_issue_url: 'https://gitlab.com/cki-project/kernel-workflow/-/issues/new?issue[title]=subsystems webhook issue'
    request_evaluation_triggers: ["subsystem", "subsystems", "externalci", "external-ci"]
    run_on_drafts: true
    run_on_changed_commits: true
    run_on_changed_description: true
    run_on_changed_to_draft: true
    run_on_changed_to_ready: true
    run_on_closing: false
  sprinter:
    name: "sprinter"
    enabled_by_default: false
    required_label_prefixes: []
    extra_labels_regex: "CWF::.+"
    required: false
    source_code: https://gitlab.com/cki-project/kernel-workflow/-/blob/main/webhook/sprinter.py
    match_to_projects: false
    faq_name: 'CKI FAQ'
    faq_url: 'https://cki-project.org/l/devel-faq'
    documentation_url: 'https://gitlab.com/cki-project/kernel-workflow/-/blob/main/docs/README.sprinter.md'
    slack_name: '#team-kernel-cki'
    slack_url: 'https://redhat-internal.slack.com/archives/C04KPCFGDTN'
    new_issue_url: 'https://gitlab.com/cki-project/infrastructure/-/issues/new?issue[title]=sprinter webhook issue'
    request_evaluation_triggers: []
    run_on_drafts: true
    run_on_changed_commits: false
    run_on_changed_description: true
    run_on_changed_to_draft: false
    run_on_changed_to_ready: false
    run_on_closing: true
    run_on_empty_mrs: true
    run_on_oversize_mrs: true
  terminator:
    name: "terminator"
    enabled_by_default: false
    required_label_prefixes: []
    required: false
    source_code: https://gitlab.com/cki-project/kernel-workflow/-/blob/main/webhook/terminator.py
    match_to_projects: false
    run_on_closed: true
    run_on_build_downstream: true
    run_on_build_upstream: true
    run_on_pipeline_upstream: true
    run_on_empty_mrs: true
    run_on_oversize_mrs: true
  umb_bridge:
    name: "umb_bridge"
    enabled_by_default: true
    required_label_prefixes: []
    required: false
    source_code: https://gitlab.com/cki-project/kernel-workflow/-/blob/main/webhook/umb_bridge.py
    run_on_drafts: true
    run_on_changed_commits: false
    run_on_changed_description: true
    run_on_changed_to_draft: false
    run_on_changed_to_ready: false
    run_on_closing: true
    run_on_empty_mrs: true
    run_on_oversize_mrs: true
