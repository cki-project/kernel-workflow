"""Process all of the Approvals that are associated with a merge request."""
from dataclasses import dataclass
from functools import cached_property
import sys
import typing

from cki_lib import logger
from cki_lib import misc
from cki_lib.gitlab import get_instance
import prometheus_client as prometheus

from . import cdlib
from . import common
from . import defs
from .approval_rules import ApprovalRuleStatus
from .approval_rules import ApprovalRuleType
from .approval_rules import BaseApprovalRule
from .approval_rules import make_owners_approval_rule
from .base_mr import BaseMR
from .base_mr_mixins import ApprovalsMixin
from .base_mr_mixins import CommitsMixin
from .base_mr_mixins import DependsMixin
from .base_mr_mixins import DiffsMixin
from .base_mr_mixins import OwnersMixin
from .libjira import fetch_issues
from .libjira import reset_preliminary_testing
from .rhissue import RHIssue
from .session import SessionRunner
from .session_events import GitlabMREvent
from .session_events import GitlabNoteEvent

if typing.TYPE_CHECKING:
    from gitlab.client import Gitlab
    from gitlab.v4.objects.merge_requests import ProjectMergeRequest

    from .approval_rules import AnyApprovalRule
    from .approval_rules import MRApprovalRules
    from .approval_rules import MergeRequestApprovalRule
    from .approval_rules import OwnersApprovalRules
    from .approval_rules import ProjectApprovalRule

LOGGER = logger.get_logger('cki.webhook.ack_nack')

CC_TAGGED_USERS_RULE_NAME = 'Cc tagged users'
MISSING_CC_EMAILS_NAME = 'MISSING'

METRIC_KWF_NONPROJECT_MEMBER = prometheus.Counter(
    'kwf_nonproject_member', 'Gitlab user not a member of the project',
    ['project_name', 'username']
)

ACKS_LABEL_PREFIX = 'Acks'


BLOCK_KEYWORD = 'block'
UNBLOCK_KEYWORD = 'unblock'


class BlockingEvent(typing.NamedTuple):
    """Simple tuple of /block or /unblock activity."""

    username: str
    action: typing.Literal[BLOCK_KEYWORD, UNBLOCK_KEYWORD]

    @property
    def rule_name(self) -> str:
        """Return the expected name of an approval rule created from this event."""
        return f'{defs.BLOCKED_BY_PREFIX} {self.username}'

    @property
    def comment_text(self) -> str:
        """Return the text to leave in the comment for a /block action."""
        if self.action != BLOCK_KEYWORD:
            return ''
        return f'@{self.username} has blocked this Merge Request via a `/block` action.'


def _emails_to_gl_user_names(
    gl_instance: 'Gitlab',
    emails: set[str]
) -> tuple[set, list[str]]:
    """Return a tuple with the mapped usernames set and list of emails that did not map."""
    users = set()
    missing = []
    for email in emails:
        if "@redhat.com" not in email and "@fedoraproject.org" not in email:
            continue
        if found_usernames := [x.username for x in gl_instance.users.list(search=email)]:
            users.update(found_usernames)
            continue
        missing.append(email)
    return users, missing


def assign_reviewers(
    gl_mr: 'ProjectMergeRequest',
    user_list: typing.Iterable[str]
) -> None:
    """Use a quickaction to assign the given user as a reviewer of the given MR."""
    # The command requires an @ in front of the username.
    users = [user if user.startswith('@') else f'@{user}' for user in user_list]
    if misc.is_production_or_staging():
        cmd = f'/assign_reviewer {" ".join(users)}'
        gl_mr.notes.create({'body': cmd})


# The DependsMixin expects any additional fields to have kw_only set!
@dataclass(repr=False, kw_only=True)
class ApproveMR(OwnersMixin, DependsMixin, CommitsMixin, ApprovalsMixin, DiffsMixin, BaseMR):
    # pylint: disable=too-many-public-methods,too-many-ancestors
    """Represent the MR, its revisions and approvals."""

    blocking_event: BlockingEvent | None
    commits_changed: bool

    @cached_property
    def reset_approvals(self) -> bool:
        """Return True if the approvals of this MR need to be reset, otherwise False."""
        # If this is True it causes any existing approvals on the MR to be ignored when
        # generating ApprovalRule data.
        # If the event indicates the commits changed and we find "real" code changes in the
        # DiffsMixin.code_changes() then we want to reset approvals.
        if self.commits_changed and self.code_changes():
            return True
        return False

    @cached_property
    def code_changed_label(self) -> defs.Label | None:
        """Return the expected CodeChanged version Label (if versions > 1), else None."""
        version_count = len(self.history)
        LOGGER.debug('MR has %d diff version(s).', version_count)

        # If there is only one diff in the version history then nothing has changed.
        if not version_count > 1:
            return None

        # Loop through each version in the history list (minus the last one) and compare it to
        # the previous one. If there are real code changes then this is the 'version' we want to
        # set in the CodeChanged::v# label.
        version_with_changes = 1
        for index in range(version_count - 1):
            if self.code_changes(self.history[index:index+2]):
                version_with_changes = version_count
                break
            version_count -= 1

        LOGGER.debug('Latest version with changes is version #%d.', version_with_changes)

        # Make a label with the version_with_changes value or return None.
        # Only do this if the version_with_changes is > 1.
        if version_with_changes > 1:
            return defs.Label(f'{defs.CODE_CHANGED_PREFIX}v{version_with_changes}')
        return None

    @cached_property
    def current_sst_rules(self) -> 'OwnersApprovalRules':
        """Return a dict with the current SST approval rules on this MR."""
        return {
            rule_name: rule for rule_name, rule in self.approval_rules.items() if
            self.owners.get_matching_subsystems_by_label(rule_name)
        }

    @cached_property
    def current_blocked_by_rules(self) -> 'MRApprovalRules':
        """Return a dict with the current Blocked-by: rules on this MR."""
        return {
            k: v for k, v in self.approval_rules.items() if k.startswith(defs.BLOCKED_BY_PREFIX)
        }

    @property
    def new_blocked_by_rule(self) -> BaseApprovalRule | None:
        """Return a new ApprovalRule if there was a valid /block action, otherwise None."""
        event = self.blocking_event

        if not event or event.action != BLOCK_KEYWORD:
            return None

        if event.rule_name in self.current_blocked_by_rules:
            LOGGER.warning('A blocking rule for %s already exists!', event.username)
            return None

        if event.username == self.author.username:
            LOGGER.warning('BlockingEvent is from the MR author???')
            return None

        blocking_user = self.user_cache.get_by_username(event.username)
        return BaseApprovalRule(
            name=event.rule_name,
            type=ApprovalRuleType.REGULAR,
            approvals_required=1,
            eligible_approvers={blocking_user},
            mr_approved_by=self.approved_by
        )

    @cached_property
    def expected_blocked_by_rules(self) -> dict[str, 'AnyApprovalRule']:
        """Return a dict with the current Blocked-by: rules, plus or minus the blocking_event."""
        existing_blocked_by: dict[str, AnyApprovalRule] = self.current_blocked_by_rules.copy()
        blocking_event = self.blocking_event

        # If there is no blocking action then accept the current blocked_by rules.
        if not blocking_event:
            return existing_blocked_by

        blocking_rule_name = f'{defs.BLOCKED_BY_PREFIX} {blocking_event.username}'

        # If we have an /unblock action then pop off the matching existing rule.
        if blocking_event.action == UNBLOCK_KEYWORD:
            rule_to_remove = existing_blocked_by.pop(blocking_rule_name, None)
            if not rule_to_remove:
                LOGGER.warning('Have unblocking event from %s but no existing rule?',
                               blocking_event.username)

        # Add any new blocking rule.
        if new_rule := self.new_blocked_by_rule:
            existing_blocked_by[new_rule.name] = new_rule

        return existing_blocked_by

    @cached_property
    def expected_all_members_rule(self) -> 'MergeRequestApprovalRule | ProjectApprovalRule':
        """Return the expected 'All Members` rule for this MR."""
        return self.expected_project_rules[defs.ALL_MEMBERS_APPROVAL_RULE]

    @cached_property
    def expected_bot_rfm_rule(self) -> 'MergeRequestApprovalRule | None':
        """Return the expected 'Bot: readyForMerge` rule for this MR, or None if there isn't one."""
        return self.expected_project_rules.get(defs.BOT_APPROVAL_RULE)

    @cached_property
    def _expected_sst_rules(self) -> 'OwnersApprovalRules':
        """Return a dict with the expected SST approval rules for this MR, regardless of draft."""
        # This must be set to get a 'unified' SST for 'redhat'!
        self.merge_subsystems = True

        rules = {}
        for entry in self.owners_subsystems:
            rule = make_owners_approval_rule(entry, self.user_cache, self.author, self.approved_by)

            # Don't blindly replace a rule with the same name, shout about it!
            if rule.name in rules:
                raise RuntimeError(
                    'Generated Owners ApprovalRules with the same name!'
                    f'{rules[rule.name]} versus {rule}'
                )

            # If we've managed to generate a rule that has no eligible_approvers (because the
            # only approver is the MR author, perhaps) then drop it. Gitlab does not allow
            # the creation of rules with an empty eligible_approvers set.
            if not rule.eligible_approvers:
                LOGGER.info('Skipping rule as it has no eligible approvers: %s', rule)
                continue

            rules[rule.name] = rule

        return rules

    @cached_property
    def expected_sst_rules(self) -> 'OwnersApprovalRules':
        """Return a dict with the expected SST approval rules for this MR. Empty for Draft."""
        return self._expected_sst_rules if not self.draft else {}

    @cached_property
    def expected_project_rules(self) -> dict[str, 'AnyApprovalRule']:
        """Return the dict of project-level rules that should be on this MR."""
        all_rules: dict[str, AnyApprovalRule] = {}

        # We always want to include all the project-level rules.
        for rule_name, project_rule in self.project_approval_rules.items():
            # Get the existing MR instance of this project rule, if any.
            mr_rule = self.approval_rules.get(rule_name)

            # If this project rule doesn't exist on the MR for some reason then just include it.
            if not mr_rule:
                all_rules[rule_name] = project_rule
                continue

            # If the rules are functionally the same then take the MR one.
            if mr_rule == project_rule:
                all_rules[rule_name] = mr_rule
                continue

            # If the MR variant has more required approvals then take it.
            if mr_rule.approvals_required >= project_rule.approvals_required:
                all_rules[rule_name] = mr_rule
                continue

            # Otherwise, only take the MR variant if this is a z-stream build MR
            if self.is_build_mr:
                LOGGER.info("This is a z-stream build MR, keeping overridden %s.", mr_rule)
                all_rules[rule_name] = mr_rule
                continue

            LOGGER.info("Author is not a maintainer and/or not a z-stream branch, will "
                        "revert overridden %s.", mr_rule)
            all_rules[rule_name] = project_rule

        return all_rules

    @cached_property
    def all_expected_rules(self) -> dict[str, 'AnyApprovalRule']:
        """Return a dict of all the expected ApprovalRules for this MR."""
        all_rules: dict[str, AnyApprovalRule] = {}

        # Add all the project-level rules.
        all_rules.update(self.expected_project_rules)

        # Add all the current Blocked-by: rules.
        all_rules.update(self.expected_blocked_by_rules)

        # Add all the expected SST-related rules.
        all_rules.update(self.expected_sst_rules)

        # We also want to include any other custom approval rules that have been set on the MR.
        for rule in self.approval_rules.values():
            if rule.name in all_rules:
                continue

            # Don't carry over old SST rules.
            if rule.name in self.current_sst_rules:
                continue

            # Don't carry over old blocking rules.
            if rule.name in self.current_blocked_by_rules:
                continue

            all_rules[rule.name] = rule

        return all_rules

    @cached_property
    def expected_sst_labels(self) -> list[defs.Label]:
        """Return a list of expected MR sst approval labels."""
        # These are double-scoped labels in the form Acks::<rule.name>::<status> where <status>
        # is OK or NeedsReview.
        labels = []
        for rule in self.expected_sst_rules.values():
            # If the SST does not require approvals then it does not get an Acks label.
            if not rule.approvals_required:
                continue

            rule_scope = defs.MrScope.OK if rule.approved else defs.MrScope.NEEDS_REVIEW
            labels.append(rule_scope.label(f'{ACKS_LABEL_PREFIX}::{rule.name}'))

        return labels

    @cached_property
    def _all_members_still_needed(self) -> int:
        """Return the fudged number of approvals still needed by the All Members rule."""
        all_members_approvals_still_needed = self.expected_all_members_rule.approvals_still_needed

        # If the rule really only requires < 1 approval then just take the still_needed value as-is.
        if self.expected_all_members_rule.approvals_required <= 1:
            return all_members_approvals_still_needed

        # If this MR requires a bot approval then we pretend the All Members rule still needs
        # one less approval than it really does.
        if self.expected_bot_rfm_rule and all_members_approvals_still_needed:
            all_members_approvals_still_needed -= 1

        return max(all_members_approvals_still_needed, 0)

    @cached_property
    def remaining_approvals(self) -> int:
        """Return the number of non-project Approval Rule approvals still required on the MR."""
        all_needed = [rule.approvals_still_needed for rule in self.all_expected_rules.values() if
                      rule.name not in self.project_approval_rules]

        # Add the number of non-bot approvals needed by the All Members rule.
        all_needed.append(self._all_members_still_needed)

        # In the draft case all_needed could very well be empty so default to 0.
        return max(all_needed, default=0)

    @property
    def deleted_files(self) -> set[str]:
        """Return the set of files deleted in this MR's non-dependency commits."""
        return set(self.version_manager.latest_deleted_files) \
            if self.version_manager.latest_deleted_files else set()

    @property
    def all_okay_no_owners_files(self) -> bool:
        """Return True if all files are okay w/o an ownerns.yaml mapping."""
        okay_prefixes = ('redhat/configs/', 'Documentation/')
        return all(file.startswith(okay_prefixes) for file in self.all_files)

    @cached_property
    def overall_approvals_scope(self) -> defs.MrScope:
        """Return the overall MrScope of this MR."""
        # If we have any Blocked-by: rules then we are BLOCKED.
        if any(not rule.approved for rule in self.expected_blocked_by_rules.values()):
            return defs.MrScope.BLOCKED

        if not self.all_okay_no_owners_files and self.all_files != self.deleted_files \
           and not self.owners_subsystems:
            return defs.MrScope.MISSING

        return defs.MrScope.NEEDS_REVIEW if self.remaining_approvals else defs.MrScope.OK

    @cached_property
    def cc_reviewers(self) -> tuple[set, list[str]]:
        """Return set of usernames derived Cc: tag emails, as well list of missing emails."""
        cc_usernames = set()
        missing = []
        if cc_emails := {cc[1] for cc in self.description.cc}:
            cc_usernames, missing = _emails_to_gl_user_names(self.session.gl_instance, cc_emails)
            if cc_usernames:
                LOGGER.info('Mapped %s Cc email addresses to %s usernames: %s', len(cc_emails),
                            len(cc_usernames), cc_usernames)
            if missing:
                LOGGER.info('Cc email addresses which could not be mapped to usernames: %s',
                            missing)
        return cc_usernames, missing

    def status_report(self) -> str:
        """Return a string with a summary of expected Approval Rule statuses."""
        summary = ''

        # Do the 'Approved by' lines.
        if self.approved_by:
            summary += "Approved by:\n"
            for approver in self.approved_by:
                summary += f" - {approver.name} ({approver.username})\n"

        sorted_rules = {
            'Required': [],
            'Optional': [],
            'Satisfied': []
        }

        # Sort each rule into Required, Optional, and Satisfied groups.
        for rule in self.all_expected_rules.values():
            # Don't do this for the project-level rules.
            if rule.name in self.expected_project_rules:
                continue

            if rule.status is ApprovalRuleStatus.SATISFIED:
                sorted_rules['Satisfied'].append(rule)
                continue

            if rule.status is ApprovalRuleStatus.OPTIONAL:
                sorted_rules['Optional'].append(rule)
                continue

            # Status must be UNSATISFIED.
            sorted_rules['Required'].append(rule)

        # Loop over the filtered_rules keys and add them to the summary string.
        for key, rule_list in sorted_rules.items():
            if rule_list:
                summary += f"\n{key} Approvals:  \n"
                summary += '  \n'.join(rule.status_str for rule in rule_list)
                summary += '\n'

        # In the draft case, the SST rules are not "expected" but we can still include them in
        # our report.
        if self.draft and self._expected_sst_rules:
            summary += ('\nExpected Approvals (rules that will be created '
                        'once the MR is out of draft):  \n')
            summary += '\n'.join(rule.status_str for rule in self._expected_sst_rules.values())
            summary += '\n'

        docs_url = 'https://gitlab.com/redhat/centos-stream/src/kernel/documentation'
        if self.overall_approvals_scope is defs.MrScope.MISSING:
            summary += ('\n**ERROR**: No files in this MR map to any known subsystem team (SST) '
                        'defined in the owners.yaml file. Please work with the SSTs to assign '
                        'these files to an SST via an [update to the owners.yaml file in the '
                        f'documentation project]({docs_url}).  \n')

        if self.remaining_approvals:
            summary += f'\nRequires {self.remaining_approvals} more Approval(s).'
        else:
            summary += '\nMerge Request has all necessary non-bot Approvals.'

        return summary

    @property
    def expected_reviewers(self) -> list[str]:
        """Return the sorted list of usernames expected to be assigned as reviewers."""
        reviewer_set = set()

        # All CC: users are added as reviewers.
        reviewer_set.update(self.cc_reviewers[0])

        # Users who /block the MR are added as reviewers.
        reviewer_set.update(user.username for
                            rule in self.expected_blocked_by_rules.values() for
                            user in rule.eligible_approvers)

        # If this MR's approvals are being reset then add the original approvers as reviewers.
        if self.reset_approvals and self.original_non_bot_approved_by:
            reviewer_set.update(username for username in self.original_non_bot_approved_by)

        return sorted(reviewer_set)

    @property
    def original_non_bot_approved_by(self) -> list[str]:
        """Return the original list of usernames that have approvals on this MR, minus any bots."""
        # self.approved_by is masked out if self.reset_approvals is True so we have to dig this
        # out of the original cached ApprovalsMixin._all_approval_rules query result.
        return [user.username for user in self._all_approval_rules[2] if
                user.username not in defs.BOT_ACCOUNTS]

    def approval_reset_report(self) -> str:
        """Return a report about the approval reset."""
        if not self.reset_approvals:
            return ''

        interdiff = cdlib.assemble_interdiff_markdown(self.code_changes())
        revision = len(self.diffs)

        report_str = ''

        if original_approvers := self.original_non_bot_approved_by:
            report_str += (f'Approval(s) from {" ".join(original_approvers)} removed due to code '
                           f'changes, fresh approvals required on v{revision}.\n\n')

        if interdiff:
            header = f'Code changes in revision v{revision} of this MR:  \n'
            report_str += common.wrap_comment_table(header, interdiff, "",
                                                    f'v{revision} interdiff')

        return report_str

    def do_reset_preliminary_testing(self) -> None:
        """Reset the Preliminary Testing field on Jira issues referenced in the MR."""
        if not self.manage_jiras or self.is_build_mr:
            return

        if not (jira_tags := list(self.description.jira_tags)):
            return

        issues = fetch_issues(list(jira_tags), jira=self.session.jira, filter_kwf=True)
        rhissues = [RHIssue.new_from_ji(ji=ji, jira=self.session.jira, mrs=None) for ji in issues]
        for rhi in rhissues:
            issues.extend([lrhi.ji for lrhi in rhi.linked_rhissues])
        reset_preliminary_testing(self.session.jira, issues)

    def do_approvals_reset(self) -> None:
        """Reset approvals on the MR and return a report about it."""
        if self.project_approval_resets_enabled:
            raise RuntimeError('This project resets approvals for us!')

        namespace = self.namespace
        gl_project = self.gl_project

        # Assume we have a private token and use it to get a new gl_instance and gl_project.
        gl_instance = get_instance(url=f'{defs.GITFORGE}/{namespace}',
                                   env_name='GITLAB_KWF_BOT_API_TOKENS')
        gl_instance.auth()
        gl_project = gl_instance.projects.get(gl_project.id)
        if not gl_instance.private_token:
            raise ValueError(f'Project {namespace} approval resets disabled, but no token!')

        gl_mergerequest = gl_project.mergerequests.get(self.iid)
        revision = len(self.diffs)
        LOGGER.info("Clearing Approvals on %s MR %s (Rev v%s)", namespace, self.iid, revision)

        if misc.is_production_or_staging():
            gl_mergerequest.reset_approvals()

    def review_request_comment(self) -> str:
        """Generate a bulk 'please review this mr' comment for reviewers for affected subsystems."""
        # We only want to request review for new SSTs.
        new_sst_rules = [rule for rule in self.expected_sst_rules.values() if
                         rule.name not in self.current_sst_rules]

        # If there are no new SST rules then there is nothing to do here.
        if not new_sst_rules:
            return ''

        sst_names_str = ', '.join(rule.name for rule in new_sst_rules)

        body = f'Affected subsystem(s): {sst_names_str}\n\n'

        for rule in new_sst_rules:
            rule_contacts = []
            for gluser in rule.usernames:
                users = self.owners.get_matching_users('gluser', gluser)
                email = users[0].get('email', 'N/A') if users else 'N/A'
                rule_contacts.append((gluser, email))

            usernames = ' '.join(f'@{gluser} ({email})'
                                 for gluser, email in rule_contacts)

            notification = (f'Requesting review of subsystem {rule.name} with '
                            f'{rule.approvals_required} required approval(s) from '
                            f'user(s) {usernames}\n\n')
            body += notification

        if missing_emails := self.cc_reviewers[1]:
            body += ('Notice: the following email address(es) found in the MR '
                     'Description Cc: tags could not be mapped to Gitlab usernames: '
                     f'{missing_emails}\n')

        return body

    @property
    def expected_labels(self) -> list[defs.Label]:
        """Return the complete list of ack_nack hook Labels expected for this MR."""
        # Begin with the CodeChanged Label, if needed.
        label_list = [self.code_changed_label] if self.code_changed_label else []

        # If the MR is draft then we want to avoid any Acks labels so stop here.
        if self.draft:
            return label_list

        # Add on the overall Acks:: label.
        label_list.append(self.overall_approvals_scope.label(ACKS_LABEL_PREFIX))

        # And the individual required SST approval rule labels...
        label_list.extend(self.expected_sst_labels)

        return label_list

    def do_label_updates(self) -> None:
        """Do any necessary label changes."""
        current_acks_labels = set(self.session.webhook.webhooks_labels(self.labels))
        expected_acks_labels = set(self.expected_labels)

        LOGGER.info('Current ack_nack labels: %s', sorted(current_acks_labels))
        LOGGER.info('Expected ack_nack labels: %s', sorted(expected_acks_labels))

        if labels_to_remove := sorted(current_acks_labels - expected_acks_labels):
            LOGGER.info('Removing ack_nack labels: %s', labels_to_remove)
            self.remove_labels(labels_to_remove)

        if labels_to_add := sorted(expected_acks_labels - current_acks_labels):
            LOGGER.info('Adding ack_nack labels: %s', labels_to_add)
            self.add_labels(labels_to_add)

    def do_comments(self):
        """Do any necessary comments."""
        if approval_reset := self.approval_reset_report():
            self.session.update_webhook_comment(self.gl_mr, approval_reset)

        if self.new_blocked_by_rule:
            blocked_by_comment = self.blocking_event.comment_text
            self.session.update_webhook_comment(self.gl_mr, blocked_by_comment)

        if self.expected_reviewers:
            LOGGER.info('Adding reviewers with a quick action: %s', self.expected_reviewers)
            assign_reviewers(self.gl_mr, self.expected_reviewers)

        if review_request := self.review_request_comment():
            self.session.update_webhook_comment(self.gl_mr, review_request)

        status_report = self.status_report()
        report_header = self.session.webhook.comment_header

        if not self.draft:
            acks_label = self.overall_approvals_scope.label(ACKS_LABEL_PREFIX)
            header_str = f'**{report_header}**: ~"{acks_label}"\n\n'
        else:
            header_str = f'**{report_header}**: **draft report**\n\n'

        self.session.update_webhook_comment(self.gl_mr, header_str + status_report,
                                            bot_name=self.session.gl_user.username,
                                            identifier=report_header)

    @property
    def rules_to_add(self) -> list['AnyApprovalRule']:
        """Return the list of expected rules which are not currently on the MR."""
        return [v for k, v in self.all_expected_rules.items() if k not in self.approval_rules]

    @property
    def rules_to_remove(self) -> list['AnyApprovalRule']:
        """Return the list of current rules which are not in the expected rules."""
        return [v for k, v in self.approval_rules.items() if k not in self.all_expected_rules]

    @property
    def rules_to_update(self) -> list['AnyApprovalRule']:
        """Return the list of expected rules which do not match what is on the MR."""
        need_updating = []

        for expected_rule in self.all_expected_rules.values():
            if not (existing_rule := self.approval_rules.get(expected_rule.name)):
                continue

            if expected_rule != existing_rule:
                need_updating.append(expected_rule)

        return need_updating

    def do_update_mr_rules(self) -> None:
        """Add new rules, remove unwanted ones, and update existing ones as needed."""
        for rule in self.rules_to_add:
            rule.create(self.gl_mr)

        for rule in self.rules_to_remove:
            if self.session.is_production_or_staging:
                self.gl_mr.approval_rules.delete(rule.gid.id)

        for rule in self.rules_to_update:
            # The objects in the rules_to_update list have the rule attribute values we want
            # but the rule we actually need to change via the API is somewhere in the dict of
            # current approval rules (self.approval_rules).
            existing_rule = self.approval_rules[rule.name]
            existing_rule.update(
                self.session.graphql,
                self.url,
                rule.approvals_required,
                rule.eligible_approvers
            )

    def do_all_updates(self) -> None:
        """Bring the MR inline with the expected values."""
        # If the approvals need reset then get that out of the way.
        if self.reset_approvals:
            self.do_reset_preliminary_testing()

            if not self.project_approval_resets_enabled:
                LOGGER.info('Resetting approvals.')
                self.do_approvals_reset()
            else:
                LOGGER.info('No need to reset approvals.')

        # Update the MR's approval rules to match what we expect.
        self.do_update_mr_rules()

        # Set the MR labels as needed.
        self.do_label_updates()

        # Do any MR comments.
        self.do_comments()


def process_merge_request(
    session: SessionRunner,
    mr_url: defs.GitlabURL,
    blocking_event: BlockingEvent | None,
    commits_changed: bool
) -> None:
    """Process the given merge request and update it."""
    approve_mr = ApproveMR.new(
        session,
        mr_url,
        source_path=session.args.rhkernel_src,
        blocking_event=blocking_event,
        commits_changed=commits_changed
    )

    approve_mr.do_all_updates()


def process_gl_event(
    _: dict,
    session: SessionRunner,
    event: GitlabMREvent | GitlabNoteEvent,
    **__: typing.Any
) -> None:
    """Process a gitlab event."""
    LOGGER.info('Processing %s event for %s', event.kind.name, event.mr_url)

    # If this is a note event with a /block or /unblock comment then make a BlockingEvent and
    # include it when processing the MR.
    blocking_event = None
    if event.kind is defs.GitlabObjectKind.NOTE:
        for match in event.match_note_text:
            if block_action := match.groupdict().get('action'):
                blocking_event = BlockingEvent(event.user, block_action)
                break

        if not blocking_event and not event.has_requested_evaluation:
            LOGGER.info('Note event does not show any relevant changes, ignoring.')
            return

    # If this is an MR event and it shows the commits changed then let the ApproveMR know.
    commits_changed = bool(event.kind is defs.GitlabObjectKind.MERGE_REQUEST
                           and event.commits_changed)

    process_merge_request(session, event.mr_url, blocking_event, commits_changed)


HANDLERS = {
    GitlabMREvent: process_gl_event,
    GitlabNoteEvent: process_gl_event,
}


def main(args):
    """Run main loop."""
    common.init_sentry()
    parser = common.get_arg_parser('ACK_NACK')
    parser.add_argument('--rhkernel-src', **common.get_argparse_environ_opts('RHKERNEL_SRC'),
                        help='Directory where rh kernel will be checked out')
    args = parser.parse_args(args)
    session = SessionRunner.new('ack_nack', args=args, handlers=HANDLERS)
    session.run()


if __name__ == '__main__':
    main(sys.argv[1:])
