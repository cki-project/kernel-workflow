"""Mixins for the BaseMR."""
# pylint: disable=too-many-lines
from dataclasses import dataclass
from dataclasses import field
from dataclasses import fields
import datetime
from functools import cached_property
from functools import lru_cache
from textwrap import dedent
import typing
from urllib import parse

from cki_lib.logger import get_logger
from cki_lib.misc import get_nested_key
from cki_lib.session import get_session
from git import Repo
from gitlab.v4.objects.merge_requests import ProjectMergeRequestDiff
from gitlab.v4.objects.projects import Project
from textdistance import LCSStr

from webhook import kgit
from webhook.approval_rules import MergeRequestApprovalRule
from webhook.approval_rules import ProjectApprovalRule
from webhook.approval_rules import make_gl_approval_rule
from webhook.base_mr import BaseMR
from webhook.base_mr import MergeRequestProtocol
from webhook.cdlib import compare_commits
from webhook.cdlib import get_submitted_diff
from webhook.common import extract_config_from_paths
from webhook.defs import BOT_APPROVAL_RULE_PREFIX
from webhook.defs import GitlabGID
from webhook.defs import INTERNAL_FILES
from webhook.description import Commit
from webhook.description import Description
from webhook.description import MRDescription
from webhook.discussions import Discussion
from webhook.fragments import GL_USER
from webhook.fragments import GL_USER_CORE
from webhook.fragments import MR_APPROVAL_RULE
from webhook.fragments import MR_COMMITS
from webhook.fragments import PROJECT_APPROVAL_RULE
from webhook.kconfigs import KconfigManager
from webhook.pipelines import PipelineResult
from webhook.rhissue import make_rhissues

if typing.TYPE_CHECKING:
    from cki_lib.owners import Subsystem

    from webhook.approval_rules import MRApprovalRules
    from webhook.approval_rules import ProjectApprovalRules
    from webhook.rhissue import RHIssue
    from webhook.users import User

LOGGER = get_logger('cki.webhook.base_mr_mixins')


@dataclass(repr=False)
class ApprovalsMixin(MergeRequestProtocol):
    # pylint: disable=too-few-public-methods
    """A Mixin to provide support for approval rules to base_mr.BaseMR."""

    APPROVALS_QUERY_BASE = dedent("""
    query mrData($mr_id: String!, $namespace: ID!) {
      project(fullPath: $namespace) {
        branchRules {
          nodes {
            name
            approvalRules {
              nodes {
                ...ProjectApprovalRule
              }
            }
          }
        }
        mr: mergeRequest(iid: $mr_id) {
          approvedBy {
            nodes {
              ...GlUserCore
            }
          }
          ...MrApprovals
        }
      }
    }

    fragment MrApprovals on MergeRequest {
      approvalState {
        rules {
          ...MrApprovalRule
          sourceRule {
            ...MrApprovalRule
          }
        }
      }
    }
    """)

    APPROVALS_QUERY = (APPROVALS_QUERY_BASE +
                       MR_APPROVAL_RULE +
                       PROJECT_APPROVAL_RULE +
                       GL_USER +
                       GL_USER_CORE)

    @cached_property
    def _all_approval_rules(self) -> tuple['ProjectApprovalRules', 'MRApprovalRules', set['User']]:
        # pylint: disable=unidiomatic-typecheck
        """Return a tuple of all the project ApprovalRules and all the MR ApprovalRules."""
        # Generate the list of project-level rules we can about for this MR. That is
        # the 'All branches' rules, the 'All projected branches' rules, and the target branch rules.
        relevant_branch_rule_names = ['All branches', 'All protected branches']
        # I don't know why this would ever not be set but better safe than sorry.
        if self.target_branch:
            relevant_branch_rule_names.append(self.target_branch)

        # Do the query.
        results = self.query(
            self.session,
            self.APPROVALS_QUERY,
            query_params={'namespace': self.namespace, 'mr_id': str(self.iid)},
            paged_key='project/mr/approvalState/rules'
        )

        # Get the approvedBy list and transform it into a set of ApprovalUsers.
        raw_approval_by = get_nested_key(results, 'project/mr/approvedBy/nodes', [])
        mr_approved_by = {self.user_cache.get_by_dict(user_dict) for user_dict in raw_approval_by}

        # If we've been told to reset approvals then pretend the set of MR approved_by is empty.
        pretend_mr_approved_by = self.approved_by if self.reset_approvals else mr_approved_by

        # Get the raw MR rules.
        raw_rules: list = get_nested_key(results, 'project/mr/approvalState/rules', [])

        # Get the raw project-level rules as well.
        # We get a list of per-branch rules that each has a list of approvalRules nodes.
        for raw_branch in get_nested_key(results, 'project/branchRules/nodes', []):
            if raw_branch['name'] not in relevant_branch_rule_names:
                continue

            raw_branch_rules = get_nested_key(raw_branch, 'approvalRules/nodes', [])
            raw_rules.extend(raw_branch_rules)

        # Transform the raw rule list data into dicts of ApprovalRules.
        approval_rules = [
            make_gl_approval_rule(rule, self.user_cache, pretend_mr_approved_by) for
            rule in raw_rules
        ]

        # Split the approval_rules list into dicts of Project rules and MR rules.
        # This should work even if the API has given us ApprovalProjectRules in the MR rule data.
        project_rules = \
            {rule.name: rule for rule in approval_rules if type(rule) is ProjectApprovalRule}
        mr_rules = \
            {rule.name: rule for rule in approval_rules if type(rule) is MergeRequestApprovalRule}

        return project_rules, mr_rules, mr_approved_by

    @property
    def approval_rules(self) -> 'MRApprovalRules':
        """Return the dict of MR ApprovalRules using the rule name as key."""
        return self._all_approval_rules[1]

    @cached_property
    def approved_by(self) -> set['User']:
        """Return the set of User objects for people who have approved the MR."""
        # If we've been told that the MR's approvals should be reset then we pretend no one has
        # approved it regardless of what the real set of approvers is.
        if self.reset_approvals:
            return set()
        return self._all_approval_rules[2]

    @property
    def bot_approval_rules(self) -> 'MRApprovalRules':
        """Return the dict of Bot MergeRequestApprovalRules."""
        return {rule.name: rule for rule in self.approval_rules.values() if
                rule.name.startswith(BOT_APPROVAL_RULE_PREFIX)}

    @property
    def project_approval_rules(self) -> 'ProjectApprovalRules':
        """Return the dict of Project ApprovalRules using the rule name as key."""
        return self._all_approval_rules[0]

    @cached_property
    def project_approval_resets_enabled(self) -> bool:
        """Return whether or not the project has approval resets on push or not."""
        return self.gl_project.approvals.get().reset_approvals_on_push

    @cached_property
    def reset_approvals(self) -> bool:
        """Return True if the approvals of this MR need to be reset, otherwise False."""
        # If this is True it causes any existing approvals on the MR to be ignored when
        # generating ApprovalRule data.
        # Objects that use the ApprovalsMixin can override this property as needed.
        return False


@dataclass(repr=False)
class CommitsMixin(MergeRequestProtocol):
    """A Mixin to provide support for commits to BaseMR."""

    COMMIT_QUERY_BASE = dedent("""
    query mrCommits($mr_id: String!, $namespace: ID!, $per_page: Int = 100, $before: String = "", $after: String = "", $with_diffs: Boolean = false) {
      project(fullPath: $namespace) {
        mr: mergeRequest(iid: $mr_id) {
          ...MrCommits
        }
      }
    }
    """)  # noqa: E501

    COMMIT_QUERY = COMMIT_QUERY_BASE + MR_COMMITS + GL_USER

    @property
    def all_commits(self):
        """Return a list of all the Commit objects of the MR."""
        if not self.description.text and not self.commits:
            return []
        return [self.faux_description_commit] + list(self.commits.values())

    @property
    def all_descriptions(self):
        """Return a list of all the Description objects of the MR."""
        if not self.description.text and not self.commits:
            return []
        return [self.description] + [commit.description for commit in self.commits.values()]

    def _build_commits(self, raw_commits) -> dict[str, Commit]:
        """Return a starter dict of Commit objects with sha as the key."""
        commits = {}
        for raw_commit in raw_commits:
            # Pass in an author User from the cache.
            author = self.user_cache.get(raw_commit.pop('author')) if \
                raw_commit.get('author') else None
            zstream = self.rh_branch.zstream if self.rh_branch else False
            commit = Commit(input_dict=raw_commit, author=author, zstream=zstream)
            commits[commit.sha] = commit

        return commits

    def _commits(self, limit: int = 0, with_diffs: bool = False) -> list[dict]:
        """Return a list of raw commit objects without diffs."""
        if limit > self.commit_count:
            raise ValueError(f'Count {limit} exceeds {self.commit_count}!')

        # If we are including diffs then we can only get 10 Commits per page.
        # https://gitlab.com/gitlab-org/gitlab/-/merge_requests/113259#note_1452284840
        per_page = 10 if with_diffs else 100

        # If the limit is smaller than the per_page value then just get limit per page.
        if limit and limit < per_page:
            per_page = limit

        commits_data: list[dict] = []

        query_params = {
            'namespace': self.namespace,
            'mr_id': str(self.iid),
            'per_page': per_page,
            'with_diffs': with_diffs,
        }

        while True:
            LOGGER.debug('query_params: %s', query_params)
            results = self.session.graphql.client.query(
                self.COMMIT_QUERY,
                variable_values=query_params,
                operation_name='mrCommits'
            )

            new_nodes = get_nested_key(results, 'project/mr/commits/nodes', [])

            # If we didn't get any commits after the first pass then either the commits have
            # changed out from under us (someone force-pushed to the MR?) or we've done
            # something wrong.
            if not new_nodes:
                # If there is no commits_data then this must be our first loop and we can only
                # conclude the MR has no non-merge commits. In this case just return.
                if not commits_data:
                    return []

                raise RuntimeError(f'No more commits data returned: {query_params}')

            commits_data.extend(new_nodes)

            page_info = get_nested_key(results, 'project/mr/commits/pageInfo', {})

            # If we haven't been given a limit and there is no next page then stop.
            if not limit and not page_info.get('hasNextPage'):
                break

            query_params['after'] = page_info.get('endCursor')

            if limit:
                commits_remaining = max(limit - len(commits_data), 0)
                LOGGER.debug('commits_remaining: %d', commits_remaining)

                if not commits_remaining:
                    break

                full_pages, remainder = divmod(commits_remaining, per_page)
                query_params['per_page'] = per_page if full_pages else remainder

        return commits_data

    @cached_property
    def commits(self) -> dict[str, Commit]:
        """Return a dict of Commit objects with sha as the key."""
        if not (raw_commits := self._commits()):
            LOGGER.info('MR has no non-merge commits.')
            return {}

        return self._build_commits(raw_commits)

    @property
    def first_dep_commit(self):
        """Return the first commit whose Jira tag refers to a Depends."""
        if not self.description or not self.description.depends:
            return ''
        for commit in self.commits.values():
            # If any of the MRDescription.depends are in the Commit then return that commit's sha.
            if not self.description.depends.isdisjoint(commit.description.jira_tags):
                return commit
        return ''

    @property
    def first_dep_sha(self):
        """Return the sha of the first commit whose Jira tag refers to a Depends."""
        if not self.first_dep_commit:
            return ''
        return self.first_dep_commit.sha

    @cached_property
    def commits_with_diffs(self) -> dict[str, Commit]:
        """Return a dict of Commit objects (with diff data) with sha as the key."""
        limit = len(self.commits_without_depends)
        commits = self._build_commits(self._commits(limit=limit, with_diffs=True))
        LOGGER.debug("Fetched diffs for %s commits", len(commits))
        return commits

    @property
    def commits_without_depends(self):
        """Return a dict of the MR commits excluding commits from depends."""
        if not self.depends_mrs:
            return self.commits
        commits = {}
        for commit in self.commits.values():
            if not self.description.depends.isdisjoint(commit.description.jira_tags):
                break
            commits[commit.sha] = commit
        return commits

    @cached_property
    def faux_description_commit(self):
        """Return a faux Commit derived from the MR Description."""
        # The MR author does not always have a visible email so take values from
        # the head commit. Surely the MR author is the same as that of the head commit?
        head_commit = list(self.commits.values())[0] if self.commits else None
        commit_params = {
            'author': self.author,
            'author_email': head_commit.author_email if head_commit else '',
            'author_name': head_commit.author_name if head_commit else '',
            'committer_email': head_commit.committer_email if head_commit else '',
            'committer_name': head_commit.committer_name if head_commit else '',
            'description': self.description,
            'sha': 'MR Description',
            'title': self.title,
            'zstream': self.rh_branch.zstream if self.rh_branch else False
        }

        return Commit(**commit_params)

    @cached_property
    def files(self) -> list[str]:
        """Return the list of files affected by the non-dependency commits of the MR."""
        if not self.has_depends:
            return super().files
        # If there are no non-dependency commits then there are no files.
        if not (commit_shas := list(self.commits_without_depends.keys())):
            return []
        comparison = self.gl_project.repository_compare(f'{commit_shas[-1]}^', commit_shas[0])
        return [diff['new_path'] for diff in comparison['diffs']]

    @property
    def fresh_commits(self):
        """Return a fresh dict of Commits."""
        if 'commits' in self.__dict__:
            del self.commits
        return self.commits

    @property
    def has_internal(self):
        """Return True if the MR description or any commits are marked INTERNAL."""
        return any(description.marked_internal for description in self.all_descriptions)

    @property
    def only_internal_files(self) -> bool:
        """Return True if all files in the MR are in allowed paths for INTERNAL issues."""
        return all(path.startswith(INTERNAL_FILES) for path in self.files)

    @property
    def has_untagged(self):
        """Return True if any commits are UNTAGGED."""
        return not all(commit.description.jira_tags or
                       commit.description.marked_internal for commit in self.commits.values())


@dataclass(repr=False)
class GitRepoMixin(MergeRequestProtocol):
    """A Mixin to provide support for a kernel git repo."""

    linux_src: str = field(kw_only=True)
    work_branch: str = ''  # branch where we're performing git operations
    worktree_dir: str = ''  # git worktree dir where the branch is checked out

    @cached_property
    def repo(self) -> Repo:
        """A GitPython Repo object."""
        return Repo(self.linux_src)

    @cached_property
    def project_remote(self) -> str:
        """The name of the project's git remote."""
        return kgit.get_remote_name(
            self.linux_src, [self.gl_project.http_url_to_repo, self.gl_project.ssh_url_to_repo]
        )

    def fetch_remote(self, remote) -> None:
        """Fetch a named git remote."""
        kgit.fetch_remote(self.linux_src, remote)

    def prep_worktree(self, remote) -> None:
        """Prep a worktree and branch to work in."""
        branch, path = kgit.prep_temp_merge_branch(remote, self.gl_mr, self.linux_src)
        self.work_branch = branch
        self.worktree_dir = path

    def cleanup_worktree(self) -> None:
        """Clean up the worktree and branch work was done in."""
        kgit.clean_up_temp_merge_branch(self.linux_src, self.work_branch, self.worktree_dir)

    def find_already_backported_orig(self, target_branch: str, commit: str) -> list[dict]:
        """Return a list of commits backported in tree already (if any)."""
        log_data = self.repo.git.log("--oneline", target_branch, f"--grep=commit {commit}",
                                     "--", *self.commits_data[commit].files).splitlines()
        return [{'commit': entry.split(" ", 1)[0], 'subject': entry.split(" ", 1)[1]}
                for entry in log_data]


@dataclass(repr=False)
class DependsMixin(MergeRequestProtocol):
    """A Mixin to provide support for Dependencies to BaseMR."""

    is_dependency: bool = field(default=False, kw_only=True)

    def _set_mr_blocks(self, depends_mr_iids: dict[int, GitlabGID]) -> None:
        """
        Add or remove any dependency 'blocks' on this MR.

        depends_mr_iids is a mapping of all the MR internal IDs called out in Depends: tags
         mapped to their GitlabGID.
        """
        blocking_data = self._blocking_data()

        # The internal MR IDs of the current 'blocks' for this project mapped to their block ID.
        existing_blocks_iids = {
            block['blocking_merge_request']['iid']: block['id'] for block in blocking_data if
            block['blocking_merge_request']['project_id'] == self.project_gid.id
        }

        LOGGER.debug('depends_mr_iids: %s, existing_blocks_iids: %s',
                     depends_mr_iids, existing_blocks_iids)

        if not depends_mr_iids and not existing_blocks_iids:
            LOGGER.info('No depends MRs or "blocks" API entries.')
            return

        # The sets of "blocks" we need to create and remove.
        to_add_iids = set(depends_mr_iids) - set(existing_blocks_iids)
        to_remove_iids = set(existing_blocks_iids) - set(depends_mr_iids)

        if not to_add_iids and not to_remove_iids:
            LOGGER.info('Depends MRs & "blocks" API are in sync.')
            return

        # If there are any new dependency 'blocks' needed, add them.
        for iid in to_add_iids:
            LOGGER.info('Adding !%s as a dependency ("block") of %s.', iid, self.url)
            self.add_blocking_mr(depends_mr_iids[iid])

        # If there are any old blocks, remove them.
        for iid in to_remove_iids:
            LOGGER.info('Removing !%s as a dependency ("block") of %s.', iid, self.url)
            self.remove_blocking_id(existing_blocks_iids[iid])

    @cached_property
    def depends_mrs(self) -> list[typing.Self]:
        """Return a list of BaseMR objects representing any Depends: MRs and set up API 'blocks'."""
        depends_mrs = [self.new_depends_mr(mr_id) for mr_id in self.description.depends_mrs]
        self._set_mr_blocks({mr.iid: mr.global_id for mr in depends_mrs})
        return depends_mrs

    def new_depends_mr(self, iid) -> typing.Self:
        """Return a new instance of self with dependency=True set."""
        new_url = f"{self.url.rsplit('/', 1)[0]}/{iid}"
        # We may be a BaseMR with some mixins that need extra kwargs passed in. Get the list of
        # fields of self, excluding the standard BaseMR ones (plus 'is_dependency') and then copy
        # those field values out of self and pass them as kwargs to BaseMR.new().
        # There has to be a better way.
        excluded_fields = [field.name for field in fields(BaseMR)] + ['is_dependency']
        kwargs = {field.name: getattr(self, field.name) for field in fields(self) if
                  field.name not in excluded_fields and field.init and field.kw_only}
        return self.new(self.session, new_url, is_dependency=True, **kwargs)


@dataclass(repr=False)
class DiscussionsMixin(MergeRequestProtocol):
    """A Mixin to provide support for Reviewers to base_mr.BaseMR."""

    DISCUSSIONS_QUERY_BASE = dedent("""
    query mrData($mr_id: String!, $namespace: ID!, $limit: Int!, $after: String = "") {
      project(fullPath: $namespace) {
        mr: mergeRequest(iid: $mr_id) {
          ...MrDiscussions
        }
      }
    }

    fragment MrDiscussions on MergeRequest {
      discussions(first: $limit, after: $after) {
        pageInfo {
          hasNextPage
          endCursor
        }
        nodes {
          resolvable
          resolved
          notes {
            nodes {
              id
              author {
                ...GlUser
              }
              body
              system
              updatedAt
            }
          }
        }
      }
    }
    """)

    DISCUSSIONS_QUERY = DISCUSSIONS_QUERY_BASE + GL_USER

    QUERY_LIMIT = 10  # number of discussions to query at a time

    # Used by get_matching_discussion to cache results.
    _discussions: list = field(init=False, default_factory=list)
    _discs_page_info: dict = field(init=False,
                                   default_factory=lambda: {'hasNextPage': True, 'endCursor': ''})

    @cached_property
    def discussions(self):
        """Return the list of Discussions on the MR."""
        if self._discs_page_info['hasNextPage']:
            query_params = {
                'namespace': self.namespace,
                'mr_id': str(self.iid),
                'after': self._discs_page_info['endCursor'],
                'limit': self.QUERY_LIMIT
            }
            results = self.query(
                self.session,
                self.DISCUSSIONS_QUERY,
                query_params=query_params,
                paged_key='project/mr/discussions',
            )
            raw_discs = get_nested_key(results, 'project/mr/discussions/nodes', [])
            new_discs = [Discussion(**r_disc, user_cache=self.user_cache) for r_disc in raw_discs]
            self._discussions.extend(new_discs)
        return self._discussions

    def discussions_by_user(self, username):
        """Return the list non-system discussions on the MR started by the given user."""
        return self._find_discussions(self.user_discussions, username)

    @staticmethod
    def _find_discussions(discussions, username, substring=None, system=False):
        """Return the list of matching Discussions."""
        if discs := [disc for disc in discussions if disc.notes[0].author.username == username and
                     disc.notes[0].system is system]:
            if substring:
                discs = [disc for disc in discs if substring in disc.notes[0].body]
        return discs

    def matching_discussion(self, username, substring, system=False):
        """Return the first Discussion by the given user with the given substring, or None."""
        # This only checks the first Note of each Discussion (thread)!
        # If we already have all or some discussions cached then search them first.
        have_all_cached = 'discussions' in self.__dict__
        if to_search := self.discussions if have_all_cached else self._discussions:
            LOGGER.debug('Searching %s %s cached discussions.',
                         'all' if have_all_cached else 'first', len(to_search))
        else:
            LOGGER.debug('No cached discussions.')
        if discs := self._find_discussions(to_search, username, substring, system):
            return discs[0]
        if have_all_cached:
            return None

        # Fetch QUERY_LIMIT discussions at a time and return the first match.
        while self._discs_page_info['hasNextPage']:
            # Do the query.
            query_params = {
                'mr_id': str(self.iid),
                'namespace': self.namespace,
                'after': self._discs_page_info['endCursor'],
                'limit': self.QUERY_LIMIT
            }
            results = self.query(
                self.session,
                self.DISCUSSIONS_QUERY,
                query_params=query_params
            )
            raw_discs = get_nested_key(results, 'project/mr/discussions/nodes', [])
            # Update the stored page_info.
            self._discs_page_info.update(get_nested_key(results, 'project/mr/discussions/pageInfo'))
            # Store the Discussions.
            new_discs = [Discussion(**r_disc, user_cache=self.user_cache) for r_disc in raw_discs]
            self._discussions.extend(new_discs)
            # Search the latest Discussions.
            if discs := self._find_discussions(new_discs, username, substring, system):
                return discs[0]
        return None

    @cached_property
    def system_discussions(self):
        """Return the list system discussions on the MR."""
        return [disc for disc in self.discussions if disc.notes[0].system]

    @cached_property
    def user_discussions(self):
        """Return the list non-system discussions on the MR."""
        return [disc for disc in self.discussions if not disc.notes[0].system]


@dataclass(repr=False)
class MRRev:
    """Provide a view of the Commits in a previous version of the MR."""

    gl_project: Project
    description: MRDescription
    gl_diff: ProjectMergeRequestDiff  # A full diff from :merge_request_iid/versions/:version_id

    def __post_init__(self) -> None:
        """Say it."""
        LOGGER.info('Created %s', self)

    def __hash__(self) -> int:
        """Return the unique diff ID."""
        return hash((self.gl_diff.merge_request_id, self.gl_diff.id))

    def __repr__(self) -> str:
        """Represent."""
        repr_str = f'commits: {len(self.commits)}'
        repr_str += f', head: {self.head_commit_sha[:8]}'
        repr_str += f', start: {self.gl_diff.start_commit_sha[:8]}'
        if self.first_dep_sha:
            repr_str += f', first_dep: {self.first_dep_sha[:8]}'
        repr_str += f', created_at: {self.gl_diff.created_at}'
        return f'<MRRev {self.gl_diff.id}, {repr_str}>'

    @cached_property
    def commits(self) -> dict[str, Commit]:
        """Return a dict of Commit objects with sha as the key."""
        commits = {}

        for diff_commit in self.gl_diff.commits:
            commits[diff_commit['id']] = Commit(
                author_name=diff_commit['author_name'],
                author_email=diff_commit['author_email'],
                date=datetime.datetime.fromisoformat(diff_commit['authored_date'][:19]),
                description=Description(diff_commit['message'] or ''),
                sha=diff_commit['id'],
                title=diff_commit['title']
            )

        return commits

    @property
    def diff(self) -> list[dict]:
        """Return the original ProjectMergeRequestDiff diffs attribute value."""
        return self.gl_diff.diffs

    @cached_property
    def diff_without_depends(self) -> list[dict]:
        """Return a "diff" showing changes excluding any dependency commits."""
        return self.diff if not self.first_dep_sha else \
            self.gl_project.repository_compare(self.start_commit_sha, self.head_commit_sha)['diffs']

    @property
    def head_commit_sha(self) -> str:
        """Return the head commit sha for this MRRev."""
        return self.gl_diff.head_commit_sha

    @property
    def start_commit_sha(self) -> str:
        """Return the first dep sha or start commit sha (IOW exclude dependency commits, if any)."""
        return self.first_dep_sha or self.gl_diff.start_commit_sha

    @property
    def first_dep_commit(self):
        """Return the first commit whose Jira tag refers to a Depends."""
        if not self.description or not self.description.depends:
            return ''
        for commit in self.commits.values():
            # If any of the MRDescription.depends are in the Commit then return that commit's sha.
            if not self.description.depends.isdisjoint(commit.description.jira_tags):
                return commit
        return ''

    @property
    def first_dep_sha(self):
        """Return the sha of the first commit whose Jira tag refers to a Depends."""
        if not self.first_dep_commit:
            return ''
        return self.first_dep_commit.sha


@dataclass(repr=False)
class DiffsMixin(MergeRequestProtocol):
    """A mixin to provide Diff information for BaseMR."""

    @cached_property
    def diffs(self) -> list[ProjectMergeRequestDiff]:
        """Return the list of basic version diffs for the MR sorted by created_at, newest first."""
        diffs_list = typing.cast(list[ProjectMergeRequestDiff], self.gl_mr.diffs.list(get_all=True))
        return sorted(diffs_list, reverse=True, key=lambda diff: diff.created_at)

    @cached_property
    def full_diffs(self) -> list[ProjectMergeRequestDiff]:
        """Return the list of full version diffs for the MR."""
        return [self.gl_mr.diffs.get(diff.id) for diff in self.diffs]

    @staticmethod
    @lru_cache
    def _get_code_changes(history1: MRRev, history2: MRRev) -> list[str]:
        """Return the list of changes found between the two diff versions."""
        if history1.head_commit_sha == history2.head_commit_sha:
            return []
        diff_strings = []
        for history in (history1, history2):
            changes = history.diff_without_depends
            diff_strings.append(get_submitted_diff(changes)[0])
        return compare_commits(diff_strings[0], diff_strings[1])

    def code_changes(self, histories: typing.Optional[list[MRRev]] = None) -> list[str]:
        """Return the list of code changes between the first two MRRev in the histories list."""
        if not histories:
            histories = self.history
        if len(histories) < 2:
            LOGGER.info('Version list is too short, nothing to compare.')
            return []
        latest_mrrev = histories[0]
        previous_mrrev = histories[1]
        comparison = self._get_code_changes(previous_mrrev, latest_mrrev)
        LOGGER.info('Comparing %s to %s found %s changes.', previous_mrrev, latest_mrrev,
                    len(comparison))
        if comparison:
            LOGGER.debug('Found changes (first 50 lines):\n%s', '\n'.join(comparison[:500]))
        return comparison

    @cached_property
    def history(self) -> list[MRRev]:
        """Return the list of MRRev objects for this MR."""
        # The first item is the latest version so should match the parent MR.
        return [MRRev(self.gl_project, self.description, diff) for diff in self.full_diffs]


@dataclass(repr=False, kw_only=True)
class OwnersMixin(MergeRequestProtocol):
    """A mixin to provide support for an owners parser to base_mr.BaseMR."""

    source_path: str                # path of kernel source checkout to use for Kconfig lookups
    merge_subsystems: bool = False  # should the owners_subsystems method merge results or not

    @cached_property
    def kconfig_manager(self) -> KconfigManager:
        """Return a KconfigManager instance."""
        return KconfigManager.new(self.source_path)

    @cached_property
    def all_files(self) -> set[str]:
        """Return the set of all files impacted by the MR."""
        kconfig_paths: set[str] = set()

        for config_name in self.config_items:
            kconfig_paths.update(self.kconfig_manager(config_name))

        return set(self.files) | kconfig_paths

    @cached_property
    def config_items(self) -> list[str]:
        """Return the list of CONFIG items touched by this MR."""
        # This excludes redhat/configs/fedora/* configs since we don't want to bother RHEL
        # reviewers about fedora config changes.
        return extract_config_from_paths(self.files)

    @cached_property
    def owners_subsystems(self) -> list['Subsystem']:
        # pylint: disable=protected-access
        """Return the list of matching owners subsystems."""
        if not self.merge_subsystems:
            return self.owners.get_matching_subsystems(self.all_files)
        # It is possible for an MR to match multiple subsystems which have the
        # same subsystem_label. In this case we merge those subsystems together
        # and just return one. Information is lost but for our purposes we
        # (currently) only care about the list of reviewers/maintainers.
        subsystems = {}
        for subsystem in self.owners.get_matching_subsystems(self.all_files):
            if not (existing := subsystems.get(subsystem.subsystem_label)):
                subsystems[subsystem.subsystem_label] = subsystem
                continue
            for user in subsystem.maintainers:
                if user not in existing.maintainers:
                    existing._data['maintainers'].append(user)
            for user in subsystem.reviewers:
                if user not in existing.reviewers:
                    if 'reviewers' not in existing._data:
                        existing._data['reviewers'] = []
                    existing._data['reviewers'].append(user)
            if subsystem.required_approvals:
                existing._data['requiredApproval'] = True
        return list(subsystems.values())

    @cached_property
    def subsystems_with_test_variants(self) -> dict[str, list[str]]:
        """Return dict of subsystems with variants the MR touches that request variant testing."""
        variant_subsystems = {}
        for subsystem in self.owners_subsystems:
            if subsystem.test_variants:
                variant_subsystems[subsystem.subsystem_label] = subsystem.test_variants
        LOGGER.debug("Got variant_subsystems of: %s", variant_subsystems)
        return variant_subsystems

    @cached_property
    def test_variants(self) -> list[str]:
        """Return set of kernel variants requesting targeted testing for matching subsystems."""
        possible_variants = {pv for sst in self.owners_subsystems for pv in sst.test_variants}
        if not self.rh_branch or not self.rh_branch.fix_versions:
            return list(possible_variants)
        fix_version = self.rh_branch.fix_versions[0]
        valid_branches = self.rh_project.get_branches_by_fix_version(fix_version)
        valid_variants = {vv for vb in valid_branches for vv in vb.components}
        return list(possible_variants & valid_variants)


@dataclass(repr=False)
class PipelinesMixin(MergeRequestProtocol):
    # pylint: disable=too-few-public-methods
    """A Mixin to provide support for Pipelines to base_mr.BaseMR."""

    PIPELINES_QUERY = dedent("""
    query mrPipelines($namespace: ID!, $mr_id: String!) {
      project(fullPath: $namespace) {
        mr: mergeRequest(iid: $mr_id) {
          headPipeline {
            jobs(retried: false) {
              nodes {
                allowFailure
                id
                name
                createdAt
                pipeline {
                  id
                }
                status
                downstreamPipeline {
                  id
                  project {
                    id
                    fullPath
                  }
                  status
                  stages {
                    nodes {
                      name
                      jobs {
                        nodes {
                          status
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    """)

    @property
    def fresh_pipelines(self):
        """Get a fresh dict of pipelines."""
        if 'pipelines' in self.__dict__:
            del self.pipelines
        return self.pipelines

    @cached_property
    def pipelines(self):
        """Return the list of newest PipelineResults for the MR's head pipeline."""
        results = self.query(
            self.session,
            self.PIPELINES_QUERY,
            query_params={'namespace': self.namespace, 'mr_id': str(self.iid)},
            operation_name='mrPipelines'
        )
        raw_pipelines = get_nested_key(results, 'project/mr/headPipeline/jobs/nodes', [])
        # If a downstream job has been retried then filter out any old results.
        return PipelineResult.prepare_pipelines(raw_pipelines)


@dataclass(repr=False)
class ReviewersMixin(MergeRequestProtocol):
    # pylint: disable=too-few-public-methods
    """A Mixin to provide support for Reviewers to base_mr.BaseMR."""

    REVIEWERS_QUERY_BASE = dedent("""
    query mrData($mr_id: String!, $namespace: ID!) {
      project(fullPath: $namespace) {
        mr: mergeRequest(iid: $mr_id) {
          ...MrReviewers
        }
      }
    }

    fragment MrReviewers on MergeRequest {
      reviewers {
        nodes {
          ...GlUser
        }
      }
    }
    """)

    REVIEWERS_QUERY = REVIEWERS_QUERY_BASE + GL_USER

    @property
    def fresh_reviewers(self):
        """Get a fresh dict of reviewers."""
        if 'reviewers' in self.__dict__:
            del self.reviewers
        return self.reviewers

    @cached_property
    def reviewers(self):
        """Return a dict of Users representing the current reviewers of the MR."""
        results = self.query(
            self.session,
            self.REVIEWERS_QUERY,
            query_params={'namespace': self.namespace, 'mr_id': str(self.iid)}
        )
        raw_reviewers = get_nested_key(results, 'project/mr/reviewers/nodes', [])
        return {raw_reviewer['username']: self.user_cache.get(raw_reviewer) for
                raw_reviewer in raw_reviewers}

    def set_reviewers(self, usernames, mode='APPEND'):
        """Call graphql.set_mr_reviewers and uncache the reviewers property."""
        LOGGER.info('%sing reviewers with: %s', mode.capitalize().removesuffix('e'), usernames)
        self.session.graphql.set_mr_reviewers(self.namespace, self.iid, usernames, mode)
        if 'reviewers' in self.__dict__:
            del self.reviewers


@dataclass(repr=False, kw_only=True)
class KerneloscopeMixin(MergeRequestProtocol):
    # pylint: disable=too-few-public-methods
    """A Mixin to provide support for Kerneloscope to base_mr.BaseMR."""

    kerneloscope_server_url: str

    def fetch_data(self, cmd, kerneloscope_args=None) -> list:
        """Return rpc response data."""
        kerneloscope_rpc_url = parse.urljoin(self.kerneloscope_server_url, f'/rpc/{cmd}/')
        kerneloscope_args = kerneloscope_args or {}

        session = get_session("kernel-workflow.mixin.kerneloscope", raise_for_status=True)
        response = session.post(kerneloscope_rpc_url, json=kerneloscope_args)
        session.close()

        json_data = response.json()
        LOGGER.debug("Query %s (args: %s) returning: %s", cmd, kerneloscope_args, json_data)
        if 'result' in json_data:
            return json_data['result']
        if 'error' in json_data:
            LOGGER.info("Query %s returned an error: %s", cmd, json_data['error'])
        return []

    @cached_property
    def kerneloscope_trees(self) -> list[str]:
        """Return the list of trees known to the kerneloscope instance."""
        return self.fetch_data('get_trees')

    @property
    def kerneloscope_tree(self) -> str:
        """Return the kerneloscope tree that closest matches target gitlab namespace/branch."""
        # Doesn't matter which one we grab between rhel-x.y and rhel-x.y.z
        fix_version = self.rh_branch.fix_versions[0]
        find_longest_common_substring = LCSStr()
        all_trees = ", ".join(tree['name'] for tree in self.kerneloscope_trees)
        return find_longest_common_substring(fix_version, all_trees)

    def find_already_backported(self, commit) -> list[dict]:
        """Return a list of commits backported in tree already (if any)."""
        kerneloscope_args = {'commit': commit,
                             'tree': self.kerneloscope_tree,
                             'commit_with_subject': False}
        return self.fetch_data('get_downstream', kerneloscope_args)

    def find_missing_fixes(self, commit) -> list[str]:
        """Return a list of commits that are fixes for the specified commit."""
        kerneloscope_args = {'tree': self.kerneloscope_tree,
                             'commits': [commit],
                             'commit_with_subject': False}
        fixes_data = self.fetch_data('get_missing_fixes', kerneloscope_args)
        return [entry['commit'] for entry in fixes_data if entry['added']
                and not entry['merge']]


@dataclass(repr=False)
class RHIssueMixin(MergeRequestProtocol):
    """A Mixin to provide support for RHIssue access to base_mr.BaseMR."""

    # Note: This Mixin _requires_ base_mr_mixins' CommitsMixin and DependsMixin to work properly.

    @property
    def all_rhissue_ids(self) -> set[str]:
        """Return the set of all the RHIssue IDs referenced in the MR."""
        if not self.description:
            return set()
        all_jiras = set()
        for desc in self.all_descriptions:
            all_jiras.update(desc.jira_tags)
            if hasattr(desc, 'depends_jiras'):
                all_jiras.update(desc.depends_jiras)
        return all_jiras

    @cached_property
    def rhissues(self) -> list['RHIssue']:
        """Return the list of all RHIssue objects derived from this MR."""
        # This should be any JIRA Issue called out in a JIRA: or Depends: tag in all the MRs
        # plus possibly the faux INTERNAL and/or UNTAGGED RHIssue.
        all_mrs = [self] + self.depends_mrs
        rhissues = make_rhissues(self.all_rhissue_ids, jira=self.session.jira, mrs=all_mrs)
        return rhissues

    @cached_property
    def zstream_clones(self) -> list['RHIssue']:
        """If y-stream, return the list of all RHIssue objects for z-stream clones."""
        if not self.rh_branch.ystream:
            return []

        clones = []
        for rhi in self.rhissues:
            clones.extend(rhi.zstream_clones)

        return clones

    @cached_property
    def linked_rhissues(self) -> list['RHIssue']:
        """Return the list of all linked RHIssue objects derived from this MR."""
        if self.is_build_mr:
            return []
        rhissues = []
        for rhi in self.rhissues:
            rhissues.extend(rhi.linked_rhissues)

        # Filter out issues that are already directly linked in the MR
        rhissues = [rhi for rhi in rhissues if rhi not in self.rhissues]
        return rhissues
