"""Update bugs to contain links to an MR and successful pipeline artifacts."""

from os import environ
import re
import sys
import typing
from xmlrpc.client import Fault

from bugzilla import BugzillaError
from cki_lib import logger
from cki_lib import misc
from gitlab.v4.objects.merge_requests import ProjectMergeRequest
from gitlab.v4.objects.pipelines import ProjectPipelineBridge
from gitlab.v4.objects.projects import Project

from webhook.rh_metadata import Branch

from . import common
from . import defs
from .description import Description
from .libjira import fetch_issues
from .libjira import remove_gitlab_link_in_issues
from .libjira import update_testable_builds
from .pipelines import BridgeJob
from .pipelines import PipelineType
from .rhissue import RHIssue
from .session import SessionRunner
from .session_events import GitlabBuildEvent
from .session_events import GitlabMREvent
from .session_events import GitlabNoteEvent
from .session_events import GitlabPipelineEvent

LOGGER = logger.get_logger('cki.webhook.buglinker')


def get_bugs(bzcon, bug_list):
    """Return a list of bug objects."""
    try:
        bz_results = bzcon.getbugs(bug_list)
        if not bz_results:
            LOGGER.info("getbugs() returned an empty list for these bugs: %s.", bug_list)
        return bz_results
    except BugzillaError:
        LOGGER.exception('Error getting bugs.')
        return None


def make_ext_bz_bug_id(mr_url):
    """Format the string needed for the tracker links."""
    namespace, mr_id = common.parse_mr_url(mr_url)
    return f"{namespace}/-/merge_requests/{mr_id}"


def update_bugzilla(bugs, mr_url, bzcon):
    """Filter input bug list and run bugzilla API actions."""
    ext_bz_bug_id = make_ext_bz_bug_id(mr_url)
    for action, bug_list in bugs.items():
        if not bug_list or action not in ('link', 'unlink'):
            continue
        if action == 'unlink':
            unlink_mr_from_bzs(bug_list, ext_bz_bug_id, bzcon)
        else:
            bugs = get_bugs(bzcon, bug_list)
            link_mr_to_bzs(bugs, ext_bz_bug_id, bzcon)


def bz_is_linked_to_mr(bug, ext_bz_bug_id):
    """Return the matching external tracker for the BZ, if any."""
    return next((tracker for tracker in bug.external_bugs if
                 tracker['type']['description'] == 'Gitlab' and
                 tracker['type']['url'] == defs.EXT_TYPE_URL and
                 tracker['ext_bz_bug_id'] == ext_bz_bug_id),
                None)


def unlink_mr_from_bzs(bugs, ext_bz_bug_id, bzcon):
    """Unlink the MR from the given list of BZs."""
    # Add any related kernel-rt bugs
    rt_bugs = {bug.id for bug in get_rt_cve_bugs(bzcon, bugs)}
    for bug in set(bugs).union(rt_bugs):
        LOGGER.info('Unlinking %s from BZ%s.', ext_bz_bug_id, bug)
        if not misc.is_production_or_staging():
            continue
        try:
            bzcon.remove_external_tracker(ext_type_description='Gitlab',
                                          ext_bz_bug_id=ext_bz_bug_id, bug_ids=bug)
        except BugzillaError:
            LOGGER.exception("Problem unlinking %s from BZ%s.", ext_bz_bug_id, bug)
        except Fault as err:
            if err.faultCode == 1006:
                LOGGER.warning('xmlrpc fault %d: %s', err.faultCode, err.faultString)
            else:
                raise


def get_kernel_bugs(bugs):
    """Filter out bugs which are not RHEL & a kernel component."""
    return [bug for bug in bugs if
            bug.product.startswith('Red Hat Enterprise Linux') and
            bug.component in ('kernel', 'kernel-rt')]


def _do_link_mr_to_bzs(bugs, ext_bz_bug_id, bzcon):
    if misc.is_production_or_staging():
        try:
            bzcon.add_external_tracker(bugs, ext_type_url=defs.EXT_TYPE_URL,
                                       ext_bz_bug_id=ext_bz_bug_id)
        except BugzillaError:
            LOGGER.exception("Problem adding tracker %s to BZs.", ext_bz_bug_id)


def link_mr_to_bzs(bugs, ext_bz_bug_id, bzcon):
    """Take a list of bugs and a MR# and update the BZ's ET with the MR url."""
    untracked_bugs = [bug for bug in bugs if not bz_is_linked_to_mr(bug, ext_bz_bug_id)]
    if not untracked_bugs:
        LOGGER.info("All bugs have an existing link to %s.", ext_bz_bug_id)
        return

    kernel_bugs = get_kernel_bugs(untracked_bugs)
    bug_list = [bug.id for bug in kernel_bugs]

    LOGGER.info("Need to add %s to external tracker list of these bugs: %s", ext_bz_bug_id,
                bug_list)
    _do_link_mr_to_bzs(bug_list, ext_bz_bug_id, bzcon)


BASIC_BZ_FIELDS = ['component',
                   'external_bugs',
                   'id',
                   'product',
                   'sub_component'
                   'summary'
                   ]

BASIC_BZ_QUERY = {'query_format': 'advanced',
                  'include_fields': BASIC_BZ_FIELDS,
                  'classification': 'Red Hat',
                  'component': 'kernel-rt'
                  }


def parse_cve_from_summary(summary):
    """Return a CVE ID from the string, or None."""
    result = re.match(r'^(\w* )?(?P<cve>CVE-\d{4}-\d{4,7})\s', summary)
    if result:
        return result.group('cve')
    return None


def get_rt_cve_bugs(bzcon, bug_list):
    """Identify whether bug_list BZs are CVEs and if so return matching kernel-rt bugs."""
    # Filter out any kernel-rt bugs given in the bug_list.
    bz_results = [bug for bug in get_bugs(bzcon, bug_list) if bug.component != 'kernel-rt']
    cve_set = set()
    for bug in bz_results:
        cve_id = parse_cve_from_summary(bug.summary)
        if not cve_id:
            continue
        cve_set.add(cve_id)

    if not cve_set:
        return []

    cve_query = {**BASIC_BZ_QUERY}
    cve_query['product'] = bz_results[0].product
    # Blocked by the CVE
    cve_query['f1'] = 'blocked'
    cve_query['o1'] = 'anywords'
    cve_query['v1'] = list(cve_set)
    # Is not CLOSED DUPLICATE.
    cve_query['f2'] = 'resolution'
    cve_query['o2'] = 'notequals'
    cve_query['v2'] = 'DUPLICATE'
    # Is the same version.
    cve_query['f3'] = 'version'
    cve_query['o3'] = 'equals'
    cve_query['v3'] = bz_results[0].version

    try:
        return bzcon.query(cve_query)
    except BugzillaError:
        LOGGER.exception('Error querying bugzilla.')
        return []


def post_to_bugs(bug_list, header, bridge_job, bzcon):
    # pylint: disable=too-many-branches
    """Submit post to given bugs."""
    LOGGER.info('Posting results for %s. bug_list is: %s', bridge_job.type.name, bug_list)
    filtered_bugs = []
    if not (bugs := get_bugs(bzcon, bug_list)):
        LOGGER.warning('No bug data found.')
        return None
    # If this is the RT pipeline and a CVE then we want to post to the kernel-rt bug.
    if bridge_job.type is PipelineType.REALTIME:
        if rt_bugs := get_rt_cve_bugs(bzcon, bug_list):
            LOGGER.info('Including CVE bugs %s', rt_bugs)
            bugs.extend(rt_bugs)
        else:
            LOGGER.info('No RT CVE bug data found.')

    # If this is an automotive pipeline then only post if the BZs' component is kernel-automotive.
    if bridge_job.type is PipelineType.AUTOMOTIVE:
        if not (bugs := [bug for bug in bugs if bug.component == 'kernel-automotive']):
            LOGGER.info("Ignoring automotive pipeline as no bug has 'automotive' subcomponent")
            return None

    # It is possible for there to be multiple 'success' events for a given pipeline
    # so we should check that we haven't already posted them to the bugs. Sigh.
    for bug in bugs:
        if bug.id not in filtered_bugs and not \
           comment_already_posted(bug, bridge_job.checkout_link):
            filtered_bugs.append(bug.id)
    if not filtered_bugs:
        LOGGER.info('This pipeline has already been posted to all relevant bugs.')
        return None

    post_text = header + bridge_job.artifacts_text
    LOGGER.info('Updating these bugs %s with comment:\n%s', filtered_bugs, post_text)
    if misc.is_production_or_staging():
        # Adding a comment with extra_private_groups is not possible with
        # python-bugzilla (it misses support for add_comment, you can only
        # update existing comments). Thus call the api directly using
        # JSON RPC.
        data = {
            "method": "Bug.add_comment",
            "version": "2.0",
            "params": {
                "id": 0,
                "comment": post_text,
                "is_private": 1,
                "minor_update": 0,
                "extra_private_groups": ["redhat_partner_engineers"]
            },
        }
        bz_url = bzcon.url.replace('xmlrpc.cgi', 'jsonrpc.cgi')
        sess = bzcon.get_requests_session()
        for bug in filtered_bugs:
            data["params"]["id"] = bug
            res = sess.post(url=bz_url, json=data,
                            headers={'Content-Type': 'application/json'})
            res.raise_for_status()
            rdict = res.json()
            LOGGER.debug('Added comment %s to bug %s', rdict["result"]["id"], bug)
    return bugs


def comment_already_posted(bug, checkout_link: str):
    """Return True if the pipeline results have already been posted to the given bug."""
    for comment in bug.getcomments():
        if comment['creator'] == environ['BUGZILLA_EMAIL'] and checkout_link in comment['text']:
            LOGGER.info('Excluding bug %s as pipeline was already posted in comment %s.', bug.id,
                        comment['count'])
            return True
    return False


def check_associated_mr(merge_request):
    """Return head pipeline ID or None based on whether we care about the MR."""
    if merge_request.work_in_progress:
        LOGGER.info("MR %s is marked work in progress, ignoring.", merge_request.iid)
        return None
    if merge_request.head_pipeline is None:
        LOGGER.info("MR %s has not triggered any pipelines? head_pipeline is None.",
                    merge_request.iid)
        return None

    pipeline_id = merge_request.head_pipeline.get('id')
    if not merge_request.head_pipeline.get('web_url', '').startswith(f'{defs.GITFORGE}/redhat/'):
        LOGGER.info("MR %s head pipeline #%s is not in the Red Hat namespace: %s",
                    merge_request.iid, pipeline_id, merge_request.head_pipeline.get('web_url'))
        return None

    return pipeline_id


def filter_bridge_jobs(
    jobs: dict[PipelineType, BridgeJob],
    branch: Branch,
) -> dict[PipelineType, BridgeJob]:
    """Return a filtered jobs dict."""
    filtered_jobs = {}
    for bridge_job in jobs.values():
        if not bridge_job.builds_valid:
            LOGGER.info("Ignoring job because it doesn't have valid builds: %s", bridge_job)
            continue
        filtered_jobs[bridge_job.type] = bridge_job
    # Only post the compat pipeline results if the centos pipeline has finished
    if filtered_jobs and PipelineType.RHEL_COMPAT in branch.pipelines:
        if PipelineType.CENTOS not in filtered_jobs:
            if popped_compat := filtered_jobs.pop(PipelineType.RHEL_COMPAT, None):
                LOGGER.info('Ignoring job as there are no centos pipeline results at this time: %s',
                            popped_compat)
    return filtered_jobs


def fetch_bridge_jobs(
    bridge_jobs_list: list[ProjectPipelineBridge],
) -> dict[PipelineType, BridgeJob]:
    """Return a dict of BridgeJobArtifacts."""
    jobs_dict = {}
    for bridge_job in bridge_jobs_list:
        bridge_job_data = BridgeJob(bridge_job)
        if bridge_job_data.type in jobs_dict:
            raise RuntimeError('Multiple bridge jobs have the same PipelineType!')
        jobs_dict[bridge_job_data.type] = bridge_job_data
    return jobs_dict


def set_targeted_testing_label(
    gl_project: Project,
    gl_mr: ProjectMergeRequest,
    needs_missing_tests_label: bool,
) -> None:
    """Add or remove the targeted testing label as needed.."""
    has_missing_tests_label = defs.TARGETED_TESTING_LABEL in gl_mr.labels
    LOGGER.info('%s label needed: %s, has: %s', defs.TARGETED_TESTING_LABEL,
                needs_missing_tests_label, has_missing_tests_label)
    if needs_missing_tests_label and not has_missing_tests_label:
        LOGGER.info('Adding %s label.', defs.TARGETED_TESTING_LABEL)
        common.add_label_to_merge_request(gl_project, gl_mr.iid, [defs.TARGETED_TESTING_LABEL])
    if not needs_missing_tests_label and has_missing_tests_label:
        LOGGER.info('Removing %s label.', defs.TARGETED_TESTING_LABEL)
        common.remove_labels_from_merge_request(gl_project, gl_mr.iid,
                                                [defs.TARGETED_TESTING_LABEL])


def post_mr_pipelines(
    bridge_jobs: list[BridgeJob],
    mr_description: Description,
    mr_url: str,
    header_str: str,
    footer_str: str,
) -> None:
    """Post artifact data to the given bugzillas and jira issues."""
    comments = {job.type: job.artifacts_text for job in bridge_jobs}
    comment_text = header_str + '\n'.join(dict(sorted(comments.items())).values()) + footer_str
    LOGGER.info('Full comment text:\n%s', comment_text)

    # Post a comment per-pipeline to each BZ.
    if mr_description.bugzilla:
        if not (bzcon := common.try_bugzilla_conn()):
            raise RuntimeError('No bugzilla connection!')
        ext_bz_bug_id = make_ext_bz_bug_id(mr_url)
        for bridge_job in bridge_jobs:
            bugs = post_to_bugs(mr_description.bugzilla, header_str, bridge_job, bzcon)
            # For kernel-rt we separately add external tracker links to those CVE BZs.
            if bridge_job.type is PipelineType.REALTIME and bugs:
                link_mr_to_bzs(bugs, ext_bz_bug_id, bzcon)

    # If there are not any jira_tags or they are not kwf-compatible then do nothing.
    if not mr_description.jira_tags or \
       not (mr_issues := fetch_issues(mr_description.jira_tags, filter_kwf=True)):
        LOGGER.info('MR description does not have any valid kwf issues, nothing to post to.')
        return
    # Sort the dict so the pipeline artifacts comment will be in a consistent order
    checkout_links = [job.checkout_link for job in bridge_jobs]

    rhissues = [RHIssue.new_from_ji(ji=ji, mrs=None) for ji in mr_issues]
    for rhi in rhissues:
        rhi.create_issue_links()
        mr_issues.extend([lrhi.ji for lrhi in rhi.linked_rhissues])
    update_testable_builds(mr_issues, comment_text, checkout_links)


def process_mr_pipeline(session: SessionRunner, gl_mr: ProjectMergeRequest) -> None:
    """Process the given MR's pipeline and possibly post the results."""
    if not check_associated_mr(gl_mr):
        return
    # If the MR doesn't have any BZs or RHIssues listed, then we have nothing to do.
    mr_description = Description(gl_mr.description)
    if not mr_description.bugzilla and not mr_description.jira_tags:
        LOGGER.info('No bugs nor JIRA issues found in MR description, nothing to do.')
        return
    branch = session.rh_projects.get_target_branch(gl_mr.project_id, gl_mr.target_branch)
    LOGGER.info('Found BZs %s and JIRA issues %s.',
                mr_description.bugzilla, mr_description.jira_tags)
    LOGGER.info("Branch '%s' uses pipelines: %s", branch.name, [b.name for b in branch.pipelines])
    gl_project = session.get_gl_project(branch.project.namespace)
    local_pipeline = gl_project.pipelines.get(gl_mr.head_pipeline.get('id'))

    # Get data from all bridge jobs.
    bridge_jobs = fetch_bridge_jobs(local_pipeline.bridges.list())
    # Set the TargetedTestingMissing label if any valid builds are missing all_sources_targeted.
    # disabled until the targeted testing is switched to subsystems
    # https://gitlab.com/groups/cki-project/-/epics/90
    # set_targeted_testing_label(
    #     gl_project, gl_mr,
    #     any(job.builds_valid and not job.all_sources_targeted for job in bridge_jobs.values())
    # )

    # Filter out jobs that we don't want to post to BZ/Jira.
    if not (bridge_jobs := filter_bridge_jobs(bridge_jobs, branch)):
        LOGGER.info('No ready BridgeJobs, nothing to do.')
        return

    # Actually do the work.
    header_str = (
        'The following Merge Request has pipeline job artifacts available:\n'
        '\n'
        f'Title: {gl_mr.title}\n'
        f'MR: {gl_mr.web_url}\n'
        f'MR Pipeline: {local_pipeline.web_url}\n'
        '\n'
        'The Repo URLs are *not* accessible from a web browser! '
        'They only function as a dnf or yum baseurl.\n'
        '\n'
        'Artifacts expire six weeks after creation. If artifacts are needed after that '
        "time please rerun the pipeline by visiting the MR's 'Pipelines' tab and clicking "
        "the 'Run pipeline' button.\n"
        '\n'
    )
    post_mr_pipelines(bridge_jobs.values(), mr_description, gl_mr.web_url,
                      header_str, session.comment.text_footer('updated'))


def process_gl_event(
    _: dict,
    session: SessionRunner,
    event: GitlabBuildEvent | GitlabNoteEvent | GitlabPipelineEvent,
    **__: typing.Any
) -> None:
    """Process a job or pipeline event."""
    LOGGER.info('Processing %s event for %s', event.kind.name, event.mr_url)
    # Quickly filter out build (job) events we don't care about.
    if event.kind is defs.GitlabObjectKind.BUILD:
        if event.body['build_status'] != 'success' or event.body['build_stage'] != 'setup':
            LOGGER.info("Ignoring event for pipeline %s with stage '%s' and status '%s'.",
                        event.body['pipeline_id'], event.body['build_stage'],
                        event.body['build_status'])
            return
    if not (gl_mr := event.gl_mr):
        LOGGER.info('This event was not resolved to an MR?')
        return
    process_mr_pipeline(session, gl_mr)


def process_mr_event(
    _: dict,
    session: SessionRunner,
    event: GitlabMREvent,
    **__: typing.Any
) -> None:
    """Process a gitlab MR event."""
    LOGGER.info('Processing %s event for %s', event.kind.name, event.mr_url)
    if event.draft_status.is_draft and not event.draft_status.changed:
        raise RuntimeError('Events for existing draft MRs are not supposed to get here!')

    # If the event shows the MR changed into draft then handle that like a closing MR.
    if is_closing := event.draft_status.into or event.closing:
        LOGGER.info('MR is closing or moved back to draft, unlinking.')

    # Get the dict of change sets.
    tag_changes = common.description_tag_changes(
        event.merge_request.get('description', ''), is_closing, event.changes
    )
    if not any(tag_changes.values()):
        LOGGER.info('No description tag changes found, nothing to do.')
        return
    LOGGER.info('Found changes: %s', tag_changes)

    # Do any jira unlinking.
    if tag_changes['unlink_rhissue']:
        remove_gitlab_link_in_issues(event.mr_url.id, event.namespace,
                                     tag_changes['unlink_rhissue'])
    # Do bugzilla linking/unlinking.
    if not (bzcon := common.try_bugzilla_conn()):
        return
    update_bugzilla(tag_changes, event.mr_url, bzcon)

    # If there are linked bzs/issues then run the artifact poster.
    if tag_changes['link'] | tag_changes['link_rhissue']:
        LOGGER.info('Sending MR event to artifact poster.')
        process_mr_pipeline(session, event.gl_mr)


HANDLERS = {
    GitlabBuildEvent: process_gl_event,
    GitlabMREvent: process_mr_event,
    GitlabNoteEvent: process_gl_event,
    GitlabPipelineEvent: process_gl_event
}


def main(args):
    """Run main loop."""
    common.init_sentry()
    parser = common.get_arg_parser('BUGLINKER')
    args = parser.parse_args(args)
    session = SessionRunner.new('buglinker', args=args, handlers=HANDLERS)
    session.run()


if __name__ == "__main__":
    main(sys.argv[1:])
