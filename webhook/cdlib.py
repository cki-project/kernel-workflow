"""Common commit/dependency library code that can be used by all webhooks."""
import difflib
import re
import typing

from cki_lib import logger
from gitdb.exc import BadName
from gitdb.exc import BadObject

from . import common
from . import defs

LOGGER = logger.get_logger(__name__)


def extract_files(diff):
    """Extract the list of files from the diff list."""
    filelist = []
    LOGGER.debug(diff)
    for path in diff:
        filelist.append(path['newPath'])
    LOGGER.debug(filelist)
    return filelist


def get_partial_diff(diff, filelist):
    """Extract partial diff, used for partial backport comparisons."""
    # this is essentially a reimplementation of the filterdiff binary in python

    in_wanted_file = False
    in_header = True
    hit = False
    partial_diff = ""
    header = []
    new_diff = []
    cache = []

    for line in diff.split('\n'):
        if line.startswith("diff --git "):
            in_wanted_file = False
            for path in filelist:
                if path in line:
                    in_wanted_file = True
                    continue

        if not in_wanted_file:
            continue

        # save the header stuff in case there is stuff to print
        if in_header:
            if line[0:2] == "@@":
                in_header = False
            else:
                header.append(line)
                continue

        # hit boundary, save cached patchlet, if it wasn't filtered
        if line[0:2] == "@@":
            if hit:
                # only print header if there is a hit
                new_diff.extend(header)
                new_diff.extend(cache)
                hit = False
                header = []

            # reset cache and start on new patchlet
            cache = [line]
            continue

        # auto save line
        cache.append(line)

        # nothing got flagged, must be important
        hit = True

    # flush final piece
    if hit:
        new_diff.extend(header)
        new_diff.extend(cache)

    for line in new_diff:
        partial_diff += f"{line}\n"

    return partial_diff


def search_compiled(line, re_list):
    """Actual regex matching happens here."""
    for item in re_list:
        match = item.search(line)
        if match:
            return True

    return False


def filter_diff(diff, strict_header):
    """Filter the diff, isolating only the kinds of differences we care about."""
    # pylint: disable=too-many-branches,too-many-statements
    # this works by going through each patchlet and filtering out as
    # much junk as possible.  If anything is left, consider it a hit and
    # save the _whole_ patchlet for later.  Proceed to next patchlet.

    in_header = True
    hit = False
    header = []
    new_diff = []
    cache = []
    diff_compiled = []
    old = ""
    new = ""

    # sort through rules to see if this chunk is worth saving
    # most of the filters are patch header junk.  Don't care if those
    # offsets are different.
    # The last filter is the most interesting: filter context differences
    # Basically that just filters out noise that changed around
    # the patch and not the parts of the patch that does anything.
    # IOW, the lines in a patch with _no_ ± in front.
    # Personal preference if that is interesting or not.

    diff_re = ["^[+|-]index ",
               "^[+|-]--- ",
               r"^[+|-]\+\+\+",
               "^[+|-]diff ",
               "^[+|-]$",
               "^[+|-]@@ ",
               "^[+|-]new file mode ",
               "^[+|-]deleted file mode ",
               "^[+|-]similarity index ",
               "^[+|-] "]

    for prefix in diff_re:
        diff_compiled.append(re.compile(prefix))

    # end pre-compile stuff

    for line in diff:

        # save the header stuff in case there is stuff to print
        if in_header:
            if line[0:2] == "@@":
                in_header = False
            else:
                header.append(line)
                continue

        # hit boundary, save cached patchlet, if it wasn't filtered
        if line[0:2] == "@@":
            if hit:
                # only print header if there is a hit
                new_diff.extend(header)
                new_diff.extend(cache)
                hit = False
                old = ""
                new = ""
                header = []

            # reset cache and start on new patchlet
            cache = [line]
            continue

        # auto save line
        cache.append(line)

        # filter lines
        if line[0] == " ":
            # patch context, ignore
            continue
        if search_compiled(line, diff_compiled):
            if not strict_header:
                # hit junk, skip
                continue
            # Check for differences in function being patched
            parts = line.split("@@ ")
            if line[0:4] == "-@@ " and len(parts) >= 3:
                old = parts[2]
                continue
            if line[0:4] == "+@@ " and len(parts) >= 3:
                new = parts[2]
            if old != new:
                hit = True
                continue
            # Check for differences in file being patched
            if "diff " in line:
                parts = line.split(" ")
                if line[0:12] == "-diff --git " and len(parts) >= 4:
                    old = parts[2] + parts[3]
                    continue
                if line[0:12] == "+diff --git " and len(parts) >= 4:
                    new = parts[2] + parts[3]
                if old != new:
                    hit = True

            continue

        # nothing got flagged, must be important
        hit = True

    # flush final piece
    if hit:
        new_diff.extend(header)
        new_diff.extend(cache)

    return new_diff


def get_submitted_diff(commit):
    """Generate a diff from data returned from gitlab rest api."""
    diff = ""
    filelist = []

    for path in commit:
        diff += f"diff --git a/{path['old_path']} b/{path['new_path']}\n"
        if path['new_file']:
            diff += f"new file mode {path['b_mode']}\n"
        if path['renamed_file']:
            diff += "similarity index xx%\n"
            diff += f"rename from {path['old_path']}\n"
            diff += f"rename to {path['new_path']}\n"
        if path['deleted_file']:
            diff += f"deleted file mode {path['a_mode']}\n"
        diff += f"index blahblah..blahblah {path['b_mode']}\n"
        if path['new_file']:
            diff += "--- /dev/null\n"
        else:
            diff += f"--- a/{path['old_path']}\n"
        if path['deleted_file']:
            diff += "+++ /dev/null\n"
        else:
            diff += f"+++ b/{path['new_path']}\n"
        diff += path['diff']
        if path['old_path'] not in filelist:
            filelist.append(path['old_path'])
        if path['new_path'] not in filelist:
            filelist.append(path['new_path'])

    return diff, filelist


def get_diff_from_gql(commit):
    """Generate a diff from GraphQL data returned for submitted patch."""
    diff = ""
    filelist = []

    for path in commit:
        diff += f"diff --git a/{path['oldPath']} b/{path['newPath']}\n"
        if path['newFile'] == "true":
            diff += f"new file mode {path['bMode']}\n"
        if path['renamedFile'] == "true":
            diff += "similarity index xx%\n"
            diff += f"rename from {path['oldPath']}\n"
            diff += f"rename to {path['newPath']}\n"
        if path['deletedFile'] == "true":
            diff += f"deleted file mode {path['aMode']}\n"
        diff += f"index blahblah..blahblah {path['bMode']}\n"
        if path['newFile'] == "true":
            diff += "--- /dev/null\n"
        else:
            diff += f"--- a/{path['oldPath']}\n"
        if path['deletedFile'] == "true":
            diff += "+++ /dev/null\n"
        else:
            diff += f"+++ b/{path['newPath']}\n"
        diff += path['diff']
        if path['oldPath'] not in filelist:
            filelist.append(path['oldPath'])
        if path['newPath'] not in filelist:
            filelist.append(path['newPath'])

    return diff, filelist


def compare_commits(adiff, bdiff, strict_header=False):
    """Perform interdiff of two patches' diffs."""
    interdiff = difflib.unified_diff(adiff.split('\n'), bdiff.split('\n'),
                                     fromfile='diff_a', tofile='diff_b', lineterm="")
    interesting = filter_diff(interdiff, strict_header)
    return interesting


def mr_get_diff_ids(mrequest):
    """Get the sorted list of diff ids in the MR."""
    diffs = mrequest.diffs.list(get_all=True)
    diff_ids = []
    for diff in diffs:
        diff_ids.append(diff.id)
    diff_ids.sort()
    return diff_ids


def mr_get_last_two_diff_ranges(mrequest, diff_ids):
    """Get the head and start sha plus created_at for the latest two diffs in MR."""
    old = {'head': '', 'start': '', 'created': ''}
    latest = {'head': '', 'start': '', 'created': ''}
    for diff_id in diff_ids:
        diff = mrequest.diffs.get(diff_id)
        old['head'] = latest['head']
        old['start'] = latest['start']
        old['created'] = latest['created']
        latest['head'] = diff.head_commit_sha
        latest['start'] = diff.start_commit_sha
        latest['created'] = diff.created_at

    return latest, old


def mr_dependencies(mrequest):
    """Check if the current MR revision has dependencies or not."""
    labels = mrequest.labels
    for label in labels:
        if label.startswith("Dependencies::"):
            dep_scope = label.split("::")[-1]
            if dep_scope not in defs.READY_SUFFIX:
                return dep_scope

    return None


def mr_get_latest_start_sha(mrequest, start_sha):
    """Get the latest Dependencies:: label, if present."""
    deps = mr_dependencies(mrequest)
    if deps is None:
        return start_sha

    return deps


def is_first_dep(commit, dep_sha):
    """Return true if this commit matches the Dependencies:: label's sha."""
    return commit.id.startswith(dep_sha)


def get_dependencies_data(mrequest):
    """Figure out if we have dependencies, and if so, where they start."""
    has_deps = False
    start_sha = mrequest.diff_refs['start_sha']
    dep_sha = mr_get_latest_start_sha(mrequest, start_sha)
    if start_sha != dep_sha:
        has_deps = True

    return has_deps, dep_sha


def mr_get_old_start_sha(mrequest, old, latest, cc_ts):
    """Get the starting sha for old MR diff."""
    start_sha = old['start']
    prior_label_exists = False
    for evtid in mrequest.resourcelabelevents.list(iterator=True):
        if not evtid.label:
            continue
        if evtid.label['name'].startswith("Dependencies::") and evtid.action == "remove":
            # ignore events newer than latest rev creation time
            if evtid.created_at > latest['created']:
                continue
            # ignore events newer than CodeChanged ts if cc_ts older than old rev
            if cc_ts and cc_ts < old['created'] and evtid.created_at > cc_ts:
                continue
            dep_scope = evtid.label['name'].split("::")[-1]
            evt_at = evtid.created_at
            if dep_scope != defs.READY_SUFFIX:
                start_sha = dep_scope
                prior_label_exists = True
            elif dep_scope == defs.READY_SUFFIX and str(evt_at) > old['created']:
                prior_label_exists = True

    if prior_label_exists:
        return start_sha

    return mr_get_latest_start_sha(mrequest, start_sha)


def extract_ucid(commit_message: list[str]) -> list[str]:
    """Extract upstream commit ID from the commit message."""
    gitref_list = []
    # Pattern for 'git show <commit>' (and git log) based backports
    gl_pattern = r'^(?P<pfx>commit) (?P<hash>[a-f0-9]{8,40})$'
    gitlog_re = re.compile(gl_pattern)
    # Pattern for 'git cherry-pick -x <commit>' based backports
    cp_pattern = r'^\((?P<pfx>cherry picked from commit) (?P<hash>[a-f0-9]{8,40})\)$'
    gitcherrypick_re = re.compile(cp_pattern)

    for line in commit_message.splitlines():
        gitref = gitlog_re.match(line) or gitcherrypick_re.match(line)
        if gitref:
            githash = gitref.group('hash')
            hash_prefix = gitref.group('pfx')
            if line == f"{hash_prefix} {githash}" and len(githash) == 40:
                if githash not in gitref_list:
                    gitref_list.append(githash)
                continue
            if githash not in gitref_list:
                gitref_list.append(githash)

    # We return empty arrays if no commit IDs are found
    LOGGER.debug("Found upstream refs: %s", gitref_list)
    return gitref_list


def get_diff_from_mr(mrequest, diff_ids, rev):
    """Extract an MR diff from the MR api."""
    rev_diff = None
    for diff_id in diff_ids:
        diff = mrequest.diffs.get(diff_id)
        if diff.head_commit_sha == rev['head']:
            rev_diff = get_submitted_diff(diff.diffs)

    if rev_diff is not None:
        return rev_diff[0]

    return None


def get_git_diff(repo, rev):
    """Extract an MR diff from git."""
    LOGGER.info('Generating diff: %s..%s, %s', rev['start'], rev['head'], rev['created'])

    try:
        rev_start = repo.commit(rev['start'])
        rev_head = repo.commit(rev['head'])
    except (BadObject, BadName, ValueError) as err:
        LOGGER.warning("Upstream gitref not found, aborting check - (%s)", err)
        return None

    rev_diff = repo.git.diff(rev_start, rev_head)

    return rev_diff


def get_last_code_changed_timestamp(mrequest, diff_ids):
    """Get the timestamp for the latest time code actually changed in the MR."""
    changed_rev = None
    timestamp = None
    labels = mrequest.labels
    for label in labels:
        if label.startswith(defs.CODE_CHANGED_PREFIX):
            changed_rev = label.split("::v")[-1]
            break

    if changed_rev is None:
        return None

    i = 0
    for diff_id in diff_ids:
        diff = mrequest.diffs.get(diff_id)
        timestamp = diff.created_at
        i += 1
        if int(i) == int(changed_rev):
            break

    return timestamp


def assemble_interdiff_markdown(diffs_list: typing.List[str]) -> typing.Union[str, None]:
    """Assemble interdiff output from diffs list."""
    # If the input list is empty, or full of empty things, then there is no markdown to generate.
    if not any(val for val in diffs_list):
        return None

    if len(diffs_list) > 100:
        return f"Interdiff contains ~{len(diffs_list)}+/- lines, re-review accordingly."

    interdiff = "```diff\n--- old version\n+++ new version\n"
    interdiff += "\n".join(diffs_list[2:])
    interdiff += "\n```\n"
    LOGGER.info("Interdiff:\n%s", interdiff)
    return interdiff


def get_filtered_changed_files(gl_mergerequest):
    """Walk all commits, excluding Dependencies, and build changed files list."""
    filelist = []

    diff_ids = mr_get_diff_ids(gl_mergerequest)
    for diff_id in diff_ids:
        diff = gl_mergerequest.diffs.get(diff_id)
        start_sha = diff.start_commit_sha

    start_sha = mr_get_latest_start_sha(gl_mergerequest, start_sha)

    for commit in gl_mergerequest.commits():
        if commit.id.startswith(start_sha):
            break
        diffs = commit.diff()
        for diff in diffs:
            filelist.append(diff['new_path'])

    filelist = set(filelist)
    filelist = list(filelist)
    filelist.sort()

    return filelist


def get_dependencies_label_scope(depcommit):
    """Set Dependencies:: label with scope of either OK or the sha of first dependent commit."""
    if depcommit is not None:
        scope = depcommit[:12]
    else:
        scope = defs.READY_SUFFIX

    return scope


def find_first_dependency_commit(project, merge_request):
    """Find the first commit in the MR that is for a Dependency."""
    dependencies = common.extract_dependencies(project, merge_request.description)
    if len(dependencies) == 0:
        return None, False

    has_dep_code = False
    first_dep_commit = None

    for commit in merge_request.commits():
        commit_ref = project.commits.get(commit.id)
        if len(commit_ref.parent_ids) > 1:
            if first_dep_commit is None:
                first_dep_commit = commit_ref.id
            continue
        bugs = common.extract_bzs(commit.message)
        for bug in bugs:
            if bug in dependencies:
                has_dep_code = True
                if first_dep_commit is None:
                    first_dep_commit = commit.id
        if first_dep_commit is not None:
            break

    return first_dep_commit, has_dep_code


def make_dependencies_label(gl_project, gl_mr) -> defs.Label:
    """Return the expected Dependencies:: label for given MR."""
    (depcommit, has_dep_code) = find_first_dependency_commit(gl_project, gl_mr)
    scope = get_dependencies_label_scope(depcommit)

    if not has_dep_code:
        if scope != defs.READY_SUFFIX:
            # MR with all dependent commits merged, but merge commits still present
            scope = f'{defs.READY_SUFFIX}::{scope}'

    return defs.Label(f'Dependencies::{scope}')


def is_rhdocs_commit(diff):
    """Check for commits from the rhdocs sub-tree."""
    files = extract_files(diff)
    for file in files:
        if file.startswith('redhat/rhdocs/'):
            return True
    return False


def is_rhconfig_commit(diff):
    """Check for commits from the rhdocs sub-tree."""
    files = extract_files(diff)

    configs = common.extract_config_from_paths(files)

    if configs:
        return True
    return False
