"""Module for representing MR comments."""
from dataclasses import dataclass
import datetime
import typing

from cki_lib.footer import Footer
from cki_lib.logger import get_logger

from webhook.cache import BaseCache
from webhook.defs import GitlabGID
from webhook.defs import GitlabURL
from webhook.rh_metadata import Webhook

if typing.TYPE_CHECKING:
    from webhook.graphql import GitlabGraph
    from webhook.session import BaseSession
    from webhook.users import User
    from webhook.users import UserCache

LOGGER = get_logger('cki.webhook.comment_cache')


@dataclass
class Comment:
    """A Gitlab comment."""

    class CommentBody(str):
        """Simple wrapper for a Comment body."""

        FOOTER_PREFIX = Footer.GITLAB_PREFIX

        @property
        def without_footer(self) -> str:
            """Return the CommentBody with the cki_lib.footer.Footer.gitlab_footer removed."""
            if (footer_start_index := self.find(f'{self.FOOTER_PREFIX}')) > 0:
                return self[:footer_start_index]
            return self

    author: 'User'
    body: CommentBody
    id: GitlabGID
    created_at: datetime.datetime
    updated_at: datetime.datetime

    @classmethod
    def new(cls, comment_data: dict, user_cache: 'UserCache') -> typing.Self:
        """Construct a Comment instances from the given input."""
        author = user_cache.get(comment_data['author'])
        return cls(author=author, **cls._parse_query_response(comment_data))

    @classmethod
    def _parse_query_response(cls, comment_data: dict) -> dict[str, typing.Any]:
        """Translate a MrComments query response into a dict of proper values."""
        parsed_data = {
            'body': cls.CommentBody(comment_data.get('body', '')),
            'id': GitlabGID(comment_data['id']),
            'updated_at': datetime.datetime.fromisoformat(comment_data['updatedAt'])
        }

        # In the non-production/staging updateNote mutation case the createdAt field won't exist.
        if 'createdAt' in comment_data:
            parsed_data['created_at'] = datetime.datetime.fromisoformat(comment_data['createdAt'])

        return parsed_data

    @classmethod
    def new_comments(cls, comments_data: list[dict], user_cache: 'UserCache') -> list[typing.Self]:
        """Construct a list of Comment instances from the given input."""
        return [cls.new(comment, user_cache) for comment in comments_data]

    def matches(self, usernames: typing.Collection[str], substring: str) -> bool:
        """Return True if this comment is by one of the usernames and contains the substring."""
        return self.author.username in usernames and substring in self.body

    def update(self, graphql: 'GitlabGraph', body_text: str) -> None:
        """Update this comment with the new body text."""
        parsed_data = self._parse_query_response(graphql.update_note(self.id, body_text))
        self.body = parsed_data['body']
        self.updated_at = parsed_data['updated_at']


CommentCacheEntry = dict[str, Comment | None]
CommentCacheData = dict[GitlabURL, CommentCacheEntry]


class CommentCache(BaseCache):
    """A cache of MR webhook comments."""

    def __init__(self, session: 'BaseSession', bot_usernames: list | None = None, **kwargs) -> None:
        """Set up the caches."""
        self.session: 'BaseSession' = session

        # A mapping of GitlabURLs to a mapping of webhook names and their Comments.
        self.data: CommentCacheData = {}

        # A mapping of GitlabURLs to its GitlabGID.
        self.mr_id_map: dict[GitlabURL, GitlabGID] = {}

        # When looking for webhook comments it is assumed the comment author username will
        # be one of these bot_usernames. If none are given then assume we are the bot :o.
        self.bot_usernames: list[str] = bot_usernames or [session.graphql.username]
        super().__init__(**kwargs)

    @property
    def current_webhook(self) -> Webhook:
        """Return the rh_metadata.Webhook associated with this session."""
        if not (webhook := getattr(self.session, 'webhook', None)):
            raise RuntimeError('No webhook_name provided and session has no webhook set.')
        return webhook

    def get_webhook_comment(self, mr_url: str, webhook_name: str = '') -> Comment | None:
        """Return the given webhook's Comment for the given MR, or None if not found."""
        # If the MR is not in the cache or the Comment value is None then fetch the MR comments.
        webhook_name = webhook_name or self.current_webhook.name
        mr_url = GitlabURL(mr_url)

        # If we don't have data for this MR or a comment for this webhook then cache comments.
        if mr_url not in self.data or not self.data[mr_url].get(webhook_name):
            self.cache_mr_comments(mr_url)

        return self.data[mr_url].get(webhook_name)

    @staticmethod
    def _matching_comment(
        comments: list[Comment],
        usernames: list[str],
        substring: str
    ) -> Comment | None:
        """Return the first matching Comment or None."""
        return next((cmnt for cmnt in comments if cmnt.matches(usernames, substring)), None)

    def _add_cache_entry(self, mr_url: str, mr_gid: str, comments_data: list) -> None:
        """Return a dict representing one MR's comment data."""
        hook_comments: CommentCacheEntry = {}

        # Stash the MR's global ID value so we can use it when making new comments.
        mr_url = GitlabURL(mr_url)
        self.mr_id_map[mr_url] = GitlabGID(mr_gid)

        # Create the Comment objects.
        mr_comments = \
            Comment.new_comments(comments_data, self.session.get_user_cache(mr_url.namespace))

        # For each hook find the matching comment using its comment_header.
        rh_project = self.session.rh_projects.get_project_by_namespace(mr_url.namespace)
        for webhook in rh_project.webhooks.values():
            if not webhook.comment_header:
                continue

            hook_comments[webhook.name] = \
                self._matching_comment(mr_comments, self.bot_usernames, webhook.comment_header)

        self.data[mr_url] = hook_comments

    def cache_mr_comments(self, mr_url: str) -> None:
        """Cache all the webhook comments for the given MR."""
        # Fetch all the MR comments along with the MR global ID and pass it all to _add_cache_entry.
        mr_url = GitlabURL(mr_url)
        mr_gid, comments_data = self.session.graphql.get_mr_comments(mr_url.namespace, mr_url.id)
        self._add_cache_entry(mr_url, mr_gid, comments_data)

    def update_comment(
        self,
        mr_url: str,
        label: str,
        body: str,
        webhook: Webhook | None = None,
        only_if_changed: bool = False
    ) -> bool:
        # pylint: disable=too-many-arguments,too-many-positional-arguments
        """Create a new comment or update the webhook's existing one for the given MR."""
        mr_url = GitlabURL(mr_url)
        webhook = webhook or self.current_webhook
        header_str = f'**{webhook.comment_header}**: ~"{label}"  \n\n'
        # Add the header bits, needed to compare with possible existing_comment if only_if_changed
        new_body = header_str + body
        webhook_footer = Footer(webhook)

        # If the MR already has a comment from this hook and it hasn't changed then do nothing
        # when only_if_changed is set.
        if existing_comment := self.get_webhook_comment(mr_url, webhook.name):
            if only_if_changed and existing_comment.body.without_footer.strip() == new_body.strip():
                LOGGER.info('Existing comment text matches new text, nothing to do:\n%s', body)
                return False

        # Add the footer bits.
        new_body += webhook_footer.gitlab_footer('updated' if existing_comment else 'created')

        # Update the existing comment or create a new one and store it in the cache.
        if existing_comment:
            existing_comment.update(self.session.graphql, new_body)
        else:
            new_data = self.session.graphql.create_note(self.mr_id_map[mr_url], new_body)
            new_comment = Comment.new(new_data, self.session.get_user_cache(mr_url.namespace))
            self.data[mr_url][webhook.name] = new_comment

        return True
