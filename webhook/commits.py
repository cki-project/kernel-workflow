"""Module to represent a Commit."""
from dataclasses import dataclass
from dataclasses import field
from dataclasses import fields
from datetime import datetime
from functools import cached_property
import typing

from cki_lib.logger import get_logger
from kwf_lib.common import GitlabURL
from kwf_lib.description import Description

from webhook.cdlib import compare_commits

if typing.TYPE_CHECKING:
    from gitlab.v4.objects.merge_requests import ProjectMergeRequest
    from gitlab.v4.objects.merge_requests import ProjectMergeRequestDiff
    from gitlab.v4.objects.projects import Project

    from webhook.base_mr import MergeRequestProtocol
    from webhook.session import BaseSession

LOGGER = get_logger('cki.webhook.commit')

NameEmail = tuple[str, str]

DependsMRs = typing.Iterable['MergeRequestProtocol']


class Diff(typing.NamedTuple):
    """Properties of a commit diff."""

    # https://docs.gitlab.com/ee/api/merge_requests.html#list-merge-request-diffs
    diff: str
    old_path: str
    new_path: str
    a_mode: str
    b_mode: str
    new_file: bool
    renamed_file: bool
    deleted_file: bool
    generated_file: bool | None = None  # a diff from graphql doesn't have this attribute.

    @property
    def affected_files(self) -> set[str]:
        """Return the set of affected files (new_path + old_path)."""
        files: set[str] = set()
        if self.old_path:
            files.add(self.old_path)
        if self.new_path:
            files.add(self.new_path)
        return files


@dataclass(repr=False, unsafe_hash=True)
class Commit:
    # pylint: disable=too-many-instance-attributes
    """Properties of a commit."""

    author: NameEmail
    committer: NameEmail
    date: datetime
    description: Description
    sha: str
    title: str

    _gl_project: 'Project'
    _diffs: list[Diff] | None = field(default=None, hash=False)
    _parent_ids: list[str] = field(default_factory=list, hash=False)

    def __post_init__(self):
        """Log it, maybe."""
        LOGGER.debug('Created %s', self)

    def __repr__(self) -> str:
        """Say hello."""
        values = [
            f'{self.short_sha} {self.title[:30]}',
            f'author={self.author}',
            f'committer={self.committer}'
        ]
        return f'{self.__class__.__name__}({", ".join(values)})'

    @classmethod
    def new(cls, gl_project: 'Project', data: dict[str, typing.Any]) -> typing.Self:
        """Construct a new Commit instance from the given REST API result."""
        # In REST we get the full commit message which includes the title, but we want the
        # description minus the title. This is rough but hopefully won't discard anything important.
        if 'message' in data:
            message_split = data.get('message', '').split('\n\n', 1)
            description = message_split[1] if len(message_split) == 2 else ''
        else:
            description = data.get('description', '')

        return cls(
            author=(data.get('author_name', ''), data.get('author_email', '')),
            committer=(data.get('committer_name', ''), data.get('committer_email', '')),
            date=datetime.fromisoformat(data['authored_date']),
            description=Description(description),
            sha=data.get('id', ''),
            title=data.get('title', ''),
            _gl_project=gl_project,
            _diffs=data.get('diffs'),
            _parent_ids=data.get('parent_ids', [])
        )

    @cached_property
    def diffs(self) -> list[Diff]:
        """Return the list of Diffs for this Commit, fetching them if needed."""
        if self._diffs is None:
            gl_diffs = self._gl_project.commits.get(self.sha, lazy=True).diff(get_all=True)
            self._diffs = [Diff(**diff) for diff in gl_diffs]

        return self._diffs

    @cached_property
    def parent_ids(self) -> list[str]:
        """Return the list of parent_id shas, fetching it if needed."""
        # REST projects/<project id>/merge_requests/<iid>/commits has a bug where the parent_ids
        # field is always empty. https://gitlab.com/gitlab-org/gitlab/-/issues/496665
        # At the same time, graphql API `Commit` does not have a `parent_ids` field at all!
        # So for now the REST projects/<project id>/repository/commits/<sha> endpoint is the only
        # way to have the parent_ids.
        if not self._parent_ids:
            gl_commit = self._gl_project.commits.get(self.sha)
            self._parent_ids = gl_commit.parent_ids

        return self._parent_ids

    @property
    def is_merge_commit(self) -> bool:
        """Return True if this is a merge commit (has more than one parent_id), otherwise False."""
        return len(self.parent_ids) > 1

    @property
    def short_sha(self):
        """Return the first 12 chars of the sha."""
        return self.sha[:12]

    @property
    def files(self) -> set[str]:
        """Return the list of affected files as derived from the Diffs."""
        return {file for diff in self.diffs for file in diff.affected_files}

    @property
    def unified_diff(self) -> str:
        """Return a "unified" diff string constructed from all the Commit's Diffs."""
        return _unified_diff(self.diffs)


RepositoryComparison = dict[tuple[str, str], dict]


@dataclass(repr=False, unsafe_hash=True)
class MrVersion:
    # pylint: disable=too-many-instance-attributes
    """A version of an MR."""

    id: int
    base_commit_sha: str
    head_commit_sha: str
    start_commit_sha: str
    patch_id_sha: str
    real_size: str
    created_at: datetime
    state: str

    mr_iid: int

    gl_project: 'Project'
    depends_mrs: DependsMRs

    def __repr__(self) -> str:
        """Say hello."""
        values = [
            f'id={self.id} ({self.base_commit_sha[:12]}…{self.head_commit_sha[:12]})',
            f'created={self.created_at.isoformat(timespec="seconds")}',
            f'state={self.state}'
        ]
        return f'{self.__class__.__name__}({", ".join(values)})'

    @classmethod
    def new(
        cls,
        gl_project: 'Project',
        gl_diff: 'ProjectMergeRequestDiff',
        depends_mrs: DependsMRs
    ) -> typing.Self:
        """Construct an MrVersion from the MR 'diffs' API response."""
        field_names = [dc_field.name for dc_field in fields(cls)]

        params = \
            {k: v for k, v in gl_diff.asdict(with_parent_attrs=True).items() if k in field_names}
        params['created_at'] = datetime.fromisoformat(gl_diff.created_at)
        params['mr_iid'] = int(gl_diff.mr_iid)
        params['real_size'] = gl_diff.real_size

        return cls(**params, gl_project=gl_project, depends_mrs=depends_mrs)

    @cached_property
    def _version_diff(self) -> 'ProjectMergeRequestDiff':
        """Return the full ProjectMergeRequestDiff for this version."""
        return self.gl_project.mergerequests.get(self.mr_iid, lazy=True).diffs.get(self.id)

    @cached_property
    def _version_diff_without_depends(self) -> dict[str, typing.Any]:
        """Return a repository comparison from the first non-dependency SHA to the HEAD."""
        start_sha = self.first_dep_sha or self.base_commit_sha
        head_sha = self.head_commit_sha

        return self.gl_project.repository_compare(start_sha, head_sha)

    @cached_property
    def commits(self) -> list[Commit]:
        """Return the list of all Commit objects associated with this MrVersion with sha as key."""
        return [Commit.new(self.gl_project, commit) for commit in self._version_diff.commits]

    @cached_property
    def diffs(self) -> list[Diff]:
        """Return the list of all Diff objects associated with this MrVersion."""
        return [Diff(**raw_diff) for raw_diff in self._version_diff.diffs]

    @cached_property
    def files(self) -> list[str]:
        """Return the list of files affected by this MrVersion's diffs."""
        return list({path for diff in self.diffs for path in diff.affected_files})

    @cached_property
    def deleted_files(self) -> list[str]:
        """Return the list of files deleted by this MrVersion's diffs."""
        return list({diff.old_path for diff in self.diffs if diff.deleted_file})

    @staticmethod
    def _first_dep_sha_index(mr_iid: int, commits: list[Commit], mr: 'MergeRequestProtocol') -> int:
        """Return the index of the first commit which has a reference to the given MR, or -1."""
        for index, commit in enumerate(commits):
            if commit.sha == mr.head_sha:
                LOGGER.debug('!%d commit %s is HEAD of Depends: !%d',
                             mr_iid, commit.short_sha, mr.iid)
                return index

            if commit.description.jira & mr.kwf_description.jira:
                LOGGER.debug('!%d commit %s has JIRA tag matching Depends: !%d',
                             mr_iid, commit.short_sha, mr.iid)
                return index

        return -1

    @cached_property
    def commits_without_depends(self) -> list[Commit]:
        """Return the list of Commits up to (excluding) the first which matches a dependency."""
        if not self.depends_mrs:
            return self.commits

        first_dep_sha_index: int = -1

        for dep_mr in self.depends_mrs:
            dep_mr_index = self._first_dep_sha_index(self.mr_iid, self.commits, dep_mr)

            if dep_mr_index > -1:
                first_dep_sha_index = dep_mr_index if first_dep_sha_index < 0 else \
                    min(dep_mr_index, first_dep_sha_index)

        return self.commits[:first_dep_sha_index] if first_dep_sha_index > -1 else self.commits

    @cached_property
    def diffs_without_depends(self) -> list[Diff]:
        """Return the list of Diff objects without dependency content."""
        if not self.depends_mrs:
            return self.diffs

        return [Diff(**raw_diff) for raw_diff in self._version_diff_without_depends['diffs']]

    @cached_property
    def files_without_depends(self) -> list[str]:
        """Return the list of files affected without dependency content, if any."""
        if not self.depends_mrs:
            return self.files

        return list({path for diff in self.diffs_without_depends for path in diff.affected_files})

    @cached_property
    def deleted_files_without_depends(self) -> list[str]:
        """Return the list of files deleted without dependency content, if any."""
        if not self.depends_mrs:
            return self.deleted_files

        return list({diff.old_path for diff in self.diffs_without_depends
                     if diff.deleted_file})

    @cached_property
    def first_dep_sha(self) -> str | None:
        """Return the sha of the first dependency commit."""
        if not self.depends_mrs or not self.commits or self.commits_without_depends == self.commits:
            return None

        return self.commits[len(self.commits_without_depends)].sha

    @cached_property
    def unified_diff(self) -> str:
        """Return a "unified" diff string constructed from all the diffs_without_depends."""
        return _unified_diff(self.diffs_without_depends)


@dataclass
class VersionManager:
    """A manager for MrVersions."""

    session: 'BaseSession'
    gl_mr: 'ProjectMergeRequest' = field(repr=False)
    depends_mrs: DependsMRs

    @cached_property
    def latest(self) -> MrVersion | None:
        """Return the latest MrVersion, or None if there are none."""
        # I am not sure it if is even possible for there to be zero versions but :shrug:.
        return self.versions[0] if self.versions else None

    @cached_property
    def latest_code_changes(self) -> list[str]:
        """Return the list of code changes between the latest MrVersion and the prior, if any."""
        if len(self.versions) < 2:
            return []

        return self.compare_mr_versions(self.versions[0], self.versions[1])

    @cached_property
    def latest_commits(self) -> dict[str, Commit]:
        """Return a dict of the non-dependency Commits from the latest MrVersion."""
        return {c.sha: c for c in self.latest.commits_without_depends} if self.latest else {}

    @cached_property
    def latest_diffs(self) -> list[Diff]:
        """Return the list of the non-dependency Diffs from the latest MrVersion."""
        return self.latest.diffs_without_depends if self.latest else []

    @cached_property
    def latest_files(self) -> list[str] | None:
        """Return the list of the non-dependency files, or None if no latest MrVersion."""
        return self.latest.files_without_depends if self.latest else None

    @cached_property
    def latest_deleted_files(self) -> list[str] | None:
        """Return the list of the non-dependency deleted files, or None if no latest MrVersion."""
        return self.latest.deleted_files_without_depends if self.latest else None

    @cached_property
    def latest_first_dep_sha(self) -> str | None:
        """Return the first_dep_sha of the latest MrVersion, or None if no latest MrVersion."""
        return self.latest.first_dep_sha if self.latest else None

    @cached_property
    def versions(self) -> list[MrVersion]:
        """Return the list of MrVersion objects for this MR."""
        url = GitlabURL(self.gl_mr.web_url)

        gl_project = self.session.get_gl_project(url.namespace, gl_host=url.base_url)
        gl_diffs = typing.cast(list['ProjectMergeRequestDiff'], self.gl_mr.diffs.list(get_all=True))

        return [MrVersion.new(gl_project, gl_diff, self.depends_mrs) for gl_diff in gl_diffs]

    @staticmethod
    def compare_mr_versions(v1: MrVersion, v2: MrVersion) -> list[str]:
        """Return the list of changes found between the two MrVersions."""
        if v1.head_commit_sha == v2.head_commit_sha:
            return []

        return compare_commits(
            _unified_diff(v1.diffs_without_depends),
            _unified_diff(v2.diffs_without_depends)
        )

    @cached_property
    def latest_code_changes_version(self) -> int:
        """Retain the # of the latest MrVersion which has a meaningful code change."""
        version_count = len(self.versions)
        # If the MR doesn't have at least 2 versions then there isn't much to do.
        if version_count < 2:
            return version_count

        # Loop through each MrVersion (minus the last one) and compare it to the prior. If there are
        # real code changes then this is the 'version' we want to set in the CodeChanged::v# label.
        version_with_changes = 1
        for index in range(version_count - 1):
            current_version = self.versions[index]
            previous_version = self.versions[index+1]

            if self.compare_mr_versions(current_version, previous_version):
                version_with_changes = version_count
                break

            version_count -= 1

        return version_with_changes


def _unified_diff(diffs: typing.Iterable[Diff]) -> str:
    """Return a "unified" diff string constructed from all the given Diffs."""
    # This is basically a copy of cdlib.get_submitted_diff() using attributes instead of keys.
    diff_str = ''

    for diff in diffs:
        diff_str += f"diff --git a/{diff.old_path} b/{diff.new_path}\n"
        if diff.new_file:
            diff_str += f"new file mode {diff.b_mode}\n"
        if diff.renamed_file:
            diff_str += "similarity index xx%\n"
            diff_str += f"rename from {diff.old_path}\n"
            diff_str += f"rename to {diff.new_path}\n"
        if diff.deleted_file:
            diff_str += f"deleted file mode {diff.a_mode}\n"
        diff_str += f"index blahblah..blahblah {diff.b_mode}\n"
        if diff.new_file:
            diff_str += "--- /dev/null\n"
        else:
            diff_str += f"--- a/{diff.old_path}\n"
        if diff.deleted_file:
            diff_str += "+++ /dev/null\n"
        else:
            diff_str += f"+++ b/{diff.new_path}\n"
        diff_str += diff.diff

    return diff_str
