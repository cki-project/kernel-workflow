# pylint: disable=too-many-lines

"""Common code that can be used by all webhooks."""
import argparse
from datetime import datetime
from math import ceil
import os
from random import uniform
import re
from time import sleep
import typing
from urllib import parse

import bugzilla
from cki_lib import gitlab
from cki_lib import logger
from cki_lib import misc
from cki_lib import owners
from cki_lib.messagequeue import MessageQueue
from cki_lib.misc import get_nested_key
from cki_lib.misc import sentry_init
from gitlab.const import MAINTAINER_ACCESS
from gitlab.exceptions import GitlabAuthenticationError
from gitlab.exceptions import GitlabCreateError
from gitlab.exceptions import GitlabGetError
import sentry_sdk

from webhook import libjira
from webhook.description import Description
from webhook.rhissue import RHIssue
from webhook.temp_utils import load_yaml_data

from . import defs

if typing.TYPE_CHECKING:
    from gitlab.client import Gitlab
    from gitlab.v4.objects.merge_requests import ProjectMergeRequest
    from gitlab.v4.objects.projects import Project


LOGGER = logger.get_logger(__name__)

gitlab.PER_PAGE = 100

WAIT_FOR_NEW_MRS_DELAY_MAX = 12
WAIT_FOR_NEW_MRS_DELAY_RANGE = 6


def init_sentry() -> None:
    """Initialize sentry."""
    sentry_init(sentry_sdk)


def get_arg_parser(webhook_prefix):
    """Intialize a commandline parser.

    Returns: argparse parser.
    """
    parser = argparse.ArgumentParser(
        description='Manual handling of merge requests')
    parser.add_argument('--merge-request',
                        help='Process given merge request URL only')
    parser.add_argument('-j', '--jira',
                        help='Jira issue to operate on.')
    parser.add_argument('--action', default='',
                        help='Action for the MR when using URL only')
    parser.add_argument('--json-message-file', default='',
                        help='Process a single JSON message in a file')
    parser.add_argument('--json-message-type', default='gitlab',
                        help='The type of message contained in the --json-message-file')
    parser.add_argument('--oldrev', action='store_true',
                        help='Treat this as changed MR when using URL only')
    parser.add_argument('--note',
                        help='Process a note for the given merge request')
    parser.add_argument('--disable-user-check', action='store_true',
                        help="Don't check if the API user matches the event creator.")
    parser.add_argument('--disable-inactive-branch-check', action='store_true',
                        help="Don't check if the event target branch is inactive.")
    parser.add_argument('--disable-closed-status-check', action='store_true',
                        help="Don't check if the event comes from a closed/merged object.")
    parser.add_argument('--disable-environment-check', action='store_true',
                        help="Don't check the type of environment we are in (production/staging).")
    parser.add_argument('--rabbitmq-host', default=os.environ.get('RABBITMQ_HOST', 'localhost'))
    parser.add_argument('--rabbitmq-port', type=int,
                        default=misc.get_env_int('RABBITMQ_PORT', 5672))
    parser.add_argument('--rabbitmq-user', default=os.environ.get('RABBITMQ_USER', 'guest'))
    parser.add_argument('--rabbitmq-password',
                        default=os.environ.get('RABBITMQ_PASSWORD', 'guest'))
    parser.add_argument('--rabbitmq-exchange', default=os.environ.get('WEBHOOK_RECEIVER_EXCHANGE',
                                                                      'cki.exchange.webhooks'))
    parser.add_argument('--rabbitmq-routing-key',
                        default=os.environ.get(f'{webhook_prefix}_ROUTING_KEYS'),
                        help='RabbitMQ routing key. Required when processing queue.')
    parser.add_argument('--rabbitmq-queue-name',
                        default=os.environ.get(f'{webhook_prefix}_QUEUE'),
                        help='RabbitMQ queue name. Required when processing queue.')
    parser.add_argument('--payload-project-id', default=123, type=int,
                        help='Project id to set in fake event payload (default is 123).')
    parser.add_argument('--payload-target-branch', default='main',
                        help="Target branch to set in fake event payload (default is 'main').")
    parser.add_argument('--owners-yaml', default=os.environ.get('OWNERS_YAML', ''),
                        help='Path to the owners.yaml file')
    parser.add_argument('--repo-path', default=os.getenv('REPO_PATH'),
                        help='Local path where the git mirror repo is stored')
    parser.add_argument('--mirror-list-path',
                        default=os.getenv('MIRROR_LIST_PATH', defs.DEFAULT_MIRROR_LIST_PATH),
                        help='Path to yaml file describing git mirror repo layout')

    return parser


def prepare_mr_url(url):
    """Parse the merge request URL used for manual handlers.

    Args:
        url: Full merge request URL.

    Returns:
        A tuple of (gitlab_instance, mr_object, project_path_with_namespace).
    """
    url_parts = parse.urlsplit(url)
    instance_url = parse.urlunparse(url_parts[:2] + ('',) * 4)
    gl_instance = gitlab.get_instance(instance_url)
    match = re.match(r'/(.*)/merge_requests/(\d+)', url_parts.path)
    project_path = re.sub('/-$', '', match[1])
    gl_project = gl_instance.projects.get(project_path)
    gl_mergerequest = gl_project.mergerequests.get(int(match[2]))

    return gl_instance, gl_project, gl_mergerequest, project_path


def parse_mr_url(url):
    """Return a tuple with the path_with_namespace and MR ID from the given URL."""
    url_parts = parse.urlsplit(url)
    path, mr_id = url_parts.path.split('/-/merge_requests/')
    return path[1:], int(mr_id)


def mr_is_closed(merge_request):
    """We really don't want to run on closed MRs, add common check function."""
    return merge_request.state == "closed"


def mr_is_merged(merge_request):
    """We really don't want to run on closed MRs, add common check function."""
    return merge_request.state == "merged"


def get_mr_state_from_event(payload):
    """Return the state of the MR indicated by the event, or None."""
    return (misc.get_nested_key(payload, 'object_attributes/state') or
            misc.get_nested_key(payload, 'merge_request/state'))


def get_payload_username(payload):
    """Return the username from the payload."""
    if payload['object_kind'] == 'push':
        return payload['user_username']
    return payload['user']['username']


def wait_for_new_mrs(payload):
    """Sleep if the payload indicates the MR was created more than MAX_DELAY seconds ago."""
    key = 'object_attributes' if payload.get('object_kind') == 'merge_request' else 'merge_request'
    if not (created_at := get_nested_key(payload, f'{key}/created_at')):
        LOGGER.info("Payload '%s' does not have MR data, not sleeping.", payload['object_kind'])
        return
    timestamp = datetime.fromisoformat(created_at[:19])
    delta = timestamp.now() - timestamp
    delay = ceil(WAIT_FOR_NEW_MRS_DELAY_MAX - delta.total_seconds())
    LOGGER.info('Event MR is %s seconds old.', round(delta.total_seconds()))
    if 0 <= delay <= WAIT_FOR_NEW_MRS_DELAY_MAX:
        delay = round(uniform(delay, delay + WAIT_FOR_NEW_MRS_DELAY_RANGE), 2)
        LOGGER.info('Sleeping for %s seconds…', delay)
        sleep(delay)


def get_messagequeue(args, **kwargs):
    """Return a new message queue instance."""
    return MessageQueue(host=args.rabbitmq_host, port=args.rabbitmq_port, user=args.rabbitmq_user,
                        password=args.rabbitmq_password, **kwargs)


def get_argparse_environ_opts(key, is_list=False):
    """Read default value for argparse from an environment variable if present."""
    val = os.environ.get(key)
    val = val.split() if val and is_list else val
    return {'default': val} if val else {'required': True}


def print_notes(notes):
    """Print the notes section of upstream commit ID report."""
    report = ""
    noteid = 0
    while noteid < len(notes):
        report += f"{noteid + 1}. "
        if notes[noteid] is None:
            noteid += 1
            continue
        report += notes[noteid].rstrip("\n").replace("\n", "\n   ")
        report += "\n"
        noteid += 1
    report += "\n" if report else ""
    report = report.replace("<", "&lt;")
    report = report.replace(">", "&gt;")
    report = report.replace("\n   ", "<br>&emsp;")
    return report


def commits_have_not_changed(payload):
    """Return True if the commits in the MR have not changed."""
    action = payload['object_attributes']['action']
    if action == 'update' and 'oldrev' not in payload['object_attributes']:
        return True
    return False


def mr_action_affects_commits(payload):
    """Return True if the message indicates there has been any change to the MR's commits."""
    # Some messages have no action? Not sure why but a KeyError won't get us anywhere.
    action = payload['object_attributes'].get('action')
    if action is None:
        LOGGER.warning("Message payload does not have an 'action'.")
        return None

    # Check to see if the target branch was changed.
    if action == 'update' and get_nested_key(payload, 'changes/merge_status'):
        return True

    # True if action is 'open' or, action is 'update' and 'oldrev' is set.
    if commits_have_not_changed(payload):
        LOGGER.debug("Ignoring MR \'update\' action without an oldrev.")
        return False
    if action not in ('update', 'open'):
        LOGGER.debug("Ignoring MR action '%s'.", action)
        return False
    return True


def build_note_string(notes):
    """Build note string for report table."""
    notestr = ", ".join(notes)
    notestr = "See " + notestr + "|\n" if notestr else "-|\n"
    return notestr


def build_commits_for_row(row):
    """Build list of commits for a row in report table."""
    commits = row[1] if len(row[1]) < 2 else row[1][:2] + ["(...)"]
    count = 0
    while count < len(commits):
        commits[count] = commits[count][:8]
        count += 1
    return commits


def cancel_pipeline(gl_project, pipeline_id):
    """Cancel the given pipeline, return True if it seems to work, otherwise False."""
    if not misc.is_production_or_staging():
        LOGGER.info('Pretending to cancel pipeline %s.', pipeline_id)
        return True
    canceled = gl_project.pipelines.get(pipeline_id).cancel()
    if canceled['status'] == 'canceled':
        LOGGER.info('Canceled pipeline %s', pipeline_id)
        return True
    LOGGER.error('Failed to cancel pipeline %s', pipeline_id)
    return False


def create_mr_pipeline(gl_mr):
    """Create a new pipeline for the given MR and return the new pipeline's ID."""
    # In the non-production case return the MR IID.
    if not misc.is_production_or_staging():
        LOGGER.info('Pretending to trigger a new pipeline for MR %s', gl_mr.iid)
        return gl_mr.iid
    new_pipeline = gl_mr.pipelines.create()
    LOGGER.info('Created new pipeline %s for MR %s', new_pipeline.id, gl_mr.iid)
    return new_pipeline.id


def create_label_object(name, color, description):
    """Return an object ready to pass to add_label_to_merge_request in a list."""
    return {'name': name, 'color': color, 'description': description}


def _get_gitlab_group(gl_project):
    """Get the gitlab group object for this project/MR."""
    return gl_project.manager.gitlab.groups.get(gl_project.namespace['full_path'])


def _edit_label(gl_project_or_group, existing_label, new_label):
    """Check if a label needs updating. Creates the label if it does not exist."""
    if existing_label:
        # If the label exists then confirm the existing properties match the new label values.
        label_changed = False
        for item in new_label:
            if new_label[item] != getattr(existing_label, item):
                setattr(existing_label, item, new_label[item])
                label_changed = True
        if label_changed:
            LOGGER.info('Editing label %s on %s.', existing_label.name, gl_project_or_group.path)
            if misc.is_production_or_staging():
                existing_label.save()
    else:
        LOGGER.info('Creating label %s on %s.', new_label, gl_project_or_group.path)
        if misc.is_production_or_staging():
            try:
                gl_project_or_group.labels.create(new_label)
            except GitlabCreateError as err:
                if err.response_code == 409 and "Label already exists" in err.error_message:
                    LOGGER.info('%s: %s.', err.error_message, new_label['name'])
                else:
                    raise


def _match_label(project, target_label, label_list=None):
    """Return the ProjectLabel object whose name matches the target."""
    if not label_list:
        label_list = project.labels.list(search=target_label)
    return next((label for label in label_list if label.name == target_label), None)


def _add_label_quick_actions(gl_project, label_list, *, level=None):
    if level is None:
        level = 'project' if gl_project.id == defs.ARK_PROJECT_ID else 'group'
    # Use /label quick action to add the label to the merge request. This requires ensuring that
    # the label is available on the project.
    label_cmds = []
    # If we're only operating on a few labels then don't bother downloading
    # the project's entire label list, just search for them one at a time in _match_label().
    all_labels = gl_project.labels.list(iterator=True) if len(label_list) >= 5 else None
    for label in label_list:
        existing_label = _match_label(gl_project, label['name'], all_labels)
        if level == 'project':
            _edit_label(gl_project, existing_label, label)
        else:
            _edit_label(_get_gitlab_group(gl_project), existing_label, label)
        label_cmds.append(f"/label \"{label['name']}\"")
    return label_cmds


def ensure_gl_instance_auth(gl_instance: 'Gitlab') -> str:
    """Ensure the given gitlab.client.Gitlab instance is authorized and return the username."""
    if not gl_instance.user:
        try:
            gl_instance.auth()
            assert gl_instance.user is not None
        except (GitlabAuthenticationError, AssertionError) as exc:
            raise RuntimeError("Authentication failure!") from exc
    return gl_instance.user.username


def _update_bot_approval_status(gl_project: 'Project', mr_id: int, approved: bool = False) -> None:
    """Approve or unapprove the given MR."""
    # We need to be authed to do this!
    bot = ensure_gl_instance_auth(gl_project.manager.gitlab)
    gl_mr = gl_project.mergerequests.get(mr_id, lazy=True)
    LOGGER.info("%s %s MR %s (via %s)", "Approving" if approved else "Unapproving",
                gl_project.name, mr_id, bot)
    if misc.is_production_or_staging():
        approved_by = list(u['user']['username'] for u in gl_mr.approvals.get().approved_by)
        if approved and bot not in approved_by:
            gl_mr.approve()
        elif not approved and bot in approved_by:
            gl_mr.unapprove()


def set_release_milestone_values(gl_mr: 'ProjectMergeRequest', update_values: list[dict]):
    """Set the jira issue(s) release milestone checkbox values."""
    if gl_mr.draft:
        LOGGER.info("Not updating JIRA Issue Release Milestone for MR %s, because it's a Draft.",
                    gl_mr.iid)
        return

    mr_desc = Description(gl_mr.description)
    if not (issues := mr_desc.jira_tags):
        LOGGER.info('No JIRA Issues to update for %s', str(gl_mr))
        return
    # If the tagged jira isn't a kernel* comp. or is the wrong type then we shouldn't be doing this.
    if not (mr_issues := libjira.fetch_issues(issues, filter_kwf=True)):
        return
    mr_rhissues = [RHIssue.new_from_ji(ji=ji, mrs=None) for ji in mr_issues]
    for rhi in mr_rhissues:
        mr_issues.extend([lrhi.ji for lrhi in rhi.linked_rhissues])

    for rhi in mr_rhissues:
        LOGGER.debug("Updating Issue %s Release Milestone field from: %s",
                     rhi.id, rhi.release_milestone)
        libjira.update_issue_field(rhi.ji, defs.JiraField.Release_Milestone, update_values)


def _update_jira_release_milestone_field(gl_mr: 'ProjectMergeRequest', current_labels: list[str]):
    release_milestone_values = dict.fromkeys(defs.RELEASE_MILESTONE_BOXES, False)
    submitter_deps = set(defs.SUBMITTER_DEPENDENCIES)
    if submitter_deps.issubset(set(current_labels)):
        release_milestone_values['submitter_checks_pass'] = True

    if f'CKI::{defs.TESTING_FAILED_SUFFIX}' in current_labels:
        release_milestone_values['cki_tests_done'] = True
    elif f'CKI::{defs.TESTING_WAIVED_SUFFIX}' in current_labels:
        release_milestone_values['cki_tests_done'] = True
        release_milestone_values['cki_tests_pass'] = True
    elif f'CKI::{defs.READY_SUFFIX}' in current_labels:
        release_milestone_values['cki_tests_done'] = True
        release_milestone_values['cki_tests_pass'] = True

    if f'Acks::{defs.READY_SUFFIX}' in current_labels:
        release_milestone_values['mr_approved'] = True

    update_values = []
    for key, value in release_milestone_values.items():
        if value:
            update_values.append({'id': defs.RELEASE_MILESTONE_BOXES[key]})
    set_release_milestone_values(gl_mr, update_values)


def _compute_mr_status_labels(gl_project, gl_mr, label_cmds):
    if gl_project.id == defs.ARK_PROJECT_ID:
        ready_deps = set(defs.ARK_READY_FOR_MERGE_DEPS)
    else:
        ready_deps = set(defs.READY_FOR_MERGE_DEPS)
    ready_for_merge_deps = ready_deps
    ready_for_qa_deps = set(defs.READY_FOR_QA_DEPS)
    current_labels = gl_mr.labels
    # special handling for Dependencies::OK::<sha>
    for label in current_labels:
        if label.startswith(f"Dependencies::{defs.READY_SUFFIX}::"):
            current_labels.append(f"Dependencies::{defs.READY_SUFFIX}")
            break

    # special handling for Merge::Warning label to make it non-blocking
    if defs.MERGE_WARNING_LABEL in current_labels:
        current_labels.append(f"Merge::{defs.READY_SUFFIX}")

    # Only an MR which is not a draft and not blocked is eligible for readyForMerge.
    not_draft_or_blocked = not gl_mr.draft and defs.BLOCKED_LABEL not in gl_mr.labels

    # Update jira issue(s) Release Milestone field checkbox values
    _update_jira_release_milestone_field(gl_mr, current_labels)

    if not_draft_or_blocked and ready_for_merge_deps.issubset(set(current_labels)):
        _update_bot_approval_status(gl_project, gl_mr.iid, approved=True)
        if defs.READY_FOR_MERGE_LABEL not in current_labels:
            merge_label = validate_labels([defs.READY_FOR_MERGE_LABEL])
            label_cmds += _add_label_quick_actions(gl_project, merge_label)
            gl_mr.labels.append(defs.READY_FOR_MERGE_LABEL)
            label_cmds.append(f'/unlabel "{defs.READY_FOR_QA_LABEL}"')
    elif not gl_mr.draft and ready_for_qa_deps.issubset(set(current_labels)):
        if defs.READY_FOR_QA_LABEL not in current_labels:
            qa_label = validate_labels([defs.READY_FOR_QA_LABEL])
            label_cmds += _add_label_quick_actions(gl_project, qa_label)
            gl_mr.labels.append(defs.READY_FOR_QA_LABEL)
            label_cmds.append(f'/unlabel "{defs.READY_FOR_MERGE_LABEL}"')
    elif defs.READY_FOR_MERGE_LABEL in current_labels \
            or defs.READY_FOR_QA_LABEL in current_labels:
        _update_bot_approval_status(gl_project, gl_mr.iid, approved=False)
        label_cmds.append(f'/unlabel "{defs.READY_FOR_MERGE_LABEL}"')
        label_cmds.append(f'/unlabel "{defs.READY_FOR_QA_LABEL}"')
    return label_cmds


def _run_label_commands(gl_mergerequest, label_cmds):
    if label_cmds:
        _update_jira_state(gl_mergerequest, label_cmds)
        LOGGER.info('Modifying labels on merge request %s: %s', gl_mergerequest.iid,
                    ' '.join(label_cmds))
        if misc.is_production_or_staging():
            try:
                gl_mergerequest.notes.create({'body': '\n'.join(label_cmds)})
            except GitlabCreateError as err:
                if err.response_code == 400 and "can't be blank" in err.error_message:
                    LOGGER.exception('Unexpected Gitlab response when creating quick action note.')
                else:
                    raise
        return gl_mergerequest.labels

    LOGGER.info('No labels to change on merge request %s', gl_mergerequest.iid)
    return gl_mergerequest.labels


def _filter_mr_labels(merge_request, label_list, remove_scoped):
    """Remove existing scoped labels that match list and add new labels. Return the new list."""
    # If this is a scoped label, then remove the old value from the mr object list so that the
    # readyForMerge label is added or removed appropriately. Support nested scoped labels like
    # BZ::123::OK in the prefix variable.
    filtered_labels = []
    to_remove = []
    for label in label_list:
        if label['name'] not in merge_request.labels:
            if '::' in label['name']:
                if not remove_scoped:
                    prefix = '::'.join(label['name'].split('::')[0:-1])
                    to_remove += [x for x in merge_request.labels if x.startswith(f'{prefix}::') and
                                  x.count('::') == label['name'].count('::')]
                else:
                    prefix = label['name'].split('::')[0]
                    to_remove += [x for x in merge_request.labels if x.startswith(f'{prefix}::')]
            merge_request.labels.append(label['name'])
            filtered_labels.append(label)
    merge_request.labels = [x for x in merge_request.labels if x not in to_remove]
    return filtered_labels, to_remove


def match_single_label(label_name, yaml_labels):
    """Return a label object which matches the label_name, if any."""
    match = None
    for ylabel in yaml_labels:
        if ylabel['name'] == label_name:
            match = create_label_object(ylabel['name'], ylabel['color'], ylabel['description'])
            break
        if ylabel.get('regex'):
            if name_match := re.match(ylabel['name'], label_name):
                description = ylabel['description'].replace('%s', name_match.group(1))
                match = create_label_object(label_name, ylabel['color'], description)
                break
    return match


def validate_labels(label_names, yaml_path=None):
    """Return a list of label objects created from the input list of label names."""
    if not yaml_path:
        yaml_path = defs.LABELS_YAML_PATH
    yaml_data = load_yaml_data(yaml_path)
    if not yaml_data or not yaml_data.get('labels'):
        raise RuntimeError(f'No label data found in {yaml_path}')

    labels = []
    unknown_labels = []
    for label_name in label_names:
        if label := match_single_label(label_name, yaml_data['labels']):
            labels.append(label)
        else:
            unknown_labels.append(label_name)

    LOGGER.debug('Found labels: %s', labels)
    if unknown_labels or len(label_names) != len(labels):
        raise RuntimeError((f'Problem loading labels from {yaml_path}. input: {label_names},'
                            f' unknown: {unknown_labels}'))
    return labels


def _create_group_milestone(gl_group, product, rhmeta):
    """Create a new group milestone based on target release."""
    release = ""
    if rhmeta.internal_target_release:
        release = rhmeta.internal_target_release
    elif rhmeta.zstream_target_release:
        release = rhmeta.zstream_target_release

    if not release:
        LOGGER.warning("Unable to find release for milestone %s", rhmeta.milestone)
        return None

    description = f'{product}, release {release}'
    LOGGER.info("Creating new milestone for %s (%s)", rhmeta.milestone, description)

    if not misc.is_production_or_staging():
        LOGGER.info("Would have created milestone for %s (%s)", rhmeta.milestone, description)
        return None

    new_milestone = gl_group.milestones.create({'title': rhmeta.milestone,
                                                'description': description})
    return new_milestone


def add_merge_request_to_milestone(branch, gl_project, gl_mergerequest):
    """Add a merge request to a milestone, creating it, if need be."""
    LOGGER.debug('milestone branch is %s', branch)
    if not branch.milestone:
        LOGGER.info("No milestone defined for %s in rh_metadata", branch.components)
        return

    gl_group = _get_gitlab_group(gl_project)

    if gl_mergerequest.milestone and gl_mergerequest.milestone['title'] == branch.milestone:
        LOGGER.debug("This MR is already assigned to target milestone %s (id: %s)",
                     gl_mergerequest.milestone['title'], gl_mergerequest.milestone['id'])
        return

    LOGGER.info("Attempting to assign %s to milestone %s",
                gl_mergerequest.references['full'], branch.milestone)
    if gl_mergerequest.milestone:
        LOGGER.debug("MR currently assigned to milestone %s (id: %s)",
                     gl_mergerequest.milestone['title'], gl_mergerequest.milestone['id'])
    new_milestone = None
    for milestone in gl_group.milestones.list(iterator=True):
        if milestone.title == branch.milestone:
            LOGGER.debug("Found existing milestone %s (id: %s) to assign MR to",
                         milestone.title, milestone.id)
            new_milestone = milestone
            break

    if new_milestone is None:
        new_milestone = _create_group_milestone(gl_group, branch.project.product, branch)

    if new_milestone is not None and misc.is_production_or_staging():
        gl_mergerequest.milestone_id = new_milestone.id
        gl_mergerequest.save()
        LOGGER.info("Assigned MR %s to Milestone %s (id: %s)",
                    gl_mergerequest.references['full'], new_milestone.title, new_milestone.id)


def add_label_to_merge_request(
    gl_project,
    mr_id,
    label_input,
    *,
    compute_mr_status_labels: bool = True,
    level=None,
    remove_scoped=False,
):
    # pylint: disable=too-many-arguments
    """Add labels to a GitLab merge request.

    Args:
        gl_project: Project object as returned by the gitlab module.
        mr_id: The ID of the MR to add the label(s) to.
        label_input: A List containing at least one dict describing a label. See
                     create_label_object().

    Returns the current list of MR labels.
    """
    LOGGER.info('Evaluating label %s for addition to MR %s/%s',
                label_input, gl_project.path_with_namespace, mr_id)

    # Validate labels against labels.yaml
    labels = validate_labels(label_input)

    # Some of the webhooks can take several minutes to run. To help avoid collisions with the
    # other webhooks, fetch the most recent version of the merge request to get the latest labels.
    gl_mergerequest = gl_project.mergerequests.get(mr_id)

    (filtered_labels, old_scoped_labels) = _filter_mr_labels(gl_mergerequest, labels,
                                                             remove_scoped)
    label_cmds = _add_label_quick_actions(gl_project, filtered_labels, level=level)
    for label in old_scoped_labels:
        label_cmds.append(f'/unlabel "{label}"')

    if compute_mr_status_labels:
        label_cmds = _compute_mr_status_labels(gl_project, gl_mergerequest, label_cmds)
    return _run_label_commands(gl_mergerequest, label_cmds)


def remove_labels_from_merge_request(
    gl_project: 'Project',
    mr_id: int,
    labels: list[str],
    compute_mr_status_labels: bool = True
) -> list[str]:
    """Remove a label on a GitLab merge request."""
    # Some of the webhooks can take several minutes to run. To help avoid collisions with the
    # other webhooks, fetch the most recent version of the merge request to get the latest labels.
    gl_mergerequest = gl_project.mergerequests.get(mr_id)

    label_cmds = []
    for label in labels:
        if label in gl_mergerequest.labels:
            label_cmds.append(f'/unlabel "{label}"')
            gl_mergerequest.labels.remove(label)

    if compute_mr_status_labels:
        label_cmds = _compute_mr_status_labels(gl_project, gl_mergerequest, label_cmds)

    return _run_label_commands(gl_mergerequest, label_cmds)


def required_label_removed(payload, suffix, changed_labels):
    """Return True if an extra required label was removed, else False."""
    no_commit_changes = commits_have_not_changed(payload)
    for label in changed_labels:
        if not label.endswith(suffix):
            continue
        # Don't act on the base ready labels, let their hooks handle them
        if label in defs.READY_FOR_MERGE_DEPS:
            continue
        prefix = label.split("::")[0]
        # We only care about ExternalCI labels here
        if prefix != "ExternalCI":
            continue
        if suffix == defs.NEEDS_TESTING_SUFFIX:
            if f'{prefix}::{defs.READY_SUFFIX}' not in changed_labels and \
               f'{prefix}::{defs.TESTING_FAILED_SUFFIX}' not in changed_labels and \
               f'{prefix}::{defs.TESTING_WAIVED_SUFFIX}' not in changed_labels and \
               no_commit_changes:
                return True
        elif suffix == defs.READY_SUFFIX:
            if f'{prefix}::{defs.NEEDS_TESTING_SUFFIX}' not in changed_labels and \
               f'{prefix}::{defs.TESTING_FAILED_SUFFIX}' not in changed_labels and \
               f'{prefix}::{defs.TESTING_WAIVED_SUFFIX}' not in changed_labels and \
               no_commit_changes:
                return True
        elif suffix == defs.TESTING_FAILED_SUFFIX:
            if f'{prefix}::{defs.READY_SUFFIX}' not in changed_labels and \
               f'{prefix}::{defs.NEEDS_TESTING_SUFFIX}' not in changed_labels and \
               f'{prefix}::{defs.TESTING_WAIVED_SUFFIX}' not in changed_labels and \
               no_commit_changes:
                return True
        elif suffix == defs.TESTING_WAIVED_SUFFIX:
            if f'{prefix}::{defs.READY_SUFFIX}' not in changed_labels and \
               f'{prefix}::{defs.NEEDS_TESTING_SUFFIX}' not in changed_labels and \
               f'{prefix}::{defs.TESTING_FAILED_SUFFIX}' not in changed_labels and \
               no_commit_changes:
                return True
    return False


def get_changed_labels(changes):
    """Return the set of changed label names."""
    if 'labels' not in changes:
        return set()

    prev_labels = {item['title'] for item in changes['labels']['previous']}
    cur_labels = {item['title'] for item in changes['labels']['current']}

    changed_labels = set()
    changed_labels.update(prev_labels.difference(cur_labels))
    changed_labels.update(cur_labels.difference(prev_labels))
    return changed_labels


def has_label_suffix_changed(msg_payload, label_suffix):
    """Return True if a changed label matches on suffix, else False."""
    if changed_labels := get_changed_labels(msg_payload['changes']):
        return required_label_removed(msg_payload, label_suffix, changed_labels)
    return False


def has_label_prefix_changed(changes, label_prefix):
    """Return True if a changed label matches on prefix, else False."""
    if changed_labels := get_changed_labels(changes):
        return bool([label for label in changed_labels if label.startswith(label_prefix)])
    return False


def has_label_changed(changes, label_name):
    """Return True if the matching label changed, else False."""
    return bool(label_name in get_changed_labels(changes))


def force_webhook_evaluation(notetext, webhook_name):
    """Check to see if the note text requested a evaluation from the webhook."""
    return notetext.startswith('request-evaluation') or \
        any(notetext.startswith(f'request-{name}-evaluation') for name in webhook_name)


def try_bugzilla_conn():
    """If BUGZILLA_API_KEY is set then try to return a bugzilla connection object."""
    if not os.environ.get('BUGZILLA_API_KEY'):
        LOGGER.info("No bugzilla API key, not connecting to bugzilla.")
        return False
    return connect_bugzilla(os.environ.get('BUGZILLA_API_KEY'))


def connect_bugzilla(api_key, cookie_file=None, token_file=None):
    """Connect to bugzilla and return a bugzilla connection object."""
    try:
        # See https://github.com/python-bugzilla/python-bugzilla/blob/master/bugzilla/base.py#L175
        bzcon = bugzilla.Bugzilla('bugzilla.redhat.com', api_key=api_key,
                                  cookiefile=cookie_file, tokenfile=token_file)
    except ConnectionError:
        LOGGER.exception("Problem connecting to bugzilla server.")
        return False
    except PermissionError:
        LOGGER.exception("Problem with file permissions for bugzilla connection.")
        return False
    return bzcon


def find_bz_in_line(line, prefix):
    """
    Try to extract a Red Hat Bugzilla bug number/URL prefix from a text line.

    The line should begin with the specified prefix, followed by a colon ':',
    and optionally surrounded by whitespace, followed by the bug URL.
    Alternatively the line could contain the prefix (not necessarily followed
    by a colon ':'), and string "INTERNAL", signifying the link to an internal
    bug.

    Args:
        line:   The string to extract the bug number and URL prefix from.
                Only the first line is considered.
        prefix: The string that should precede the bug URL (without colon ':').

    Returns:
        One of the following tuples:
        * The string containing the bug number, and the URL prefix, if the
          line format is correct and contains the bug URL. The full bug URL
          could be obtained by concatenating the returned URL prefix and the
          bug number strings.
        * ('INTERNAL', ''), if the line contained the link to an internal bug.
        * (None, None), if neither was found.
    """
    line = line.rstrip()
    pattern = r'^\s*' + prefix + \
        r':( *|\t)(?P<url>http(s)?://bugzilla\.redhat\.com/(show_bug\.cgi\?id=)?)(?P<bug>\d{4,8})$'
    bznum_re = re.compile(pattern, re.IGNORECASE)
    bugs = bznum_re.match(line)
    if bugs:
        url_prefix = bugs.group('url')
        bug = bugs.group('bug')
        LOGGER.debug("Found bz url of %s%s", url_prefix, bug)
        return bug, url_prefix
    if prefix in line and 'INTERNAL' in line:
        return 'INTERNAL', ''
    return None, None


def find_jira_in_line(line, prefix):
    """
    Try to extract a Red Hat Jira issue ID/URL prefix from a text line.

    The line should begin with the specified prefix, followed by a colon ':',
    and optionally surrounded by whitespace, followed by the issue URL.
    Alternatively the line could contain the prefix (not necessarily followed
    by a colon ':'), and string "INTERNAL", signifying the link to an internal
    issue.

    Args:
        line:   The string to extract the issue ID and URL prefix from.
                Only the first line is considered.
        prefix: The string that should precede issue URL (without colon ':').

    Returns:
        One of the following tuples:
        * The string containing the issue ID, and the URL prefix, if the
          line format is correct and contains the issue URL. The full issue
          URL could be obtained by concatenating the returned URL prefix and
          the issue ID strings.
        * ('INTERNAL', ''), if the line contained the link to internal issue.
        * (None, None), if neither was found.
    """
    line = line.rstrip()
    pattern = r'^\s*' + prefix + \
        r':( *|\t)(?P<url>https://issues\.redhat\.com/(?:browse|projects/RHEL/issues)/)(?P<ji>' + \
        defs.JPFX + r'\d{1,8})\s*$'
    rhissue_re = re.compile(pattern, re.IGNORECASE)
    rhissues = rhissue_re.match(line)
    if rhissues:
        url_prefix = rhissues.group('url')
        rhissue = rhissues.group('ji')
        LOGGER.debug("Found jira issue url of %s%s", url_prefix, rhissue)
        return rhissue, url_prefix
    if prefix in line and 'INTERNAL' in line:
        return 'INTERNAL', ''
    return None, None


def find_cve_in_line(line, prefix):
    """Return CVE number from properly formatted CVE: line."""
    # CVEs must be called out one-per-line, begin with f'{prefix}: ' and contain a complete CVE
    # format. Format being, prefix + Year + Arbitrary Digits, where Arbitrary digit are a sequence
    # from 5 to 7 digits
    line = line.rstrip()
    cvenum_re = re.compile(prefix + r'(^\s*|: )(?P<cve>CVE-\d{4}-\d{4,7})')
    cves = cvenum_re.match(line)

    return cves.group('cve') if cves else None


def find_dep_mr_in_line(namespace, line):
    """
    Try to extract a dependency MR number from a string.

    The string must contain the string "Depends: " followed either by a GitLab
    merge request URL, or a reference to an MR using the standard GitLab
    syntax of the number prefixed with an exclamation mark '!'. The URL must
    point to an MR in the specified project namespace.

    Args:
        namespace:  The namespace the MR URL should have.
        line:       The string to extract the MR number from.

    Returns:
        The extracted MR number as an integer, or None if the line didn't
        contain an MR reference, an MR URL, or if the latter was not in the
        specified namespace.
    """
    # Depends: <mr url> lines require a full MR URL, one-per-line.
    line = line.rstrip()
    pattern = r'Depends: https://gitlab\.com/(.*)/-/merge_requests/(\d+)'
    if match := re.match(pattern, line):
        if match[1] != namespace:
            LOGGER.warning("Dependent MR is from another project")
            return None
        LOGGER.debug("Extracted MR number of %s from GitLab URL", match[2])
        return int(match[2])

    # Alternate GitLab syntax, Depends: !<MR#>, always from the same project
    pattern = r'Depends: !(?P<mr_number>\d+)'
    if match := re.match(pattern, line):
        mr_number = match.group('mr_number')
        LOGGER.debug("Extracted MR number of %s from GitLab syntax", mr_number)
        return int(mr_number)

    return None


def get_owners_parser(owners_yaml):
    """Return a parser for the owners.yaml to lookup kernel subsystem information."""
    return owners.Parser(load_yaml_data(owners_yaml))


def extract_all_from_message(message, mr_bugs, mr_cves, dependencies):
    """Extract all BZs from the message."""
    # pylint: disable=too-many-locals,too-many-branches
    bzs = []
    cves = []
    non_mr_bzs = []
    non_mr_cves = []
    dep_bzs = []

    if message:
        mlines = message.splitlines()
        for line in mlines:
            # BZs must be called out one-per-line, begin with 'Bugzilla:' and
            # contain a complete BZ URL or 'INTERNAL'. We parse it in a
            # permissive way first to show proper error messages.
            if "bugzilla" in line.lower() or "jira" in line.lower():
                if "bugzilla" in line.lower():
                    tag = "Bugzilla"
                    bug, url_prefix = find_bz_in_line(line, tag)
                else:
                    tag = "JIRA"
                    bug, url_prefix = find_jira_in_line(line, tag)
                if not bug:
                    continue
                if not line.startswith(f'{tag}: {url_prefix}{bug}'):
                    continue
                if bug in dependencies:
                    if bug in mr_bugs:
                        LOGGER.warning("Found bug %s as both %s: and Depends:", tag, bug)
                    dep_bzs.append(bug)
                elif mr_bugs and bug not in mr_bugs:
                    LOGGER.debug("%s: %s not listed in MR description.", tag, bug)
                    non_mr_bzs.append(bug)
                else:
                    bzs.append(bug)

            elif "CVE" in line:
                cve = find_cve_in_line(line, "CVE")
                if not cve:
                    continue
                if mr_cves and cve not in mr_cves:
                    LOGGER.debug("CVE: %s not listed in MR description.", cve)
                    non_mr_cves.append(cve)
                else:
                    cves.append(cve)

    # We return empty arrays if no bugs or jira issues are found
    return bzs, non_mr_bzs, dep_bzs, cves, non_mr_cves


def extract_bzs(message):
    """Extract BZs from the message."""
    bzs, _x, _y, _z, _w = extract_all_from_message(message, [], [], [])
    return bzs


def extract_cves(message):
    """Extract BZs from the message."""
    _x, _y, _z, cves, _w = extract_all_from_message(message, [], [], [])
    return cves


def extract_dependencies(project, description):
    """
    Retrieve dependency bugs and issues from an MR description.

    Retrieve a list of dependency Red Hat Bugzilla bug numbers and Jira issue
    IDs from a description of a project MR, plus from descriptions of any
    same-project dependency MRs found in it.

    Args:
        project:        The project the MR is open against.
        description:    The MR description string to extract the bug
                        dependencies from. Can be None.

    Returns:
        A list of strings containing extracted Bugzilla bug numbers and/or
        Jira issue IDs, if any.
    """
    # Dependency Red Hat Bugzilla/Jira bug numbers/issue IDs (strings).
    dep_refs = []
    # Dependency MR numbers (integers).
    mr_list = []

    # Depends must be called out one-per-line, begin with 'Depends:' and
    # contain a complete BZ URL. We parse it in a permissive way first to show
    # proper error messages.
    if description:
        dlines = description.splitlines()
        for line in dlines:
            bug, _ = find_bz_in_line(line, 'Depends')
            if bug:
                dep_refs.append(bug)
                continue
            rhissue, _ = find_jira_in_line(line, 'Depends')
            if rhissue:
                dep_refs.append(rhissue)
                continue
            if dep_mr := find_dep_mr_in_line(project.path_with_namespace, line):
                mr_list.append(dep_mr)
    if mr_list:
        for dep_mr in mr_list:
            mreq = get_mr(project, dep_mr)
            for line in mreq.description.splitlines():
                if bug := find_bz_in_line(line, 'Bugzilla')[0]:
                    LOGGER.debug("Adding dependent bug from MR URL %s", bug)
                    dep_refs.append(bug)
                if rhissue := find_jira_in_line(line, 'JIRA')[0]:
                    LOGGER.debug("Adding dependent jira issue from MR URL %s", rhissue)
                    dep_refs.append(rhissue)
    # We return an empty array if there are no bz or jira deps
    return dep_refs


def description_tag_changes(
    mr_text: str,
    is_closing: bool,
    event_changes: dict
) -> dict[str, set]:
    """Return a dict with the sets of bugs/rhissues to process for this MR based on the action."""
    bugs = {'link': set(), 'unlink': set(), 'link_rhissue': set(), 'unlink_rhissue': set()}
    current_description = Description(mr_text)
    if is_closing:
        bugs['unlink'] = current_description.bugzilla
        bugs['unlink_rhissue'] = current_description.jira_tags
        return bugs

    old_text = get_nested_key(event_changes, 'description/previous', '')
    old_description = Description(old_text)

    # Just because a BZ was mentioned in the previous description don't assume it was linked.
    bugs['link'] = current_description.bugzilla
    bugs['link_rhissue'] = current_description.jira_tags
    bugs['unlink'] = old_description.bugzilla - current_description.bugzilla
    bugs['unlink_rhissue'] = old_description.jira_tags - current_description.jira_tags
    return bugs


def get_mr(gl_project, mr_id):
    """Return a MR object."""
    try:
        return gl_project.mergerequests.get(mr_id)
    except GitlabGetError as err:
        if err.response_code == 404:
            LOGGER.warning('MR %s does not exist in project %s (404).', mr_id, gl_project.id)
            return None
        raise


def get_pipeline(gl_project, pipeline_id):
    """Return a pipeline object."""
    try:
        return gl_project.pipelines.get(pipeline_id)
    except GitlabGetError as err:
        if err.response_code == 404:
            LOGGER.warning('Pipeline %s does not exist in project %s (404).', pipeline_id,
                           gl_project.id)
            return None
        raise


def draft_status(payload):
    """Return a tuple with the current Draft status and whether it just changed."""
    is_draft = payload['object_attributes'].get('draft', False)
    changed = 'draft' in payload['changes']
    LOGGER.debug('is_draft: %s, changed: %s', is_draft, changed)
    return is_draft, changed


def extract_config_from_paths(path_list):
    """Return a list with CONFIGs changed according to the path_list."""
    config_paths = ('redhat/configs/rhel/',
                    'redhat/configs/common/',
                    'redhat/configs/debug/',
                    'redhat/configs/generic/'
                    )

    configs = [path.rsplit('/')[-1] for path in path_list if path.startswith(config_paths) and
               '/CONFIG_' in path]

    return configs


def get_authlevel(gl_project, author_id):
    """Get the gitlab access level for the given author id."""
    authal = 0
    try:
        authinfo = gl_project.members_all.get(author_id)
        authal = authinfo.access_level
    except GitlabGetError as err:
        if err.response_code == 404:
            LOGGER.info('%s: author id %s.', err.error_message, author_id)
        else:
            raise
    return authal


def get_commits_count(gl_project, gl_mergerequest):
    """Get the number of commits in the MR."""
    count = len(gl_mergerequest.commits())
    # For some value of large, large merge requests report 0 to len, unless
    # we wrap the call with list()
    if count == 0:
        count = len(list(gl_mergerequest.commits()))

    authlevel = get_authlevel(gl_project, gl_mergerequest.author['id'])
    if count > defs.MAX_COMMITS_PER_MR and authlevel < MAINTAINER_ACCESS:
        LOGGER.warning("MR %s has %d commits, too many to process -- wrong target branch?",
                       gl_mergerequest.iid, count)

    return count, authlevel


def _update_jira_state(gl_mergerequest, label_cmds):
    """Run set_jira_state if we're adding a readyForX label and not called from the jira hook."""
    if f'/label "{defs.READY_FOR_QA_LABEL}"' not in label_cmds and \
            f'/label "{defs.READY_FOR_MERGE_LABEL}"' not in label_cmds:
        return

    set_mr_issues_status(gl_mergerequest)


def set_mr_issues_status(gl_mr):
    """Move the MR's JIRA: Issues to the given new_status."""
    if gl_mr.draft:
        LOGGER.info("Not updating JIRA Issue status for MR %s, because it's marked as a Draft.",
                    gl_mr.iid)
        return

    mr_desc = Description(gl_mr.description)
    if not (issues := mr_desc.jira_tags):
        LOGGER.info('No JIRA Issues to update for %s', str(gl_mr))
        return
    # If the tagged jira isn't a kernel* comp. or is the wrong type then we shouldn't be doing this.
    if not (mr_issues := libjira.fetch_issues(issues, filter_kwf=True)):
        return
    mr_rhissues = [RHIssue.new_from_ji(ji=ji, mrs=None) for ji in mr_issues]
    for rhi in mr_rhissues:
        mr_issues.extend([lrhi.ji for lrhi in rhi.linked_rhissues])

    libjira.move_issue_states_forward(mr_issues)
    libjira.request_preliminary_testing(mr_issues)


def wrap_comment_table(table_header, table_entries, footnotes, desc,
                       limit=defs.TABLE_ENTRY_THRESHOLD):
    """Wrap a comment table with more than X entries in collapsible element markdown."""
    collapse_header = ""
    collapse_footer = ""

    if not table_entries:
        return ""

    count = len(table_entries.splitlines())
    if count > limit:
        LOGGER.info("Comment table has %s entries, wrapping (threshold: %s)", count, limit)
        collapse_header = f"<details><summary>Click to show/hide {desc}</summary>\n\n"
        collapse_footer = "</details>\n\n"

    return collapse_header + table_header + table_entries + footnotes + collapse_footer


def get_pipeline_variable(payload, key, *, default=None):
    """Return the value of the given variables key, or None."""
    variables = misc.get_nested_key(payload, 'object_attributes/variables')
    return next((var['value'] for var in (variables or []) if var['key'] == key), default)
