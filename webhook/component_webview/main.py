"""A small program to simplify userspace data lookup."""
import os

import Levenshtein
from cki_lib import misc
from cki_lib import session
from nicegui import ui
import sentry_sdk

SESSION = session.get_session('webhook.component_webview', raise_for_status=True)

# GET on webpage load to act as cached JSON for performance
COMPONENTS = SESSION.get(os.environ['COMPONENTS_JSON_URL']).json()


@ui.page("/")
def main_page():
    """Create the UI for the landing page."""
    # Search Handler Function
    async def search() -> None:
        search_field.classes("mt-2", remove="mt-2")  # move the search field up
        results.clear()

        if search_field.value == "":
            return

        def createui(component, release):
            ui.label(component["Name"]).classes("text-blue")
            ui.label(f"{release}").classes("italic font-bold")
            with ui.grid(columns="150px 1fr").classes("gap-2"):
                ui.label("Assignee:").classes()
                ui.label(component.get("Default Assignee", ""))
                ui.label("SE Contact:").classes()
                ui.label(component.get("Sustaining Engineer", ""))
                ui.label("QA:").classes()
                ui.label(component.get("QA Contact", ""))
                ui.label("CC-List:").classes()
                ui.label(component.get("Cc List", ""))
                ui.label("SST:").classes()
                ui.label(component.get("SST Pool", ""))
                ui.label("Filename:")
                ui.label(component.get("Filename", ""))
                ui.label("Description:").classes()
                ui.label(component.get("Description", ""))

        def createcards(release):
            with ui.grid().classes("auto-rows-fr grid-cols-3 mx-auto gap-2"):
                for component in product_dict[release] or []:
                    if (
                        Levenshtein.ratio(search_field.value, component["Name"])
                        >= 0.70
                    ):
                        with ui.card():
                            createui(component=component, release=release)

        with results:  # enter the context of the the results row
            for product_dict in COMPONENTS["bugzilla_data"]:
                for key in product_dict.keys():
                    if radio.value == key:
                        createcards(radio.value)
                    elif radio.value == "ALL":  # the "ALL" case
                        createcards(key)

    # UI Elements

    # Website Header
    ui.label("Userspace Component Data").classes("text-h2 self-center")
    ui.label("Pulled from BZ Component Data").classes("text-h4 self-center")
    ui.link("BZ Components",
            "https://gitlab.cee.redhat.com/bugzilla-data/components",
            new_tab=True).classes("self-center")

    # Control product filtering
    radio = (
        ui.radio(
            [
                "RHEL7",
                "RHEL8",
                "RHEL9",
                "RHEL10",
                "RHEL11",
                "RHIVOS1",
                "ALL",
            ],
            value="RHEL9",
            on_change=search,
        )
        .props("inline")
        .classes("mt-10 mb-5 self-center")
    )

    # Create a search field which is initially focused and leaves space at the top
    # Assign handler function to search function defined above
    search_field = (
        ui.input()
        .on(
            "keydown.enter",
            handler=search,
        )
        .props('autofocus rounded item-aligned input-class="ml-3"')
        .classes("w-96 self-center mt-30 transition-all")
    )
    results = ui.row().classes(replace="row-span-1")
    with ui.footer(fixed=False).classes("p-2"):
        ui.label("Maintained by debarbos(AT)redhat(DOT)com").classes("text-sm")


# Run the application
if __name__ in {"__main__", "__mp_main__"}:
    misc.sentry_init(sentry_sdk)
    ui.run(title="BZData", favicon="🐞", dark=True, show=False)
