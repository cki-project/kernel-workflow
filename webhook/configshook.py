"""Process the merge request for kernel config changes."""
from dataclasses import dataclass
from dataclasses import field
from dataclasses import fields
import enum
from functools import cached_property
import glob
import os
from pathlib import Path
import shutil
import subprocess
import sys
import typing

from cki_lib import logger

from . import cdlib
from . import common
from . import defs
from . import kgit
from .base_mr import BaseMR
from .base_mr_mixins import CommitsMixin
from .base_mr_mixins import DependsMixin
from .base_mr_mixins import GitRepoMixin
from .base_mr_mixins import OwnersMixin
from .description import Commit
from .session import SessionRunner
from .session_events import GitlabMREvent
from .session_events import GitlabNoteEvent

LOGGER = logger.get_logger('cki.webhook.configshook')

CONFIGS_LABEL_PREFIX = 'Configs'
CONFIG_ARK_MISMATCH_LABEL = defs.Label('Config::ARK::Mismatch')


def x86_filename_replacement(filename: str) -> tuple[str, str]:
    """Map and replace x86 configs to match ARK."""
    filename = filename.replace("x86/x86_64", "x86")
    arkfilename = filename.replace("common", "rhel")
    commonfilename = filename.replace("rhel", "common")

    return commonfilename, arkfilename


def read_config_content(workdir: str, filename: str) -> str:
    """Read the content of a config file if it exists."""
    config_file = Path(os.path.join(workdir, filename))

    if not config_file.exists():
        return None

    return config_file.read_text(encoding='ascii').rstrip()


def fetch_file_content(ark_worktree: str, commonfilename: str, arkfilename: str) -> tuple[str, str]:
    """Fetch the content of a config file inside ARK worktree."""
    common_output = None
    ark_output = None

    common_output = read_config_content(ark_worktree, commonfilename)

    if not common_output:
        ark_output = read_config_content(ark_worktree, arkfilename)

    return common_output, ark_output


def get_config_change(path: str) -> tuple[str, str]:
    """Extract the specific config change from a diff."""
    config_setting = ''
    changed_file = None

    if path['deletedFile'] == 'true':
        changed_file = path['oldPath']
        config_setting = None
    elif path['renamedFile'] == 'true':
        pass
    else:
        # File is new or it is a file change
        changed_file = path['newPath']

        config_setting = ''
        for line in path['diff'].split('\n'):
            if line.startswith('+') and 'CONFIG_' in line:
                config_setting += line[1:]

    if changed_file and not changed_file.startswith('redhat/configs/'):
        # Revert when it is not a CONFIG file.
        config_setting = ''
        changed_file = None

    return config_setting, changed_file


def make_dist_configs(cwd: str) -> str | None:
    """Run `make dist-configs-check` and return resulting stdout, or None if the command failed."""
    cmd = 'make dist-configs-check'
    result = subprocess.run(cmd.split(), cwd=cwd, capture_output=True, text=True, check=False)

    if result.returncode != 0:
        LOGGER.warning("'%s' returned failed: %s", cmd, result)
        return None

    return result.stdout


class ConfigMatch(enum.IntEnum):
    """Type of config we've evaluated to match to."""

    NOT_A_CONFIG = 0
    OK_CONFIG = 1
    BAD_CONFIG = 2
    ARK_CONFIG_MISMATCH = 3

    @property
    def description(self) -> str:
        """Return a formatted text description string."""
        match self:
            case ConfigMatch.NOT_A_CONFIG:
                text = "Not a Config"
            case ConfigMatch.OK_CONFIG:
                text = "OK Config"
            case ConfigMatch.BAD_CONFIG:
                text = "Bad Config"
            case ConfigMatch.ARK_CONFIG_MISMATCH:
                text = "ARK Config Mismatch"

        return text

    @property
    def footnote(self) -> str:
        """Return the footnote string associated with the match type."""
        match self:
            case ConfigMatch.NOT_A_CONFIG:
                text = ""
            case ConfigMatch.OK_CONFIG:
                text = "A CONFIG change that looks correct."
            case ConfigMatch.BAD_CONFIG:
                text = "A CONFIG change that does not look correct or could not be merged."
            case ConfigMatch.ARK_CONFIG_MISMATCH:
                text = "A CONFIG change was detected and does not match ARK."
        return text


@dataclass
class RHConfigCommit(Commit):
    """Per-MR-commit data class for storing config evaluation data."""

    match: int = ConfigMatch.NOT_A_CONFIG
    footnotes: list = field(default_factory=list)
    differences: str = ''

    @classmethod
    def from_commit(cls, commit) -> typing.Self:
        """Build RHConfigCommit object from a Commit object."""
        return cls(**{f.name: getattr(commit, f.name) for f in fields(Commit)})

    def has_ark_config_diffs(self, output: str, common_output: str, ark_output: str) -> bool:
        """Check if the current repository has the same configs of ARK repo."""
        LOGGER.debug("Content in common '%s' and in ark '%s'. The expected "
                     "output is '%s'.", common_output, ark_output, output)

        if not (common_output or ark_output):
            # If there is no CONFIG in ARK, downstream should disable (None) too.
            if not output:
                LOGGER.debug("Matched config change to common directory in ARK")
                return False

            msg = (f'A CONFIG change in RHEL commit {self.sha} (`{output}`) does not match '
                   'ARK, no matching common or rhel config file found.')
            LOGGER.debug(msg)
            self.differences = msg
            return True

        if output:
            # Considering the CONFIG exists and was changed, we need to compare
            # with common or ark dirs.
            if output == common_output:
                LOGGER.debug("Matched config change to common directory in ARK")
                return False

            if output == ark_output:
                LOGGER.debug("Matched config change to ark directory in ARK")
                return False

        # Any other combination is considered a mismatch.

        msg = (f'A CONFIG change in RHEL commit {self.sha} (`{output}`) does not match ARK '
               f'configs for future RHEL releases (redhat/configs/common: `{common_output}`, '
               f'redhat/configs/rhel: `{ark_output}`).')
        LOGGER.debug(msg)
        self.differences = msg
        return True


# The DependsMixin expects any additional fields to have kw_only set!
@dataclass(repr=False, kw_only=True)
class ConfigsMR(GitRepoMixin, OwnersMixin, DependsMixin, CommitsMixin, BaseMR):
    # pylint: disable=too-many-public-methods,too-many-instance-attributes,too-many-ancestors
    """Represent the MR, its dependencies, commits, etc."""

    ark_branch: str = ''
    config_worktree: str = ''
    config_evaluation: str = ''
    # Comment table uses footnotes and footnote_ids to build footnotes.
    footnotes: list[str] = field(default_factory=list, init=False)
    footnote_ids: dict = field(default_factory=dict, init=False)
    # Both config_diffs populated by assemble_configs()
    mr_config_diffs: str = ''
    merged_config_diffs: str = ''
    mergeable: bool = True
    can_make_configs = True  # Whether or not `make dist-configs` completed successfully.

    @cached_property
    def rhconfigs(self) -> list[RHConfigCommit]:
        """The list of commit objects that are making kernel config changes."""
        rhconfigs = []
        for commit in self.commits_with_diffs.values():
            if not cdlib.is_rhconfig_commit(commit.diff):
                continue
            rhconfig = RHConfigCommit.from_commit(commit)
            rhconfigs.append(rhconfig)

        return rhconfigs

    @property
    def overall_configs_scope(self) -> defs.MrScope:
        """Return the overall MrScope of this ConfigsMR."""
        if self.rhconfigs:
            if not self.can_make_configs or not self.mergeable or self.has_bad_config:
                return defs.MrScope.NEEDS_REVIEW

        return defs.MrScope.OK

    @property
    def configs_approved(self) -> bool:
        """Return True if none of the commits are BAD_CONFIG or ARK_CONFIG_MISMATCH."""
        return not (self.has_ark_config_mismatch or self.has_bad_config)

    @property
    def has_ark_config_mismatch(self) -> bool:
        """Return True if any commits indicates an ARK Config Mismatch."""
        return any(rhconfig.match is ConfigMatch.ARK_CONFIG_MISMATCH for rhconfig in self.rhconfigs)

    @property
    def has_bad_config(self) -> bool:
        """Return True if any commits indicates a Bad Config."""
        return any(rhconfig.match is ConfigMatch.BAD_CONFIG for rhconfig in self.rhconfigs)

    @property
    def configs_label(self) -> defs.Label:
        """Return the CommitRefs Label derived from the self.overall_configs_scope."""
        return self.overall_configs_scope.label(CONFIGS_LABEL_PREFIX)

    @cached_property
    def expected_labels(self) -> list[defs.Label]:
        """Return the list of Labels the MR should have."""
        labels = [self.configs_label]

        # Add the 'Configuration' label, if we actually have some configs we found
        if self.config_items:
            labels.append(defs.Label(defs.CONFIG_LABEL))

        # Add the CONFIG_ARK_MISMATCH_LABEL if any configs don't match ARK
        if self.has_ark_config_mismatch:
            labels.append(CONFIG_ARK_MISMATCH_LABEL)

        return labels

    def update_config_match_data(self, rhconfig, matchtype) -> None:
        """Update given commit and footnotes mappings."""
        rhconfig.match = matchtype
        if rhconfig.match is ConfigMatch.NOT_A_CONFIG:
            return
        if matchtype not in self.footnote_ids:
            self.footnotes += [matchtype.footnote]
            self.footnote_ids[matchtype] = str(len(self.footnotes))
            rhconfig.footnotes.append(self.footnote_ids[matchtype])

    def handle_stale_worktree(self) -> None:
        """Ensure we're not stomping on another run, and clean it up if it's old."""
        LOGGER.warning("Worktree already exists at %s!", self.config_worktree)
        self.clean_up_temp_ark_branch()
        try:
            kgit.branch_delete(self.source_path, f"{self.ark_branch}-save")
        except subprocess.CalledProcessError:
            pass

    def prep_temp_ark_branch(self) -> None:
        """Set up the temporary branch/worktree to test upstream CONFIGs in it."""
        # kernel-ark remote is hard coded because this repo is the baseline.
        if self.ark_branch:
            # We already prepped the branch for another config check in this MR
            return

        target_branch = "kernel-ark/os-build"
        self.ark_branch = "kernel-ark-config-check"
        self.config_worktree = (f"/{'/'.join(self.source_path.strip('/').split('/')[:-1])}"
                                f"/{self.ark_branch}/")

        # Ensure we don't have a stale/failed checkout lingering
        if os.path.isdir(self.config_worktree):
            self.handle_stale_worktree()

        kgit.fetch_remote(self.source_path, "kernel-ark")
        # This is the lookaside cache we maintain for examining diffs between revisions of a
        # merge request, which we're going to create temporary worktrees off of
        LOGGER.info("Creating git worktree at %s with branch %s for config comparison, please hold",
                    self.config_worktree, self.ark_branch)
        kgit.worktree_add(self.source_path, self.ark_branch, self.config_worktree, target_branch)

    def move_config_files(self) -> None:
        """Move merged config files so they can be evaluated."""
        kconfig_files = glob.glob(f'{self.worktree_dir}/redhat/configs/kernel-*.config')
        for file in kconfig_files:
            file_name = os.path.basename(file)
            shutil.move(file, f'{self.worktree_dir}/{self.config_evaluation}/{file_name}')

    def assemble_configs(self) -> None:
        """Assemble the merged configs."""
        self.config_evaluation = 'config_evaluation'
        os.mkdir(f'{self.worktree_dir}/{self.config_evaluation}')

        if (make_dist_configs_stdout := make_dist_configs(self.worktree_dir)) is None:
            LOGGER.info('Could not build configs before merging.')
            self.can_make_configs = False
            return

        LOGGER.debug("Initial make dist-configs output: %s", make_dist_configs_stdout)
        self.move_config_files()

        kgit.add(self.worktree_dir, self.config_evaluation)
        kgit.commit(self.worktree_dir, "Add baseline config files")

        try:
            kgit.merge(self.worktree_dir, f'{self.project_remote}/merge-requests/{self.iid}')
        except subprocess.CalledProcessError:
            self.mergeable = False
            return

        if (make_dist_configs_stdout := make_dist_configs(self.worktree_dir)) is None:
            LOGGER.info('Could not build configs after merging %s.', self.iid)
            self.can_make_configs = False
            return

        LOGGER.debug("Post-MR-merge make dist-configs output: %s", make_dist_configs_stdout)
        self.move_config_files()

        config_base = f'{self.project_remote}/{self.target_branch}'
        rh_cfgs_diff_ret = kgit.raw_diff(self.worktree_dir, config_base, 'redhat/configs/')
        if rh_cfgs_diff_ret.stdout:
            self.mr_config_diffs = f"```diff\n{rh_cfgs_diff_ret.stdout}\n```\n"
        kconfig_diff_ret = kgit.raw_diff(self.worktree_dir, None, self.config_evaluation)
        if kconfig_diff_ret.stdout:
            kconfig_diff = kconfig_diff_ret.stdout.replace(self.config_evaluation, 'redhat/configs')
            self.merged_config_diffs = f"```diff\n{kconfig_diff}\n```\n"
        else:
            self.merged_config_diffs = "This MR's config changes had ZERO impact on merged configs"

    def evaluate_merged_configs(self, rhconfig) -> bool:
        """Evaluate the merged Kconfig files vs. what the MR added/subtracted."""
        if not self.mergeable:
            return True
        bad_config = False
        for path in rhconfig.diff:
            config_setting, _ = get_config_change(path)
            if not config_setting:
                continue
            config_addition = config_setting.startswith("+CONFIG_")
            if config_addition and config_setting not in self.merged_config_diffs:
                self.update_config_match_data(rhconfig, ConfigMatch.BAD_CONFIG)
                bad_config = True
        return bad_config

    def evaluate_config(self, rhconfig) -> bool:
        """Compare and evaluate all the config files in diff with upstream."""
        if self.gl_project.id == defs.ARK_PROJECT_ID:
            self.update_config_match_data(rhconfig, ConfigMatch.OK_CONFIG)
            return False

        mapped_config_changes = {}
        mismatch = False

        for path in rhconfig.diff:
            config_setting, changed_file = get_config_change(path)

            if changed_file:
                mapped_config_changes[changed_file] = config_setting

        if not mapped_config_changes:
            return False

        # Kernel ARK is hard coded because it is our baseline
        self.prep_temp_ark_branch()

        for filename, content in mapped_config_changes.items():
            commonfilename, arkfilename = x86_filename_replacement(filename)

            common_output, ark_output = fetch_file_content(self.config_worktree,
                                                           commonfilename, arkfilename)

            if rhconfig.has_ark_config_diffs(content, common_output, ark_output):
                self.update_config_match_data(rhconfig, ConfigMatch.ARK_CONFIG_MISMATCH)
                mismatch = True

        return mismatch

    def clean_up_temp_ark_branch(self) -> None:
        """Clean up the git worktree and branches from the upstream worktree."""
        if not self.ark_branch:
            return
        kgit.worktree_remove(self.source_path, self.config_worktree)
        kgit.branch_delete(self.source_path, self.ark_branch)
        LOGGER.debug("Removed worktree %s and deleted branch %s",
                     self.config_worktree, self.ark_branch)

    def run_config_checks(self) -> None:
        """Run checks for config changes in MR commits."""
        LOGGER.info("Running kernel config checks on MR %s/!%s", self.namespace, self.iid)
        LOGGER.debug("Merge request description:\n%s", self.description.text)
        if not any(file.startswith('redhat/configs/') for file in self.files):
            LOGGER.info("No kernel config changes in MR %s/!%s", self.namespace, self.iid)
            return

        # First up, we assemble the merged configs and diffs vs. target branch
        if not self.worktree_dir:
            self.fetch_remote(self.project_remote)
            self.prep_worktree(self.project_remote)
        self.assemble_configs()
        self.cleanup_worktree()

        # Now examine the individual commits for Kconfig additions and evaluate them
        for rhconfig in self.rhconfigs:
            # See if the config matches kernel-ark
            mismatch = self.evaluate_config(rhconfig)
            # See if the config is an addition not showing up in merged output
            bad_config = self.evaluate_merged_configs(rhconfig)

            if not mismatch and not bad_config:
                self.update_config_match_data(rhconfig, ConfigMatch.OK_CONFIG)

        self.clean_up_temp_ark_branch()

    def config_eval_report(self) -> str:
        """Print a kernel config evaluation report for gitlab."""
        # pylint: disable=too-many-locals,too-many-branches
        report = ("This report indicates how any detected Kconfig changes compare with expected "
                  "changes in the merged .config and with the ARK configs.\n")
        table_header = ("|Sub CID  |Evaluation |Footnotes|\n"
                        "|:--------|:----------|:--------|\n")
        table_entries = ""
        for rhconfig in reversed(self.rhconfigs):
            if rhconfig.match is ConfigMatch.NOT_A_CONFIG:
                continue
            table_entries += f"|{rhconfig.sha}|{rhconfig.match.description}|"
            table_entries += common.build_note_string(rhconfig.footnotes)

        footnotes = common.print_notes(self.footnotes)
        report += common.wrap_comment_table(table_header, table_entries, footnotes, "table")
        if self.mergeable and self.rhconfigs and self.configs_approved:
            report += "\nMerge Request **passes** basic config evaluation.  \n"
        else:
            if not self.rhconfigs:
                report += "\nThis Merge Request has no detected kernel config changes in it.  \n"
            if not self.can_make_configs:
                report += ("\n`make dist-configs` did not complete successfully on the target"
                           " branch or after merging this MR onto it.  Please check this"
                           " locally and report any problems with the target branch to the"
                           " kernel maintainers.  \n")
            if not self.mergeable:
                report += ("\nWARNING: This Merge Request cannot be merged to its target branch, "
                           "so not all checks were able to be performed. You most likely need to "
                           "rebase your merge request.  \n")
            if self.has_ark_config_mismatch:
                report += "\nThis Merge Request contains configs that do not match ARK. \n"
                for rhconfig in reversed(self.rhconfigs):
                    if rhconfig.match is ConfigMatch.ARK_CONFIG_MISMATCH and rhconfig.differences:
                        report += f'- {rhconfig.differences}  \n'
            if self.has_bad_config:
                report += ("\nThis Merge Request contains configs that do not result in the "
                           "expected merged config, or the MR was not mergeable. \n")
            report += "\nTo request re-evalution after resolving any issues with the configs in " \
                      "the merge request, add a comment to this MR with only the text:  " \
                      "request-configs-evaluation \n"

        if self.mr_config_diffs:
            report += ("\nPlease double-check the resulting Merged Kconfig Results below to ensure "
                       "that they actually match expectations.  \n")
            report += common.wrap_comment_table("", self.mr_config_diffs, "",
                                                "Config Diffs From MR")
            report += common.wrap_comment_table("", self.merged_config_diffs, "",
                                                "Merged Kconfig Results")

        return report

    def update_mr(self) -> None:
        """Update the MR with the results of our examination."""
        current_labels = self.labels
        LOGGER.info('Current labels: %s', current_labels)

        # Add any expected labels to the MR which are not already set.
        LOGGER.info('Expected configshook labels: %s', self.expected_labels)
        to_add = [lbl for lbl in self.expected_labels if lbl not in current_labels]
        if to_add:
            LOGGER.info('Adding labels: %s', to_add)
            self.add_labels(to_add, remove_scoped=True)
        else:
            LOGGER.info('No labels to add.')

        # Remove Configuration and/or Config::ARK::Mismatch label if they're no longer expected.
        to_remove: list[defs.Label] = [
            label for label in (defs.CONFIG_LABEL, CONFIG_ARK_MISMATCH_LABEL) if
            label not in self.expected_labels and label in current_labels
        ]
        if to_remove:
            LOGGER.info('Removing unneeded labels: %s', to_remove)
            self.remove_labels(to_remove)
        else:
            LOGGER.info('No unneeded labels to remove.')

        # Generate the full report and update the MR comment with it.
        LOGGER.info('Reporting kernel config readiness.')
        report_header = (f'**{self.session.webhook.comment_header}**: '
                         f'~"{self.configs_label}"\n\n')
        report = report_header + self.config_eval_report()

        self.session.update_webhook_comment(self.gl_mr, report,
                                            bot_name=self.session.gl_user.username,
                                            identifier=self.session.webhook.comment_header)


def process_merge_request(
    session: SessionRunner,
    mr_url: defs.GitlabURL
) -> None:
    """Process the given merge request and update it."""
    configs_mr = ConfigsMR.new(
        session,
        mr_url,
        source_path=session.args.rhkernel_src,
        linux_src=session.args.rhkernel_src
    )

    configs_mr.run_config_checks()
    configs_mr.update_mr()


def process_gl_event(
    _: dict,
    session: SessionRunner,
    event: GitlabMREvent | GitlabNoteEvent,
    **__: typing.Any
) -> None:
    """Process a gitlab event."""
    LOGGER.info('Processing %s event for %s', event.kind.name, event.mr_url)

    process_merge_request(session, event.mr_url)


HANDLERS = {
    GitlabMREvent: process_gl_event,
    GitlabNoteEvent: process_gl_event,
}


def main(args):
    """Run main loop."""
    common.init_sentry()
    parser = common.get_arg_parser('CONFIGSHOOK')
    parser.add_argument('--rhkernel-src',  **common.get_argparse_environ_opts('RHKERNEL_SRC'),
                        help='Directory containing Red Hat kernel git tree')
    args = parser.parse_args(args)
    session = SessionRunner.new('configshook', args=args, handlers=HANDLERS)
    session.run()


if __name__ == "__main__":
    main(sys.argv[1:])
