"""Module to provide upstream kernel commit to CVE number lookups."""
from collections import defaultdict
from dataclasses import dataclass
from dataclasses import field
from pathlib import Path
import typing

from cki_lib.logger import get_logger
from git import Repo

LOGGER = get_logger('cki.webhook.cvedb')

# A dict w/commit hashes as keys, CVE numbers as values
CommitMapping = dict[str, str]


@dataclass
class VulnerabilityManager:
    """Manage commit to CVE lookups in linux security/vulns database."""

    vulns_source: Path
    _cve_commits: CommitMapping | None = field(default=None)
    _vulns_head_sha: str | None = field(default=None)

    def _check_cache(self) -> None:
        """Check if the vulns_source has changed, and if so, rebuild the _cve_commits."""
        repo = Repo(self.vulns_source)

        current_head = repo.head.commit.hexsha

        if current_head == self._vulns_head_sha:
            return

        first_time = self._vulns_head_sha is None

        LOGGER.info('HEAD is %s%s, %sloading data from %s.', current_head[:12],
                    f' (was {self._vulns_head_sha[:12]})' if not first_time else '',
                    '' if first_time else 're', self.vulns_source)

        # Save the current HEAD and rebuild the mapping.
        self._vulns_head_sha = current_head
        self._cve_commits = _build_cve_commit_map(self.vulns_source)

    def __call__(self, commit_hash: str) -> str | None:
        """Return the CVE number associatee with the given commit_hash."""
        self._check_cache()
        return _lookup_commit(self._cve_commits, commit_hash)

    @classmethod
    def new(cls, vulns_source: str) -> typing.Self:
        """Construct a new VulnerabilityManager instance."""
        if not vulns_source:
            raise RuntimeError('No vulns_source path given.')

        vulns_source_path = Path(vulns_source)
        LOGGER.info('Using %s for vulns_source.', vulns_source_path)

        if not vulns_source_path.is_dir():
            raise RuntimeError(f'Path {vulns_source_path} does not exist or is not a directory!')

        return cls(vulns_source_path)


def cve_json_url(cve: str) -> str:
    """Build and return a CVE json url for a given CVE."""
    vulns_base = "https://git.kernel.org/pub/scm/linux/security/vulns.git/tree/cve/published"
    year = cve.split("-")[1]
    json_url = f"{vulns_base}/{year}/{cve}.json"
    return json_url


def _build_cve_commit_map(vulns_source: Path) -> CommitMapping:
    """Build a mapping of commit hashes to CVE numbers from the given vulns_source."""
    if not (cve_files := list(vulns_source.rglob('cve/published/*/CVE-*.sha1'))):
        raise RuntimeError(f'No valid CVE data found in {vulns_source}.')

    data: CommitMapping = defaultdict(set)

    for cve_file in cve_files:
        cve = cve_file.name.removesuffix('.sha1')
        commit_hash = str(cve_file.read_text().strip())
        data[commit_hash] = cve

    LOGGER.info('Kernel commit to CVE mapping contains %d items.', len(data))
    return data


def _lookup_commit(commit_map: CommitMapping, commit_hash: str) -> str | None:
    """Return the list of names of the paths associated with the given commit_hash."""
    if cve := commit_map.get(commit_hash):
        LOGGER.debug('%s matches CVE: %s', commit_hash, cve)
        return cve

    LOGGER.debug('%s does not match any known CVE numbers', commit_hash)
    return None
