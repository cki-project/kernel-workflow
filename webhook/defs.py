"""Common variable definitions that can be used by all webhooks (and common code)."""
from enum import Enum
from enum import IntEnum
from enum import StrEnum
from enum import auto
from enum import unique
from pathlib import Path
from re import compile as re_compile
from re import sub as re_sub

# These are now part of the external kwf_lib package but keep them imported here for
# compatibility.
from kwf_lib.common import FixVersion  # noqa: F401  pylint: disable=unused-import
from kwf_lib.common import GitlabURL  # noqa: F401  pylint: disable=unused-import
from kwf_lib.common import JiraKey  # noqa: F401  pylint: disable=unused-import

ARK_BOT_ACCOUNT = 'cki-ark-bot'
CKI_BOT_ACCOUNT = 'cki-bot'
KWF_BOT_ACCOUNT = 'cki-kwf-bot'
CKI_BACKPORT_BOT_ACCOUNT = 'cki-backport-bot'
BOT_ACCOUNTS = (CKI_BOT_ACCOUNT, KWF_BOT_ACCOUNT, ARK_BOT_ACCOUNT, CKI_BACKPORT_BOT_ACCOUNT)
JIRA_BOT_ACCOUNTS = ('gitlab-jira', 'gitlab-bot')
ARK_PROJECT_ID = 13604247
BOT_APPROVAL_RULE_PREFIX = 'Bot'
BOT_APPROVAL_RULE = f'{BOT_APPROVAL_RULE_PREFIX}: readyForMerge'
ALL_MEMBERS_APPROVAL_RULE = 'All Members'

MAX_COMMITS_PER_MR = 2000
MAX_COMMITS_PER_COMMENT_ROW = 20
TABLE_ENTRY_THRESHOLD = 5

BUGZILLA_HOST = 'bugzilla.redhat.com'

UMB_BRIDGE_MESSAGE_TYPE = 'cki.kwf.umb-bz-event'
JIRA_WEBHOOK_MESSAGE_TYPE = 'jira'
JPFX = 'RHEL-'
# KWF valid issue components.
KWF_SUPPORTED_ISSUE_COMPONENTS = ('kernel', 'kernel-rt', 'kernel-automotive')

LABELS_YAML_PATH = 'utils/labels.yaml'

GITFORGE = 'https://gitlab.com'
BOT_FORK_NAMESPACE_BASE = 'redhat/red-hat-ci-tools/kernel/bot-branches'

BLOCKED_LABEL = 'Blocked'
CONFIG_LABEL = 'Configuration'
NO_COMMITS_LABEL = 'NoCommits'
OVERSIZE_LABEL = 'Oversize'
ZSTREAM_BUILD_LABEL = 'ZStreamBuild'
INACTIVE_BRANCH_LABEL = 'InactiveBranch'
NEEDS_REVIEW_SUFFIX = 'NeedsReview'
NEEDS_TESTING_SUFFIX = 'NeedsTesting'
NEW_STATE = 'New'
PLANNING_STATE = 'Planning'
IN_PROGRESS_STATE = 'InProgress'
MISSING_SUFFIX = 'Missing'
TESTING_FAILED_SUFFIX = 'Failed'
TESTING_WAIVED_SUFFIX = 'Waived'
READY_SUFFIX = 'OK'
BLOCKED_BY_PREFIX = 'Blocked-by:'
BLOCKED_SUFFIX = 'Blocked'
TESTING_SUFFIXES = (NEEDS_TESTING_SUFFIX, TESTING_FAILED_SUFFIX)

READY_FOR_MERGE_HELPER_FUNC = 'ready_for_merge_helper'

SUBMITTER_DEPENDENCIES = [
    f'CommitRefs::{READY_SUFFIX}',
    f'Merge::{READY_SUFFIX}',
    f'Signoff::{READY_SUFFIX}',
    f'Dependencies::{READY_SUFFIX}',
    f'ExternalCI::{READY_SUFFIX}',
]

BASE_DEPENDENCIES = [f'Acks::{READY_SUFFIX}',
                     f'CKI::{READY_SUFFIX}',
                     f'CommitRefs::{READY_SUFFIX}',
                     f'Merge::{READY_SUFFIX}',
                     f'Signoff::{READY_SUFFIX}']

READY_FOR_MERGE_DEPS = BASE_DEPENDENCIES + [f'Dependencies::{READY_SUFFIX}',
                                            f'JIRA::{READY_SUFFIX}',
                                            f'ExternalCI::{READY_SUFFIX}']

READY_FOR_QA_DEPS = BASE_DEPENDENCIES + [f'JIRA::{IN_PROGRESS_STATE}']

ARK_READY_FOR_MERGE_DEPS = [f'Acks::{READY_SUFFIX}',
                            f'CKI::{READY_SUFFIX}',
                            f'Merge::{READY_SUFFIX}',
                            f'Signoff::{READY_SUFFIX}']

READY_FOR_MERGE_LABEL = 'readyForMerge'
READY_FOR_QA_LABEL = 'readyForQA'
READY_LABELS = (READY_FOR_MERGE_LABEL, READY_FOR_QA_LABEL)
TARGETED_TESTING_LABEL = 'TargetedTestingMissing'
TIME_CRITICAL_LABEL = 'Time Critical'

BUG_FIELDS = ['cf_internal_target_release',
              'cf_verified',
              'cf_zstream_target_release',
              'component',
              'external_bugs',
              'flags',
              'id',
              'product',
              'status',
              'summary'
              ]

EXT_TYPE_URL = 'https://gitlab.com/'
CODE_CHANGED_PREFIX = "CodeChanged::"
SEVERITY_PREFIX = "Severity::"

DCO_URL = "https://developercertificate.org"
DCO_PASS = "The DCO Signoff Check for all commits and the MR description has **PASSED**.\n"
DCO_FAIL = ("**ERROR: DCO 'Signed-off-by:' tags were not found on all commits and the MR "
            "description. Please review the results in the table below.**  \n"
            "This project requires developers add a Merge Request description and per-commit "
            f"acknowlegement of the [Developer Certificate of Origin]({DCO_URL}), also known "
            "as the DCO. This can be accomplished by adding an explicit 'Signed-off-by:' tag "
            "to your MR description and each commit.\n\n"
            "**This Merge Request's commits will not be considered for inclusion into this "
            "project until these problems are resolved. After making the required changes please "
            "resubmit your merge request for review.**\n\n")
SUBSYS_LABEL_PREFIX = 'Subsystem'

INTERNAL_FILES = (
    'redhat/',
    '.gitignore',  # This exists upstream but we heavily modify it.
    '.gitlab',
    '.get_maintainer.conf',
    'Kconfig.redhat',
    'makefile',
    'Makefile.rhelver'
)

DEFAULT_RH_METADATA_PATH = 'utils/rh_metadata.yaml'
DEFAULT_RH_WEBHOOKS_PATH = 'utils/rh_webhooks.yaml'
DEFAULT_MIRROR_LIST_PATH = 'utils/rh_kernel_git_repos.yml'

DEFAULT_RH_METADATA_RPM_PATH = Path('/usr/share/kwf/config/rh_projects_private.yaml')

JIRA_BOT_ASSIGNEES = [
    'arch-hw-aarch64-triage@redhat.com',
    'arch-hw-firmware-triage@redhat.com',
    'arch-hw-powerpc-triage@redhat.com',
    'arch-hw-triage@redhat.com',
    'arch-hw-x86-triage@redhat.com',
    'cifs-team@redhat.com',
    'core-kernel-mgr@redhat.com',
    'fs-maint@redhat.com',
    'gfs2-maint@redhat.com',
    'hwkernel-mgr@redhat.com',
    'kdump-bugs@redhat.com',
    'kernel-mgr@redhat.com',
    'kpatch-maint@redhat.com',
    'lvm-team@redhat.com',
    'mm-maint@redhat.com',
    'nfs-team@redhat.com',
    'nst-kernel-bugs@redhat.com',
    'rhel-networking-bot@redhat.com',
    'rt-maint@redhat.com',
    'se-kernel@redhat.com',
    'virt-maint@redhat.com',
    'xgl-maint@redhat.com',
    'no-reply@redhat.com',
]

YAML_DO_SCHEMA_VALIDATE = True
YAML_DO_CACHE_LOADS = False


class BZPriority(IntEnum):
    """Possible priority of a bugzilla BZ."""

    # https://bugzilla.redhat.com/page.cgi?id=fields.html#priority
    UNKNOWN = auto()
    UNSPECIFIED = auto()
    LOW = auto()
    MEDIUM = auto()
    HIGH = auto()
    URGENT = auto()

    @classmethod
    def from_str(cls, priority_str):
        """Return the BZPriority matching the string, or BZPriority.UNKNOWN."""
        return next((member for name, member in cls.__members__.items()
                     if name == priority_str.upper()), cls.UNKNOWN)


class JIPriority(IntEnum):
    """Possible priority of a JIRA Issue."""

    UNKNOWN = 0
    UNDEFINED = 0
    MINOR = auto()
    NORMAL = auto()
    MAJOR = auto()
    CRITICAL = auto()
    BLOCKER = auto()

    @classmethod
    def from_str(cls, priority_str):
        """Return the JIPriority matching the string, or JIPriority.UNKNOWN."""
        return next((member for name, member in cls.__members__.items()
                     if name == priority_str.upper()), cls.UNKNOWN)


class IssueSeverity(IntEnum):
    """Possible Severity of a JIRA Weakness/Vulnerability Issue."""

    UNKNOWN = 0
    # https://access.redhat.com/security/updates/classification
    LOW = 1
    MODERATE = 2
    IMPORTANT = 3
    CRITICAL = 4
    # For Jira.
    MINOR = LOW
    NORMAL = MODERATE
    MAJOR = IMPORTANT
    # For bugzilla: https://bugzilla.redhat.com/page.cgi?id=fields.html#severity
    MEDIUM = MODERATE
    HIGH = IMPORTANT
    URGENT = CRITICAL

    @classmethod
    def from_str(cls, severity_str):
        """Return the IssueSeverity matching the string, or IssueSeverity.UNKNOWN."""
        return next((member for name, member in cls.__members__.items()
                     if name == severity_str.upper()), cls.UNKNOWN)


class JIType(IntEnum):
    """Possible type of a JIRA Issue."""

    UNKNOWN = 0
    BUG = auto()
    STORY = auto()
    TASK = auto()
    WEAKNESS = auto()
    VULNERABILITY = auto()

    @classmethod
    def supported(cls):
        """Return the list of supported issue types as Capitalized strings."""
        return [n.capitalize() for n, m in cls.__members__.items() if m != cls.UNKNOWN]

    @classmethod
    def from_str(cls, type_str):
        """Return the JIType matching the string, or JIType.UNKNOWN."""
        return next((member for name, member in cls.__members__.items()
                     if name == type_str.upper()), cls.UNKNOWN)


class BZStatus(IntEnum):
    """Possible status of a bugzilla BZ."""

    # https://bugzilla.redhat.com/page.cgi?id=fields.html#bug_status
    UNKNOWN = auto()
    NEW = auto()
    ASSIGNED = auto()
    POST = auto()
    MODIFIED = auto()
    ON_DEV = auto()
    ON_QA = auto()
    VERIFIED = auto()
    RELEASE_PENDING = auto()
    CLOSED = auto()

    @classmethod
    def from_str(cls, status):
        """Return the BZStatus matching the status string, or BZStatus.UNKNOWN."""
        return next((member for name, member in cls.__members__.items()
                     if name == status.upper()), cls.UNKNOWN)


class JIStatus(IntEnum):
    """Possible status of a JIRA Issue."""

    # Red Hat's JIRA project has New (id=11), Planning (id=81), In Progress(id=111),
    # Integration (id=41), Release Pending (id=101) and Closed (id=61).
    # The rest are pseudo-status that use other fields.
    UNKNOWN = auto()
    NEW = auto()
    PLANNING = auto()
    IN_PROGRESS = auto()
    # READY_FOR_QA: customfield_12321540 "Preliminary Testing: Requested" (set by our bot)
    READY_FOR_QA = auto()
    # TESTED: customfield_12321540 "Preliminary Testing: Pass" (set by QA, not us)
    TESTED = auto()
    INTEGRATION = auto()
    RELEASE_PENDING = auto()
    CLOSED = auto()

    @classmethod
    def from_str(cls, status):
        """Return the JIStatus matching the status string, or JIStatus.UNKNOWN."""
        return next((member for name, member in cls.__members__.items()
                     if name == str(status).replace(' ', '_').upper()), cls.UNKNOWN)

    @property
    def status_str(self):
        """Return the original properly capitalized status string Jira uses."""
        return " ".join([bit.capitalize() for bit in self.name.split('_')])


class JIPTStatus(IntEnum):
    """Possible status of a JIRA Issue's Preliminary Testing field."""

    UNKNOWN = auto()
    UNSET = auto()
    REQUESTED = auto()
    PASS = auto()
    FAIL = auto()

    @classmethod
    def from_str(cls, status):
        """Return the JIPTStatus matching the status string, or JIPTStatus.UNKNOWN."""
        return next((member for name, member in cls.__members__.items()
                     if name == str(status).replace(' ', '_').upper()), cls.UNKNOWN)


class BZResolution(IntEnum):
    """Possible resolution of a BZStatus.CLOSED BZ."""

    # https://bugzilla.redhat.com/page.cgi?id=fields.html#resolution
    UNKNOWN = auto()
    CURRENTRELEASE = auto()
    DUPLICATE = auto()
    ERRATA = auto()
    NOTABUG = auto()
    WONTFIX = auto()
    CANTFIX = auto()
    DEFERRED = auto()
    INSUFFICIENT_DATA = auto()
    NEXTRELEASE = auto()
    RAWHIDE = auto()
    UPSTREAM = auto()
    WORKSFORME = auto()
    EOL = auto()
    MIGRATED = auto()
    COMPLETED = auto()

    @classmethod
    def from_str(cls, resolution):
        """Return the BZResolution matching the status string, or BZResolution.UNKNOWN."""
        return next((member for name, member in cls.__members__.items()
                     if name == resolution.upper()), cls.UNKNOWN)


class JIResolution(IntEnum):
    """Possible resolution of a JIStatus.CLOSED JIRA Issue."""

    UNKNOWN = auto()
    DONE = auto()
    WONTDO = auto()
    CANNOTREPRODUCE = auto()
    CANTDO = auto()
    DUPLICATE = auto()
    NOTABUG = auto()
    DONEERRATA = auto()
    MIRRORORPHAN = auto()
    OBSOLETE = auto()
    TESTPENDING = auto()

    @classmethod
    def from_str(cls, resolution):
        """Return the JIResolution matching the status string, or JIResolution.UNKNOWN."""
        return next((member for name, member in cls.__members__.items()
                    if name == re_sub('[^a-zA-Z]+', '', str(resolution).upper())), cls.UNKNOWN)


class MrScope(IntEnum):
    """Possible scopes of an MR."""

    INVALID = 0
    CLOSED = auto()
    FAILED = auto()
    MISMATCH = auto()
    TESTING_FAILED = auto()
    BLOCKED = auto()
    NEEDS_REVIEW = auto()
    MISSING = auto()
    NEW = auto()
    PLANNING = auto()
    IN_PROGRESS = auto()
    RUNNING = auto()
    READY_FOR_QA = auto()
    NEEDS_TESTING = READY_FOR_QA
    READY_FOR_MERGE = auto()
    OK = READY_FOR_MERGE
    MERGED = auto()
    WAIVED = auto()

    def label(self, prefix):
        """Return a formatted label string."""
        match self.name:
            case 'NEW':
                scope = NEW_STATE
            case 'NEEDS_REVIEW':
                scope = NEEDS_REVIEW_SUFFIX
            case 'PLANNING':
                scope = PLANNING_STATE
            case 'IN_PROGRESS':
                scope = IN_PROGRESS_STATE
            case 'READY_FOR_QA':
                if prefix == "JIRA":
                    scope = IN_PROGRESS_STATE
                else:
                    scope = NEEDS_TESTING_SUFFIX
            case 'READY_FOR_MERGE' | 'OK':
                scope = READY_SUFFIX
            case 'TESTING_FAILED':
                scope = TESTING_FAILED_SUFFIX
            case 'WAIVED':
                scope = TESTING_WAIVED_SUFFIX
            case _:
                scope = self.name.capitalize()
        return Label(f'{prefix}::{scope}')

    @classmethod
    def get(cls, type_str):
        """Return the MrScope that matches the type_str."""
        if scope := cls.__members__.get(type_str.upper(), cls.INVALID):
            return scope
        # We often use camelCase for the values but the enum members use underscores :/.
        type_str = ''.join('_' + char if char.isupper() else char for char in type_str).strip('_')
        return cls.__members__.get(type_str.upper(), cls.INVALID)


class MrState(IntEnum):
    """Possible states of a GL MR."""

    # https://docs.gitlab.com/ee/api/graphql/reference/#mergerequeststate
    UNKNOWN = auto()
    ALL = auto()
    CLOSED = auto()
    LOCKED = auto()
    MERGED = auto()
    OPENED = auto()

    @classmethod
    def from_str(cls, state):
        """Return the MrState matching the state string, on MrState.UNKNOWN."""
        return next((member for name, member in cls.__members__.items()
                     if name == state.upper()), cls.UNKNOWN)


class DCOState(IntEnum):
    """Possible Commit DCO check results."""

    OK = auto()
    MISSING = auto()
    UNRECOGNIZED = auto()
    EMAIL_MATCHES = auto()
    NAME_MATCHES = auto()
    OK_NOT_REDHAT = auto()

    @property
    def footnote(self):
        """Return the footnote string."""
        return DCO_FOOTNOTES[self]

    @property
    def title(self):
        """Return the name formatted pretty."""
        return self.name if self is DCOState.OK else self.name.replace('_', ' ').capitalize()


DCO_FOOTNOTES = {
    DCOState.OK: 'A matching valid DCO Signoff was found for this commit.',
    DCOState.MISSING: 'No valid DCO Signoff was found.',
    DCOState.UNRECOGNIZED: 'None of the commit Signoffs match the commit committer name or email.',
    DCOState.EMAIL_MATCHES: 'A Signed-off-by email matches, but the names do not.',
    DCOState.NAME_MATCHES: 'A Signed-off-by name matches, but the email addresses do not.',
    DCOState.OK_NOT_REDHAT: 'The commit committer email address is not @redhat.com.'
}


class Label(str):
    """Simple class to represent a GL label.

    For a string 'Example::primary::secondary':
    - gl_prefix is 'Example::primary'
    - prefix is 'Example'
    """

    def __init__(self, string):
        """Ensure no one has gone wild with the scopes."""
        if string.count('::') > 2:
            raise ValueError(f"Label string cannot have more than two :: pairs: '{string}'")
        super().__init__()

    @property
    def gl_prefix(self):
        """Return the GL-style first component of a scoped label , or the label if not scoped."""
        return self.rsplit('::', 1)[0]

    @property
    def prefix(self):
        """Return the first component of a scoped label, or the label itself if not scoped."""
        return self.split('::', 1)[0]

    @property
    def scope(self):
        """Return the MrScope of a scoped label, or None."""
        return MrScope.get(self.secondary or self.primary) if self.scoped else None

    @property
    def scoped(self):
        """Return 0 if not scoped, 1 if single scoped, 2 if double scoped, etc."""
        return self.count('::')

    @property
    def primary(self):
        """Return the second component of a scoped label, or None if not scoped."""
        return self.split('::', 2)[1] if self.scoped else None

    @property
    def secondary(self):
        """Return the third component of a double scoped label, or None if not scoped."""
        return self.split('::', 2)[2] if self.scoped >= 2 else None


MERGE_CONFLICT_LABEL = Label('Merge::Conflicts')
MERGE_WARNING_LABEL = Label('Merge::Warning')


class MessageType(Enum):
    """Known message types."""

    GITLAB = 'gitlab'
    UMB_BRIDGE = 'cki.kwf.umb-bz-event'
    JIRA = 'jira'
    DATAWAREHOUSE = 'datawarehouse'

    @classmethod
    def get(cls, type_str):
        """Return the MessageType that matches the type_str, or None."""
        try:
            return cls(type_str)
        except ValueError:
            return None


class GitlabObjectKind(IntEnum):
    """Known object_kind values for Gitlab."""

    # There are a few more kinds but these are the ones we use.
    PUSH = auto()
    MERGE_REQUEST = auto()
    NOTE = auto()
    PIPELINE = auto()
    BUILD = auto()
    JOB = BUILD
    ISSUE = auto()

    @classmethod
    def get(cls, type_str):
        """Return the matching GitlabObjectKind, or None."""
        return cls.__members__.get(type_str.upper(), None)


class DevStage(IntEnum):
    """Basic development stage values for RH."""

    UNKNOWN = 0
    ALPHA = auto()
    BETA = auto()
    ZSTREAM = auto()
    EARLY_ZSTREAM = auto()
    YSTREAM = auto()


class GitlabGID(str):
    """Wrapper for a Gitlab global ID string."""

    # https://docs.gitlab.com/ee/api/graphql/reference/#globalid
    gid_regex = re_compile(r'^gid://gitlab/(?P<active_record>.*)/(?P<record_id>\d*)$')

    def __init__(self, gid_str: str) -> None:
        """Parse the Gid string into its component bits."""
        if not (match := self.gid_regex.match(gid_str)):
            raise ValueError(f'Input is not a recognized Gitlab global ID: {gid_str}')
        self.record_type = match.group('active_record')
        self.id = int(match.group('record_id'))


@unique
class JiraField(StrEnum):
    # pylint: disable=invalid-name
    """Map pretty names to backend field name values."""

    assignee = 'assignee'
    components = 'components'
    versions = 'versions'
    fixVersions = 'fixVersions'
    issuelinks = 'issuelinks'
    issuetype = 'issuetype'
    labels = 'labels'
    priority = 'priority'
    project = 'project'
    reporter = 'reporter'
    status = 'status'
    subtasks = 'subtasks'
    summary = 'summary'
    updated = 'updated'
    resolution = 'resolution'
    Preliminary_Testing = 'customfield_12321540'
    Testable_Builds = 'customfield_12321740'
    Release_Blocker = 'customfield_12319743'
    Internal_Target_Milestone = 'customfield_12321040'
    Dev_Target_Milestone = 'customfield_12318141'
    Developer = 'customfield_12315141'
    QA_Contact = 'customfield_12315948'
    Commit_Hashes = 'customfield_12324041'
    Pool_Team = 'customfield_12317259'
    Reset_Contacts = 'customfield_12322640'
    Keywords = 'customfield_12323341'
    CVE_ID = 'customfield_12324749'
    Embargo_Status = 'customfield_12324750'
    Severity = 'customfield_12316142'
    Sub_System_Group = 'customfield_12320851'
    Release_Milestone = 'customfield_12319252'

    @classmethod
    def all(cls):
        """Return the list of all the field values."""
        return [field.value for field in cls]


RELEASE_MILESTONE_BOXES = {
    'submitter_checks_pass': '45450',
    'cki_tests_done': '45451',
    'cki_tests_pass': '45452',
    'mr_approved': '45453'
}
