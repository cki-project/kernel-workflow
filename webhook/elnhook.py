"""A webhook for handling new configs in kernel-ark (ELN specifically)."""

import sys
import typing

from cki_lib.logger import get_logger
from cki_lib.owners import Subsystem
from jira import Issue
from kwf_lib.common import TrackerType

from webhook import common
from webhook import jira
from webhook import kwf_tracker
from webhook.jirahook import JiraMR
from webhook.kconfigs import KconfigManager
from webhook.session import SessionRunner
from webhook.session_events import GitlabMREvent
from webhook.session_events import GitlabPushEvent

LOGGER = get_logger('cki.webhook.elnhook')


def _is_config_branch(branch: str) -> bool:
    """Check that this is a config branch."""
    if branch.startswith("configs/os-build/"):
        return True

    LOGGER.debug('The branch %s is not a config branch', branch)
    return False


def _is_new_branch(before: str) -> bool:
    """Check that this is a new branch."""
    # expecting the "before" commit hash to be set to all zeros
    if int(before, base=16) == 0:
        return True
    return False


def _fixup_description(message: str, mr_link: str = None) -> str:
    """Fix up the description by removing the SoB and adding the MR link if there is one."""
    mrinfo = ""
    msg = ""
    if mr_link:
        mrinfo = f'See Merge Request: {mr_link}'
    for line in message.splitlines():
        if not line.startswith('Signed-off-by:'):
            msg += line + '\n'
    msg += mrinfo
    return msg


def _get_components(session: SessionRunner, configs: list) -> set:
    """Return the jira componets for the specified kernel configs."""
    ssts: set[Subsystem] = set()
    components: set[str] = set()
    files: set[str] = set()
    # find the Kconfig file and use owners to find their jira component
    kconfig_manager = KconfigManager.new(session.args.repo_path)
    for config in configs:
        file = kconfig_manager(config)
        files.update(file)
        ssts.update(session.owners.get_matching_subsystems(file))

    # if there are no SSTs assigned, assign it to kernel/Other.
    if not ssts:
        LOGGER.info('No SSTs are assigned to %s', files)
        components.add('kernel / Other')
        return components

    # check that we're only returning components that Jira will accept
    for sst in ssts:
        component = sst.jira_component
        # The RHEL Jira project does not allow "kernel" as a component.
        if component == 'kernel':
            component = 'kernel / Other'
        LOGGER.debug('component: %s', component)
        components.add(component)
    return components


def _open_jira_issue(session, summary, desc, component) -> Issue:
    """Open a new Jira Issue."""
    issue = jira.create_issue(jira=session.jira,
                              summary=summary,
                              description=desc,
                              fixversion='eln',
                              component=component,
                              issuetype=TrackerType.TASK)
    if issue:
        LOGGER.info('JIRA: %s', issue.permalink())
    return issue


def process_config_branch(session: SessionRunner, event: GitlabPushEvent):  # pragma: no cover
    """Process a kernel-ark config branch push event."""
    modified: set[str] = set()
    commits = event.commits
    for commit in commits:
        modified.update(commit['modified'])
    LOGGER.debug('Modified: %s', modified)
    configs = common.extract_config_from_paths(modified)
    components = _get_components(session, configs)

    # probably a race condition - find the MR that should already be open for this issue
    gl = session.get_gl_project('cki-project/kernel-ark', session.gl_host)
    mrs = gl.mergerequests.list(state='opened', source_branch=event.target_branch)
    if len(mrs) == 1:
        mr_link = mrs[0].attributes['web_url']
        LOGGER.info('Found MR%s created from this source branch', mrs[0].attributes['iid'])
        LOGGER.info(mr_link)
        desc = _fixup_description(commits[0]['message'], mr_link)
    else:
        LOGGER.info('Found %s MRs using this source branch.', len(mrs))
        desc = _fixup_description(commits[0]['message'])

    # open a jira issue for this config change
    _open_jira_issue(session, commits[0]['title'], desc, components)


def process_config_merge_request(session: SessionRunner, mr: JiraMR) -> None:
    """Process a kernel-ark config MR."""
    # check if there is a jira issue linked to this MR
    issues = jira.get_issues_with_link(session.jira, mr.url)

    # Do not open any additional issues. In the future we may handled MR updates but exit for now.
    if issues:
        LOGGER.info('Found %s jira issue already linked to MR%s', len(issues), mr.iid)
        return

    components = _get_components(session, mr.config_items)

    # open a jira issue for this config change for each component.
    # (It's possible that multiple SSTs should review this.)
    desc = _fixup_description(mr.kwf_description, mr.url)
    issues.clear()
    for component in components:
        issue = _open_jira_issue(session, mr.title, desc, component)
        if issue:
            issues.append(issue)

    if issues:
        # link the MR to the Jira so that we don't create another issue for this MR
        trackers = kwf_tracker.make_trackers(issues, session)
        for tracker in trackers:
            mr.link_to_tracker(tracker)
        # update the MR description to include the JIRA tag(s)
        jira_tag = ''
        for issue in issues:
            jira_tag += f'JIRA: {issue.permalink()}  \n'
        mr.gl_mr.description = f'{jira_tag}{mr.gl_mr.description}'
        mr.gl_mr.save()
    else:
        LOGGER.info('No issues created, skipping MR updates.')


def process_push_event(  # pragma: no cover
    _: dict,
    session: SessionRunner,
    event: GitlabPushEvent,
    **__: typing.Any
) -> None:
    """Process a push event."""
    if _is_config_branch(event.target_branch) and _is_new_branch(event.before):
        process_config_branch(session, event)


def process_mr_event(
    _: dict,
    session: SessionRunner,
    event: GitlabPushEvent,
    **__: typing.Any
) -> None:
    """Process an MR event."""
    mr = JiraMR.new(session, event.mr_url,
                    source_path=session.args.repo_path,
                    merge_subsystems=True)
    if _is_config_branch(mr.source_branch):
        process_config_merge_request(session, mr)


HANDLERS = {
    # Disable push events for now. We may use this in the future.
    # GitlabPushEvent: process_push_event,
    GitlabMREvent: process_mr_event
}


def main(args):
    """Run main loop."""
    common.init_sentry()
    parser = common.get_arg_parser('ELNHOOK')
    args = parser.parse_args(args)

    session = SessionRunner.new('elnhook', args=args, handlers=HANDLERS)
    session.run()


if __name__ == "__main__":
    main(sys.argv[1:])
