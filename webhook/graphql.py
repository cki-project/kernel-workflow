"""Common helpers for graphql."""
import datetime
from functools import cached_property
import typing

from cki_lib import gitlab
from cki_lib.logger import get_logger
from cki_lib.misc import get_nested_key
from cki_lib.misc import is_production_or_staging

from webhook import fragments
from webhook.defs import ALL_MEMBERS_APPROVAL_RULE
from webhook.defs import GITFORGE
from webhook.defs import GitlabGID
from webhook.defs import GitlabURL
from webhook.defs import Label
from webhook.defs import MrState

if typing.TYPE_CHECKING:
    from webhook.users import User

LOGGER = get_logger('cki.webhook.graphql')

GET_USER_DETAILS_BASE = """
query userData {
  ...CurrentUser
}
"""

GET_USER_DETAILS_QUERY = GET_USER_DETAILS_BASE + fragments.CURRENT_USER + fragments.GL_USER

SET_MR_REVIEWERS_MUTATION_BASE = """
mutation setReviewers($input: MergeRequestSetReviewersInput!) {
  mergeRequestSetReviewers(input: $input) {
    mr: mergeRequest {
      reviewers {
        nodes {
          ...GlUser
        }
      }
    }
  }
}
"""

SET_MR_REVIEWERS_MUTATION = SET_MR_REVIEWERS_MUTATION_BASE + fragments.GL_USER

FIND_MEMBER_BASE = """
query mrData($namespace: ID!, $search_key: String!) {
  group(fullPath: $namespace) {
    groupMembers(search: $search_key) {
      nodes {
        user {
          ...GlUser
        }
      }
    }
  }
  project(fullPath: $namespace) {
    projectMembers(search: $search_key) {
      nodes {
        user {
          ...GlUser
        }
      }
    }
  }
}
"""

FIND_MEMBER_QUERY = FIND_MEMBER_BASE + fragments.GL_USER

GET_USER_BASE = """
query userQuery($username: String!) {
  user(username: $username) {
    ...GlUser
  }
}
"""

GET_USER_QUERY = GET_USER_BASE + fragments.GL_USER

GET_USER_BY_ID_BASE = """
query mrData($userid: UserID!) {
  user(id: $userid) {
    ...GlUser
  }
}
"""

GET_USER_BY_ID_QUERY = GET_USER_BY_ID_BASE + fragments.GL_USER

GET_MR_DESCRIPTIONS_QUERY = """
query mrData($namespace: ID!, $mr_ids: [String!]) {
  project(fullPath: $namespace) {
    mergeRequests(iids: $mr_ids) {
      nodes {
        iid
        description
      }
    }
  }
}
"""

MR_LABELS_QUERY_BASE = """
  query mrLabels($mr_id: String!, $namespace: ID!, $first: Boolean = true, $after: String = "") {
    project(fullPath: $namespace) {
      mr: mergeRequest(iid: $mr_id) {
        ...MrLabelsPaged @include(if: $first)
      }
    }
  }
"""

MR_LABELS_QUERY = MR_LABELS_QUERY_BASE + fragments.MR_LABELS_PAGED

ALL_MEMBERS_BASE = """
query data($namespace: ID!, $after: String = "") {
  %s(fullPath: $namespace) {
    %sMembers(after: $after) {
      pageInfo {
        hasNextPage
        endCursor
      }
      nodes {
        user {
          ...GlUser
        }
      }
    }
  }
}
"""

ALL_MEMBERS_QUERY = ALL_MEMBERS_BASE + fragments.GL_USER

ALL_PROJECT_ISSUES_QUERY = """
query data($namespace: ID!, $after: String = "") {
  project(fullPath: $namespace) {
    issues(after: $after, state: opened) {
      pageInfo {
        hasNextPage
        endCursor
      }
      nodes {
        iid
        title
        webUrl
      }
    }
  }
}
"""

CREATE_ISSUE_MUTATION = """
mutation createIssue($input: CreateIssueInput!) {
  createIssue(input: $input) {
    issue {
      iid
      webUrl
    }
  }
}
"""

CHECK_MR_STATE = """
query mr_state($mr_id: String!, $namespace: ID!) {
  project(fullPath: $namespace) {
    mr: mergeRequest(iid: $mr_id) {
      commitCount
      preparedAt
      state
      ...MrLabels
      targetBranch
    }
  }
}
"""

CHECK_MR_STATE_QUERY = CHECK_MR_STATE + fragments.MR_LABELS

MR_COMMENTS_BASE = """
query mrComments($mr_id: String!, $namespace: ID!, $after: String = "") {
  project(fullPath: $namespace) {
    mr: mergeRequest(iid: $mr_id) {
      id
      notes(filter: ONLY_COMMENTS, after: $after) {
        pageInfo {
          endCursor
          hasNextPage
        }
        nodes {
          ...GlNote
        }
      }
    }
  }
}
"""

MR_COMMENTS_QUERY = MR_COMMENTS_BASE + fragments.GL_NOTE + fragments.GL_USER

CREATE_NOTE_MUTATION_BASE = """
mutation createNote($input: CreateNoteInput!) {
  createNote(input: $input) {
    note {
      ...GlNote
    }
  }
}
"""

CREATE_NOTE_MUTATION = CREATE_NOTE_MUTATION_BASE + fragments.GL_NOTE + fragments.GL_USER

UPDATE_NOTE_MUTATION_BASE = """
mutation updateNote($input: UpdateNoteInput!) {
  updateNote(input: $input) {
    note {
      ...GlNote
    }
  }
}
"""

UPDATE_NOTE_MUTATION = UPDATE_NOTE_MUTATION_BASE + fragments.GL_NOTE + fragments.GL_USER

UPDATE_APPROVAL_RULE_BASE = """
mutation updateApprovalRule($input: MergeRequestUpdateApprovalRuleInput!) {
  mergeRequestUpdateApprovalRule(input: $input) {
    errors
    mergeRequest {
      approvalState {
        rules {
          ...MrApprovalRule
        }
      }
    }
  }
}
"""

UPDATE_APPROVAL_RULE_MUTATION = (
        UPDATE_APPROVAL_RULE_BASE + fragments.MR_APPROVAL_RULE + fragments.GL_USER
)

MR_MERGE_PERMISSION_QUERY = """
query mrMergePermission($namespace: ID!, $search_username: String!, $mr_gid: MergeRequestID!) {
  project(fullPath: $namespace) {
    projectMembers(search: $search_username) {
      nodes {
        user {
          username
        }
        mergeRequestInteraction(id: $mr_gid) {
          canMerge
        }
      }
    }
  }
}
"""


class StateTuple(typing.NamedTuple):
    """Tuple of state bits."""

    commit_count: int
    labels: typing.List[Label]
    prepared_at: typing.Union[datetime.datetime, None]
    state: MrState
    target_branch: str

    def __repr__(self) -> str:
        """Be brief."""
        repr_str = f'commits: {self.commit_count}, labels: {len(self.labels)}'
        repr_str += f', preparedAt: {self.prepared_at}, state: {self.state.name}'
        repr_str += f', target branch: {self.target_branch}'
        return f'<StateTuple {repr_str}>'


class GitlabGraph:
    """A wrapper object for interacting with gitlab graphql."""

    @staticmethod
    def _check_user(results, check_user):
        """Return True if the query currentUser username matches the check_user."""
        return check_user == results['currentUser']['username']

    @staticmethod
    def _check_keys(results, check_keys):
        """Return True if all the keys are in the given query results."""
        return check_keys <= results.keys()

    @classmethod
    def check_query_results(cls, results, check_keys=None, check_user=None):
        """Perform some optional checks of query results."""
        # See GitlabGraph.execute_query().
        # Ignore our own messages (for bots). Query must include 'currentUser' field.
        if check_user and cls._check_user(results, check_user):
            LOGGER.info('Ignoring message from %s.', check_user)
            return None

        # Raise an error if not all expected keys are in the results.
        if check_keys and not cls._check_keys(results, check_keys):
            raise RuntimeError(f'Gitlab did not return all keys {check_keys} in {results}.')
        return results

    def __init__(self, get_user=False, hostname=None):
        """Set up the client."""
        hostname = hostname or GITFORGE
        self.client = gitlab.get_graphql_client(hostname)
        LOGGER.info('Connected to %s.', hostname)
        if get_user:
            LOGGER.info('Logged in as %s (%s).', self.username, self.user_id)

    @cached_property
    def user(self):
        """Return the details of the user we are logged in as."""
        return self.client.query(GET_USER_DETAILS_QUERY, operation_name='userData')['currentUser']

    @property
    def user_id(self):
        """Return the global user ID of the user we are logged in as as an int."""
        if not self.user:
            return None
        return int(self.user['gid'].rsplit('/')[-1])

    @property
    def username(self):
        """Return the username we are logged in as."""
        if not self.user:
            return None
        return self.user['username']

    def get_user(self, username):
        """Return the user details if found, otherwise None."""
        if not username:
            raise ValueError(f"username must be a valid non-zero length string, not '{username}'")
        results = self.client.query(GET_USER_QUERY, {'username': username})
        return get_nested_key(results, 'user')

    def get_mr_descriptions(self, namespace, mr_ids):
        """Return a dict of descriptions with the mr_id as key."""
        params = {'namespace': namespace, 'mr_ids': [str(mr_id) for mr_id in mr_ids]}
        results = self.client.query(GET_MR_DESCRIPTIONS_QUERY, params)
        mrs = get_nested_key(results, 'project/mergeRequests/nodes', [])
        return {int(mr['iid']): mr['description'] for mr in mrs}

    def get_user_by_id(self, userid):
        """Return the user with the given GID, or None."""
        if isinstance(userid, int):
            userid = f'gid://gitlab/User/{userid}'
        if not isinstance(userid, str) or not userid.startswith('gid://gitlab/User/'):
            raise ValueError('userid must be an int or valid GID string.')
        results = self.client.query(GET_USER_BY_ID_QUERY, {'userid': userid})
        return results.get('user') if results else None

    def get_all_issues(self, namespace):
        """Return the list of all open Issues of the given project. Returns None if no project."""
        paged_key = 'project/issues'
        results = self.client.query(ALL_PROJECT_ISSUES_QUERY, {'namespace': namespace}, paged_key)
        if results and results.get('project') is None:
            LOGGER.warning('get_all_issues: project namespace not found: %s', namespace)
            return None
        return get_nested_key(results, f'{paged_key}/nodes', [])

    def get_all_members(self, namespace, namespace_type):
        """Return a dict of all group or poject members. Returns None if the ns is not found."""
        if namespace_type not in ('group', 'project'):
            raise ValueError(f"namespace_type must be 'group' or 'project', not {namespace_type}")
        params = {'namespace': namespace}
        paged_key = f'{namespace_type}/{namespace_type}Members'
        results = self.client.query(ALL_MEMBERS_QUERY % (namespace_type, namespace_type), params,
                                    paged_key)
        if results and results.get(namespace_type) is None:
            LOGGER.warning('get_all_members: %s namespace not found: %s', namespace_type, namespace)
            return None
        return {user['user']['username']: user['user'] for user in
                get_nested_key(results, f'{paged_key}/nodes', [])}

    def get_all_mr_labels(
        self, namespace: str,
        mr_id: int,
        existing_results: typing.Optional[dict] = None
    ) -> list[Label]:
        """Return the complete list of Label objects for the given MR."""
        # If the optional existing_results dict is provided it should be in the following format:
        # {'pageInfo': {'hasNextPage': bool, 'endCursor': str}, 'nodes': list[dict]}
        # In other words, the 'labels' field contents of an existing query result.
        existing_results = existing_results or {}
        has_next_page: bool = get_nested_key(existing_results, 'pageInfo/hasNextPage', True)
        end_cursor: str = get_nested_key(existing_results, 'pageInfo/endCursor', '')
        existing_results_nodes: list = existing_results.get('nodes', [])

        if has_next_page:
            params = {'namespace': namespace, 'mr_id': str(mr_id), 'after': end_cursor}
            more_results = self.client.query(MR_LABELS_QUERY, variable_values=params,
                                             paged_key='project/mr/labels/',
                                             operation_name='mrLabels')
            # If we didn't get back any labels nodes then we've been given bad input or maybe
            # don't have the right permissions. Blow up for now?
            if (
                label_nodes := get_nested_key(more_results, 'project/mr/labels/nodes', None)
            ) is None:
                raise ValueError(f"Unexpected missing labels nodes: {more_results}")
            existing_results_nodes.extend(label_nodes)
        return [Label(label['title']) for label in existing_results_nodes]

    def check_mr_state(self, namespace: str, mr_id: int) -> StateTuple:
        """Return a tuple with commit_count, prepared_at, state, and labels."""
        query_params = {'namespace': namespace, 'mr_id': str(mr_id)}
        results = self.client.query(CHECK_MR_STATE_QUERY, query_params)
        mr_data = get_nested_key(results, 'project/mr') or {}

        commits = int(mr_data.get('commitCount', 0))
        if prepared_at := mr_data.get('preparedAt'):
            prepared_at = datetime.datetime.fromisoformat(prepared_at[:19])
        state = MrState.from_str(mr_data.get('state', ''))
        labels = [Label(lbl['title']) for lbl in get_nested_key(mr_data, 'labels/nodes', [])]
        target_branch = mr_data.get('targetBranch', '')

        state_tuple = StateTuple(
            commit_count=commits,
            labels=labels,
            prepared_at=prepared_at,
            state=state,
            target_branch=target_branch
        )
        LOGGER.info(state_tuple)
        return state_tuple

    def create_project_issue(self, namespace, title, body, extra_input=None):
        """Create a new Issue with the given title and body on the given namespace."""
        params = {'input': {'projectPath': namespace, 'title': title, 'description': body}}
        if extra_input:
            params['input'].update(extra_input)
        if is_production_or_staging():
            results = self.client.query(CREATE_ISSUE_MUTATION, params)
            return results['createIssue']['issue']
        return {'iid': 0, 'webUrl': f'{GITFORGE}/{namespace}/-/issues/0'}

    def find_member(self, namespace, attribute, search_key):
        """Find the member of the given namespace with the matching attribute."""
        # This won't find anything if the search_key isn't in the first 100 results!
        if not attribute or not search_key:
            raise ValueError('attibute and search_key must be non-zero length strings.')
        params = {'namespace': namespace, 'search_key': search_key}
        results = self.client.query(FIND_MEMBER_QUERY, params)
        users = get_nested_key(results, 'group/groupMembers/nodes', []) or \
            get_nested_key(results, 'project/projectMembers/nodes', [])
        return next((user['user'] for user in users if
                     user['user'].get(attribute, None) == search_key), None)

    def find_member_by_email(self, namespace, email, username):
        """Find the member with the given email that has the given username."""
        # This won't find anything if the search_key isn't in the first 100 results!
        if not email or not username:
            raise ValueError('email and username must be non-zero length strings.')
        params = {'namespace': namespace, 'search_key': email}
        results = self.client.query(FIND_MEMBER_QUERY, params)
        users = get_nested_key(results, 'group/groupMembers/nodes', []) or \
            get_nested_key(results, 'project/projectMembers/nodes', [])
        return next((user['user'] for user in users if
                     user['user']['username'] == username), None)

    def set_mr_reviewers(self, namespace, mr_id, usernames, mode):
        """Perform a mergeRequestSetReviewers mutation and returns the new list of reviewers."""
        # https://docs.gitlab.com/ee/api/graphql/reference/#mutationmergerequestsetreviewers
        valid_modes = ('APPEND', 'REMOVE', 'REPLACE')
        if mode not in valid_modes:
            raise ValueError(f'mode {mode} is invalid. Must be one of: {valid_modes}')
        if not usernames:
            raise ValueError('usernames must be a non-empty iterable')
        if not is_production_or_staging():
            return [{'username': username} for username in usernames]

        params = {'input': {'projectPath': namespace, 'iid': str(mr_id),
                            'reviewerUsernames': list(usernames),
                            'operationMode': mode
                            }}
        LOGGER.debug('Running mergeRequestSetReviewers mutation with: %s', params)
        if not (results := self.client.query(SET_MR_REVIEWERS_MUTATION, params)):
            raise RuntimeError('Set reviewers mutation did not return expected results.')
        return get_nested_key(results, 'mergeRequestSetReviewers/mr/reviewers/nodes', [])

    def get_mr_comments(
        self,
        namespace: str,
        mr_id: int,
    ) -> tuple[str, list[dict[str, typing.Any]]]:
        """Query the full list of comments on the given MR and return it with the MR global ID."""
        params = {'namespace': namespace, 'mr_id': str(mr_id)}
        results = self.client.query(MR_COMMENTS_QUERY, params, paged_key='project/mr/notes')

        if not (mr_data := get_nested_key(results, 'project/mr')):
            raise RuntimeError(f'No MR data returned for mrComments query: {namespace}!{mr_id}')

        return mr_data['id'], mr_data['notes']['nodes']

    def create_note(self, noteable_id: str, body: str) -> dict[str, typing.Any]:
        """Create a new note with the given body and return the new note details."""
        params = {'input': {'noteableId': noteable_id, 'body': body}}

        LOGGER.info("Creating new webhook comment:\n%s", body)
        if is_production_or_staging():
            results = self.client.query(CREATE_NOTE_MUTATION, params)
            return results['createNote']['note']
        # This return value is a subset of what is returned in the real case but we can at least
        # fake the createdAt timestamp.
        created_at = datetime.datetime.now(tz=datetime.UTC).isoformat()
        return {
            'author': self.user,
            'id': 'gid://gitlab/Note/0',
            'body': body,
            'createdAt': created_at,
            'updatedAt': created_at
        }

    def update_note(self, note_id: str, body: str) -> dict[str, typing.Any]:
        """Update the note of NoteID with the given body and return the new note details."""
        params = {'input': {'id': note_id, 'body': body}}

        LOGGER.info("Overwriting existing webhook comment:\n%s", body)
        if is_production_or_staging():
            results = self.client.query(UPDATE_NOTE_MUTATION, params)
            return results['updateNote']['note']
        # This return value is a subset of what is returned in the real case but we can at least
        # fake the updatedAt timestamp.
        updated_at = datetime.datetime.now(tz=datetime.UTC).isoformat()
        return {'author': self.user, 'id': note_id, 'body': body, 'updatedAt': updated_at}

    def update_mr_approval_rule(
        self,
        *,
        mr_url: GitlabURL,
        rule_gid: GitlabGID,
        rule_name: str,
        approvals_required: int,
        eligible_approvers: list["User"],
    ) -> dict:
        # pylint: disable=too-many-arguments
        """Update the given approval rule with the given values and return the new rule dict."""
        # The rule will not be considered valid by gitlab if it requires more approvals than there
        # are eligible approvers.
        # Except in the case where this is the special 'All Members' rule when the
        # eligibleApprovers list is typically empty.
        if rule_name != ALL_MEMBERS_APPROVAL_RULE and approvals_required > len(eligible_approvers):
            raise ValueError(f'approvals_required ({approvals_required}) should be <= '
                             f'len(eligible_approver_ids): {eligible_approvers}')

        params = {
            'approvalRuleId': rule_gid.id,
            'approvalsRequired': approvals_required,
            'iid': str(mr_url.id),
            'name': rule_name,
            'projectPath': mr_url.namespace,
            'userIds': [str(user.gid.id) for user in eligible_approvers]
        }

        # Create a fake result payload in the non prod-staging case.
        if not is_production_or_staging():
            # Transform the users.User objects into a fragments.GL_USER style dict.
            faux_users = []
            for user in eligible_approvers:
                faux_users.append({
                    'gid': str(user.gid),
                    'name': user.name,
                    'username': user.username,
                    'email': user.emails[0] if user.emails else ''
                })

            return {
                'approvalsRequired': approvals_required,
                'id': rule_gid,
                'name': rule_name,
                'type': 'REGULAR',
                'eligibleApprovers': faux_users
            }

        results = self.client.query(UPDATE_APPROVAL_RULE_MUTATION, {'input': params})

        # If there are errors then raise.
        if errors := get_nested_key(results, 'mergeRequestUpdateApprovalRule/errors'):
            raise RuntimeError(f'Errors updating rule {params}: {errors}')

        # If the rule we just updated can't be found then raise.
        rules_key = 'mergeRequestUpdateApprovalRule/mergeRequest/approvalState/rules'
        raw_rules = get_nested_key(results, rules_key, [])
        raw_approval_rule = next((rule for rule in raw_rules if rule['id'] == rule_gid), None)

        if not raw_approval_rule:
            raise RuntimeError(f'Cannot find approval rule after update: {params}, {raw_rules}')

        return raw_approval_rule

    def user_can_merge(self, namespace: str, mr_gid: GitlabGID, username: str) -> bool:
        """Return True if the user has permission to merge the given MR, otherwise False."""
        params = {
            'namespace': namespace,
            'search_username': username,
            'mr_gid': mr_gid
        }

        results = self.client.query(MR_MERGE_PERMISSION_QUERY, params)
        user_nodes = get_nested_key(results, 'project/projectMembers/nodes', [])
        searched_user = \
            next((user for user in user_nodes if user['user']['username'] == username), None)

        if not searched_user or searched_user.get('mergeRequestInteraction') is None:
            raise RuntimeError(f"Bad results for params: {params}: {results}")

        return searched_user['mergeRequestInteraction']['canMerge']
