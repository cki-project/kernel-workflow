"""Jira module."""
import typing

from cki_lib.logger import get_logger
from cki_lib.misc import is_production_or_staging
from jira.exceptions import JIRAError
from jira.resources import Comment
from jira.resources import Issue
from jira.resources import PinnedComment
from kwf_lib.common import TrackerType
from kwf_lib.defs import KWF_JQL_FILTER

if typing.TYPE_CHECKING:
    from jira import JIRA
    from jira.resources import Resource

LOGGER = get_logger('cki.webhook.jira')


def update_issue(
    issue: Issue,
    *,
    fields: dict[str, typing.Any],
    update: dict[str, typing.Any],
    **kwargs
) -> None:
    """Update the python jira Issue with the given 'fields' & 'updates' values."""
    # https://jira.readthedocs.io/api.html#jira.resources.Issue.update
    LOGGER.info('Updating %s with fields: %s, update: %s, kwargs: %s',
                issue.key, fields, update, kwargs)

    # If we're not in a production or staging environment then don't actually do anything.
    if not is_production_or_staging():
        return

    # "keyword arguments will generally be merged into fields, except lists, which will be merged
    # into updates."
    issue.update(fields=fields, update=update, **kwargs)


def jql_query(jira: 'JIRA', jql_str: str) -> list[Issue]:
    """Run a jira JQL query and return the list of jira.resources.Issues objects."""
    LOGGER.debug('query jql: %s', jql_str)
    # A JIRAError is raised if one of the requested issues cannot be found.
    try:
        raw_issues = jira.search_issues(jql_str)
    except JIRAError as err:
        LOGGER.warning('%s (%s)', repr(err), err.status_code)
        raw_issues = []

    LOGGER.debug('query returned: %s', [issue.key for issue in raw_issues])
    return raw_issues


def get_issues(
    jira: 'JIRA',
    *,
    issue_ids: typing.Iterable[str] | None = None,
    cve_ids: typing.Iterable[str] | None = None,
    with_linked: bool = False,
    filter_kwf: bool = False
) -> list[Issue]:
    """
    Return a list of jira.resources.Issues matching the given list of jira keys or CVE labels.

    issue_ids: issues to try to fetch; these are never filtered.
    cve_ids: include issues with the given CVE ID label or CVE_ID field value.
    with_linked: whether to also fetch issues linked to the given issue_ids.
    filter_kwf: whether to limit the cve_ids & with_linked portion of the query to
                kernel workflow issues (see kwf_jql_filter()).

    """
    if not issue_ids and not cve_ids:
        raise ValueError('Search lists are empty.')

    LOGGER.info('Querying %s for issue_ids: %s, cve_ids: %s, filter_kwf: %s, with_linked: %s',
                jira.client_info(), issue_ids, cve_ids, filter_kwf, with_linked)

    kwf_jql = f'({KWF_JQL_FILTER}) AND' if filter_kwf else ''
    ids_jql = ''
    jql_ors: list[str] = []

    if issue_ids:
        ids_jql = ' OR '.join(f'key={issue}' for issue in issue_ids)

        if with_linked:
            jql_ors.append(' OR '.join(f'issue in linkedIssues({key})' for key in issue_ids))

    if cve_ids:
        jql_ors.append(f"labels in ({', '.join(cve_ids)})")
        jql_ors.append(' OR '.join(f'"CVE ID" ~ {cve}' for cve in cve_ids))

    ors_str = ' OR '.join(jql_ors)

    if ids_jql and ors_str:
        jql_str = f'{ids_jql} OR ({kwf_jql} ({ors_str}))'
    elif ors_str:
        jql_str = f'{kwf_jql} ({ors_str})'
    else:
        jql_str = ids_jql

    return jql_query(jira, jql_str)


def get_issue(jira: 'JIRA', key: str) -> Issue | None:
    """Fetch and return the jira.resources.Issue with the given key."""
    issue: Issue | None = None

    try:
        issue = jira.issue(key)
    except JIRAError as err:
        LOGGER.warning('API response for %s triggered exception: %s (%s)',
                       key, repr(err), err.status_code)

    return issue


def get_issues_with_link(jira: 'JIRA', url: str) -> list[Issue]:
    """Fetch the list of KWF Issues which have a Remote Link for the given url."""
    if not url:
        raise RuntimeError('url must not be empty.')

    LOGGER.info('Querying %s for KWF issues linked to url: %s', jira.client_info(), url)

    jql_str = f'({KWF_JQL_FILTER}) AND issueFunction in linkedIssuesOfRemote("{url}")'
    return jql_query(jira, jql_str)


def get_matching_comments(
    comments: typing.Iterable[Comment | PinnedComment],
    substring: str,
    author_usernames: typing.Iterable[str] | None = None
) -> list[Comment | PinnedComment]:
    """Return the list of (Pinned)Comment objects which match on substring and author username."""
    matching_comments: list[Comment] = []

    for comment in comments:
        real_comment: Comment = comment.comment if isinstance(comment, PinnedComment) else comment

        if substring not in real_comment.body:
            continue

        if author_usernames and real_comment.author.name not in author_usernames:
            continue

        matching_comments.append(comment)

    return matching_comments


def delete_resource(resource: 'Resource', raise_on_404: bool = False) -> bool:
    """Call the Resource.delete() method in prod and catch any 404s. Return True on success."""
    # https://jira.readthedocs.io/api.html#jira.resources.Resource.delete
    action = 'Deleting' if is_production_or_staging() else 'Pretending to delete'
    LOGGER.info('%s resource %s.', action, repr(resource))

    # If this is not production/staging then don't actually do anything.
    if not is_production_or_staging():
        return True

    try:
        resource.delete()

    except JIRAError as err:
        # 404 generally means the Resource was not found and so in most cases we can carry on.
        if err.status_code != 404 or raise_on_404:
            raise

        LOGGER.info('Ignored 404 status code response while deleting %s: %s', repr(resource), err)
        return False

    return True


def update_resource(resource: 'Resource', **kwargs: typing.Any) -> None:
    """Call the Resource.update() method with the given kwargs in prod."""
    # https://jira.readthedocs.io/api.html#jira.resources.Resource.update
    action = 'Updating' if is_production_or_staging() else 'Pretending to update'
    LOGGER.info('%s resource %s with: %s', action, repr(resource), kwargs)

    # If this is not production/staging then don't actually do anything.
    if not is_production_or_staging():
        return

    resource.update(**kwargs)


# pylint: disable=too-many-arguments,too-many-positional-arguments
def create_issue(jira: 'JIRA',
                 summary: str,
                 description: str,
                 fixversion: str,
                 component: str,
                 issuetype: TrackerType,
                 project: str = 'RHEL'
                 ) -> Issue | None:
    """Open a new Jira Issue."""
    issue = None
    issue_fields = {
        'project': {'key': project},
        'summary': summary,
        'description': description,
        'issuetype': {'name': issuetype.value[0]},
        'fixVersions': [{'name': fixversion}],
        'components': [{'name': component}]
    }
    LOGGER.debug('Create Jira issue: %s', issue_fields)

    if is_production_or_staging():
        issue = jira.create_issue(fields=issue_fields)
        LOGGER.info('Opened Jira issue: %s (%s)', issue.key, summary)
    else:
        LOGGER.info('No issue created when in development mode.')
    return issue
