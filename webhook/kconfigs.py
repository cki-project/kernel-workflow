"""Module to provide kernel config lookups."""
from collections import defaultdict
from dataclasses import dataclass
from dataclasses import field
from pathlib import Path
import re
import subprocess
import typing

from cki_lib.logger import get_logger

LOGGER = get_logger('cki.webhook.kconfig')

# This is kind of a sloppy regex but there are Kconfig files with odd spacing that we must accept.
KCONFIG_FILE_REGEX = re.compile(r'^\s?(menuconfig|config)\s+(?P<config>.*)\s*$', re.MULTILINE)

KconfigMapping = dict[str, set[str]]


@dataclass
class KconfigManager:
    """Manage kernel configuration lookups."""

    kernel_source: Path
    _kernel_configs: KconfigMapping | None = field(default=None)
    _kernel_head_sha: str | None = field(default=None)

    def _check_cache(self) -> None:
        """Check if the kernel_source has changed, and if so, rebuild the _kernel_configs."""
        current_head = _get_head_sha(self.kernel_source)

        if current_head == self._kernel_head_sha:
            return

        first_time = self._kernel_head_sha is None

        LOGGER.info('HEAD is %s%s, %sloading data from %s.', current_head[:12],
                    f' (was {self._kernel_head_sha[:12]})' if not first_time else '',
                    '' if first_time else 're', self.kernel_source)

        # Save the current HEAD and rebuild the mapping.
        self._kernel_head_sha = current_head
        self._kernel_configs = _build_kernel_config_map(self.kernel_source)

    def __call__(self, config_name: str, arch: str = '') -> list[str]:
        """Return the list of names of the paths associated with the given config_name."""
        self._check_cache()
        return _lookup_kconfig(self._kernel_configs, config_name, arch)

    @classmethod
    def new(cls, kernel_source: str) -> typing.Self:
        """Construct a new ConfigManager instance."""
        if not kernel_source:
            raise RuntimeError('No kernel_source path given.')

        kernel_source_path = Path(kernel_source)
        LOGGER.info('Using %s for kernel_source.', kernel_source_path)

        if not kernel_source_path.is_dir():
            raise RuntimeError(f'Path {kernel_source_path} does not exist or is not a directory!')

        return cls(kernel_source_path)


def _build_kernel_config_map(kernel_source: Path) -> KconfigMapping:
    """
    Build a mapping of CONFIG names to Kconfig paths from the given kernel_source.

    CONFIG names as defined in Kconfig files do not have a `CONFIG_` prefix. We store them here
    as-is and lookups can just .removeprefix('CONFIG_') and not have to worry about it.
    """
    if not (kconfig_files := list(kernel_source.rglob('Kconfig*'))):
        raise RuntimeError(f'No Kconfig data found in {kernel_source}.')

    data: KconfigMapping = defaultdict(set)

    for kconfig in kconfig_files:
        for match in KCONFIG_FILE_REGEX.findall(kconfig.read_text()):
            data[match[1]].add(str(kconfig.relative_to(kernel_source)))

    LOGGER.info('Kernel Kconfig mapping contains %d items.', len(data))
    return data


def _get_head_sha(git_path: str | Path) -> str:
    """Return the HEAD sha of the given local git repo path."""
    command = f'git -C {git_path} rev-parse HEAD'.split()
    return subprocess.run(command, capture_output=True, check=True, text=True).stdout.strip()


def _lookup_kconfig(kconfig_map: KconfigMapping, config_name: str, arch: str = '') -> list[str]:
    """Return the list of names of the paths associated with the given config_name."""
    # Tear off any `CONFIG_` prefix because a KconfigMapping should not have them.
    name_without_prefix = config_name.removeprefix('CONFIG_')

    if paths := kconfig_map.get(name_without_prefix):
        paths = [path for path in paths if f'/{arch}/' in path] if arch else list(paths)
        LOGGER.debug('%s%s matches paths: %s', config_name, f' ({arch})' if arch else '', paths)
        return paths

    LOGGER.warning('%s does not match any path!', config_name)
    return []
