"""Git repo manipulation/check functions used by kernel webhooks."""
from datetime import datetime
from datetime import timedelta
import os
import pkgutil
import subprocess
import typing

from cki_lib import logger
from cki_lib import messagequeue
from cki_lib import yaml as cki_lib_yaml

from webhook.temp_utils import load_and_validate

LOGGER = logger.get_logger('cki.webhook.kgit')

GIT_REPOS_SCHEMA = cki_lib_yaml.load(
    contents=pkgutil.get_data(__name__, 'schema_rh_kernel_git_repos.yaml')
)


def _git(src_dir, args, input_data=None, **kwargs):
    """Run a git via subprocess (for things GitPython doesn't support)."""
    kwargs.setdefault('check', True)
    # pylint: disable=subprocess-run-check
    LOGGER.debug("Running command: git %s", " ".join(args))
    return subprocess.run(['git'] + args, cwd=src_dir, input=input_data,
                          capture_output=True, text=True, **kwargs)


def branch_copy(location, branch):
    """Save a copy of the branch for later use w/git reset."""
    _git(location, ['branch', '--copy', branch])


def checkout(location, branch):
    """Reset the current checkout in location to branch."""
    _git(location, ['checkout', branch])


def branches_differ(location, branch_a, branch_b):
    """Compare two branches, return True if branches differ."""
    ret = _git(location, ['diff', '--stat', branch_a, branch_b])
    return bool(ret.stdout)


def hard_reset(location, branch):
    """Reset the current checkout in location to branch."""
    _git(location, ['reset', '--hard', branch])


def add(location, path):
    """Git add file(s) at path to repo."""
    _git(location, ['add', path])


def commit(location, message):
    """Commit data to repo with provided commit message."""
    _git(location, ['commit', '-s', '-m', message])


def raw_diff(location, base, path):
    """Get raw diff."""
    if base:
        diff_args = ['diff', base, '--', path]
    else:
        diff_args = ['diff', path]
    return _git(location, diff_args)


def merge(location, reference):
    """Merge reference (quietly, with on commit message editing) to location."""
    _git(location, ['merge', '--quiet', '--no-edit', reference])


def cherry_pick(location, reference):
    """Cherry-pick the given commit to location, with cherry-picked ref and signoff."""
    _git(location, ['cherry-pick', '-x', '--signoff', reference])


def branch_delete(location, branch):
    """Delete git branch with extreme prejudice."""
    _git(location, ['branch', '-D', branch])


def get_remote_name(location: str, repo_urls: typing.Iterable[str]) -> str:
    """Return the name of the first git remote in the repo that matches one of the repo_urls."""
    result = _git(location, ['remote', '-v'])

    for line in result.stdout.splitlines():
        line_parts = line.split(maxsplit=3)

        if len(line_parts) >= 3 and line_parts[2] == '(fetch)':
            if line_parts[1] in repo_urls:
                return line_parts[0]

    return ''


def remotes(location):
    """Fetch and return list of git remotes."""
    return _git(location, ['remote'], encoding='utf8').stdout.splitlines()


def pull_remote_branch(location, remote_name, branch):
    """Do a git pull on remote/branch."""
    _git(location, ['pull', remote_name, branch])


def fetch_remote(location, remote_name):
    """Fetch one git remote by name."""
    LOGGER.info("Fetching git remote %s", remote_name)
    try:
        _git(location, ['fetch', remote_name])
    except subprocess.CalledProcessError as err:
        LOGGER.warning('Fetch of %s failed: %s', remote_name, err)


def fetch_all(location):
    """Do a fetch of all remotes."""
    git_remotes = remotes(location)
    for remote_name in git_remotes:
        fetch_remote(location, remote_name)


def worktree_add(location, branch, worktree, target_branch):
    """Add worktree and branch."""
    _git(location, ['worktree', 'add', '-B', branch, worktree, target_branch])


def worktree_remove(location, worktree):
    """Delete worktree and branch."""
    _git(location, ['worktree', 'remove', '-f', worktree])


def patch_id(location, sha):
    """Get stable patch-id value for given commit sha."""
    patch_text = _git(location, ['show', sha]).stdout
    ret = _git(location, ['patch-id', '--stable'], input_data=patch_text)
    if not ret.stdout:
        return ""

    return ret.stdout.splitlines()[0].split()[0]


def setup_git_user(location, name, email):
    """Configure git user name and email address."""
    _git(location, ['config', 'user.name', name])
    _git(location, ['config', 'user.email', email])


def branch_mergeable(worktree_dir, target_branch, merge_branch):
    """Ensure MR branch can be merged to target branch."""
    hard_reset(worktree_dir, target_branch)
    try:
        merge(worktree_dir, merge_branch)
    except subprocess.CalledProcessError:
        return False

    return True


def create_worktree_timestamp(worktree_dir):
    """Create a timestamp file in the worktree."""
    now = datetime.now()
    ts_file = f"{worktree_dir}/timestamp"
    with open(ts_file, 'w', encoding='ascii') as ts_fh:
        ts_fh.write(now.strftime('%Y%m%d%H%M%S'))
        ts_fh.close()


def clean_up_temp_merge_branch(src_dir, merge_branch, worktree_dir):
    """Clean up the git worktree and branches from the test merges."""
    worktree_remove(src_dir, worktree_dir)
    branch_delete(src_dir, merge_branch)
    LOGGER.debug("Removed worktree %s and deleted branch %s", worktree_dir, merge_branch)


def handle_stale_worktree(src_dir, merge_branch, worktree_dir):
    """Ensure we're not stomping on another run, and clean it up if it's old."""
    LOGGER.warning("Worktree already exists at %s!", worktree_dir)
    ts_file = f"{worktree_dir}/timestamp"
    if not os.path.exists(ts_file):
        create_worktree_timestamp(worktree_dir)

    with open(ts_file, 'r', encoding='ascii') as ts_fh:
        old_ts = ts_fh.read()
        ts_fh.close()

    if old_ts < (datetime.now() - timedelta(hours=1)).strftime('%Y%m%d%H%M%S'):
        LOGGER.warning("Stale worktree at %s is over 1h old, purging it.", worktree_dir)
        clean_up_temp_merge_branch(src_dir, merge_branch, worktree_dir)
        try:
            branch_delete(src_dir, f"{merge_branch}-save")
        except subprocess.CalledProcessError:
            return
    else:
        raise messagequeue.QuietNackException("Worktree less than 1h old, trying back later.")


def prep_temp_merge_branch(remote_name, gl_mergerequest, src_dir):
    """Set up the temporary branch/worktree to test mergeability in."""
    # This target_branch is dependent on the repo layout in utils/rh_kernel_git_repos.yml,
    # where everything is an additional remote on top of kernel-ark, w/remote name == project name
    target_branch = f"{remote_name}/{gl_mergerequest.target_branch}"
    merge_branch = f"{remote_name}-{gl_mergerequest.target_branch}-{gl_mergerequest.iid}"
    worktree_dir = f"/{'/'.join(src_dir.strip('/').split('/')[:-1])}/{merge_branch}-merge/"

    # Ensure we don't have a stale/failed checkout lingering
    if os.path.isdir(worktree_dir):
        handle_stale_worktree(src_dir, merge_branch, worktree_dir)

    # This is the lookaside cache we maintain for examining diffs between revisions of a
    # merge request, which we're going to create temporary worktrees off of
    LOGGER.info("Creating git worktree at %s with branch %s for mergeability testing, please hold",
                worktree_dir, merge_branch)
    worktree_add(src_dir, merge_branch, worktree_dir, target_branch)
    return merge_branch, worktree_dir


RepoConfig = dict[str, typing.Any]
RepoConfigs = dict[str, RepoConfig]


def load_repo_configs(file_path: str, extra_file_path: str = '') -> RepoConfigs:
    """Load the dict of repository configs from the given file_path and optional extra_file_path."""
    if not file_path:
        raise RuntimeError('file_path must be set.')

    file_paths = [file_path, extra_file_path] if extra_file_path else [file_path]
    repo_data: RepoConfigs = {}

    for path in file_paths:
        repo_list: list[dict, str] = load_and_validate(path, GIT_REPOS_SCHEMA)['repositories']

        repo_names = [repo['name'] for repo in repo_list]
        LOGGER.debug('file: %s, repo_names: %s', path, repo_names)

        # Validate that all the remote names in this file are unique.
        if duplicate_repo_names := {name for name in repo_names if repo_names.count(name) > 1}:
            raise ValueError(f'Duplicate repository names found in {path}: {duplicate_repo_names}')

        # Warn if the file's data is going to override an existing key.
        for repo_name in repo_names:
            if repo_name in repo_data:
                LOGGER.warning("'%s' repo from %s overrides existing entry.", repo_name, path)

        # Put the repository data from the file into a dict.
        repo_data.update({repo['name']: repo for repo in repo_list})

    # Validate that only one repo has 'checkout' set.
    has_checkout_set = [repo['name'] for repo in repo_data.values() if repo['checkout']]
    if len(has_checkout_set) > 1:
        raise ValueError(
            f'More than one repository from {file_paths} has checkout set: {has_checkout_set}'
        )

    LOGGER.info('Found %d repo configs in files %s.', len(repo_data), file_paths)
    return repo_data
