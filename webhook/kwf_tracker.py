"""Red Hat issue tracker interaction."""
from collections import UserDict
from dataclasses import dataclass
from dataclasses import field
from functools import cached_property
from functools import singledispatch
from pathlib import Path
import typing

from bugzilla.bug import Bug
from cki_lib.logger import get_logger
from jira.resources import Issue
from kwf_lib.common import BugzillaID
from kwf_lib.common import CveID
from kwf_lib.common import IdType
from kwf_lib.common import IssueID
from kwf_lib.common import JiraKey
from kwf_lib.common import TrackerID
from kwf_lib.protocol import KwfTrackerProtocol
from kwf_lib.tracker import BaseTracker
from kwf_lib.tracker import CveFlaw
from kwf_lib.tracker import IssueTracker
from kwf_lib.tracker import JsonDict
from kwf_lib.tracker import make_tracker as _make_tracker
from kwf_lib.tracker import make_tracker_id

from webhook.bugzilla import get_bug
from webhook.bugzilla import get_bugs
from webhook.jira import delete_resource
from webhook.jira import get_issue
from webhook.jira import get_issues
from webhook.jira import update_resource

if typing.TYPE_CHECKING:
    from jira.resources import Comment
    from jira.resources import PinnedComment
    from jira.resources import RemoteLink

    from webhook.rh_metadata import Branch
    from webhook.rh_metadata import Project
    from webhook.session import BaseSession

LOGGER = get_logger('cki.webhook.tracker')


class SessionProtocol(typing.Protocol):
    # pylint: disable=too-few-public-methods
    """Provides a session attribute."""

    session: 'BaseSession'


@dataclass(repr=False, kw_only=True, unsafe_hash=True)
class BasicTracker(BaseTracker):
    """BaseSession mixin for tracker.IssueTracker."""

    session: 'BaseSession'
    _cache: 'TrackerCache | None' = field(default=None, hash=False)

    def __post_init__(self) -> None:
        """Say hello."""
        LOGGER.info('Created %s', self)

    @cached_property
    def _api_object(self) -> Bug | Issue:
        """Return the appropriate API object for this issue."""
        if not self._data:
            raise RuntimeError('No raw data available to create API object.')

        # The python jira module does not give us a nice way to update an issue without a
        # jira.resources.Issue instance so provide one here :/.
        match self.id:
            case JiraKey():
                return Issue({}, self.session.jira, self._data)

            # Do the same for bugzilla just because?
            case BugzillaID() | CveID():
                return Bug(self.session.bugzilla, dict=self._data)

        raise TypeError(f"No api_object for {self}.")

    @property
    def is_bugzilla(self) -> bool:
        """Return True if this Tracker is in bugzilla."""
        return isinstance(self.id, (BugzillaID, CveID))

    @property
    def is_jira(self) -> bool:
        """Return True if this Tracker is in jira."""
        return isinstance(self.id, JiraKey)


@dataclass(repr=False, kw_only=True, unsafe_hash=True)
class RemoteLinksMixin(SessionProtocol, KwfTrackerProtocol):
    # pylint: disable=too-few-public-methods
    """Mixin to provide a customized remote_links getter for jira."""

    _jira_remote_links: list['RemoteLink'] = field(default_factory=list, hash=False)

    @property
    def remote_links(self) -> list[str]:
        """Return the list of remote URLs attached to this tracker, fetching them if needed."""
        if self._remote_links is None and isinstance(self.id, JiraKey):
            self._jira_remote_links = self.session.jira.remote_links(self.id)
            self._remote_links = [link.object.url for link in self._jira_remote_links]

        return self._remote_links or []


TrackerDict = dict[IssueID, 'KwfIssueTracker']


@dataclass(repr=False, kw_only=True, unsafe_hash=True)
class KwfFlawTracker(RemoteLinksMixin, BasicTracker, CveFlaw):
    # pylint: disable=too-many-ancestors
    """Red Hat issue tracker representation of a CVE flaw."""

    @property
    def trackers(self) -> TrackerDict:
        """Return the dict of KwfIssueTrackers associated with this CVE."""
        if self._cache is None:
            raise RuntimeError(f'Cache is not set for {self}.')

        result = {
            tracker.id: tracker for tracker in self.find_trackers(self.cve_id, self._cache.trackers)
        }

        return typing.cast(TrackerDict, result)


CveDict = dict[CveID: KwfFlawTracker]


@dataclass(repr=False, kw_only=True, unsafe_hash=True)
class KwfIssueTracker(RemoteLinksMixin, BasicTracker, IssueTracker):
    # pylint: disable=too-many-ancestors
    """Red Hat issue tracker."""

    def _require_jira(self, method_name: str) -> None:
        """Raise a NotImplemented exception if this is not a jira tracker."""
        if not self.is_jira:
            raise NotImplementedError(f'Cannot use {method_name} on non-jira {self}.')

    @cached_property
    def rh_branch(self) -> 'Branch | None':
        """Return the rh_metadata.Branch associated with this FixVersion and component, or None."""
        return self.session.rh_projects.get_matching_branch(
            self.fix_version,
            self.component,
            confidential=bool(self.embargoed)
        )

    @cached_property
    def rh_project(self) -> 'Project | None':
        """Return the rh_metadata.Project associated with this tracker, or None."""
        return self.rh_branch.project if self.rh_branch else None

    @property
    def cves(self) -> CveDict:
        """Return the dict of KwfFlawTrackers associated with this IssueTracker."""
        return {cve_id: self._cache.get(cve_id) for cve_id in self.cve_ids}

    @property
    def testing_tasks(self) -> list[typing.Self]:
        """Return the list of testing Tasks linked to this issue."""
        return self.find_testing_tasks(self, self._cache.trackers)

    @property
    def variant_trackers(self) -> list[typing.Self]:
        """Return the list of variant trackers."""
        return self.find_variant_trackers(self, self._cache.trackers)

    @property
    def ystream_tracker(self) -> typing.Self | None:
        """Return the first tracker in the ystream_trackers list, if any."""
        return self.find_ystream_tracker(self, self._cache.trackers)

    @property
    def zstream_trackers(self) -> list[typing.Self]:
        """Return the list of zstream clones related to this issue, assuming it is a ystream."""
        return self.find_zstream_trackers(self, self._cache.trackers)

    def comments(self) -> list['Comment']:
        """Return the list of jira.resources.Comment objects for this tracker."""
        self._require_jira('comments')
        return self.session.jira.comments(self.id)

    def pinned_comments(self) -> list['PinnedComment']:
        """Return the list of jira.resources.PinnedComment objects for this tracker."""
        self._require_jira('pinned_comments')
        return self.session.jira.pinned_comments(self.id)

    def post_comment(self, comment_text: str, pinned: bool = False) -> None:
        """Post the given comment to this issue."""
        self._require_jira('post_comment')

        if not comment_text:
            raise ValueError('comment_text must not be empty.')

        LOGGER.info('Posting %s to %s:\n%s',
                    'pinned comment' if pinned else 'comment', self.id, comment_text)

        if not self.session.is_production_or_staging:
            return

        comment = self.session.jira.add_comment(self.id, comment_text)

        if pinned:
            self.session.jira.pin_comment(self.id, comment.id, True)

    def remove_remote_link(self, url: str) -> bool:
        """Remove any Remote Links for the given URL and return True if work was done."""
        self._require_jira('remove_remote_link')

        if url not in self.remote_links:
            LOGGER.info('%s: No existing RemoteLink to %s, nothing to do.', self.id, url)
            return False

        LOGGER.info('%s: Removing all RemoteLinks to %s ...', self.id, url)

        links_removed = False

        for link in self._jira_remote_links:
            if link.object.url != url:
                continue

            if delete_resource(link):
                links_removed = True

        return links_removed

    def set_remote_link(
        self,
        url: str,
        title: str,
        link_gid: str | None = None,
        extra_destination: dict | None = None,
        remove_duplicates: bool = True
    ) -> bool:
        # pylint: disable=too-many-arguments,too-many-positional-arguments
        """
        Create or update a Remote Link pointing to the given URL.

        Return True if a new link was created, otherwise False.
        """
        self._require_jira('set_remote_link')

        # See if there is an existing link which matches the input, and any duplicates for the URL.
        matching_link, duplicate_links = \
            _get_matching_link(self._jira_remote_links, url, link_gid)

        # Build the "destination" dict for the jira.add_remote_link() call from the given extras.
        destination = extra_destination.copy() if extra_destination else {}
        destination.update({'url': url, 'title': title})

        # If there is an existing link for this URL then see if it matches what we want.
        if matching_link:
            LOGGER.info('%s: Found existing RemoteLink for %s: %s',
                        self.id, url, repr(matching_link))

            if getattr(matching_link, 'globalId', None) == link_gid and \
               (destination.items() <= matching_link.raw['object'].items()):
                LOGGER.info('%s: Existing RemoteLink %s matches input, nothing to do.',
                            self.id, repr(matching_link))
                return False

            LOGGER.info("%s: Updating existing RemoteLink %s with globalId: %s",
                        self.id, url, link_gid)

            update_resource(matching_link, object=destination, globalID=link_gid)

            if remove_duplicates and duplicate_links:
                LOGGER.info('Removing duplicate RemoteLinks: %s', duplicate_links)

                for duplicate_link in duplicate_links:
                    delete_resource(duplicate_link)

            return False

        LOGGER.info("%s: Adding new RemoteLink %s with globalId: %s", self.id, url, link_gid)

        if self.session.is_production_or_staging:
            self.session.jira.add_remote_link(self.id, destination, globalId=link_gid)

        return True


KWF_CLASSES: dict[IdType, typing.Type[CveFlaw | IssueTracker]] = {
    BugzillaID: KwfIssueTracker,
    CveID: KwfFlawTracker,
    JiraKey: KwfIssueTracker
}


@singledispatch
def make_tracker(
    input_data: int | str | Path | JsonDict,
    session: 'BaseSession'
) -> BasicTracker | None:
    """Construct a Tracker from the given input data."""
    if tracker_id := make_tracker_id(input_data):
        return _make_tracker_from_id(tracker_id, session)

    return _make_tracker(input_data, BasicTracker, KWF_CLASSES, session=session)


@make_tracker.register(Bug | Issue)
def _make_tracker_from_api(input_obj: Bug | Issue, session: 'BaseSession') -> BasicTracker | None:
    """Construct a Tracker from the given bugzilla.bug.Bug or jira.resources.Issue."""
    input_data: JsonDict = {}

    if isinstance(input_obj, Issue):
        input_data = input_obj.raw
    if isinstance(input_obj, Bug):
        input_data = input_obj.get_raw_data()

    return make_tracker(input_data, session)


@make_tracker.register(TrackerID)
def _make_tracker_from_id(tracker_id: TrackerID, session: 'BaseSession') -> BasicTracker | None:
    """Construct a Tracker from the given TrackerID."""
    api_object: Issue | Bug | None = None

    match tracker_id:
        case JiraKey():
            if not (api_object := get_issue(session.jira, tracker_id)):
                LOGGER.warning('API did not return any data for %s', tracker_id)
                return None

        case BugzillaID() | CveID():
            if not (api_object := get_bug(session.bugzilla, tracker_id)):
                LOGGER.warning('No data returned from API for BZ %s.', tracker_id)
                return None

        case _:
            raise TypeError(f'tracker_id ({tracker_id}) has unexpected type {type(tracker_id)}.')

    return _make_tracker_from_api(api_object, session)


def make_trackers(
    input_data: list[TrackerID | Bug | Issue] | set[TrackerID | Bug | Issue],
    session: 'BaseSession'
) -> list[BasicTracker]:
    """
    Construct Trackers from the input list or set and return them as a list.

    The resulting list will include linked trackers.
    """
    new_trackers: dict[TrackerID, BasicTracker] = {}
    bug_ids: set[BugzillaID] = set()
    cve_ids: set[CveID] = set()
    jira_ids: set[JiraKey] = set()

    for item in input_data:
        if isinstance(item, (Bug, Issue)):
            tracker = _make_tracker_from_api(item, session)
            new_trackers[tracker.id] = tracker
            continue

        if tracker_id := make_tracker_id(item):
            match tracker_id:
                case BugzillaID():
                    bug_ids.add(tracker_id)
                case CveID():
                    cve_ids.add(tracker_id)
                case JiraKey():
                    jira_ids.add(tracker_id)

            continue

        LOGGER.warning("Ignoring unknown input '%s'", item)

    if bug_ids or cve_ids:
        raw_bugs = get_bugs(session.bugzilla, bug_ids | cve_ids)
        new_trackers.update({bug.id: bug for bug in make_trackers(raw_bugs, session)})

    if jira_ids or cve_ids:
        raw_issues = get_issues(session.jira, issue_ids=jira_ids, cve_ids=cve_ids, with_linked=True,
                                filter_kwf=True)
        new_trackers.update({issue.id: issue for issue in make_trackers(raw_issues, session)})

    return list(new_trackers.values())


TrackerCacheDict = dict[IssueID, BasicTracker]


class TrackerCache(UserDict[TrackerCacheDict]):
    """A cache for BasicTrackers.

    The dict keys are a BugzillaID or a JiraKey.
    The dict values can be a BasicTracker, KwfIssueTracker, or a KwfFlawTracker.

    When __getitem__ is called it tries to return the matching item in the cache, fetching it from
    the tracker API if needed.

    When the API is queried we also look for and cache any linked issues and any related
    CVE flaws/trackers.

    When __setitem__ is called it sets the tracker's _cache attribute to point to the TrackerCache
    instance.

    If given a CveID, __getitem__() will try to return the KwfFlawTracker with a matching
    cve_id attribute value.
    """

    def __init__(
        self,
        input_data: dict[IssueID, BasicTracker] | None = None,
        *,
        session: 'BaseSession'
    ) -> None:
        """Store the BaseSession."""
        self.session = session

        super().__init__(input_data.copy() if isinstance(input_data, dict) else {})

    @staticmethod
    def __makekey__(key: str | int) -> TrackerID:
        """Convert the given key into a TrackerID."""
        if not (tracker_id := make_tracker_id(key)):
            raise TypeError(f"{key}' is not recognized as a valid tracker ID.")
        return tracker_id

    def __contains__(self, key: typing.Any) -> bool:
        """Return True if the given key exists in the cache."""
        return key in self.data or key in self.cves

    def __getitem__(self, key: str | int) -> BasicTracker:
        """
        Return the matching BasicTracker from the cache, fetching it first in necessary.

        The input must convert to a TrackerID or a TypeError is raised.

        If the input is found in self.cves (is a CveID) then we return that KwfFlawTracker.

        Otherwise, the input is passed to UserDict __getitem__ which will call __missing__ if
        the key is not in our dict.
        """
        tracker_id = self.__makekey__(key)

        if flaw := self.cves.get(tracker_id):
            return flaw

        return super().__getitem__(tracker_id)

    def __missing__(self, key: TrackerID) -> BasicTracker:
        """Try to fetch the missing tracker and return it, or raise KeyError."""
        result = self.cache_cves([key]) if isinstance(key, CveID) else self.cache_issues([key])

        if key not in result:
            raise KeyError(key)

        return result[key]

    def __setitem__(self, key: str | int, issue_tracker: BasicTracker) -> None:
        """Point the KwfIssueTracker's _cache attribute to this cache and then add it."""
        tracker_id = self.__makekey__(key)

        if not isinstance(issue_tracker, BasicTracker):
            raise TypeError(f"'{issue_tracker}' is not a BasicTracker or subclass instance.")

        # Point the BasicTracker's _cache to this cache.
        issue_tracker._cache = self

        super().__setitem__(tracker_id, issue_tracker)

    @property
    def cves(self) -> CveDict:
        """Return the dict of KwfFlawTrackers in the cache."""
        return {cve.cve_id: cve for cve in self.data.values() if isinstance(cve, KwfFlawTracker)}

    @property
    def trackers(self) -> TrackerDict:
        """Return the dist of KwfIssueTrackers in the cache (so BasicTrackers are excluded)."""
        return {issue.id: issue for issue in self.data.values() if
                isinstance(issue, KwfIssueTracker)}

    def get(self, key: str | int, default: typing.Any = None) -> BasicTracker | None:
        """Return the matching BasicTracker from the cache, fetching it first in necessary."""
        try:
            return self[key]
        except KeyError:
            pass

        return default

    def cache_cves(self, cve_ids: typing.Iterable[CveID]) -> CveDict:
        """Fetch the flaw and all trackers of the given CVE IDs."""
        if not (cve_ids := set(cve_ids)):
            return {}

        # Fetch & cache the requested CVE flaw trackers from bugzilla & jira.
        self.update({tracker.id: tracker for tracker in make_trackers(cve_ids, self.session)})

        return {k: v for k, v in self.cves.items() if k in cve_ids}

    def cache_issues(self, tracker_ids: typing.Iterable[TrackerID]) -> TrackerCacheDict:
        """Fetch the given issues & friends from their API, then cache & return them as a dict."""
        if not (tracker_ids := set(tracker_ids)):
            return {}

        # Fetch & cache the requested issues (this also gets any issues they are linked to).
        new_issues = {issue.id: issue for issue in make_trackers(tracker_ids, self.session)}
        self.update(new_issues)

        new_cve_ids: set[CveID] = set()
        migrated_ids: set[BugzillaID | JiraKey] = set()

        # Update the set of CveIDs with any new from the requested issues and find any migrated IDs.
        for issue in new_issues.values():
            if not isinstance(issue, (KwfIssueTracker, KwfFlawTracker)):
                continue

            new_cve_ids.update(cve_id for cve_id in issue.cve_ids if cve_id not in tracker_ids)

            if issue.migrated_id and issue.migrated_id not in new_issues:
                migrated_ids.add(issue.migrated_id)

        if new_cve_ids:
            # As well as the flaw from bugzilla this fetches all the associated tracker issues from
            # jira so its jql query may return some issues we just fetched but that's okay.
            self.cache_cves(new_cve_ids)

        # If the requested issues were migrated from bugzilla then fetch & cache their original BZs.
        if migrated_ids:
            self.update({issue.id: issue for issue in make_trackers(migrated_ids, self.session)})

        return {k: v for k, v in dict(self.data | self.cves).items() if k in tracker_ids}


# Internal functions.

def _get_matching_link(
    links: list['RemoteLink'],
    url: str,
    global_id: str | None = None
) -> tuple['RemoteLink | None', list['RemoteLink']]:
    """Return the RemoteLink from the list that best matches the input, plus any duplicates."""
    matching_link: 'RemoteLink | None' = None

    # If given a global_id (aka "globalId") then look for that without considering the URL.
    if global_id:
        matching_link = next(
            (link for link in links if hasattr(link, 'globalId') and link.globalId == global_id),
            None
        )

    # Find all the input links which match the URL.
    url_matches: list['RemoteLink'] = [link for link in links if link.object.url == url]

    # If we matched on globalId then return it, and every URL match which isn't it.
    if matching_link:
        return matching_link, [link for link in url_matches if link is not matching_link]

    # If we did not match on globalId but have URL matches, then take the first one as the best
    # match and any others as duplicates.
    if url_matches:
        return url_matches[0], url_matches[1:]

    # If we didn't match anything then say so.
    return None, []
