"""Backport-specific variable definitions and functions."""
from dataclasses import dataclass
from dataclasses import field
from functools import cached_property
import re
import subprocess
import typing

from cki_lib.logger import get_logger
from git import Repo
from git.exc import BadName
from git.exc import GitCommandError

from webhook import kgit
from webhook.defs import BOT_FORK_NAMESPACE_BASE
from webhook.defs import JIStatus
from webhook.rhissue import RHIssue
from webhook.session import SessionRunner

if typing.TYPE_CHECKING:
    from git import Commit
    from gitlab.v4.objects import Project

    from webhook.rh_metadata import Branch
    from webhook.rh_metadata import Project as RHProject

LOGGER = get_logger('cki.webhook.libbackport')
MAX_PATCHES_FULL_LOG = 1
KWF_BACKPORT_SUCCESS_LABEL = 'kwf-backport-success'
KWF_BACKPORT_FAIL_LABEL = 'kwf-backport-fail'


def mr_for_branch_exists(gl_project: 'Project', bp_branch: str, target_branch: str) -> bool:
    """Check if an MR created from this branch already exists."""
    return bool(gl_project.mergerequests.list(all=True, state='opened', target_branch=target_branch,
                                              source_branch=bp_branch))


def trim_title_for_mr(title: str) -> str:
    """Clean up the Jira bug summary for use as MR title."""
    # Example input: "TRIAGE CVE-1999-2000 kernel: bug: this is a jira bug summary [rhel-8.10.z]"
    # Resulting output: "CVE-1999-2000: bug: this is a jira bug summary"

    # Remove any lingering ProdSec "TRIAGE" bit from the front of the title
    trimmed = title.removeprefix('TRIAGE').strip()
    if 'kernel:' in trimmed:
        parts = trimmed.split(':')
        # Remove " kernel" from the "CVE-xxxx-yyyy kernel:" portion of the title
        parts[0] = parts[0].removesuffix('kernel').strip()
        trimmed = ':'.join(parts)
    # Remove the z-stream "[rhel-x.y]" trailer from the title
    if '[rhel' in trimmed:
        parts = trimmed.split('[')
        trimmed = ''.join(parts[:-1]).strip()
    return trimmed


@dataclass
class Backport:
    # pylint: disable=too-many-instance-attributes
    """Wrapper for a backport attempt."""

    rhissue: RHIssue
    session: SessionRunner
    description: str = ''
    mr_url: str = ''
    status: str = ''
    provided_hashes: list = field(default_factory=list, init=False)

    def __repr__(self) -> str:
        """Print out a user-friendly summary of the backport attempt."""
        msg = (f'Backport attempt:\n'
               f' * Issue: {self.rhissue.id} (Fix Version: {self.rhissue.ji_fix_version})\n'
               f' * Target branch: {self.project.name}/{self.branch.name}\n'
               f' * Project namespace: {self.project.namespace}\n'
               f' * Backport directory: {self.repo.working_tree_dir}\n'
               f' * Source branch: {self.repo.active_branch.name}\n'
               f' * Commit(s): {self.shas}')
        return f'<{self.__class__.__name__} {self.rhissue.id}\n{msg}>'

    @cached_property
    def ksrc(self) -> str:
        """The kernel source directory we're operating in."""
        return self.session.args.rhkernel_src

    @property
    def shas(self) -> typing.List[str]:
        """Return the list of commit SHAs to try to backport."""
        return self.provided_hashes or self.rhissue.ji_commit_hashes

    @property
    def project(self) -> typing.Union['RHProject', None]:
        """Return the Red Hat project matched to the jira fix version."""
        return self.rhissue.ji_project

    @property
    def branch(self) -> typing.Union['Branch', None]:
        """Return the branch matched to the jira fix version."""
        return self.rhissue.ji_branch

    @cached_property
    def bp_branch(self) -> str:
        """The constructed name of the backport branch we'll operate in."""
        return f"backport-{self.rhissue.id}-{self.project.name}-{self.branch.name}"

    @cached_property
    def worktree_dir(self) -> str:
        """The constructed name of the git worktree directory we'll operate in."""
        return f"/{'/'.join(self.ksrc.strip('/').split('/')[:-1])}/{self.bp_branch}/"

    @cached_property
    def repo(self) -> 'Repo':
        """Prepare branch/worktree, return git Repo object where backport operations happen."""
        # This target_branch is dependent on the repo layout in utils/rh_kernel_git_repos.yml,
        # where everything is an additional remote on top of kernel-ark,
        # w/remote name == project name
        proj_target_branch = f"{self.project.name}/{self.branch.name}"
        # This is the lookaside cache we maintain for examining diffs between revisions of a
        # merge request, which we're going to create temporary worktrees off of
        LOGGER.info("Creating git worktree at %s for backport attempt, please hold",
                    self.worktree_dir)
        try:
            kgit.worktree_add(self.ksrc, self.bp_branch, self.worktree_dir, proj_target_branch)
        except subprocess.CalledProcessError:
            LOGGER.info("Worktree for %s backport already exists, reusing it...", self.rhissue.id)
            kgit.hard_reset(self.worktree_dir, proj_target_branch)

        return Repo(self.worktree_dir)

    def should_try_backport(self) -> str:
        """Evaluate if we should try backporting for this Jira issue or not."""
        log_msg = ''
        labels = self.rhissue.ji.fields.labels
        is_testing = self.session.args.testing

        if self.rhissue.ji_status not in (JIStatus.NEW, JIStatus.PLANNING) and not is_testing:
            log_msg = f'Aborting backport, the JIRA Issue {self.rhissue.id} not New or Planning.'

        elif not self.rhissue.ji_fix_version:
            log_msg = f'Aborting due to no Fix Version set in {self.rhissue.id}.'

        elif not self.rhissue.ji_project:
            log_msg = f'No project found for environment, aborting {self.rhissue.id} backport.'

        elif not self.rhissue.ji_branch or not self.rhissue.ji_branch.allow_backporter:
            log_msg = f'No backport automation for {self.rhissue.ji_branch}'

        elif self.rhissue.ji_branch.inactive:
            log_msg = f'Skipping inactive branch: {self.rhissue.ji_branch}'

        elif KWF_BACKPORT_FAIL_LABEL in labels and not is_testing:
            log_msg = f'Prior backport attempt for {self.rhissue.id} already failed, aborting.'

        elif KWF_BACKPORT_SUCCESS_LABEL in labels and not is_testing:
            log_msg = (f'Prior backport attempt for {self.rhissue.id} was successful, '
                       'no need to try again.')

        elif not self.rhissue.ji_commit_hashes:
            log_msg = f'Aborting due to {self.rhissue.id} having no Commit Hashes specified.'

        elif self.rhissue.mr_urls and not is_testing:
            log_msg = f'Aborting due to existing MR in {self.rhissue.id}.'

        return log_msg

    def fetch_and_prune_required_remotes(self) -> bool:
        """Fetch and prune the target remote and bot remote for this backport attempt."""
        try:
            remote = self.repo.remote(self.rhissue.ji_project.name)
            bot_remote = self.repo.remote(f'bot-{self.rhissue.ji_project.name}')
        except ValueError:
            LOGGER.error("One or more required remotes for %s not found in repo",
                         self.rhissue.ji_project.name)
            return False

        try:
            remote.fetch()
            bot_remote.fetch(prune=True)
        except GitCommandError as err:
            LOGGER.error("Unable to fetch required remotes for %s: %s",
                         self.rhissue.ji_project.name, err)
            return False

        return True

    @cached_property
    def backport_commits(self) -> dict[str, 'Commit'] | None:
        """Get requested backport commits from repo or return None and save error messages."""
        all_reachable = True
        commits = {}
        for sha in self.shas:
            try:
                commits[sha] = self.repo.commit(sha)
            except (BadName, ValueError) as error:
                all_reachable = False
                self.status += f"Commit ref not found in backporter local repo cache: {error}\n"

        return commits if all_reachable else None

    def commits_already_backported(self) -> bool:
        """Check to see if the commit was already backported in y-stream."""
        if not self.rhissue.ji_branch or not self.rhissue.ji_branch.ystream:
            return False

        gl_project = self.session.get_gl_project(self.rhissue.ji_project.namespace)
        for sha in self.shas:
            full_sha = self.backport_commits[sha].hexsha
            possible_commits = gl_project.search('commits', full_sha)
            patterns = (f'commit {full_sha}', f'(cherry picked from commit {full_sha})')
            for commit in possible_commits:
                if any(pat in commit['message'] for pat in patterns):
                    self.status = (f"Commit {full_sha} appears to have already been backported "
                                   f"to {self.project.name}/{self.branch.name} as {commit['id']}. "
                                   "You may be able to close this issue as already fixed.\n")
                    return True

        return False

    def try_to_backport(self, extra_info: list[str] = None) -> bool:
        """Attempt to backport the sha(s) to the given project/target_branch."""
        rhmetadata = f'JIRA: {self.rhissue.jira_link}'
        for cve in self.rhissue.ji_cves:
            rhmetadata += f'  \nCVE: {cve}'

        self.description = f'{rhmetadata}\n\n'
        extra_info = extra_info or []
        self.description += '\n'.join(extra_info)
        patch_line_prefix = " *" if self.rhissue.ji_branch.lead_stream else " * Y-Commit:"

        # This initializes the worktree and branch
        repo = self.repo
        LOGGER.info("Repo object initialized at %s", self.worktree_dir)
        commit_backported = False

        if not self.fetch_and_prune_required_remotes() or not self.backport_commits:
            return False

        # if these commits were already backported and this is y-stream, bail out
        if self.rhissue.ji_branch.ystream and self.commits_already_backported():
            return False

        for sha in self.shas:
            if not self.rhissue.ji_branch.lead_stream and self.rhissue.ji_cves:
                commit_data = self.backport_commits[sha]
                if not any(cve in commit_data.message for cve in self.rhissue.ji_cves) and \
                   sha not in self.rhissue.ji_commit_hashes:
                    LOGGER.info("Skipping commit %s, because it doesn't reference a CVE", sha)
                    continue

            ycommit = ''
            try:
                kgit.cherry_pick(self.worktree_dir, sha)
            except subprocess.CalledProcessError:
                self.status = (f"Unable to auto-backport commits for {self.rhissue.id} "
                               f"(failed on {sha}).\n")
                return False

            signoff = (f"Signed-off-by: {repo.head.commit.committer.name} "
                       f"<{repo.head.commit.committer.email}>")
            title = repo.head.commit.summary
            patch_desc = repo.head.commit.message.replace(f"{signoff}\n", "")
            patch_desc = re.sub("JIRA: ", "Y-JIRA: ", patch_desc)
            patch_desc = re.sub("\nBugzilla: ", "\nY-Bugzilla: ", patch_desc)
            # Insert y-commit info and don't use git log reformatting for commit message
            if not self.rhissue.ji_branch.lead_stream:
                ycommit = f"Y-Commit: {sha}\n"
                glog = patch_desc
            else:
                glog = repo.git.log(f'{sha}^..{sha}')
            if len(self.shas) > MAX_PATCHES_FULL_LOG:
                self.description += f"{patch_line_prefix} {sha} {title}  \n"
            else:
                # Patch commit message, less our signoff and existing bz/jira tags
                self.description += f'```\n{glog}```\n'
            new_msg = f'{title}\n\n{rhmetadata}\n{ycommit}\n{glog}'
            repo.git.commit('--reset-author', '-s', '--amend', '-m', new_msg)
            commit_backported = True

        if not commit_backported:
            self.status = (f"Unable to auto-backport commits for {self.rhissue.id}, none of the "
                           "commits were viable -- bad commit list in a CVE bug?\n")
            return False

        self.description += f"\n{signoff}"
        self.description += self.session.comment.gitlab_footer()

        self.status = f"Successfully backported commits {self.shas} for {self.rhissue.id}.\n"
        return True

    def submit_merge_request(self) -> bool:
        """Submit a merge request using the successful auto-backport."""
        LOGGER.info("Creating backport for %s from %s", self.rhissue.id, self.worktree_dir)
        mr_title = trim_title_for_mr(self.rhissue.ji.fields.summary)

        gl_project = self.session.get_gl_project(self.rhissue.ji_project.namespace)
        fork_ns = f'{BOT_FORK_NAMESPACE_BASE}/{gl_project.name}'
        fork_project = self.session.gl_instance.projects.get(fork_ns)
        target_branch = self.rhissue.ji_branch.name
        if self.session.args.testing:
            self.status += (f'Not pushing in testing mode. Backport details: {self}\n'
                            f'Source Project: {fork_project.path_with_namespace}\n'
                            f'Target Project: {gl_project.path_with_namespace}\n'
                            f'MR Details:\n{mr_title}\n\n{self.description}\n\n'
                            f'HEAD Commit details:\n{self.repo.head.commit.message}')
            return False

        try:
            bot_remote = self.repo.remote(f'bot-{self.rhissue.ji_project.name}')
        except ValueError:
            self.status += (f'Backporter bot remote bot-{self.rhissue.ji_project.name} not found '
                            'in git checkout.\n')
            return False

        refspec = f'refs/heads/{self.bp_branch}'
        if self.repo.active_branch.name not in bot_remote.refs:
            try:
                bot_remote.push(refspec=refspec).raise_if_error()
            except GitCommandError as err:
                self.status += f'Branch push failed, aborting operation: {err}.\n'
                return False
        else:
            LOGGER.info("Existing backport branch found in bot remote")
            existing_ref = f'refs/remotes/bot-{self.rhissue.ji_project.name}/{self.bp_branch}'
            if kgit.branches_differ(self.worktree_dir, self.bp_branch, existing_ref):
                bot_remote.push(refspec=refspec, force=True)
                self.status += f'Updated existing branch for {self.rhissue.id}.\n'
            else:
                self.status += 'Latest backport attempt does not differ from prior one\n'

        if mr_for_branch_exists(gl_project, self.bp_branch, target_branch):
            self.status += 'Existing open MR found, not creating a new one'
            return False

        mr_data = {
            'source_branch': self.bp_branch,
            'target_branch': target_branch,
            'title': f'Draft: {mr_title}',
            'description': self.description,
            'target_project_id': gl_project.id
        }
        new_mr = fork_project.mergerequests.create(mr_data)
        self.mr_url = new_mr.web_url
        self.status += f'New MR: {self.mr_url}'
        return True

    def cleanup(self) -> None:
        """Remove the worktree and branch(es) used for backport attempt."""
        kgit.worktree_remove(self.ksrc, self.worktree_dir)
        kgit.branch_delete(self.ksrc, self.bp_branch)

    def add_info_to_jira_issue(self, success: bool) -> None:
        """Add backport success or failure label to Jira issue."""
        assignee_name = self.rhissue.assignee.name if self.rhissue.assignee else None
        developer_name = self.rhissue.developer.name if self.rhissue.developer else None
        notify_user = assignee_name or developer_name or None
        notify_tag = f"[~{notify_user}]" if notify_user else "the assigned subsystem team"

        jira_labels = self.rhissue.ji.fields.labels
        label = KWF_BACKPORT_SUCCESS_LABEL
        if success:
            comment = ("KWF backport automation successfully created a merge request for "
                       f"this Jira issue at {self.mr_url}. This issue still needs human "
                       f"review by {notify_tag}, the merge request taken out of draft, "
                       "and developer review done in the merge request to progress.\n")
        else:
            label = KWF_BACKPORT_FAIL_LABEL
            comment = ("KWF backport automation was unable to create a merge request for "
                       f"this Jira issue. Manual intervention from {notify_tag} required.\n")
            comment += self.status
        comment += self.session.comment.jira_footer()
        jira_labels.append(label)
        LOGGER.info("Adding label %s to Jira issue %s and leaving comment: %s",
                    label, self.rhissue.id, comment)
        if self.session.is_production and not self.session.args.testing:
            rh_only_comment = {'type': 'group', 'value': 'Red Hat Employee'}
            self.rhissue.ji.update(fields={"labels": jira_labels})
            self.session.jira.add_comment(self.rhissue.id, comment, visibility=rh_only_comment)
