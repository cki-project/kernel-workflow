"""Library for interacting with jira."""
from dataclasses import dataclass
from dataclasses import field
from functools import wraps
from os import environ
import time
import typing

from cki_lib.logger import get_logger
from cki_lib.misc import get_nested_key
from cki_lib.misc import is_production_or_staging
from cki_lib.misc import only_log_exceptions
from cki_lib.session import RequestLogger
from cki_lib.yaml import load
from jira import JIRA
from jira.exceptions import JIRAError
from jira.resources import Issue
from jira.resources import User

from webhook.defs import FixVersion
from webhook.defs import GITFORGE
from webhook.defs import IssueSeverity
from webhook.defs import JIRA_BOT_ACCOUNTS
from webhook.defs import JIRA_BOT_ASSIGNEES
from webhook.defs import JIStatus
from webhook.defs import JIType
from webhook.defs import JPFX
from webhook.defs import JiraField
from webhook.defs import KWF_SUPPORTED_ISSUE_COMPONENTS

if typing.TYPE_CHECKING:
    from webhook.rhissue import RHIssue

LOGGER = get_logger('cki.webhook.libjira')


class KwfJiraClient(JIRA):
    """Jira client wrapper with cki logging and retries on 401 errors."""

    def __init__(self, *args: str, **kwargs: typing.Any) -> None:
        """Attach the jira client session to the cki logger."""
        super().__init__(*args, **kwargs)

        # Setting this stops JIRA.add_remote_link() from trying to access the endpoint
        # rest/applinks/latest/listApplicationlinks . For most RH users that is going to return
        # a 401 and then our wrapper will waste time retrying it. If this attribute exists then
        # JIRA.applicationlinks() will immediately return it and avoid the whole check.
        self._applicationlinks = []

        with only_log_exceptions():
            RequestLogger(LOGGER).hook(self._session)

    def __getattribute__(self, name: str) -> typing.Any:
        """Call the JIRA function but retry on 401."""
        attr = super().__getattribute__(name)
        if not callable(attr):
            return attr

        @wraps(attr)
        def wrapper(*args, **kwargs):
            retry = 0
            while True:
                try:
                    ret = attr(*args, **kwargs)
                    return ret
                except JIRAError as e:
                    LOGGER.warning('Got JIRAError, url: %s, status code: %s',
                                   e.url, e.status_code)
                    if e.status_code == 401:
                        if retry < 3:
                            retry = retry + 1
                            time.sleep(0.3*retry)
                            LOGGER.warning('Retrying.., attempt no. %d', retry)
                            continue
                    raise e
        return wrapper


def connect_jira(token_auth: str | None = None, host: str | None = None) -> JIRA:
    """Connect and return an authenticated jira.client.JIRA object."""
    def remove_scheme(url: str) -> str:
        """Return the input string with the url scheme removed."""
        return url.split('://', 1)[1] if '://' in url else url

    host = host or environ.get('JIRA_SERVER', 'https://issues.redhat.com')

    # The format of JIRA_TOKENS mirrors GITLAB_TOKENS as used by cki_lib.gitlab.get_token().
    if not token_auth:
        token_env = load(contents=environ.get('JIRA_TOKENS', '{}')).get(remove_scheme(host))
        token_auth = environ.get(token_env) if token_env else environ.get('JIRA_TOKEN_AUTH')

    # Something like JIRA_PROXIES='{"https":"http://proxy.example.com:4321"}'
    # See https://requests.readthedocs.io/en/latest/user/advanced/#proxies
    proxies = load(contents=environ.get('JIRA_PROXIES', '{}'))

    return KwfJiraClient(host, token_auth=token_auth, validate=True, timeout=300, proxies=proxies)


def get_jira_field(issue, field_name, default=None):
    """Fetch a single field value out of the the jira issue's fields."""
    if issue is None:
        return default

    field_value = get_nested_key(issue.fields, f'{field_name}', default, lookup_attrs=True)
    return field_value


def filter_kwf_issues(
    issues: typing.Iterable[Issue],
    valid_components: tuple[str, ...] = KWF_SUPPORTED_ISSUE_COMPONENTS,
) -> list[Issue]:
    """Return the list of Issues which are a valid issuetype."""
    invalid_issues = []
    for issue in issues:
        if JIType.from_str(get_jira_field(issue, 'issuetype/name')) == JIType.UNKNOWN or \
           not any(comp.name.split(" / ")[0].strip() in valid_components for comp in
                   get_jira_field(issue, 'components', default=[])):
            invalid_issues.append(issue)
    if invalid_issues:
        LOGGER.info('Filtered out unsupported issues: %s', invalid_issues)
    return [issue for issue in issues if issue not in invalid_issues]


def _getissues(
    jira: JIRA,
    issues: typing.Optional[typing.Iterable[str]] = None,
    cves: typing.Optional[typing.Iterable[str]] = None,
    filter_kwf: bool = False
) -> list[Issue]:
    """Return jira Issues matching the given list of issue keys or CVE labels."""
    if not issues and not cves:
        raise ValueError('Search lists are empty.')

    jql_statements: list[str] = []

    if cves:
        jql_statements.append(f"labels in ({', '.join(cves)})")
        jql_statements.append(' OR '.join(f'"CVE ID" ~ {cve}' for cve in cves))

    if issues:
        jql_statements.append(' OR '.join(f'key={issue}' for issue in issues))

    if filter_kwf:
        filter_str = \
            ('AND component in componentMatch("^(kernel|kernel-rt|kernel-automotive)($| / .*$)")'
             f" AND issuetype in ({', '.join(JIType.supported())})")
    else:
        filter_str = ''

    jql_str = f"project = {JPFX.rstrip('-')} {filter_str} AND "
    jql_str += f"({' OR '.join(jql_statements)})"
    LOGGER.info('Fetching Jira data for issues: %s, cves: %s, JQL: %s', issues, cves, jql_str)
    # This will catch unknown issues but not unknown CVEs.
    try:
        issues = jira.search_issues(jql_str, fields=JiraField.all())
    except JIRAError:
        LOGGER.warning("User specified invalid jira issue(s): %s", issues)
        issues = []
    return issues


def parse_search_list(search_list: typing.Iterable[str]) -> tuple[set, set]:
    """Split the given search_list items into a tuple of jira Issues and CVE names."""
    issues: set[str] = set()
    cves: set[str] = set()
    other = []
    for item in search_list:
        if isinstance(item, str):
            if item.startswith('CVE-'):
                cves.add(item)
            else:
                issues.add(item)
        else:
            other.append(item)
    if other:
        raise ValueError(f'Unknown search term: {other}')
    return issues, cves


def sanitize_str_for_jira(text: str) -> str:
    """Sanitize text to prevent permission denied errors from WAF."""
    text = text.replace('()', '')
    text = text.replace('/usr/bin/', 'usr bin ')
    text = text.replace('/usr/local/bin/', 'usr local bin ')
    text = text.replace('/sbin/', 'sbin ')
    text = text.replace('/bin/', 'bin ')
    return text


def update_issue_field(
    issue: Issue,
    jfield: JiraField,
    value: typing.Union[str, dict, User, list, None],
) -> None:
    """Update a given issue's given field with the given value."""
    if jfield in (JiraField.Internal_Target_Milestone,
                  JiraField.Dev_Target_Milestone,
                  JiraField.Preliminary_Testing,
                  JiraField.Release_Blocker):
        LOGGER.info("Setting issue %s's field %s to %s", issue.key, jfield.name, value)
        if is_production_or_staging():
            if value is None:
                issue.update(fields={jfield: None})
            else:
                issue.update(fields={jfield: {'value': str(value)}})
        return

    if jfield in (JiraField.Testable_Builds):
        value = sanitize_str_for_jira(value)
        LOGGER.info("Setting issue %s's field %s to %s", issue.key, jfield.name, f'{value[:32]}...')
        if is_production_or_staging():
            issue.update(fields={jfield: value})
        return

    if jfield in (JiraField.assignee):
        LOGGER.info("Setting issue %s's field %s to %s", issue.key, jfield.name, value)
        if is_production_or_staging():
            issue.update(assignee={'name': value})
        return

    if jfield in (JiraField.fixVersions):
        LOGGER.info("Setting issue %s's field %s to %s", issue.key, jfield.name, value)
        if is_production_or_staging():
            issue.update(fields={jfield: [{'name': value}]})
        return

    if jfield in (JiraField.Release_Milestone):
        LOGGER.info("Setting issue %s's field %s to %s", issue.key, jfield.name, value)
        # These checkbox custom fields are odd ducks, which require you to pass in the list of
        # checkbox IDs you want checked, and any not passed in will be unchecked. For example,
        # pass in [{'id': '12345'}, {'id': '12346'}] to set those two boxes and unset all others.
        if is_production_or_staging():
            issue.update(update={jfield: [{'set': value}]})
        return

    LOGGER.warning("Attempt to use update_issue_field for an unknown field: %s", jfield.name)


@dataclass
class SyncField:
    """Simple class to hold a field name and content."""

    # Field name in Jira API
    name: str = field(default='', init=False)
    # Data present in the field
    data: int | str = field(init=False)

    def __init__(self, name):
        """Initialize the field name."""
        self.name = name


@dataclass
class SyncFields:
    """Simple class to hold the fields to sync to linked issues."""

    def __init__(self, issue):
        """Initialize the SyncField names."""
        # Internal Target Milestone (integer value)
        self.itm = SyncField(name=JiraField.Internal_Target_Milestone)
        self.itm.data = int(get_jira_field(issue, f'{self.itm.name}/value', default=0))
        # Dev Target Milestone (integer value)
        self.dtm = SyncField(name=JiraField.Dev_Target_Milestone)
        self.dtm.data = int(get_jira_field(issue, f'{self.dtm.name}/value', default=0))
        # Testable Builds (free-form text field)
        self.tbuilds = SyncField(name=JiraField.Testable_Builds)
        self.tbuilds.data = get_jira_field(issue, self.tbuilds.name, default='')
        # Assignee
        self.assignee = SyncField(name=JiraField.assignee)
        self.assignee.data = get_jira_field(issue, self.assignee.name, default=None)
        # Release Blocker/Exception
        self.bne = SyncField(name=JiraField.Release_Blocker)
        self.bne.data = get_jira_field(issue, f'{self.bne.name}/value', default=None)


def _sync_linked_issue_fields(
    from_i: Issue,
    to_i: Issue,
    from_fields: SyncFields,
) -> None:
    """Sync the sync_fields from one issue to a linked issue."""
    to_fields = SyncFields(to_i)

    # Sync Internal Target Milestone
    if from_fields.itm.data and to_fields.itm.data < from_fields.itm.data:
        update_issue_field(to_i, to_fields.itm.name, from_fields.itm.data)
    else:
        LOGGER.debug("ITM value %s for %s and %s already match",
                     from_fields.itm.data, from_i.key, to_i.key)

    # Sync Dev Target Milestone
    if from_fields.dtm.data and to_fields.dtm.data < from_fields.dtm.data:
        update_issue_field(to_i, to_fields.dtm.name, from_fields.dtm.data)
    else:
        LOGGER.debug("DTM value %s for %s and %s already match",
                     from_fields.dtm.data, from_i.key, to_i.key)

    # Sync Testable Builds
    if from_fields.tbuilds.data and not to_fields.tbuilds.data:
        update_issue_field(to_i, to_fields.tbuilds.name, from_fields.tbuilds.data)
    else:
        LOGGER.debug("Testable Builds already populated for %s", to_i.key)

    # Sync Assignee
    if from_fields.assignee.data and from_fields.assignee.data != to_fields.assignee.data:
        update_issue_field(to_i, to_fields.assignee.name, from_fields.assignee.data.name)
    else:
        LOGGER.debug("Assignee field already matches")

    # Sync Blocker/Exception
    if from_fields.bne.data and from_fields.bne.data != to_fields.bne.data:
        update_issue_field(to_i, to_fields.bne.name, from_fields.bne.data)
    else:
        LOGGER.debug("Blocker/Exception field already matches")


def sync_cve_issue_fields(
    rhissues: typing.Iterable['RHIssue'],
    linked_rhissues: typing.Iterable['RHIssue'],
) -> None:
    """Make sure linked jira issues have necessary fields filled in."""
    for issue in rhissues:
        # Don't run if there are no CVEs in the main issue or if it is Closed
        if not issue.ji_cves or issue.ji_status is JIStatus.CLOSED:
            continue

        issue_fields = SyncFields(issue.ji)

        for linked_rhi in linked_rhissues:
            if linked_rhi.ji_type == JIType.TASK:
                LOGGER.debug("Tasks (%s) are handled differently", linked_rhi.id)
                continue
            if issue.ji_cves != linked_rhi.ji_cves:
                LOGGER.debug("Issue %s and %s CVE lists do not match, ignoring",
                             issue.id, linked_rhi.id)
                continue
            _sync_linked_issue_fields(issue.ji, linked_rhi.ji, issue_fields)


def fetch_issues(
    search_list: typing.Iterable[str],
    jira: typing.Optional[JIRA] = None,
    filter_kwf: bool = False,
) -> list[Issue]:
    # pylint: disable=dangerous-default-value
    """Return a list of issue objects queried from JIRA."""
    if not search_list:
        raise ValueError('Search list is empty.')
    issues, cves = parse_search_list(search_list)
    if not jira:
        jira = connect_jira()
    return _getissues(jira, issues=issues, cves=cves, filter_kwf=filter_kwf)


def issues_with_lower_status(issue_list, status, min_status=JIStatus.NEW):
    """Return the list of issues that have a status lower than the input status."""
    return [issue for issue in set(issue_list)
            if min_status <= JIStatus.from_str(issue.fields.status) < status]


def reset_preliminary_testing(jira: JIRA, issue_list: list[Issue]) -> None:
    """Clear Preliminary Testing: values in given issues and leave a comment about it."""
    task_list = []
    for issue in issue_list:
        if JIType.from_str(issue.fields.issuetype.name) == JIType.TASK:
            task_list.append(issue)
            continue
        if not getattr(issue.fields, JiraField.Preliminary_Testing):
            continue
        update_issue_field(issue, JiraField.Preliminary_Testing, None)
        comment = "Clearing Preliminary Testing due to new code in Merge Request."
        rh_only_comment = {'type': 'group', 'value': 'Red Hat Employee'}
        if is_production_or_staging():
            jira.add_comment(issue.key, comment, visibility=rh_only_comment)

    # kernel variant testing Task issues get moved back to New
    update_issue_status(task_list, JIStatus.NEW)


def request_preliminary_testing(issue_list: list[Issue]) -> None:
    """Set Preliminary Testing: Requested in a given issue, if not Pass/Requested already."""
    task_list = []
    for issue in issue_list:
        if JIType.from_str(issue.fields.issuetype.name) == JIType.TASK:
            task_list.append(issue)
            continue
        prelim_testing = getattr(issue.fields, JiraField.Preliminary_Testing)
        if prelim_testing and prelim_testing.value in ('Pass', 'Requested'):
            LOGGER.debug("Jira Issue %s PT already set to %s", issue.key, prelim_testing.value)
            continue
        if prelim_testing and prelim_testing.value == 'Fail':
            LOGGER.info("Jira Issue %s Failed QE Preliminary Testing", issue.key)
            continue
        update_issue_field(issue, JiraField.Preliminary_Testing, 'Requested')

    # kernel variant testing Task issues get moved to In Progress
    update_issue_status(task_list, JIStatus.IN_PROGRESS)


def issues_to_move_to_in_progress(jira_issues: typing.Iterable[Issue]) -> set[Issue]:
    """Return the set of issues from the input jira Issues that need to be moved to In Progress."""
    ji_to_update = issues_with_lower_status(jira_issues, JIStatus.IN_PROGRESS)

    update_set = set(ji_to_update)

    for issue in ji_to_update:
        issue_type = JIType.from_str(issue.fields.issuetype.name)

        # 'Bug' issue type cannot be moved beyond PLANNING status without having Severity set.
        if issue_type is JIType.BUG:
            severity = getattr(issue.fields, JiraField.Severity)
            if not severity or not IssueSeverity.from_str(severity.value):
                update_set.remove(issue)
                continue

        if not (fix_version := issue.fields.fixVersions):
            update_set.remove(issue)
            continue

        if issue_type is not JIType.VULNERABILITY:
            is_zstream = FixVersion(fix_version[0].name).zstream
            itm = getattr(issue.fields, JiraField.Internal_Target_Milestone)
            dtm = getattr(issue.fields, JiraField.Dev_Target_Milestone)

            if not is_zstream and not (itm and dtm):
                update_set.remove(issue)
                continue

        if not issue.fields.assignee or issue.fields.assignee.emailAddress in JIRA_BOT_ASSIGNEES:
            update_set.remove(issue)

    return update_set


def add_gitlab_link_in_issues(session, issues, this_mr):
    """Add GitLab MR link to Issue Links field in JIRA Issues."""
    # pylint: disable=too-many-locals
    base_url = f'{GITFORGE}/{this_mr.namespace}'
    mr_title = f'{this_mr.rh_project.name}!{this_mr.iid}'
    mr_link = f'{base_url}/-/merge_requests/{this_mr.iid}'
    branch_link = f'{base_url}/-/tree/{this_mr.rh_branch.name}'
    title = f'Merge Request: {this_mr.title}'
    icon = (f'{GITFORGE}/assets/favicon-'
            '72a2cad5025aa931d6ea56c3201d1f18e68a8cd39788c7c80d5b2b82aa5143ef.png')
    gitlab_link = {'url': mr_link, 'title': title,
                   'icon': {'url16x16': icon, 'title': 'GitLab Merge Request'}}
    comment = (
        f"Merge request {session.comment.jira_link(mr_title, mr_link)} ({this_mr.title}) "
        f"targetting {session.comment.jira_link(this_mr.rh_branch.name, branch_link)} "
        "linked to this issue.\n"
    ) + session.comment.jira_footer()

    jira = connect_jira()
    for issue in filter_kwf_issues(issues):
        current_links = jira.remote_links(issue)
        link_exists = False
        for link in current_links:
            link_detail = jira.remote_link(issue=issue, id=link.id)
            if link_detail.object.url == mr_link:
                link_exists = True
                break
        if link_exists:
            LOGGER.info("MR %s already linked in %s", this_mr.iid, issue.key)
        else:
            LOGGER.info("Linking [%s](%s) to issue %s", title, mr_link, issue.key)
            if is_production_or_staging():
                jira.add_simple_link(issue=issue, object=gitlab_link)
                jira.add_comment(issue.key, comment)


def remove_gitlab_link_comment_in_issue(jira, issue, mr_url):
    """Remove the comment we left in the issue pointing to the MR."""
    for comment in jira.comments(issue):
        c_detail = jira.comment(issue.id, comment.id)
        if mr_url in c_detail.body and c_detail.author.name in JIRA_BOT_ACCOUNTS:
            LOGGER.info("Removing %s comment pointing to %s", issue.key, mr_url)
            c_detail.delete()


def remove_gitlab_link_in_issues(mr_id, namespace, issue_list):
    """Remove GitLab MR link from Issue Links field in JIRA Issues."""
    if not issue_list:
        return

    mr_link = f'{GITFORGE}/{namespace}/-/merge_requests/{mr_id}'
    jira = connect_jira()
    link = {}
    issues = _getissues(jira, issues=issue_list)
    for issue in filter_kwf_issues(issues):
        current_links = jira.remote_links(issue)
        link_exists = False
        for link in current_links:
            link_detail = jira.remote_link(issue=issue, id=link.id)
            if link_detail.object.url == mr_link:
                link_exists = True
                break
        if link_exists:
            LOGGER.info("MR %s linked in %s, removing it", mr_id, issue.key)
            jira.delete_remote_link(issue=issue, internal_id=link.id)
            remove_gitlab_link_comment_in_issue(jira, issue, mr_link)


def update_testable_builds(issues, text, pipeline_urls):
    """Fill in Testable Builds field for the given JIRA Issues."""
    LOGGER.info("Filling in Testable Builds for issues: %s", issues)
    LOGGER.debug("Text:\n%s\n", text)
    for issue in filter_kwf_issues(issues):
        if JIType.from_str(issue.fields.issuetype.name) == JIType.TASK:
            continue
        testable_builds = getattr(issue.fields, JiraField.Testable_Builds)
        if testable_builds and all(url in testable_builds for url in pipeline_urls):
            LOGGER.info("All downstream pipelines found in %s, not updating field.", issue.key)
            continue
        update_issue_field(issue, JiraField.Testable_Builds, text)


def transition_issue(
    jira: JIRA,
    rhissue: typing.Union[str, int, Issue],
    jistatus: JIStatus,
    extra_transition_kwargs: typing.Optional[dict] = None,
) -> None:
    """Transition the issue to the given status."""
    if not is_production_or_staging():
        return
    jira.transition_issue(rhissue, jistatus.status_str, **extra_transition_kwargs)


def update_issue_status(issue_list, new_status, min_status=JIStatus.NEW):
    """Change the issue status to new_status if it is currently lower, otherwise do nothing."""
    issue_list = filter_kwf_issues(issue_list)
    # Do nothing if the current status is lower than min_status.
    # Returns the list of issue objects which have had their status changed.
    if not issue_list:
        LOGGER.info('No issues to update status for.')
        return []
    if new_status not in (JIStatus.PLANNING, JIStatus.IN_PROGRESS):
        LOGGER.warning("Unsupported transition status: %s", new_status.status_str)
        return []
    if not (ji_to_update := issues_with_lower_status(issue_list, new_status, min_status)):
        LOGGER.info('All issues have status of %s or higher.', new_status.status_str)
        return []
    ji_keys = [ji.key for ji in ji_to_update]
    LOGGER.info('Updating status to %s for these issues: %s', new_status.status_str, ji_keys)
    if not is_production_or_staging():
        for issue in ji_to_update:
            issue.status = new_status.name
        return ji_to_update
    status_updated = []
    transition_comment = f"GitLab kernel MR bot updated status to {new_status.status_str}"
    jira = connect_jira()
    for issue in ji_to_update:
        transition_issue(jira, issue, new_status, {'comment': transition_comment})
        issue.fields.status = new_status.status_str
        status_updated.append(issue)
    return status_updated


def move_issue_states_forward(issue_list):
    """Move the given JIRA issues states to Planning or In Progress."""
    to_in_progress = issues_to_move_to_in_progress(issue_list)
    LOGGER.debug("Issues to move to In Progress: %s", to_in_progress)
    update_issue_status(to_in_progress, JIStatus.IN_PROGRESS)
    to_planning = {issue for issue in issue_list if issue not in to_in_progress and
                   JIType.from_str(issue.fields.issuetype.name) != JIType.TASK}
    LOGGER.debug("Issues to move to Planning: %s", list(to_planning))
    update_issue_status(to_planning, JIStatus.PLANNING)
