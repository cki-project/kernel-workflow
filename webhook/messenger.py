"""Definitions for inter-webhook messaging."""
from dataclasses import asdict as dataclasses_asdict
from dataclasses import dataclass
from dataclasses import field
import pkgutil
import typing

from cki_lib import misc
from cki_lib import yaml

from webhook import defs
from webhook.temp_utils import maybe_yaml_validate

MESSENGER_SCHEMA = yaml.load(contents=pkgutil.get_data(__name__, 'schema_kwf_messenger.yaml'))


@dataclass(frozen=True)
class KwfMessage:
    """Format of an interwebhook message of typ defs.UMB_BRIDGE_MESSAGE_TYPE."""

    class DictFactory(dict):
        """A dict factory for mangling dataclass attribute names."""

        def __init__(self, data: typing.Any) -> None:
            """Translate message_type into message-type."""
            tuples = []
            for key, value in data:
                if key == 'message_type':
                    key = 'message-type'
                tuples.append((key, value))
            super().__init__(tuples)

    @dataclass(frozen=True)
    class KwfHeaders:
        """Headers of a KwfMessage."""

        event_target_webhook: str
        event_source_webhook: str
        event_source_id: str = ''
        event_source_environment: str = field(default_factory=misc.deployment_environment)
        message_type: str = field(init=False, default=defs.UMB_BRIDGE_MESSAGE_TYPE)

    @dataclass(frozen=True)
    class KwfBody:
        """Body of a KwfMessage."""

        gitlab_url: str = ''
        jira_key: str = ''

    queue_name: str
    exchange: str
    headers: KwfHeaders
    data: KwfBody

    def __post_init__(self) -> typing.Self:
        """Validate ourselves."""
        self.validate()

    @classmethod
    def create(
        cls,
        *,
        queue_name: str,
        exchange: str,
        target_hook: str,
        source_hook: str,
        source_id: str = '',
        gitlab_url: typing.Optional[str] = None,
        jira_key: typing.Optional[str] = None
    ) -> typing.Self:
        # pylint: disable=too-many-arguments
        """Construct a KwfMessage with the given parameters."""
        if not gitlab_url and not jira_key:
            raise RuntimeError('At least one of gitlab_url or jira_key must be set.')

        headers = cls.KwfHeaders(
            event_target_webhook=target_hook,
            event_source_webhook=source_hook,
            event_source_id=source_id
        )
        data = cls.KwfBody(gitlab_url=gitlab_url or '', jira_key=jira_key or '')
        return cls(queue_name, exchange, headers, data)

    def asdict(self) -> dict:
        """Convert the given KwfMessage into a dict."""
        # This is used to replace message_type with message-type :/ .
        return dataclasses_asdict(self, dict_factory=self.DictFactory)

    def validate(self) -> None:
        """Validate this message against the schema, raising an exception if it fails."""
        maybe_yaml_validate(self.asdict(), MESSENGER_SCHEMA)
