"""Get SAST status and results for a given MR."""

from dataclasses import dataclass
from enum import Enum
from re import match
from re import sub
from sys import argv
import typing
from urllib.parse import quote_plus
from urllib.parse import urlencode

from cki_lib.logger import get_logger
from cki_lib.session import get_session

from webhook import common
from webhook.base_mr import BaseMR
from webhook.base_mr_mixins import CommitsMixin
from webhook.session import SessionRunner
from webhook.session_events import GitlabMREvent
from webhook.session_events import GitlabNoteEvent

if typing.TYPE_CHECKING:
    from gitlab.v4.objects.merge_requests import ProjectMergeRequest
    from gitlab.v4.objects.projects import Project

LOGGER = get_logger('cki.webhook.sast')
SAST_BASE_URL_ENV_VAR = "KERNEL_SAST_AUTOMATION_SERVER_REST_API"
SAST_GL_PROJECTS = "GL_PROJECTS"
REPORT_HEADER = '**Kernel SAST Scan Report**'
MAX_NUMBER_FINDINGS_TO_DISPLAY = 30


class UrlSubitem(Enum):
    """Simple enum class to control of REST API calls to our backend."""

    STATUS = "status"
    RESULTS = "results"


@dataclass(repr=False)
class MR(CommitsMixin, BaseMR):
    """Represent the MR."""


def _check_update_comment(
    session: SessionRunner,
    comment_text: str | None,
    gl_mr: 'ProjectMergeRequest',
    gl_project: 'Project',
    new_label: str
) -> None:
    """Update comment only current is different than the new one."""
    if not comment_text or comment_text.strip() == "":
        comment_text = "<br/><br/>No findings available"
    report_text = REPORT_HEADER + f' ~"SAST::{new_label}"\n\n' + comment_text

    for gl_note in gl_mr.notes.list(iterator=True):
        # Skip system notes.
        if gl_note.system:
            continue

        if gl_note.author['username'] != gl_project.manager.gitlab.user.username:
            continue

        # this must be "startswith", because in session we have:
        # body = text + self.comment.gitlab_footer('updated')
        # so our text is only the beginning of the full comment
        if gl_note.body.startswith(report_text):
            LOGGER.info("Matches note #%s, nothing to do:\n%s", gl_note.id, report_text)
            return

    session.update_webhook_comment(gl_mr, report_text,
                                   bot_name=gl_project.manager.gitlab.user.username,
                                   identifier=REPORT_HEADER)


def _update_mr(
    session: SessionRunner,
    gl_project: 'Project',
    mr_id: int,
    new_label: str,
    comment_text: str | None
) -> None:
    """Update the MR with a note of the results and possibly set the label scope."""
    gl_mr = gl_project.mergerequests.get(mr_id)

    current_mr_labels: list[str] = gl_mr.labels or []

    current_label = next((lbl.rsplit('::', 1)[-1] for lbl in current_mr_labels if
                          lbl.startswith('SAST::')), None)

    LOGGER.debug("current label: '%s', new label: '%s'", current_label, new_label)

    if current_label != new_label:
        LOGGER.debug("SAST webhook update_mr: setting new label: SAST::%s", new_label)
        common.add_label_to_merge_request(gl_project, mr_id, [f"SAST::{new_label}"])

    _check_update_comment(session, comment_text, gl_mr, gl_project, new_label)


def get_sast_item(url, subitem: UrlSubitem, mr_id):
    """Get the SAST scan information from ProdSec's servers."""
    if not match(r'^\d+$', f"{mr_id}"):
        LOGGER.error("The supplied MR id is not entirely numeric: %s", f"{mr_id}")
        return None
    if not match(r'https?:\/\/(?:w{1,3}\.)?[^\s.]+(?:\.[a-z]+)*(?::\d+)?((?:\/\w+)|(?:-\w+))*\/?' +
                 r'(?![^<]*(?:<\/\w+>|\/?>))', f"{url}"):
        LOGGER.error("The supplied base URL is not valid: %s", f"{url}")
        return None
    base_url = f"{url}/{subitem.value}/mr/{mr_id}"
    LOGGER.debug("SAST webhook get_sast_%s: getting from URL: %s", subitem.value, base_url)
    headers = {"accept": "application/json"}
    req = None
    try:
        s = get_session("kwf/webhook.sast", timeout=10)
        s.headers.update(headers)
        req = s.get(base_url)
    except Exception as e:
        LOGGER.error("SAST webhook get_sast_%s: exception during get: %s: %s", subitem.value,
                     base_url, e)
        return None
    if req is None:
        LOGGER.error("SAST webhook get_sast_%s: got None response: %s", subitem.value, base_url)
        return None
    LOGGER.debug("SAST webhook get_sast_%s: got response code %d", subitem.value, req.status_code)
    try:
        req.raise_for_status()
        LOGGER.debug("SAST webhook get_sast_%s: returning JSON", subitem.value)
        return req.json()
    except Exception as e:
        LOGGER.error("Unable to get Kernel SAST %s for MR (%d): %s", subitem.value, mr_id, e)
    LOGGER.debug("SAST webhook get_sast_%s: returning None", subitem.value)
    return None


def _findings_to_gitlab_html(findings_list: list[str], sast_mr: MR) -> str:
    """Convert the JSON structure with SAST findings into Gitlab HTML comment.

    This is nice and easy for the maintainers to view, analyze and understand.
    """
    final_str = ""

    for itm in findings_list[:MAX_NUMBER_FINDINGS_TO_DISPLAY]:
        try:
            itm = itm.replace("kernel-999/", "").replace("kernel-999", "")
            subi = itm.split("\n", 2)
            subi0 = sub(r'CWE-(\d+)', r'<a href="https://cwe.mitre.org/data/definitions/\1' +
                        r'.html" target="_blank">CWE-\1</a>', subi[0])
            fpth = subi[1].split(":", 2)
            encoded_jira_fp = urlencode({"summary": "False positive finding in kernel MR",
                                         "description": "Please, add the following OSH error " +
                                         f"from [{sast_mr.rh_project.name} MR {sast_mr.iid}|" +
                                         f"{sast_mr.url}] to the list of known false " +
                                         f"positives:\n\n{subi[0]} {fpth[0]}:{fpth[1]}\n\n"
                                         "Reason: {TYPE_THE_REASON_FOR_FALSE_POSITIVE}"},
                                        quote_via=quote_plus)
            fp_url = "https://issues.redhat.com/secure/CreateIssueDetails!init.jspa?" + \
                     "pid=12331126&issuetype=3&labels=RHEL&labels=SecArch&security=11697" + \
                     "&priority=10200&" + encoded_jira_fp
            encoded_jira_tp = urlencode({"summary": f"Kernel {sast_mr.rh_project.name} MR " +
                                         f"{sast_mr.iid}: {subi[0]} {fpth[0]}:{fpth[1]}"},
                                        quote_via=quote_plus)
            tp_url = "https://issues.redhat.com/secure/CreateIssueDetails!init.jspa?" + \
                     "pid=12332745&issuetype=1&labels=SAST&labels=Kernel&security=11694" + \
                     "&priority=10200&" + encoded_jira_tp
            final_str = final_str + f"""\n<details>
    <summary>
        {subi0} {fpth[0]}:{fpth[1]}&nbsp;&nbsp;&nbsp;<a href="{fp_url}">&#10007;</a>
        &nbsp;&nbsp;&nbsp;<a href="{tp_url}">&#10003;</a>
    </summary>
    <pre>
{subi[0]}
{subi[1]}
{subi[2]}
    </pre>
</details>"""
        except Exception as e:
            LOGGER.error("Error parsing finding: %s", e)
    if final_str != "":
        final_str = "The following list contains findings from performing a Static Analysis" + \
                    " Security Testing (SAST) on this MR. For more details on what needs to " + \
                    "be done, please review [our FAQ]" + \
                    "(https://docs.google.com/document/d/" + \
                    "1giNDNRzDh2C8EO5zdNaadACYGlWzhRgUkpMJujNFCZc). " + \
                    "This activity is currently non-gating, but the RHEL " + \
                    "Security Architects encourage you to perform it prior to " + \
                    "merging.\n\n" + final_str
    LOGGER.debug(final_str)
    return final_str


def process_mr(session: SessionRunner, sast_mr: MR) -> bool:
    """Process given MR object."""
    sast_status_label = "pending"
    report_text = None
    sast_status = get_sast_item(session.args.sast_rest_api_url, UrlSubitem.STATUS,
                                sast_mr.iid)
    LOGGER.debug("SAST webhook process_mr: sast_status = %s", sast_status)
    if sast_status is None or not isinstance(sast_status, dict) or \
            "status" not in sast_status or "amount" not in sast_status:
        LOGGER.error("Kernel SAST status returned for MR %d is wrong: %s",
                     sast_mr.iid, sast_status)
        _update_mr(session, sast_mr.gl_project, sast_mr.iid, sast_status_label, report_text)
        return False
    sast_status_label = sast_status['status']
    LOGGER.debug("SAST webhook process_mr: new label = %s", sast_status_label)
    if sast_status["status"] == "finished":
        LOGGER.debug("SAST webhook process_mr: we have %d finding(s)",
                     sast_status['amount'])
        if sast_status["amount"] > 0:
            sast_findings = get_sast_item(session.args.sast_rest_api_url, UrlSubitem.RESULTS,
                                          sast_mr.iid)
            LOGGER.debug("SAST webhook process_mr: sast_findings = %s", sast_findings)
            if not isinstance(sast_findings, dict) or \
               not isinstance(sast_findings.get("findings"), list):
                LOGGER.error("Kernel SAST findings returned for MR %d are wrong: %s",
                             sast_mr.iid, sast_findings)
                _update_mr(session, sast_mr.gl_project, sast_mr.iid, "failed", report_text)
                return False

            findings_list: list[str] = sast_findings["findings"]
            if len(findings_list) == 1 and findings_list[0] == "\n":
                sast_status['amount'] = 0
            report_text = _findings_to_gitlab_html(findings_list, sast_mr)
        if sast_status['amount'] > 0:
            sast_status_label = f"{sast_status_label}_w_findings"
        else:
            sast_status_label = f"{sast_status_label}_no_findings"
    _update_mr(session, sast_mr.gl_project, sast_mr.iid, sast_status_label, report_text)
    return True


def process_gl_event(
    _: dict,
    session: 'SessionRunner',
    event: GitlabMREvent | GitlabNoteEvent,
    **__: typing.Any
) -> None:
    """Process a gitlab event."""
    LOGGER.info('Processing %s event for %s', event.kind.name, event.mr_url)
    sast_mr = MR.new(session, event.mr_url)
    if process_mr(session, sast_mr):
        LOGGER.info("Finished event processing for %s, %s", event.kind.name, event.mr_url)


def nudger(session: SessionRunner) -> None:
    """Find all open MRs, nudge the webhook."""
    for gl_proj_path in session.args.gl_projects:
        LOGGER.debug("Nudger: gl_proj_path == %s", gl_proj_path)
        rh_project = session.rh_projects.get_project_by_namespace(gl_proj_path)
        if rh_project is None:
            raise ValueError(f"{gl_proj_path} is not a project from rh_metadata.yaml")
        gl_project = session.get_gl_project(gl_proj_path)
        gl_mrs = gl_project.mergerequests.list(all=True, state="opened")
        LOGGER.debug("Found gl_mrs == %r", gl_mrs)
        for gl_mr in gl_mrs:
            sast_mr = MR.new(session, gl_mr.web_url)
            process_mr(session, sast_mr)
        LOGGER.info("Finished on project: %s", gl_proj_path)
    LOGGER.info("Nudger finished")


HANDLERS = {
    GitlabMREvent: process_gl_event,
    GitlabNoteEvent: process_gl_event,
}


def main(args):
    """Run main loop."""
    common.init_sentry()
    parser = common.get_arg_parser('SAST')
    parser.add_argument('--sast-rest-api-url',
                        **common.get_argparse_environ_opts(SAST_BASE_URL_ENV_VAR),
                        help='The URL to the SAST REST API automation server')
    gl_proj = common.get_argparse_environ_opts(SAST_GL_PROJECTS, is_list=True)
    if "required" in gl_proj:
        gl_proj["required"] = False
        gl_proj["default"] = None
    parser.add_argument('--gl-projects', **gl_proj,
                        help='The path to the Gitlab projects to nudge, e.g. ' +
                             '"redhat/rhel/src/kernel/rhel-9"')
    args = parser.parse_args(args)
    session = SessionRunner.new('sast', args=args, handlers=HANDLERS)
    LOGGER.debug("args.gl_projects type %s == %r", type(args.gl_projects), args.gl_projects)
    if args.gl_projects is not None and isinstance(args.gl_projects, list):
        nudger(session)
        return
    session.run()


if __name__ == "__main__":
    main(argv[1:])
