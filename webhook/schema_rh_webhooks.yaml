---
$defs:
  webhook: &webhook
    title: webhook
    description: Properties of a kernel-workflow webhook.
    type: object
    properties:
      name:
        description: Name of the webhook.
        type: string
      enabled_by_default:
        description: Whether the webhook is assumed to be enabled for any given project.
        type: boolean
        default: false
      required:
        description: Whether the webhook should be considered for MR readiness.
        type: boolean
        default: true
      required_label_prefixes:
        description: List of scoped label prefixes controlled by this webhook that must be present on an MR. These must have ::OK scope for readyForMerge if the 'required' property is enabled.
        type: array
        items: {
          type: string
        }
      extra_labels_regex:
        description: Regex string matching other labels controlled by this webhook
        type: string
        default: ""
      required_for_qa_scope:
        description: Scope needed on all required_label_prefixes to reach readyForQA status.
        type: string
        enum: ["InProgress", "NeedsTesting", "OK"]
      match_to_projects:
        description: Whether the Event processor will try to match events to a known Project for this webhook.
        type: boolean
        default: true
      request_evaluation_triggers:
        description: List of substrings that can be used to trigger this webhook via an MR comment in the format request-{substring}-evaluation.
        type: array
        items: {
          type: string
        }
      run_on_changed_approvals:
        description: Whether this webhook runs when an event indicates the MR has been approved or unapproved or an approval rule has been modified.
        type: boolean
        default: false
      run_on_drafts:
        description: Whether this webhook runs on events from draft MRs.
        type: boolean
        default: true
      run_on_blocking_mismatch:
        description: Whether this webhook runs if the Blocked label presence does not match the event's discussions blocked property.
        type: boolean
        default: false
      run_on_changed_commits:
        description: Whether this webhook runs when an event indicates the MR commits changed.
        type: boolean
        default: true
      run_on_changed_description:
        description: Whether this webhook runs when an event indicates the MR description changed.
        type: boolean
        default: true
      run_on_changed_to_draft:
        description: Whether this webhook runs when an event shows the MR changing to draft state.
        type: boolean
        default: false
      run_on_changed_to_ready:
        description: Whether this webhook runs when an event shows the MR changing out of draft state.
        type: boolean
        default: true
      run_on_closed:
        description: Whether this webhook runs on events from closed or merged MRs.
        type: boolean
        default: false
      run_on_closing:
        description: Whether this webhook runs when an event shows the MR has closed or been merged.
        type: boolean
        default: false
      run_on_build_downstream:
        description: Whether this webhook runs on build (job) events from a downstream project.
        type: boolean
        default: false
      run_on_build_upstream:
        description: Whether this webhook runs on build (job) events from an upstream project.
        type: boolean
        default: false
      run_on_pipeline_downstream:
        description: Whether this webhook runs on pipeline events from a downstream project.
        type: boolean
        default: false
      run_on_pipeline_upstream:
        description: Whether this webhook runs on pipeline events from an upstream project.
        type: boolean
        default: false
      run_on_empty_mrs:
        description: Whether this webhook runs when the event MR has zero commits.
        type: boolean
        default: false
      run_on_oversize_mrs:
        description: Whether this webhook runs when the event MR has more than MAX_COMMITS_PER_MR.
        type: boolean
        default: false
      note_text_patterns:
        description: List of regex to match in new MR comments (notes) that will trigger this webhook.
        type: array
        items: {
          type: string
        }
      skip_session_filter_check:
        description: Whether the SessionRunner will skip the event's normal filter checks for this webhook.
        type: boolean
        default: false
      skip_session_trigger_check:
        description: Whether the SessionRunner will skip the event's normal trigger checks for this webhook.
        type: boolean
        default: false
      instance_manages_ready_labels:
        description: Whether instances of this webhook will handle events showing changes to the readyForQA & readyForMerge labels. Deprecated by manage_special_labels?
        type: boolean
        default: false
      manage_special_labels:
        description: Whether instances of this webhook will handle changes to readyForQA, readyForMerge, or Blocked labels. One and only one production instance must enable this.
        type: boolean
        default: false
      source_code:
        description: URL of this webhook's primary source code file.
        type: string
      faq_name:
        description: Title of the faq_url.
        type: string
      faq_url:
        description: URL of a helpful FAQ for this webhook.
        type: string
      documentation_url:
        description: URL of a README or some other documentation for this webhook.
        type: string
      slack_name:
        description: Name of a slack channel for support with this webhook.
        type: string
      slack_url:
        description: URL of the slack_name channel.
        type: string
      new_issue_url:
        description: URL to report issues with this webhook.
        type: string
      comment_header:
        description: Header string used in this webhook's report comment. This will be wrapped in **double asterisk** to make it bold.
        type: string
        default: ""
    additionalProperties: false


$schema: https://json-schema.org/draft/2020-12/schema
$id: https://gitlab.com/cki-project/kernel-workflow/-/raw/main/webhook/schema_rh_webhooks.yaml
title: Schema for kernel-workflow 'webhooks' metadata
description: Properties of the kernel-workflow webhooks.
type: object
properties:
  webhooks:
    description: Mapping of webhook objects with name as key.
    type: object
    properties:
      ack_nack: *webhook
      backporter: *webhook
      buglinker: *webhook
      ckihook: *webhook
      commit_compare: *webhook
      configshook: *webhook
      limited_ci: *webhook
      elnhook: *webhook
      fixes: *webhook
      jirahook: *webhook
      skeleton: *webhook
      mergehook: *webhook
      owners_validator: *webhook
      report_generator: *webhook
      sast: *webhook
      signoff: *webhook
      subsystems: *webhook
      sprinter: *webhook
      terminator: *webhook
      umb_bridge: *webhook
    additionalProperties: false
required: ["webhooks"]
additionalProperties: true
