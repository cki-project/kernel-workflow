"""A session object for webhooks."""
from dataclasses import dataclass
import functools
import json
import os
from pathlib import Path
import typing

from bugzilla import Bugzilla
from cki_lib import footer
from cki_lib.gitlab import get_instance
from cki_lib.logger import get_logger
from cki_lib.messagequeue import MessageQueue
from cki_lib.metrics import prometheus_init
from cki_lib.misc import deployment_environment
from cki_lib.misc import get_nested_key
from cki_lib.misc import is_production
from cki_lib.misc import is_production_or_staging
from cki_lib.misc import is_staging
from cki_lib.session import get_session

from .comments import CommentCache
from .common import add_label_to_merge_request as add_labels
from .common import get_owners_parser
from .common import parse_mr_url
from .common import remove_labels_from_merge_request as remove_labels
from .defs import BUGZILLA_HOST
from .defs import DEFAULT_MIRROR_LIST_PATH
from .defs import GITFORGE
from .defs import GitlabObjectKind
from .defs import MessageType
from .defs import READY_LABELS
from .graphql import GitlabGraph
from .kgit import load_repo_configs
from .labels import LabelsCache
from .libjira import connect_jira
from .rh_metadata import Projects
from .session_events import create_event
from .session_events import special_label_changes
from .users import UserCache

if typing.TYPE_CHECKING:
    from argparse import Namespace

    from cki_lib.owners import Parser
    from gitlab.client import Gitlab
    from gitlab.v4.objects.groups import Group
    from gitlab.v4.objects.projects import Project

    from .kgit import RepoConfigs
    from .rh_metadata import Webhook
    from .session_events import AnyEvent
    from .session_events import BaseEvent

LOGGER = get_logger('cki.webhook.session')

SESSION = get_session('cki.webhook.session')


@functools.cache
def get_bugzilla(host: str = BUGZILLA_HOST) -> Bugzilla:
    """Return a bugzilla connection."""
    bzcon = Bugzilla(
        url=host,
        api_key=os.environ.get('BUGZILLA_API_KEY'),
        force_rest=True,
        requests_session=SESSION
    )

    if not bzcon.logged_in:
        raise RuntimeError(f"Not authenticated to bugzilla host '{host}'")

    return bzcon


@functools.cache
def get_gl_group(namespace: str, gl_host: str = GITFORGE) -> 'Group':
    """Return a python-gitlab Group object for the given namespace & gl_host."""
    gl_instance = get_gl_instance(gl_host)
    return gl_instance.groups.get(namespace)


@functools.cache
def get_gl_project(namespace: str, gl_host: str = GITFORGE) -> 'Project':
    """Return a python-gitlab Project object for the given namespace & gl_host."""
    gl_instance = get_gl_instance(gl_host)
    return gl_instance.projects.get(namespace)


@functools.cache
def get_gl_instance(gl_host: str = GITFORGE) -> 'Gitlab':
    """Return an authenticated gitlab.client.Gitlab instance for the given host."""
    instance = get_instance(gl_host)
    instance.auth()
    if not getattr(instance, 'user', None):
        raise RuntimeError(f"Not authenticated to Gitlab host '{gl_host}'")
    return instance


@functools.cache
def get_graphql(gl_host: str = GITFORGE) -> GitlabGraph:
    """Return an authenticated webhook.gitlab.GitlabGraph instance for the given host."""
    graph = GitlabGraph(get_user=True, hostname=gl_host)
    if not graph.user:
        raise RuntimeError('Not authenticated to Gitlab.')
    return graph


@dataclass
class BaseSession:
    """An object to hold basic session data."""

    rh_projects: Projects

    @classmethod
    def new(cls, rh_projects: typing.Optional[Projects] = None) -> typing.Self:
        """Construct a new BaseSession instance."""
        new_instance = cls(rh_projects or Projects())
        LOGGER.info('Created %s', new_instance)
        return new_instance

    def __hash__(self) -> int:
        """Return our ID."""
        return id(self)

    def __repr__(self):
        """Talk about yourself."""
        environment = self.environment.upper() if self.is_production else self.environment
        return f"<{self.__class__.__name__}: env: {environment}>"

    @property
    def bugzilla(self) -> Bugzilla:
        """Return a bugzilla connection."""
        return get_bugzilla(os.environ.get('BUGZILLA_HOST', BUGZILLA_HOST))

    @functools.cached_property
    def gl_host(self) -> str:
        """Return the hostname used for Gitlab API connections."""
        return GITFORGE

    @property
    def gl_instance(self) -> 'Gitlab':
        """Return an authenticated Gitlab REST instance for the default host."""
        return self.get_gl_instance(self.gl_host)

    @property
    def graphql(self):
        """Return an authenticated GL graphql instance."""
        return self.get_graphql(self.gl_host)

    @functools.cached_property
    def jira(self):
        """Return an authenticated JIRA API instance."""
        return connect_jira()

    @functools.cached_property
    def gl_labels(self) -> LabelsCache:
        """Return a LabelsCache object."""
        return LabelsCache(session=self)

    @property
    def gl_user(self):
        """Return the current GL user the Session is authenticaed as."""
        return self.gl_instance.user

    @functools.cached_property
    def environment(self):
        """Return a string describing the environment."""
        return deployment_environment()

    @functools.cached_property
    def is_production_or_staging(self):
        """Return True if we are running in production or staging, otherwise False."""
        return is_production_or_staging()

    @functools.cached_property
    def is_production(self):
        """Return True if we are running in production, otherwise False."""
        return is_production()

    @functools.cached_property
    def is_staging(self):
        """Return True if we are running in staging, otherwise False."""
        return is_staging()

    @staticmethod
    def get_gl_group(namespace: str, gl_host: str = GITFORGE) -> 'Group':
        """Return a python-gitlab Group object for the given namespace & gl_host."""
        return get_gl_group(namespace, gl_host)

    @staticmethod
    def get_gl_instance(gl_host: str = GITFORGE) -> 'Gitlab':
        """Return an authenticated gitlab.client.Gitlab instance for the given host."""
        return get_gl_instance(gl_host)

    @staticmethod
    def get_gl_project(namespace: str, gl_host: str = GITFORGE) -> 'Project':
        """Return a python-gitlab Project object for the given namespace & gl_host."""
        return get_gl_project(namespace, gl_host)

    @staticmethod
    def get_graphql(gl_host: str = GITFORGE) -> GitlabGraph:
        """Return an authenticated webhook.gitlab.GitlabGraph instance for the given host."""
        return get_graphql(gl_host)

    # pylint: disable=method-cache-max-size-none
    @functools.cache
    def get_user_cache(self, namespace: str) -> UserCache:
        """Return a UserCache object for the given namespace."""
        return UserCache(self, namespace)

    @functools.cache
    def get_comment_cache(self, bot_usernames: tuple[str] | None = None) -> CommentCache:
        """Return a CommentCache object."""
        bot_usernames = list(bot_usernames) if bot_usernames else [self.graphql.username]
        return CommentCache(session=self, bot_usernames=bot_usernames)

    @property
    def owners(self) -> 'Parser':
        """Return an owners.Parser instance."""
        owners_yaml = get_nested_key(
            self, 'args/owners_yaml', lookup_attrs=True, default=os.environ.get('OWNERS_YAML')
        )
        if not owners_yaml:
            raise RuntimeError('--owners-yaml command line arg or OWNERS_YAML env must be set.')
        return get_owners_parser(owners_yaml)

    @functools.cached_property
    def repo_configs(self) -> 'RepoConfigs':
        """Return the dict of git repo data."""
        file_path = self.args.mirror_list_path if \
            hasattr(self, 'args') else os.environ.get('MIRROR_LIST_PATH', DEFAULT_MIRROR_LIST_PATH)
        extra_file_path = os.environ.get('MIRROR_LIST_PATH_EXTRA', '')
        return load_repo_configs(file_path, extra_file_path)


@dataclass(kw_only=True)
class SessionRunner(BaseSession):
    """A BaseSession with event handling."""

    webhook: 'Webhook'
    args: 'Namespace'
    handlers: dict['BaseEvent', typing.Callable]

    @classmethod
    def new(
        cls,
        webhook_name: str,
        args: 'Namespace',
        handlers: typing.Optional[dict] = None,
        rh_projects: typing.Optional[dict] = None,
    ) -> typing.Self:
        # pylint: disable=arguments-differ
        """Construct a new SessionRunner instance."""
        rh_projects = rh_projects or Projects()
        if not (webhook := rh_projects.webhooks.get(webhook_name)):
            raise RuntimeError(
                (f"webhook_name '{webhook_name}' not found in Projects data webhooks:"
                 f" {list(rh_projects.webhooks.keys())}")
            )
        new_instance = cls(
            rh_projects=rh_projects,
            webhook=webhook,
            args=args,
            handlers=handlers or {},
        )
        LOGGER.info('Created %s', new_instance)
        return new_instance

    def __hash__(self) -> int:
        # pylint: disable=useless-parent-delegation
        """Return our ID."""
        return super().__hash__()

    def __repr__(self):
        """Talk about yourself."""
        repr_str = super().__repr__()
        handlers = [handler.__name__ for handler in self.handlers]
        return f'{repr_str[:-1]}, hook: {self.webhook.name}, handlers: {handlers}>'

    def run(self, **kwargs):
        """Run it."""
        if self.args.merge_request or self.args.jira:
            func = self.process_cmdline_message
        elif self.args.json_message_file:
            func = self.process_json_message
        else:
            func = self.consume_messages
        return func(**kwargs)

    @staticmethod
    def build_cmdline_message(args):
        """Return a GitlabMsg object matching the command line parameters."""
        if not args.merge_request:
            raise ValueError("'args.merge_request' must be set.")
        if args.action:
            gl_kind = GitlabObjectKind.get(args.action)
        elif args.note:
            gl_kind = GitlabObjectKind.NOTE
        else:
            gl_kind = GitlabObjectKind.MERGE_REQUEST
        msg_dict = {'url': args.merge_request,
                    'object_kind': gl_kind,
                    'project_id': args.payload_project_id,
                    'target_branch': args.payload_target_branch,
                    'note': args.note}
        return GitlabMsg(**msg_dict)

    def process_cmdline_message(self, **kwargs):
        """Set up a fake payload from the given cmdline parameters and process the message."""
        if self.args.jira:
            jkey = self.args.jira.rsplit('/', 1)[-1]
            headers = {
                'message-type': MessageType.UMB_BRIDGE,
                'event_source_environment': self.environment,
                'event_target_webhook': self.webhook.name
            }
            body = {'jira_key': jkey}
        else:
            headers = {'message-type': MessageType.GITLAB}
            body = self.build_cmdline_message(self.args).as_payload()
        return self.process_one_message(routing_key='cmdline', headers=headers, body=body, **kwargs)

    def process_json_message(self, **kwargs):
        """Set up a message from input json and process it."""
        headers = {'message-type': self.args.json_message_type}
        file_name = self.args.json_message_file
        msg_json = Path(file_name).read_text(encoding='utf-8')
        body = json.loads(msg_json)
        return self.process_one_message(routing_key=file_name, headers=headers, body=body, **kwargs)

    def process_one_message(
        self,
        routing_key: str,
        headers: typing.Dict,
        body: typing.Dict,
        **kwargs: typing.Dict
    ) -> bool:
        # pylint: disable=too-many-return-statements
        """Match the event to a Handler and run it if we determine we should."""
        event = create_event(self, headers, body)
        if not event:
            LOGGER.warning('Not a recognized event: %s', headers.get('message-type'))
            return False
        LOGGER.info('Received message on %s of type %s:\n%s', routing_key,
                    event.kind.name if hasattr(event, 'kind') else event.type,
                    json.dumps(body, indent=None))

        if not (handler_func := self.handlers.get(event.__class__)):
            LOGGER.info('No handler for this message, nothing to do.')
            return False

        # Filter out messages which do not match the environment.
        if not event.matches_environment:
            LOGGER.debug('Event does not match environment, ignoring.')
            return False
        # Filter out unwanted messages.
        if not event.passes_filters:
            LOGGER.debug('Event does not pass filters, ignoring.')
            return False
        # This could be an old event or we could just be unlucky so make sure the MR associated
        # with the event is still open.
        if not event.mr_is_open and not self.args.disable_closed_status_check:
            LOGGER.info("MR state is '%s', ignoring.", event.gl_mr.state)
            return False
        # If we passed the filters then take care of the readyForX and Blocked labels, etc...
        self.update_special_labels(event)
        # If the MR branch is inactive then we ignore it.
        if event.mr_branch_is_inactive:
            LOGGER.info("MR target branch is inactive, ignoring.")
            return False
        # If the event doesn't match any of the trigger conditions then we ignore it.
        if not event.matches_trigger:
            LOGGER.debug('Event does not match any triggers, ignoring.')
            return False
        # If the MR has zero commits then don't process the message.
        if event.mr_is_empty:
            LOGGER.info('Event MR has zero commits, ignoring.')
            return False
        # If the MR has too many commits then don't process the message.
        if event.mr_is_oversize:
            LOGGER.info('Event MR is too big for this webhook, ignoring.')
            return False
        # Call the handler and away we go!
        LOGGER.debug('Invoking handler function %s …', handler_func)
        handler_func(body, self, event, **kwargs)
        return True

    @functools.cached_property
    def queue(self):
        """Return a MessageQueue object."""
        mqueue = MessageQueue(host=self.args.rabbitmq_host,
                              port=self.args.rabbitmq_port,
                              user=self.args.rabbitmq_user,
                              password=self.args.rabbitmq_password)
        mqueue.msg_logging_env.add_hook(MessageType.UMB_BRIDGE, make_umb_bridge_logging_extras)
        mqueue.msg_logging_env.add_hook(MessageType.JIRA, make_jira_logging_extras)
        mqueue.msg_logging_env.add_hook(MessageType.DATAWAREHOUSE, make_dw_logging_extras)
        return mqueue

    def consume_messages(self, **kwargs):
        # pylint: disable=unnecessary-lambda
        """Get the MessageQueue and start processing messages."""
        prometheus_init()
        if not self.args.rabbitmq_routing_key:
            raise RuntimeError('--rabbitmq-routing-key must be set.')
        self.queue.consume_messages(
            exchange=self.args.rabbitmq_exchange,
            routing_keys=self.args.rabbitmq_routing_key.split(),
            callback=lambda **cbkwargs: self.process_one_message(**cbkwargs, **kwargs),
            queue_name=self.args.rabbitmq_queue_name
        )

    def update_special_labels(self, event: 'AnyEvent') -> None:
        """Update special labels as needed on GL events."""
        # Only the Webhook instance with 'manage_special_labels' set should be doing this.
        if not self.webhook.manage_special_labels or event.type is not MessageType.GITLAB:
            return
        to_add, to_remove = special_label_changes(event)
        ready_label_changed = \
            any(ready_label in getattr(event, 'changed_labels', []) for ready_label in READY_LABELS)
        if not to_add and not to_remove and not ready_label_changed:
            LOGGER.info('No special label changes.')
            return
        gl_project = event.gl_project
        # If a readyForX label changed then any add/remove_labels call will recalculate it.
        if ready_label_changed:
            LOGGER.info('Event shows a readyForX label in changed labels %s, recalculating it...',
                        event.changed_labels)
        if to_add or not to_remove:
            add_labels(gl_project, event.mr_url.id, to_add)
        if to_remove:
            remove_labels(gl_project, event.mr_url.id, to_remove)

    @functools.cached_property
    def comment(self) -> footer.Footer:
        """Return a footer.Footer instance for the session's Webhook."""
        return footer.Footer(self.webhook)

    def update_webhook_comment(self, gl_mergerequest, text, *, bot_name=None, identifier=None):
        """Find the webhook's status comment so we can edit it in place, add one if none exists."""
        if bot_name and identifier:
            for discussion in gl_mergerequest.discussions.list(iterator=True):
                note = discussion.attributes['notes'][0]
                if note['author']['username'] == bot_name and identifier in note['body']:
                    body = text + self.comment.gitlab_footer('updated')
                    if self.is_production_or_staging:
                        LOGGER.info('Overwriting existing webhook comment:\n%s', body)
                        comment = discussion.notes.get(note['id'])
                        comment.body = body
                        comment.save()
                    else:
                        LOGGER.info('Would overwrite existing webhook comment:\n%s', body)
                    return

        body = text + self.comment.gitlab_footer()
        if self.is_production_or_staging:
            LOGGER.info('Creating new webhook comment:\n%s', body)
            gl_mergerequest.notes.create({'body': body})
        else:
            LOGGER.info('Would create new webhook comment:\n%s', body)


def make_jira_logging_extras(_, body: dict) -> dict:
    """Create the dict of 'extras' logging info for an event from jira."""
    return {'key': body['issue'].get('key'), 'user': body['issue'].get('user')} if \
        'issue' in body else {}


def make_umb_bridge_logging_extras(_, body):
    """Create the dict of 'extras' logging info for an ampq_bridge event."""
    extras = {}
    if gitlab_url := body.get('gitlab_url'):
        extras['gitlab_url'] = gitlab_url
    if jira_key := body.get('jira_key'):
        extras['jira_key'] = jira_key
    return extras


def make_dw_logging_extras(_, body):
    """Create the dict of 'extras' logging info for an datawarehouse event."""
    return {
        'status': body.get('status'),
        'object_type': body.get('object_type'),
        'object_id': get_nested_key(body, 'object/id', default=None),
    }


@dataclass
class GitlabMsg:
    """Creates a gitlab webhook event message."""

    url: str
    object_kind: GitlabObjectKind = GitlabObjectKind.MERGE_REQUEST
    project_id: int = 123
    target_branch: str = 'main'
    note: str = ''

    def as_payload(self):
        """Return the payload as a dict."""
        payload = {'object_kind': self.object_kind.name.lower()}
        if self.object_kind is not GitlabObjectKind.BUILD:
            payload['project'] = self.project_dict
        if self.object_kind is not GitlabObjectKind.PUSH:
            payload['user'] = {'username': 'cli', 'id': 0}

        match self.object_kind:
            case GitlabObjectKind.MERGE_REQUEST:
                payload.update(
                    {'changes': {},
                     'labels': [],
                     'object_attributes': self.merge_request_dict}
                )
            case GitlabObjectKind.NOTE:
                payload.update(
                    {'merge_request': self.merge_request_dict,
                     'object_attributes': {'noteable_type': 'MergeRequest', 'note': self.note}}
                )
            case GitlabObjectKind.PIPELINE:
                payload.update(
                    {'merge_request': self.merge_request_dict}
                )
            case GitlabObjectKind.PUSH:
                payload.update(
                    {'ref': f'refs/head/{self.target_branch}',
                     'user_id': 0,
                     'user_username': 'cli'}
                )
        return payload

    @property
    def iid(self):
        """Return the MR ID as an int."""
        return parse_mr_url(self.url)[1]

    @property
    def path_with_namespace(self):
        """Return the path_with_namespace string."""
        return parse_mr_url(self.url)[0]

    @property
    def project_name(self):
        """Return the project name string."""
        return self.path_with_namespace.rsplit('/', 1)[-1]

    @property
    def merge_request_dict(self):
        """Return the merge_request dict."""
        mr_dict = {'iid': self.iid,
                   'labels': [],
                   'state': 'opened',
                   'target_branch': self.target_branch,
                   'work_in_progress': False,
                   'url': self.url
                   }
        if self.object_kind is GitlabObjectKind.MERGE_REQUEST:
            mr_dict.update({'action': 'open', 'head_pipeline_id': None})
        return mr_dict

    @property
    def project_dict(self):
        """Return the project dict."""
        scheme = self.url.split(':', 1)[0]
        netloc = self.url.removeprefix(f'{scheme}://').split('/', 1)[0]
        return {'id': self.project_id,
                'name': self.project_name,
                'description': 'A faux project',
                'web_url': f'{scheme}://{netloc}/{self.path_with_namespace}',
                'git_ssh_url': f'git@{netloc}:{self.path_with_namespace}.git',
                'git_http_url': f'{scheme}://{netloc}/{self.path_with_namespace}.git',
                'namespace': self.path_with_namespace,
                'visibility_level': 0,
                'path_with_namespace': self.path_with_namespace,
                'default_branch': 'main',
                "ci_config_path": ""
                }
