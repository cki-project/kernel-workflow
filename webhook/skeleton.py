"""Skeleton-hook for MRs."""
from dataclasses import dataclass
import logging
import sys
import typing

from webhook import common
from webhook.base_mr import BaseMR
from webhook.base_mr_mixins import CommitsMixin
from webhook.session import SessionRunner
from webhook.session_events import GitlabMREvent
from webhook.session_events import GitlabNoteEvent

LOGGER = logging.getLogger('cki.webhook.skeleton')


@dataclass(repr=False, kw_only=True)
class SkeletonMR(CommitsMixin, BaseMR):
    # pylint: disable=too-many-ancestors
    """Store for Skeleton MR data."""

    def update_statuses(self, labels_to_add, labels_to_remove, comment) -> None:
        """Wrap old API stuff to set the labels, check dependencies and set testing requested."""
        self.session.update_webhook_comment(self.gl_mr, comment,
                                            bot_name=self.session.gl_user.username)

        # We don't need the info of fresh labels here
        self.remove_labels(labels_to_remove)

        current_labels = self.add_labels(labels_to_add)
        LOGGER.debug('Current MR labels: %s', current_labels)


def process_mr(session, mr_url):
    """Process the given MR."""
    # Fetch and parse MR data.
    this_mr = SkeletonMR.new(session, mr_url)
    LOGGER.debug("Full MR details: %s", vars(this_mr.gl_mr))

    comment = f"Product: {this_mr.gl_mr.milestone['description']}\n"
    comment += f"Red Hat Project: {this_mr.rh_project}\n"
    comment += f"Red Hat Branch: {this_mr.rh_branch}\n"
    comment += f"MR Title: {this_mr.gl_mr.title}\n"
    comment += f"MR Commit Count: {this_mr.commit_count}\n"
    comment += f"MR Files touched: {this_mr.files}\n"
    comment += f"MR Commit data: {this_mr.commits}\n"

    this_mr.update_statuses([], [], comment)


def process_event(
    _: dict,
    session: SessionRunner,
    event: GitlabMREvent | GitlabNoteEvent,
    **__: typing.Any
) -> None:
    """Process an event."""
    process_mr(session, event.mr_url)


HANDLERS = {
    GitlabMREvent: process_event,
    GitlabNoteEvent: process_event
}


def main(args):
    """Run main loop."""
    common.init_sentry()
    parser = common.get_arg_parser('SKELETON')
    args = parser.parse_args(args)
    session = SessionRunner.new('skeleton', args=args, handlers=HANDLERS)
    session.run()


if __name__ == "__main__":
    main(sys.argv[1:])
