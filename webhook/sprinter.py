"""Implement the CKI GitLab workflow."""

import os
import re
import sys
import typing

from cki_lib import gitlab
from cki_lib import logger
from cki_lib import misc
from cki_lib import yaml
import dateutil.parser

from . import common
from .session import SessionRunner
from .session_events import GitlabIssueEvent
from .session_events import GitlabMREvent

LOGGER = logger.get_logger('cki.webhook.sprinter')

LABELS_FORMAT = 'CWF::Sprint::{year}-week-{week}'

CONFIG = yaml.load(contents=os.environ.get('SPRINTER_CONFIG'),
                   file_path=os.environ.get('SPRINTER_CONFIG_PATH'))


def get_label_name(date):
    """Return label name for current sprint."""
    date = dateutil.parser.parse(date).isocalendar()
    return LABELS_FORMAT.format(year=date.year, week=date.week)


def run_issue_commands(gl_project, issue_iid, cmds):
    """Run slash commands on a GitLab issue."""
    LOGGER.info('Running slash commands on issue %s/%s: %s',
                gl_project.web_url, issue_iid, ' '.join(cmds))
    if misc.is_production_or_staging():
        gl_project.issues.get(issue_iid, lazy=True).notes.create({'body': '\n'.join(cmds)})


def add_labels_to_issue(
    project_url: str,
    issue_iid: int,
    label_names: list[str],
    *,
    labels: list[dict[typing.Any]] | None = None,
    level: str | None = None,
) -> None:
    """Add labels to a GitLab issue."""
    LOGGER.info('Adding labels %s to %s/%s', label_names, project_url, issue_iid)
    gl_instance, gl_project = gitlab.parse_gitlab_url(project_url)
    with gl_instance:
        label_dicts = common.validate_labels(label_names)
        # pylint: disable=protected-access  # api needs to stabilize before move to common
        label_cmds = common._add_label_quick_actions(gl_project, label_dicts, level=level)
        run_issue_commands(gl_project, issue_iid, label_cmds)

    if labels is not None:
        labels.extend({'title': name}
                      for name in set(label_names) - set(label['title'] for label in labels))


def remove_labels_from_issue(
    project_url: str,
    issue_iid: int,
    label_names: list[str],
    *,
    labels: list[dict[typing.Any]] | None = None,
) -> None:
    """Remove labels from a GitLab issue."""
    LOGGER.info('Removing labels %s from %s/%s', label_names, project_url, issue_iid)
    gl_instance, gl_project = gitlab.parse_gitlab_url(project_url)
    with gl_instance:
        run_issue_commands(gl_project, issue_iid, [f"/unlabel \"{name}\"" for name in label_names])
    if labels is not None:
        labels[:] = [label for label in labels if label['title'] not in label_names]


def process_issue(body, *_, **__):
    """Check issues."""
    project_url = misc.get_nested_key(body, 'project/web_url')
    if (rule := enabled('iterations', body)) is not None:
        check_iterations(project_url, body, rule)
    if (rule := enabled('sprint_labels', body)) is not None:
        check_sprint_label(project_url, body, rule)
    if (rule := enabled('incident_labels', body)) is not None:
        check_incident_labels(project_url, body, rule)
    if (rule := enabled('team_labels', body)) is not None:
        check_team_labels(project_url, body, rule)
    if (rule := enabled('stage_labels', body)) is not None:
        check_stage_labels(project_url, body, rule)
    if (rule := enabled('automatic_weights', body)) is not None:
        check_automatic_weights(project_url, body, rule)


def check_iterations(project_url, payload, _):
    """Add closed issues to the current iteration."""
    issue_iid = misc.get_nested_key(payload, 'object_attributes/iid')
    action = misc.get_nested_key(payload, 'object_attributes/action')

    if action == 'close':
        gl_instance, gl_project = gitlab.parse_gitlab_url(project_url)
        with gl_instance:
            gl_issue = gl_project.issues.get(issue_iid)
            if not gl_issue.attributes.get('iteration'):
                run_issue_commands(gl_project, issue_iid, ['/iteration --current'])


def check_sprint_label(project_url, payload, rule):
    """Add sprint label to closed issues."""
    issue_iid = misc.get_nested_key(payload, 'object_attributes/iid')
    action = misc.get_nested_key(payload, 'object_attributes/action')
    labels = misc.get_nested_key(payload, 'object_attributes/labels')
    closed_at = misc.get_nested_key(payload, 'object_attributes/closed_at')
    level = rule.get('level')

    sprint_labels = [title for label in labels
                     if (title := label['title']).startswith('CWF::Sprint::')]

    if action == 'reopen':
        if sprint_labels:
            remove_labels_from_issue(project_url, issue_iid, sprint_labels,
                                     labels=labels)
    if action == 'close':
        add_labels_to_issue(project_url, issue_iid, [get_label_name(closed_at)],
                            labels=labels,
                            level=level)


def check_incident_labels(project_url, payload, rule):
    """Ensure correct incident labels on issues."""
    changes = payload.get('changes', {})
    issue_iid = misc.get_nested_key(payload, 'object_attributes/iid')
    state = misc.get_nested_key(payload, 'object_attributes/state')
    action = misc.get_nested_key(payload, 'object_attributes/action')
    labels = misc.get_nested_key(payload, 'object_attributes/labels')
    level = rule.get('level')

    had_type_incident = any(label['title'] == 'CWF::Type::Incident'
                            for label in misc.get_nested_key(changes, 'labels/previous', []))
    has_type_incident = any(label['title'] == 'CWF::Type::Incident' for label in labels)
    incident_labels = [title for label in labels
                       if (title := label['title']).startswith('CWF::Incident::')]

    if action == 'close':
        if incident_labels:
            remove_labels_from_issue(project_url, issue_iid, incident_labels)
    if action in {'open', 'reopen'} or (action == 'update' and state == 'opened'):
        if incident_labels and not has_type_incident and had_type_incident:
            remove_labels_from_issue(project_url, issue_iid, incident_labels,
                                     labels=labels)
        elif incident_labels and not has_type_incident and not had_type_incident:
            add_labels_to_issue(project_url, issue_iid, ['CWF::Type::Incident'],
                                labels=labels,
                                level=level)
        elif has_type_incident and not incident_labels:
            add_labels_to_issue(project_url, issue_iid, ['CWF::Incident::Active'],
                                labels=labels,
                                level=level)


def check_team_labels(project_url, payload, rule):
    """Ensure correct team labels on issues."""
    issue_iid = misc.get_nested_key(payload, 'object_attributes/iid')
    state = misc.get_nested_key(payload, 'object_attributes/state')
    action = misc.get_nested_key(payload, 'object_attributes/action')
    labels = misc.get_nested_key(payload, 'object_attributes/labels')
    level = rule.get('level')
    team_label = rule.get('team')

    has_team_labels = any(label['title'].startswith('CWF::Team::') for label in labels)

    if action in {'open', 'reopen'} or (action == 'update' and state == 'opened'):
        if not has_team_labels and team_label:
            add_labels_to_issue(project_url, issue_iid, [f'CWF::Team::{team_label}'],
                                labels=labels,
                                level=level)


def check_stage_labels(project_url, payload, _):
    """Ensure correct stage labels on issues."""
    issue_iid = misc.get_nested_key(payload, 'object_attributes/iid')
    action = misc.get_nested_key(payload, 'object_attributes/action')
    labels = misc.get_nested_key(payload, 'object_attributes/labels')

    stage_labels = [title for label in labels
                    if (title := label['title']).startswith('CWF::Stage::')]

    if action == 'close' and stage_labels:
        remove_labels_from_issue(project_url, issue_iid, stage_labels, labels=labels)


def check_automatic_weights(project_url, payload, rule):
    """Ensure correct weights on issues."""
    issue_iid = misc.get_nested_key(payload, 'object_attributes/iid')
    state = misc.get_nested_key(payload, 'object_attributes/state')
    action = misc.get_nested_key(payload, 'object_attributes/action')
    weight = misc.get_nested_key(payload, 'object_attributes/weight')
    default_weight = rule.get('weight')

    if action in {'open', 'reopen'} or (action == 'update' and state == 'opened'):
        if not weight and default_weight:
            gl_instance, gl_project = gitlab.parse_gitlab_url(project_url)
            with gl_instance:
                run_issue_commands(gl_project, issue_iid, [f'/weight {default_weight}'])


def enabled(
    key: str,
    body: typing.Any,
) -> typing.Optional[dict[str, typing.Any]]:
    """Check whether the message is in-scope for the given rules.

    Returns the rule that enables it, or None if the message is out-of-scope.
    """
    project_url = misc.get_nested_key(body, 'project/web_url')
    labels = misc.get_nested_key(body, 'object_attributes/labels', [])
    for rule in misc.get_nested_key(CONFIG, key, []):
        if rule_project := rule.get('project'):
            if project_url == rule_project:
                return rule if rule.get('enabled', True) else None
            continue
        if rule_group := rule.get('group'):
            if project_url.startswith(rule_group + '/'):
                return rule if rule.get('enabled', True) else None
            continue
        if rule_team_label := rule.get('team_label'):
            if any(label['title'] == f'CWF::Team::{rule_team_label}' for label in labels):
                return rule if rule.get('enabled', True) else None
            continue
        return {**rule, 'enabled': True} if rule.get('enabled', True) else None
    return {'enabled': True}


def process_mr(body, *_, **__):
    """Check MRs."""
    project_url = misc.get_nested_key(body, 'project/web_url')
    if (rule := enabled('issue_checks', body)) is not None:
        check_mr_issue_ref(project_url, body, rule)


def check_mr_issue_ref(project_url, payload, rule):
    """Check MRs for GitLab issue links."""
    changes = payload['changes']
    action = misc.get_nested_key(payload, 'object_attributes/action')
    description = misc.get_nested_key(payload, 'object_attributes/description')
    iid = misc.get_nested_key(payload, 'object_attributes/iid')
    level = rule.get('level')

    description_changed = 'description' in changes
    label_changed = common.has_label_prefix_changed(changes, 'CWF::Issue::')
    if action != 'open' and not description_changed and not label_changed:
        LOGGER.info('MR event does not indicate any relevant changes, ignoring.')
        return

    regex = re.compile(r"https://gitlab[^ ]+/-/(work_items|issues|epics)/\d+|(?:[^\s]+)?#\d+")
    label = 'CWF::Issue::Missing'
    if description and regex.search(description):
        label = 'CWF::Issue::OK'

    _, gl_project = gitlab.parse_gitlab_url(project_url)
    common.add_label_to_merge_request(gl_project, iid, [label],
                                      compute_mr_status_labels=False, level=level)


HANDLERS = {
    GitlabIssueEvent: process_issue,
    GitlabMREvent: process_mr,
}


def main(args):
    """Run main loop."""
    common.init_sentry()
    parser = common.get_arg_parser('SPRINTER')
    args = parser.parse_args(args)
    args.disable_inactive_branch_check = True
    args.disable_closed_status_check = True

    session = SessionRunner.new('sprinter', args=args, handlers=HANDLERS)
    session.run()


if __name__ == "__main__":
    main(sys.argv[1:])
