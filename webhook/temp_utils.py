"""
Things from common that can't be imported by some webhook modules.

This file is a mistake and this code should live in webhook.common.
See https://gitlab.com/cki-project/kernel-workflow/-/issues/625 .
"""
import copy
import os
import pathlib
import typing

from cki_lib import logger
from cki_lib import misc
from cki_lib import yaml

from webhook import defs

LOGGER = logger.get_logger(__name__)


YamlData = typing.Union[typing.Dict, typing.List, str]
yaml_cache = {}


def with_yaml_validation(func):
    """Decorate to selectively enable yaml validation."""
    def wrapper(*args, **kwargs):
        orig_validate = defs.YAML_DO_SCHEMA_VALIDATE
        defs.YAML_DO_SCHEMA_VALIDATE = True
        orig_cache = defs.YAML_DO_CACHE_LOADS
        defs.YAML_DO_CACHE_LOADS = False
        try:
            result = func(*args, **kwargs)
        finally:
            defs.YAML_DO_SCHEMA_VALIDATE = orig_validate
            defs.YAML_DO_CACHE_LOADS = orig_cache
        return result
    return wrapper


def set_yaml_validation(val):
    """Set yaml validation."""
    defs.YAML_DO_SCHEMA_VALIDATE = val


def set_yaml_load_caching(val):
    """Set yaml caching."""
    defs.YAML_DO_CACHE_LOADS = val


def maybe_yaml_validate(instance, schema, *args, **kwargs):
    """Call yaml.validate depending on state of global variable."""
    if defs.YAML_DO_SCHEMA_VALIDATE:
        return yaml.validate(instance, schema, *args, **kwargs)
    return instance


def load_and_validate(path: str | pathlib.Path, schema: dict | list) -> YamlData:
    """Return yaml data from the given path after validating it against the schema."""
    yaml_data = load_one_yaml(path)
    maybe_yaml_validate(yaml_data, schema)
    return yaml_data


def load_one_yaml(path: str | pathlib.Path) -> typing.Union[YamlData, None]:
    """Call cki_lib.yaml.load() on the given path."""
    if defs.YAML_DO_CACHE_LOADS and path in yaml_cache:
        LOGGER.info("Loaded yaml from cache, path: '%s'", path)
        return copy.deepcopy(yaml_cache[path])
    with misc.only_log_exceptions():
        LOGGER.info("Loading yaml from path: '%s'", path)
        ret = yaml.load(file_path=path)
        yaml_cache[path] = ret
        return copy.deepcopy(ret)
    return None


def load_yaml_data(
    preferred_path: str | pathlib.Path,
    backup_env_var: str = '',
    other_paths: typing.Optional[typing.Iterable[str | pathlib.Path]] = None,
) -> typing.Union[YamlData, None]:
    """
    Return yaml data from one of the given paths.

    If either of preferred_path or the environment variable backup_env_var are set then
    try to load the first one that is set.

    If neither preferred_path nor the environment variable backup_env_var are set then
    iterate through the other_paths and try to load the first one that exists.

    If preferred_path & backup_env_var are not set and no other_paths exist then return None.
    """
    if path := preferred_path or os.environ.get(backup_env_var, ''):
        return load_one_yaml(path)
    for path in other_paths or []:
        if os.path.exists(path):
            return load_one_yaml(path)
    return None
