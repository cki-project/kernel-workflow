"""Process bugzilla events from the UMB."""
from collections import defaultdict
from dataclasses import dataclass
from dataclasses import field
from functools import cached_property
import re
import sys
from threading import Lock
from threading import Thread
import time
import typing

from cki_lib import logger
from gitlab.exceptions import GitlabListError
from kwf_lib.common import JiraKey
from kwf_lib.common import TrackerType
from pika.exceptions import AMQPError

from webhook import common
from webhook import defs
from webhook.description import MRDescription
from webhook.kwf_tracker import KwfIssueTracker
from webhook.libbackport import KWF_BACKPORT_FAIL_LABEL
from webhook.libbackport import KWF_BACKPORT_SUCCESS_LABEL
from webhook.messenger import KwfMessage
from webhook.session import SessionRunner
from webhook.session_events import GitlabMREvent
from webhook.session_events import JiraCreateEvent
from webhook.session_events import JiraUpdateEvent

if typing.TYPE_CHECKING:
    from gitlab.client import Gitlab
    from gitlab.v4.objects.merge_requests import ProjectMergeRequest

    from webhook.graphql import GitlabGraph
    from webhook.rh_metadata import Project

LOGGER = logger.get_logger('cki.webhook.umb_bridge')
SEND_DELAY = 1
KWF_BACKPORT_LABELS = {KWF_BACKPORT_SUCCESS_LABEL, KWF_BACKPORT_FAIL_LABEL}


# A type hint to represent an iterable of of MRInfo objects
MRInfoList = typing.Iterable['MRInfo']

# Regex to get a RHEL jira key from a testing Task summary :grimacing:.
TESTING_TASK_PARENT_KEY_REGEX = re.compile(r'testing of (?P<key>RHEL-\d+) ')


def backporter_prospect(issue: KwfIssueTracker) -> bool:
    """Return True if the KwfIssueTracker is a backporter prospect."""
    return issue.commit_hashes and not set(issue.labels) & KWF_BACKPORT_LABELS


def get_project_mrs(
    gl_instance: 'Gitlab',
    project_namespace: str
) -> typing.List['ProjectMergeRequest']:
    """Return the list of open MRs in the given project namespace."""
    gl_project = gl_instance.projects.get(project_namespace, lazy=True)
    try:
        return gl_project.mergerequests.list(all=True, state='opened', per_page=100)
    except GitlabListError as err:
        if err.response_code == 403:
            LOGGER.error("User '%s' does not have access to %s, skipping.",
                         gl_instance.user.username, project_namespace)
        else:
            raise
    return []


def get_project_mr_infos(
    gl_instance: 'Gitlab',
    graphql: 'GitlabGraph',
    rh_project: 'Project'
) -> typing.List['MRInfo']:
    """Return a list of MRInfo objects derived from open MRs in the given rh_metadata.Project."""
    mr_infos = []
    for gl_mr in get_project_mrs(gl_instance, rh_project.namespace):
        if not (branch := rh_project.get_branch_by_name(gl_mr.target_branch)):
            LOGGER.warning('%s does not have a matching metadata Branch.', gl_mr.web_url)
            continue
        mr_infos.append(MRInfo.new(gl_mr.web_url, branch.fix_versions, gl_mr.description, graphql))
    return mr_infos


def get_projects_mr_infos(
    gl_instance: 'Gitlab',
    graphql: 'GitlabGraph',
    rh_projects: typing.Iterable['Project']
) -> typing.List['MRInfo']:
    """Return a list of MRInfo objects derived from the given Projects."""
    return [mr for p in rh_projects for mr in get_project_mr_infos(gl_instance, graphql, p)]


def _set_up_send_queue(send_queue, send_message):
    """Set up the send_queue."""
    # Before we do anything check the sender thread is running.
    send_queue.check_status()

    if send_queue.send_function is None and not send_queue.fake_queue:
        LOGGER.info('Setting send_queue.send_message to: %s', send_message)
        send_queue.send_function = send_message


def _get_issue_keys(issue: KwfIssueTracker) -> set[JiraKey]:
    """Return the set of JiraKeys that we want to look up in our MR mapping."""
    issue_ids: set[JiraKey] = {issue.id}

    if issue.type is TrackerType.TASK:
        # If this KwfIssueTracker is a Task then also consider any issues it `blocks`.
        # It should be blocking the issue it was created for, and we should know the MR associated
        # with it.
        issue_ids.update(issue.links.blocks)

        # If this event relates to a task being created then it won't show any issuelinks. But the
        # parent issue is given in the description so dig that out and include it in our lookup.
        issue_ids.update(
            defs.JiraKey(key) for key in TESTING_TASK_PARENT_KEY_REGEX.findall(issue.description)
        )

    return issue_ids


def process_jira_event(
    _: typing.Dict,
    session: SessionRunner,
    event: JiraCreateEvent | JiraUpdateEvent,
    mr_info_cache: 'MRInfoCache',
    send_queue: 'SendQueue',
    **__: typing.Any
) -> None:
    """Process Jira webhook events."""
    _set_up_send_queue(send_queue, session.queue.send_message)
    LOGGER.info('Processing jira event for %s triggered by %s', event.key, event.user)

    issue = event.tracker

    if not isinstance(issue, KwfIssueTracker):
        LOGGER.info('Ignoring event for non-kwf issue.')
        return

    LOGGER.info('Issue CVE labels: %s', issue.cve_ids)

    if backporter_prospect(issue):
        LOGGER.info('No backport labels and commit hashes populated, pass to backporter.')
        send_queue.send_jira_key(target_hook='backporter', jira_key=issue.id)

    issue_ids = _get_issue_keys(issue)
    LOGGER.info('Checking for MRs which match these keys: %s', issue_ids)

    if (matching_mrs := mr_info_cache.match(jiras=issue_ids,
                                            cves=issue.cve_ids,
                                            fix_version=issue.fix_version)):
        LOGGER.info('Event matches %s', matching_mrs)

        for mr_url in matching_mrs:
            send_queue.send_gitlab_url(target_hook='jirahook', gitlab_url=mr_url)
    else:
        LOGGER.info('Ignoring event not relevant to any known MRs.')


def process_gitlab_mr(
    _: dict,
    session: SessionRunner,
    event: GitlabMREvent,
    mr_info_cache: 'MRInfoCache',
    **__: typing.Any
) -> None:
    """Process gitlab MR hook events."""
    # This should also pass through events showing a target branch change but at the moment
    # there is no simple way to know this.
    # https://gitlab.com/gitlab-org/gitlab/-/merge_requests/128747
    if event.closing or event.mr_inactive:
        mr_info_cache.remove(event.mr_url)
        return
    # If an MR is opened against an unexpected target branch then we need to ignore it.
    if not event.rh_branch:
        LOGGER.info('Event is not associated with a recognized branch, ignoring.')
        return
    mr_info = MRInfo.new(
        url=event.mr_url,
        fix_versions=event.rh_branch.fix_versions,
        description=event.merge_request.get('description', ''),
        graphql=session.graphql
    )
    mr_info_cache.add(mr_info)


@dataclass(frozen=True)
class MRInfo:
    """Tiny object to hold MR details."""

    url: defs.GitlabURL
    fix_versions: typing.FrozenSet[defs.FixVersion] = frozenset()
    cves: typing.FrozenSet[str] = frozenset()
    jiras: typing.FrozenSet[defs.JiraKey] = frozenset()

    def __repr__(self) -> str:
        """Say hi."""
        fvs_str = ', '.join(self.fix_versions) if self.fix_versions else 'None'
        return f'<{self.__class__.__name__} {self.gl_reference} (fix_versions: {fvs_str})>'

    @classmethod
    def new(cls, url: str, fix_versions: typing.Iterable[str], description: str,
            graphql: 'GitlabGraph') -> 'MRInfo':
        """Construct a new MRInfo object."""
        url = defs.GitlabURL(url)
        mr_description = MRDescription(description, url.namespace, graphql)
        cves = mr_description.cve
        jiras = mr_description.jira_tags | mr_description.depends_jiras
        return cls(url, frozenset(defs.FixVersion(fv) for fv in fix_versions),
                   frozenset(cves), frozenset(jiras))

    @cached_property
    def gl_reference(self) -> str:
        """Return a reference to the MR in the Gitlab format 'namespace!ID'."""
        if self.url.type != 'merge_requests':
            raise RuntimeError(f"Only MRs are supported, not '{self.url.type}'!")
        return f'{self.url.namespace}!{self.url.id}'


MRInfoDict = typing.Dict[defs.GitlabURL, MRInfo]
CveDict = typing.DefaultDict[str, typing.Set[MRInfo]]
JiraDict = typing.DefaultDict[defs.JiraKey, typing.Set[MRInfo]]


@dataclass
class MRInfoCache:
    """Maintains a mapping of bugs/cves/jiras to MRs."""

    # mr_infos: a dict with MR URLs keys and MRInfo instances as the values.
    #
    # rebuild_caches() parses all the current mr_infos and populates new bugs/cves/jiras dicts.
    mr_infos: MRInfoDict = field(default_factory=dict)
    cves: CveDict = field(init=False, default_factory=lambda: defaultdict(set))
    jiras: JiraDict = field(init=False, default_factory=lambda: defaultdict(set))

    def __post_init__(self) -> None:
        """Populate the caches."""
        self.rebuild_caches()
        LOGGER.info('Created %s', self)

    def __repr__(self) -> str:
        """Say it out loud."""
        repr_str = f'CVEs: {len(self.cves)}, Jiras: {len(self.jiras)}'
        return f'<{self.__class__.__name__} MRs: {len(self.mr_infos)}, {repr_str}'

    @staticmethod
    def _build_caches(
        mr_infos: MRInfoDict
    ) -> typing.Tuple[
            defaultdict[str, typing.Set[MRInfo]], defaultdict[defs.JiraKey, typing.Set[MRInfo]]
            ]:
        """Build the caches with data from the given mr_infos."""
        cves: CveDict = defaultdict(set)
        jiras: JiraDict = defaultdict(set)
        for mr_info in mr_infos.values():
            for cve in mr_info.cves:
                cves[cve].add(mr_info)
            for jira in mr_info.jiras:
                jiras[jira].add(mr_info)
        return cves, jiras

    def rebuild_caches(self) -> None:
        """Rebuild the caches."""
        LOGGER.debug('Rebuilding cache...')
        self.cves, self.jiras = self._build_caches(self.mr_infos)

    def add(self, mr_info: MRInfo) -> None:
        """Add an MRInfo and rebuild the bugs/cves/jiras/ cache."""
        LOGGER.info('Adding %s to cache.', mr_info)
        self.mr_infos[mr_info.url] = mr_info
        self.rebuild_caches()

    def remove(self, mr_url: str) -> None:
        """Remove the given URL's entries from the cache."""
        if not (mr_info := self.mr_infos.pop(defs.GitlabURL(mr_url), None)):
            LOGGER.warning('%s not found in cache, nothing to remove.', mr_url)
            return
        LOGGER.info('Removed %s', mr_info)
        self.rebuild_caches()

    def match(
        self,
        cves: typing.Optional[typing.Iterable[str]] = None,
        jiras: typing.Optional[typing.Iterable[defs.JiraKey]] = None,
        fix_version: typing.Optional[defs.FixVersion] = None
    ) -> typing.Set[str]:
        """Return the list of MR URLs from the caches which match the input."""
        mrs: typing.Set[MRInfo] = set()
        mrs.update(
            mr_info for search_key in jiras or [] for mr_info in self.jiras.get(search_key, set()))
        for cve in cves or []:
            # If given a fix_version then only include the mr_ref if it has a matching fix_version,
            # otherwise just include all of them :shrug:.
            mrs.update(
                mr_info for mr_info in self.cves.get(cve, set()) if
                not fix_version or
                fix_version in mr_info.fix_versions
            )
        return {mr_info.url for mr_info in mrs}

    @classmethod
    def new(cls, session: 'SessionRunner') -> 'MRInfoCache':
        """Return a new MRInfoCache loaded with data from the session's relevant projects."""
        if not (projects := session.rh_projects.projects.values()):
            raise RuntimeError('No projects found, nothing to do.')
        LOGGER.info('Loading cache data from these projects: %s', projects)
        mr_infos = get_projects_mr_infos(session.gl_instance, session.graphql, projects)
        return cls({mr_info.url: mr_info for mr_info in mr_infos})


HANDLERS = {
    GitlabMREvent: process_gitlab_mr,
    JiraCreateEvent: process_jira_event,
    JiraUpdateEvent: process_jira_event
}


class SendQueue:
    # pylint: disable=too-many-instance-attributes
    """Manage queue of MRs to send to the webhooks hook as a separate thread."""

    def __init__(self, sender_exchange: str, sender_route: str, fake_queue: bool = False) -> None:
        """Set up send params and spawn thread."""
        self.data: dict[KwfMessage, float] = {}
        self.logger = logger.get_logger('cki.webhook.umb_bridge.SendQueue')
        self.fake_queue = fake_queue

        # Must be set to MessageQueue.send_message() method before using the queue.
        self.send_function = None

        # Params needed for each KwfMessage.
        self.kwf_message_params: dict[str, typing.Any] = {
            'queue_name': sender_route,
            'exchange': sender_exchange,
            'source_hook': 'umb_bridge'
        }

        self._lock = Lock()
        self._thread = Thread(target=self._sender_thread, daemon=True)

    def check_send_function(self) -> bool:
        """Barf if send_function has not been set."""
        if self.fake_queue:
            return True
        if self.send_function is None:
            self.logger.warning('send_function is not set!')
            return False
        return True

    def add(self, kwf_message: KwfMessage) -> None:
        """Add or update an entry in the queue."""
        if not self.check_send_function():
            return
        with self._lock:
            self.logger.info('Adding %s for %s.',
                             kwf_message.data.jira_key or kwf_message.data.gitlab_url,
                             kwf_message.headers.event_target_webhook)
            # Remove it so we can depend on insertion order to test age.
            if kwf_message in self.data:
                del self.data[kwf_message]
            self.data[kwf_message] = time.time()

    def create_event(
        self,
        target_hook: str,
        gitlab_url: str = '',
        jira_key: str = '',
        source_id: str = ''
    ) -> KwfMessage:
        """Return a KwfMessage with the given parameters."""
        params = {
            'target_hook': target_hook,
            'gitlab_url': gitlab_url,
            'jira_key': jira_key,
            'source_id': source_id
        }
        return KwfMessage.create(**self.kwf_message_params | params)

    def send_jira_key(self, target_hook: str, jira_key: str, source_id: str = '') -> None:
        """Add or update a KwfMessage in the queue with a jira_key."""
        self.add(self.create_event(target_hook=target_hook, jira_key=jira_key, source_id=source_id))

    def send_gitlab_url(self, target_hook: str, gitlab_url: str, source_id: str = '') -> None:
        """Add or update a KwfMessage in the queue with a gitlab_url."""
        self.add(
            self.create_event(target_hook=target_hook, gitlab_url=gitlab_url, source_id=source_id)
        )

    def _get(self) -> typing.Union[KwfMessage, None]:
        """Return the oldest item at least SEND_DELAY old."""
        # Caller must hold self.lock!
        if not self.data:
            self.logger.debug('Empty.')
            return None

        # Is the first (oldest) item at least SEND_DELAY old?
        kwf_message = next(iter(self.data))
        if self.data[kwf_message] > time.time() - SEND_DELAY:
            self.logger.debug('Nothing older than SEND_DELAY (%d) in data.', SEND_DELAY)
            return None
        return kwf_message

    def _send_message(self, kwf_message: KwfMessage) -> None:
        """Send a message to the queue."""
        send_params = kwf_message.asdict()
        try:
            # pylint: disable=not-callable
            if not self.fake_queue:
                self.send_function(**send_params)
            self.logger.info('Sent %s for %s.',
                             kwf_message.data.jira_key or kwf_message.data.gitlab_url,
                             kwf_message.headers.event_target_webhook)
        except AMQPError as err:
            self.logger.error('Error sending message: %s', err)
        del self.data[kwf_message]

    def send_all(self):
        """Send all outstanding messages."""
        with self._lock:
            while (kwf_message := self._get()) is not None:
                if not self.check_send_function():
                    return
                self._send_message(kwf_message)
            self.logger.debug('Nothing to send.')

    def _sender_thread(self):
        """Process send queue."""
        while True:
            time.sleep(10)
            self.send_all()

    def check_status(self):
        """Confirm the sender thread is running, otherwise die."""
        # If the sender thread is not running try to send any messages in the
        # queue and then die.
        if not self._thread.is_alive():
            self.logger.error('Sender thread not running as expected.')
            with self._lock:
                for kwf_message in self.data.copy():
                    self._send_message(kwf_message)
                sys.exit(1)
        return True

    def start(self):
        """Start the sender thread."""
        self.logger.debug('Starting sender thread.')
        self._thread.start()
        self.check_status()


def main(args):
    """Run main loop."""
    common.init_sentry()
    parser_prefix = 'UMB_BRIDGE'
    parser = common.get_arg_parser(parser_prefix)
    parser.add_argument('--rabbitmq-sender-exchange',
                        **common.get_argparse_environ_opts(f'{parser_prefix}_SENDER_EXCHANGE'),
                        help='RabbitMQ Exchange for sending messages.')
    parser.add_argument('--rabbitmq-sender-route',
                        **common.get_argparse_environ_opts(f'{parser_prefix}_SENDER_ROUTE'),
                        help='RabbitMQ Routing Key for sending messages.')
    parser.add_argument('--no-send-queue', action='store_true',
                        help='Run without a SendQueue for debugging purposes.')

    args = parser.parse_args(args)

    if not args.rabbitmq_routing_key:
        raise RuntimeError(f'--rabbitmq-routing-key ({parser_prefix}_ROUTING_KEYS) must be set')

    session = SessionRunner.new('umb_bridge', args=args, handlers=HANDLERS)

    # Setting --no-send-queue means umb_bridge will not generate any events so it should never
    # be used in production.
    if args.no_send_queue and session.is_production:
        raise RuntimeError('--no-send-queue should not be used in production!')

    # Initiate and start the send_queue handler.
    send_queue = SendQueue(args.rabbitmq_sender_exchange, args.rabbitmq_sender_route,
                           fake_queue=args.no_send_queue)
    send_queue.start()

    # Populate GL bug/cve/jira mapping and start consuming messages.
    mr_info_cache = MRInfoCache.new(session)
    session.run(mr_info_cache=mr_info_cache, send_queue=send_queue)


if __name__ == "__main__":
    main(sys.argv[1:])
