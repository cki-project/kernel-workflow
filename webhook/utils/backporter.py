#!/usr/bin/env python
"""Try to backport upstream commit referenced in provided Jira issue."""

import os
import sys
import typing

from cki_lib import logger

from webhook import common
from webhook import kgit
from webhook import libbackport
from webhook.rhissue import make_rhissues
from webhook.session import SessionRunner
from webhook.session_events import UmbBridgeEvent

LOGGER = logger.get_logger('webhook.utils.backporter')


def process_event(
    _: dict,
    session: SessionRunner,
    event: UmbBridgeEvent,
    **__: typing.Any
) -> None:
    """Process an event."""
    rhissue = make_rhissues([event.jira_key], None,
                            jira=session.jira, projects=session.rh_projects)[0]
    LOGGER.debug("Got issue %s (%s), commit hashes: %s",
                 rhissue.id, rhissue.ji.fields.summary, rhissue.ji_commit_hashes)

    if not session.args.testing:
        LOGGER.info("Setting up git user: %s <%s>", session.args.name, session.args.email)
        kgit.setup_git_user(session.args.rhkernel_src, session.args.name, session.args.email)

    backport = libbackport.Backport(rhissue, session)

    if (log_msg := backport.should_try_backport()):
        LOGGER.warning(log_msg)
        return

    LOGGER.debug("%s", backport)

    if backport.try_to_backport():
        if backport.submit_merge_request():
            backport.add_info_to_jira_issue(success=True)
    else:
        backport.add_info_to_jira_issue(success=False)

    if not session.args.testing:
        backport.cleanup()
    LOGGER.info(backport.status)


HANDLERS = {
    UmbBridgeEvent: process_event
}


def main(args) -> None:
    """Find open MRs and check for merge conflicts."""
    common.init_sentry()
    parser = common.get_arg_parser('BACKPORTER')
    parser.add_argument('-r', '--rhkernel-src', default=os.environ.get('RHKERNEL_SRC', ''),
                        help='Directory containing RH kernel projects git tree')
    parser.add_argument('-T', '--testing', action='store_true', default=False,
                        help="Run in testing mode, omit certain checks")
    parser.add_argument('-n', '--name', default=os.environ.get('GIT_COMMITTER_NAME', ''),
                        help="Name to use for git commits and signoffs")
    parser.add_argument('-e', '--email', default=os.environ.get('GIT_COMMITTER_EMAIL', ''),
                        help="Email address to use for git commits and signoffs")
    pargs = parser.parse_args(args)

    if not pargs.rhkernel_src:
        LOGGER.warning("No path to RH Kernel source git found, aborting!")
        return

    session = SessionRunner.new('backporter', args=pargs, handlers=HANDLERS)
    session.run()


if __name__ == '__main__':
    main(sys.argv[1:])
