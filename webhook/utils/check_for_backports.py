#!/usr/bin/env python
"""Look for z-stream jira issues with ready y-stream MRs to auto-backport from."""
from dataclasses import dataclass
import os
import sys
import typing

from cki_lib import logger
from cki_lib.misc import is_production_or_staging

from webhook import common
from webhook.base_mr import BaseMR
from webhook.base_mr_mixins import ApprovalsMixin
from webhook.base_mr_mixins import CommitsMixin
from webhook.defs import BOT_ACCOUNTS
from webhook.defs import JiraField
from webhook.defs import READY_FOR_MERGE_LABEL
from webhook.libbackport import Backport
from webhook.rhissue import make_rhissues
from webhook.session import SessionRunner

if typing.TYPE_CHECKING:
    from jira import JIRA
    from jira.resources import Issue

    from webhook.rhissue import RHIssue
    from webhook.users import User

LOGGER = logger.get_logger('webhook.utils.check_for_backports')

# We only want kernel bugs in the RHEL project that aren't already labeled as having had a
# backport attempt done (or an MR attached), aren't already Closed, and have an empty
# Commit Hashes field. Some further filtering to be done on top of this...
BASE_JQL = ('project = RHEL AND component in componentMatch("^kernel($| / .*$)") AND '
            'labels not in (kwf-backport-fail, kwf-backport-success, kwf-mr-linked) AND '
            'status in (New, Planning) AND "Commit Hashes" is EMPTY AND '
            '"Embargo Status" != "True" ')
JIRA_FIELDS = ['labels', 'versions', 'fixVersions', 'issuelinks', JiraField.Commit_Hashes]


@dataclass(repr=False)
class YstreamMR(ApprovalsMixin, CommitsMixin, BaseMR):
    """Represent a Merge Request with Approvals and Commits data."""


def find_issues_for_fix_version(jira: 'JIRA', fix_version: str) -> list['Issue']:
    """Find jira issues for the given fix_version."""
    jql_str = BASE_JQL + f'AND fixVersion = {fix_version}'
    issue_list = jira.search_issues(jql_str=jql_str, maxResults=500, fields=JIRA_FIELDS)
    return issue_list


def get_zstream_maintainer(z_mr: BaseMR, session: SessionRunner) -> str | None:
    """Assign the MR to the stream maintainer."""
    search_for = f'RHEL {z_mr.rh_branch.major}.{z_mr.rh_branch.minor} Kernel Maintainer'
    maintainer_ss = session.owners.get_matching_subsystem_by_name(search_for)
    if not maintainer_ss:
        LOGGER.warning("No maintainer found for '%s'", search_for)
        return None
    return maintainer_ss.maintainers[0]


def build_assignment_comment(
    z_maintainer: str,
    y_approved_by: list['User'],
    y_author: 'User',
    y_mr_url: str
) -> str:
    """Build a comment string to leave in gitlab MR w/notes and assignments."""
    reviewers = set(y_approved_by)
    if y_author.username not in BOT_ACCOUNTS:
        reviewers.add(y_author)
    review_tags = [f'@{reviewer.username}' for reviewer in reviewers]
    review_tags.sort()

    LOGGER.info("Assigning MR to @%s and setting %s as reviewers", z_maintainer, review_tags)
    return ('Please review this automated z-stream Merge Request, as you were either an author or '
            'approver of the y-stream Merge Request from which it was generated.\n\n'
            f'Y-Stream Merge Request: {y_mr_url}  \n'
            f'Y-Stream MR Author: {y_author.name}  \n'
            f'Y-Stream MR Approvers: {", ".join([approver.name for approver in y_approved_by])}  \n'
            '\n'
            f'/assign @{z_maintainer}\n'
            f'/assign_reviewer {" ".join(review_tags)}')


def report_success_in_jira(backport: Backport) -> None:
    """Report our successful backport in the originating Jira issue."""
    backport.add_info_to_jira_issue(success=True)
    field_text = "# lead stream commit hashes backported\n"
    field_text += ' '.join(backport.provided_hashes)
    fields = {JiraField.Commit_Hashes: field_text}
    if is_production_or_staging():
        backport.rhissue.ji.update(fields=fields)


def file_gitlab_tps_reports(session: SessionRunner, backport: Backport, y_mr: YstreamMR) -> None:
    """Do administrative tasks following successful merge request creation."""
    z_mr = BaseMR.new(session, backport.mr_url)
    maintainer = get_zstream_maintainer(z_mr, session)
    y_approved_by = [user for user in y_mr.approved_by if user.username not in BOT_ACCOUNTS]
    y_author = y_mr.author

    assignment_comment = build_assignment_comment(maintainer, y_approved_by, y_author, y_mr.url)
    session.update_webhook_comment(z_mr.gl_mr, assignment_comment)


def attempt_backport(rhissue: 'RHIssue', session: SessionRunner, y_mr: YstreamMR) -> bool:
    """Attempt to perform a backport for the z-stream issue."""
    if rhissue.mr_urls:
        LOGGER.info("Issue %s already has a merge request linked to it", rhissue.id)
        return False

    backport = Backport(rhissue, session)
    backport.provided_hashes = list(reversed(y_mr.commits.keys()))
    LOGGER.debug("%s", backport)

    extra_info = [f'Backported from: {y_mr.url} ({y_mr.rh_branch.name})  ']
    # We already have assured there's only one y-stream jira issue
    extra_info.append(f'Y-Jira: {rhissue.cloned_from[0].jira_link}  ')
    extra_info.append('Commits backported:  ')

    success = True
    if backport.try_to_backport(extra_info=extra_info):
        if backport.submit_merge_request():
            file_gitlab_tps_reports(session, backport, y_mr)
            report_success_in_jira(backport)
    else:
        success = False
        backport.add_info_to_jira_issue(success=False)

    if not session.args.testing:
        backport.cleanup()
    LOGGER.info(backport.status)
    return success


def find_backport_work(session: SessionRunner, issue: 'Issue') -> bool:
    """Find y-stream bug and matching MRs to auto-backport commits from."""
    rhissue = make_rhissues([issue.key], None, jira=session.jira,
                            projects=session.rh_projects)[0]
    backport_doable = True

    if rhissue.embargoed:
        LOGGER.warning("Refusing to backport an issue (%s) marked as embargoed", issue.key)
        backport_doable = False

    y_rhissues = rhissue.cloned_from

    if len(y_rhissues) > 1:
        LOGGER.warning("I have no idea what to do with an issue (%s) that is apparently cloned "
                       "from %s different issues: %s",
                       issue.key, len(y_rhissues), [rhi.id for rhi in y_rhissues])
        backport_doable = False
    if not y_rhissues:
        LOGGER.info("Issue %s does not have a y-stream issue we can find", issue.key)
        backport_doable = False

    if not backport_doable:
        return False

    LOGGER.debug("Got y-stream issue list of %s", y_rhissues)
    y_rhissue = y_rhissues[0]
    y_mr_urls = y_rhissue.mr_urls

    if len(y_mr_urls) > 1:
        LOGGER.warning("Cowardly refusing to try to backport from a y-stream issue (%s) with "
                       "multiple Merge Requests linked to it: %s",
                       issue.key, [f'{mr.namespace}/!{mr.id}' for mr in y_mr_urls])
        backport_doable = False
    if not y_mr_urls:
        LOGGER.info("Issue %s's lead stream issue has no MR attached.", issue.key)
        backport_doable = False

    if not backport_doable:
        return False

    y_mr = YstreamMR.new(session, y_mr_urls[0])
    # This will capture both merged and about to be merged MRs
    ready_for_merge = READY_FOR_MERGE_LABEL in y_mr.labels

    if y_mr.commits and ready_for_merge:
        return attempt_backport(rhissue, session, y_mr)
    return False


def main(args) -> None:
    """Find Jira issues in need of backports."""
    common.init_sentry()
    parser = common.get_arg_parser('BACKPORTER')
    parser.add_argument('-T', '--testing', action='store_true', default=False,
                        help="Run in testing mode, omit certain checks")
    parser.add_argument('-p', '--projects', default=os.environ.get('GL_PROJECTS', '').split(),
                        help="GitLab projects to search for possible backport candidates",
                        nargs='+', required=False)
    parser.add_argument('-f', '--fix-versions', default=os.environ.get('FIX_VERSIONS', '').split(),
                        help="Fix Versions to search for possible backport candidates", nargs='+',
                        required=False)
    parser.add_argument('-r', '--rhkernel-src', default=os.environ.get('RHKERNEL_SRC', ''),
                        help='Directory containing RH kernel projects git tree')
    parser.add_argument('-n', '--name', default=os.environ.get('GIT_COMMITTER_NAME', ''),
                        help="Name to use for git commits and signoffs")
    parser.add_argument('-e', '--email', default=os.environ.get('GIT_COMMITTER_EMAIL', ''),
                        help="Email address to use for git commits and signoffs")
    pargs = parser.parse_args(args)

    session = SessionRunner.new('backporter', args=pargs)

    issue_lists = {}
    fix_versions = set()
    if session.args.jira:
        jql_str = ' OR '.join(f'key={issue}' for issue in session.args.jira.split(','))
        issue_lists['cli'] = session.jira.search_issues(jql_str=jql_str, fields=JIRA_FIELDS)

    for fix_version in session.args.fix_versions:
        fix_versions.add(fix_version)

    for project_ns in session.args.projects:
        project = session.rh_projects.get_project_by_namespace(project_ns)
        if not project:
            LOGGER.warning("No project found for project namespace: %s", project_ns)
            continue
        for branch in project.branches:
            # Filter out lead stream and inactive stream branches
            if branch.lead_stream or branch.inactive:
                continue
            for fix_version in branch.fix_versions:
                if fix_version.zstream:
                    fix_versions.add(fix_version)

    for fix_version in fix_versions:
        issue_lists[fix_version] = find_issues_for_fix_version(session.jira, fix_version)
        LOGGER.info("Got issue list of %s items for %s",
                    len(issue_lists[fix_version]), fix_version)

    successful_backports = 0
    for fix_version, jira_issues in issue_lists.items():
        LOGGER.info("Evaluating %s issues: %s", fix_version, jira_issues)
        for issue in jira_issues:
            if find_backport_work(session, issue):
                successful_backports += 1
    LOGGER.info("Successful backports: %s", successful_backports)


if __name__ == '__main__':
    main(sys.argv[1:])
